rrwm.pk3 (and rrwm_hud.pk3, if -with_hud switch is enabled) will appear here after running the build script (build.ps1) from the main folder.

If something went wrong, detailed information can be found in the log file, which will also appear in this directory after executing the build script.
