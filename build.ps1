##############################################################################
#                                                                            #
# RRWM PowerShell build script                                               #
# by Player701                                                               #
#                                                                            #
# This script has two modes: a build-only mode and a build+test mode.        #
# The mode is specified by the "-mode" argument. Default is "build",         #
# for build+test use "test".                                                 #
#                                                                            #
# Other arguments:                                                           #
#                                                                            #
# -with_hud             : Builds the standalone HUD package.                 #
#                         This usually takes some time due to the need       #
#                         to process source files, so it is disabled         #
#                         by default.                                        #
#                                                                            #
# -dir_src       <path> : Override the path to RRWM sources.                 #
#                         Default is "src".                                  #
#                                                                            #
# -dir_build     <path> : Override the output path where the resulting       #
#                         PK3 file will appear. Default is "build".          #
#                                                                            #
# -archiver_path <path> : Override the path to the 7-Zip archiver.           #
#                         Default is "C:\Program Files\7-Zip\7z.exe",        #
#                         or "/usr/bin/7z" (Linux)                           #
#                                                                            #
# -pandoc_path   <path> : Override the path to Pandoc.                       #
#                         Default is "C:\Program Files\pandoc\pandoc.exe".   #
#                         Pandoc is used to convert Markdown documents       #
#                         to PDFs when preparing a release archive.          #
#                         Using Pandoc is optional; if it is not found,      #
#                         PDF creation will be skipped.                      #
#                                                                            #
# -test_file     <path> : Path to a text file which contains the GZDoom      #
#                         path on the first line and GZDoom arguments        #
#                         on the second line. Adding an argument to load     #
#                         RRWM itself is unnecessary.                        #
#                         Defaults to "build.test", will be generated        #
#                         if it doesn't exist and updated automatically.     #
#                                                                            #
# -gzdoom_path <path>   : Path to GZDoom. If omitted, the path from the      #
#                         test file will be used.                            #
#                                                                            #
# -gzdoom_args <args>   : Command-line arguments to pass to GZDoom. Adding   #
#                         an argument to load RRWM itself is unnecessary.    #
#                         Defaults to an empty string. If omitted,           #
#                         the arguments from the test file will be used.     #
#                                                                            #
##############################################################################
param(
    [string] $mode = "build",
    [string] $dir_src,
    [string] $dir_build,
    [string] $archiver_path = $(if ($IsWindows -or $ENV:OS -eq "Windows_NT") { "C:\Program Files\7-Zip\7z.exe" } else { "/usr/bin/7z" }),
    [string] $pandoc_path = "C:\Program Files\Pandoc\pandoc.exe",
    [string] $test_file = "build.test",
    [string] $gzdoom_path,
    [string] $gzdoom_args,
    [switch] $with_hud,
    [switch] $debug
);

$project_name = "rrwm";
$project_ext  = "pk3";
$version_template_name = "VersionInfoTemplate.txt";

# We have to terminate on uncaught errors.
$ErrorActionPreference = "Stop";

# Get the path to this script (PowerShell v1 compatible).
$script_root = Split-Path -Parent $MyInvocation.MyCommand.Definition;

# Main()
# Entry point for the script.
function Main()
{
    $log_file = Join-Path $script_root "build.log";
    Start-Transcript $log_file;

    Write-Section "RRWM build script by Player701";

    # Resolve paths from parameters or environment variables
    $archiver_path = Get-ParamOrEnvVar $archiver_path "RRWM_ARCHIVER_PATH" "7-Zip";
    $pandoc_path = Get-ParamOrEnvVar $pandoc_path "RRWM_PANDOC_PATH" "Pandoc";

    # Get the display string for the current mode
    # and also check if the mode is valid.
    $display_mode = Check-Mode $mode;

    Write-Host "Mode: ${display_mode}";

    # Check that we have 7-Zip
    if (-not (Test-Path $archiver_path))
    {
        Throw "Could not find 7-Zip archiver at ${archiver_path}";
    }

    # Resolve full paths for all directories.
    $dir_src   = Get-Path $dir_src   "src";
    $dir_build = Get-Path $dir_build "build";

    Write-Host "Source path: ${dir_src}";
    Write-Host "Output path: ${dir_build}";
    Write-Host "7-Zip path: ${archiver_path}";

    # Do stuff based on the provided mode.
    switch ($mode)
    {
        "build"   { Do-Build;               }
        "test"    { Do-Build; Do-Test;      }
        "release" { Do-Release $(Do-Build); }
    }

    Write-Section "Finished";
    return 0;
}

# Get-ParamOrEnvVar()
# Returns the provided value if the path exists.
# If it doesn't, checks the value of the corresponding environment variable
# and if it exists, returns the result.
function Get-ParamOrEnvVar($value, $env_var_name, $what)
{
    if (-not (Test-Path $value))
    {
        Write-Warning "Could not find ${what} at ${value}";
        Write-Host "Checking environment variable: ${env_var_name}...";

        $env_path = "env:\${env_var_name}";
        $env_found = $false;

        if (Test-Path $env_path)
        {
            $env_value = (Get-Item $env_path).Value;

            if ($env_value)
            {
                Write-Host "OK: path = ${env_value}";
                $value = $env_value;
                $env_found = $true;
            }
        }

        if (-not $env_found)
        {
            Write-Warning "Could not find value for environment variable ${env_var_name}";
        }
    }

    return $value;
}

# Get-Path()
# Returns the resolved absolute path from the provided value,
# or a default path if the value is empty.
function Get-Path($path, $default_path)
{
    # If path is not defined,
    # use a path relative to the script root directory.
    if (-not $path)
    {
        return Join-Path $script_root $default_path;
    }

    # Resolve the path normally if it is defined.
    return Resolve-Path $path;
}

# Check-Mode()
# Used to verify the mode and return a friendly name for it.
function Check-Mode($mode)
{
    switch ($mode)
    {
        "build"   { return "Build only"; }
        "test"    { return "Build and test"; }
        "release" { return "Build and make a release archive"; }

        default { Throw "Invalid mode: `"${mode}`""; }
    };
}

# Get-ProjectFileName()
# Returns the project name with an absolute path.
function Get-ProjectFileName()
{
    return Join-Path $dir_build "${project_name}.${project_ext}";
}

# Get-HudProjectFileName()
# Returns the name of the HUD project with an absolute path.
function Get-HudProjectFileName()
{
    return Join-Path $dir_build "${project_name}_hud.${project_ext}";
}

# Do-Build()
# Performs build tasks.
function Do-Build()
{
    $version_string = Update-VersionInfo;

    Build-Package;

    if ($with_hud)
    {
        Build-HudPackage;
    }

    return $version_string;
}

# Build-Package()
# Builds the normal RRWM package.
function Build-Package()
{
    Write-Section "Building package";

    $staging_path = $dir_src + "_staging";

    Create-Directory $staging_path;
    Try
    {
        BeginBuild-Package $dir_src $staging_path;
        EndBuild-Package $staging_path (Get-ProjectFileName);
    }
    Finally
    {
        Remove-Item $staging_path -Force -Recurse;
    }
}

# Build-HudPackage()
# Builds the standalone HUD package.
function Build-HudPackage()
{
    Write-Section "Building HUD package";

    $hud_src_path = $dir_src + "_hud";
    $staging_path = $hud_src_path + "_staging";
    $asset_list = Join-Path $script_root "HudAssetList.txt";

    Create-Directory $staging_path;
    Try
    {
        BeginBuild-Package $hud_src_path $staging_path;
        Process-HudFiles $dir_src $asset_list $staging_path;
        EndBuild-Package $staging_path (Get-HudProjectFileName);
    }
    Finally
    {
        Remove-Item $staging_path -Force -Recurse;
    }
}

# BeginBuild-Package()
# Copies the widget library files and primary source files
# to the provided staging path.
function BeginBuild-Package($source_path, $staging_path)
{
    $wl_src_path = $dir_src + "_wl";

    Copy-AllFiles $wl_src_path $staging_path;
    Copy-AllFiles $source_path $staging_path;
}

# EndBuild-Package()
# Removes unused ZScript files and creates a PK3 archive
# from the contents of the provided staging folder.
function EndBuild-Package($staging_path, $output_file)
{
    Remove-UnusedClasses $staging_path;
    Package-Files $staging_path $output_file;
}

# Remove-UnusedClasses()
# Removes unused ZScript files from the staging folder.
function Remove-UnusedClasses($staging_path)
{
    foreach ($zscript_file in Get-ChildItem $staging_path -Name -File -Filter "zscript.*")
    {
        Remove-UnusedClassesFromFile $staging_path $zscript_file;
    }
}

# Remove-UnusedClassesFromFile()
# Processes a single ZScript include file and removes unused files
# that have been commented out.
function Remove-UnusedClassesFromFile($zscript_path, $zscript_file)
{
    Write-Host "Checking for unused script files in ${zscript_file}...";

    foreach ($line in Get-Content (Join-Path $zscript_path $zscript_file))
    {
        if ($line -match "^\/\/#include `"(.*)`"")
        {
            $unused_count++;
            $excluded = $Matches.1;

            Remove-Item (Join-Path $zscript_path $excluded);
            Write-Host "Removed unused file ${excluded}";
        }
    }

    if ($unused_count -gt 0)
    {
        Write-Host "Removed ${unused_count} unused file(s).";
    }
    else
    {
        Write-Host "No unused files to remove.";
    }
}

# Process-HudFiles()
# Processes all RRWM source files from the asset list
# and puts them into the HUD staging directory.
function Process-HudFiles($source_path, $asset_list, $target_path)
{
    Write-Host "Processing HUD package files... please stand by.";

    foreach ($line in Get-Content $asset_list)
    {
        $raw = ($line -like "!*");
        $query = ($line -like "``?*");

        if ($raw -or $query)
        {
            $line = $line.Substring(1);
        }

        $result = Process-HudFile $source_path $line $target_path $raw $false;

        if ($query)
        {
            foreach ($line in Get-Content $result)
            {
                if ($line -match "^#include `"(.*)`"")
                {
                    Process-HudFile $source_path $Matches.1 $target_path $false $true | Out-Null;
                }
            }
        }
    }

    Write-Host "All source files have been processed.";
}

# Get-TagName()
# Checks if a line matches an opening or closing include/exclude tag pattern,
# and returns the corresponding tag name in this case.
function Get-TagName($line, $is_markdown)
{
    $pattern = if ($is_markdown) { "^\s*\[\/\/\]:\s+#\s+\(<(\/)?(\S+)>\)\s*$" } else { "^\s*\/\/\s*<(\/)?(\S+)>\s*$" }
    if ($line -match $pattern)
    {
        return $Matches.2, $Matches.1 -ne $null;
    }

    return $null, $null;
}

# Get-TagKeyAndMode()
# Returns the key and mode of the include/exclude tag
# provided that it matches one of the known patterns.
function Get-TagKeyAndMode($tag)
{
    if ($tag -match "(\S+)Only")
    {
        return $Matches.1, $true;
    }
    elseif ($tag -match "(Not|ExcludeFrom)(\S+)")
    {
        return $Matches.2, $false;
    }

    return $null, $null;
}

# Get-InlineTagName()
# Checks if a line matches an opening or closing include/exclude inline tag pattern,
# and returns the corresponding tag name in this case.
# Also returns the two parts of the line before and after the tag.
function Get-InlineTagName($line)
{
    $match = [regex]::Match($line, "\/\*\s*<(\/)?(\S+?)>\s*\*\/");

    if ($match.Success)
    {
        $ind = $match.Groups[0].Index;
        $len = $match.Groups[0].Value.Length;
        $closing = $match.Groups[1].Success
        $tag = $match.Groups[2].Value;

        return $tag, $closing, $line.Substring(0, $ind), $line.Substring($ind + $len);
    }

    return $null, $null, $line, "";
}

# Process-AssetLine()
# Removes all include/exclude tags from the line
# and the content between them according to keys_enabled
# as well as the corresponding tag modes.
function Process-AssetLine($src, $line, $linenum, $keys_enabled, $stack)
{
    $sb = New-Object System.Text.StringBuilder;
    $writing = $next_writing = $true;

    if ($stack.Count -eq 0)
    {
        $stack.Push(@($null, $null));
    }

    while ($line.length -gt 0)
    {
        # get first opening OR closing tag
        $tag, $closing, $prev, $line = Get-InlineTagName $line;

        if ($tag -ne $null)
        {
            if ($closing)
            {
                 # make sure this is a valid tag
                $key = Get-TagKeyAndMode $tag;

                if ($key -ne $null)
                {
                    $stack_top = $stack.Peek();
                    $last_tag = $stack_top[0];

                    # check for this EXACT tag (not key!) on the stack
                    if ($last_tag -ne $tag)
                    {

                        if ($last_tag -ne $null)
                        {
                            Throw "Error: Include/exclude tag mismatch in file ${src} line ${linenum}, </${tag}> found but <${last_tag}> seen previously on the same line";
                        }
                        else
                        {
                            Throw "Error: Include/exclude tag mismatch in file ${src} line ${linenum}, </${tag}> found but no opening tags seen previously on the same line";
                        }
                    }

                    # ok, now pop the stack
                    $next_writing = $stack_top[1];
                    $stack.Pop() | Out-Null;
                }
            }
            else
            {
                # get key and mode (positive = Only, negative = Not/ExcludeFrom)
                $key, $mode = Get-TagKeyAndMode $tag;

                if ($key -ne $null)
                {
                    $new_writing = $null;

                    # check for known keys
                    $i = 0;
                    $found = $false;
                    while (($i -lt $keys_enabled.length) -and (-not $found))
                    {
                        if ($key -eq $keys_enabled[$i])
                        {
                            # found known key
                            $found = $true;
                            $push_key = $key;
                            $new_writing = $mode;
                        }

                        $i++;
                    }

                    # Check if key not found and relation is positive,
                    # then we must skip this block, otherwise include
                    if (-not $found)
                    {
                        $push_key = $key;
                        $new_writing = -not $mode;
                    }

                    # Push tag onto the stack and change mode
                    if ($new_writing -ne $null)
                    {
                        $stack.Push(@($tag, $writing));
                        $next_writing = ($new_writing -and $writing);
                    }
                }
            }
        }

        if ($writing)
        {
            $sb.Append($prev) | Out-Null;
        }

        $writing = $next_writing;
    }

    # make sure we have no more tags on the stack
    $last_tag = $stack.Peek()[0];

    if ($last_tag -ne $null)
    {
        Throw "Error: Include/exclude tag mismatch in file ${src} line ${linenum}, no closing tag found for <${last_tag}> seen previously on the same line";
    }

    return $sb.ToString();
}

# Process-AssetFile()
# Copies the content of the source file to the target path,
# removing sections that are excluded from the HUD package
# or from release builds
function Process-AssetFile($src_path, $asset_name, $target_path, $raw, $keys_enabled, $skip_if_exists)
{
    $src = Join-Path $src_path $asset_name;
    $dest = Join-Path $target_path $asset_name;
    $folder = (Split-Path $dest -Parent);

    if (Test-Path $dest)
    {
        if ($skip_if_exists)
        {
            Write-Host "Skipped ${asset_name} (already exists in target folder)";
            return $dest;
        }
        else
        {
            Throw "Error: Asset file ${asset_name} already exists in target folder";
        }
    }

    if (-not (Test-Path $folder))
    {
         New-Item -Path $folder -ItemType "directory" | Out-Null;
    }

    if ($raw)
    {
        Copy-Item $src $dest;
    }
    else
    {
        New-Item -Path $dest -ItemType "file" | Out-Null;
        $stream = New-Object IO.StreamWriter $dest, $false, (New-Object Text.UTF8Encoding $false);
        $writing = $true;
        $linenum = 0;
        $is_markdown = $asset_name -match "\.md$";

        $stack_inline = New-Object System.Collections.Stack;
        $stack = New-Object System.Collections.Stack;
        $stack.Push(@($null, $null, $null));

        Try
        {
            foreach ($line in Get-Content $src -Encoding UTF8)
            {
                $linenum++;

                # check for closing tags first
                $tag, $closing = Get-TagName $line $is_markdown;

                if ($tag -ne $null)
                {
                    if ($closing)
                    {
                        # make sure this is a valid tag
                        $key = Get-TagKeyAndMode $tag;

                        if ($key -ne $null)
                        {
                            $stack_top = $stack.Peek();
                            $last_tag = $stack_top[0];
                            $prev_linenum = $stack_top[1];

                            # check for this EXACT tag (not key!) on the stack
                            if ($last_tag -ne $tag)
                            {
                                if ($last_tag -ne $null)
                                {
                                    Throw "Error: Include/exclude tag mismatch in file ${src} line ${linenum}, </${tag}> found but <${last_tag}> seen previously on line ${prev_linenum}";
                                }
                                else
                                {
                                    Throw "Error: Include/exclude tag mismatch in file ${src} line ${linenum}, </${tag}> found but no opening tags seen previously";
                                }
                            }

                            # ok, now pop the stack
                            $writing = $stack_top[2];
                            $stack.Pop() | Out-Null;
                        }
                    }
                    else
                    {
                        # get key and mode (positive = Only, negative = Not)
                        $key, $mode = Get-TagKeyAndMode $tag;

                        if ($key -ne $null)
                        {
                            $new_writing = $null;

                            # check for known keys
                            $i = 0;
                            $found = $false;
                            while (($i -lt $keys_enabled.length) -and (-not $found))
                            {
                                if ($key -eq $keys_enabled[$i])
                                {
                                    # found known key
                                    $found = $true;
                                    $push_key = $key;
                                    $new_writing = $mode;
                                }

                                $i++;
                            }

                            # Check if key not found and relation is positive,
                            # then we must skip this block, otherwise include
                            if (-not $found)
                            {
                                $push_key = $key;
                                $new_writing = -not $mode;
                            }

                            # Push tag onto the stack and change mode
                            if ($new_writing -ne $null)
                            {
                                $stack.Push(@($tag, $linenum, $writing));
                                $writing = ($new_writing -and $writing);
                            }
                        }
                    }
                }
                elseif ($writing)
                {
                    # write line if writing is enabled
                    if (-not $is_markdown)
                    {
                        $line = Process-AssetLine $src $line $linenum $keys_enabled $stack_inline;
                    }

                    $stream.WriteLine($line);
                }
            }

            # make sure we have no more tags on the stack
            $stack_top = $stack.Peek();
            $last_tag = $stack_top[0];
            $prev_linenum = $stack_top[1];

            if ($last_tag -ne $null)
            {
                Throw "Error: Include/exclude tag mismatch in file ${src}, no closing tag found for <${last_tag}> seen previously on line ${prev_linenum}";
            }
        }
        Finally
        {
            $stream.Close();
        }
    }

    Write-Host "Processed ${asset_name}";
    return $dest;
}

# Process-HudFile()
# Copies the content of the source file to the target path,
# removing sections that are excluded from the HUD package
function Process-HudFile($src_path, $asset_name, $target_path, $raw, $skip_if_exists)
{
    Process-AssetFile $src_path $asset_name $target_path $raw ((Get-EnabledKeys) + "HudPackage") $skip_if_exists;
}

# Get-EnabledKeys()
# Returns an array with keys for include/exclude tags
# that should always be enabled in the current build configuration.
function Get-EnabledKeys()
{
    return ,@(if ($debug) { "Debug" } else { "Release" });
}

# Copy-AllFiles()
# Copies all files from one directory to another.
function Copy-AllFiles($source_path, $target_path)
{
    Write-Host "Processing files from ${source_path} into staging folder...";

    $keys_enabled = (Get-EnabledKeys);

    foreach ($item in Get-ChildItem $source_path -Recurse -Name -File)
    {
        $raw = $item -notmatch "\.(txt|wl|zs)$";
        Process-AssetFile $source_path $item $target_path $raw $keys_enabled $false | Out-Null;
    }

    Write-Host "All files have been processed successfully.";
}

# Update-VersionInfo()
# Retrieves the version info from Git and updates the version template file.
function Update-VersionInfo()
{
    Write-Section "Updating version info";

    $version_info = Get-VersionInfo;
    $version_string = $version_info[0];
    $version_timestamp = $version_info[1];

    Update-VersionInfoTemplate $version_string $version_timestamp;

    return $version_string;
}

# Get-VersionInfo()
# Returns the Git version string and timestamp.
function Get-VersionInfo()
{
    # Get the version string from Git.
    $version_string = & "git" "describe" "--tags" "--dirty=-m";
    if (-not $?)
    {
        Write-Warning "Error getting version string: Git returned exit code ${LastExitCode}";
        $version_string = "Unknown";
    }
    else
    {
        # Get the version timestamp from Git.
        $version_timestamp = $version_string | & "git" "log" "-1" "--format=%ai";
        if (-not $?)
        {
            Write-Warning "Error getting version timestamp: Git returned exit code ${LastExitCode}";
        }
    }

    # If failed to get the timestamp, fallback to "Unknown".
    if (-not $version_timestamp)
    {
        $version_timestamp = "Unknown";
    }

    $version_string;
    $version_timestamp;
    return;
}

# Update-VersionInfoTemplate()
# Updates RRWM version info with the provided version string and timestamp.
function Update-VersionInfoTemplate($version_string, $version_timestamp)
{
    Write-Host "Version string: ${version_string}";
    Write-Host "Version timestamp: ${version_timestamp}";

    $version_template_file = Join-Path $script_root $version_template_name;
    $version_output_file = Join-Path $dir_src "classes\rrwm\common\RR_VersionInfo.zs";

    # Make sure the version template file exists.
    if (-not (Test-Path $version_template_file))
    {
        Throw "Error: could not find template file ${version_template_file}";
    }

    # This scary-looking thing reads the template file's contents,
    # substitutes placeholders with actual values and writes
    # the destination file with the substituted values.
    (Get-Content $version_template_file)                                  `
    | Where { $_ -notmatch "^\/\/!" }                                     `
    | % { $_.replace("%VERSION_STRING%",        $version_string       ) } `
    | % { $_.replace("%VERSION_TIMESTAMP%",     $version_timestamp    ) } `
    | % { $_.replace("%VERSION_TEMPLATE_NAME%", $version_template_name) } `
    | Set-Content $version_output_file;

    Write-Host "Values written to: ${version_output_file}";
}

# Package-Files()
# Creates a PK3 archive with RRWM source files.
function Package-Files($folder_name, $output_file)
{
    Write-Section "Packaging files";

    $project_file = Get-ProjectFileName;
    Archive-Files $archiver_path $folder_name $output_file;
}

# Archive-Files()
# Creates an archive from the contents of a directory.
function Archive-Files($archiver_path, $input_dir, $output_file)
{
    $log_file = Replace-Extension $output_file "log";

    & $archiver_path "a" "-tzip" "-mm=deflate" "-mx=9" "-bb2" $output_file "${input_dir}/*" "-up0q0r2x2y2z1w2" `
    | Tee-Object $log_file `
    | Log-WithPrefix       `
    | Write-Host;

    # Check that we've successfully packaged all files.
    if (-not $?)
    {
        Throw "Error packaging files: 7-Zip returned exit code ${LastExitCode}";
    }

    Write-Host "Files packaged successfully.";
    Write-Host "Output file: ${output_file}";
    Write-Host "Log file: ${log_file}";
}

# Replace-Extension()
# Replaces the extension of the file name with a new one.
function Replace-Extension($file, $new_ext)
{
    $dir = [System.IO.Path]::GetDirectoryName($file);
    $file_no_ext = [System.IO.Path]::GetFileNameWithoutExtension($file);
    return Join-Path $dir "${file_no_ext}.${new_ext}";
}

# Log-WithPrefix()
# Append ">> " to piped input to indicate it came from an external program.
function Log-WithPrefix
{
    param([parameter(ValueFromPipeline=$true)] $x);
    Process
    {
        return ">> ${x}";
    }
}

# Do-Test()
# Performs tasks to run GZDoom for testing.
function Do-Test
{
    Write-Section "Preparing GZDoom for testing";
    $test_params = Get-TestParameters;
    $gzdoom_path = $test_params[0];
    $gzdoom_args = $test_params[1];
    Run-GZDoom;
}

# Run-GZDoom()
# Runs GZDoom after its path and arguments have been resolved.
function Run-GZDoom()
{
    # Check that the GZDoom executable exists.
    if (-not (Test-Path $gzdoom_path))
    {
        Throw "Error: GZDoom executable not found at the specified path.";
    }

    $file_name = if ($with_hud) { Get-HudProjectFileName } else { Get-ProjectFileName };
    $project_file = MustGet-File $file_name;

    # If arguments are provided, split them to individual parts.
    # Otherwise, they will not be recognized.
    if ($gzdoom_args)
    {
        $gzdoom_args = $gzdoom_args.Split(" ");
    }

    Write-Host "Starting GZDoom...";
    & $gzdoom_path "-file" $project_file $gzdoom_args;
}

# MustGet-File()
# Returns the file name
# or throws an error if the file doesn't exist.
function MustGet-File($file_name)
{
    # Check that the project file exists.
    if (-not (Test-Path $file_name))
    {
        Throw "Error: project file not found in the output directory.";
    }

    return $file_name;
}

# Get-TestParameters()
# Gets GZDoom path and argument from the command line or the test file,
# based on where they are found. Command line has preference.
# Updates the test file afterwards.
function Get-TestParameters()
{
    # By default, we'll use the command line.
    $result = $gzdoom_path, $gzdoom_args;

    # Check that the test file exists.
    $test_file = Join-Path $pwd $test_file;
    $test_path = Test-Path $test_file;

    if ($test_path)
    {
        Write-Host "Test file: ${test_file}";
        $testfile_data = Get-Content $test_file -TotalCount 2;

        # If GZDoom path is not specified, try to use data from the test file.
        if (-not $gzdoom_path)
        {
            if ($testfile_data.length -gt 0)
            {
                $result[0] = $testfile_data[0];
                Write-Host "Using GZDoom path from test file.";
            }
            else
            {
                Throw "Error: GZDoom path not specified and not found in test file.";
            }
        }

        # If GZDoom arguments are not specified, try to use arguments from the test file.
        if ((-not $gzdoom_args) -and ($testfile_data.length -gt 1))
        {
            $result[1] = $testfile_data[1];
            Write-Host "Using GZDoom arguments from test file.";
        }
    }
    else
    {
        # If not test file found, check that GZDoom path is provided.
        Write-Host "No test file found.";

        if (-not $gzdoom_path)
        {
            Throw "Error: GZDoom path not specified.";
        }
    }

    # Output the resulting paths.
    Write-Host "GZDoom path: $($result[0])";
    if ($result[1])
    {
        Write-Host "GZDoom arguments: $($result[1])";
    }

    try
    {
        # Try to update the test file. It is not a critical error if we fail to do that,
        # but we still need to print a warning message.
        if (-not $test_path)
        {
            Write-Host "Saving parameters to test file: ${test_file}";
        }
        else
        {
            Write-Host "Updating test file: ${test_file}";
        }

        Set-Content -Path $test_file -Value $result;
    }
    catch
    {
        # We couldn't update the file, inform the user about it.
        Write-Warning "Couldn't write to test file: ${test_file}";
    }

    return $result;
}

# Do-Release()
# Creates a release archive in the build folder.
function Do-Release($version_string)
{
    Write-Section "Creating release archive";

    $project_file = MustGet-File $(Get-ProjectFileName);
    $release_path = Get-ReleasePath;
    $staging_path = Get-ReleaseStagingPath $release_path;
    $release_file = Get-ReleaseArchiveName $release_path $version_string;

    # Create the staging folder
    Create-Directory $staging_path;
    Try
    {
        # Create PDFs
        Make-Documents $staging_path;

        # Copy the package and archive it
        Copy-Item $project_file $staging_path;
        Archive-Files $archiver_path $staging_path $release_file;

        if ($with_hud)
        {
            Write-Section "Creating release archive for HUD";

            Remove-Item (Join-Path $staging_path "*") -Force -Recurse;
            Make-Documents $staging_path $true;

            $hud_project_name = MustGet-File $(Get-HudProjectFileName);
            $hud_release_file = Get-HudReleaseArchiveName $release_path $version_string;

            Copy-Item $hud_project_name $staging_path;
            Archive-Files $archiver_path $staging_path $hud_release_file;
        }
    }
    Finally
    {
        # Even if an error occurs, clean up the staging folder afterwards
        Remove-Item $staging_path -Force -Recurse;
    }
}

# Get-ReleasePath()
# Returns the path to generate release archives at.
function Get-ReleasePath()
{
    return Join-Path $dir_build "release";
}

# Get-ReleaseArchiveName()
# Returns the name of the release archive to generate.
function Get-ReleaseArchiveName($release_path, $version_string)
{
    return Join-Path $release_path "${project_name}-v${version_string}.zip";
}

# Get-HudReleaseArchiveName()
# Returns the name of the HUD release archive to generate.
function Get-HudReleaseArchiveName($release_path, $version_string)
{
    return Join-Path $release_path "${project_name}_hud-v${version_string}.zip";
}

# Get-ReleaseStagingPath()
# Returns the release staging path.
function Get-ReleaseStagingPath($release_path)
{
    return Join-Path $release_path "staging";
}

# Create-Directory()
# Creates a directory, deleting it first if it already exists.
function Create-Directory($path)
{
    if (Test-Path $path)
    {
        Remove-Item $path -Force -Recurse;
    }

    New-Item -Path $path -ItemType "directory" | Out-Null;
}

# Make-Documents()
# Creates PDFs from Markdown document files.
function Make-Documents($staging_path, $for_hud)
{
    if (-not (Test-Path $pandoc_path))
    {
        Write-Warning "Could not find Pandoc at ${pandoc_path}";
        Write-Warning "PDF generation will be disabled."
    }
    else
    {
        Write-Host "Creating PDFs...";
        Make-Pdf $pandoc_path "CREDITS" $staging_path $for_hud;

        if (-not $for_hud)
        {
            Make-Pdf $pandoc_path "README" $staging_path;
        }
    }
}

# Make-Pdf()
# Creates a PDF file from a Markdown file from the root folder.
function Make-Pdf($pandoc_path, $input_file, $staging_path, $for_hud)
{
    $asset_name = "${input_file}.md"
    $src_file = Join-Path $script_root $asset_name;
    $dest_file = Join-Path $staging_path $asset_name;

    if (-not (Test-Path $src_file))
    {
        Throw "Could not find document file ${src_file}";
    }

    if ($for_hud)
    {
        Process-HudFile $script_root $asset_name $staging_path $false $false;
        (Get-Content $dest_file) -replace "RRWM", "RRWM HUD" | Set-Content $dest_file;
    }
    else
    {
        Copy-Item $src_file $dest_file;
    }

    $pandoc_args = "-V geometry:margin=1in --variable urlcolor=blue";
    $output_file = Join-Path $staging_path "${input_file}.pdf".ToLower();

    # Replace links to external resources within the repository (images etc.)
    # with their absolute paths so that Pandoc can see them.
    (Get-Content $dest_file)                                                             `
    | Select-String -pattern "\[\/\/\]" -notmatch                                        `
    | % { $_ -replace "\(\/(.*)\?raw=true\)", ("(${script_root}\`${1})" -replace "/", "\") } `
    | & $pandoc_path $pandoc_args.Split(" ") "-o" $output_file;

    Remove-Item $dest_file;

    # Check that the operation has succeeded.
    if (-not $?)
    {
        Throw "Error creating PDF: Pandoc returned exit code ${LastExitCode}";
    }

    Write-Host "Created ${output_file}";
}

# Write-Section()
# Writes a nicely formatted string indicating the start of the next build phase.
function Write-Section($what)
{
    Write-Fixed "";
    Write-Fixed $what;
    Write-Fixed "";
}

# Write-Fixed
# Writes a string enclosed in "-" characters, the result is always of fixed length.
function Write-Fixed($what)
{
    # Enclose non-empty string in spaces.
    if ($what -ne "")
    {
        $what = " " + $what + " ";
    }

    $row_length = 80;
    $cur_length = $what.length;

    # Only do this stuff if our string is shorter than the fixed width value,
    # otherwise it wouldn't fit anyway.
    if ($cur_length -lt $row_length)
    {
        function String-Join($char, $length)
        {
            return $("-" * $length);
        }

        $left = [Math]::Truncate(($row_length - $cur_length) / 2);
        $right = $row_length - $cur_length - $left;

        $fill_char = '-';
        $what = (String-Join $fill_char $left) + $what + (String-Join $fill_char $right);
    }

    Write-Host $what;
}

# Start the script here.
exit Main;
