#  Building RRWM

If you want to build a playable RRWM package directly from the contents of this repository, there are two methods available:

1. Automatic build using the build script (recommended)
2. Manual build

## Automatic build using the build script

This method is the easiest one but requires that you have the following software installed on your PC:

* [PowerShell](https://github.com/PowerShell/PowerShell) (if you're on Windows 7 or newer, you should already have it)
* [Git](https://git-scm.com/downloads/) (added to PATH)
* [7-Zip](https://www.7-zip.org/download.html)

**NB:** the build script has only been tested on Windows systems.

Assuming you have 7-Zip installed to `C:\Program Files\7-Zip`, all you have to do is run the build script from the repository root folder:

```
powershell ./build
```

If you have 7-Zip in a different place, use the `-archiver_path` command-line parameter to specify the correct path. Note that if the path contains spaces, it must be enclosed in *single* quotes! Another way to change the path to 7-Zip is to set an environment variable called `RRWM_ARCHIVER_PATH`. If the build script can't find 7-Zip at the default path, it will use the value of the environment variable instead.

If your PowerShell refuses to run the script, you probably have to [change your script execution policy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7).

Successful execution of the build script will result in `rrwm.pk3` appearing in the `build` subfolder. Running the script will also create a `build.log` in the root folder -- you should check it out if the build process is unable to complete successfully for some reason.

### Building the HUD package

Just add `-with_hud` to the command line when running the build script to also build `rrwm_hud.pk3`.


## Manual build

This method obviously doesn't require any external tools, but you will probably waste a lot of time running it repeatedly. Note that you **cannot** create the HUD package this way (unless you're prepared to do a lot of editing).

1. Create a new folder somewhere on your PC (let's call it `staging`).
2. Copy all files from `src` to `staging`.
3. Copy all files from `src_wl` to `staging`.
4. Copy `VersionInfoTemplate.txt` to `staging/classes/rrwm/common` and rename it to `RR_VersionInfo.zs`.
5. *(Optional)* Edit `RRWM_VERSION_STRING` and `RRWM_VERSION_TIMESTAMP` within `RR_VersionInfo.zs` as you see fit.
6. Use your favorite ZIP archiver to archive the entire contents of `staging` to create `rrwm.pk3`.
7. ???????
8. PROFIT
