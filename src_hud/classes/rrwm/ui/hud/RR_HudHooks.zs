//===========================================================================
//
// RR_HudHooks
//
// Replacement RR_HudHooks implementation for the standalone HUD package.
// See the original RR_HudHooks file in the main src folder for details.
//
//===========================================================================
class RR_HudHooks ui
{
    //===========================================================================
    //
    // RR_HudHooks::IsWeaponReloadable
    //
    // Always returns false.
    //
    //===========================================================================
    static bool IsWeaponReloadable(Weapon weap, out int clipAmount, out int clipCapacity)
    {
        return false;
    }

    //===========================================================================
    //
    // RR_HudHooks::IsAmmoInTable
    //
    // Always returns true.
    //
    //===========================================================================
    static bool IsAmmoInTable(class<Ammo> ammoType, bool fromWeapon)
    {
        return true;
    }

    //===========================================================================
    //
    // RR_HudHooks::GetSpecialAmmoColor
    //
    // Returns the name of the color to use for the provided ammo type.
    // If there is no special color found, this method should return 'None'.
    //
    //===========================================================================
    static name GetSpecialAmmoColor(class<Ammo> ammoType)
    {
        return 'None';
    }

    //===========================================================================
    //
    // RR_HudHooks::GetSpecialPowerupProperties
    //
    // Always returns false.
    //
    //===========================================================================
    static bool GetSpecialPowerupProperties(Powerup powerup, out bool isTimeVisible, out double alpha, out name clrTime)
    {
        return false;
    }

    //===========================================================================
    //
    // RR_HudHooks::GetStatusBarAmmoType
    //
    // Returns the ammo type to display in the status bar ammo table
    // for the selected index.
    //
    //===========================================================================
    static name GetStatusBarAmmoType(int index)
    {
        switch(index)
        {
            case 0:
                return 'Clip';
            case 1:
                return 'Shell';
            case 2:
                return 'RocketAmmo';
            case 3:
                return 'Cell';
        }

        ThrowAbortException("Invalid index specified.");
        return 'None';
    }

    //===========================================================================
    //
    // RR_HudHooks::GetShieldItem
    //
    // Always returns null.
    //
    //===========================================================================
    static Inventory GetShieldItem(PlayerInfo player)
    {
        return null;
    }

    //===========================================================================
    //
    // RR_HudHooks::CanShowClipIndicators
    //
    // Always returns false.
    //
    //===========================================================================
    static bool CanShowClipIndicators()
    {
        return false;
    }
}
