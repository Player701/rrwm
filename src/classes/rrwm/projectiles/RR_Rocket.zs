//===========================================================================
//
// RR_Rocket
//
// A cool-looking explosive projectile fired from the Striker launcher.
//
//===========================================================================
class RR_Rocket : RR_Projectile
{
    // How many smoke actors to spawn per tick?
    const MAXSMOKES = 50;

    // The minimum distance between the rocket and the ceiling
    // needed to prevent the explosion sprite from being cut off
    // in most circumstances.
    const MIN_DIST_FROM_CEILING = 24;

    // Distance between the rocket and the exhaust actor
    const EXHAUST_DIST = 4;

    // An instance of RR_RocketExhaust associated with this projectile.
    private RR_RocketExhaust _exhaust;

    // Last processed ticks for both the rocket and the exhaust actor.
    // Used to re-synchronize the exhaust's position after both actors'
    // Tick methods have run.
    private int _lastTickSelf, _lastTickExhaust;

    // Affects the number of sparks spawned when the rocket explodes.
    // The more this value is, the less sparks are spawned.
    private int _sparkDivisor;

    // The setting that controls whether rocket smoke is enabled.
    private transient RR_Setting _smokeSetting;

    Default
    {
        Radius 11;
        Height 8;
        Speed 20;
        Damage 20;
        SeeSound "rrwm/projectiles/rocket/fire";
        DeathSound "rrwm/projectiles/rocket/explode";

        Obituary "$OB_RR_ROCKET";

        Decal "RR_RocketScorch";
        Scale 0.75;
        +FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            SPRO A 1 A_SpawnRocketSmoke;
            Loop;
        Death:
            SPUF A 1        BRIGHT Light("RR_ROCKET_EXPLODE_1") A_RocketExplode;
            SPUF BC 1       BRIGHT Light("RR_ROCKET_EXPLODE_1");
            SPUF DEF 1      BRIGHT Light("RR_ROCKET_EXPLODE_2");
            SPUF GHIJKLMN 1 BRIGHT Light("RR_ROCKET_EXPLODE_3");
            SPUF OPQRSTUV 1 BRIGHT Light("RR_ROCKET_EXPLODE_4");
            SPUF WXYZ 1     BRIGHT Light("RR_ROCKET_EXPLODE_5");
            Stop;
    }

    //===========================================================================
    //
    // RR_Rocket::A_SpawnRocketSmoke
    //
    // Spawns some smoke.
    //
    //===========================================================================
    void A_SpawnRocketSmoke()
    {
        if (GetAge() > 0)
        {
            int numSmokes = GetNumSmokes();

            for (int i = 0; i < numSmokes; i++)
            {
                SpawnSmoke(Vel.Length() / numSmokes * i);
            }
        }
    }

    //===========================================================================
    //
    // RR_Rocket::GetNumSmokes
    //
    // Returns the number of smoke actors to spawn each tick.
    //
    //===========================================================================
    private int GetNumSmokes() const
    {
        int val = RR_Settings.GetCached(RR_SETTING_ROCKETSMOKE, _smokeSetting)
            .GetPlayValue()
            .GetInt();

        switch(val)
        {
            case RR_RSC_FULL:
                return MAXSMOKES;
            case RR_RSC_REDUCED:
                return MAXSMOKES / 2;
            case RR_RSC_OFF:
            default:
                return 0;
        }
    }

    //===========================================================================
    //
    // RR_Rocket::SpawnSmoke
    //
    // Spawns a single smoke actor.
    //
    //===========================================================================
    private void SpawnSmoke(double x)
    {
        let smokeOffset = ShiftOffsetByVel((
            -x,
            FRandom[RR_Rocket__SpawnSmoke](-3, 3),
            0
        ));

        Spawn('RR_RocketSmoke', RR_Math.OffsetToPosition(smokeOffset, self), ALLOW_REPLACE);
    }

    //===========================================================================
    //
    // RR_Rocket::A_RocketExplode
    //
    // Blows up the rocket.
    // Also changes the sprite's translucency and size and spawns some sparks.
    //
    //===========================================================================
    void A_RocketExplode()
    {
        // Destroy the exhaust actor if it's present.
        if (_exhaust != null)
        {
            _exhaust.Destroy();
            _exhaust = null;
        }

        // Change appearance
        A_SetRenderStyle(0.85, STYLE_Add);
        A_SetScale(0.35);

        // Explode
        A_Explode();

        // Spawn sparks
        RR_Spark.SpawnManyAt(
            self, null,
            Random[RR_Rocket__A_RocketExplode](10, 16) / _sparkDivisor,
            1.25, 1.75,
            1.5, 1.75,
            5.0, 8.0
        );

        // Move the explosion sprite a little closer towards the shooter
        // if the rocket has exploded near the ceiling.
        // This is needed so that the explosion sprite won't get cut off.
        double curDist = CeilingZ - Pos.z;

        if (curDist < MIN_DIST_FROM_CEILING && Pitch < 0)
        {
            // Find the distance to step by. It can be potentially infinite,
            // so we have to cap it. Yes, it means it doesn't always work as desired.
            // But we are not going to reposition the sprite any further than this,
            // this is already a _kind of_ a hack, though a very small one.
            double extraDist = Min((MIN_DIST_FROM_CEILING - curDist) / Sin(-Pitch), MIN_DIST_FROM_CEILING);
            SetOrigin(RR_Math.OffsetToPosition((-extraDist, 0, 0), self), false);
        }
    }

    //===========================================================================
    //
    // RR_Rocket::BeginPlay
    //
    // Initializes the spark divisor value to 1.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        _sparkDivisor = 1;
    }

    //===========================================================================
    //
    // RR_Rocket::PostBeginPlay
    //
    // Spawns an RR_RocketExhaust behind this actor.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // Spawn the exhaust actor.
        _exhaust = RR_RocketExhaust(Spawn('RR_RocketExhaust'));

        // If spawned successfully, assign the target to the spawned exhaust
        // so that it doesn't destroy itself.
        if (_exhaust != null)
        {
            _exhaust.Target = self;
            _lastTickSelf = _lastTickExhaust = -1;
        }
    }

    //===========================================================================
    //
    // RR_Rocket::SyncExhaust
    //
    // Synchronizes the position of the exhaust actor with the rocket
    // after both actors' ticks have been processed.
    //
    //===========================================================================
    void SyncExhaust(Actor source)
    {
        if (source == self)
        {
            // This is the rocket
            _lastTickSelf = Level.maptime;
        }
        else if (source == _exhaust)
        {
            // This is the exhaust actor
            _lastTickExhaust = Level.maptime;
        }

        // If both values match, this means both the rocket and the exhaust
        // have ticked. Check that they stay in sync with each other.
        if (_exhaust != null && _lastTickSelf == _lastTickExhaust)
        {
            let exhaustPos = RR_Math.OffsetToPosition((-EXHAUST_DIST, 0, 0), self);

            // Check if we need to adjust the exhaust's position.
            // This happens on the first tick,
            // and also when we pass through portals.
            if (GetAge() == 0 || !RR_Math.AreAlmostEqualV(_exhaust.Pos, exhaustPos))
            {
                _exhaust.SetOrigin(exhaustPos, false);
            }

            _exhaust.Vel = Vel;
        }
    }

    //===========================================================================
    //
    // RR_Rocket::Tick
    //
    // Makes the exhaust move with the rocket by adjusting its velocity.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        // Signify that the rocket's tick has been processed.
        // If the exhaust has also ticked, a synchronization check is due.
        SyncExhaust(self);
    }

    //===========================================================================
    //
    // RR_Rocket::SpecialMissileHit
    //
    // Makes the rocket spawn less sparks when hitting a living thing.
    //
    //===========================================================================
    override int SpecialMissileHit(Actor victim)
    {
        // We reduce the number of sparks because we consider some of them
        // to come from the surface we've hit, and others from the rocket itself.
        // If we hit a monster or other living object, then only the sparks
        // from the rocket itself should appear.
        if (victim != null && victim != Target)
        {
            // Can it be shot at?
            bool shootable = victim.bSolid && victim.bShootable && !victim.bNonShootable;
            // Does it bleed?
            bool bleeds = !victim.bNoBlood;
            // Is it currently invulnerable?
            bool invulnerable = victim.bDormant || victim.bInvulnerable;

            if (shootable && (bleeds || invulnerable))
            {
                _sparkDivisor = 2;
            }
        }

        return Super.SpecialMissileHit(victim);
    }
}
