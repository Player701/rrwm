//===========================================================================
//
// RR_Projectile
//
// Base class for projectiles used in RRWM.
//
//===========================================================================
class RR_Projectile : Actor abstract
{
    Default
    {
        Projectile;
    }

    //===========================================================================
    //
    // RR_Projectile::GetObituary
    //
    // Generates an obituary based on the projectile's tag and obituary verb,
    // also taking into account who shot the projectile.
    //
    //===========================================================================
    final override string GetObituary(Actor victim, Actor inflictor, Name mod, bool playerattack)
    {
        return RR_Obituary.Select(GetRawObituary(), self, Target);
    }

    //===========================================================================
    //
    // RR_Projectile::GetRawObituary
    //
    // Returns the raw obituary for this projectile.
    //
    //===========================================================================
    protected virtual string GetRawObituary() const
    {
        return Obituary;
    }

    //===========================================================================
    //
    // RR_Projectile::Tick
    //
    // Calculates the pitch based on the projectile's velocity.
    //
    //===========================================================================
    override void Tick()
    {
        // Recalculate the pitch if our velocity is not zero.
        if (!RR_Math.IsAlmostZeroV(Vel))
        {
            Pitch = PitchFromVel();
        }

        Super.Tick();
    }

    //===========================================================================
    //
    // RR_Projectile::ShiftOffsetByVel
    //
    // Subtracts the length of the projectile's velocity vector
    // from the X coordinate of the provided value and returns the result.
    // Used to spawn smoke, particles, trails etc. behind the projectile
    // while it is in motion.
    //
    //===========================================================================
    protected vector3 ShiftOffsetByVel(vector3 v)
    {
        return (v.x - Vel.Length(), v.y, v.z);
    }
}
