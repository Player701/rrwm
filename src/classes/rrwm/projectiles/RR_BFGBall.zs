//===========================================================================
//
// RR_BFGBall
//
// A very damaging ball of pure energy fired by the Disruptor Pulse Cannon.
//
//===========================================================================
class RR_BFGBall : RR_Projectile
{
    // How many rays the ball will generate upon impact.
    // This value is the same as A_BFGSpray's default.
    const NUMRAYS = 40;

    // Has the ball exploded?
    private bool _hasExploded;

    Default
    {
        Radius 13;
        Height 8;
        Speed 25;
        Damage 100;
        SeeSound("rrwm/projectiles/bfgball/fire");
        DeathSound("rrwm/projectiles/bfgball/explode");

        Obituary "$OB_RR_BFGBALL";

        Decal "RR_BFGBallFade";
        RenderStyle "Add";
        Alpha 0.75;
        Scale 0.75;
        +FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            PPRJ ABCDEFGHIJKLMNOPQRSTU 1 BRIGHT Light("RR_BFGBALL") A_SpawnBFGTrail;
            Loop;
        Death:
            PXPL ABCD 3  BRIGHT Light("RR_BFGBALL_EXPLODE_1");
            PXPL EF 3    BRIGHT Light("RR_BFGBALL_EXPLODE_2");
            PXPL G 3     BRIGHT Light("RR_BFGBALL_EXPLODE_2") A_BFGExplode;
            PXPL H 3     BRIGHT Light("RR_BFGBALL_EXPLODE_2");
            PXPL IJKLM 3 BRIGHT Light("RR_BFGBALL_EXPLODE_3");
            Stop;
    }

    //===========================================================================
    //
    // RR_BFGBall::A_SpawnBFGTrail
    //
    // Spawns an instance of RR_BFGTrail behind the ball.
    //
    //===========================================================================
    void A_SpawnBFGTrail()
    {
        if (GetAge() == 0)
        {
            return;
        }

        let trailOffset = ShiftOffsetByVel((
            FRandom[RR_BFGBall__A_SpawnBFGTrail](-1, -5),
            FRandom[RR_BFGBall__A_SpawnBFGTrail](-4,  4),
            FRandom[RR_BFGBall__A_SpawnBFGTrail](-4,  4)
        ));

        let trail = Spawn('RR_BFGTrail', RR_Math.OffsetToPosition(trailOffset, self));

        if (trail)
        {
            // Copy the current sprite frame to the trail.
            trail.frame = frame;
        }
    }

    //===========================================================================
    //
    // RR_BFGBall::A_BFGExplode
    //
    // Performs a BFG spray attack using instances of RR_BFGExtra
    // to signify targets that were hit.
    //
    //===========================================================================
    void A_BFGExplode()
    {
        // We have to re-implement A_BFGSpray with default parameters here.
        // The reason for that is because we want to have spray obituary
        // to be decoupled from the PlayerPawn class.
        //
        // Currently, it is hardcoded in PlayerPawn to check for
        // BFGSpray damage type and return the correct obituary.
        //
        // Several other posisble solutions exist, but they are not ideal:
        //
        // 1) We can override GetObituary in RR_Player.
        //    However, this will couple the logic to RR_Player
        //    instead of our projectile, which is not technically correct.
        //
        // 2) We can set BFGF_MISSILEORIGIN flag in A_BFGSpray call,
        //    then override GetObituary in our projectile.
        //    However, this will have an impact on the gameplay,
        //    and our goal is to keep as it as close to vanilla as possible.
        //
        // Therefore, we choose to re-implement the BFG spray logic here.
        // It also uncouples the damage type from the spray class.
        // We consider the spray class to be a special effect
        // that shouldn't contain any gameplay-impacting properties.
        //
        // The implementation is broken into three separate methods
        // to improve readability.

        // Set the flag so that the correct obituary will be generated.
        _hasExploded = true;

        if (Target == null)
        {
            // Do not spawn sprays without target
            return;
        }

        // Spawn all sprays
        for (int i = 0; i < NUMRAYS; i++)
        {
            let ang = Angle - 45 + 90.0 / NUMRAYS * i;
            ShootSpray(ang);
        }
    }

    //===========================================================================
    //
    // RR_BFGBall::ShootSpray
    //
    // Shoots a single spray in the specified direction.
    //
    //===========================================================================
    private void ShootSpray(double ang)
    {
        FTranslatedLineTarget t;
        Target.AimLineAttack(ang, 1024, t, 32);

        if (t.linetarget == null || t.linetarget == Target)
        {
            // Do not hurt self and do not proceed if no target was found.
            return;
        }

        // Let there be an extra offset so that multiple sprays spawned
        // on the same target don't blend into one.
        double maxSpreadXY = RR_Math.GetNonZeroValue(
            t.linetarget.Radius,
            t.linetarget.Default.Radius
        ) / 2;

        double maxSpreadZ = RR_Math.GetNonZeroValue(
            t.linetarget.Height,
            t.linetarget.Default.Height
        ) / 4;

        let extraOffset = (
            FRandom[RR_BFGBall__ShootSpray](-maxSpreadXY, maxSpreadXY),
            FRandom[RR_BFGBall__ShootSpray](-maxSpreadXY, maxSpreadXY),
            FRandom[RR_BFGBall__ShootSpray](-maxSpreadZ,  maxSpreadZ)
        );

        let sprayPos = t.linetarget.Pos + (0, 0, t.linetarget.Height / 2) + extraOffset;
        Spawn('RR_BFGExtra', sprayPos, ALLOW_REPLACE);

        int dmg = GetSprayDamage();
        int newdam = t.linetarget.DamageMobj(self, Target, dmg, 'BFGSpray', DMG_USEANGLE, t.angleFromSource);
        t.TraceBleed(newdam > 0 ? newdam : dmg, self);
    }

    //===========================================================================
    //
    // RR_BFGBall::GetSprayDamage
    //
    // Returns a randomized damage value for a single spray.
    //
    //===========================================================================
    private int GetSprayDamage()
    {
        int dmg = 0;

        for (int i = 0; i < 15; i++)
        {
            dmg += Random[RR_BFGBall__GetSprayDamage](1, 8);
        }

        return dmg;
    }

    //===========================================================================
    //
    // RR_BFGBall::GetRawObituary
    //
    // Returns the correct raw obituary
    // based on whether the BFG ball has exploded.
    //
    //===========================================================================
    override string GetRawObituary() const
    {
        string obit = Super.GetRawObituary();
        return _hasExploded ? obit .. "_SPRAY" : obit;
    }
}
