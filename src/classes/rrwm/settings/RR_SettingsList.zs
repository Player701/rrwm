//===========================================================================
//
// RR_SettingsList
//
// Contains the code to initialize the settings list.
// This is only done once, when the engine is starting up.
//
//===========================================================================
class RR_SettingsList
{
    //===========================================================================
    //
    // RR_SettingsList::InitSettings
    //
    // Initializes the list of RRWM settings.
    //
    //===========================================================================
    static void InitSettings(RR_Settings settings)
    {
        //===========================================================================
        // User settings
        //===========================================================================
//<ExcludeFromHudPackage>

        // Controls whether automatic reloading is enabled.
        // If true, weapons will reload automatically once the clip is empty,
        // with no need to press the reload button or the fire buttom manually.
        settings.NewOption(RR_SETTING_RELOADMODE)
            .HasOptionValues('ReloadModeOptions')
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_LOW)
            .Add();

        // Controls the amount of available ammo (in % of the clip capacity)
        // required to trigger full auto reloading.
        settings.NewNumber(RR_SETTING_AUTORELOAD_THRESHOLD)
            .HasRange(0, 100)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_LOW)
            .DependsOnOptionValue(RR_SETTING_RELOADMODE, RR_RM_FULLAUTO)
            .Add();
//</ExcludeFromHudPackage>

        // Controls speed of HUD animations.
        settings.NewNumber(RR_SETTING_HUD_ANIMSPEED)
            .HasFractionalRange(0.5, 2)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Toggles maximum values for vital stats.
        settings.NewSwitch(RR_SETTING_HUD_MAXVITALS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Controls the amount of health below which the health indicator
        // starts fading in and out to signal a potential danger.
        settings.NewNumber(RR_SETTING_HUD_LOWHEALTH_THRESHOLD)
            .HasRange(0, 50)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Toggles kills stat in the HUD.
        // Can be set up to show the stat only if total monster count is non-zero.
        settings.NewNonzeroOnOff(RR_SETTING_HUD_STAT_KILLS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .Add();

        // Toggles items stat in the HUD.
        // Can be set up to show the stat only if total item count is non-zero.
        settings.NewNonzeroOnOff(RR_SETTING_HUD_STAT_ITEMS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .Add();

        // Toggles secrets stat in the HUD.
        // Can be set up to show the stat only if total secret count is non-zero.
        settings.NewNonzeroOnOff(RR_SETTING_HUD_STAT_SECRETS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .Add();

        // Toggles individual statistics counters in the HUD.
        settings.NewSwitch(RR_SETTING_HUD_INDSTATS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_KILLS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_ITEMS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_SECRETS, RR_OPTVAL_OFF, true)
            .Add();

        // Toggles icons for HUD stat counters.
        settings.NewSwitch(RR_SETTING_HUD_STATICONS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_KILLS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_ITEMS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_SECRETS, RR_OPTVAL_OFF, true)
            .Add();

        // Controls the display style of the HUD stat counters.
        settings.NewOption(RR_SETTING_HUD_STATSTYLE)
            .HasOptionValues('StatStyleOptions')
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_NOTDEATHMATCH)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_KILLS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_ITEMS, RR_OPTVAL_OFF, true)
            .DependsOnOptionValue(RR_SETTING_HUD_STAT_SECRETS, RR_OPTVAL_OFF, true)
            .Add();

        // Toggles HUD mugshot.
        settings.NewSwitch(RR_SETTING_HUD_MUGSHOT)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .HasVisibilityFlags(RR_SVF_DOOMONLY)
            .Add();

        // Controls whether small icons for keys are used.
        settings.NewSwitch(RR_SETTING_HUD_SMALLKEYICONS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Controls whether items tags in the inventory bar
        // are enabled.
        settings.NewSwitch(RR_SETTING_HUD_ITEMTAGS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Controls the alignment of the inventory bar.
        settings.NewOption(RR_SETTING_HUD_INVBARALIGN)
            .HasOptionValues('InvbarAlignOptions')
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();
//<ExcludeFromHudPackage>

        // Controls whether low ammo indicator is enabled.
        // The indicator is drawn below the crosshair
        // when the amount of ammo in the weapon's clip
        // is less than the specified threshold.
        settings.NewSwitch(RR_SETTING_HUD_LOWAMMO)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Controls the amount of available ammo (in % of the clip capacity)
        // below which the low ammo indicator is displayed.
        settings.NewNumber(RR_SETTING_HUD_LOWAMMO_THRESHOLD)
            .HasRange(0, 50)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .DependsOnSwitch(RR_SETTING_HUD_LOWAMMO)
            .Add();
//</ExcludeFromHudPackage>

        // Controls whether to display a health bar
        // for the enemy in the crosshairs.
        settings.NewOption(RR_SETTING_HUD_ENEMYHEALTH)
            .HasOptionValues('EnemyHealthOptions')
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Sets the maximum distance between the targeted enemy
        // and the player for the purposes of displaying the health bar.
        settings.NewNumber(RR_SETTING_HUD_ENEMYHEALTH_MAXDIST)
            .HasRange(0, 10000)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .DependsOnOptionValue(RR_SETTING_HUD_ENEMYHEALTH, RR_EH_OFF, true)
            .Add();

        // Toggles the weapon slot indicator in the HUD.
        settings.NewSwitch(RR_SETTING_HUD_WEAPONSLOTS)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();
//<ExcludeFromHudPackage>

        // Toggles the display of clip indicators underneath the weapon slots.
        settings.NewSwitch(RR_SETTING_HUD_WEAPONSLOTS_CLIP)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .DependsOnSwitch(RR_SETTING_HUD_WEAPONSLOTS)
            .Add();
//</ExcludeFromHudPackage>

        // Toggles ammo table in the HUD.
        // Shows ammo amounts / max amounts for various ammo types in small font.
        // Which ammo types are shown is customizable (see below).
        settings.NewSwitch(RR_SETTING_HUD_AMMOTABLE)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Controls whether the ammo table shows ammo types for all available weapons,
        // or only for those weapons that are currently in player's possession.
        settings.NewNonzeroOnOff(RR_SETTING_HUD_AMMOTABLE_SHOWALL)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .DependsOnSwitch(RR_SETTING_HUD_AMMOTABLE)
            .Add();

        // Controls whether ammo types for the currently selected weapon are shown
        // in the ammo table. This overrides the previous setting.
        settings.NewSwitch(RR_SETTING_HUD_AMMOTABLE_SHOWCUR)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .DependsOnSwitch(RR_SETTING_HUD_AMMOTABLE)
            .Add();

        // Controls whether to show the powerup display.
        // Can be set up to display the icon only in multiplayer.
        settings.NewOption(RR_SETTING_HUD_POWERUPS)
            .HasOptionValues('PowerupDisplayOptions')
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Toggles the oxygen counter while underwater.
        settings.NewSwitch(RR_SETTING_HUD_AIRSUPPLY)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Toggles player-colored icon in the HUD.
        // Can be set up to display the icon only in multiplayer.
        settings.NewMultiplayerAware(RR_SETTING_HUD_PLAYERCOLOR)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();

        // Toggles HUD debugging. Probably not very useful in normal play.
        settings.NewNumber(RR_SETTING_HUD_DEBUG)
            .HasRange(0, 10)
            .HasScope(RR_SS_USER)
            .HasCategory(RR_SC_HUD)
            .Add();
//<ExcludeFromHudPackage>

        //===========================================================================
        // Server settings
        //===========================================================================

        // Toggles partial ammo pickup.
        // If enabled, ammo pickups will not disappear when the player
        // receives more ammo than they can carry.
        settings.NewSwitch(RR_SETTING_PARTIALPICKUP)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Toggles whether all remaining ammo is dropped on death in addition to the weapon.
        settings.NewSwitch(RR_SETTING_WEAPONDROP_AMMO)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Controls sniping.
        // By default, pistol and chaingun's first shots will be 100% precise like in vanilla.
        // This behavior can be disabled entirely, or restricted to the pistol's laser sight.
        settings.NewOption(RR_SETTING_SNIPING)
            .HasOptionValues('SnipingOptions')
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Togggles recoil for all non-melee weapons.
        settings.NewSwitch(RR_SETTING_RECOIL)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_HIGH)
            .Add();

        // Forces vertical spread for hitscan weapons for all players,
        // disregarding the built-in options.
        settings.NewSwitch(RR_SETTING_FORCEVERTSPREAD)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Toggles vertical spread for monsters.
        settings.NewSwitch(RR_SETTING_MONSTERVERTSPREAD)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Toggles the debris queue.
        // If on, the total amount of debris objects on the level is limited
        // by the value of the next setting. Otherwise, it's unlimited.
        settings.NewMultiplayerAware(RR_SETTING_DEBRISQUEUE_ENABLED)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        // Controls the maximum amount of debris object on the level.
        // Only has effect if the debris queue is enabled.
        settings.NewNumber(RR_SETTING_DEBRISQUEUE_LIMIT)
            .HasRange(0, RR_DebrisManager.LIMIT_MAX)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .DependsOnMultiplayerAware(RR_SETTING_DEBRISQUEUE_ENABLED)
            .Add();

        // Toggles casing smoke.
        settings.NewSwitch(RR_SETTING_CASINGSMOKE)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        // Toggles chainsaw smoke.
        settings.NewSwitch(RR_SETTING_CHAINSAWSMOKE)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        // Toggles rocket smoke. Applies only to Striker missiles.
        settings.NewOption(RR_SETTING_ROCKETSMOKE)
            .HasOptionValues('RocketSmokeOptions')
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        // Toggles whether spider demons will eject casings when they attack.
        // Depending on the level, can have an impact on performance if turned on
        // and the debris queue capacity is high or unlimited.
        settings.NewSwitch(RR_SETTING_SPIDERDEMON_CASINGS)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        // Replaces cyberdemon's rockets with Striker missiles if turned on.
        settings.NewSwitch(RR_SETTING_CYBERDEMON_NEWROCKETS)
            .HasScope(RR_SS_SERVER)
            .HasCategory(RR_SC_COSMETIC)
            .HasGameplayImpact(RR_SGI_NONE)
            .Add();

        //===========================================================================
        // Persistent server settings (can only be changed for a new game)
        //===========================================================================

        // Replaces the Striker missile launcher with the Railgun if turned on.
        // NB! Can make some levels unwinnable. Refer to full description for more info.
        settings.NewSwitch(RR_SETTING_RAILGUN)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_HIGH)
            .Add();

        // Adds two more armor types to the game.
        // They will randomly replace normal armors on the level.
        settings.NewSwitch(RR_SETTING_EXTRAARMOR)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Enables an extra armor slot for the Super Armor.
        // In this mode, the Super Armor can worn over any other armor
        // without overriding them.
        settings.NewSwitch(RR_SETTING_SUPERARMORSLOT)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .DependsOnSwitch(RR_SETTING_EXTRAARMOR)
            .Add();

        // Makes the berserk effect time-limited.
        // Might have a severe impact on gameplay in some cases.
        settings.NewSwitch(RR_SETTING_LIMITBERSERK)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_GAMEPLAY)
            .HasGameplayImpact(RR_SGI_HIGH)
            .Add();

        // If enabled, monsters will reload their weapons.
        // The strategy they use to decide whether to reload or not is somewhat non-trivial.
        settings.NewSwitch(RR_SETTING_MONSTERRELOAD)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Toggles realistic reloading sequence for the shotgun.
        // If this is enabled, the shotgun is pumped once after loading the first shell,
        // but only if it was empty before reload.
        settings.NewSwitch(RR_SETTING_SHOTGUN_REALISTICRELOAD)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_WEAPONS)
            .HasGameplayImpact(RR_SGI_MEDIUM)
            .Add();

        // Toggles between old and new chaingun guy sprites.
        // New sprites fit better with the chaingun replacement RRWM provides,
        // otherwise there is no difference.
        settings.NewSwitch(RR_SETTING_NEWCHAINGUNGUY)
            .HasScope(RR_SS_SERVER_PERSISTENT)
            .HasCategory(RR_SC_COSMETIC)
            .Add();
//</ExcludeFromHudPackage>
    }
}
