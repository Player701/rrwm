//===========================================================================
//
// RR_Settings
//
// Manages the RRWM settings and provides methods to retrieve their values
// and metadata,
//
//===========================================================================
class RR_Settings
{
    // Stores all settings in a list.
    private Array<RR_Setting> _settings;

    // Stores all settings in a map for quick access by CVar name.
    private Map<name, RR_Setting> _settingsMap;

//<ExcludeFromHudPackage>

    // Globally registered persistent settings are stored here.
    private RR_PersistentSettings _globalPersistentSettings;
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Settings::Create
    //
    // Creates a new instance of this class and initializes all settings.
    //
    //===========================================================================
    static RR_Settings Create()
    {
        let settings = new('RR_Settings');
        settings.Init();

        return settings;
    }

    //===========================================================================
    //
    // RR_Settings::GetInstance
    //
    // Returns the global instance of this class from the static event handler.
    //
    //===========================================================================
    static RR_Settings GetInstance()
    {
        return RR_GlobalEventHandler.GetInstance().GetSettings();
    }

    //===========================================================================
    //
    // RR_Settings::Get
    //
    // Returns a setting by its corresponding CVar name.
    //
    //===========================================================================
    static RR_Setting Get(name cvarName)
    {
        return GetInstance().GetSetting(cvarName);
    }

    //===========================================================================
    //
    // RR_Settings::GetCached
    //
    // Returns a setting by its corresponding CVar name,
    // using and/or updating the provided cached value.
    //
    //===========================================================================
    static RR_Setting GetCached(name cvarName, in out RR_Setting cachedSetting)
    {
        if (cachedSetting == null || cachedSetting.GetCvarName() != cvarName)
        {
            cachedSetting = Get(cvarName);
        }

        return cachedSetting;
    }

    //===========================================================================
    //
    // RR_Settings::InPlay
    //
    // Returns the value of the provided setting in play scope.
    // For user settings, a player number must be specified.
    //
    //===========================================================================
    static play RR_SettingValue InPlay(name cvarName, int playerNumber = -1)
    {
        return Get(cvarName).GetPlayValue(playerNumber);
    }

    //===========================================================================
    //
    // RR_Settings::InUi
    //
    // Returns the value of the provided setting for UI purposes only.
    //
    //===========================================================================
    static ui RR_SettingValue InUi(name cvarName)
    {
        return Get(cvarName).GetUiValue();
    }

    //===========================================================================
    //
    // RR_Settings::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
//<ExcludeFromHudPackage>
        _globalPersistentSettings = RR_PersistentSettings.Create();
//</ExcludeFromHudPackage>
        RR_SettingsList.InitSettings(self);
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Settings::GetGlobalPersistentSettings
    //
    // Returns the global instance of the persistent settings container.
    // This operation can be attempted at any time.
    //
    //===========================================================================
    RR_PersistentSettings GetGlobalPersistentSettings() const
    {
        return _globalPersistentSettings;
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Settings::GetSetting
    //
    // Returns a setting by its CVar name.
    //
    //===========================================================================
    RR_Setting GetSetting(name cvarName) const
    {
        let [result, hasValue] = _settingsMap.CheckValue(cvarName);

        if (!hasValue)
        {
            ThrowAbortException("%s: Could not find a setting corresponding to CVar \"%s.\"", GetClassName(), cvarName);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_Settings::NextSetting
    //
    // Gets the total number of settings registered.
    //
    //===========================================================================
    int Count() const
    {
        return _settings.Size();
    }

    //===========================================================================
    //
    // RR_Settings::GetByIndex
    //
    // Returns a setting by its index in the settings array.
    //
    //===========================================================================
    RR_Setting GetByIndex(int index) const
    {
        if (index < 0 || index >= Count())
        {
            ThrowAbortException("%s: Invalid setting index %d.", index);
        }

        return _settings[index];
    }

    //===========================================================================
    //
    // RR_Settings::ClearCache
    //
    // Clears any cached values to prevent junk data from persisting.
    //
    //===========================================================================
    void ClearCache()
    {
        foreach (setting : _settings)
        {
            if (setting != null)
            {
                setting.ClearCache();
            }
        }
    }

    //===========================================================================
    //
    // RR_Settings::CreateValueProvider
    //
    // Creates a value provider for a setting with the CVar name and scope.
    //
    //===========================================================================
    RR_ValueProvider CreateValueProvider(name cvarName, E_RR_SettingScope scope) const
    {
        switch (scope)
        {
            case RR_SS_USER:
                return RR_ValueProvider(RR_ValueProviderUser.Create(cvarName));
//<ExcludeFromHudPackage>
            case RR_SS_SERVER:
                return RR_ValueProvider(RR_ValueProviderServer.Create(cvarName));
            case RR_SS_SERVER_PERSISTENT:
                return RR_ValueProvider(RR_ValueProviderServerPersistent.Create(cvarName));
//</ExcludeFromHudPackage>
        }

        ThrowAbortException("%s: Invalid setting scope %d.", GetClassName(), scope);
        return null;
    }

    //===========================================================================
    //
    // RR_Settings::Add
    //
    // Adds a new setting.
    //
    //===========================================================================
    void Add(RR_Setting setting)
    {
        let cvarName = setting.GetCVarName();

        if (_settingsMap.CheckKey(cvarName))
        {
            ThrowAbortException("%s: A setting for CVar \"%s\" already exists.", GetClassName(), cvarName);
        }

        // Add the setting both to the array and to the map
        _settings.Push(setting);
        _settingsMap.Insert(cvarName, setting);
    }

    //===========================================================================
    //
    // RR_Settings::NewSwitch
    //
    // Creates a builder to add a new switch with the fluent API.
    //
    //===========================================================================
    RR_SwitchBuilder NewSwitch(name cvarName)
    {
        return RR_SwitchBuilder.Create(self, cvarName);
    }

    //===========================================================================
    //
    // RR_Settings::NewMultiplayerAware
    //
    // Creates a builder to add a new multiplayer-aware switch
    // with the fluent API.
    //
    //===========================================================================
    RR_OptionBuilder NewMultiplayerAware(name cvarName)
    {
        return RR_OptionBuilder.Create(self, cvarName)
            .HasOptionValues('MultiplayerOnOff');
    }

    //===========================================================================
    //
    // RR_Settings::NewNonzeroOnOff
    //
    // Creates a builder to add a new option with the NonzeroOnOff values
    // with the fluent API.
    //
    //===========================================================================
    RR_OptionBuilder NewNonzeroOnOff(name cvarName)
    {
        return RR_OptionBuilder.Create(self, cvarName)
            .HasOptionValues('NonzeroOnOff');
    }

    //===========================================================================
    //
    // RR_Settings::NewOption
    //
    // Creates a builder to add a new option with the fluent API.
    //
    //===========================================================================
    RR_OptionBuilder NewOption(name cvarName)
    {
        return RR_OptionBuilder.Create(self, cvarName);
    }

    //===========================================================================
    //
    // RR_Settings::NewNumber
    //
    // Creates a builder to add a new number setting with the fluent API.
    //
    //===========================================================================
    RR_NumberBuilder NewNumber(name cvarName)
    {
        return RR_NumberBuilder.Create(self, cvarName);
    }
}
