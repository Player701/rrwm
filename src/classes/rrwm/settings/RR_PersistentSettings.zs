//===========================================================================
//
// RR_PersistentSettings
//
// Implements a settings container
// which remember values during the registration phase
// and returns the stored values when requested.
//
//===========================================================================
class RR_PersistentSettings
{
    // Used to look up stored values by CVar name.
    private Map<name, RR_SettingValue> _valuesMap;

    //===========================================================================
    //
    // RR_PersistentSettings::Create
    //
    // Returns a new instance of this class.
    //
    //===========================================================================
    static RR_PersistentSettings Create()
    {
        return new('RR_PersistentSettings');
    }

    //===========================================================================
    //
    // RR_PersistentSettings::GetGlobalInstance
    //
    // Returns the global instance of the persistent settings container.
    //
    //===========================================================================
    static RR_PersistentSettings GetGlobalInstance()
    {
        return RR_Settings.GetInstance().GetGlobalPersistentSettings();
    }

    //===========================================================================
    //
    // RR_PersistentSettings::GetLocalInstance
    //
    // Returns the local instance of the persistent settings container.
    //
    //===========================================================================
    static RR_PersistentSettings GetLocalInstance()
    {
        return RR_LocalEventHandler.GetInstance().GetPersistentSettings();
    }

    //===========================================================================
    //
    // RR_PersistentSettings::AreValuesLocked
    //
    // Checks if the values for persistent settings are currently locked.
    // Values are locked during the game, and unlocked outside of it.
    //
    //===========================================================================
    static bool AreValuesLocked()
    {
        return gamestate == GS_LEVEL || gamestate == GS_INTERMISSION || gamestate == GS_FINALE || gamestate == GS_CUTSCENE;
    }

    //===========================================================================
    //
    // RR_PersistentSettings::GetValue
    //
    // Returns the synchronized value of a CVar by its name.
    //
    //===========================================================================
    RR_SettingValue GetValue(name cvarName) const
    {
        let [result, hasValue] = _valuesMap.CheckValue(cvarName);

        if (!hasValue)
        {
            ThrowAbortException("%s: Could not find a persistent setting value corresponding to CVar \"%s.\"", GetClassName(), cvarName);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_PersistentSettings::SyncWithActualValues
    //
    // Synchronizes the values of all persistent settings with their CVAR values.
    // This is done before a new game starts.
    //
    //===========================================================================
    void SyncWithActualValues()
    {
        Clear();

        let settings = RR_Settings.GetInstance();

        for (int i = 0; i < settings.Count(); i++)
        {
            let setting = settings.GetByIndex(i);

            if (setting.GetScope() == RR_SS_SERVER_PERSISTENT)
            {
                let cvarName = setting.GetCvarName();
                let cvar = RR_CvarHelper.MustFindCvar(cvarName);

                _valuesMap.Insert(cvarName, RR_SettingValue.FromCvarStatic(cvar));
            }
        }
    }

    //===========================================================================
    //
    // RR_PersistentSettings::SyncWith
    //
    // Synchronizes the values of all persistent settings with those stored
    // in another container.
    // This is done when a new level starts (local with global)
    // and when a level ends (global with local).
    //
    //===========================================================================
    void SyncWith(RR_PersistentSettings container)
    {
        Clear();

        let settings = RR_Settings.GetInstance();

        for (int i = 0; i < settings.Count(); i++)
        {
            let setting = settings.GetByIndex(i);

            if (setting.GetScope() == RR_SS_SERVER_PERSISTENT)
            {
                let cvarName = setting.GetCvarName();
                _valuesMap.Insert(cvarName, container.GetValue(cvarName));
            }
        }
    }

    //===========================================================================
    //
    // RR_PersistentSettings::Clear
    //
    // Clears all registered values.
    //
    //===========================================================================
    private void Clear()
    {
        _valuesMap.Clear();
    }
}
