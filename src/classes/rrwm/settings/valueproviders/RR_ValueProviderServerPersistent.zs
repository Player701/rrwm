//===========================================================================
//
// RR_ValueProviderServerPersistent
//
// Implements the value provider for persistent settings.
//
//===========================================================================
class RR_ValueProviderServerPersistent : RR_ValueProviderServer
{
    // The cached instance of the local persistent settings container.
    private RR_PersistentSettings _localPersistentSettings;

    //===========================================================================
    //
    // RR_ValueProviderServerPersistent::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_ValueProviderServerPersistent Create(name cvarName)
    {
        let valueProvider = new('RR_ValueProviderServerPersistent');
        valueProvider.Init(cvarName, RR_SS_SERVER_PERSISTENT);

        return valueProvider;
    }

    //===========================================================================
    //
    // RR_ValueProviderServerPersistent::GetPlayValue
    //
    // Returns the value of the setting in the play scope.
    //
    //===========================================================================
    override RR_SettingValue GetPlayValue(int playerNumber)
    {
        // This will always work in play scope,
        // no need to fall back to parent method.
        return GetValueAndCache();
    }

    //===========================================================================
    //
    // RR_ValueProviderServerPersistent::GetUiValue
    //
    // Returns the value of the setting in the UI scope.
    //
    //===========================================================================
    override RR_SettingValue GetUiValue()
    {
        return RR_PersistentSettings.AreValuesLocked()
            ? GetValueAndCache()
            : Super.GetUiValue();
    }

    //===========================================================================
    //
    // RR_ValueProviderServerPersistent::GetValueAndCache
    //
    // Caches the local persistent container instance and returns the value
    // obtained from it.
    //
    //===========================================================================
    private RR_SettingValue GetValueAndCache()
    {
        if (_localPersistentSettings == null)
        {
            // Get the local persistent settings container.
            _localPersistentSettings = RR_PersistentSettings.GetLocalInstance();
        }

        return _localPersistentSettings.GetValue(GetCvarName());
    }
}
