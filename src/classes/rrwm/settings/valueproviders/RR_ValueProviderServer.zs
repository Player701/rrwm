//===========================================================================
//
// RR_ValueProviderServer
//
// Implements the value provider for server settings.
//
//===========================================================================
class RR_ValueProviderServer : RR_ValueProvider
{
    // Stores the cached dynamic value.
    private RR_SettingValue _cachedValue;

    //===========================================================================
    //
    // RR_ValueProviderServer::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_ValueProviderServer Create(name cvarName)
    {
        let valueProvider = new('RR_ValueProviderServer');
        valueProvider.Init(cvarName, RR_SS_SERVER);

        return valueProvider;
    }

    //===========================================================================
    //
    // RR_ValueProviderServer::GetPlayValue
    //
    // Returns the value of the CVAR in play scope.
    //
    //===========================================================================
    override RR_SettingValue GetPlayValue(int playerNumber)
    {
        return GetValueAndCache();
    }

    //===========================================================================
    //
    // RR_ValueProviderServer::GetUiValue
    //
    // Returns the value of the CVAR in UI scope.
    //
    //===========================================================================
    override RR_SettingValue GetUiValue()
    {
        return GetValueAndCache();
    }

    //===========================================================================
    //
    // RR_ValueProviderServer::GetValueAndCache
    //
    // Caches and returns the CVAR value.
    //
    //===========================================================================
    private RR_SettingValue GetValueAndCache()
    {
        if (_cachedValue == null)
        {
            let cvar = RR_CvarHelper.MustFindCvar(GetCvarName());
            _cachedValue = RR_SettingValue.FromCVarDynamic(cvar);
        }

        return _cachedValue;
    }
}
