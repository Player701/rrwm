//===========================================================================
//
// RR_ValueProvider
//
// Base class for value providers of RRWM settings.
//
//===========================================================================
class RR_ValueProvider abstract
{
    // Stores the CVAR name this value provider is associated with.
    private name _cvarName;

    // Stores the scope this value provider implements.
    private E_RR_SettingScope _scope;

    //===========================================================================
    //
    // RR_ValueProvider::Init
    //
    // Performs first-time initialization. This method is called from subclasses.
    //
    //===========================================================================
    protected void Init(name cvarName, E_RR_SettingScope scope)
    {
        _cvarName = cvarName;
        _scope = scope;
    }

    //===========================================================================
    //
    // RR_ValueProvider::GetCvarName
    //
    // Returns the name of the CVAR this value provider is associated with.
    //
    //===========================================================================
    name GetCvarName() const
    {
        return _cvarName;
    }

    //===========================================================================
    //
    // RR_ValueProvider::GetScope
    //
    // Returns the scope this value provider implements.
    //
    //===========================================================================
    E_RR_SettingScope GetScope() const
    {
        return _scope;
    }

    //===========================================================================
    //
    // RR_ValueProvider::GetPlayValue
    //
    // Returns a setting value to use in play scope.
    // Allows specifying a player number to indicate the player
    // to get the value for.
    //
    //===========================================================================
    abstract play RR_SettingValue GetPlayValue(int playerNumber);

    //===========================================================================
    //
    // RR_ValueProvider::GetUiValue
    //
    // Returns a setting value to use in UI scope.
    // If values can be different for each player, the returned value
    // is always associated with the console player.
    //
    //===========================================================================
    abstract ui RR_SettingValue GetUiValue();

    //===========================================================================
    //
    // RR_ValueProvider::ClearCache
    //
    // Clears any cached values to prevent junk data from persisting.
    // This is called in the global WorldLoaded event.
    //
    //===========================================================================
    virtual void ClearCache()
    {
        // Does nothing by default
    }
}
