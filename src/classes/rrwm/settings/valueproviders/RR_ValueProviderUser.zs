//===========================================================================
//
// RR_ValueProviderUser
//
// Implements the value provider for user settings.
//
//===========================================================================
class RR_ValueProviderUser : RR_ValueProvider
{
    // Cached dynamic values for each player.
    private RR_SettingValue _cachedPlayerValues[MAXPLAYERS];

    // Cached dynamic value for use in UI scope.
    private RR_SettingValue _cachedUiValue;

    //===========================================================================
    //
    // RR_ValueProviderUser::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_ValueProviderUser Create(name cvarName)
    {
        let valueProvider = new('RR_ValueProviderUser');
        valueProvider.Init(cvarName, RR_SS_USER);

        return valueProvider;
    }

    //===========================================================================
    //
    // RR_ValueProviderUser::GetPlayValue
    //
    // Returns a setting value to use in play scope
    // for the player having the provided number.
    //
    //===========================================================================
    override RR_SettingValue GetPlayValue(int playerNumber)
    {
        // Check that the player number is correct.
        if (playerNumber < 0 || playerNumber >= MAXPLAYERS)
        {
            ThrowAbortException("%s: Must specify a valid player number for user settings.", GetClassName());
        }

        return GetPlayValueAndCache(playerNumber);
    }

    //===========================================================================
    //
    // RR_ValueProviderUser::GetUiValue
    //
    // Returns a setting value to use in UI scope.
    // If values can be different for each player, the returned value
    // is always associated with the console player.
    //
    //===========================================================================
    override RR_SettingValue GetUiValue()
    {
        if (_cachedUiValue == null)
        {
            let cvar = RR_CvarHelper.MustFindCvar(GetCvarName());
            _cachedUiValue = RR_SettingValue.FromCvarDynamic(cvar);
        }

        return _cachedUiValue;
    }

    //===========================================================================
    //
    // RR_ValueProviderUser::ClearCache
    //
    // Clears any cached values to prevent junk data from persisting.
    //
    //===========================================================================
    override void ClearCache()
    {
        // It appears that user CVARs are recreated each time when a new game starts
        // or when a save is loaded. Therefore, we need to clear all cached CVARs here,
        // otherwise they will keep returning incorrect values.
        // It's kind of weird they aren't destroyed and nulled automatically by the GC.
        // Server CVARs do not exhibit this issue and do not need to be re-cached
        // when loading a save or starting a new game.
        for (int i = 0; i < MAXPLAYERS; i++)
        {
            _cachedPlayerValues[i] = null;
        }

        // No need to clear the UI CVAR cache, it appears to be static
        // and doesn't change between games / levels.
    }

    //===========================================================================
    //
    // RR_ValueProviderUser::GetPlayValueAndCache
    //
    // Caches the user CVAR instance for the player with the provided
    // number and returns the value of the cached CVAR.
    //
    //===========================================================================
    private RR_SettingValue GetPlayValueAndCache(int playerNumber)
    {
        let result = _cachedPlayerValues[playerNumber];

        if (result == null)
        {
            let cvarName = GetCvarName();
            PlayerInfo player = null;

            // Check that the player is in game.
            if (playeringame[playerNumber])
            {
                player = players[playerNumber];
            }

            // Make sure the player exists.
            if (player == null)
            {
                ThrowAbortException("%s: Attempt to get CVAR \"%s\" for non-existent player %d.", GetClassName(), cvarName, playerNumber);
            }

            // Get the CVAR and cache the result.
            let cvar = RR_CvarHelper.MustGetCvar(cvarName, player);
            result = _cachedPlayerValues[playerNumber] = RR_SettingValue.FromCvarDynamic(cvar);
        }

        // Return the value of the cached CVAR.
        return result;
    }
}
