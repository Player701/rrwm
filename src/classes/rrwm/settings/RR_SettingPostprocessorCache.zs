//===========================================================================
//
// RR_SettingPostprocessorCache
//
// Used to cache the result of RR_Setting::PostprocessValue
// to avoid extra allocations.
//
//===========================================================================
class RR_SettingPostprocessorCache
{
    private RR_SettingValue _lastInputValue, _lastOutputValue;

    //===========================================================================
    //
    // RR_SettingPostprocessorCache::Create
    //
    // Creates a new instance of the RR_SettingPostprocessorCache class.
    //
    //===========================================================================
    static RR_SettingPostprocessorCache Create()
    {
        return new('RR_SettingPostprocessorCache');
    }

    //===========================================================================
    //
    // RR_SettingPostprocessorCache::GetPostprocessedValue
    //
    // If the last input value is the same as the provided value,
    // returns the last postprocessed value.
    // Otherwise, calls PostprocessValue on the provided RR_Setting instance,
    // caches and returns the result.
    //
    //===========================================================================
    RR_SettingValue GetPostprocessedValue(readonly<RR_Setting> setting, RR_SettingValue inputValue)
    {
        if (setting == null)
        {
            ThrowAbortException("%s: Setting cannot be null.", GetClassName());
        }

        if (inputValue == null)
        {
            ThrowAbortException("%s: Input value cannot be null.", GetClassName());
        }

        if (_lastOutputValue == null || _lastInputValue == null || _lastInputValue.CompareTo(inputValue) != 0)
        {
            _lastInputValue = inputValue.Freeze();
            _lastOutputValue = setting.PostprocessValue(inputValue);
        }

        return _lastOutputValue;
    }

    //===========================================================================
    //
    // RR_SettingPostprocessorCache::Clear
    //
    // Clear the cached input and output values.
    //
    //===========================================================================
    void Clear()
    {
        _lastInputValue = _lastOutputValue = null;
    }
}
