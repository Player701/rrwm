//===========================================================================
//
// RR_MultiDependency
//
// Implements a setting dependency on multiple sub-dependencies.
// The setting is enabled if at least one of the sub-dependencies passes.
//
//===========================================================================
class RR_MultiDependency : RR_SettingDependency
{
    private Array<RR_SettingDependency> _dependencies;

    //===========================================================================
    //
    // RR_MultiDependency::Create
    //
    // Creates a new instance of the RR_MultiDependency class.
    //
    //===========================================================================
    static RR_MultiDependency Create()
    {
        return new('RR_MultiDependency');
    }

    //===========================================================================
    //
    // RR_MultiDependency::Add
    //
    // Adds a new sub-dependency to this instance of RR_MultiDependency.
    //
    //===========================================================================
    void Add(RR_SettingDependency dependency)
    {
        _dependencies.Push(dependency);
    }

    //===========================================================================
    //
    // RR_MultiDependency::Validate
    //
    // Returns true if at least one sub-dependency passes the check.
    //
    //===========================================================================
    override bool Check(readonly<RR_Setting> thisSetting)
    {
        int n = _dependencies.Size();

        if (n == 0)
        {
            ThrowAbortException("%s: At least one dependency must be specified.", GetClassName());
        }

        foreach (dep : _dependencies)
        {
            if (dep.Check(thisSetting))
            {
                return true;
            }
        }

        return false;
    }
}
