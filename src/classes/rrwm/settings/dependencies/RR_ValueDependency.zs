//===========================================================================
//
// RR_ValueDependency
//
// Implementes a setting dependency on a specific value of another setting.
//
//===========================================================================
class RR_ValueDependency : RR_SingleDependency
{
    // The desired value.
    private RR_SettingValue _desiredValue;
    // If true, the dependency check passes when the actual value
    // is NOT equal to the desired value.
    private bool _isInverse;

    //===========================================================================
    //
    // RR_ValueDependency::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_ValueDependency Create(name dependsOn, RR_SettingValue desiredValue, bool isInverse)
    {
        let d = new('RR_ValueDependency');
        d.Init(dependsOn, desiredValue, isInverse);

        return d;
    }

    //===========================================================================
    //
    // RR_ValueDependency::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(name dependsOn, RR_SettingValue desiredValue, bool isInverse)
    {
        Super.Init(dependsOn);

        if (desiredValue == null)
        {
            ThrowAbortException("%s: Missing value for dependency comparison.", GetClassName());
        }

        _desiredValue = desiredValue;
        _isInverse = isInverse;
    }

    //===========================================================================
    //
    // RR_ValueDependency::Validate
    //
    // Returns true if the actual and desired values match.
    //
    //===========================================================================
    override bool Validate(RR_SettingValue otherValue)
    {
        return (otherValue.CompareTo(_desiredValue) == 0) == !_isInverse;
    }
}
