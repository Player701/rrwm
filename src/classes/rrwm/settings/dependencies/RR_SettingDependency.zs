//===========================================================================
//
// RR_SettingDependency
//
// Base class for setting dependency behaviors.
//
//===========================================================================
class RR_SettingDependency abstract
{
    //===========================================================================
    //
    // RR_SettingDependency::Check
    //
    // Returns true is the provided setting passes the dependency check.
    //
    //===========================================================================
    abstract ui bool Check(readonly<RR_Setting> thisSetting);
}
