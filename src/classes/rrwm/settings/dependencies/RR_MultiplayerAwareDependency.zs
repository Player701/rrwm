//===========================================================================
//
// RR_MultiplayerAwareDependency
//
// Implements a setting dependency on a multiplayer-aware switch.
// The setting is enabled as long as the value of the switch takes effect
// in the current game mode.
//
//===========================================================================
class RR_MultiplayerAwareDependency : RR_SingleDependency
{
    //===========================================================================
    //
    // RR_MultiplayerAwareDependency::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_MultiplayerAwareDependency Create(name dependsOn)
    {
        let d = new('RR_MultiplayerAwareDependency');
        d.Init(dependsOn);

        return d;
    }

    //===========================================================================
    //
    // RR_MultiplayerAwareDependency::Validate
    //
    // Returns true if the other setting takes effect
    // depending on the current game mode.
    //
    //===========================================================================
    override bool Validate(RR_SettingValue otherValue)
    {
        return otherValue.IsOnMultiplayerAware();
    }
}
