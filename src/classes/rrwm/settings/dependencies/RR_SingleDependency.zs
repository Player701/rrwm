//===========================================================================
//
// RR_SingleDependency
//
// Implements a dependency on a single setting.
//
//===========================================================================
class RR_SingleDependency : RR_SettingDependency abstract
{
    private name _dependsOn;
    private RR_Setting _dependsOnSetting;

    //===========================================================================
    //
    // RR_SingleDependency::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(name dependsOn)
    {
        _dependsOn = dependsOn;
    }

    //===========================================================================
    //
    // RR_SingleDependency::Check
    //
    // Returns true is the provided setting passes the dependency check.
    //
    //===========================================================================
    override bool Check(readonly<RR_Setting> thisSetting)
    {
        _dependsOnSetting = RR_Settings.GetCached(_dependsOn, _dependsOnSetting);

        // Just in case...
        if (thisSetting == _dependsOnSetting)
        {
            ThrowAbortException("%s: A setting cannot depend on itself.", GetClassName());
        }

        // Make sure that the both settings have the same scope.
        if (thisSetting.GetScope() != _dependsOnSetting.GetScope())
        {
            ThrowAbortException("%s: Settings in the dependency chain must have the same scope.", GetClassName());
        }

        // Make sure that they also have the same category.
        if (thisSetting.GetCategory() != _dependsOnSetting.GetCategory())
        {
            ThrowAbortException("%s: Settings in the dependency chain must have the same category.", GetClassName());
        }

        // If the dependent setting is currently visible,
        // then make sure the other setting is visible too.
        if (thisSetting.IsVisible() && !_dependsOnSetting.IsVisible())
        {
            ThrowAbortException("%s: Settings in the dependency chain must be visible at the same time.", GetClassName());
        }

        return Validate(_dependsOnSetting.GetCvarValue());
    }

    //===========================================================================
    //
    // RR_SingleDependency::Validate
    //
    // When overridden in derived classes, returns true if the provided value
    // passes the dependency check.
    //
    //===========================================================================
    protected abstract ui bool Validate(RR_SettingValue otherValue);
}
