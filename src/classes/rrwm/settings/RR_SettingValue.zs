//===========================================================================
//
// RR_SettingValue
//
// Represents a setting value. This is similar to GZDoom's UCVarValue
// used internally in the source code, and it can also function
// as a reference to a CVar to retrieve the value dynamically.
//
//===========================================================================
class RR_SettingValue
{
    // The CVar (if this value is dynamic).
    private transient CVar _cvar;

    // The value type.
    private E_RR_SettingValueType _type;

    // Depending on the type, one of these fields stores the actual value.
    private bool _boolValue;
    private int _intValue;
    private double _doubleValue;

    //===========================================================================
    //
    // RR_SettingValue::Create
    //
    // Creates a new instance of this class. Used internally.
    //
    //===========================================================================
    private static RR_SettingValue Create(CVar cvar, E_RR_SettingValueType type, bool boolValue = false, int intValue = 0, double doubleValue = 0)
    {
        let v = new('RR_SettingValue');
        v.Init(cvar, type, boolValue, intValue, doubleValue);

        return v;
    }

    //===========================================================================
    //
    // RR_SettingValue::CreateBool
    //
    // Creates a new instance of RR_SettingValue with the provided bool value.
    //
    //===========================================================================
    static RR_SettingValue CreateBool(bool value)
    {
        return Create(null, RR_SVT_BOOL, boolValue: value);
    }

    //===========================================================================
    //
    // RR_SettingValue::CreateInt
    //
    // Creates a new instance of RR_SettingValue with the provided int value.
    //
    //===========================================================================
    static RR_SettingValue CreateInt(int value)
    {
        return Create(null, RR_SVT_INT, intValue: value);
    }

    //===========================================================================
    //
    // RR_SettingValue::CreateDouble
    //
    // Creates a new instance of RR_SettingValue with the provided double value.
    //
    //===========================================================================
    static RR_SettingValue CreateDouble(double value)
    {
        return Create(null, RR_SVT_DOUBLE, doubleValue: value);
    }

    //===========================================================================
    //
    // RR_SettingValue::Clamp
    //
    // Clamps the value between the specified bounds.
    //
    //===========================================================================
    static RR_SettingValue Clamp(RR_SettingValue value, RR_SettingValue minValue, RR_SettingValue maxValue)
    {
        return minValue.CompareTo(value) > 0 ? minValue : (value.CompareTo(maxValue) > 0 ? maxValue : value);
    }

    //===========================================================================
    //
    // RR_SettingValue::FromCvarDynamic
    //
    // Creates a dynamic value from the provided CVAR.
    // Dynamic values are essentially proxies for CVARs,
    // and the actual value returned will always correspond to the CVAR value.
    //
    //===========================================================================
    static RR_SettingValue FromCvarDynamic(CVar cvar)
    {
        return Create(cvar, GetTypeFromCvar(cvar));
    }

    //===========================================================================
    //
    // RR_SettingValue::FromCvarStatic
    //
    // Creates a static value from the provided CVAR.
    // Static values do not change if the CVAR value changes.
    //
    //===========================================================================
    static RR_SettingValue FromCvarStatic(CVar cvar)
    {
        let type = GetTypeFromCvar(cvar);

        switch (type)
        {
            case RR_SVT_BOOL:
                return CreateBool(cvar.GetBool());
            case RR_SVT_INT:
                return CreateInt(cvar.GetInt());
            case RR_SVT_DOUBLE:
                return CreateDouble(cvar.GetFloat());
            default:
                ThrowAbortException("RR_SettingValue: Unknown setting type %d.", type);
                return null;
        }
    }

    //===========================================================================
    //
    // RR_SettingValue::Freeze
    //
    // If this value is dynamic, "freezes" is by returning a new RR_SettingValue
    // instance which is static and contains the value of the corresponding CVAR
    // as it was when this method was called. If the value is already static,
    // the same instance is returned instead.
    //
    //===========================================================================
    RR_SettingValue Freeze()
    {
        return _cvar == null ? self : FromCvarStatic(_cvar);
    }

    //===========================================================================
    //
    // RR_SettingValue::GetTypeFromCvar
    //
    // Returns the corresponding setting type based on the CVAR type.
    //
    //===========================================================================
    private static E_RR_SettingValueType GetTypeFromCvar(CVar cvar)
    {
        if (cvar == null)
        {
            ThrowAbortException("RR_SettingValue: CVar cannot be null.");
        }

        int cvarType = cvar.GetRealType();

        switch(cvarType)
        {
            case CVar.CVAR_Bool:
                return RR_SVT_BOOL;
            case CVar.CVAR_Int:
                return RR_SVT_INT;
            case CVar.CVAR_Float:
                return RR_SVT_DOUBLE;
        }

        ThrowAbortException("RR_SettingValue: CVar type %d is not supported.", cvarType);
        return -1;
    }

    //===========================================================================
    //
    // RR_SettingValue::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(CVar cvar, E_RR_SettingValueType type, bool boolValue, int intValue, double doubleValue)
    {
        _cvar = cvar;
        _type = type;
        _boolValue = boolValue;
        _intValue = intValue;
        _doubleValue = doubleValue;
    }

    //===========================================================================
    //
    // RR_SettingValue::GetType
    //
    // Returns the type of this value.
    //
    //===========================================================================
    E_RR_SettingValueType GetType() const
    {
        return _type;
    }

    //===========================================================================
    //
    // RR_SettingValue::GetBool
    //
    // Returns the bool value.
    //
    //===========================================================================
    bool GetBool() const
    {
        CheckType(RR_SVT_BOOL, "GetBool");
        return _cvar != null ? _cvar.GetBool() : _boolValue;
    }

    //===========================================================================
    //
    // RR_SettingValue::GetInt
    //
    // Returns the int value.
    //
    //===========================================================================
    int GetInt() const
    {
        CheckType(RR_SVT_INT, "GetInt");
        return _cvar != null ? _cvar.GetInt() : _intValue;
    }

    //===========================================================================
    //
    // RR_SettingValue::GetDouble
    //
    // Returns the double value.
    //
    //===========================================================================
    double GetDouble() const
    {
        CheckType(RR_SVT_DOUBLE, "GetDouble");
        return _cvar != null ? _cvar.GetFloat() : _doubleValue;
    }

    //===========================================================================
    //
    // RR_SettingValue::IsOn
    //
    // Returns the bool value. Synonym for GetBool.
    //
    //===========================================================================
    bool IsOn() const
    {
        return GetBool();
    }

    //===========================================================================
    //
    // RR_SettingValue::IsOff
    //
    // Returns the negated bool value.
    //
    //===========================================================================
    bool IsOff() const
    {
        return !GetBool();
    }

    //===========================================================================
    //
    // RR_SettingValue::IsOnMultiplayerAware
    //
    // Checks if a multiplayer-aware option takes effect in the current game.
    //
    //===========================================================================
    bool IsOnMultiplayerAware() const
    {
        int intVal = GetInt();
        return intVal == RR_OPTVAL_ON || multiplayer && intVal == RR_OPTVAL_MULTIPLAYER_ONLY;
    }

    //===========================================================================
    //
    // RR_SettingValue::IsOnNonZero
    //
    // Checks if an option with an "if non-zero" choice takes effect
    // depending on the provided value.
    //
    //===========================================================================
    bool IsOnNonZero(int value) const
    {
        int intVal = GetInt();
        return intVal == RR_OPTVAL_ON || intVal == RR_OPTVAL_IF_NONZERO && value != 0;
    }

    //===========================================================================
    //
    // RR_SettingValue::IsOptionValue
    //
    // Checks if the integer value corresponds to the provided option value.
    //
    //===========================================================================
    bool IsOptionValue(int value) const
    {
        return GetInt() == value;
    }

    //===========================================================================
    //
    // RR_SettingValue::AsInt
    //
    // Represents the actual value as an integer and returns it.
    // This only works if the value is actually an integer or a boolean.
    //
    //===========================================================================
    int AsInt() const
    {
        if (_type != RR_SVT_BOOL && _type != RR_SVT_INT)
        {
            ThrowIncorrectType("AsInt");
        }

        return _type == RR_SVT_INT
            ? GetInt()
            : int(GetBool());
    }

    //===========================================================================
    //
    // RR_SettingValue::AsDouble
    //
    // Represents the actual value as a fractional number and returns it.
    // This only works if the value is actually a double or an integer.
    //
    //===========================================================================
    double AsDouble() const
    {
        if (_type != RR_SVT_INT && _type != RR_SVT_DOUBLE)
        {
            ThrowIncorrectType("AsDouble");
        }

        return _type == RR_SVT_DOUBLE
            ? GetDouble()
            : GetInt();
    }

    //===========================================================================
    //
    // RR_SettingValue::AsOptionString
    //
    // Returns an option string that matches the current value.
    // This only works if the value is a boolean or an integer.
    //
    //===========================================================================
    ui string AsOptionString(RR_OptionValues optVals) const
    {
        int intVal = AsInt();
        int cnt = optVals.GetCount();

        for (int i = 0; i < cnt; i++)
        {
            if (intVal == optVals.GetValue(i))
            {
                return optVals.GetText(i);
            }
        }

        // We shouldn't be here
        ThrowAbortException("%s: Could not find option text for value %d.", GetClassName(), intVal);
        return "";
    }

    //===========================================================================
    //
    // RR_SettingValue::AsNumericString
    //
    // Returns a string representing the current numeric value.
    // This only works if the value is a double or an integer.
    //
    //===========================================================================
    ui string AsNumericString() const
    {
        double doubleVal = AsDouble();

        return _type == RR_SVT_DOUBLE
            ? string.Format("%f", doubleVal)
            : string.Format("%d", int(doubleVal));
    }

    //===========================================================================
    //
    // RR_SettingValue::CompareTo
    //
    // Returns 0 if the values match, and a non-zero value otherwise.
    // The values must have the same type, or an exception is thrown.
    //
    //===========================================================================
    int CompareTo(RR_SettingValue other) const
    {
        // We cannot compare with null.
        if (other == null)
        {
            ThrowAbortException("%s: Attempt to compare with null.", GetClassName());
        }

        // If this is the same instance, the values always match by definition.
        if (self == other)
        {
            return 0;
        }

        // Values of different types cannot be compared.
        if (_type != other.GetType())
        {
            ThrowAbortException("%s: Attempt to compare values of different types (%s and %s).", GetClassName(), GetValueTypeString(), other.GetValueTypeString());
        }

        switch(_type)
        {
            case RR_SVT_BOOL:
                return GetBool() == other.GetBool() ? 0 : 1;
            case RR_SVT_INT:
                return GetInt() - other.GetInt();
            case RR_SVT_DOUBLE:
                double diff = GetDouble() - other.GetDouble();
                return RR_Math.IsAlmostZero(diff)
                    ? 0
                    : (diff > 0 ? 1 : -1);
        }

        // We shouldn't be here.
        ThrowInvalidType();
        return 0;
    }

    //===========================================================================
    //
    // RR_SettingValue::CheckType
    //
    // Checks that the value type matches the provided one, and if it doesn't,
    // throws an exception.
    //
    //===========================================================================
    private void CheckType(E_RR_SettingValueType desiredType, string methodName) const
    {
        if (_type != desiredType)
        {
            ThrowIncorrectType(methodName);
        }
    }

    //===========================================================================
    //
    // RR_SettingValue::GetValueTypeString
    //
    // Returns a human-readable string representing the value type.
    // Used for diagnostic messages and debugging only.
    //
    //===========================================================================
    private string GetValueTypeString() const
    {
        switch(_type)
        {
            case RR_SVT_BOOL:
                return "bool";
            case RR_SVT_INT:
                return "int";
            case RR_SVT_DOUBLE:
                return "double";
        }

        // We shouldn't be here.
        ThrowInvalidType();
        return "";
    }

    //===========================================================================
    //
    // RR_SettingValue::ThrowIncorrectType
    //
    // Throws an exception with an incorrect type error message.
    //
    //===========================================================================
    private void ThrowIncorrectType(string methodName) const
    {
        ThrowAbortException("%s: Incorrect type (%s) for %s.", GetClassName(), GetValueTypeString(), methodName);
    }

    //===========================================================================
    //
    // RR_SettingValue::ThrowInvalidType
    //
    // Throws an exception with an invalid type error message.
    //
    //===========================================================================
    private void ThrowInvalidType() const
    {
        ThrowAbortException("%s: Invalid value type %d incountered.", GetClassName(), _type);
    }
}
