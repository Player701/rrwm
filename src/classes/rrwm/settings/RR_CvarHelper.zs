//===========================================================================
//
// RR_CvarHelper
//
// Contains some helper methods for retrieving CVARs.
//
//===========================================================================
class RR_CvarHelper
{
    //===========================================================================
    //
    // RR_CvarHelper::MustFindCvar
    //
    // Returns the CVAR via FindCVar if it exists,
    // otherwise throws an exception.
    //
    //===========================================================================
    static CVar MustFindCvar(name cvarName)
    {
        return CvarNotNull(CVar.FindCVar(cvarName), cvarName);
    }

    //===========================================================================
    //
    // RR_CvarHelper::MustGetCvar
    //
    // Returns the CVAR via GetCVar if it exists,
    // otherwise throws an exception.
    //
    //===========================================================================
    static CVar MustGetCvar(name cvarName, PlayerInfo player)
    {
        return CvarNotNull(CVar.GetCVar(cvarName, player), cvarName);
    }

    //===========================================================================
    //
    // RR_CvarHelper::CvarNotNull
    //
    // Returns the provided CVAR if it's not a null reference,
    // otherwise throws an exception.
    //
    //===========================================================================
    private static CVar CvarNotNull(CVar cvar, name cvarName)
    {
        if (cvar == null)
        {
            ThrowAbortException("RR_CvarHelper: Could not find registered CVar \"%s\".", cvarName);
        }

        return cvar;
    }
}
