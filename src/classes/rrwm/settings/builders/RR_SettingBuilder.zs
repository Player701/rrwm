//===========================================================================
//
// RR_SettingBuilder
//
// Base class for building settings via fluent API.
//
//===========================================================================
class RR_SettingBuilder abstract
{
    private RR_Settings _settings;

    // Required fields
    private name _cvarName;

    private E_RR_SettingScope _scope;
    private bool _hasScope;

    private E_RR_SettingCategory _category;
    private bool _hasCategory;

    // Optional fields
    private E_RR_SettingGameplayImpact _gameplayImpact;
    private bool _hasGameplayImpact;

    private E_RR_SettingVisibilityFlags _visibilityFlags;
    private bool _hasVisibilityFlags;

    private Array<RR_SettingDependency> _dependencies;

    //===========================================================================
    //
    // RR_SettingBuilder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_Settings settings, name cvarName)
    {
        _settings = settings;
        _cvarName = cvarName;
        _gameplayImpact = RR_SGI_NONE;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::HasCvarName
    //
    // Sets the new setting to have the provided CVar name.
    //
    //===========================================================================
    RR_SettingBuilder HasCvarName(name cvarName)
    {
        if (_cvarName != 'None')
        {
            ThrowAbortException("%s: Attempt to redefine CVar name.", GetClassName());
        }

        _cvarName = cvarName;
        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::HasScope
    //
    // Sets the new setting to have the provided scope.
    //
    //===========================================================================
    RR_SettingBuilder HasScope(E_RR_SettingScope scope)
    {
        if (_hasScope)
        {
            ThrowAbortException("%s: Attempt to redefine scope.", GetClassName());
        }

        _scope = scope;
        _hasScope = true;

        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::HasCategory
    //
    // Sets the new setting to be in the provided category.
    //
    //===========================================================================
    RR_SettingBuilder HasCategory(E_RR_SettingCategory category)
    {
        if (_hasCategory)
        {
            ThrowAbortException("%s: Attempt to redefine category.", GetClassName());
        }

        _category = category;
        _hasCategory = true;

        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::HasGameplayImpact
    //
    // Sets the new setting to have the provided gameplay impact.
    // This is an optional call.
    //
    //===========================================================================
    RR_SettingBuilder HasGameplayImpact(E_RR_SettingGameplayImpact gameplayImpact)
    {
        if (_hasGameplayImpact)
        {
            ThrowAbortException("%s: Attempt to redefine gameplay impact.", GetClassName());
        }

        _gameplayImpact = gameplayImpact;
        _hasGameplayImpact = true;

        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::HasVisibilityFlags
    //
    // Sets the new setting to have the provided visibility flags.
    // This is an optional call.
    //
    //===========================================================================
    RR_SettingBuilder HasVisibilityFlags(E_RR_SettingVisibilityFlags visibilityFlags)
    {
        if (_hasVisibilityFlags)
        {
            ThrowAbortException("%s: Attempt to redefine visibility flags.", GetClassName());
        }

        _visibilityFlags = visibilityFlags;
        _hasVisibilityFlags = true;

        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::DependsOnSwitch
    //
    // Adds a dependency on a switch to the new setting.
    // When the switch is on, the setting is enabled.
    //
    //===========================================================================
    RR_SettingBuilder DependsOnSwitch(name cvarName)
    {
        return SetDependency(RR_ValueDependency.Create(cvarName, RR_SettingValue.CreateBool(true), false));
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_SettingBuilder::DependsOnMultiplayerAware
    //
    // Adds a dependency on a multiplayer-aware switch to the new setting.
    // When the switch takes effect, the setting is enabled.
    //
    //===========================================================================
    RR_SettingBuilder DependsOnMultiplayerAware(name cvarName)
    {
        return SetDependency(RR_MultiplayerAwareDependency.Create(cvarName));
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_SettingBuilder::DependsOnOptionValue
    //
    // Adds a dependency on an option value to the new setting.
    // When the option has the provided value, the setting is enabled.
    //
    //===========================================================================
    RR_SettingBuilder DependsOnOptionValue(name cvarName, int value, bool isInverse = false)
    {
        return SetDependency(RR_ValueDependency.Create(cvarName, RR_SettingValue.CreateInt(value), isInverse));
    }

    //===========================================================================
    //
    // RR_SettingBuilder::SetDependency
    //
    // Sets the setting to have the provided dependency.
    //
    //===========================================================================
    private RR_SettingBuilder SetDependency(RR_SettingDependency dependency)
    {
        _dependencies.Push(dependency);
        return self;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::GetDependency
    //
    // Creates a single instance of RR_SettingDependency from all dependencies
    // previously added to this builder.
    //
    //===========================================================================
    private RR_SettingDependency GetDependency() const
    {
        int n = _dependencies.Size();

        if (n == 0)
        {
            // No dependencies
            return null;
        }

        if (n == 1)
        {
            // Single dependency
            return _dependencies[0];
        }

        // Many dependencies
        let result = RR_MultiDependency.Create();

        foreach (dep : _dependencies)
        {
            result.Add(dep);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_SettingBuilder::Add
    //
    // Adds the setting to the settings list.
    //
    //===========================================================================
    void Add()
    {
        if (!_hasScope)
        {
            ThrowAbortException("%s: Scope must be specified.", GetClassName());
        }

        if (!_hasCategory)
        {
            ThrowAbortException("%s: Category must be specified.", GetClassName());
        }

        let valueProvider = _settings.CreateValueProvider(_cvarName, _scope);
        let setting = CreateSetting(
            valueProvider,
            _category, _gameplayImpact,
            _visibilityFlags, GetDependency()
        );

        _settings.Add(setting);
    }

    //===========================================================================
    //
    // RR_SettingBuilder::CreateSetting
    //
    // When overridden in derived classes, creates a new setting instance
    // that will be added to the settings list.
    //
    //===========================================================================
    protected abstract RR_Setting CreateSetting(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    );
}
