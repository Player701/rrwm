//===========================================================================
//
// RR_NumberBuilder
//
// Used to build an instance of RR_SettingNumber with the fluent API.
//
//===========================================================================
class RR_NumberBuilder : RR_SettingBuilder
{
    private RR_SettingValue _minValue, _maxValue;

    //===========================================================================
    //
    // RR_NumberBuilder::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_NumberBuilder Create(RR_Settings settings, name cvarName)
    {
        let b = new('RR_NumberBuilder');
        b.Init(settings, cvarName);

        return b;
    }

    //===========================================================================
    //
    // RR_NumberBuilder::HasRange
    //
    // Sets the new setting to have the provided range of integer values.
    //
    //===========================================================================
    RR_NumberBuilder HasRange(int minValue, int maxValue)
    {
        CheckRange(minValue, maxValue);

        _minValue = RR_SettingValue.CreateInt(minValue);
        _maxValue = RR_SettingValue.CreateInt(maxValue);

        return self;
    }

    //===========================================================================
    //
    // RR_NumberBuilder::HasFractionalRange
    //
    // Sets the new setting to have the provided range of fractional values.
    //
    //===========================================================================
    RR_NumberBuilder HasFractionalRange(double minValue, double maxValue)
    {
        CheckRange(minValue, maxValue);

        _minValue = RR_SettingValue.CreateDouble(minValue);
        _maxValue = RR_SettingValue.CreateDouble(maxValue);

        return self;
    }

    //===========================================================================
    //
    // RR_NumberBuilder::CheckRange
    //
    // Verifies that the range has not already been set and that the minimum
    // value is less than the maximum value.
    //
    //===========================================================================
    private void CheckRange(double minValue, double maxValue)
    {
        if (_minValue != null)
        {
            ThrowAbortException("%s: Attempt to redefine value range.", GetClassName());
        }

        if (minValue >= maxValue)
        {
            ThrowAbortException("%s: Minimum value must be less than the maximum value.", GetClassName());
        }
    }

    //===========================================================================
    //
    // RR_NumberBuilder::CreateSetting
    //
    // Creates a new instance of RR_SettingNumber with the parameters
    // specified earlier.
    //
    //===========================================================================
    override RR_Setting CreateSetting(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        if (_minValue == null)
        {
            ThrowAbortException("%s: Value range must be specified.", GetClassName());
        }

        return RR_SettingNumber.Create(
            valueProvider,
            _minValue, _maxValue,
            category, gameplayImpact,
            visibilityFlags, dependency
        );
    }
}
