//===========================================================================
//
// RR_OptionBuilder
//
// Used to build an instance of RR_SettingOption with the fluent API.
//
//===========================================================================
class RR_OptionBuilder : RR_SettingBuilder
{
    private name _optionValues;

    //===========================================================================
    //
    // RR_OptionBuilder::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_OptionBuilder Create(RR_Settings settings, name cvarName)
    {
        let b = new('RR_OptionBuilder');
        b.Init(settings, cvarName);

        return b;
    }

    //===========================================================================
    //
    // RR_OptionBuilder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_Settings settings, name cvarName)
    {
        Super.Init(settings, cvarName);
        _optionValues = 'None';
    }

    //===========================================================================
    //
    // RR_OptionBuilder::HasOptionValues
    //
    // Sets the new setting to have the provided option values.
    //
    //===========================================================================
    RR_OptionBuilder HasOptionValues(name optionValues)
    {
        if (_optionValues != 'None')
        {
            ThrowAbortException("%s: Attempt to redefine option values.", GetClassName());
        }

        _optionValues = optionValues;
        return self;
    }

    //===========================================================================
    //
    // RR_OptionBuilder::CreateSetting
    //
    // Creates a new instance of RR_SettingOption with the parameters
    // specified earlier.
    //
    //===========================================================================
    override RR_Setting CreateSetting(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        if (_optionValues == 'None')
        {
            ThrowAbortException("%s: Option values must be specified.", GetClassName());
        }

        return RR_SettingOption.Create(
            valueProvider,
            _optionValues,
            category, gameplayImpact,
            visibilityFlags, dependency
        );
    }
}
