//===========================================================================
//
// RR_SwitchBuilder
//
// Used to build an instance of RR_SettingSwitch with the fluent API.
//
//===========================================================================
class RR_SwitchBuilder : RR_SettingBuilder
{
    //===========================================================================
    //
    // RR_SwitchBuilder::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_SwitchBuilder Create(RR_Settings settings, name cvarName)
    {
        let b = new('RR_SwitchBuilder');
        b.Init(settings, cvarName);

        return b;
    }

    //===========================================================================
    //
    // RR_SwitchBuilder::CreateSetting
    //
    // Creates a new instance of RR_SettingSwitch with the parameters
    // specified earlier.
    //
    //===========================================================================
    override RR_Setting CreateSetting(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        return RR_SettingSwitch.Create(
            valueProvider,
            category, gameplayImpact,
            visibilityFlags, dependency
        );
    }
}
