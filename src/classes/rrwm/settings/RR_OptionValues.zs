//===========================================================================
//
// RR_OptionValues
//
// Encapsulates the OptionValues definition for menus.
// Note that GZDoom already has this defined internally,
// but it's not exported to ZScript as a struct or a class.
//
//===========================================================================
class RR_OptionValues
{
    private name _optVals;

    //===========================================================================
    //
    // RR_OptionValues::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_OptionValues Create(name optVals)
    {
        let ov = new('RR_OptionValues');
        ov.Init(optVals);

        return ov;
    }

    //===========================================================================
    //
    // RR_OptionValues::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(name optVals)
    {
        _optVals = optVals;
    }

    //===========================================================================
    //
    // RR_OptionValues::GetName
    //
    // Returns the name of the option values list.
    //
    //===========================================================================
    name GetName() const
    {
        return _optVals;
    }

    //===========================================================================
    //
    // RR_OptionValues::GetCount
    //
    // Returns the total number of values.
    //
    //===========================================================================
    int GetCount() const
    {
        int cnt = OptionValues.GetCount(_optVals);

        // Check for empty option value list
        // NB: this cannot be checked during initialization,
        // since it happens before OptionValues are fully parsed.
        if (cnt == 0)
        {
            ThrowAbortException("%s: OptionValues must have at least one value.", GetClassName());
        }

        return cnt;
    }

    //===========================================================================
    //
    // RR_OptionValues::GetValue
    //
    // Returns the option value at the provided index.
    //
    //===========================================================================
    int GetValue(int index) const
    {
        return int(OptionValues.GetValue(_optVals, index));
    }

    //===========================================================================
    //
    // RR_OptionValues::GetText
    //
    // Returns the option text at the provided index.
    //
    //===========================================================================
    ui string GetText(int index) const
    {
        return OptionValues.GetText(_optVals, index);
    }
}
