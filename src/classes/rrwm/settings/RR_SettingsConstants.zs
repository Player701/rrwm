//===========================================================================
//
// RR_SettingsConstants
//
// Contains enums and constants for the RRWM settings subsystem.
//
//===========================================================================

// This is the common prefix for all setting-related strings.
// Used to generate a localized string name for language.enu lookup.
const RR_CVAR_PREFIX = "rrwm_";

// This enumeration contains all CVAR names used by RRWM settings.
// NB: name constants are not allowed, but string constants work just fine.
enum E_RR_SettingName
{
    // HUD options
    RR_SETTING_HUD_ANIMSPEED           = "wl_animspeed",
    RR_SETTING_HUD_LOWHEALTH_THRESHOLD = "rrwm_hud_lowhealth_threshold",
    RR_SETTING_HUD_STAT_KILLS          = "rrwm_hud_stat_kills",
    RR_SETTING_HUD_STAT_ITEMS          = "rrwm_hud_stat_items",
    RR_SETTING_HUD_STAT_SECRETS        = "rrwm_hud_stat_secrets",
    RR_SETTING_HUD_INDSTATS            = "rrwm_hud_indstats",
    RR_SETTING_HUD_STATICONS           = "rrwm_hud_staticons",
    RR_SETTING_HUD_STATSTYLE           = "rrwm_hud_statstyle",
    RR_SETTING_HUD_MUGSHOT             = "rrwm_hud_mugshot",
    RR_SETTING_HUD_SMALLKEYICONS       = "rrwm_hud_smallkeyicons",
    RR_SETTING_HUD_ITEMTAGS            = "rrwm_hud_itemtags",
    RR_SETTING_HUD_INVBARALIGN         = "rrwm_hud_invbaralign",
//<ExcludeFromHudPackage>
    RR_SETTING_HUD_LOWAMMO             = "rrwm_hud_lowammo",
    RR_SETTING_HUD_LOWAMMO_THRESHOLD   = "rrwm_hud_lowammo_threshold",
//</ExcludeFromHudPackage>
    RR_SETTING_HUD_MAXVITALS           = "rrwm_hud_maxvitals",
    RR_SETTING_HUD_ENEMYHEALTH         = "rrwm_hud_enemyhealth",
    RR_SETTING_HUD_ENEMYHEALTH_MAXDIST = "rrwm_hud_enemyhealth_maxdist",
    RR_SETTING_HUD_WEAPONSLOTS         = "rrwm_hud_weaponslots",
//<ExcludeFromHudPackage>
    RR_SETTING_HUD_WEAPONSLOTS_CLIP    = "rrwm_hud_weaponslots_clip",
//</ExcludeFromHudPackage>
    RR_SETTING_HUD_AMMOTABLE           = "rrwm_hud_ammotable",
    RR_SETTING_HUD_AMMOTABLE_SHOWALL   = "rrwm_hud_ammotable_showall",
    RR_SETTING_HUD_AMMOTABLE_SHOWCUR   = "rrwm_hud_ammotable_showcur",
    RR_SETTING_HUD_PLAYERCOLOR         = "rrwm_hud_playercolor",
    RR_SETTING_HUD_POWERUPS            = "rrwm_hud_powerups",
    RR_SETTING_HUD_AIRSUPPLY           = "rrwm_hud_airsupply",
    RR_SETTING_HUD_DEBUG               = "rrwm_hud_debug",
//<ExcludeFromHudPackage>

    // User options
    RR_SETTING_RELOADMODE              = "rrwm_reloadmode",
    RR_SETTING_AUTORELOAD_THRESHOLD    = "rrwm_autoreload_threshold",

    // Game options
    RR_SETTING_SNIPING                 = "rrwm_sniping",
    RR_SETTING_RECOIL                  = "rrwm_recoil",
    RR_SETTING_FORCEVERTSPREAD         = "rrwm_forcevertspread",
    RR_SETTING_MONSTERVERTSPREAD       = "rrwm_monstervertspread",
    RR_SETTING_PARTIALPICKUP           = "rrwm_partialpickup",
    RR_SETTING_WEAPONDROP_AMMO         = "rrwm_weapondrop_ammo",
    RR_SETTING_DEBRISQUEUE_ENABLED     = "rrwm_debrisqueue_enabled",
    RR_SETTING_DEBRISQUEUE_LIMIT       = "rrwm_debrisqueue_limit",
    RR_SETTING_CASINGSMOKE             = "rrwm_casingsmoke",
    RR_SETTING_CHAINSAWSMOKE           = "rrwm_chainsawsmoke",
    RR_SETTING_ROCKETSMOKE             = "rrwm_rocketsmoke",
    RR_SETTING_SPIDERDEMON_CASINGS     = "rrwm_spiderdemon_casings",
    RR_SETTING_CYBERDEMON_NEWROCKETS   = "rrwm_cyberdemon_newrockets",

    // Persistent game options
    RR_SETTING_RAILGUN                 = "rrwm_railgun",
    RR_SETTING_EXTRAARMOR              = "rrwm_extraarmor",

    RR_SETTING_SUPERARMORSLOT          = "rrwm_superarmorslot",
    RR_SETTING_LIMITBERSERK            = "rrwm_limitberserk",
    RR_SETTING_MONSTERRELOAD           = "rrwm_monsterreload",
    RR_SETTING_SHOTGUN_REALISTICRELOAD = "rrwm_shotgun_realisticreload",
    RR_SETTING_NEWCHAINGUNGUY          = "rrwm_newchaingunguy",
//</ExcludeFromHudPackage>
};

// Enumerates the value types used by RRWM settings.
enum E_RR_SettingValueType
{
    // Boolean (true or false).
    RR_SVT_BOOL,

    // Integer value.
    RR_SVT_INT,

    // Fractional value.
    RR_SVT_DOUBLE
};

// Flags to configure when the setting is visible
enum E_RR_SettingVisibilityFlags
{
    // This setting is not visible in deathmatch games.
    RR_SVF_NOTDEATHMATCH = 1,

    // This setting is only visible in Doom games (incl. Chex Quest).
    RR_SVF_DOOMONLY      = 1 << 1,

    // This setting is not visible in Heretic and Hexen.
    RR_SVF_NOTRAVEN      = 1 << 2
};

// These are the possible setting scopes.
enum E_RR_SettingScope
{
    // This is a user setting, which is local to the player
    // and can be changed at any time.
    RR_SS_USER,
//<ExcludeFromHudPackage>

    // This is a server setting, which affects all players
    // and can only be changed by the host at any time.
    RR_SS_SERVER,

    // This is a persistent server setting, which affects all players
    // and can only be changed by the host.
    // Any changes will only take effect on a new game.
    RR_SS_SERVER_PERSISTENT,
//</ExcludeFromHudPackage>

    // Total number of scopes. This is not an actual scope value
    // but a marker to easily get the number of scopes we have.
    RR_NUM_SETTING_SCOPES
};

// Possible values for the gameplay impact parameter
enum E_RR_SettingGameplayImpact
{
    // No impact on gameplay.
    RR_SGI_NONE,

    // Little impact on gameplay.
    RR_SGI_LOW,

    // Medium impact on gameplay.
    RR_SGI_MEDIUM,

    // Sever impact on gameplay.
    RR_SGI_HIGH
};

// Possible values for setting categories
enum E_RR_SettingCategory
{
//<ExcludeFromHudPackage>
    // General gameplay options
    RR_SC_GAMEPLAY,

    // Weapon options
    RR_SC_WEAPONS,

    // Cosmetic options
    RR_SC_COSMETIC,
//</ExcludeFromHudPackage>
    // HUD options
    RR_SC_HUD,

    // Total number of categories. Not an actual value,
    // used in the menu code to group settings by category.
    RR_NUM_SETTING_CATEGORIES
};
//<ExcludeFromHudPackage>

// Possible values for the "reload mode" setting
enum E_RR_ReloadMode
{
    // Full auto: the weapon will reload automatically if its clip is empty,
    // without the need to press the reload button.
    RR_RM_FULLAUTO = 0,

    // Semi-auto: the weapon will reload if attempting to fire it with an empty clip.
    RR_RM_SEMIAUTO = 1,

    // Manual: the weapon will only reload if the reload buttom is pressed.
    RR_RM_MANUAL   = 2
};

// Possible values for the "enable sniping" setting
enum E_RR_SnipingMode
{
    // Sniping is off
    RR_SM_OFF         = 0,

    // Sniping is like in vanilla (first shot is 100% accurate).
    RR_SM_VANILLALIKE = 1,

    // Sniping is only possible with the pistol's laser sight.
    RR_SM_LASERSIGHT  = 2
};

// Possible values for the "enable rocket smoke" setting
enum E_RR_RocketSmokeCount
{
    // No smoke.
    RR_RSC_OFF     = 0,

    // Normal (full) smoke count.
    RR_RSC_FULL    = 1,

    // Reduced smoke count (50% of full).
    RR_RSC_REDUCED = 2
};
//</ExcludeFromHudPackage>

// Possible values for common option value sets
enum E_RR_OptionValues
{
    // The option is always off.
    RR_OPTVAL_OFF              = 0,

    // The option is always on.
    RR_OPTVAL_ON               = 1,

    // The option is on if the value it controls is non-zero.
    RR_OPTVAL_IF_NONZERO       = 2,

    // The option is on in multiplayer and off in single player.
    RR_OPTVAL_MULTIPLAYER_ONLY = 2
};

// Possible values for the "statistics display style" setting
enum E_RR_HudStatStyle
{
    // Current and maximum value
    RR_HSS_CUR_MAX = 0,

    // Percentage
    RR_HSS_PCT     = 1,

    // Current value and percentage
    RR_HSS_CUR_PCT = 2
};

// Possible values for the "inventory bar alignment" setting
enum E_RR_InvBarAlign
{
    // Center
    RR_IBA_CENTER = 0,

    // Left
    RR_IBA_LEFT   = 1,

    // Right
    RR_IBA_RIGHT  = 2
};

// Possible values for the "show enemy health bar" setting
enum E_RR_EnemyHealth
{
    // Off
    RR_EH_OFF          = 0,

    // Single-color
    RR_EH_SINGLE_COLOR = 1,

    // Interpolated
    RR_EH_INTERPOLATED = 2
};

// Possible values for the "show powerups" setting
enum E_RR_PowerupDisplay
{
    // Off
    RR_PD_OFF        = 0,

    // Icons only
    RR_PD_ICONS_ONLY = 1,

    // Icons + time
    RR_PD_ICONS_TIME = 2
};
