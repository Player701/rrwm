//===========================================================================
//
// RR_SettingNumber
//
// A type of RRWM setting represented by a numeric value
// within a certain range.
//
//===========================================================================
class RR_SettingNumber : RR_Setting
{
    // The minimum and maximum values of this setting.
    private RR_SettingValue _minValue, _maxValue;

    //===========================================================================
    //
    // RR_SettingNumber::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_SettingNumber Create(
        RR_ValueProvider valueProvider,
        RR_SettingValue minValue, RR_SettingValue maxValue,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        let setting = new('RR_SettingNumber');

        setting.Init(
            valueProvider,
            minValue, maxValue,
            category, gameplayImpact,
            visibilityFlags, dependency
        );

        return setting;
    }

    //===========================================================================
    //
    // RR_SettingNumber::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(
        RR_ValueProvider valueProvider,
        RR_SettingValue minValue, RR_SettingValue maxValue,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency)
    {
        Super.Init(
            valueProvider,
            category, gameplayImpact,
            visibilityFlags, dependency
        );

        if (minValue == null)
        {
            ThrowAbortException("%s: Minimum value must be specified.", GetClassName());
        }

        if (maxValue == null)
        {
            ThrowAbortException("%s: Maximum value must be specified.", GetClassName());
        }

        _minValue = minValue;
        _maxValue = maxValue;
    }

    //===========================================================================
    //
    // RR_SettingNumber::GetMinValue
    //
    // Returns the minimum possible value for this setting.
    //
    //===========================================================================
    RR_SettingValue GetMinValue() const
    {
        return _minValue;
    }

    //===========================================================================
    //
    // RR_SettingNumber::GetMaxValue
    //
    // Returns the maximum possible value for this setting.
    //
    //===========================================================================
    RR_SettingValue GetMaxValue() const
    {
        return _maxValue;
    }

    //===========================================================================
    //
    // RR_SettingNumber::GetFriendlyTypeName
    //
    // Returns the friendly type name for this setting.
    //
    //===========================================================================
    override string GetFriendlyTypeName() const
    {
        return GetUiValue().GetType() == RR_SVT_DOUBLE
            ? "$RR_MENU_SETTING_TYPE_DOUBLE"
            : "$RR_MENU_SETTING_TYPE_INT";
    }

    //===========================================================================
    //
    // RR_SettingNumber::ValueToString
    //
    // Returns a string representing the provided numeric value.
    //
    //===========================================================================
    override string ValueToString(RR_SettingValue value) const
    {
        return value.AsNumericString();
    }

    //===========================================================================
    //
    // RR_SettingNumber::GetMenuItem
    //
    // Returns a slider menu item to control this setting.
    //
    //===========================================================================
    override OptionMenuItem GetMenuItem() const
    {
        return RR_MenuItemSlider.Create(self);
    }

    //===========================================================================
    //
    // RR_SettingNumber::PostprocessValue
    //
    // Clamps the resulting value between the bounds.
    //
    //===========================================================================
    override RR_SettingValue PostprocessValue(RR_SettingValue value) const
    {
        return RR_SettingValue.Clamp(value, _minValue, _maxValue);
    }
}
