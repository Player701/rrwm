//===========================================================================
//
// RR_Setting
//
// Base class for objects that store metadata for RRWM settings.
// They are initialized only once, when the engine starts.
//
//===========================================================================
class RR_Setting abstract
{
    // Stores the value provider instance for this setting.
    private RR_ValueProvider _valueProvider;

    // The category this settings belongs to.
    private E_RR_SettingCategory _category;

    // The gameplay impact value of the setting.
    private E_RR_SettingGameplayImpact _gameplayImpact;

    // Flags to configure the visbility of this setting.
    private E_RR_SettingVisibilityFlags _visibilityFlags;

    // If non-null, stores dependency logic for this setting.
    private RR_SettingDependency _dependency;

    // Stores the cached dynamic value derived from the corresponding CVAR.
    // Used for UI purposes only.
    private RR_SettingValue _cachedUiCvarValue;

    // Cached values before and after postprocessing (to avoid needless allocations)
    private RR_SettingPostprocessorCache _uiValueCache, _playValueCache;

    //===========================================================================
    //
    // RR_Setting::Init
    //
    // Performs first-time initialization. This method is called from subclasses.
    //
    //===========================================================================
    protected void Init(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visbilityFlags, RR_SettingDependency dependency
    )
    {
        _valueProvider = valueProvider;
        _gameplayImpact = gameplayImpact;
        _category = category;
        _visibilityFlags = visbilityFlags;
        _dependency = dependency;
        _uiValueCache = RR_SettingPostprocessorCache.Create();
        _playValueCache = RR_SettingPostprocessorCache.Create();
    }

    //===========================================================================
    //
    // RR_Setting::GetScope
    //
    // Gets the scope of this setting.
    //
    //===========================================================================
    E_RR_SettingScope GetScope() const
    {
        return _valueProvider.GetScope();
    }

    //===========================================================================
    //
    // RR_Setting::IsServer
    //
    // Checks whether this setting is controlled by the server.
    //
    //===========================================================================
    bool IsServer() const
    {
        return GetScope() != RR_SS_USER;
    }

    //===========================================================================
    //
    // RR_Setting::GetCategory
    //
    // Returns the category this setting belongs to.
    //
    //===========================================================================
    E_RR_SettingCategory GetCategory() const
    {
        return _category;
    }

    //===========================================================================
    //
    // RR_Setting::GetGameplayImpact
    //
    // Returns the value which signifies how much effect
    // the setting has on the gameplay.
    //
    //===========================================================================
    E_RR_SettingGameplayImpact GetGameplayImpact() const
    {
        return _gameplayImpact;
    }

    //===========================================================================
    //
    // RR_Setting::GetCvarName
    //
    // Returns the name of the CVAR which corresponds to this setting.
    //
    //===========================================================================
    name GetCvarName() const
    {
        return _valueProvider.GetCvarName();
    }

    //===========================================================================
    //
    // RR_Setting::GetFriendlyName
    //
    // Returns the friendly, descriptive name of this setting.
    //
    //===========================================================================
    ui string GetFriendlyName() const
    {
        return GetStringLabel("TITLE");
    }

    //===========================================================================
    //
    // RR_Setting::GetFriendlyTypeName
    //
    // Returns the friendly type name of this setting
    // to be displayed on the help screen.
    //
    //===========================================================================
    abstract ui string GetFriendlyTypeName() const;

    //===========================================================================
    //
    // RR_Setting::GetDescription
    //
    // Returns the detailed description of this setting.
    //
    //===========================================================================
    ui string GetDescription() const
    {
        string result = GetBaseDescription();

        // If necessary, add a notice that this setting is not available
        // in deathmatch games.
        if (_visibilityFlags & RR_SVF_NOTDEATHMATCH)
        {
            result = string.Format(
                "%s\n\n%s",
                StringTable.Localize(result),
                StringTable.Localize("$RR_MENU_HELP_DESCRIPTION_NOTDEATHMATCH")
            );

            // Check if this setting additionally has an explanation
            // for why it's not available in deathmatch.
            string ex = GetStringLabel("DESCRIPTION_NOTDEATHMATCH");
            string exLocalized;

            if (RR_StringTools.TryLocalize(ex, exLocalized))
            {
                result = string.Format("%s %s", result, exLocalized);
            }
        }

        // Additionally, check if this setting has a warning
        return AddWarning(result, "DESCRIPTION", "\n\n");
    }

    //===========================================================================
    //
    // RR_Setting::AddWarning
    //
    // Adds formatted warning text at the end of the provided string,
    // searching for string labels with the provided pattern
    // and using the specified delimiter to append the warning text.
    //
    //===========================================================================
    protected string AddWarning(string str, string prefix, string delimiter) const
    {
        string warning = GetStringLabel(string.Format("%s_WARNING", prefix));
        string warningLocalized;

        if (RR_StringTools.TryLocalize(warning, warningLocalized))
        {
            if (RR_StringTools.IsLabel(str))
            {
                str = StringTable.Localize(str);
            }

            // Check for different warning title (e.g. "IMPORTANT WARNING")
            string warningTitle, warningTitleLocalized;
            string customWarningTitle = GetStringLabel(string.Format("%s_WARNING_TITLE", prefix));

            if (!RR_StringTools.TryLocalize(customWarningTitle, warningTitleLocalized))
            {
                warningTitleLocalized = StringTable.Localize("$RR_WARNING");
            }

            str = string.Format(
                "%s%s%s%s:%s %s",
                str,
                delimiter,
                TEXTCOLOR_FIRE,
                warningTitleLocalized,
                TEXTCOLOR_NORMAL,
                warningLocalized
            );
        }

        return str;
    }

    //===========================================================================
    //
    // RR_Setting::GetBaseDescription
    //
    // Returns the main part of this setting's description.
    //
    //===========================================================================
    protected virtual ui string GetBaseDescription() const
    {
        return GetStringLabel("DESCRIPTION");
    }

    //===========================================================================
    //
    // RR_Setting::GetStringLabel
    //
    // Returns a LANGUAGE string label for this setting
    // that has the provided suffix.
    //
    //===========================================================================
    protected string GetStringLabel(string suffix) const
    {
        string newName = RR_StringTools.MaybeStringPrefix(GetCvarName(), RR_CVAR_PREFIX);
        return string.Format("$RR_SETTING_%s_%s", newName, suffix).MakeUpper();
    }

    //===========================================================================
    //
    // RR_Setting::IsVisible
    //
    // Returns whether this setting should be visible in the menu
    // at the current time.
    //
    //===========================================================================
    ui bool IsVisible() const
    {
        // If this is a deathmatch game or a team deathmatch game,
        // do not display this setting if it's set to disappear in deathmatch.
        bool hidden = (_visibilityFlags & RR_SVF_NOTDEATHMATCH) && (deathmatch || teamplay);
        // If this is a non-Doom-like game (Doom + Chex),
        // do not display this setting if it's set to appear for Doom games only.
        hidden = hidden || (_visibilityFlags & RR_SVF_DOOMONLY) && !(GameInfo.GameType & GAME_DoomChex);
        // If this is a Heretic or Hexen game, do not display this setting
        // if it's set to disappear in Raven games.
        hidden = hidden || (_visibilityFlags & RR_SVF_NOTRAVEN) && (GameInfo.GameType & GAME_Raven);

        return !hidden;
    }

    //===========================================================================
    //
    // RR_Setting::IsEnabled
    //
    // Returns whether this setting is currently enabled in the menu.
    //
    //===========================================================================
    ui bool IsEnabled() const
    {
        // The setting is always enabled if it's a user setting,
        // server settings are enabled if the current player can control them.
        bool result = !IsServer() || players[consoleplayer].settings_controller;

        // Check the dependency if it's non-null.
        if (_dependency != null)
        {
            result = result && _dependency.Check(self);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_Setting::GetPlayValue
    //
    // Returns the value of this setting in the play scope.
    // For user settings, the player argument must be specified.
    //
    //===========================================================================
    play RR_SettingValue GetPlayValue(int playerNumber = -1) const
    {
        return _playValueCache.GetPostprocessedValue(self, _valueProvider.GetPlayValue(playerNumber));
    }

    //===========================================================================
    //
    // RR_Setting::GetUiValue
    //
    // Returns the value of this setting in the UI scope.
    // This can be used for all types of settings without any arguments.
    //
    //===========================================================================
    ui RR_SettingValue GetUiValue() const
    {
        return _uiValueCache.GetPostprocessedValue(self, _valueProvider.GetUiValue());
    }

    //===========================================================================
    //
    // RR_Setting::GetCvarValue
    //
    // Returns the current value of the CVar that corresponds to this setting.
    // This is not necessarily the current setting value.
    // Used for UI purposes only.
    //
    //===========================================================================
    ui RR_SettingValue GetCvarValue() const
    {
        if (_cachedUiCvarValue == null)
        {
            let cvar = RR_CvarHelper.MustFindCvar(_valueProvider.GetCVarName());
            _cachedUiCvarValue = RR_SettingValue.FromCvarDynamic(cvar);
        }

        return _cachedUiCvarValue;
    }

    //===========================================================================
    //
    // RR_Setting::GetValueAsString
    //
    // Returns the provided value as a human-readable string in the context
    // of this setting. If the value is null, the current value is used instead.
    //
    //===========================================================================
    ui string GetValueAsString(RR_SettingValue value = null) const
    {
        if (value == null)
        {
            value = GetUiValue();
        }

        return ValueToString(value);
    }

    //===========================================================================
    //
    // RR_Setting::ValueToString
    //
    // When overridden in a derived class, converts the provided value
    // to a human-readable string in the context of this setting.
    //
    //===========================================================================
    protected abstract ui string ValueToString(RR_SettingValue value) const;

    //===========================================================================
    //
    // RR_Setting::GetMenuItem
    //
    // Returns an interactive menu item to control this setting.
    //
    //===========================================================================
    abstract ui OptionMenuItem GetMenuItem() const;

    //===========================================================================
    //
    // RR_Setting::ClearCache
    //
    // Clears any cached values to prevent junk data from persisting.
    //
    //===========================================================================
    void ClearCache()
    {
        _valueProvider.ClearCache();
        _uiValueCache.Clear();
        _playValueCache.Clear();
    }

    //===========================================================================
    //
    // RR_Setting::PostprocessValue
    //
    // Changes the resulting value according to the restrictions
    // imposed by this setting.
    //
    //===========================================================================
    virtual RR_SettingValue PostprocessValue(RR_SettingValue value) const
    {
        return value;
    }
}
