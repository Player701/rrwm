//===========================================================================
//
// RR_SettingSwitch
//
// A type of RRWM setting which allows only two values: enabled and disabled
// (or "Yes" and "No"). It's subclassed from RR_SettingOption because
// it is used more often than the more-generic option type setting.
//
//===========================================================================
class RR_SettingSwitch : RR_SettingOption
{
    //===========================================================================
    //
    // RR_SettingSwitch::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_SettingSwitch Create(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        let setting = new('RR_SettingSwitch');

        setting.Init(
            valueProvider,
            category, gameplayImpact,
            visibilityFlags, dependency
        );

        return setting;
    }

    //===========================================================================
    //
    // RR_SettingSwitch::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(
        RR_ValueProvider valueProvider,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        Super.Init(
            valueProvider,
            'YesNo',
            category, gameplayImpact,
            visibilityFlags, dependency
        );
    }

    //===========================================================================
    //
    // RR_SettingSwitch::GetFriendlyTypeName
    //
    // Returns the friendly type name for this setting.
    //
    //===========================================================================
    override string GetFriendlyTypeName()
    {
        return "$RR_MENU_SETTING_TYPE_SWITCH";
    }

    //===========================================================================
    //
    // RR_SettingSwitch::PostprocessValue
    //
    // Unlike in RR_SettingOption, we don't have to explicitly check
    // the option values here.
    //
    //===========================================================================
    override RR_SettingValue PostprocessValue(RR_SettingValue value)
    {
        return value;
    }
}
