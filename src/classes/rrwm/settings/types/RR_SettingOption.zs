//===========================================================================
//
// RR_SettingOption
//
// A type of RRWM setting which is an option with a set of predefined values
// to choose from.
//
//===========================================================================
class RR_SettingOption : RR_Setting
{
    // The option values list to use for this setting's menu item.
    private RR_OptionValues _optionValues;

    static const string OPTVAL_PREFIXES[] = { "$RR_MENU_OPTION_", "$OPTVAL_", "$TXT_" };

    //===========================================================================
    //
    // RR_SettingOption::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_SettingOption Create(
        RR_ValueProvider valueProvider,
        name optionValues,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        let setting = new('RR_SettingOption');

        setting.Init(
            valueProvider,
            optionValues,
            category, gameplayImpact,
            visibilityFlags, dependency
        );

        return setting;
    }

    //===========================================================================
    //
    // RR_SettingOption::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(
        RR_ValueProvider valueProvider,
        name optionValues,
        E_RR_SettingCategory category, E_RR_SettingGameplayImpact gameplayImpact,
        E_RR_SettingVisibilityFlags visibilityFlags, RR_SettingDependency dependency
    )
    {
        Super.Init(
            valueProvider,
            category, gameplayImpact,
            visibilityFlags, dependency
        );

        _optionValues = RR_OptionValues.Create(optionValues);
    }

    //===========================================================================
    //
    // RR_SettingOption::GetOptionValues
    //
    // Returns the option values list for this setting.
    //
    //===========================================================================
    RR_OptionValues GetOptionValues() const
    {
        return _optionValues;
    }

    //===========================================================================
    //
    // RR_SettingOption::GetFriendlyTypeName
    //
    // Returns the friendly type name for this setting.
    //
    //===========================================================================
    override string GetFriendlyTypeName() const
    {
        return "$RR_MENU_SETTING_TYPE_OPTION";
    }

    //===========================================================================
    //
    // RR_SettingOption::GetBaseDescription
    //
    // If this setting has a description header defined in LANGUAGE,
    // generates a description from that header and the option values,
    // pulling a separate description string for each value.
    // Otherwise, returns the normal description.
    //
    //===========================================================================
    override string GetBaseDescription() const
    {
        // Check if we have a description header
        string header = GetStringLabel("DESCRIPTION_HEADER");
        string headerLocalized;
        string result;

        bool hasHeader = RR_StringTools.TryLocalize(header, headerLocalized);
        bool addSimpleValueList;

        if (hasHeader)
        {
            result = headerLocalized;
        }
        else
        {
            // No header - use normal description, add a simple value list at the end
            result = Super.GetBaseDescription();
            addSimpleValueList = GetUiValue().GetType() != RR_SVT_BOOL;

            if (addSimpleValueList)
            {
                result = string.Format(
                    "%s\n\n%s:",
                    StringTable.Localize(result),
                    StringTable.Localize("$RR_MENU_HELP_DESCRIPTION_VALUE_LIST")
                );
            }
        }

        int cnt = _optionValues.GetCount();

        for (int i = 0; i < cnt; i++)
        {
            int val = _optionValues.GetValue(i);
            string label = _optionValues.GetText(i);
            string optionText = StringTable.Localize(label);

            if (hasHeader)
            {
                // If we have a header, build a description based on our option values
                string descSuffix;

                // Check if we can pull a nicely-named label
                bool hasPrefix = false;
                int m = OPTVAL_PREFIXES.Size();

                for (int j = 0; j < m; j++)
                {
                    string suffix;

                    if (RR_StringTools.TryStripPrefix(label, OPTVAL_PREFIXES[j], suffix))
                    {
                        descSuffix = string.Format("DESCRIPTION_VALUE_%s", suffix);
                        hasPrefix = true;
                        break;
                    }
                }

                // If not, resort to a not-so-nicely-named label with a number
                if (!hasPrefix)
                {
                    descSuffix = string.Format("DESCRIPTION_VALUE_%d", val);
                }

                // Get the full description text for the current option value
                string desc = StringTable.Localize(GetStringLabel(descSuffix));

                // Add a warning to it (if applicable)
                desc = AddWarning(desc, descSuffix, " ");

                // Get the full description text for the current option value
                // including the value itself
                string optionDesc = string.Format(
                    "%s[%d] %s%s:%s %s",
                    TEXTCOLOR_TEAL,
                    val,
                    TEXTCOLOR_GREEN,
                    optionText,
                    TEXTCOLOR_NORMAL,
                    desc
                );

                // Add the description text for this option value to the result value
                result = string.Format("%s\n\n%s", result, optionDesc);
            }
            else if (addSimpleValueList)
            {
                // Simply add each value and its option text to the list
                result = string.Format(
                    "%s\n%s[%d] %s%s%s",
                    result,
                    TEXTCOLOR_TEAL,
                    val,
                    TEXTCOLOR_GREEN,
                    optionText,
                    TEXTCOLOR_NORMAL
                );
            }
        }

        return result;
    }

    //===========================================================================
    //
    // RR_SettingOption::ValueToString
    //
    // Returns the corresponding option string for the provided value.
    //
    //===========================================================================
    override string ValueToString(RR_SettingValue value) const
    {
        return value.AsOptionString(_optionValues);
    }

    //===========================================================================
    //
    // RR_SettingOption::GetMenuItem
    //
    // Returns a menu item to control this setting.
    //
    //===========================================================================
    override OptionMenuItem GetMenuItem() const
    {
        return RR_MenuItemOption.Create(self);
    }

    //===========================================================================
    //
    // RR_SettingOption::PostprocessValue
    //
    // Restricts the raw value to one of the allowed option values.
    //
    //===========================================================================
    override RR_SettingValue PostprocessValue(RR_SettingValue value) const
    {
        int intValue = value.GetInt();
        int cnt = _optionValues.GetCount();

        // Make sure the value is included in the options list.
        int bestValue;

        for (int i = 0; i < cnt; i++)
        {
            int curValue = _optionValues.GetValue(i);

            if (curValue == intValue)
            {
                // everything's OK
                return value;
            }

            // Find the value closest to the provided one
            if (i == 0 || Abs(intValue - curValue) < Abs(intValue - bestValue))
            {
                bestValue = curValue;
            }
        }

        return RR_SettingValue.CreateInt(bestValue);
    }
}
