//===========================================================================
//
// RR_PowerAdrenaline
//
// Primary powerup class given by RR_Adrenaline pickups.
// This item provides visual effects and is also checked for by RR_Fist
// to boost the punching speed.
//
//===========================================================================
class RR_PowerAdrenaline : RR_AdrenalinePowerupBase
{
    Default
    {
        Powerup.Color "00 00 ff", 0.25;
        RR_Powerup.HudColor 'Blue';
    }

    //===========================================================================
    //
    // RR_PowerAdrenaline::TryPickup
    //
    // Gives additional powerups if pickup succeeds.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        bool result = Super.TryPickup(toucher);

        if (result)
        {
            // If the pickup succeeds, give the strength and protection powerups.
            toucher.GiveInventoryType('RR_PowerStrength');
            toucher.GiveInventoryType('RR_PowerAdrenalineProtection');

            // If the player already has a strength powerup,
            // move it so that the HUD icon won't jump around
            // when the time expires.
            let str = RR_PowerStrength(toucher.FindInventory('RR_PowerStrength'));

            if (str != null)
            {
                str.Reposition();
            }
        }

        return result;
    }
}
