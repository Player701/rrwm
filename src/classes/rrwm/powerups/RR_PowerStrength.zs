//===========================================================================
//
// RR_PowerStrength
//
// Indicates that the owner's fists attacks are now powered up.
// This works similar to PowerStrength, but can be limited by duration
// depending on the global settings.
//
//===========================================================================
class RR_PowerStrength : RR_AdrenalinePowerupBase
{
    Default
    {
        RR_Powerup.HudAlpha 0.5;
    }

    //===========================================================================
    //
    // RR_PowerStrength::Tick
    //
    // Prevents the powerup from running out unless the time limit is enabled
    // in global settings.
    //
    //===========================================================================
    override void Tick()
    {
        if (RR_Settings.InPlay(RR_SETTING_LIMITBERSERK).IsOff())
        {
            // Compensate for effect tics decrementing.
            // Not sure why the original PowerStrength adds 2 tics.
            // It makes them constantly increase - whish also works, of course,
            // but seems redundant to me.
            EffectTics++;
        }

        Super.Tick();
    }

    //===========================================================================
    //
    // RR_PowerStrength::RepositionAfter
    //
    // Repositions the item to succeed the main Adrenaline powerup.
    // This is needed so that the HUD icon won't jump around
    // when the powerup's time expires.
    //
    //===========================================================================
    override bool RepositionAfter(Inventory item)
    {
        return item is 'RR_PowerAdrenaline';
    }

    //===========================================================================
    //
    // RR_PowerStrength::IsVisibleInHud
    //
    // The Berserk powerup is not drawn in the HUD if the owner has
    // the main Adrenaline powerup.
    //
    //===========================================================================
    override bool IsVisibleInHud() const
    {
        return Owner != null && !Owner.FindInventory('RR_PowerAdrenaline');
    }

    //===========================================================================
    //
    // RR_PowerStrength::IsTimeVisibleInHud
    //
    // The Berserk powerup doesn't display its time in the HUD, even if it's
    // time-limited, as the main Adrenaline powerup has the same duration.
    //
    //===========================================================================
    override bool IsTimeVisibleInHud() const
    {
        return false;
    }
}
