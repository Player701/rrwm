//===========================================================================
//
// RR_PowerAdrenalineProtection
//
// Provides unconditional 50% damage protection.
//
//===========================================================================
class RR_PowerAdrenalineProtection : RR_AdrenalinePowerupBase
{
    Default
    {
        // Do not show this powerup in the HUD
        Inventory.Icon "";
    }

    //===========================================================================
    //
    // RR_PowerAdrenalineProtection::AttachToOwner
    //
    // After pickup has succeeded, repositions the item in the inventory chain.
    //
    //===========================================================================
    override void AttachToOwner(Actor other)
    {
        Super.AttachToOwner(other);

        // At this point, we have attached to the owner,
        // so we can reposition ourselves.
        Reposition();
    }

    //===========================================================================
    //
    // RR_PowerAdrenalineProtection::RepositionAfter
    //
    // The protection powerup must follow any BasicArmor and HexenArmor items
    // as well as the super armor's shield in the inventory chain, since
    // the protection effect only kicks in after the damage has been absorbed
    // by armor.
    //
    //===========================================================================
    override bool RepositionAfter(Inventory item)
    {
        let cls = item.GetClass();
        return cls == 'BasicArmor' || cls == 'HexenArmor' || cls == 'RR_SuperArmorShieldPickup';
    }

    //===========================================================================
    //
    // RR_PowerAdrenalineProtection::AbsorbDamage
    //
    // Cuts all incoming damage in half, but never reduces it to zero.
    //
    //===========================================================================
    override void AbsorbDamage(int damage, Name damageType, out int newdamage)
    {
        if (damage > 0)
        {
            // Make sure we always get at least 1 damage.
            newdamage = Max(1, damage / 2);
        }
    }
}
