//===========================================================================
//
// RR_AdrenalinePowerupBase
//
// Base class for powerups given by the Adrenaline pickup,
//
//===========================================================================
class RR_AdrenalinePowerupBase : RR_Powerup abstract
{
    Default
    {
        Inventory.Icon "ADRNB0";
        +INVENTORY.HUBPOWER;
        +INVENTORY.ADDITIVETIME;

        Powerup.Duration -30;
    }

    //===========================================================================
    //
    // RR_AdrenalinePowerupBase::RepositionAfter
    //
    // Checks if the powerup should reposition itself after the provided item.
    //
    //===========================================================================
    protected virtual bool RepositionAfter(Inventory item)
    {
        return false;
    }

    //===========================================================================
    //
    // RR_AdrenalinePowerupBase::Reposition
    //
    // Repositions the powerup in the inventory chain so that it is directly
    // preceded by the last item for which RepositionAfter returns true.
    //
    //===========================================================================
    protected void Reposition()
    {
        // Find the last item to position ourselves after in the inventory chain.
        Inventory newPrev = null;
        let ii = Owner.Inv;

        while (ii != null)
        {
            if (RepositionAfter(ii))
            {
                newPrev = ii;
            }

            ii = ii.Inv;
        }

        // Have we found anything?
        if (newPrev == null || newPrev.Inv == self)
        {
            // We're already there
            return;
        }

        // Rearrange the link between the previous item and us
        if (Owner.Inv == self)
        {
            // Nothing before us, so set the owner's first item
            // to the next item in the chain.
            Owner.Inv = Inv;
        }
        else
        {
            // Find the item that directly precedes us
            let prev = Owner.Inv;

            while (prev != null && prev.Inv != self)
            {
                prev = prev.Inv;
            }

            prev.Inv = Inv;
        }

        // Set up the remaining links
        Inv = newPrev.Inv;
        newPrev.Inv = self;
    }
}
