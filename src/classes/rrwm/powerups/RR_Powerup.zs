//===========================================================================
//
// RR_Powerup
//
// Base class for RRWM powerups.
// Currently used only by PowerAdrenaline to provide HUD color.
//
//===========================================================================
class RR_Powerup : Powerup abstract
{
    // HUD color of this powerup.
    // This color will be used to draw the number of time remaining.
    meta name HudColor;

    // HUD alpha value of this powerup.
    // Applied to both the icon and the time counter.
    meta double HudAlpha;

    property HudColor : HudColor;
    property HudAlpha : HudAlpha;

    Default
    {
        self.HUDAlpha 1;
    }

    //===========================================================================
    //
    // RR_Powerup::GetPowerupIcon
    //
    // Returns the normal icon if the powerup is visible,
    // otherwise returns an invalid (missing) icon.
    //
    //===========================================================================
    override TextureID GetPowerupIcon() const
    {
        if (IsVisibleInHud())
        {
            return Super.GetPowerupIcon();
        }

        TextureID missingIcon;
        missingIcon.SetNull();

        return missingIcon;
    }

    //===========================================================================
    //
    // RR_Powerup::IsVisibleInHud
    //
    // Returns whether this powerup should be drawn in the HUD right now.
    //
    //===========================================================================
    protected virtual bool IsVisibleInHud() const
    {
        return true;
    }

    //===========================================================================
    //
    // RR_Powerup::IsTimeVisibleInHud
    //
    // Returns whether this powerup's time should be drawn in the HUD right now.
    //
    //===========================================================================
    virtual bool IsTimeVisibleInHud() const
    {
        return true;
    }
}
