//===========================================================================
//
// RR_ChainsawSoundPlayer
//
// Used to play sounds for the Chainsaw.
//
// Perhaps this is a kind of a hack, but we have to circumvent this behavior:
// when the chainsaw is picked up, the BecomeItem method stops all sounds
// that were playing. We could restart the sound manually, but then it would
// start playing from the beginning and not from the middle of the loop
// it was in. Therefore, we opt for this more robust solution, even if it
// has to involve an extra actor.
//
// NB! In multiplayer, we have to fall back to restarting the sounds
// as playing them from another actor doesn't mix well with movement
// prediction. To avoid extra code in RR_Chainsaw, this actor is also used
// to route the sounds to its parent (the player or the weapon itself,
// depending on whether the weapon is on the ground or in the inventory)
// in multiplayer games.
//
//===========================================================================
class RR_ChainsawSoundPlayer : Actor
{
    // The channel where all sounds are played.
    const PLAYER_CHANNEL = RR_SoundChannels.CHAN_CHAINSAW;

    Default
    {
        +NOINTERACTION;
    }

    // The number of ticks left before the current sound has finished.
    private int _timer;

    // Are we playing a looping sound right now?
    private bool _isLooping;

    // The sound which is currently looping.
    private sound _loopingSound;

    // The source that is currently playing the looping sound.
    private Actor _loopingSource;

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::IsLooping
    //
    // Checks whether a looping sound effect is currently playing.
    //
    //===========================================================================
    private bool IsLooping(sound snd = -1)
    {
        return _isLooping && (snd == -1 || _loopingSound == snd);
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::GetSoundSource
    //
    // Returns the actor to use as the source for all sounds.
    //
    //===========================================================================
    private Actor GetSoundSource()
    {
        return netgame && Target is 'PlayerPawn' ? Target : Actor(self);
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::Tick
    //
    // Performs the necessary actions that have to be done each tick.
    //
    //===========================================================================
    override void Tick()
    {
        if (Target == null)
        {
            Destroy();
        }
        else
        {
            // Stick to the target
            SetOrigin(Target.Pos, false);

            // Alert monsters if we're playing a looping sound
            if (IsLooping())
            {
                A_AlertMonsters();
            }
        }
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::SetParent
    //
    // Updates the parent this sound player is attached to.
    //
    //===========================================================================
    void SetParent(Actor newParent)
    {
        if (newParent == null)
        {
            ThrowAbortException("%s: New parent cannot be null.", GetClassName());
        }

        if (newParent == Target)
        {
            // No change necessary
            return;
        }

        // Set the new parent and move to it immediately
        Target = newParent;
        SetOrigin(Target.Pos, false);

        // Check if we're currently playing a looping sound
        // and the source has changed.
        if (IsLooping() && _loopingSource != GetSoundSource())
        {
            // Stop the currently looping sound at the old source.
            if (_loopingSource != null)
            {
                _loopingSource.A_StopSound(PLAYER_CHANNEL);
            }

            // Restart the currently looping sound at the new source.
            StartSound(_loopingSound, true);
        }
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::OnDestroy
    //
    // If playing a looping sound, stops it before destroying the actor.
    //
    //===========================================================================
    override void OnDestroy()
    {
        // Note that in multiplayer, the actual source playing the sound
        // may be different, so it needs to be stopped manually.
        if (IsLooping())
        {
            let src = GetSoundSource();

            if (src != null)
            {
               src.A_StopSound(PLAYER_CHANNEL);
            }
        }

        Super.OnDestroy();
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::PlaySound
    //
    // Plays the specified sound once, or loops it indefinitely.
    //
    //===========================================================================
    void PlaySound(sound snd, bool looping)
    {
        // Do not override a looping sound with the same looping sound,
        // but rather let it keep playing normally.
        if (!IsLooping(snd))
        {
            // Start playing now
            StartSound(snd, looping);
        }
    }

    //===========================================================================
    //
    // RR_ChainsawSoundPlayer::StartSound
    //
    // Starts the sound in either normal or looping mode.
    //
    //===========================================================================
    private void StartSound(sound snd, bool looping)
    {
        // Play now
        let src = GetSoundSource();

        if (src != null)
        {
            src.A_StartSound(snd, PLAYER_CHANNEL, looping ? CHANF_LOOPING : 0);
            A_AlertMonsters();
        }

        _isLooping = looping;

        if (looping)
        {
            // If we're looping, we need to know what and who's doing it.
            _loopingSound = snd;
            _loopingSource = src;
        }
    }
}
