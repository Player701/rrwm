//===========================================================================
//
// RR_SoundChannels
//
// Contains sound channel numbers used in RRWM.
//
//===========================================================================
class RR_SoundChannels
{
    // Base channel number for playing item pickup sounds.
    // Different sounds will get different channel numbers assigned to them
    // by incrementing this value.
    const CHAN_ITEM_BASE = 485911971;

    // Defines the sound channel reserved for extra weapon sounds.
    // Main sounds (i.e. weapon fire) are played on CHAN_WEAPON.
    // Everything else (dry-fire, reloading and the like) is played on this channel.
    const CHAN_WEAPON_AUX = 361631386;

    // Additional channel to play pistol laser sight on/off sound.
    const CHAN_PISTOL_LASER = 513655660;

    // Used to play most of the chainsaw sounds.
    // Avoids potential interference in multiplayer
    // when the player pawn actor is used as a source.
    const CHAN_CHAINSAW = 223409809;

    // Used to play the chainsaw protection sound effect.
    const CHAN_CHAINSAW_SHIELD = 34589076;

    // Used to play the chainsaw upgrade sound effect.
    const CHAN_CHAINSAW_UPGRADE = 8024943354;

    // Used to play sound effects when damage is absorbed by the Super Armor.
    const CHAN_ARMOR_SHIELD = 17085857;

    // Used to play an additional sound when a computer map powerup
    // is picked up.
    const CHAN_ALLMAP_AUX = 155625852;

    // Used to play an additional sound when an infrared powerup
    // is picked up.
    const CHAN_INFRARED_AUX = 987583381;
}
