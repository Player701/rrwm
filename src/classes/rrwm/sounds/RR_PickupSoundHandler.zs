//===========================================================================
//
// RR_PickupSoundHandler
//
// Tracks sound channels to play item pickup sounds without overlapping.
// Identical sounds will override each other, different sounds will overlap.
//
//===========================================================================
class RR_PickupSoundHandler : Inventory
{
    // Maps sounds to channels
    private Map<sound, int> _channelMap;

    Default
    {
        +INVENTORY.UNDROPPABLE;
    }

    //===========================================================================
    //
    // RR_PickupSoundHandler::Get
    //
    // Returns the instance of RR_PickupSoundHandler attached to the provided
    // source actor.
    //
    //===========================================================================
    static RR_PickupSoundHandler Get(Actor source)
    {
        // Find the pickup sound handler.
        let handler = RR_PickupSoundHandler(source.FindInventory('RR_PickupSoundHandler'));

        if (handler == null)
        {
            // There is no handler yet, so add it.
            handler = RR_PickupSoundHandler(Spawn('RR_PickupSoundHandler'));

            if (!handler.CallTryPickup(source))
            {
                // Whoops
                handler.Destroy();
                handler = null;
            }
        }

        return handler;
    }

    //===========================================================================
    //
    // RR_PickupSoundHandler::PlayPickupSound
    //
    // Picks a suitable channel and plays the pickup sound of the provided item
    // using the owner as the source.
    //
    //===========================================================================
    void PlayPickupSound(Inventory item)
    {
        if (Owner == null)
        {
            return;
        }

        // Determine the sound parameters.
        int flags = Owner.CheckLocalView() ? CHAN_NOPAUSE : 0;
        double atten = item.bNoAttenPickupSound ? ATTN_NONE : ATTN_NORM;

        // Find the proper channel and play the sound.
        let snd = item.PickupSound;
        Owner.A_StartSound(snd, FindChannel(snd), flags|CHAN_MAYBE_LOCAL, 1, atten);
    }

    //===========================================================================
    //
    // RR_PickupSoundHandler::FindChannel
    //
    // Finds a suitable channel for the provided sound,
    // creating a new one if necessary.
    //
    //===========================================================================
    private int FindChannel(sound snd)
    {
        let [channelNum, hasChannel] = _channelMap.CheckValue(snd);

        // If there is no channel for this specific sound,
        // map a new one.
        if (!hasChannel)
        {
            channelNum = _channelMap.CountUsed() + RR_SoundChannels.CHAN_ITEM_BASE;
            _channelMap.Insert(snd, channelNum);
        }

        return channelNum;
    }
}
