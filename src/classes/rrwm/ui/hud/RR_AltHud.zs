//===========================================================================
//
// RR_AltHud
//
// Implements alternative HUD extensions for RRWM.
//
//===========================================================================
class RR_AltHud : AltHud
{
    //===========================================================================
    //
    // RR_AltHud::DrawAmmo
    //
    // Draws the clip counter in addition to the normal ammo table.
    //
    //===========================================================================
    override int DrawAmmo(PlayerInfo CPlayer, int x, int y)
    {
        // Draw the ammo table (this will also ensure the table data is present)
        int result = Super.DrawAmmo(CPlayer, x, y);

        let rel = RR_ReloadableWeapon(CPlayer.ReadyWeapon);

        if (rel != null)
        {
            // OK, this code is copy-pasted from AltHud.
            // Unfortunately, there doesn't seem to be a better solution,
            // but at least we've got something!

            let [ammocurlen, ammomaxlen] = GetAmmoTextLengths(CPlayer);

            string buf = string.Format("%0*d/%0*d", ammocurlen, 0, ammomaxlen, 0);
            int def_width = ConFont.StringWidth(buf);

            int clipAmount = rel.GetClipAmount();
            int len = GetDigitCount(clipAmount) * HudFont.GetCharWidth("0");

            x -= def_width + 25 + len;

            if (hud_ammo_order > 0)
            {
                x -= 4;
            }

            y += (ConFont.GetHeight() + HudFont.GetHeight()) / 2;

            for (int i = orderedammos.Size() - 1; i >= 0; i--)
            {
                if (orderedammos[i] == rel.Ammo1.GetClass())
                {
                    DrawHudNumber(HudFont, RR_HudStyleContext.GetAmmoColor(rel.Ammo1), clipAmount, x, y);
                    break;
                }

                y -= 10;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // RR_AltHud::DrawArmor
    //
    // If the player has the super armor in a separate slot, draws
    // the cumulative amount of the shield and all other armors as well as
    // the shield icon. Otherwise, draws armor as normal.
    //
    //===========================================================================
    override void DrawArmor(BasicArmor barmor, HexenArmor harmor, int x, int y)
    {
        let ownedItem = barmor != null ? Inventory(barmor) : Inventory(harmor);

        // Unlikely that both items are null, but there doesn't seem to be a way
        // to get the player reference directly in this method.
        if (ownedItem != null)
        {
            let shield = ownedItem.Owner.FindInventory('RR_SuperArmorShieldPickup');

            if (shield != null && shield.Amount > 0)
            {
                int total = GetTotalArmorAmount(barmor, harmor, shield);

                // Always use the primary icon here,
                // same as with BasicArmor and armor pickups.
                DrawImageToBox(shield.Icon, x, y, 31, 17);

                // The armor color is always red, even though this may not
                // be the case when this armor shares the primary slot.
                // This is because we want to show the fact that this armor
                // is independent from any other armor that may also be present.
                DrawHudNumber(HudFont, Font.CR_RED, total, x + 33, y + 17);

                return;
            }
        }

        Super.DrawArmor(barmor, harmor, x, y);
    }

    //===========================================================================
    //
    // RR_AltHud::GetTotalArmorAmount
    //
    // Returns the total amount of armor the player has.
    //
    //===========================================================================
    private static int GetTotalArmorAmount(BasicArmor barmor, HexenArmor harmor, Inventory shield)
    {
        int total = shield.Amount;

        // If there is any BasicArmor, display its amount as well.
        if (barmor != null)
        {
            total += barmor.Amount;
        }

        // If there is any HexenArmor, display its amount as well.
        if (harmor != null)
        {
            double sum = 0;

            // NB: we count all 5 slots here, unlike in RRWM HUD,
            // because this is what AltHUD does.
            for (int i = 0; i < 5; i++)
            {
                sum += harmor.Slots[i];
            }

            total += int(sum);
        }

        return total;
    }
}
