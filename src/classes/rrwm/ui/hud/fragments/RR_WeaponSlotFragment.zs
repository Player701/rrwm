//===========================================================================
//
// RR_WeaponSlotFragment
//
// Represents an indicator for a single weapon slot.
//
//===========================================================================
class RR_WeaponSlotFragment : RR_HudFragment
{
    private int _slot;
    private WL_Counter _slotNum;
    private RR_WeaponSlotAdapter _adapter;

    //===========================================================================
    //
    // RR_WeaponSlotFragment::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_WeaponSlotFragment Create(int slot, RR_HudStyleContext context)
    {
        let f = new('RR_WeaponSlotFragment');
        f.Init(slot, context);

        return f;
    }

    //===========================================================================
    //
    // RR_WeaponSlotFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(int slot, RR_HudStyleContext context)
    {
        _slot = slot;
        Super.Init(context);
    }

    //===========================================================================
    //
    // RR_WeaponSlotFragment::CreateView
    //
    // Creates a view for this fragment to display.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let lv = WL_ListView.Create();
        lv.SetAdapter((_adapter = RR_WeaponSlotAdapter.Create(_slot)));
        lv.SetLayoutManager(WL_LinearLayoutManager.Create(WL_O_Vertical));

        _slotNum = GetStyleContext().CreateTinyCounter();
        _slotNum.SetValue(_slot);
        _slotNum.SetPadding(WL_Margins.All(2));
        // The list view is displayed in the background of the slot counter
        _slotNum.SetBackgroundView(lv);

        return _slotNum;
    }

    //===========================================================================
    //
    // RR_WeaponSlotFragment::Update
    //
    // Updates the state of this fragment.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        _adapter.Update(context);
        bool result = _adapter.HasAssignedWeapons();

        if (result)
        {
            // Make sure the list view is updated.
            _adapter.NotifyDataChanged();

            // Set the corresponding color for this slot.
            bool hasWeapons = _adapter.HasActualWeapons();
            _slotNum.SetColor(hasWeapons ? (_adapter.IsSelected() ? Font.CR_GOLD : Font.CR_GREY) : Font.CR_DARKGRAY);
        }

        return result;
    }
}
