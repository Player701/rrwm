//===========================================================================
//
// RR_AmmoFragment
//
// Displays the ammo counters.
//
//===========================================================================
class RR_AmmoFragment : RR_HudFragment
{
    private WL_Counter _cntClip;
    private WL_VisibilityChanger _visClip;

    private WL_Counter _cntAmmo;
    private WL_TextureView _iconAmmo;
    private WL_TextColorChanger _colorClip;

    private WL_Counter _cntAltAmmo;
    private WL_TextureView _iconAltAmmo;

    private WL_TableRow _rowAmmo;
    private WL_TableRow _rowAltAmmo;

    //===========================================================================
    //
    // RR_AmmoFragment::Create
    //
    // Creates a new instance of the RR_AmmoFragment class.
    //
    //===========================================================================
    static RR_AmmoFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_AmmoFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_AmmoFragment::CreateView
    //
    // Creates the view hierarchy for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let context = GetStyleContext();

        // As with health/armor, we use a TableLayout.
        let tl = context.CreateTable(true, true);

        // These are layout parameters for icons
        let iconParams = WL_FrameLayout.CreateParams(
            margins: WL_Margins.Left(RR_HudStyleContext.ICON_MARGIN)
        );

        // This is the row for primary ammo.
        _rowAmmo = context.CreateRow();

        // Icon
        _iconAmmo = context.CreateBigIcon(iconParams, true);
        _rowAmmo.AddView(context.WrapBigIcon(_iconAmmo));

        // Counter
        _rowAmmo.AddView((_cntAmmo = context.CreateBigCounter()));

        // Clip counter w/delimiter
        let slash = context.CreateBigDelimiter();
        _rowAmmo.AddView(slash);
        _rowAmmo.AddView((_cntClip = context.CreateBigCounter()));

        // Group the slash and the clip counter to toggle their visibility
        // with the VisibilityChanger.
        _visClip = WL_VisibilityChanger.Create();
        _visClip.ObserveView(slash);
        _visClip.ObserveView(_cntClip);

        // Setup the color changer for the clip counter.
        _colorClip = WL_TextColorChanger.Create();
        _colorClip.ObserveView(slash);
        _colorClip.ObserveView(_cntClip);

        tl.AddView(_rowAmmo);

        // This is the row for alternate ammo.
        _rowAltAmmo = context.CreateRow();

        // Icon
        _iconAltAmmo = context.CreateBigIcon(iconParams, true);
        _rowAltAmmo.AddView(context.WrapBigIcon(_iconAltAmmo));

        // Counter
        _rowAltAmmo.AddView((_cntAltAmmo = context.CreateBigCounter()));

        tl.AddView(_rowAltAmmo);

        // Done.
        return tl;
    }

    //===========================================================================
    //
    // RR_AmmoFragment::Update
    //
    // Updates the ammo counters.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        let player = context.GetPlayer();
        let weap = player.ReadyWeapon;

        if (weap == null)
        {
            return false;
        }

        let ammo1 = weap.Ammo1;
        let ammo2 = weap.Ammo2;

        // Check what types of ammo we need to show.
        bool showAmmo = ammo1 != null;
        bool showAltAmmo = ammo2 != null && ammo2 != ammo1;

        _rowAmmo.SetVisibility(showAmmo ? WL_V_Visible : WL_V_Gone);
        _rowAltAmmo.SetVisibility(showAltAmmo ? WL_V_Visible : WL_V_Gone);

        // Check for infinite ammo
        bool isInfiniteAmmo = sv_infiniteammo || player.mo.FindInventory('PowerInfiniteAmmo');

        if (showAmmo)
        {
            UpdateAmmo(ammo1, isInfiniteAmmo, weap);
        }

        if (showAltAmmo)
        {
            UpdateAltAmmo(ammo2, isInfiniteAmmo);
        }

        return showAmmo || showAltAMmo;
    }

    //===========================================================================
    //
    // RR_AmmoFragment::UpdateAmmo
    //
    // Updates the primary ammo counter.
    //
    //===========================================================================
    private void UpdateAmmo(Ammo ammoitem, bool isInfiniteAmmo, Weapon weap)
    {
        UpdateAmmoRow(ammoitem, _iconAmmo, _cntAmmo, isInfiniteAmmo);

        // Check if we need to display the clip capacity.
        int clipAmount;
        int clipCapacity;

        if (RR_HudHooks.IsWeaponReloadable(weap, clipAmount, clipCapacity))
        {
            // Make the clip counter visible.
            _visClip.SetVisibility(WL_V_Visible);

            // Set clip value
            _cntClip.SetValue(clipAmount);
            _cntClip.MinDecimalPlacesFromValue(clipCapacity);

            // Set clip color
            _colorClip.SetColor(GetStyleContext().GetAmmoColor(ammoitem));
        }
        else
        {
            _visClip.SetVisibility(WL_V_Gone);
        }
    }

    //===========================================================================
    //
    // RR_AmmoFragment::UpdateAltAmmo
    //
    // Updates the alternate ammo fragment.
    //
    //===========================================================================
    private void UpdateAltAmmo(Ammo ammoitem, bool isInfiniteAmmo)
    {
        UpdateAmmoRow(ammoitem, _iconAltAmmo, _cntAltAmmo, isInfiniteAmmo);
    }

    //===========================================================================
    //
    // RR_AmmoFragment::UpdateAmmoRow
    //
    // Updates ammo amount, icon and color.
    //
    //===========================================================================
    private void UpdateAmmoRow(Ammo ammoitem, WL_TextureView icon, WL_Counter counter, bool isInfiniteAmmo)
    {
        // Set ammo icon
        icon.SetInventoryIcon(ammoitem, BaseStatusBar.DI_SKIPALTICON);

        // Set ammo value
        counter.SetValue(ammoitem.Amount);
        counter.MinDecimalPlacesFromValue(ammoitem.MaxAmount);

        // Set ammo color
        // NB: Do not use infinite ammo color
        // if this ammo type is not in ammo table at all
        // (special handling for chainsaw upgrade level)
        let clr = isInfiniteAmmo && RR_HudHooks.IsAmmoInTable(ammoitem.GetClass(), true)
            ? Font.CR_ICE
            : GetStyleContext().GetAmmoColor(ammoitem);

        counter.SetColor(clr);
    }
}
