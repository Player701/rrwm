//===========================================================================
//
// RR_LevelStatsFragment
//
// Displays level statistics (kills, items, secrets).
//
//===========================================================================
class RR_LevelStatsFragment : RR_HudFragment
{
    private Array<RR_LevelStatsCounter> _statCounters;
    private WL_FontFallbackCoordinator _fc;

    //===========================================================================
    //
    // RR_LevelStatsFragment::Create
    //
    // Creates a new instance of the RR_LevelStatsFragment class.
    //
    //===========================================================================
    static RR_LevelStatsFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_LevelStatsFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_LevelStatsFragment::CreateView
    //
    // Creates the view hierarchy for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        _fc = WL_FontFallbackCoordinator.Create();

        let context = GetStyleContext();
        let tl = context.CreateTable(false, false);

        // Add counters for all three statistics.
        AddStatCounter(RR_LevelStatsCounterKills.Create(), tl);
        AddStatCounter(RR_LevelStatsCounterItems.Create(), tl);
        AddStatCounter(RR_LevelStatsCounterSecrets.Create(), tl);

        return tl;
    }

    //===========================================================================
    //
    // RR_LevelStatsFragment::AddStatCounter
    //
    // Attaches the provided stat counter to the table
    // and adds it to the array so that it can be updated later.
    //
    //===========================================================================
    private void AddStatCounter(RR_LevelStatsCounter statCounter, WL_TableLayout table)
    {
        // Attach the stat counter to the table.
        statCounter.Attach(GetStyleContext(), table, _fc);

        // Add the stat counter to the array so that we will be able
        // to retrieve its values during update
        _statCounters.Push(statCounter);
    }

    //===========================================================================
    //
    // RR_LevelStatsFragment::Update
    //
    // Updates the level stats data in the HUD.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        // All counters are off in deathmatch games, no exceptions.
        if (deathmatch || teamplay)
        {
            return false;
        }

        let flags = GetUpdateFlags();
        bool result = false;

        foreach (statCounter : _statCounters)
        {
            result = statCounter.Update(context, flags) || result;
        }

        return result;
    }

    //===========================================================================
    //
    // RR_VitalsFragment::GetUpdateFlags
    //
    // Returns the flags to pass when updating each counter.
    //
    //===========================================================================
    private static RR_LevelStatsCounterUpdateFlags GetUpdateFlags()
    {
        RR_LevelStatsCounterUpdateFlags result = 0;

        // Toggle visibility of icons/prefixes based on the settings
        if (RR_Settings.InUi(RR_SETTING_HUD_STATICONS).IsOn())
        {
            result |= RR_LSCUF_USE_ICON;
        }

        // Toggle visibility of individual stats based on the settings
        if (multiplayer && RR_Settings.InUi(RR_SETTING_HUD_INDSTATS).IsOn())
        {
            result |= RR_LSCUF_SHOW_IND_VALUE;
        }

        // Set the style flags
        let style = RR_Settings.InUi(RR_SETTING_HUD_STATSTYLE).GetInt();
        bool isCombined = style == RR_HSS_CUR_PCT;

        if (isCombined || style == RR_HSS_CUR_MAX)
        {
            result |= RR_LSCUF_SHOW_CUR_VALUE;
        }

        if (isCombined || style == RR_HSS_PCT)
        {
            result |= RR_LSCUF_SHOW_PERCENTAGE;
        }

        return result;
    }
}
