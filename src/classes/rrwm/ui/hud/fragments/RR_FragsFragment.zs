//===========================================================================
//
// RR_FragsFragment
//
// Displays frags.
//
//===========================================================================
class RR_FragsFragment : RR_HudFragment
{
    private WL_Counter _cntFrags;

    //===========================================================================
    //
    // RR_FragsFragment::Create
    //
    // Creates a new instance of the RR_FragsFragment class.
    //
    //===========================================================================
    static RR_FragsFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_FragsFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_FragsFragment::CreateView
    //
    // Creates the view to display.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let context = GetStyleContext();

        let ll = WL_LinearLayout.Create();
        ll.SetGravity(WL_Gravity.Vertical(WL_VG_Center));
        ll.SetOrientation(WL_O_Horizontal);

        // Skull icon
        let iconParams = WL_LinearLayout.CreateParams(
            margins: WL_Margins.Right(RR_HudStyleContext.ICON_MARGIN)
        );

        let icon = context.CreateBigIcon(iconParams, true);
        icon.SetTextureName("RR_FRAGS");
        ll.AddView(icon);

        // Frags counter
        _cntFrags = context.CreateBigCounter();
        _cntFrags.SetColor(Font.CR_BROWN);
        ll.AddView(_cntFrags);

        return ll;
    }

    //===========================================================================
    //
    // RR_FragsFragment::Update
    //
    // Updates the state of this fragment.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        bool result = deathmatch || teamplay;

        if (result)
        {
            _cntFrags.SetValue(context.GetPlayer().FragCount);
        }

        return result;
    }
}
