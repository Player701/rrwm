//===========================================================================
//
// RR_WeaponSlotsFragment
//
// Displays information about the player's weapon slots.
//
//===========================================================================
class RR_WeaponSlotsFragment : RR_HudFragment
{
    const SLOT_COUNT = PlayerPawn.NUM_WEAPON_SLOTS;

    private RR_WeaponSlotFragment _slots[SLOT_COUNT];

    //===========================================================================
    //
    // RR_WeaponSlotsFragment::Create
    //
    // Creates a new instance of the RR_WeaponSlotsFragment class.
    //
    //===========================================================================
    static RR_WeaponSlotsFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_WeaponSlotsFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_WeaponSlotsFragment::CreateView
    //
    // Creates a view for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let ll = WL_LinearLayout.Create();
        ll.SetOrientation(WL_O_Horizontal);

        let context = GetStyleContext();

        for (int i = 0; i < SLOT_COUNT; i++)
        {
            int slot = i + 1;

            // Slot 0 should be the last
            if (slot == SLOT_COUNT)
            {
                slot = 0;
            }

            let f = _slots[i] = RR_WeaponSlotFragment.Create(slot, context);
            ll.AddView(f);
        }

        return ll;
    }

    //===========================================================================
    //
    // RR_WeaponSlotsFragment::Update
    //
    // Updates the state of this fragment.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        // Do not process if the weapon slots indicator is disabled
        if (RR_Settings.InUi(RR_SETTING_HUD_WEAPONSLOTS).IsOff())
        {
            return false;
        }

        let player = context.GetPlayer();
        int slotsVisible = 0;

        // Check all slots
        foreach (f : _slots)
        {
            if (f.Update(context))
            {
                f.SetVisibility(WL_V_Visible);
                slotsVisible++;
            }
            else
            {
                f.SetVisibility(WL_V_Gone);
            }
        }

        // In an (unlikely) event of no slot assignments whatsoever,
        // the indicator should be gone completely.
        return slotsVisible > 0;
    }
}
