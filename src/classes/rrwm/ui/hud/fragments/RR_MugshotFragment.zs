//===========================================================================
//
// RR_MugshotFragment
//
// Draws the mugshot.
//
//===========================================================================
class RR_MugshotFragment : RR_HudFragment
{
    private WL_TextureView _texMugshot;

    //===========================================================================
    //
    // RR_MugshotFragment::Create
    //
    // Creates a new instance of the RR_MugshotFragment class.
    //
    //===========================================================================
    static RR_MugshotFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_MugshotFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_MugshotFragment::CreateView
    //
    // Creates the view for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        _texMugshot = WL_TextureView.Create();

        _texMugshot.SetWidth(WL_Dimension.Fixed(34));
        _texMugshot.SetHeight(WL_Dimension.Fixed(38));

        return _texMugshot;
    }

    //===========================================================================
    //
    // RR_MugshotFragment::Update
    //
    // Draws the mugshot.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        bool result = RR_Hud.IsDoomLikeGame() && RR_Settings.InUi(RR_SETTING_HUD_MUGSHOT).IsOn();

        if (result)
        {
            _texMugshot.SetTexture(context.GetStatusBar().GetMugshot(5));
        }

        return result;
    }
}
