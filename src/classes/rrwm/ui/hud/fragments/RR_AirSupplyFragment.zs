//===========================================================================
//
// RR_AirSupplyFragment
//
// Displays the air supply.
//
//===========================================================================
class RR_AirSupplyFragment : RR_HudFragment
{
    private WL_Counter _cntAirSupply;
    private WL_TextColorChanger _clrAirSupply;

    //===========================================================================
    //
    // RR_AirSupplyFragment::Create
    //
    // Creates a new instance of the RR_AirSupplyFragment class.
    //
    //===========================================================================
    static RR_AirSupplyFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_AirSupplyFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_AirSupplyFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        Super.Init(context);

        // Air supply is not fullscreen-only
        SetFullscreenOnly(false);
    }

    //===========================================================================
    //
    // RR_AirSupplyFragment::CreateView
    //
    // Creates the view for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let context = GetStyleContext();

        let ll = WL_LinearLayout.Create();
        ll.SetGravity(WL_Gravity.Vertical(WL_VG_Center));

        // Text
        let tv = context.CreateBigText("$RR_HUD_AIR", localized: true);
        tv.SetPadding(WL_Margins.Right(8));
        tv.SetFormatter(WL_ColonAppender.Create());
        tv.SetLineTrimming(true);

        ll.AddView(tv);

        // Counter
        ll.AddView(_cntAirSupply = context.CreateBigCounter());

        // Wire up everything to the FontFallbackCoordinator and color changer.
        let fc = WL_FontFallbackCoordinator.Create();
        fc.ObservePrimitiveViews(ll);
        _clrAirSupply = WL_TextColorChanger.Create();
        _clrAirSupply.ObservePrimitiveViews(ll);

        return ll;
    }

    //===========================================================================
    //
    // RR_AirSupplyFragment::Update
    //
    // Updates the state of this fragment.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        let player = context.GetPlayer();

        double airCapacity = player.mo.AirCapacity;
        int airSupply = Level.airSupply;

        if (airSupply == 0 || airCapacity == 0)
        {
            // The player can't drown if either the level's air supply is 0
            // or the player's air capacity is 0 (p_user.cpp::ResetAirSupply).
            // Therefore, it is pointless to show the counter in these cases.
            return false;
        }

        // Normally, if the player is above water, we don't need to show the air supply,
        // as it is instantly restored to maximum.
        // However, what if CheckAirSupply is overridden so that it is not?
        // Then we will show the player that their air supply is being restored.
        int waterLevel = player.mo.waterlevel;
        int airTics = Max(0, player.air_finished - Level.maptime);

        // Do we have full air?
        // Subtracting 1 is necessary because the HUD is always updated
        // only on the next tic after the air supply is reset...
        bool hasFullAir = RR_Math.AreAlmostEqual(airTics, (airSupply * airCapacity) - 1);

        // Do not show the air time if we have full air AND are above water.
        // Also do not show if the player is dead and the cause of death wasn't drowning.
        if (hasFullAir && waterLevel < 3 || player.Health == 0 && player.LastDamageType != 'Drowning')
        {
            return false;
        }

        // In other cases, the counter is displayed.
        int airSeconds = RR_Math.TicsToSeconds(airTics);

        _cntAirSupply.SetValue(airSeconds);
        _clrAirSupply.SetColor(GetAirSupplyColor(airSeconds, context.GetPlayer()));

        return true;
    }

    //===========================================================================
    //
    // RR_AirSupplyFragment::GetAirSupplyColor
    //
    // Gets the current color of the air supply counter.
    //
    //===========================================================================
    private static int GetAirSupplyColor(int airSeconds, PlayerInfo player)
    {
        // If we have more than 5 seconds left, we're fine.
        if (airSeconds > 5)
        {
            return Font.CR_GREEN;
        }

        // Otherwise, alternate between red and yellow if we're at zero...
        // ...if we're dead, it's also red (to indicate it's truly fatal now).
        if (airSeconds == 0 && ((Level.maptime & 0x10) || player.Health == 0))
        {
            return Font.CR_RED;
        }

        // ...or it's just yellow if we aren't.
        return Font.CR_YELLOW;
    }
}
