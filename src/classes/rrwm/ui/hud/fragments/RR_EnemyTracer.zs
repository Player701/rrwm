//===========================================================================
//
// RR_EnemyTracer
//
// Finds the enemy in the player's crosshairs
// so that its health bar can be displayed.
//
//===========================================================================
class RR_EnemyTracer : LineTracer
{
    private PlayerInfo _curPlayer;

    //===========================================================================
    //
    // RR_EnemyTracer::Create
    //
    // Creates a new instance of the RR_EnemyTracer class.
    //
    //===========================================================================
    static RR_EnemyTracer Create()
    {
        return new('RR_EnemyTracer');
    }

    //===========================================================================
    //
    // RR_EnemyTracer::TraceEnemy
    //
    // Returns the enemy in the player's crosshairs, if any.
    //
    //===========================================================================
    Actor TraceEnemy(PlayerInfo player, double distance)
    {
        let pos = player.mo.Pos;
        let tracePos = (pos.X, pos.Y, player.ViewZ);
        let traceDir = RR_Math.GetRotatedOffset((1, 0, 0), player.mo);

        // Remember the current player so that we can trace through them.
        _curPlayer = player;

        if (Trace(tracePos, player.mo.CurSector, traceDir, distance, 0) && Results.HitType == TRACE_HitActor)
        {
            return Results.HitActor;
        }

        return null;
    }

    //===========================================================================
    //
    // RR_EnemyTracer::TraceCallback
    //
    // Traces through non-shootable actors.
    //
    //===========================================================================
    override ETraceStatus TraceCallback()
    {
        switch(Results.HitType)
        {
            // We hit an actor
            case TRACE_HitActor:
                let hitActor = Results.HitActor;

                // Skip through the player that we're tracing from
                if (hitActor == _curPlayer.mo)
                {
                    return TRACE_Skip;
                }

                // If the actor is not shootable, or if it's dead,
                // continue tracing.
                if (!hitActor.bShootable || hitActor.Health <= 0)
                {
                    return TRACE_Skip;
                }

                // Skip tracing if the actor is currently not visible.
                let rStyle = hitActor.GetRenderStyle();

                if (rStyle == STYLE_None || rStyle != STYLE_Normal && hitActor.Alpha == 0)
                {
                    return TRACE_Skip;
                }

                // Abort tracing if the actor is not a monster,
                // or if it's friendly.
                if (!hitActor.bIsMonster || hitActor.bFriendly)
                {
                    return TRACE_Abort;
                }

                return TRACE_Stop;
            // We hit a wall
            case TRACE_HitWall:
                // If we hit something other than the middle side, abort
                if (Results.Tier != TIER_Middle)
                {
                    return TRACE_Abort;
                }

                // Also abort tracing if this line is 1-sided,
                // or if it blocks both hitscans and projectiles
                let hitLine = Results.HitLine;
                int flags = hitLine.flags;

                if (!(flags & Line.ML_TWOSIDED) || (flags & (Line.ML_BLOCKPROJECTILE | Line.ML_BLOCKHITSCAN)) || (flags & Line.ML_BLOCKEVERYTHING))
                {
                    return TRACE_Abort;
                }

                // Trace through this line
                return TRACE_Skip;
        }

        // Do not trace through anything else
        return TRACE_Abort;
    }
}
