//===========================================================================
//
// RR_EnemyHealthFragment
//
// Displays the health of the enemy targeted by the player's crosshair.
//
//===========================================================================
class RR_EnemyHealthFragment : WL_HudFragment
{
    private RR_EnemyTracer _tracer;
    private WL_ProgressBar _healthBar;

    //===========================================================================
    //
    // RR_EnemyHealthFragment::Create
    //
    // Creates a new instance of the RR_EnemyHealthFragment class.
    //
    //===========================================================================
    static RR_EnemyHealthFragment Create()
    {
        let f = new('RR_EnemyHealthFragment');
        f.Init();

        return f;
    }

    //===========================================================================
    //
    // RR_EnemyHealthFragment::CreateView
    //
    // Creates the view for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        // Create the tracer
        _tracer = RR_EnemyTracer.Create();

        // Create the health bar widget
        _healthBar = WL_ProgressBar.Create();
        _healthBar.SetWidth(WL_Dimension.Fixed(400));
        _healthBar.SetHeight(WL_Dimension.Fixed(15));
        _healthBar.SetBackgroundColor(Color(255, 124, 124, 124));

        return _healthBar;
    }

    //===========================================================================
    //
    // RR_EnemyHealthFragment::Update
    //
    // Updates this fragment's state.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        let player = context.GetPlayer();

        // Don't do anything if the player is dead,
        // not looking through their own eyes,
        // or if the health bar is disabled
        if (player.Health <= 0 || player.camera != player.mo || RR_Settings.InUi(RR_SETTING_HUD_ENEMYHEALTH).IsOptionValue(RR_EH_OFF))
        {
            return false;
        }

        // Otherwise, check if we have an enemy to show health for.
        int maxDist = RR_Settings.InUi(RR_SETTING_HUD_ENEMYHEALTH_MAXDIST).GetInt();
        let enemy = _tracer.TraceEnemy(player, maxDist);
        bool result = enemy != null;

        // Only update the bar if we have a valid enemy.
        // (it would be hidden otherwise)
        if (result)
        {
            SetEnemy(enemy);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_EnemyHealthFragment::SetEnemy
    //
    // Sets the health bar to reflect the provided enemy's health.
    //
    //===========================================================================
    private void SetEnemy(Actor enemy)
    {
        // Determine health bar color
        if (enemy.bInvulnerable || enemy.bDormant)
        {
            _healthBar.SetColor(Color(255, 194, 194, 204));
        }
        else
        {
            bool interpolate = RR_Settings.InUi(RR_SETTING_HUD_ENEMYHEALTH).IsOptionValue(RR_EH_INTERPOLATED);

            if (interpolate)
            {
                _healthBar.ClearGradientColors();
                _healthBar.AddGradientColor(Color(255, 255, 0, 0));
                _healthBar.AddGradientColor(Color(255, 255, 255, 0));
                _healthBar.AddGradientColor(Color(255, 0, 255, 0));
            }
            else
            {
                _healthBar.SetColor(Color(255, 255, 0, 0));
            }
        }

        _healthBar.SetRelativeValue(enemy.Health, 0, enemy.SpawnHealth());
    }
}
