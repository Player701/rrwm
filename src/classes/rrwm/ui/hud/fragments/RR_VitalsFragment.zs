//===========================================================================
//
// RR_VitalsFragment
//
// Displays the health, armor, and oxygen meters.
//
//===========================================================================
class RR_VitalsFragment : RR_HudFragment
{
    private Array<RR_VitalsCounter> _counters;
    private WL_View _root;
    private WL_VisibilityChanger _visMaxAmounts;

    private WL_Margins _paddingNormal;
    private WL_Margins _paddingColorVisible;
    private WL_ColorIndicator _playerColor;

    const PLAYER_COLOR_WIDTH = 5;

    //===========================================================================
    //
    // RR_VitalsFragment::Create
    //
    // Creates a new instance of the RR_VitalsFragment class.
    //
    //===========================================================================
    static RR_VitalsFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_VitalsFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_VitalsFragment::CreateView
    //
    // Creates the view hierarchy for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        _visMaxAmounts = WL_VisibilityChanger.Create();

        let context = GetStyleContext();
        let tl = context.CreateTable(true, false);

        // Counters
        AddCounter(RR_VitalsCounterHealth.Create(), context, tl);
        AddCounter(RR_VitalsCounterArmor.Create(), context, tl);
        AddCounter(RR_VitalsCounterHexenArmor.Create(), context, tl);
//<ExcludeFromHudPackage>
        AddCounter(RR_VitalsCounterShield.Create(), context, tl);
//</ExcludeFromHudPackage>
        AddCounter(RR_VitalsCounterAir.Create(), context, tl);

        // Player color indicator
        _playerColor = WL_ColorIndicator.Create();
        _playerColor.SetWidth(WL_Dimension.Fixed(PLAYER_COLOR_WIDTH));
        _playerColor.SetHeight(WL_Dimension.MatchParent());

        let fl = WL_FrameLayout.Create();
        fl.SetGravity(WL_Gravity.Horizontal(WL_HG_Right));
        fl.AddView(_playerColor);
        tl.SetBackgroundView(fl);

        _paddingNormal = tl.GetPadding();
        _paddingColorVisible = WL_Margins.Create(
            _paddingNormal.GetLeft(),
            _paddingNormal.GetTop(),
            _paddingNormal.GetRight() + RR_HudStyleContext.FONT_SHADOW + PLAYER_COLOR_WIDTH * 2,
            _paddingNormal.GetBottom()
        );

        return (_root = tl);
    }

    //===========================================================================
    //
    // RR_VitalsFragment::AddCounter
    //
    // Adds a counter to the provided table.
    //
    //===========================================================================
    private void AddCounter(RR_VitalsCounter counter, RR_HudStyleContext context, WL_TableLayout tl)
    {
        counter.Attach(context, tl, _visMaxAmounts);
        _counters.Push(counter);
    }

    //===========================================================================
    //
    // RR_VitalsFragment::Update
    //
    // Updates the state of the views to reflect the game state.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        // Toggle the maximum amounts depending on the setting
        _visMaxAmounts.SetVisibility(RR_Settings.InUi(RR_SETTING_HUD_MAXVITALS).IsOn() ? WL_V_Visible : WL_V_Gone);

        let player = context.GetPlayer();

        // Set player color if enabled
        if (RR_Settings.InUi(RR_SETTING_HUD_PLAYERCOLOR).IsOnMultiplayerAware())
        {
            let clr = player.GetDisplayColor();

            _root.SetPadding(_paddingColorVisible);
            _playerColor.SetVisibility(WL_V_Visible);
            _playerColor.SetColor(Color(255, clr.R, clr.G, clr.B));
        }
        else
        {
            _root.SetPadding(_paddingNormal);
            _playerColor.SetVisibility(WL_V_Gone);
        }

        let flags = GetUpdateFlags(player);
        bool result = false;

        foreach (counter : _counters)
        {
            result = counter.Update(player, flags) || result;
        }

        return result;
    }

    //===========================================================================
    //
    // RR_VitalsFragment::GetUpdateFlags
    //
    // Checks whether the player is invulnerable due to a cheat, powerup etc.
    // The second flag is set when the player is invulnerable NOT due to
    // a cheat, and all invulnerability powerups are blinking.
    //
    //===========================================================================
    private static RR_VitalsCounterUpdateFlags GetUpdateFlags(PlayerInfo player)
    {
        RR_VitalsCounterUpdateFlags result = 0;

        // First of all, check if the player is cheating. This has to be checked
        // before anything else, or the result could be a false negative.
        bool invulnerable = player.cheats & (CF_GODMODE | CF_GODMODE2);

        if (!invulnerable && player.mo.bInvulnerable)
        {
            // If the player is not cheating outright, he might be invulnerable
            // due to an active powerup or something else.
            invulnerable = true;

            // Check if all powerups are blinking.
            bool hasPower = false, isBlinking = true;

            // Check all of the player's items.
            for (let ii = player.mo.Inv; ii != null; ii = ii.Inv)
            {
                let powerInv = PowerInvulnerable(ii);

                if (powerInv != null)
                {
                    hasPower = true;
                    isBlinking = isBlinking && powerInv.IsBlinking();
                }
            }

            // There must be at least one powerup, and all of them
            // must be blinking for isBlinking to be true.
            if (isBlinking && hasPower)
            {
                result |= RR_VCUF_INVULN_BLINKING;
            }
        }

        if (invulnerable)
        {
            result |= RR_VCUF_INVULNERABLE;
        }

        return result;
    }
}
