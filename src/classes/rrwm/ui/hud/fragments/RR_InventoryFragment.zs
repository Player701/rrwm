//===========================================================================
//
// RR_InventoryFragment
//
// Used to draw the inventory bar.
//
//===========================================================================
class RR_InventoryFragment : WL_InventoryBarFragment
{
    private RR_HudStyleContext _context;

    //===========================================================================
    //
    // RR_InventoryFragment::Create
    //
    // Creates a new instance of the RR_InventoryFragment class.
    //
    //===========================================================================
    static RR_InventoryFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_InventoryFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_InventoryFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;
        SetItemScrollTime(250);
        SetFullscreenOnly(true);

        Super.Init();
    }

    //===========================================================================
    //
    // RR_InventoryFragment::Update
    //
    // Resets the player's inventorytics value to 0
    // if the HUD is in fullscreen mode.
    // Also changes the inventory bar alignment according to user settings.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        // Respect NoInventoryBar MAPINFO setting
        if (Level.NoInventoryBar && GameInfo.gametype != GAME_Strife)
        {
            return false;
        }

        if (context.IsFullscreen())
        {
            context.GetPlayer().inventorytics = 0;
        }

        int alignment = RR_Settings.InUi(RR_SETTING_HUD_INVBARALIGN).GetInt();
        SetListHorizontalGravity(GetListHorizontalGravity(alignment));

        return Super.Update(context);
    }

    //===========================================================================
    //
    // RR_InventoryFragment::GetListHorizontalGravity
    //
    // Returns the horizontal gravity value corresponding
    // to the provided alignment setting value.
    //
    //===========================================================================
    private WL_HorizontalGravity GetListHorizontalGravity(int alignment)
    {
        switch (alignment)
        {
            case RR_IBA_CENTER:
                return WL_HG_Center;
            case RR_IBA_LEFT:
                return WL_HG_Left;
            case RR_IBA_RIGHT:
                return WL_HG_Right;
        }

        ThrowAbortException("%s: Unsupported inventory bar alignment value: %d", GetClassName(), alignment);
        return -1;
    }

    //===========================================================================
    //
    // RR_InventoryFragment::CreateItemFragment
    //
    // Returns a new instance of RR_ItemFragment to display items.
    //
    //===========================================================================
    override WL_InventoryItemFragment CreateItemFragment()
    {
        return RR_ItemFragment.Create(_context);
    }

    //===========================================================================
    //
    // RR_InventoryFragment::CreateLeftArrow
    //
    // Creates the view for the left arrow.
    //
    //===========================================================================
    override WL_View CreateLeftArrow()
    {
        return CreateArrow("INVGEML1");
    }

    //===========================================================================
    //
    // RR_InventoryFragment::CreateRightArrow
    //
    // Creates the view for the right arrow.
    //
    //===========================================================================
    override WL_View CreateRightArrow()
    {
        return CreateArrow("INVGEMR1");
    }

    //===========================================================================
    //
    // RR_InventoryFragment::CreateArrow
    //
    // Creates a view to use for one of the arrows.
    //
    //===========================================================================
    protected WL_View CreateArrow(string textureName)
    {
        double iconSize = RR_HudStyleContext.ICON_SIZE_BIG;

        let iv = WL_TextureView.Create();
        iv.SetWidth(WL_Dimension.Fixed(iconSize * 0.75));
        iv.SetHeight(WL_Dimension.Fixed(iconSize));
        iv.SetTextureName(textureName, TexMan.TYPE_MiscPatch);
        iv.SetLayoutParams(WL_LinearLayout.CreateParams(gravity: WL_Gravity.Vertical(WL_VG_CENTER)));

        return iv;
    }
}
