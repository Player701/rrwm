//===========================================================================
//
// RR_LowAmmoFragment
//
// Displays the low ammo indicator under the crosshair.
//
//===========================================================================
class RR_LowAmmoFragment : WL_HudFragment
{
    private WL_FontGroup _fnt;
    private WL_TextView _msg;

    private WL_ViewAnimator _blinkLooper;

    const BLINK_DELAY = 150;

    //===========================================================================
    //
    // RR_LowAmmoFragment::Create
    //
    // Creates a new instance of the RR_LowAmmoFragment class.
    //
    //===========================================================================
    static RR_LowAmmoFragment Create(WL_FontGroup fnt)
    {
        let f = new('RR_LowAmmoFragment');
        f.Init(fnt);

        return f;
    }

    //===========================================================================
    //
    // RR_LowAmmoFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_FontGroup fnt)
    {
        _fnt = fnt;

        Super.Init();
        SetFullscreenOnly(false);
    }

    //===========================================================================
    //
    // RR_LowAmmoFragment::CreateView
    //
    // Creates the view for this fragment to display.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        _msg = WL_TextView.Create();
        _msg.SetText("$RR_HUD_LOWAMMO");
        _msg.SetLocalized(true);
        _msg.SetFont(_fnt);
        _msg.SetScaleUniform(6);

        return _msg;
    }

    //===========================================================================
    //
    // RR_LowAmmoFragment::Update
    //
    // Calls UpdateMessage and toggles blinking animation.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        bool result = UpdateMessage(context);

        if (result && _blinkLooper == null)
        {
            _msg.SetVisibility(WL_V_Visible);
            _blinkLooper = WL_Waiter.Create(BLINK_DELAY)
                .BuildUpon()
                .ThenRun(WL_Blinker.Create())
                .LoopForever()
                .ObserveView(_msg);
        }

        if (!result && _blinkLooper != null)
        {
            _msg.GetObserverList().Clear();
            _blinkLooper = null;
        }

        return result;
    }

    //===========================================================================
    //
    // RR_LowAmmoFragment::UpdateMessage
    //
    // Checks if the player's ammo is low, and updates the fragment's visibility
    // and color.
    //
    //===========================================================================
    private bool UpdateMessage(WL_HudContext context)
    {
        let player = context.GetPlayer();

        // Do not display anything if the player is dead
        if (player.Health <= 0)
        {
            return false;
        }

        // Get the necessary values from the settings.
        bool lowAmmoEnabled = RR_Settings.InUi(RR_SETTING_HUD_LOWAMMO).IsOn();
        int lowAmmoThreshold = RR_Settings.InUi(RR_SETTING_HUD_LOWAMMO_THRESHOLD).GetInt();

        // Check what kind of weapon we are currently carrying.
        let rel = RR_ReloadableWeapon(player.ReadyWeapon);

        // Check the required conditions to draw the indicator.
        if (!lowAmmoEnabled || rel == null || rel.GetClipAmountPercentage() > lowAmmoThreshold)
        {
            return false;
        }

        _msg.SetColor(RR_HudStyleContext.GetAmmoColor(rel.Ammo1));
        return true;
    }
}
