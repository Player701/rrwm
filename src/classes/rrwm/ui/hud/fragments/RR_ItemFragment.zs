//===========================================================================
//
// RR_ItemFragment
//
// Used to display inventory items.
//
//===========================================================================
class RR_ItemFragment : WL_InventoryItemFragment
{
    private WL_TextureView _icon;
    private WL_Counter _counter;

    private WL_TextView _itemText;
    private WL_ScrollView _scrollView;
    private WL_ViewAnimator _scrollLooper;

    private RR_HudStyleContext _context;

    const SCROLL_SPEED = 0.1;
    const SCROLL_WAIT = 500;

    //===========================================================================
    //
    // RR_ItemFragment::Create
    //
    // Creates a new instance of the RR_ItemFragment class.
    //
    //===========================================================================
    static RR_ItemFragment Create(RR_HudStyleContext context)
    {
        let f = new('RR_ItemFragment');
        f.Init(context);

        return f;
    }

    //===========================================================================
    //
    // RR_ItemFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;
        Super.Init();
    }

    //===========================================================================
    //
    // RR_ItemFragment::CreateView
    //
    // Creates the view to display.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let ll = WL_LinearLayout.Create();
        ll.SetOrientation(WL_O_Vertical);
        ll.SetGravity(WL_Gravity.Horizontal(WL_HG_Center));

        _scrollView = WL_ScrollView.Create();
        _scrollView.SetOrientation(WL_O_Horizontal);
        _scrollView.SetWidth(WL_Dimension.Fixed(RR_HudStyleContext.ICON_SIZE_BIG * 2));

        _itemText = _context.CreateSmallText(localized: true);
        _scrollView.SetContent(_itemText);

        ll.AddView(_scrollView);

        let iconParams = WL_LinearLayout.CreateParams(margins: WL_Margins.Vertical(4, 4));
        _icon = _context.CreateBigIcon(iconParams, false);

        ll.AddView(_icon);

        _counter = _context.CreateTinyCounter();
        ll.AddView(_counter);

        return ll;
    }

    //===========================================================================
    //
    // RR_ItemFragment::BindItem
    //
    // Sets the state of the views to show the data for the provided item.
    //
    //===========================================================================
    override void BindItem(Inventory item, bool isSelected)
    {
        bool isDepleted = item.Amount == 0;
        double alpha = isSelected ? 1 : 0.5;
        if (isDepleted) alpha -= 0.25;

        _icon.SetTexture(BaseStatusBar.GetInventoryIcon(item, 0));
        _icon.SetAlpha(alpha);

        int clr = isDepleted
            ? (isSelected ? Font.CR_ORANGE : Font.CR_BLACK)
            : (isSelected ? Font.CR_GOLD   : Font.CR_GREY);

        _counter.SetValue(item.Amount);
        _counter.SetColor(clr);

        // Check if we need to show the item text.
        bool itemTagsEnabled = RR_Settings.InUi(RR_SETTING_HUD_ITEMTAGS).IsOn();
        bool showItemText = itemTagsEnabled && isSelected;
        _itemText.SetVisibility(showItemText ? WL_V_Visible : itemTagsEnabled ? WL_V_Hidden : WL_V_Gone);

        if (showItemText)
        {
            _itemText.SetText(item.GetTag());
            _itemText.SetColor(isDepleted ? Font.CR_DARKGRAY : Font.CR_GREY);
        }

        // If the item is selected, add the scroll looper.
        if (showItemText && _scrollLooper == null)
        {
            _scrollView.SetScrollPos(0);
            _scrollLooper = WL_Waiter.Create(SCROLL_WAIT)
                .BuildUpon()
                .ThenRun(WL_Scroller.ToEdge(SCROLL_SPEED))
                .ThenWaitFor(SCROLL_WAIT)
                .ThenRun(WL_Scroller.ToEdge(-SCROLL_SPEED))
                .LoopForever()
                .ObserveView(_scrollView);
        }

        // If it is not, remove the scroll looper.
        if (!showItemText && _scrollLooper != null)
        {
            _scrollView.GetObserverList().Clear();
            _scrollLooper = null;
        }
    }
}
