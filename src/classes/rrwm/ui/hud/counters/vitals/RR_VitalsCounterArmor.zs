//===========================================================================
//
// RR_VitalsCounterArmor
//
// Displays the player's armor.
//
//===========================================================================
class RR_VitalsCounterArmor : RR_VitalsCounter
{
    //===========================================================================
    //
    // RR_VitalsCounterArmor::Create
    //
    // Creates a new instance of the RR_VitalsCounterArmor class.
    //
    //===========================================================================
    static RR_VitalsCounterArmor Create()
    {
        return new('RR_VitalsCounterArmor');
    }

    //===========================================================================
    //
    // RR_VitalsCounterArmor::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    override bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st)
    {
        // Find the player's armor item.
        let armor = BasicArmor(player.mo.FindInventory('BasicArmor'));

        if (armor == null || armor.Amount <= 0)
        {
            return false;
        }

        int amount = st.CurValue = armor.Amount;
        int maxAmount = st.MaxValue = GetArmorMaxAmount(armor);

        st.CurColor = amount > maxAmount
            ? Font.FindFontColor('RR_HUD_ArmorBonus')
            : Font.CR_RED;

        st.Icon = armor.Icon;

        return true;
    }

    //===========================================================================
    //
    // RR_VitalsCounterArmor::GetArmorMaxAmount
    //
    // Returns the real maximum amount of the player's armor type.
    // This method is needed because picking up armor bonuses will change
    // the MaxAmount value of the player's BasicArmor, and we want to exclude
    // this case from our logic.
    //
    //===========================================================================
    private static int GetArmorMaxAmount(BasicArmor armor)
    {
        // Here I blame the system because BasicArmor stores neither the armor instance
        // nor the class type, only the name. Also bad that BasicArmorPickup and BasicArmorBonus
        // store their max amounts in different properties. Cue all this type-casting shit...

        // And also this. This case is possible if armor is given by the IDFA / IDKFA cheats.
        // It will have a SaveAmount of 0, which is something we definitely don't want to draw!
        // So here we use the expression copy-pasted from player_cheat.txt in gzdoom.pk3
        // to take care of this special case.
        if (armor.ArmorType == 'BasicArmorPickup')
        {
            return 100 * deh.BlueAC;
        }

        // Otherwise, we have to check if it's a basic armor pickup or a basic armor bonus.
        // BasicArmorPickup's max amount is its SaveAmount, while BasicArmorBonus
        // has the max amount stored in MaxSaveAmount.
        let armorPickupClass = (class<BasicArmorPickup>)(armor.ArmorType);

        if (armorPickupClass != null)
        {
            return GetDefaultByType(armorPickupClass).SaveAmount;
        }

        // Not a pickup, so probably a bonus.
        let armorBonusClass = (class<BasicArmorBonus>)(armor.ArmorType);

        if (armorBonusClass != null)
        {
            return GetDefaultByType(armorBonusClass).MaxSaveAmount;
        }

        // We shouldn't end up here, but let's err on the side of caution.
        return armor.MaxAmount;
    }
}
