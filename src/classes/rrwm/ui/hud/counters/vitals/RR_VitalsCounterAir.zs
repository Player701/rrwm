//===========================================================================
//
// RR_VitalsCounterAir
//
// Displays the remaining air supply as a percentage value.
//
//===========================================================================
class RR_VitalsCounterAir : RR_VitalsCounter
{
    //===========================================================================
    //
    // RR_VitalsCounterAir::Create
    //
    // Creates a new instance of the RR_VitalsCounterAir class.
    //
    //===========================================================================
    static RR_VitalsCounterAir Create()
    {
        return new('RR_VitalsCounterAir');
    }

    //===========================================================================
    //
    // RR_VitalsCounterAir::CreateIconView
    //
    // Creates the special icon view for this counter.
    //
    //===========================================================================
    override WL_TextureView, WL_View CreateIconView(RR_HudStyleContext context)
    {
        let result = WL_FrameLayout.Create();

        result.SetLayoutParams(WL_TableRow.CreateParams(WL_Margins.Right(RR_HudStyleContext.ICON_MARGIN)));
        result.SetWidth(WL_Dimension.Fixed(RR_HudStyleContext.ICON_SIZE_BIG));
        result.SetHeight(WL_Dimension.Fixed(RR_HudStyleContext.ICON_SIZE_BIG));

        // Bottom right part (the small "2")
        let smallText = context.CreateSmallText("2");
        smallText.SetLayoutParams(WL_FrameLayout.CreateParams(gravity: WL_Gravity.Create(WL_HG_Right, WL_VG_Bottom)));

        result.AddView(smallText);

        // Center part (the big "O")
        let bigText = context.CreateBigText("O");
        bigText.SetGravity(WL_Gravity.Create(WL_HG_Left, WL_VG_Center));

        result.SetBackgroundView(bigText);

        return null, result;
    }

    //===========================================================================
    //
    // RR_VitalsCounterAir::PrepareState
    //
    // Sets the initial state of this vitals counter.
    //
    //===========================================================================
    override void PrepareState(out RR_VitalsCounterState st)
    {
        // This does not change, so let's set it here once
        st.MaxValue = 100;
    }

    //===========================================================================
    //
    // RR_VitalsCounterAir::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    override bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st)
    {
        int oxygen = st.CurValue = GetOxygenAmount(player);

        if (oxygen < 0)
        {
            return false;
        }

        st.CurColor = GetOxygenColor(oxygen);
        st.IsCritical = oxygen == 0;

        return true;
    }

    //===========================================================================
    //
    // RR_VitalsCounterAir::GetOxygenAmount
    //
    // Returns the amount of oxygen as a percentage of the remaining supply.
    // If the counter needs to be hidden, the returned value is -1.
    //
    //===========================================================================
    private static int GetOxygenAmount(PlayerInfo player)
    {
        double airCapacity = player.mo.AirCapacity;
        int airSupply = Level.airSupply;

        if (!RR_Settings.InUi(RR_SETTING_HUD_AIRSUPPLY).IsOn() || airSupply == 0 || airCapacity == 0)
        {
            // The player can't drown if either the level's air supply is 0
            // or the player's air capacity is 0 (p_user.cpp::ResetAirSupply).
            // Therefore, it is pointless to show the counter in these cases.
            return -1;
        }

        // Normally, if the player is above water, we don't need to show the air supply,
        // as it is instantly restored to maximum.
        // However, what if CheckAirSupply is overridden so that it is not?
        // Then we will show the player that their air supply is being restored.
        int airTics = Max(0, player.air_finished - Level.maptime);

        // Subtracting 1 is necessary because the HUD is always updated
        // only on the next tic after the air supply is reset...
        double maxAirTics = (airSupply * airCapacity) - 1;

        // Do we have full air?
        bool isFullAir = RR_Math.AreAlmostEqual(airTics, maxAirTics);

        // Do not show the air time if we have full air AND are above water.
        // Also do not show if the player is dead and the cause of death wasn't drowning.
        if (isFullAir && player.mo.waterlevel < 3 || player.Health == 0 && player.LastDamageType != 'Drowning')
        {
            return -1;
        }

        return int(Ceil(airTics / maxAirTics * 100));
    }

    //===========================================================================
    //
    // RR_VitalsCounterAir::GetOxygenColor
    //
    // Returns the color of the oxygen counter depending on the current amount.
    //
    //===========================================================================
    private static int GetOxygenColor(int amount)
    {
        // If we have more than half of our air supply left, we're fine.
        if (amount > 50)
        {
            return Font.CR_GREEN;
        }

        // Otherwise, it's yellow when above 25% and red otherwise.
        return amount > 25 ? Font.CR_YELLOW : Font.CR_RED;
    }
}
