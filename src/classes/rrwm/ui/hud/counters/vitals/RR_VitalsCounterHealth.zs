//===========================================================================
//
// RR_VitalsCounterHealth
//
// Displays the player's health.
//
//===========================================================================
class RR_VitalsCounterHealth : RR_VitalsCounter
{
    //===========================================================================
    //
    // RR_VitalsCounterHealth::Create
    //
    // Creates a new instance of the RR_VitalsCounterHealth class.
    //
    //===========================================================================
    static RR_VitalsCounterHealth Create()
    {
        return new('RR_VitalsCounterHealth');
    }

    //===========================================================================
    //
    // RR_VitalsCounterHealth::PrepareState
    //
    // Sets the initial state of this vitals counter.
    //
    //===========================================================================
    override void PrepareState(out RR_VitalsCounterState st)
    {
        // This does not change, so let's set it here once
        st.Icon = GameInfo.healthpic;
    }

    //===========================================================================
    //
    // RR_VitalsCounterHealth::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    override bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st)
    {
        int health = st.CurValue = Max(player.Health, 0);
        int maxHealth = st.MaxValue = player.mo.GetMaxHealth(true);

        st.CurColor = health > maxHealth
            ? Font.FindFontColor('RR_HUD_HealthBonus')
            : Font.CR_RED;

        // Enable fade animator if health is low
        int threshold = RR_Settings.InUi(RR_SETTING_HUD_LOWHEALTH_THRESHOLD).GetInt();
        st.IsCritical = health <= threshold;

        return true;
    }
}
