//===========================================================================
//
// RR_VitalsCounterHexenArmor
//
// Displays the player's Hexen armor.
//
//===========================================================================
class RR_VitalsCounterHexenArmor : RR_VitalsCounter
{
    static const string HEXEN_ARMOR_ICONS[] = { "AR_1A0", "AR_2A0", "AR_3A0", "AR_4A0" };

    //===========================================================================
    //
    // RR_VitalsCounterHexenArmor::Create
    //
    // Creates a new instance of the RR_VitalsCounterHexenArmor class.
    //
    //===========================================================================
    static RR_VitalsCounterHexenArmor Create()
    {
        return new('RR_VitalsCounterHexenArmor');
    }

    //===========================================================================
    //
    // RR_VitalsCounterHexenArmor::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    override bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st)
    {
        // Find the player's armor item.
        let armor = HexenArmor(player.mo.FindInventory('HexenArmor'));
        int amount = 0, bestslot;

        if (armor != null)
        {
            // Find the current amount and best slot
            [amount, bestslot] = GetArmorAmount(armor);
        }

        if (amount == 0)
        {
            return false;
        }

        st.CurValue = amount;
        int maxAmount = st.MaxValue = GetArmorMaxAmount(armor);
        st.CurColor = amount > maxAmount ? Font.CR_PURPLE : Font.CR_RED;
        st.IconName = HEXEN_ARMOR_ICONS[bestslot];

        return true;
    }

    //===========================================================================
    //
    // RR_VitalsCounterHexenArmor::GetHexenArmorAmount
    //
    // Returns the current amount of Hexen armor the player has.
    // This amount is equal to the armor's save percent (armor class * 5).
    // Also returns the index of the armor slot that currently offers
    // best protection (to choose the proper icon).
    //
    //===========================================================================
    private static int, int GetArmorAmount(HexenArmor armor)
    {
        double sum = 0;
        int bestslot = 0;

        // NB: we have 5 armor slots but we only count the first 4 of them.
        // This is because the last slot is the built-in armor value
        // that never changes, so it doesn't make much sense to display it.
        for (int i = 0; i < 4; i++)
        {
            if (i > 0 && armor.Slots[i] > armor.Slots[bestslot])
            {
                bestslot = i;
            }

            sum += armor.Slots[i];
        }

        return int(sum), bestslot;
    }

    //===========================================================================
    //
    // RR_VitalsFragment::GetArmorMaxAmount
    //
    // Returns the maximum amount of Hexen armor the player can have.
    // This amount is equal to the maximum possible save percent
    // (max. armor class * 5) excluding the Dragonskin Bracers.
    //
    //===========================================================================
    private static int GetArmorMaxAmount(HexenArmor armor)
    {
        double sum = 0;

        for (int i = 0; i < 4; i++)
        {
            sum += armor.SlotsIncrement[i];
        }

        return int(sum);
    }
}
