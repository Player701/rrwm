//===========================================================================
//
// RR_VitalsCounterState
//
// Represents the current state of a vitals counter.
//
//===========================================================================
struct RR_VitalsCounterState ui
{
    // Gets or sets the current value.
    // -1 means the counter is not visible.
    int CurValue;

    // Gets or sets the maximum value.
    int MaxValue;

    // Gets or sets the current font color.
    int CurColor;

    // Gets or sets the current icon.
    TextureID Icon;

    // Gets or sets the current texture name of the icon.
    // This value overrides the TextureID field.
    string IconName;

    // Gets or sets whether the value is critical enough
    // for the fade-in-out animation to kick in.
    bool IsCritical;
}
