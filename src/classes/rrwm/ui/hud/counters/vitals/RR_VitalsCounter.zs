//===========================================================================
//
// RR_VitalsCounter
//
// Represents a single row of the vitals fragment.
//
//===========================================================================
class RR_VitalsCounter abstract ui
{
    // Total time in milliseconds to fade in and out (assuming normal speed)
    const ANIM_FADE_TIME = 750;
    const ANIM_FADE_TO = 0.25;

    private WL_TableRow _row;
    private WL_TextureView _icon;
    private WL_Counter _cntCur, _cntMax;

    private WL_TextColorChanger _colorChanger;
    private WL_AnimatorToggler _anim;

    private RR_VitalsCounterState _state;

    //===========================================================================
    //
    // RR_VitalsCounter::CreateFadeToggler
    //
    // Creates an AnimatorToggler which controls a fade-in-out animation.
    //
    //===========================================================================
    private static WL_AnimatorToggler CreateFadeToggler()
    {
        // Calculate speed based on the time a full cycle should take.
        double fadeSpeed = (1 - ANIM_FADE_TO) * 2 / ANIM_FADE_TIME;

        return WL_AnimatorToggler.Create(
            WL_Fader.Create(ANIM_FADE_TO, fadeSpeed)
                .BuildUpon()
                .ThenRun(WL_Fader.Create(1.0, fadeSpeed))
                .GetResult()
        );
    }

    //===========================================================================
    //
    // RR_VitalsCounter::Attach
    //
    // Adds a row representing this counter to the provided table.
    //
    //===========================================================================
    void Attach(RR_HudStyleContext context, WL_TableLayout table, WL_VisibilityChanger visMaxValue)
    {
        if (_row != null)
        {
            ThrowAbortException("%s: This counter is already attached to a table.", GetClassName());
        }

        _row = context.CreateRow();

        // Icon
        WL_View wrapper;
        [_icon, wrapper] = CreateIconView(context);
        _row.AddView(wrapper);

        // Counters
        _row.AddView((_cntCur = context.CreateBigCounter()));
        _row.AddView(context.CreateBigDelimiter().ObservedBy(visMaxValue));
        _row.AddView((_cntMax = context.CreateBigCounter()).ObservedBy(visMaxValue));

        // Add a fade animation
        _anim = CreateFadeToggler();
        _anim.ObserveView(_row);

        // Add a color changer to all text views in the row
        _colorChanger = WL_TextColorChanger.Create();
        _colorChanger.ObservePrimitiveViews(_row);

        table.AddView(_row);

        PrepareState(_state);
    }

    //===========================================================================
    //
    // RR_VitalsCounter::CreateIconView
    //
    // Creates a texture view to display the icon. This view may be null.
    // The second return value is the wrapper view to add to the layout.
    //
    //===========================================================================
    protected virtual WL_TextureView, WL_View CreateIconView(RR_HudStyleContext context)
    {
        // These are layout parameters for icons
        let iconParams = WL_FrameLayout.CreateParams(
            margins: WL_Margins.Right(RR_HudStyleContext.ICON_MARGIN)
        );

        let iconView = context.CreateBigIcon(iconParams, true);
        let wrapper = context.WrapBigIcon(iconView);

        return iconView, wrapper;
    }

    //===========================================================================
    //
    // RR_VitalsCounter::InitState
    //
    // Sets the initial state of this vitals counter.
    //
    //===========================================================================
    protected virtual void PrepareState(out RR_VitalsCounterState st)
    {
    }

    //===========================================================================
    //
    // RR_VitalsCounter::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    protected abstract bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st);

    //===========================================================================
    //
    // RR_VitalsCounter::Update
    //
    // Updates the values of this vitals counter and returns true
    // if the counter should currently be visible, false otherwise.
    //
    //===========================================================================
    bool Update(PlayerInfo player, RR_VitalsCounterUpdateFlags flags)
    {
        if (_row == null)
        {
            ThrowAbortException("%s: This counter has not been attached to a table.", GetClassName());
        }

        if (!UpdateState(player, _state))
        {
            // The counter is hidden
            _row.SetVisibility(WL_V_Gone);
            return false;
        }

        // The counter is visible
        bool wasVisible = _row.IsVisible();
        _row.SetVisibility(WL_V_Visible);

        // Update the counters to reflect the new state
        _cntCur.SetValue(_state.CurValue);
        _cntCur.MinDecimalPlacesFromValue(_state.MaxValue);
        _cntMax.SetValue(_state.MaxValue);

        bool invulnerable = flags & RR_VCUF_INVULNERABLE;
        int clr = invulnerable && !(flags & RR_VCUF_INVULN_BLINKING)
            ? Font.CR_ICE
            : _state.CurColor;

        _colorChanger.SetColor(clr);

        if (_icon != null)
        {
            if (_state.IconName != "")
            {
                _icon.SetTextureName(_state.IconName);
            }
            else
            {
                _icon.SetTexture(_state.Icon);
            }
        }

        // Update the animation state
        bool animate = _state.IsCritical && player.health > 0 && !invulnerable;
        bool resetAnimation = !animate && !wasVisible;
        _anim.Toggle(animate, resetAnimation);

        // Reset animation state if the counter is just popping up
        if (resetAnimation)
        {
            _row.SetAlpha(1);
        }

        return true;
    }
}

//===========================================================================
//
// RR_VitalsCounterUpdateFlags
//
// Enumerates flags passed to the vitals counter update routine.
//
//===========================================================================
enum RR_VitalsCounterUpdateFlags
{
    // The player is currently invulnerable.
    RR_VCUF_INVULNERABLE       = 1,

    // The player's invulnerability powerup is blinking
    // (and no other non-blinking means of invulnerability are in effect).
    RR_VCUF_INVULN_BLINKING    = 1 << 1,
};
