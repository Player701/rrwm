//===========================================================================
//
// RR_VitalsCounterShield
//
// Displays the amount of the super armor item
// which resides in a separate armor slot.
//
//===========================================================================
class RR_VitalsCounterShield : RR_VitalsCounter
{
    //===========================================================================
    //
    // RR_VitalsCounterShield::Create
    //
    // Creates a new instance of the RR_VitalsCounterShield class.
    //
    //===========================================================================
    static RR_VitalsCounterShield Create()
    {
        return new('RR_VitalsCounterShield');
    }

    //===========================================================================
    //
    // RR_VitalsCounterShield::PrepareState
    //
    // Sets the initial state of this vitals counter.
    //
    //===========================================================================
    override void PrepareState(out RR_VitalsCounterState st)
    {
        // This does not change, so let's set it here once
        st.CurColor = Font.CR_RED;
    }

    //===========================================================================
    //
    // RR_VitalsCounterShield::UpdateState
    //
    // Updates the current state of this vitals counter.
    //
    //===========================================================================
    override bool UpdateState(PlayerInfo player, out RR_VitalsCounterState st)
    {
        // Find the shield item.
        let shield = player.mo.FindInventory('RR_SuperArmorShieldPickup');

        if (shield == null || shield.Amount <= 0)
        {
            return false;
        }

        st.CurValue = shield.Amount;
        st.MaxValue = shield.MaxAmount;
        st.Icon = shield.Icon;

        return true;
    }
}
