//===========================================================================
//
// RR_LevelStatsCounter
//
// Base class for encapsulating level statistics counters.
// Used by RR_LevelStatssFragment to create and manage views on its behalf.
//
//===========================================================================
class RR_LevelStatsCounter abstract ui
{
    // Default color for all counters
    const STATS_COLOR = Font.CR_GREY;

    // Initial parameters
    private RR_Setting _toggleSetting;
    private string _prefix;
    private string _iconName;

    // Table row that contains all views below
    private WL_TableRow _row;
    private WL_Counter _cntIndValue, _cntCurValue, _cntMaxValue;
    private WL_TextView _percentSign;
    private WL_TextureView _icon;

    private WL_VisibilityChanger _visPrefix, _visIndValue, _visCurValue;

    // The current state of the counter
    private RR_LevelStatsCounterState _state;

    //===========================================================================
    //
    // RR_LevelStatsCounter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(name toggleSettingName, string prefix, string iconName)
    {
        _toggleSetting = RR_Settings.Get(toggleSettingName);
        _prefix = prefix;
        _iconName = iconName;
    }

    //===========================================================================
    //
    // RR_LevelStatsCounter::Attach
    //
    // Adds a row representing this counter to the provided table.
    //
    //===========================================================================
    void Attach(RR_HudStyleContext context, WL_TableLayout table, WL_FontFallbackCoordinator fc)
    {
        if (_row != null)
        {
            ThrowAbortException("%s: This counter is already attached to a table.", GetClassName());
        }

        _row = context.CreateRow();
        _visPrefix = WL_VisibilityChanger.Create();
        _visIndValue = WL_VisibilityChanger.Create();
        _visCurValue = WL_VisibilityChanger.Create();

        // This is the stat's description.
        let descParams = WL_TableRow.CreateParams(
            gravity: WL_Gravity.Horizontal(WL_HG_Center)
        );

        let desc = context.CreateSmallText(_prefix, true, STATS_COLOR);
        desc.SetLayoutParams(descParams);
        desc.SetLineTrimming(true);
        _row.AddView(desc.ObservedBy(_visPrefix));

        // The ":" is in a separate column to avoid any misalignments
        // since the font is not monospaced
        _row.AddView(context.CreateSmallDelimiter(":", STATS_COLOR).ObservedBy(_visPrefix));

        // Icon
        let iconParams = WL_TableRow.CreateParams(
            margins: WL_Margins.Right(RR_HudStyleContext.ICON_MARGIN)
        );

        _icon = context.CreateSmallIcon(iconParams, false);
        _icon.SetTextureName(_iconName);
        _row.AddView(_icon);

        // Individual value has a distinct color
        _row.AddView((_cntIndValue = context.CreateSmallCounter(Font.CR_CREAM)).ObservedBy(_visIndValue));
        _row.AddView(context.CreateSmallDelimiter(clr: STATS_COLOR).ObservedBy(_visIndValue));
        // Current value (level-wise)
        _row.AddView((_cntCurValue = context.CreateSmallCounter(STATS_COLOR)).ObservedBy(_visCurValue));
        _row.AddView(context.CreateSmallDelimiter(clr: STATS_COLOR).ObservedBy(_visCurValue));
        // Maximum value
        _row.AddView((_cntMaxValue = context.CreateSmallCounter(STATS_COLOR)));
        _row.AddView((_percentSign = context.CreateSmallDelimiter("%", STATS_COLOR)));

        fc.ObservePrimitiveViews(_row);
        table.AddView(_row);
    }

    //===========================================================================
    //
    // RR_LevelStatsCounter::Update
    //
    // If the counter should be visible, updates the row's contents
    // and returns true. Otherwise, returns false.
    //
    //===========================================================================
    bool Update(WL_HudContext context, RR_LevelStatsCounterUpdateFlags flags)
    {
        if (_row == null)
        {
            ThrowAbortException("%s: This counter has not been attached to a table.", GetClassName());
        }

        UpdateState(context.GetPlayer(), _state);

        bool showIndValue = flags & RR_LSCUF_SHOW_IND_VALUE;
        int testVal = Max(
            _state.CurValue,
            _state.MaxValue,
            showIndValue ? _state.IndValue : 0
        );

        // Use the test value to determine
        // if the counter itself should be displayed.
        bool result = _toggleSetting.GetUiValue().IsOnNonZero(testVal);

        if (result)
        {
            // Counter enabled - row visible
            _row.SetVisibility(WL_V_Visible);

            // Toggle icon/text
            bool useIcon = flags & RR_LSCUF_USE_ICON;
            _icon.SetVisibility(useIcon ? WL_V_Visible : WL_V_Gone);
            _visPrefix.SetVisibility(useIcon ? WL_V_Gone : WL_V_Visible);

            // Show the current value?
            bool showCur = flags & RR_LSCUF_SHOW_CUR_VALUE;

            if (showCur)
            {
                _visCurValue.SetVisibility(WL_V_Visible);
                _cntCurValue.SetValue(_state.CurValue);
            }
            else
            {
                _visCurValue.SetVisibility(WL_V_Gone);
            }

            // Show the percentage?
            if (flags & RR_LSCUF_SHOW_PERCENTAGE)
            {
                _percentSign.SetVisibility(WL_V_Visible);

                int pct;

                if (_state.MaxValue == 0)
                {
                    // Avoid dividing by 0
                    pct = 100;
                }
                else
                {
                    double ratio = double(_state.CurValue)/_state.MaxValue;
                    pct = int(Floor(ratio * 100));
                }

                _cntMaxValue.SetValue(pct);
            }
            else if (showCur)
            {
                // Show max value instead of the percentage
                _percentSign.SetVisibility(WL_V_Gone);
                _cntMaxValue.SetValue(_state.MaxValue);
            }
            else
            {
                // Current value is hidden and percentage is not shown. This is not allowed!
                ThrowAbortException("%s: Must specify at least one of SHOW_CUR_VALUE or PERCENTAGE flags when updating.", GetClassName());
            }

            // Show the individual value?
            if (showIndValue)
            {
                _visIndValue.SetVisibility(WL_V_Visible);
                _cntIndValue.SetValue(_state.IndValue);
            }
            else
            {
                _visIndValue.SetVisibility(WL_V_Gone);
            }
        }
        else
        {
            // Counter disabled - row not visible
            _row.SetVisibility(WL_V_Gone);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_LevelStatsCounter::UpdateState
    //
    // Updates the state of this level statistics counter.
    //
    //===========================================================================
    protected abstract void UpdateState(PlayerInfo player, out RR_LevelStatsCounterState st);
}

//===========================================================================
//
// RR_LevelStatsCounterUpdateFlags
//
// Enumerates flags passed to the statistics counter update routine.
//
//===========================================================================
enum RR_LevelStatsCounterUpdateFlags
{
    // Use an icon instead of text.
    RR_LSCUF_USE_ICON        = 1,

    // Show the individual value
    // (in single-player, applies only to monsters)
    RR_LSCUF_SHOW_IND_VALUE  = 1 << 1,

    // Show the current value.
    RR_LSCUF_SHOW_CUR_VALUE  = 1 << 2,

    // Show percentage instead of the max value.
    // Individual value is not affected.
    RR_LSCUF_SHOW_PERCENTAGE = 1 << 3
};
