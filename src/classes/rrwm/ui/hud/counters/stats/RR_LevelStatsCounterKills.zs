//===========================================================================
//
// RR_LevelStatsCounterKills
//
// Provides values for the kills statistic on the current level.
//
//===========================================================================
class RR_LevelStatsCounterKills : RR_LevelStatsCounter
{
    //===========================================================================
    //
    // RR_LevelStatsCounterKills::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_LevelStatsCounterKills Create()
    {
        let c = new('RR_LevelStatsCounterKills');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterKills::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        Super.Init(RR_SETTING_HUD_STAT_KILLS, "$RR_HUD_STAT_KILLS", "RR_KILLS");
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterKills::UpdateState
    //
    // Updates the state of this level statistics counter.
    //
    //===========================================================================
    override void UpdateState(PlayerInfo player, out RR_LevelStatsCounterState st)
    {
        st.CurValue = Level.killed_monsters;
        st.MaxValue = Level.total_monsters;
        st.IndValue = player.killcount;
    }
}
