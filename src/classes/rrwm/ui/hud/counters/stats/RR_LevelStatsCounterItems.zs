//===========================================================================
//
// RR_LevelStatsCounterItems
//
// Provides values for the items statistic on the current level.
//
//===========================================================================
class RR_LevelStatsCounterItems : RR_LevelStatsCounter
{
    //===========================================================================
    //
    // RR_LevelStatsCounterItems::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_LevelStatsCounterItems Create()
    {
        let c = new('RR_LevelStatsCounterItems');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterItems::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        Super.Init(RR_SETTING_HUD_STAT_ITEMS, "$RR_HUD_STAT_ITEMS", "RR_ITEMS");
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterItems::UpdateState
    //
    // Updates the state of this level statistics counter.
    //
    //===========================================================================
    override void UpdateState(PlayerInfo player, out RR_LevelStatsCounterState st)
    {
        st.CurValue = Level.found_items;
        st.MaxValue = Level.total_items;
        st.IndValue = player.itemcount;
    }
}
