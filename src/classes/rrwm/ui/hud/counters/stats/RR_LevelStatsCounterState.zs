//===========================================================================
//
// RR_LevelStatsCounterState
//
// Represents the current state of a level statistics counter.
//
//===========================================================================
struct RR_LevelStatsCounterState ui
{
    // Gets or sets the current value.
    int CurValue;

    // Gets or sets the maximum value.
    int MaxValue;

    // Gets or sets the current individual value.
    int IndValue;
}
