//===========================================================================
//
// RR_LevelStatsCounterSecrets
//
// Provides values for the secrets statistic on the current level.
//
//===========================================================================
class RR_LevelStatsCounterSecrets : RR_LevelStatsCounter
{
    //===========================================================================
    //
    // RR_LevelStatsCounterSecrets::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_LevelStatsCounterSecrets Create()
    {
        let c = new('RR_LevelStatsCounterSecrets');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterSecrets::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        Super.Init(RR_SETTING_HUD_STAT_SECRETS, "$RR_HUD_STAT_SECRETS", "RR_SCRTS");
    }

    //===========================================================================
    //
    // RR_LevelStatsCounterSecrets::UpdateState
    //
    // Updates the state of this level statistics counter.
    //
    //===========================================================================
    override void UpdateState(PlayerInfo player, out RR_LevelStatsCounterState st)
    {
        st.CurValue = Level.found_secrets;
        st.MaxValue = Level.total_secrets;
        st.IndValue = player.secretcount;
    }
}
