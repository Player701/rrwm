//===========================================================================
//
// RR_CrosshairCanvas
//
// A special canvas used to draw the low ammo indicator.
// This canvas automatically gets its scaling factor
// from the crosshair scale.
//
// This is a neat trick to avoid constantly adjusting
// the low ammo indicator's position and text scale:
// the canvas does this all automatically.
//
//===========================================================================
class RR_CrosshairCanvas : WL_ScreenCanvas
{
    //===========================================================================
    //
    // RR_CrosshairCanvas::Create
    //
    // Creates a new instance of the RR_CrosshairCanvas class.
    //
    //===========================================================================
    static RR_CrosshairCanvas Create()
    {
        let c = new('RR_CrosshairCanvas');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // RR_CrosshairCanvas::GetScaleFactors
    //
    // Returns the scaling factor based on crosshair scale.
    //
    //===========================================================================
    override vector2 GetScaleFactors() const
    {
        // 0.2 seems to be the real crosshair scale, WTF?
        double xhairscale = RR_Math.GetNonZeroValue(crosshairscale, 0.2);
        return (xhairscale, xhairscale);
    }
}
