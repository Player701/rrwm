//===========================================================================
//
// RR_AmmoTypeHolder
//
// A class that holds the generated views for the RR_AmmoTableAdapter.
//
//===========================================================================
class RR_AmmoTypeHolder ui
{
    private WL_Counter _cntAmount;
    private WL_Counter _cntMaxAmount;
    private WL_TextureView _icon;

    private WL_TextColorChanger _colorChanger;
    private WL_AlphaChanger _alphaChanger;

    private WL_TableRow _row;

    private RR_HudStyleContext _context;

    //===========================================================================
    //
    // RR_AmmoTypeHolder::Create
    //
    // Creates a new instance of the RR_AmmoTypeHolder class.
    //
    //===========================================================================
    static RR_AmmoTypeHolder Create(RR_HudStyleContext context)
    {
        let h = new('RR_AmmoTypeHolder');
        h.Init(context);

        return h;
    }

    //===========================================================================
    //
    // RR_AmmoTypeHolder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;

        // Create one row to show data for an ammo type.
        _row = _context.CreateRow();

        _row.AddView((_cntAmount = _context.CreateSmallCounter()));
        _row.AddView(_context.CreateSmallDelimiter());
        _row.AddView((_cntMaxAmount = _context.CreateSmallCounter()));

        let iconParams = WL_TableRow.CreateParams(
            margins: WL_Margins.Left(RR_HudStyleContext.ICON_MARGIN)
        );

        _icon = _context.CreateSmallIcon(iconParams, false);
        _row.AddView(_icon);

        _colorChanger = WL_TextColorChanger.Create();
        _alphaChanger = WL_AlphaChanger.Create();

        // Attach the TextColorChanger and AlphaChanger to all views within the row.
        _colorChanger.ObservePrimitiveViews(_row);
        _alphaChanger.ObservePrimitiveViews(_row);
    }

    //===========================================================================
    //
    // RR_AmmoTypeHolder::GetView
    //
    // Returns the view to display.
    //
    //===========================================================================
    WL_View GetView() const
    {
        // This needs to be done here because, unlike in Android,
        // Bind happens while we're still updating.
        _row.SetVisibility(_icon.IsValidTexture() ? WL_V_Visible : WL_V_Gone);
        return _row;
    }

    //===========================================================================
    //
    // RR_AmmoTypeHolder::Bind
    //
    // Updates the views contained within this instance with the provided data.
    //
    //===========================================================================
    void Bind(class<Ammo> ammoType, int amount, int maxAmount, bool isCurrent)
    {
        _icon.SetTexture(GetDefAmmoIcon(ammoType));
        _cntAmount.SetValue(amount);
        _cntAmount.MinDecimalPlacesFromValue(maxAmount);
        _cntMaxAmount.SetValue(maxAmount);
        _colorChanger.SetColor(_context.GetAmmoColorByType(ammoType));
        _alphaChanger.SetAlpha(isCurrent ? 1.0 : 0.65);
    }

    //===========================================================================
    //
    // RR_AmmoTypeHolder::GetDefAmmoIcon
    //
    // Gets the default icon for an ammo type.
    //
    //===========================================================================
    private static TextureID GetDefAmmoIcon(class<Ammo> ammotype)
    {
        // This is basically a stripped down re-implementation
        // of GetInventoryIcon from GZDoom's source code.
        // Why do we need this? Two reasons:
        // 1) Normal pointers STILL cannot be converted to readonly pointers.
        // 2) BaseStatusBar::GetInventoryIcon doesn't accept a readonly pointer.
        // This method can be removed when these issues have been resolved.

        let def = GetDefaultByType(ammotype);
        let icon = def.Icon;

        // If the icon is not valid, use the sprite from the item's spawn state,
        // provided this state exists.
        if (!icon.IsValid())
        {
            let state = def.SpawnState;

            if (state != null)
            {
                icon = state.GetSpriteTexture(0);
            }
        }

        return icon;
    }
}
