//===========================================================================
//
// RR_WeaponSlotAdapter
//
// Checks the status of a single weapon slot and generates progress
// indicators to reflect the clip amounts for the slot's weapons.
//
//===========================================================================
class RR_WeaponSlotAdapter : WL_HudListAdapter
{
    private int _slot;
    private bool _hasAssignedWeapons, _hasActualWeapons, _isSelected;

    private Array<WL_ProgressColorIndicator> _clipIndicators;

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_WeaponSlotAdapter Create(int slot)
    {
        let a = new('RR_WeaponSlotAdapter');
        a.Init(slot);

        return a;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(int slot)
    {
        _slot = slot;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::HasAssignedWeapons
    //
    // Returns whether the slot has any weapons assigned.
    //
    //===========================================================================
    bool HasAssignedWeapons() const
    {
        return _hasAssignedWeapons;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::HasActualWeapons
    //
    // Returns whether the player has any weapons for this slot.
    //
    //===========================================================================
    bool HasActualWeapons() const
    {
        return _hasActualWeapons;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::IsSelected
    //
    // Returns whether a weapon from this slot is currently selected.
    //
    //===========================================================================
    bool IsSelected() const
    {
        return _isSelected;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::GenerateItems
    //
    // Updates the state of the parent fragment and binds progress indicators
    // to reflect the clip amounts for the provided slot's weapons.
    //
    //===========================================================================
    override int GenerateItems(WL_HudContext context)
    {
        let player = context.GetPlayer();
        int slotSize = player.weapons.SlotSize(_slot);

        int clipCnt = 0;
        bool showClip = RR_HudHooks.CanShowClipIndicators();

        _hasActualWeapons = false;
        _isSelected = false;

        for (int i = 0; i < slotSize; i++)
        {
            let weaponType = player.weapons.GetWeapon(_slot, i);
            let weap = Weapon(player.mo.FindInventory(weaponType));

            // Do we have this weapon?
            bool hasWeapon = weap != null;

            // Check if the slot is active and/or selected
            _hasActualWeapons = _hasActualWeapons || hasWeapon;
            _isSelected = _isSelected || hasWeapon && weap == player.ReadyWeapon;

            // Check if we want to show clip indicators.
            if (showClip)
            {
                int clip, clipMax;

                // Is this weapon reloadable?
                if (RR_HudHooks.IsWeaponReloadable(weap, clip, clipMax))
                {
                    BindClipIndicator(clipCnt++, clip, clipMax);
                }
            }
        }

        // If there are too many unused indicators, remove extra ones.
        if (_clipIndicators.Size() >= clipCnt * 2)
        {
            _clipIndicators.Resize(clipCnt);
        }

        _hasAssignedWeapons = slotSize > 0;
        return clipCnt;
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::BindClipIndicator
    //
    // Creates or reuses an existing clip indicator to display the clip amount.
    //
    //===========================================================================
    private void BindClipIndicator(int index, int clip, int clipMax)
    {
        while (index >= _clipIndicators.Size())
        {
            _clipIndicators.Push(null);
        }

        let i = _clipIndicators[index];

        if (i == null)
        {
            i = WL_ProgressColorIndicator.Create();
            i.SetWidth(WL_Dimension.MatchParent());
            i.SetHeight(WL_Dimension.MatchParent());
            i.SetAlphaGradient(Color(255, 255, 0, 0), 0);
            i.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));

            _clipIndicators[index] = i;
        }

        i.SetRelativeValue(clip, 0, clipMax);
    }

    //===========================================================================
    //
    // RR_WeaponSlotAdapter::GetOrCreateView
    //
    // Returns the progress indicator at the provided index.
    //
    //===========================================================================
    override WL_View GetOrCreateView(int index, WL_View convertView)
    {
        return _clipIndicators[index];
    }
}
