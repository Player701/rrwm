//===========================================================================
//
// RR_PowerupHolder
//
// Holds the views for the RR_PowerupsAdapter to display.
//
//===========================================================================
class RR_PowerupHolder ui
{
    private RR_HudStyleContext _context;
    private bool _showTime;

    private WL_TextureView _icon;
    private WL_Counter _cntTimer;
    private WL_AlphaChanger _globalAlpha;

    private WL_LinearLayout _ll;

    //===========================================================================
    //
    // RR_PowerupHolder::Create
    //
    // Creates a new instance of the RR_PowerupHolder class.
    //
    //===========================================================================
    static RR_PowerupHolder Create(RR_HudStyleContext context)
    {
        let h = new('RR_PowerupHolder');
        h.Init(context);

        return h;
    }

    //===========================================================================
    //
    // RR_PowerupHolder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;

        _ll = WL_LinearLayout.Create();
        _ll.SetGravity(WL_Gravity.Create(WL_HG_Right, WL_VG_Center));
        _ll.SetOrientation(WL_O_Horizontal);
        _ll.SetReversed(true);
        _context.SetRowStyle(_ll);

        // Powerup icon
        let iconParams = WL_LinearLayout.CreateParams(
            margins: WL_Margins.Left(RR_HudStyleContext.ICON_MARGIN)
        );

        _icon = _context.CreateBigIcon(iconParams, false);
        _ll.AddView(_icon);

        // Powerup timer
        _ll.AddView(_cntTimer = _context.CreateBigCounter());

        // Wire up the alpha controller for the whole row.
        _globalAlpha = WL_AlphaChanger.Create();
        _globalAlpha.ObservePrimitiveViews(_ll);
    }

    //===========================================================================
    //
    // RR_PowerupHolder::SetShowTime
    //
    // Sets whether or not the time should be visible.
    //
    //===========================================================================
    void SetShowTime(bool showTime)
    {
        _showTime = showTime;
    }

    //===========================================================================
    //
    // RR_PowerupHolder::GetView
    //
    // Returns the view to display.
    //
    //===========================================================================
    WL_View GetView() const
    {
        // Alter the row's visibility accordingly.
        _ll.SetVisibility(_icon.IsValidTexture() ? WL_V_Visible : WL_V_Gone);
        return _ll;
    }

    //===========================================================================
    //
    // RR_PowerupHolder::Bind
    //
    // Updates the state of the views to show the provided powerup.
    //
    //===========================================================================
    void Bind(Powerup powerup)
    {
        bool isTimeVisible;
        double alpha;

        // Get known powerup icon and color, if any
        let [iconName, clrTime] = GetKnownPowerupIconAndColor(powerup);

        // Check for special powerup properties
        if (!RR_HudHooks.GetSpecialPowerupProperties(powerup, isTimeVisible, alpha, clrTime))
        {
            // Use default properties
            isTimeVisible = CheckTimeVisible(powerup);
            alpha = 1;
        }

        // First check if the powerup itself provides an icon.
        _icon.SetTexture(powerup.GetPowerupIcon());

        // If the default icon is not valid and we have a known icon, use it.
        if (!_icon.IsValidTexture() && iconName != "")
        {
            _icon.SetTextureName(iconName);
        }

        // Check if the icon is valid; if not, the powerup is not visible
        if (!_icon.IsValidTexture())
        {
            return;
        }

        // Only show time if not disabled and not blinking
        if (_showTime && isTimeVisible && !powerup.IsBlinking())
        {
            // The times is visible, so adjust its parameters accordingly.
            _cntTimer.SetVisibility(WL_V_Visible);
            _cntTimer.SetColorName(clrTime);
            _cntTimer.SetValue(RR_Math.TicsToSeconds(powerup.EffectTics));
        }
        else
        {
            // The timer is not visible.
            _cntTimer.SetVisibility(WL_V_Gone);
        }

        // Set alpha for the whole row.
        _globalAlpha.SetAlpha(alpha);
    }

     //===========================================================================
    //
    // RR_PowerupHolder::GetKnownPowerupIconAndColor
    //
    // Returns the image and color for a known powerup.
    //
    //===========================================================================
    private static string, name GetKnownPowerupIconAndColor(Powerup powerup)
    {
        if (RR_Hud.IsDoomLikeGame())
        {
            let powerupName = powerup.GetClassName();

            switch(powerupName)
            {
                case 'PowerInvulnerable':
                    // Invulnerability.
                    return "PINVA0", 'DarkGreen';
                case 'PowerInvisibility':
                    // Invisibility (blur sphere).
                    return "PINSA0", 'Red';
                case 'PowerIronFeet':
                    // Radiation suit.
                    return "SUITA0", 'Green';
                case 'PowerLightAmp':
                    // Light amp.
                    return "PVISA0", 'LightBlue';
                case 'PowerStrength':
                    // Berserk. Note that color is not required,
                    // since the powerup is effectively infinite
                    // and we don't draw its remaining time.
                    return "PSTRA0", 'None';
            }
        }

        // We don't know what it is.
        return "", 'None';
    }

    //===========================================================================
    //
    // RR_PowerupHolder::CheckTimeVisible
    //
    // Checks if remaining time should be displayed for the provided powerup.
    //
    //===========================================================================
    private static bool CheckTimeVisible(Powerup powerup)
    {
        let powerupName = powerup.GetClassName();

        switch(powerupName)
        {
            case 'PowerStrength':
                return false;
            case 'PowerFlight':
                // Negation of logic from gzdoom.pk3
                // that makes the powerup last indefinitely
                return multiplayer || !Level.infinite_flight;
            default:
                return true;
        }
    }
}
