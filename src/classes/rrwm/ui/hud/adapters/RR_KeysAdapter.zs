//===========================================================================
//
// RR_KeysAdapter
//
// Generates views to display keys in the RRWM HUD.
//
//===========================================================================
class RR_KeysAdapter : WL_KeysAdapter
{
    const PADDING_RIGHT_TOP = 1.0;

    private RR_HudStyleContext _context;
    private WL_LayoutParams _iconParams;

    //===========================================================================
    //
    // RR_KeysAdapter::Create
    //
    // Creates a new instance of the RR_KeysAdapter class.
    //
    //===========================================================================
    static RR_KeysAdapter Create(RR_HudStyleContext context)
    {
        let a = new('RR_KeysAdapter');
        a.Init(context);

        return a;
    }

    //===========================================================================
    //
    // RR_KeysAdapter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;
        _iconParams = WL_GridLayout.CreateParams(margins: WL_Margins.Create(0, PADDING_RIGHT_TOP, PADDING_RIGHT_TOP, 0));
    }

    //===========================================================================
    //
    // RR_KeysAdapter::GetOrCreateKeyView
    //
    // Creates a view for the provided key.
    //
    //===========================================================================
    override WL_View GetOrCreateKeyView(Key key, WL_View convertView)
    {
        let iv = WL_TextureView(convertView);

        if (iv == null)
        {
            iv = _context.CreateIcon(12, _iconParams, true);
            iv.SetPadding(WL_Margins.All(1));
        }

        bool smallIcons =  RR_Settings.InUi(RR_SETTING_HUD_SMALLKEYICONS).IsOn();
        iv.SetInventoryIcon(key, smallIcons ? 0 : BaseStatusBar.DI_SKIPICON);

        return iv;
    }
}
