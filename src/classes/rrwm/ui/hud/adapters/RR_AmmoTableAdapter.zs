//===========================================================================
//
// RR_AmmoTableAdapter
//
// Implements a list adapter for the ammo table.
//
//===========================================================================
class RR_AmmoTableAdapter : WL_HudListAdapter
{
    private RR_HudStyleContext _context;

    private Map<name, int> _ammoTypes; // actually just a set (value is unused)
    private Array<RR_AmmoTypeHolder> _holders;

    //===========================================================================
    //
    // RR_AmmoTableAdapter::Create
    //
    // Creates a new instance of the RR_AmmoTableAdapter class.
    //
    //===========================================================================
    static RR_AmmoTableAdapter Create(RR_HudStyleContext context)
    {
        let a = new('RR_AmmoTableAdapter');
        a.Init(context);

        return a;
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _context = context;
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::GenerateItems
    //
    // Finds all ammo types that should be displayed.
    //
    //===========================================================================
    override int GenerateItems(WL_HudContext context)
    {
        _ammoTypes.Clear();

        if (RR_Settings.InUi(RR_SETTING_HUD_AMMOTABLE).IsOn())
        {
            // This logic is similar to the one used in AltHUD.
            // First the weapon slots...

            // Show all ammo types?
            int showAll = RR_Settings.InUi(RR_SETTING_HUD_AMMOTABLE_SHOWALL).GetInt();

            // Show the currently used ammo type?
            bool showCurrent = RR_Settings.InUi(RR_SETTING_HUD_AMMOTABLE_SHOWCUR).IsOn();

            let player = context.GetPlayer();

            for (int i = 0; i < PlayerPawn.NUM_WEAPON_SLOTS; i++)
            {
                // Start from slot 1 (normal keyboard order)
                int slot = i + 1;

                // Slot 0 goes last
                if (slot == PlayerPawn.NUM_WEAPON_SLOTS)
                {
                    slot = 0;
                }

                int slotSize = player.weapons.SlotSize(slot);

                for (int j = 0; j < slotSize; j++)
                {
                    let weaponType = player.weapons.GetWeapon(slot, j);

                    // Only show ammo for this weapon type if:
                    // 1) the player has a weapon of this type OR
                    // 2) "show all ammo types" is on.
                    let weap = player.mo.FindInventory(weaponType);

                    if (weap != null || showAll != RR_OPTVAL_OFF)
                    {
                        // To show ammo if it's zero, we should have this weapon,
                        // or "show useless ammo" must not be set to "if non-zero".
                        AddAmmoItemsForWeapon(weaponType, player, showCurrent, weap || showAll != RR_OPTVAL_IF_NONZERO, false);
                    }
                }
            }

            // ...then the remaining weapons.
            for (let ii = player.mo.Inv; ii != null; ii = ii.Inv)
            {
                let weap = Weapon(ii);

                if (weap != null)
                {
                    AddAmmoItemsForWeapon(weap.GetClass(), player, showCurrent, true, true);
                }
            }
        }

        int cnt = _ammoTypes.CountUsed();

        // If there are too many unused view holders, remove extra ones.
        if (_holders.Size() >= cnt * 2)
        {
            _holders.Resize(cnt);
        }

        return cnt;
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::AddAmmoItemsForWeapon
    //
    // Adds all ammo items used by a weapon to the table.
    //
    //===========================================================================
    private void AddAmmoItemsForWeapon(class<Weapon> weaponType, PlayerInfo player, bool showCurrent, bool showZero, bool fromWeapon)
    {
        let def = GetDefaultByType(weaponType);

        if (def.AmmoType1 != null)
        {
            AddAmmoItem(def.AmmoType1, player, showCurrent, showZero, fromWeapon);
        }

        if (def.AmmoType2 != null)
        {
            AddAmmoItem(def.AmmoType2, player, showCurrent, showZero, fromWeapon);
        }
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::AddAmmoItem
    //
    // Adds an ammo item to the table.
    //
    //===========================================================================
    private void AddAmmoItem(class<Ammo> ammoType, PlayerInfo player, bool showCurrent, bool showZero, bool fromWeapon)
    {
        // If necessary, ensure this ammo type is relevant.
        if (!RR_HudHooks.IsAmmoInTable(ammoType, fromWeapon))
        {
            return;
        }

        // Do not add this ammo type if it's being used by the player's
        // current werapon and showCurrent is false.
        bool isCurrent = IsAmmoUsedByWeapon(ammoType, player.ReadyWeapon);

        if (isCurrent && !showCurrent)
        {
            return;
        }

        // Check if the player has this ammo.
        let ammoitem = player.mo.FindInventory(ammoType);

        // If showZero is false, check if the amount is non-zero.
        if (!showZero && (ammoitem == null || ammoitem.Amount == 0))
        {
            return;
        }

        // Do not add twice
        let ammoTypeName = ammoType.GetClassName();

        if (!_ammoTypes.CheckKey(ammoTypeName))
        {
            // Get the current and max amounts.
            int amount = ammoitem != null ? ammoitem.Amount : 0;
            int maxAmount = ammoitem != null
                ? ammoitem.MaxAmount
                : GetDefaultByType(ammoType).MaxAmount;

            let holder = GetOrCreateViewHolder();
            // Bind the values to the view holder.
            holder.Bind(ammoType, amount, maxAmount, isCurrent);

            // Add the ammo type to the array so that it's not added again.
            _ammoTypes.InsertNew(ammoTypeName);
        }
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::IsAmmoUsedByWeapon
    //
    // Checks if an ammo type is used by a weapon.
    //
    //===========================================================================
    private static bool IsAmmoUsedByWeapon(class<Ammo> ammotype, Weapon weap)
    {
        // The answer is obviously "no" if there is no weapon.
        if (weap == null)
        {
            return false;
        }

        // The usual stuff.
        return weap.AmmoType1 == ammotype || weap.AmmoType2 == ammotype;
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::GetOrCreateViewHolder
    //
    // Returns an existing or creates a new RR_AmmoTypeHolder
    // to hold the views for an ammo type row.
    //
    //===========================================================================
    private RR_AmmoTypeHolder GetOrCreateViewHolder()
    {
        RR_AmmoTypeHolder holder;
        int cnt = _ammoTypes.CountUsed();

        // Check if we have an existing view holder
        // of if we need a new one.
        if (cnt < _holders.Size())
        {
            holder = _holders[cnt];
        }
        else
        {
            holder = RR_AmmoTypeHolder.Create(_context);
            _holders.Push(holder);
        }

        return holder;
    }

    //===========================================================================
    //
    // RR_AmmoTableAdapter::GetOrCreateView
    //
    // Creates or converts a view to display for an ammo type.
    //
    //===========================================================================
    override WL_View GetOrCreateView(int index, WL_View convertView)
    {
        return _holders[index].GetView();
    }
}
