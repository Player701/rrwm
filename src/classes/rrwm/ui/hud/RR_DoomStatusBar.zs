//===========================================================================
//
// RR_DoomStatusBar
//
// Reimplementation of the Doom status bar
// with RRWM-specific feature support.
//
//===========================================================================
class RR_DoomStatusBar ui
{
    private BaseStatusBar _sb;

    private HUDFont _hudFont;
    private HUDFont _indexFont;
    private HUDFont _amountFont;
    private InventoryBarState _diparms;

    //===========================================================================
    //
    // RR_DoomStatusBar::Create
    //
    // Creates a new instance of the RR_DoomStatusBar class.
    //
    //===========================================================================
    static RR_DoomStatusBar Create(BaseStatusBar sb)
    {
        let dsb = new('RR_DoomStatusBar');
        dsb.Init(sb);

        return dsb;
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(BaseStatusBar sb)
    {
        _sb = sb;
        _sb.SetSize(32, 320, 200);

        // Create the font used for the fullscreen HUD
        Font fnt = "HUDFONT_DOOM";
        _hudFont = HUDFont.Create(fnt, fnt.GetCharWidth("0"), Mono_CellLeft, 1, 1);
        fnt = "INDEXFONT_DOOM";
        _indexFont = HUDFont.Create(fnt, fnt.GetCharWidth("0"), Mono_CellLeft);
        _amountFont = HUDFont.Create("INDEXFONT");
        _diparms = InventoryBarState.Create();
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::Draw
    //
    // Draws the status bar.
    //
    //===========================================================================
    void Draw(double TicFrac)
    {
        _sb.BeginStatusBar();
        DrawMainBar(TicFrac);
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawMainBar
    //
    // Draws the main part of the statur bar.
    //
    //===========================================================================
    private void DrawMainBar(double TicFrac)
    {
        _sb.DrawImage("STBAR", (0, 168), BaseStatusBar.DI_ITEM_OFFSETS);
        _sb.DrawImage("STTPRCNT", (90, 171), BaseStatusBar.DI_ITEM_OFFSETS);
        _sb.DrawImage("STTPRCNT", (221, 171), BaseStatusBar.DI_ITEM_OFFSETS);

        int ammoCount;
        bool hasAmmo = TryGetAmmoCount(ammoCount);

        if (hasAmmo) _sb.DrawString(_hudFont, _sb.FormatNumber(ammoCount, 3), (44, 171), BaseStatusBar.DI_TEXT_ALIGN_RIGHT|BaseStatusBar.DI_NOSHADOW);
        _sb.DrawString(_hudFont, _sb.FormatNumber(_sb.CPlayer.health, 3), (90, 171), BaseStatusBar.DI_TEXT_ALIGN_RIGHT|BaseStatusBar.DI_NOSHADOW);
        _sb.DrawString(_hudFont, _sb.FormatNumber(GetArmorAmount(), 3), (221, 171), BaseStatusBar.DI_TEXT_ALIGN_RIGHT|BaseStatusBar.DI_NOSHADOW);

        DrawBarKeys();
        DrawBarAmmo();

        if (deathmatch || teamplay)
        {
            _sb.DrawString(_hudFont, _sb.FormatNumber(_sb.CPlayer.FragCount, 3), (138, 171), BaseStatusBar.DI_TEXT_ALIGN_RIGHT);
        }
        else
        {
            DrawBarWeapons();
        }

        if (multiplayer)
        {
            _sb.DrawImage("STFBANY", (143, 168), BaseStatusBar.DI_ITEM_OFFSETS|BaseStatusBar.DI_TRANSLATABLE);
        }

        if (_sb.CPlayer.mo.InvSel != null && !Level.NoInventoryBar)
        {
            _sb.DrawInventoryIcon(_sb.CPlayer.mo.InvSel, (160, 198), BaseStatusBar.DI_DIMDEPLETED);
            if (_sb.CPlayer.mo.InvSel.Amount > 1)
            {
                _sb.DrawString(_amountFont, _sb.FormatNumber(_sb.CPlayer.mo.InvSel.Amount), (175, 198-_indexFont.mFont.GetHeight()), BaseStatusBar.DI_TEXT_ALIGN_RIGHT, Font.CR_GOLD);
            }
        }
        else
        {
            _sb.DrawTexture(_sb.GetMugShot(5), (143, 168), BaseStatusBar.DI_ITEM_OFFSETS);
        }
        if (_sb.isInventoryBarVisible())
        {
            _sb.DrawInventoryBar(_diparms, (48, 169), 7, BaseStatusBar.DI_ITEM_LEFT_TOP);
        }

    }

    //===========================================================================
    //
    // RR_DoomStatusBar::TryGetAmmoCount
    //
    // Checks whether the player's ready weapon uses ammo,
    // and returns the current ammo or clip count if it does.
    //
    //===========================================================================
    private bool TryGetAmmoCount(out int result)
    {
        int unused;
        bool hasAmmo = RR_HudHooks.IsWeaponReloadable(_sb.CPlayer.ReadyWeapon, result, unused);

        if (!hasAmmo)
        {
            let a = _sb.GetCurrentAmmo();

            if (a != null)
            {
                hasAmmo = true;
                result = a.Amount;
            }
        }

        return hasAmmo;
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::GetArmorAmount
    //
    // Returns the total amount of armor the player has.
    //
    //===========================================================================
    private int GetArmorAmount()
    {
        int total = _sb.GetArmorAmount();
        let shield = RR_HudHooks.GetShieldItem(_sb.CPlayer);

        if (shield != null && shield.Amount > 0)
        {
            total += shield.Amount;
        }

        return total;
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawBarKeys
    //
    // Draws the key icons.
    //
    //===========================================================================
    private void DrawBarKeys()
    {
        bool locks[6];
        String image;
        for (int i = 0; i < 6; i++) locks[i] = _sb.CPlayer.mo.CheckKeys(i + 1, false, true);
        // key 1
        if (locks[1] && locks[4]) image = "STKEYS6";
        else if (locks[1]) image = "STKEYS0";
        else if (locks[4]) image = "STKEYS3";
        _sb.DrawImage(image, (239, 171), BaseStatusBar.DI_ITEM_OFFSETS);
        // key 2
        if (locks[2] && locks[5]) image = "STKEYS7";
        else if (locks[2]) image = "STKEYS1";
        else if (locks[5]) image = "STKEYS4";
        else image = "";
        _sb.DrawImage(image, (239, 181), BaseStatusBar.DI_ITEM_OFFSETS);
        // key 3
        if (locks[0] && locks[3]) image = "STKEYS8";
        else if (locks[0]) image = "STKEYS2";
        else if (locks[3]) image = "STKEYS5";
        else image = "";
        _sb.DrawImage(image, (239, 191), BaseStatusBar.DI_ITEM_OFFSETS);
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawBarAmmo
    //
    // Draws the ammo table.
    //
    //===========================================================================
    private void DrawBarAmmo()
    {
        for (int i = 0; i < 4; i++)
        {
            let [amt1, maxamt] = _sb.GetAmount(RR_HudHooks.GetStatusBarAmmoType(i));
            _sb.DrawString(_indexFont, _sb.FormatNumber(amt1, 3), (288, 173 + 6 * i), BaseStatusBar.DI_TEXT_ALIGN_RIGHT);
            _sb.DrawString(_indexFont, _sb.FormatNumber(maxamt, 3), (314, 173 + 6 * i), BaseStatusBar.DI_TEXT_ALIGN_RIGHT);
        }
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawBarWeapons
    //
    // Draws the weapon slot indicators.
    //
    //===========================================================================
    private void DrawBarWeapons()
    {
        _sb.DrawImage("STARMS", (104, 168), BaseStatusBar.DI_ITEM_OFFSETS);

        for (int i = 0; i < 6; i++)
        {
            int s = i + 2;
            string texName = string.Format("ST%sNUM%d", _sb.CPlayer.HasWeaponsInSlot(s) ? "YS" : "G", s);
            _sb.DrawImage(texName, (111 + 12 * (i % 3), 172 + 10 * (i / 3)), BaseStatusBar.DI_ITEM_OFFSETS);
        }
    }
}
