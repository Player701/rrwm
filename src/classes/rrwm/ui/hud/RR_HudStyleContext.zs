//===========================================================================
//
// RR_HudStyleContext
//
// Contains convenience methods to build HUD elements for the RRWM HUD
// in a common style.
//
//===========================================================================
class RR_HudStyleContext ui
{
    private WL_FontGroup _bigFont;
    private WL_FontGroup _smallFont;

    private WL_FontGroup _bigNumberFont;
    private WL_FontGroup _smallNumberFont;
    private WL_FontGroup _tinyNumberFont;

    private WL_TextureViewCollapser _textureCollapser;

    const FONT_SHADOW = 2;

    const DEFAULT_AMMO_COLOR = Font.CR_RED;
    const DEFAULT_COLOR = Font.CR_UNTRANSLATED;

    const ICON_SIZE_BIG = 20;
    const ICON_SIZE_SMALL = 10;
    const ICON_MARGIN = 4;

    //===========================================================================
    //
    // RR_HudStyleContext::Create
    //
    // Creates a new instance of the RR_HudStyleContext class.
    //
    //===========================================================================
    static RR_HudStyleContext Create(WL_HudCanvas canvas)
    {
        let c = new('RR_HudStyleContext');

        c.Init(canvas);
        return c;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_HudCanvas canvas)
    {
        // This font is used for big slashes and other big text
        _bigFont = CreateBigFont(canvas);

        // This font is used for primary numbers (health, armor, ammo)
        // as well as for air supply and powerup timers.
        _bigNumberFont = CreateBigNumberFont(canvas);

        // This font is used for small delimiters (slashes, colons)
        // and item tags.
        _smallFont = BuildSmallFont(canvas)
            .CreateFontGroup();

        // This font is used for small numbers (level stats, ammo table).
        _smallNumberFont = BuildSmallFont(canvas)
            .SetMonospacingMode(Mono_CellCenter)
            .MonospaceForDigits()
            .CreateFontGroup();

        // This font is used for inventory item counters and weapon slots.
        _tinyNumberFont = CreateTinyNumberFont(canvas);

        _textureCollapser = WL_TextureViewCollapser.Create();
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigFont
    //
    // Creates the big font to use for text.
    //
    //===========================================================================
    private static WL_FontGroup CreateBigFont(WL_HudCanvas canvas)
    {
        return canvas.BuildFontUpon(BigFont)
            .SetShadowUniform(FONT_SHADOW)
            .UseBackupFont(OriginalBigFont)
            .CreateFontGroup();
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigNumberFont
    //
    // Creates the font to use for primary counters (health, armor, ammo).
    //
    //===========================================================================
    private static WL_FontGroup CreateBigNumberFont(WL_HudCanvas canvas)
    {
        Font fnt;
        bool isRavenFont = false;

        // AltHUD logic
        switch (GameInfo.gametype)
        {
            case GAME_Heretic:
            case GAME_Hexen:
                fnt = Font.FindFont("HUDFONT_RAVEN");
                isRavenFont = fnt != null;
                break;
            case GAME_Strife:
                fnt = BigFont;
                break;
            default:
                fnt = Font.FindFont("HUDFONT_DOOM");
                break;
        }

        if (fnt == null)
        {
            fnt = BigFont;
        }

        let builder = canvas.BuildFontUpon(fnt);

        if (isRavenFont)
        {
            // The Raven HUD font is rather small, so it needs to be scaled up
            // to work with the big font. Also, it reports having a bigger height
            // than it actually does, so we compensate for that with negative line spacing.
            builder.SetBaseScaleUniform(2);
        }
        else
        {
            // The Raven font already looks like it has a sort of a shadow,
            // so we won't add our own shadow in this case.
            builder.SetShadowUniform(FONT_SHADOW);
        }

        return builder
            .SetMonospacingMode(fnt == BigFont ? Mono_CellCenter : Mono_CellLeft)
            .MonospaceForDigits(isRavenFont ? 1 : 0)
            .CreateFontGroup();
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateTinyNumberFont
    //
    // Creates the font to use for inventory item amounts and weapon slots.
    //
    //===========================================================================
    private static WL_FontGroup CreateTinyNumberFont(WL_HudCanvas canvas)
    {
        Font fnt;

        // Enhanced logic
        switch(GameInfo.gametype)
        {
            case GAME_Heretic:
            case GAME_Hexen:
                fnt = Font.FindFont("INDEXFONT_RAVEN");
                break;
            case GAME_Strife:
                fnt = Font.FindFont("INDEXFONT_STRIFE_GREEN");
                break;
            default:
                fnt = Font.FindFont("INDEXFONT_DOOM");
                break;
        }

        if (fnt == null)
        {
            fnt = Font.FindFont("INDEXFONT");

            // Still no font?!
            if (fnt == null)
            {
                fnt = ConFont;
            }
        }

        return canvas.BuildFontUpon(fnt)
            .SetMonospacingMode(Mono_CellLeft)
            .MonospaceForDigits()
            .CreateFontGroup();
    }

    //===========================================================================
    //
    // RR_HudStyleContext::BuildSmallFont
    //
    // Returns a FontBuilder instance that builds a small font.
    //
    //===========================================================================
    private static WL_FontBuilder BuildSmallFont(WL_HudCanvas canvas)
    {
        return canvas.BuildFontUpon(SmallFont)
            .SetShadowUniform(FONT_SHADOW)
            .UseBackupFont(OriginalSmallFont);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::GetAmmoColor
    //
    // Returns the color to use for the provided ammo item.
    //
    //===========================================================================
    static int GetAmmoColor(Ammo ammoItem)
    {
        return GetAmmoColorByType(ammoItem.GetClass());
    }

    //===========================================================================
    //
    // RR_HudStyleContext::GetAmmoColorByType
    //
    // Returns the color to use for the provided ammo item by its type.
    //
    //===========================================================================
    static int GetAmmoColorByType(class<Ammo> ammoType)
    {
        name result = RR_HudHooks.GetSpecialAmmoColor(ammoType);
        let clrName = result != 'None' ? result : GetDefaultAmmoColor(ammoType);

        return clrName != 'None' ? Font.FindFontColor(clrName) : DEFAULT_AMMO_COLOR;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::GetDefaultAmmoColor
    //
    // Returns the default color for an ammo type.
    //
    //===========================================================================
    private static name GetDefaultAmmoColor(class<Ammo> ammoType)
    {
        switch (ammoType.GetClassName())
        {
            // Doom ammo types
            case 'Clip':
            case 'Shell':
                return 'Red';
            case 'RocketAmmo':
                return 'Brown';
            case 'Cell':
                return 'Green';
            // Heretic ammo types
            case 'GoldWandAmmo':
                return 'Gold';
            case 'CrossbowAmmo':
                return 'Green';
            case 'MaceAmmo':
                return 'Grey';
            case 'BlasterAmmo':
                return 'LightBlue';
            case 'SkullRodAmmo':
                return 'Red';
            case 'PhoenixRodAmmo':
                return 'Orange';
            // Hexen ammo types
            case 'Mana1':
                return 'LightBlue';
            case 'Mana2':
                return 'Green';
        }

        return 'None';
    }

    //===========================================================================
    //
    // RR_HudStyleContext::SetTableStyle
    //
    // Applies the common table style to the provided view.
    //
    //===========================================================================
    void SetTableStyle(WL_View table) const
    {
        // This negative padding compensates for row padding
        // so that it only appears between the rows themselves.
        // Neat!
        table.SetPadding(WL_Margins.Bottom(-2));

        // All HUD tables in RRWM HUD center their contents
        // vertically and have right horizontal alignment.
        table.SetGravity(WL_Gravity.Create(WL_HG_Right, WL_VG_Center));
    }

    //===========================================================================
    //
    // RR_HudStyleContext::SetRowStyle
    //
    // Applies the common table row style to the provided view.
    //
    //===========================================================================
    void SetRowStyle(WL_View row) const
    {
        row.SetPadding(WL_Margins.Bottom(2));
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateTable
    //
    // Creates a table for use in the HUD with the predefined parameters.
    //
    //===========================================================================
    WL_TableLayout CreateTable(bool reversed = false, bool columnsReversed = false) const
    {
        let tl = WL_TableLayout.Create();

        // All HUD tables in RRWM HUD have vertically-oriented rows
        // and center their contents vertically.
        tl.SetOrientation(WL_O_Vertical);

        tl.SetReversed(reversed);
        tl.SetColumnsReversed(columnsReversed);

        SetTableStyle(tl);

        return tl;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateRow
    //
    // Creates a TableRow for use in the HUD
    // with the predefined parameters.
    //
    //===========================================================================
    WL_TableRow CreateRow() const
    {
        let tr = WL_TableRow.Create();
        SetRowStyle(tr);

        return tr;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateCounter
    //
    // Creates and returns a Counter that uses the given font.
    //
    //===========================================================================
    private WL_Counter CreateCounter(WL_FontGroup fnt, int clr = DEFAULT_COLOR, int minDecimalPlaces = 1) const
    {
        let c = WL_Counter.Create();
        c.SetFont(fnt);
        c.SetColor(clr);
        c.SetMinDecimalPlaces(minDecimalPlaces);

        return c;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigCounter
    //
    // Creates and returns a Counter that is used in RRWM HUD for primary values
    // (health, armor, ammo, powerups).
    //
    //===========================================================================
    WL_Counter CreateBigCounter(int clr = DEFAULT_COLOR) const
    {
        return CreateCounter(_bigNumberFont, clr);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateSmallCounter
    //
    // Creates and returns a Counter that is used in RRWM HUD for secondary
    // values (level stats, ammo table).
    //
    //===========================================================================
    WL_Counter CreateSmallCounter(int clr = DEFAULT_COLOR) const
    {
        return CreateCounter(_smallNumberFont, clr);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateTinyCounter
    //
    // Creates and returns a Counter that is used in RRWM HUD
    // for inventory item amounts and weapon slots.
    //
    //===========================================================================
    WL_Counter CreateTinyCounter() const
    {
        return CreateCounter(_tinyNumberFont);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateText
    //
    // Creates and returns a TextView with the specified parameters.
    //
    //===========================================================================
    private WL_TextView CreateText(string text, bool localized, WL_FontGroup fnt, int clr) const
    {
        let tv = WL_TextView.Create();
        tv.SetText(text);
        tv.SetLocalized(localized);
        tv.SetFont(fnt);
        tv.SetColor(clr);

        return tv;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigSlash
    //
    // Creates and returns a TextView that prints the provided text
    // using the big font. Adjusts the padding for use as a delimiter.
    //
    //===========================================================================
    WL_TextView CreateBigDelimiter(string text = "/", int clr = DEFAULT_COLOR) const
    {
        let tv = CreateText(text, false, _bigFont, clr);
        tv.SetLineTrimming(true);
        tv.SetPadding(WL_Margins.Horizontal(4, 4));

        return tv;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateSmallDelimiter
    //
    // Creates and returns a TextView that prints the provided text
    // using the small font. Adjusts the padding for use as a delimiter.
    //
    //===========================================================================
    WL_TextView CreateSmallDelimiter(string text = "/", int clr = DEFAULT_COLOR) const
    {
        let tv = CreateText(text, false, _smallFont, clr);
        tv.SetLineTrimming(true);
        tv.SetPadding(WL_Margins.Horizontal(2, 2));

        return tv;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateSmallText
    //
    // Creates and returns a TextView that prints the given text
    // using the small font.
    //
    //===========================================================================
    WL_TextView CreateSmallText(string text = "", bool localized = false, int clr = Font.CR_UNTRANSLATED) const
    {
        return CreateText(text, localized, _smallFont, clr);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigText
    //
    // Creates and returns a TextView that prints the given text
    // using the big font.
    //
    //===========================================================================
    WL_TextView CreateBigText(string text = "", bool localized = false, int clr = Font.CR_UNTRANSLATED) const
    {
        return CreateText(text, localized, _bigFont, clr);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateIcon
    //
    // Creates and returns a TextureView with equal width and height
    // and the provided layout parameters.
    //
    //===========================================================================
    WL_TextureView CreateIcon(double size, WL_LayoutParams lp, bool autoCollapse) const
    {
        let iv = WL_TextureView.Create();

        iv.SetWidth(WL_Dimension.Fixed(size));
        iv.SetHeight(WL_Dimension.Fixed(size));
        iv.SetLayoutParams(lp);

        if (autoCollapse)
        {
            _textureCollapser.ObserveView(iv);
        }

        return iv;
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateBigIcon
    //
    // Creates and returns a TextureView used in RRWM HUD for primary icons
    // (health, armor, ammo, powerups).
    //
    //===========================================================================
    WL_TextureView CreateBigIcon(WL_LayoutParams lp, bool autoCollapse) const
    {
        return CreateIcon(ICON_SIZE_BIG, lp, autoCollapse);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::CreateSmallicon
    //
    // Creates and returns a TextureView used in RRWM HUD for secondary icons
    // (ammo table, level stats in icon mode).
    //
    //===========================================================================
    WL_TextureView CreateSmallicon(WL_LayoutParams lp, bool autoCollapse) const
    {
        return CreateIcon(ICON_SIZE_SMALL, lp, autoCollapse);
    }

    //===========================================================================
    //
    // RR_HudStyleContext::WrapBigIcon
    //
    // Wraps the provided view into a FrameLayout so that it retains the height
    // when the inner view (the icon) is collapsed.
    //
    //===========================================================================
    WL_View WrapBigIcon(WL_View icon) const
    {
        let fl = WL_FrameLayout.Create();
        fl.SetHeight(WL_Dimension.Fixed(ICON_SIZE_BIG));
        fl.AddView(icon);

        return fl;
    }
}
