//===========================================================================
//
// RR_Hud
//
// Implements the RRWM HUD.
//
//===========================================================================
class RR_Hud : WL_HudBase
{
    // This is the renderer for fragments
    // whose scale is tied to the crosshair scale.
    private WL_HudRenderer _xhairScaleRenderer;

    // Implements the Doom statur bar.
    private RR_DoomStatusBar _dsbar;

    //===========================================================================
    //
    // RR_Hud::IsDoomLikeGame
    //
    // Returns true if the current game is Doom-like (Doom or Chex).
    //
    //===========================================================================
    static bool IsDoomLikeGame()
    {
        return GameInfo.GameType & GAME_DoomChex;
    }

    //===========================================================================
    //
    // RR_Hud::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    override void Init()
    {
        Super.Init();

        SetNativePowerupsHudEnabled(false);

        // Only initialize the status bar for Doom-like games.
        if (IsDoomLikeGame())
        {
            _dsbar = RR_DoomStatusBar.Create(self);
        }
    }

    //===========================================================================
    //
    // RR_Hud::CreateRootView
    //
    // Creates the view hierarchy for the fullscreen HUD.
    //
    //===========================================================================
    override WL_View CreateRootView(WL_HudCanvas canvas)
    {
        let context = RR_HudStyleContext.Create(canvas);

        let fl = WL_FrameLayout.Create();

        fl.SetWidth(WL_Dimension.MatchParent());
        fl.SetHeight(WL_Dimension.MatchParent());
        fl.SetPadding(WL_Margins.All(4));

        // ******* BOTTOM - left to right *******

        let llBottom = WL_LinearLayout.Create();
        llBottom.SetWidth(WL_Dimension.MatchParent());
        llBottom.SetGravity(WL_Gravity.Create(WL_HG_Left, WL_VG_Bottom));
        llBottom.SetLayoutParams(WL_FrameLayout.CreateParams(gravity: WL_Gravity.Vertical(WL_VG_Bottom)));

        // * First column - Bottom left (bottom to top) *

        let llBottomLeft = WL_LinearLayout.Create();
        llBottomLeft.SetOrientation(WL_O_Vertical);
        llBottomLeft.SetGravity(WL_Gravity.Horizontal(WL_HG_Left));
        llBottomLeft.SetReversed(true);
        llBottomLeft.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Right(2)));

        // Vitals (health, armor etc.)
        let vitals = RR_VitalsFragment.Create(context);
        vitals.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Top(2)));
        llBottomLeft.AddView(vitals);

        // Level stats
        let levelStats = RR_LevelStatsFragment.Create(context);
        llBottomLeft.AddView(levelStats);

        // Frags
        let frags = RR_FragsFragment.Create(context);
        llBottomLeft.AddView(frags);

        llBottom.AddView(llBottomLeft);

        // * Second column *

        // Mugshot
        let mugshot = RR_MugshotFragment.Create(context);
        mugshot.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Create(4, 0, 0, 2)));
        llBottom.AddView(mugshot);

        // * Third column *

        // Keys
        let keys = CreateKeysFragment(context);
        keys.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Create(4, 0, 0, 2)));
        llBottom.AddView(keys);

        // * Fourth column - inventory *

        // Use a FrameLayout to prevent slip of the right-side counters
        // when the inventory bar is gone.
        let invFrame = WL_FrameLayout.Create();
        invFrame.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));

        let inv = RR_InventoryFragment.Create(context);
        inv.SetLayoutParams(WL_FrameLayout.CreateParams(margins: WL_Margins.Horizontal(10, 10)));
        invFrame.AddView(inv);

        llBottom.AddView(invFrame);

        // * Fifth column - Bottom right (bottom to top) *

        let llBottomRight = WL_LinearLayout.Create();
        llBottomRight.SetOrientation(WL_O_Vertical);
        llBottomRight.SetGravity(WL_Gravity.Horizontal(WL_HG_Right));
        llBottomRight.SetReversed(true);

        // Ammo counters
        let ammo = RR_AmmoFragment.Create(context);
        ammo.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Top(2)));
        llBottomRight.AddView(ammo);

        // Weapon slots
        let slots = RR_WeaponSlotsFragment.Create(context);
        slots.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Top(4)));
        llBottomRight.AddView(slots);

        // Ammo table
        let ammoTable = CreateAmmoTableFragment(context);
        ammoTable.SetLayoutParams(WL_LinearLayout.CreateParams(margins: WL_Margins.Top(2)));
        llBottomRight.AddView(ammoTable);

        llBottom.AddView(llBottomRight);

        fl.AddView(llBottom);

        // ******* END BOTTOM *******

        // ******* TOP RIGHT *******

        // Powerups
        let powerups = CreatePowerupsFragment(context);
        powerups.SetLayoutParams(WL_FrameLayout.CreateParams(gravity: WL_Gravity.Create(WL_HG_Right, WL_VG_Top)));

        fl.AddView(powerups);

        // ******* END TOP RIGHT *******

        // Register all HUD fragments
        RegisterHudFragment(vitals);
        RegisterHudFragment(ammo);
        RegisterHudFragment(slots);
        RegisterHudFragment(levelStats);
        RegisterHudFragment(mugshot);
        RegisterHudFragment(keys);
        RegisterHudFragment(ammoTable);
        RegisterHudFragment(inv);
        RegisterHudFragment(frags);
        RegisterHudFragment(powerups);

        // Init crosshair scale-dependent renderer
        InitCrosshairScaleRenderer(context);

        return fl;
    }

    //===========================================================================
    //
    // RR_Hud::InitCrosshairScaleRenderer
    //
    // Initializes the canvas for fragments whose size is tied
    // to crosshair scale.
    //
    //===========================================================================
    private void InitCrosshairScaleRenderer(RR_HudStyleContext context)
    {
        // The low ammo indicator is drawn on a separate canvas
        // that is tied to crosshair scale.

        let canvas = RR_CrosshairCanvas.Create();
        // First, create the view hierarchy.
        let fl = WL_FrameLayout.Create();
        fl.SetWidth(WL_Dimension.MatchParent());
        fl.SetHeight(WL_Dimension.MatchParent());
        fl.SetGravity(WL_Gravity.Center());

        let fnt = canvas.BuildFontUpon(BigFont)
            .UseBackupFont(OriginalBigFont)
            .CreateFontGroup();

        let enemyHealth = RR_EnemyHealthFragment.Create();
        enemyHealth.SetLayoutParams(WL_FrameLayout.CreateParams(margins: WL_Margins.Bottom(312)));
        fl.AddView(enemyHealth);
//<ExcludeFromHudPackage>

        let lowAmmo = RR_LowAmmoFragment.Create(fnt);
        lowAmmo.SetLayoutParams(WL_FrameLayout.CreateParams(margins: WL_Margins.Top(312)));
        fl.AddView(lowAmmo);
//</ExcludeFromHudPackage>

        // Register the fragments
        RegisterHudFragment(enemyHealth);
//<ExcludeFromHudPackage>
        RegisterHudFragment(lowAmmo);
//</ExcludeFromHudPackage>

        // Now initialize the renderer with our canvas.
        // The renderer is called in our Draw override.
        _xhairScaleRenderer = WL_HudRenderer.Create(canvas, fl);
    }

    //===========================================================================
    //
    // RR_Hud::Draw
    //
    // Draws the RRWM HUD.
    //
    //===========================================================================
    override void Draw(int state, double ticFrac)
    {
        // Draw fragments whose size is tied to crosshair scale
        _xhairScaleRenderer.Render(state, GetDebugLevel());

        // Draw the main HUD.
        Super.Draw(state, ticFrac);

        // Draw the status bar if applicable.
        if (state == HUD_StatusBar && _dsbar != null)
        {
            _dsbar.Draw(ticFrac);
        }
    }

    //===========================================================================
    //
    // RR_Hud::CreateKeysFragment
    //
    // Creates the HUD fragment to display keys.
    //
    //===========================================================================
    private static WL_HudFragment CreateKeysFragment(RR_HudStyleContext context)
    {
        let lm = WL_GridLayoutManager.Create(WL_O_Horizontal, 3, columnsReversed: true);

        // Assign padding to the layout to compensate for element padding
        double padding = RR_KeysAdapter.PADDING_RIGHT_TOP;
        lm.GetLayout().SetPadding(WL_Margins.Create(0, -padding, -padding, 0));

        let f = WL_HudListFragment.Create(RR_KeysAdapter.Create(context), lm);
        f.SetFullscreenOnly(true);

        return f;
    }

    //===========================================================================
    //
    // RR_Hud::CreateAmmoTableFragment
    //
    // Creates the HUD fragment to display the ammo table.
    //
    //===========================================================================
    private static WL_HudFragment CreateAmmoTableFragment(RR_HudStyleContext context)
    {
        let lm = WL_TableLayoutManager.Create(WL_O_Vertical);
        context.SetTableStyle(lm.GetLayout());

        let f = WL_HudListFragment.Create(RR_AmmoTableAdapter.Create(context), lm);
        f.SetFullscreenOnly(true);

        return f;
    }

    //===========================================================================
    //
    // RR_Hud::CreatePowerupsFragment
    //
    // Creates the HUD fragment to display powerups.
    //
    //===========================================================================
    private static WL_HudFragment CreatePowerupsFragment(RR_HudStyleContext context)
    {
        let lm = WL_LinearLayoutManager.Create(WL_O_Vertical, reversed: true);
        context.SetTableStyle(lm.GetLayout());

        return WL_HudListFragment.Create(RR_PowerupsAdapter.Create(context), lm);
    }

    //===========================================================================
    //
    // RR_Hud::GetDebugLevel
    //
    // Gets the debug level based on the value of the corresponding setting.
    //
    //===========================================================================
    override int GetDebugLevel() const
    {
        return RR_Settings.InUi(RR_SETTING_HUD_DEBUG).GetInt();
    }
}
