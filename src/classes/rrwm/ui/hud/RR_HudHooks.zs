//===========================================================================
//
// RR_HudHooks
//
// Implements a bridge between the RRWM HUD and RRWM-specific features.
//
// This class can be replaced to provide a standalone distribution
// of the RRWM HUD, decoupled from RRWM itself.
//
//===========================================================================
class RR_HudHooks ui
{
    //===========================================================================
    //
    // RR_HudHooks::IsWeaponReloadable
    //
    // Checks if the provided weapon is reloadable,
    // and if it is, returns its clip capacity and clip amount.
    //
    //===========================================================================
    static bool IsWeaponReloadable(Weapon weap, out int clipAmount, out int clipCapacity)
    {
        let rel = RR_ReloadableWeapon(weap);
        bool result = rel != null;

        if (rel != null)
        {
            clipAmount = rel.GetClipAmount();
            clipCapacity = rel.GetClipCapacity();

            return true;
        }

        return false;
    }

    //===========================================================================
    //
    // RR_HudHooks::IsAmmoInTable
    //
    // Checks if the provided ammo type should be included in the ammo table.
    //
    //===========================================================================
    static bool IsAmmoInTable(class<Ammo> ammoType, bool fromWeapon)
    {
        let a = RR_Ammo(GetDefaultByType(ammoType));

        if (a != null)
        {
            // Check if we need to show this type at all
            if (!a.IsInAmmoTable())
            {
                return false;
            }

            // If we do, either show if this type is added from a weapon,
            // or if it's relevant for the current game.
            return fromWeapon || a.IsGameRelevant();
        }

        // Always show other ammo types.
        return true;
    }

    //===========================================================================
    //
    // RR_HudHooks::GetSpecialAmmoColor
    //
    // Returns the name of the color to use for the provided ammo type.
    // If there is no special color found, this method should return 'None'.
    //
    //===========================================================================
    static name GetSpecialAmmoColor(class<Ammo> ammoType)
    {
        let a = RR_Ammo(GetDefaultByType(ammoType));
        return a != null ? a.HudColor : 'None';
    }

    //===========================================================================
    //
    // RR_HudHooks::GetSpecialPowerupProperties
    //
    // Returns special powerup properties for a given powerup,
    // if they exist.
    //
    //===========================================================================
    static bool GetSpecialPowerupProperties(Powerup powerup, out bool isTimeVisible, out double alpha, out name clrTime)
    {
        // See if this is a RRWM powerup, which can tell us more about itself.
        let p = RR_Powerup(powerup);
        bool result = p != null;

        if (result)
        {
            // Use parameters from the RRWM powerup.
            isTimeVisible = p.IsTimeVisibleInHud();
            alpha = p.HudAlpha;
            clrTime = p.HudColor;
        }

        return result;
    }

    //===========================================================================
    //
    // RR_HudHooks::GetStatusBarAmmoType
    //
    // Returns the ammo type to display in the status bar ammo table
    // for the selected index.
    //
    //===========================================================================
    static name GetStatusBarAmmoType(int index)
    {
        switch(index)
        {
            case 0:
                return 'RR_Clip';
            case 1:
                return 'RR_Shell';
            case 2:
                return RR_Settings.InUi(RR_SETTING_RAILGUN).IsOn() ? 'RR_RailAmmo' : 'RR_RocketAmmo';
            case 3:
                return 'RR_Cell';
        }

        ThrowAbortException("Invalid index specified.");
        return 'None';
    }

    //===========================================================================
    //
    // RR_HudHooks::GetShieldItem
    //
    // Returns the super armor if the player has it in a separate slot.
    //
    //===========================================================================
    static Inventory GetShieldItem(PlayerInfo player)
    {
        return player.mo.FindInventory('RR_SuperArmorShieldPickup');
    }

    //===========================================================================
    //
    // RR_HudHooks::CanShowClipIndicators
    //
    // Returns true if the HUD should display the clip amount indicators
    // underneath the weapon slot indicators.
    //
    //===========================================================================
    static bool CanShowClipIndicators()
    {
        return RR_Settings.InUi(RR_SETTING_HUD_WEAPONSLOTS_CLIP).IsOn();
    }
}
