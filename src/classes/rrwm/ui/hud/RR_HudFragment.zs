//===========================================================================
//
// RR_HudFragment
//
// Base class for most HUD fragments used by the RRWM HUD.
//
//===========================================================================
class RR_HudFragment : WL_HudFragment abstract
{
    private RR_HudStyleContext _styleContext;

    //===========================================================================
    //
    // RR_HudFragment::GetContext
    //
    // Returns the RRWM HUD context instance.
    //
    //===========================================================================
    protected RR_HudStyleContext GetStyleContext() const
    {
        return _styleContext;
    }

    //===========================================================================
    //
    // RR_HudFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(RR_HudStyleContext context)
    {
        _styleContext = context;
        SetFullscreenOnly(true);

        Super.Init();
    }
}
