//===========================================================================
//
// RR_MenuBase
//
// Base class for RRWM menus with dynamic item generation.
//
//===========================================================================
class RR_MenuBase : OptionMenu abstract
{
    // Stores all menu items before they are copied back to the descriptor.
    // This is needed because there can be other items defined in MENUDEF.
    // We add our own items both before and after them, so we cannot modify
    // the existing mItems array in-place.
    private Array<OptionMenuItem> _allItems;

    //===========================================================================
    //
    // RR_MenuBase::Init
    //
    // Initializes the menu.
    //
    //===========================================================================
    override void Init(Menu parent, OptionMenuDescriptor desc)
    {
        if (desc != null)
        {
            desc.mTitle = GetTitle();
            int prevCount = desc.mItems.Size();

            // Add header items, these are non-scrollable.
            AddHeaderItems();
            int headerCount = _allItems.Size();

            // If there is more than one header item, add a gap.
            if (headerCount > 0)
            {
                AddGap();
                headerCount++;
            }

            desc.mScrollTop = headerCount;

            // Add body items.
            AddBodyItems();

            // Finalize the items.
            FinalizeMenuItems(desc);

            // Get the index of the item that was selected previously.
            int sel = desc.mSelectedItem;

            // If the number of items in the menu changed
            // (this can happen in a netgame when becoming the new arbitrator),
            // reset the selected item and the scroll position.
            if (sel != -1 && prevCount != desc.mItems.Size())
            {
                desc.mSelectedItem = -1;
                desc.mScrollPos = 0;
            }
        }

        Super.Init(parent, desc);
    }

    //===========================================================================
    //
    // RR_MenuBase::GetTitle
    //
    // Returns the title of this menu.
    //
    //===========================================================================
    protected virtual string GetTitle() const
    {
        return "$RR_MENU_TITLE_MAIN";
    }

    //===========================================================================
    //
    // RR_MenuBase::AddHeaderItems
    //
    // Adds header items to the menu. These items are not scrollable
    // and always precede any MENUDEF-defined items.
    //
    //===========================================================================
    protected virtual void AddHeaderItems()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // RR_MenuBase::AddBodyItems
    //
    // Adds body items to the menu. These items are always scrollable
    // and appear after any MENUDEF-defined items.
    //
    //===========================================================================
    protected abstract void AddBodyItems();

    //===========================================================================
    //
    // RR_MenuBase::AddMenuItem
    //
    // Adds an arbitrary menu item to the items array.
    //
    //===========================================================================
    protected int AddMenuItem(OptionMenuItem item)
    {
        int index = _allItems.Size();
        _allItems.Push(item);

        return index;
    }

    //===========================================================================
    //
    // RR_MenuBase::AddStaticText
    //
    // Adds a static text menu item to the items list.
    //
    //===========================================================================
    protected void AddStaticText(string text, int color)
    {
        AddMenuItem(CreateStaticText(text, color));
    }

    //===========================================================================
    //
    // RR_MenuBase::AddHeader
    //
    // Adds a static text menu item with the header color.
    //
    //===========================================================================
    protected void AddHeader(string text)
    {
        AddStaticText(text, OptionMenuSettings.mFontColorHeader);
    }

    //===========================================================================
    //
    // RR_MenuBase::AddSubtitle
    //
    // Adds a static text menu item with the normal color.
    //
    //===========================================================================
    protected void AddSubtitle(string text)
    {
        AddStaticText(text, OptionMenuSettings.mFontColorValue);
    }

    //===========================================================================
    //
    // RR_MenuBase::AddGap
    //
    // Adds a gap (an empty menu item) to the items list.
    //
    //===========================================================================
    protected void AddGap()
    {
        AddStaticText("", Font.CR_UNTRANSLATED);
    }

    //===========================================================================
    //
    // RR_MenuBase::AddSubmenu
    //
    // Adds a submenu item to the items list.
    //
    //===========================================================================
    protected int AddSubmenu(string title, name command)
    {
        let i = new('OptionMenuItemSubmenu');
        i.Init(title, command);

        return AddMenuItem(i);
    }

    //===========================================================================
    //
    // RR_MenuBase::CreateStaticText
    //
    // Creates a new OptionMenuItem representing some static text.
    //
    //===========================================================================
    private OptionMenuItem CreateStaticText(string title, int color)
    {
        let staticText = new('OptionMenuItemStaticText');
        staticText.InitDirect(title, color);

        return staticText;
    }

    //===========================================================================
    //
    // RR_MenuBase::FinalizeMenuItems
    //
    // Replaces all menu items in the provided menu descriptor with new items
    // that have been added by calling AddMenuItem.
    //
    //===========================================================================
    private void FinalizeMenuItems(OptionMenuDescriptor desc)
    {
        desc.mItems.Clear();

        // NB: "Copy" should have been named "CopyFrom"
        desc.mItems.Copy(_allItems);
    }

    //===========================================================================
    //
    // RR_MenuBase::Drawer
    //
    // Draws the menu proper or the current help page
    // depending on whether help mode is on.
    //
    //===========================================================================
    final override void Drawer()
    {
        OnDraw();

        // Draw version info regardless of the current mode.
        // It always stays in the top right corner of the screen.
        DrawVersionString();
    }

    //===========================================================================
    //
    // RR_MenuBase::OnDraw
    //
    // Called when this menu gets drawn, calls the parent Drawer method
    // by default. This method (instead of Drawer) should be overridden
    // in subclasses to ensure that the version string always appears.
    //
    //===========================================================================
    protected virtual void OnDraw()
    {
        Super.Drawer();
    }

    //===========================================================================
    //
    // RR_MenuBase::DrawVersionString
    //
    // Draws the version string in the top right corner of the screen.
    //
    //===========================================================================
    private static void DrawVersionString()
    {
        string versionString = string.Format(
            StringTable.Localize("$RR_MENU_VERSION"),
            RR_VersionInfo.VERSION_STRING
        ) .. " ";

        let fnt = OptionFont();
        int width = fnt.StringWidth(versionString);

        Screen.DrawText(
            fnt, OptionMenuSettings.mFontColorValue,
            Screen.GetWidth() - width * CleanXfac_1, 0,
            versionString,
            DTA_CleanNoMove_1, true);
    }
}
