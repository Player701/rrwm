//===========================================================================
//
// RR_RootMenu
//
// Implements the root RRWM menu.
// Generates submenu items for settings filtered by category and scope.
//
//===========================================================================
class RR_RootMenu : RR_MenuBase
{
    //===========================================================================
    //
    // RR_RootMenu::GetTitle
    //
    // Returns the title of this menu.
    //
    //===========================================================================
    override string GetTitle() const
    {
        return "$RR_MENU_TITLE_MAIN";
    }

    //===========================================================================
    //
    // RR_RootMenu::AddBodyItems
    //
    // Adds submenu items for each setting group.
    //
    //===========================================================================
    override void AddBodyItems()
    {
        // Classic view
        AddSubmenu("$RR_MENU_CLASSIC_VIEW", 'RR_OptionsMenuClassic');

        // Filter by category
        AddGap();
        AddHeader("$RR_MENU_SUBTITLE_FILTER_CATEGORY");

        for (int i = 0; i < RR_NUM_SETTING_CATEGORIES; i++)
        {
            string title = RR_MenuStringHelper.GetCategoryString(i);
            AddSubmenu(title, GetSubmenuNameForCategory(i));
        }

        // Filter by scope
        AddGap();
        AddHeader("$RR_MENU_SUBTITLE_FILTER_SCOPE");

        for (int i = 0; i < RR_NUM_SETTING_SCOPES; i++)
        {
            string title = RR_MenuStringHelper.GetScopeString(i);
            AddSubmenu(title, GetSubmenuNameForScope(i));
        }

        // In multiplayer, add the command to apply changes and restart the level
        if (RR_MenuItemApplyChanges.IsVisible())
        {
            AddGap();
            AddMenuItem(RR_MenuItemApplyChanges.Create());
        }
    }

    //===========================================================================
    //
    // RR_RootMenu::GetSubmenuNameForCategory
    //
    // Returns the name of the submenu corresponding to the provided category.
    //
    //===========================================================================
    private static name GetSubmenuNameForCategory(E_RR_SettingCategory category)
    {
        // These are actually not class names but menu descriptor names
        // defined in MENUDEF. For consistency, we keep them the same
        // as the actual menu class names.
        switch(category)
        {
            case RR_SC_GAMEPLAY:
                return 'RR_OptionsMenuGameplay';
            case RR_SC_WEAPONS:
                return 'RR_OptionsMenuWeapons';
            case RR_SC_COSMETIC:
                return 'RR_OptionsMenuCosmetic';
            case RR_SC_HUD:
                return 'RR_OptionsMenuHud';
        }

        ThrowAbortException("RR_RootMenu: Unknown category value %d.", category);
        return 'None';
    }

    //===========================================================================
    //
    // RR_RootMenu::GetSubmenuNameForScope
    //
    // Returns the name of the submenu corresponding to the provided scope.
    //
    //===========================================================================
    private static name GetSubmenuNameForScope(E_RR_SettingScope scope)
    {
        switch(scope)
        {
            case RR_SS_USER:
                return 'RR_OptionsMenuUser';
            case RR_SC_WEAPONS:
                return 'RR_OptionsMenuServer';
            case RR_SC_COSMETIC:
                return 'RR_OptionsMenuServerPersistent';
        }

        ThrowAbortException("RR_RootMenu: Unknown scope value %d.", scope);
        return 'None';
    }
}
