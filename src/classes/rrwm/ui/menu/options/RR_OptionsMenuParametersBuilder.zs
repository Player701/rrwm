//===========================================================================
//
// RR_OptionsMenuParametersBuilder
//
// Proivdes a fluent API to create an instance of RR_OptionsMenuParameters.
//
//===========================================================================
class RR_OptionsMenuParametersBuilder ui
{
    private E_RR_SettingPropType _groupKeyType;

    private E_RR_SettingPropType _filterKeyType;
    private int _filterKey;
    private bool _filterNegated;

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::Create
    //
    // Creates a new instance of the RR_OptionsMenuParametersBuilder class.
    //
    //===========================================================================
    static RR_OptionsMenuParametersBuilder Create()
    {
        return new('RR_OptionsMenuParametersBuilder');
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::FilterByCategory
    //
    // Indicates that all settings in the menu should be filtered
    // by the provided category.
    //
    //===========================================================================
    RR_OptionsMenuParametersBuilder FilterByCategory(E_RR_SettingCategory category)
    {
        _filterKeyType = RR_SPT_CATEGORY;
        _filterKey = category;

        return self;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::FilterByScope
    //
    // Indicates that all settings in the menu should be filtered
    // by the provided scope.
    //
    //===========================================================================
    RR_OptionsMenuParametersBuilder FilterByScope(E_RR_SettingScope scope)
    {
        _filterKeyType = RR_SPT_SCOPE;
        _filterKey = scope;

        return self;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::NegateFilter
    //
    // Indicates that the filter condition should be negated.
    //
    //===========================================================================
    RR_OptionsMenuParametersBuilder NegateFilter()
    {
        if (_filterKeyType == RR_SPT_NONE)
        {
            ThrowAbortException("%s: Filter must be set.", GetClassName());
        }

        _filterNegated = true;
        return self;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::GroupByCategory
    //
    // Indicates that all settings in the menu should be grouped by category.
    //
    //===========================================================================
    RR_OptionsMenuParametersBuilder GroupByCategory()
    {
        _groupKeyType = RR_SPT_CATEGORY;
        return self;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::GroupByCategory
    //
    // Indicates that all settings in the menu should be grouped by scope.
    //
    //===========================================================================
    RR_OptionsMenuParametersBuilder GroupByScope()
    {
        _groupKeyType = RR_SPT_SCOPE;
        return self;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParametersBuilder::Build
    //
    // Creates an instance of RR_OptionsMenuParameters based on the state
    // of this instance of RR_OptionsMenuParametersBuilder.
    //
    //===========================================================================
    RR_OptionsMenuParameters Build()
    {
        return RR_OptionsMenuParameters.Create(
            _groupKeyType,
            _filterKeyType,
            _filterKey,
            _filterNegated
        );
    }
}
