//===========================================================================
//
// RR_OptionsMenuWeapons
//
// Displays option menu items for weapons settings only.
//
//===========================================================================
class RR_OptionsMenuWeapons : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuWeapons::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByScope()
            .FilterByCategory(RR_SC_WEAPONS)
            .Build();
    }
}
