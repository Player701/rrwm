//===========================================================================
//
// RR_OptionsMenuHud
//
// Displays option menu items for HUD settings only.
//
//===========================================================================
class RR_OptionsMenuHud : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuHud::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByScope()
            .FilterByCategory(RR_SC_HUD)
            .Build();
    }
}
