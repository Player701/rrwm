//===========================================================================
//
// RR_OptionsMenuGameplay
//
// Displays option menu items for gameplay settings only.
//
//===========================================================================
class RR_OptionsMenuGameplay : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuGameplay::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByScope()
            .FilterByCategory(RR_SC_GAMEPLAY)
            .Build();
    }
}
