//===========================================================================
//
// RR_OptionsMenuCosmetic
//
// Displays option menu items for cosmetic settings only.
//
//===========================================================================
class RR_OptionsMenuCosmetic : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuCosmetic::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByScope()
            .FilterByCategory(RR_SC_COSMETIC)
            .Build();
    }
}
