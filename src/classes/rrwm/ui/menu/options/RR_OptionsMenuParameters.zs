//===========================================================================
//
// RR_OptionsMenuParameters
//
// Contains filtering and grouping parameters for option menu.
//
//===========================================================================
class RR_OptionsMenuParameters ui
{
    private E_RR_SettingPropType _groupKeyType;

    private E_RR_SettingPropType _filterKeyType;
    private int _filterKey;
    private bool _filterNegated;

    //===========================================================================
    //
    // RR_OptionsMenuParameters::Create
    //
    // Creates a new instance of the RR_OptionsMenuParameters class.
    //
    //===========================================================================
    static RR_OptionsMenuParameters Create(E_RR_SettingPropType groupKeyType, E_RR_SettingPropType filterKeyType, int filterKey, bool filterNegated)
    {
        let p = new('RR_OptionsMenuParameters');
        p.Init(groupKeyType, filterKeyType, filterKey, filterNegated);

        return p;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(E_RR_SettingPropType groupKeyType, E_RR_SettingPropType filterKeyType, int filterKey, bool filterNegated)
    {
        if (groupKeyType == RR_SPT_NONE)
        {
            ThrowAbortException("%s: Group key type must be specified.", GetClassName());
        }

        if (groupKeyType == filterKeyType)
        {
            ThrowAbortException("%s: Group and filter key types must be different.", GetClassName());
        }

        _groupKeyType = groupKeyType;
        _filterKeyType = filterKeyType;
        _filterKey = filterKey;
        _filterNegated = filterNegated;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::GetGroupKeyType
    //
    // Gets the key type used for grouping.
    //
    //===========================================================================
    E_RR_SettingPropType GetGroupKeyType() const
    {
        return _groupKeyType;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::GetFilterKeyType
    //
    // Gets the key type used for filtering.
    //
    //===========================================================================
    E_RR_SettingPropType GetFilterKeyType() const
    {
        return _filterKeyType;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::GetFilterKey
    //
    // Gets the filter key value.
    //
    //===========================================================================
    int GetFilterKey() const
    {
        return _filterKey;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::IsFilterNegated
    //
    // Gets whether the filter is negated.
    //
    //===========================================================================
    bool IsFilterNegated() const
    {
        return _filterNegated;
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::GetGroupKey
    //
    // Gets the group key value for the provided setting.
    //
    //===========================================================================
    int GetGroupKey(RR_Setting setting) const
    {
        return GetPropValue(setting, _groupKeyType);
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::IsSettingRelevant
    //
    // Checks if the provided setting is relevant according to filter setup.
    //
    //===========================================================================
    bool IsSettingRelevant(RR_Setting setting) const
    {
        if (_filterKeyType == RR_SPT_NONE)
        {
            return true;
        }

        int keyValue = GetPropValue(setting,_filterKeyType);
        return _filterNegated ^ (keyValue == _filterKey);
    }

    //===========================================================================
    //
    // RR_OptionsMenuParameters::GetPropValue
    //
    // Gets the value of the either category or scope of the provided setting.
    //
    //===========================================================================
    private int GetPropValue(RR_Setting setting, E_RR_SettingPropType propType)
    {
        return propType == RR_SPT_CATEGORY
            ? setting.GetCategory()
            : setting.GetScope();
    }
}

//===========================================================================
//
// RR_SettingPropType
//
// Enumerates setting property types for filtering and grouping.
//
//===========================================================================
enum E_RR_SettingPropType
{
    // Do not filter/group
    RR_SPT_NONE,

    // Filter/group by category
    RR_SPT_CATEGORY,

    // Filter/group by scope
    RR_SPT_SCOPE
};
