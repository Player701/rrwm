//===========================================================================
//
// RR_OptionsMenuBase
//
// Base class for dynamically-generated RRWM options menus with help pages.
//
//===========================================================================
class RR_OptionsMenuBase : RR_MenuBase abstract
{
    // Maps menu items to settings.
    private Map<int, RR_Setting> _settingMap;

    // An instance of RR_OptionsMenuHelp used to draw help pages.
    private RR_OptionsMenuHelp _help;

    // Contains parameters for grouping and filtering.
    private RR_OptionsMenuParameters _parameters;

    //===========================================================================
    //
    // RR_OptionsMenuBase::Init
    //
    // Initializes this RRWM options menu.
    //
    //===========================================================================
    override void Init(Menu parent, OptionMenuDescriptor desc)
    {
        Super.Init(parent, desc);

        // Now that the descriptor has been assigned,
        // we can initialize the help menu.
        _help = RR_OptionsMenuHelp.Create(self);
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    protected abstract RR_OptionsMenuParameters CreateParameters();

    //===========================================================================
    //
    // RR_OptionsMenuBase::GetOrCreateParameters
    //
    // Creates a new or returns an existing instance
    // of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    private RR_OptionsMenuParameters GetOrCreateParameters()
    {
        if (_parameters == null)
        {
            _parameters = CreateParameters();
        }

        return _parameters;
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::GetHelpTitle
    //
    // Returns the title of this menu to be used on help pages
    // with the word "help" added to the end of it.
    //
    //===========================================================================
    virtual string GetHelpTitle() const
    {
        return "$RR_MENU_TITLE_MAIN";
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::GetTitle
    //
    // Returns the title of this menu.
    //
    //===========================================================================
    override string GetTitle() const
    {
        let params = GetOrCreateParameters();
        let filterKeyType = params.GetFilterKeyType();

        if (filterKeyType == RR_SPT_NONE || params.IsFilterNegated())
        {
            return Super.GetTitle();
        }

        int filterKey = params.GetFilterKey();

        string filterStr = filterKeyType == RR_SPT_SCOPE
            ? RR_MenuStringHelper.GetScopeStringShort(filterKey)
            : RR_MenuStringHelper.GetCategoryStringShort(filterKey);

        string titleStr = StringTable.Localize("$RR_MENU_TITLE_FILTERED");
        return string.Format(titleStr, StringTable.Localize(filterStr));
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::AddHeaderItems
    //
    // Adds the "press f1 for help" and other messages to the menu header.
    //
    //===========================================================================
    override void AddHeaderItems()
    {
//<ExcludeFromHudPackage>
        let params = GetOrCreateParameters();
        let filterKeyType = params.GetFilterKeyType();

        if (filterKeyType == RR_SPT_SCOPE && !params.IsFilterNegated())
        {
            let scope = params.GetFilterKey();
            bool canControl = scope == RR_SS_USER || players[consoleplayer].settings_controller;

            if (!canControl)
            {
                // Show a warning that server settings cannot be changed
                // if the current player cannot control them.
                AddSubtitle("$RR_MENU_SUBTITLE_MULTIPLAYER_CLIENT_WARNING");
            }
            else if (scope == RR_SS_SERVER_PERSISTENT)
            {
                // Show a warning/hint for persistent settings.
                AddPersistentSubtitle();
            }

            // Do not show "press f1 for help"
            // if the player cannnot control the settings.
            if (!canControl)
            {
                return;
            }
        }

//</ExcludeFromHudPackage>
        AddSubtitle("$RR_MENU_PRESS_F1_FOR_HELP");
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::AddPersistentSubtitle
    //
    // Adds the subtitle with a warning or hint about persistent settings
    // depending on whether the game is in multiplayer mode.
    //
    //===========================================================================
    private void AddPersistentSubtitle()
    {
        string label = multiplayer && RR_LocalEventHandler.CanRestartLevel()
            ? "$RR_MENU_SUBTITLE_SERVER_PERSISTENT_MULTIPLAYER"
            : "$RR_MENU_SUBTITLE_SERVER_PERSISTENT_WARNING";

        AddSubtitle(label);
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::AddBodyItems
    //
    // Adds menu items for all relevant settings.
    //
    //===========================================================================
    override void AddBodyItems()
    {
        Map<name, int> settingGroups;
        Array<int> groupCounts;

        let params = GetOrCreateParameters();

        // Get the global RR_Settings instance.
        let settings = RR_Settings.GetInstance();
//<ExcludeFromHudPackage>

        int nc = 0;
//</ExcludeFromHudPackage>

        // Group settings by key
        for (int i = 0; i < settings.Count(); i++)
        {
            let setting = settings.GetByIndex(i);

            if (!setting.IsVisible() || !params.IsSettingRelevant(setting))
            {
                // Filtered out
                continue;
            }

            int groupKey = params.GetGroupKey(setting);
            settingGroups.Insert(setting.GetCvarName(), groupKey);

            while (groupCounts.Size() <= groupKey)
            {
                groupCounts.Push(0);
            }

            // Add setting to this group
            int cnt = groupCounts[groupKey];
//<ExcludeFromHudPackage>

            if (cnt == 0)
            {
                nc++;
            }
//</ExcludeFromHudPackage>

            groupCounts[groupKey]++;
        }

        // Now add settings from each group
        bool added = false;

        for (int i = 0; i < groupCounts.Size(); i++)
        {
           int sc = groupCounts[i];

            if (sc == 0)
            {
                // Skip empty group
                continue;
            }

            // If this is not the first group we're adding, add a gap before it.
            if (added)
            {
                AddGap();
            }
//<ExcludeFromHudPackage>

            // Add title for the group
            AddGroupTitle(nc, i);
//</ExcludeFromHudPackage>

            // Add items for the group
            for (int j = 0; j < settings.Count(); j++)
            {
                let setting = settings.GetByIndex(j);
                let [g, found] = settingGroups.CheckValue(setting.GetCvarName());

                if (found && g == i)
                {
                    AddSettingMenuItem(setting);
                }
            }

            added = true;
        }
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::AddGroupTitle
    //
    // Adds menu items to represent the title of the group
    // with the provided index.
    //
    //===========================================================================
    protected virtual void AddGroupTitle(int nonzeroCount, int group)
    {
        let groupKeyType = GetOrCreateParameters().GetGroupKeyType();
        bool isScope = groupKeyType == RR_SPT_SCOPE;

        // Only display titles if we have more than one group
        if (nonzeroCount > 1)
        {
            string title = isScope
                ? RR_MenuStringHelper.GetScopeString(group)
                : RR_MenuStringHelper.GetCategoryString(group);

            AddHeader(title);
        }

        if (!isScope)
        {
            return;
        }

        // Show a warning that server settings cannot be changed
        // if the current player cannot control them.
        if (!players[consoleplayer].settings_controller && group != RR_SS_USER)
        {
            AddSubtitle("$RR_MENU_SUBTITLE_MULTIPLAYER_CLIENT_WARNING");
        }
        else if (group == RR_SS_SERVER_PERSISTENT)
        {
            // Show a warning/hint for persistent settings.
            AddPersistentSubtitle();
        }
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_OptionsMenuBase::AddSettingMenuItem
    //
    // Adds a menu item for the provided setting.
    //
    //===========================================================================
    protected void AddSettingMenuItem(RR_Setting setting)
    {
        let item = setting.GetMenuItem();
        int index = AddMenuItem(item);

        _settingMap.Insert(index, setting);
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::GetSelectedSetting
    //
    // Gets the setting the currently selected item represents, if any.
    //
    //===========================================================================
    RR_Setting GetSelectedSetting()
    {
        // Sometimes the index can be out of range (probably when using the mouse),
        // make sure we got a valid index for the items array first.
        int selectedIndex = mDesc.mSelectedItem;

        if (selectedIndex >= 0 && selectedIndex < mDesc.mItems.Size())
        {
            let [result, hasValue] = _settingMap.CheckValue(selectedIndex);

            if (hasValue)
            {
                return result;
            }
        }

        return null;
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::Drawer
    //
    // Draws the menu proper or the current help page
    // depending on whether help mode is on.
    //
    //===========================================================================
    override void OnDraw()
    {
        if (_help.IsEnabled())
        {
            // Hey, didn't know it was possible to call functions like that.
            // This takes care of the back button.
            // This comes in useful, but we need the title still...
            Menu.Drawer();

            // And this draws the rest of the help (title included).
            _help.Draw();
        }
        else
        {
            Super.OnDraw();
        }
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::MenuEvent
    //
    // Handles keyboard events for special menu keys.
    //
    //===========================================================================
    override bool MenuEvent(int mkey, bool fromcontroller)
    {
        // Handle events for help mode.
        if (_help.IsEnabled())
        {
            // Skip handling of events by OptionMenu with this construct.
            return _help.MenuEvent(mkey) || Menu.MenuEvent(mkey, fromcontroller);
        }

        return Super.MenuEvent(mkey, fromcontroller);
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::OnUIEvent
    //
    // Handles generic UI events.
    //
    //===========================================================================
    override bool OnUIEvent(UIEvent ev)
    {
        // Handle UI events for help mode.
        if (_help.IsEnabled())
        {
            // Skip handling of events by OptionMenu with this construct.
            return _help.OnUIEvent(ev) || Menu.OnUIEvent(ev);
        }

        // Check if we can enter help mode.
        bool handled = false;

        if (_help.CheckForHelpTrigger(ev))
        {
            handled = _help.SetHelpMode(true);
        }

        return handled || Super.OnUIEvent(ev);
    }

    //===========================================================================
    //
    // RR_OptionsMenuBase::MouseEvent
    //
    // Handles mouse events.
    //
    //===========================================================================
    override bool MouseEvent(int type, int x, int y)
    {
        // Handle mouse events for help mode.
        if (_help.IsEnabled())
        {
            // Skip handling of events by OptionMenu with this construct.
            return _help.MouseEvent(type, x, y) || Menu.MouseEvent(type, x, y);
        }

        return Super.MouseEvent(type, x, y);
    }
}
