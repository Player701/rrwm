//===========================================================================
//
// RR_OptionsMenuHelp
//
// Contains methods to draw help pages for RRWM settings.
// Now with 100% Extra Smooth Scrolling (TM)!
//
//===========================================================================
class RR_OptionsMenuHelp ui
{
    enum ESettingPropertyIndex
    {
        SPI_TYPE,
//<ExcludeFromHudPackage>
        SPI_CATEGORY,
        SPI_GAMEPLAY_IMPACT,
        SPI_SCOPE_AFFECTS,
        SPI_SCOPE_CHANGEABLE,
//</ExcludeFromHudPackage>
        SPI_CVAR,

        SPI_COUNT
    };

    // Is help mode enabled?
    private bool _isEnabled;

    private RR_OptionsMenuBase _optionsMenu;

    private WL_MenuCanvas _canvas;
    private WL_View _viewRoot;

    private WL_TextView _textSettingName;
    private WL_TextView _textSettingDesc;
    private Array<WL_TextView> _textPropertyValues;

    private WL_ScrollView _scrollDesc;

    // The time it takes to complete a single scroll action
    // (both long and short-distance)
    const SCROLL_TIME = 100;

    // Same here: short distance for arrows and mousewheel,
    // long for PgUp and PgDn keys
    const SCROLL_DIST_SHORT = 16;
    const SCROLL_DIST_LONG =  64;

    //===========================================================================
    //
    // RR_OptionsMenuHelp::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_OptionsMenuHelp Create(RR_OptionsMenuBase optionsMenu)
    {
        let optionsMenuHelp = new('RR_OptionsMenuHelp');
        optionsMenuHelp.Init(optionsMenu);

        return optionsMenuHelp;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(RR_OptionsMenuBase optionsMenu)
    {
        _optionsMenu = optionsMenu;
        _canvas = WL_MenuCanvas.Create();
        _viewRoot = CreateHelpView(_canvas);
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::CreateHelpView
    //
    // Creates the view hierarchy for the help menu.
    //
    //===========================================================================
    private WL_View CreateHelpView(WL_MenuCanvas canvas)
    {
        // Font to use for the title
        // (Code from gzdoom.pk3)
        let fnt = _optionsMenu.mDesc.mFont;
        fnt = generic_ui || !fnt ? NewSmallFont : fnt;

        let bigFnt = canvas.BuildFontUpon(fnt)
            .CreateFontGroup();

        double textPadding = OptionMenuSettings.mLinespacing;
        double lineSpacing = textPadding - NewSmallFont.GetHeight();

        // Font to use for all menu text EXCEPT the title
        let smallFnt = canvas.BuildFontUpon(NewSmallFont)
            .SetLineSpacing(lineSpacing, 0)
            .CreateFontGroup();

        // Outer frame (title + contents);
        let ll = WL_LinearLayout.Create();
        ll.SetOrientation(WL_O_Vertical);
        ll.SetGravity(WL_Gravity.Horizontal(WL_VG_CENTER));

        int y = _optionsMenu.mDesc.mPosition;

        if (y <= 0)
        {
            // Menu title
            int titleColor = OptionMenuSettings.mTitleColor;
            let textTitle = CreateText(bigFnt, titleColor, _optionsMenu.GetHelpTitle());
            textTitle.SetPadding(WL_Margins.Vertical(10, -y - 10 - lineSpacing));
            textTitle.SetFormatter(RR_HelpTitleFormatter.Create());
            ll.AddView(textTitle);
        }

        int normalColor = OptionMenuSettings.mFontColorValue;
        int headerColor = OptionMenuSettings.mFontColorHeader;

        // Setting name
        _textSettingName = CreateText(smallFnt, headerColor);
        _textSettingName.SetPadding(WL_Margins.Bottom(textPadding));
        ll.AddView(_textSettingName);

        // Setting properties - layout
        let gl = WL_GridLayout.Create();
        gl.SetPadding(WL_Margins.Bottom(textPadding));
        gl.SetOrientation(WL_O_Vertical);
        gl.SetColumnCount(2);
        gl.SetColumnWeight(0, 1);
        gl.SetColumnWeight(1, 1);

        // Setting properties - texts
        let ca = WL_ColonAppender.Create();

        for (int i = 0; i < SPI_COUNT; i++)
        {
            // Name
            let tvName = CreateText(smallFnt, normalColor, GetSettingPropertyName(i));
            tvName.SetFormatter(ca);
            tvName.SetLayoutParams(WL_GridLayout.CreateParams(gravity: WL_Gravity.Horizontal(WL_HG_Right)));

            gl.AddView(tvName);

            // Value
            let tvValue = CreateText(smallFnt, normalColor, localized: i != SPI_CVAR);
            tvValue.SetPadding(WL_Margins.Left(5));
            tvValue.SetLayoutParams(WL_GridLayout.CreateParams(gravity: WL_Gravity.Horizontal(WL_HG_Left)));

            _textPropertyValues.Push(tvValue);
            gl.AddView(tvValue);
        }

        ll.AddView(gl);

        // Description - title
        let textDescHeader = CreateText(smallFnt, headerColor, "$RR_MENU_HELP_DESCRIPTION");
        textDescHeader.SetFormatter(ca);
        textDescHeader.SetPadding(WL_Margins.Bottom(textPadding));
        ll.AddView(textDescHeader);

        // Description - main area
        let llDesc = WL_LinearLayout.Create();
        llDesc.SetOrientation(WL_O_Horizontal);
        llDesc.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));

        let fl = WL_FrameLayout.Create();
        fl.SetWidth(WL_Dimension.MatchParent());
        fl.SetHeight(WL_Dimension.MatchParent());
        fl.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));

        // Scroll indicators
        int scrollArrowText = OptionMenuSettings.mFontColorSelection;

        let scrollArrowUp = CreateScrollArrow(smallFnt, scrollArrowText, "▲", WL_VG_Top);
        fl.AddView(scrollArrowUp);

        let scrollArrowDown = CreateScrollArrow(smallFnt, scrollArrowText, "▼", WL_VG_Bottom);
        fl.AddView(scrollArrowDown);

        llDesc.AddView(fl);

        // Description - scroll view
        _scrollDesc = WL_ScrollView.Create();
        _scrollDesc.SetOrientation(WL_O_Vertical);
        _scrollDesc.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 2));
        llDesc.AddView(_scrollDesc);

        // Wire up the arrows to the ScrollView
        WL_ScrollArrowToggler.Create(
            scrollArrowUp,
            scrollArrowDown
        ).ObserveView(_scrollDesc);

        // Description - text
        _textSettingDesc = CreateText(smallFnt, normalColor);
        _scrollDesc.SetContent(_textSettingDesc);

        // Description - right area (empty, needed for balancing)
        fl = WL_FrameLayout.Create();
        fl.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));
        llDesc.AddView(fl);

        ll.AddView(llDesc);

        return ll;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::CreateText
    //
    // Creates a TextView with the provided parameters.
    //
    //===========================================================================
    private static WL_TextView CreateText(WL_FontGroup fnt, int clr, string text = "", bool localized = true)
    {
        let tv = WL_TextView.Create();
        tv.SetFont(fnt);
        tv.SetColor(clr);
        tv.SetText(text);
        tv.SetLocalized(localized);

        return tv;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::CreateScrollArrow
    //
    // Creates a view for a scroll arrow.
    //
    //===========================================================================
    private static WL_View CreateScrollArrow(WL_FontGroup fnt, int clr, string text, WL_VerticalGravity vGravity)
    {
        let arrow = CreateText(fnt, clr, text, false);
        arrow.SetPadding(WL_Margins.Right(7));
        arrow.SetLayoutParams(WL_FrameLayout.CreateParams(gravity: WL_Gravity.Create(WL_HG_Right, vGravity)));

        return arrow;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::SetHelpSetting
    //
    // Updates the views of the menu with the information
    // about the corresponding setting.
    //
    //===========================================================================
    private void SetHelpSetting(RR_Setting setting)
    {
        // Setting name
        _textSettingName.SetText(setting.GetFriendlyName());

        // Setting properties
        for (int i = 0; i < SPI_COUNT; i++)
        {
            _textPropertyValues[i].SetText(GetSettingPropertyValue(setting, i));
        }

        // Description
        _textSettingDesc.SetText(setting.GetDescription());

        // Reset scroll view position
        _scrollDesc.SetScrollPos(0);

        // Clear the pending scroller, if any
        _scrollDesc.GetObserverList().RemoveByType('WL_Scroller');
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::GetSettingPropertyName
    //
    // Returns the name of the describable property with the provided index.
    //
    //===========================================================================
    private static string GetSettingPropertyName(ESettingPropertyIndex index)
    {
        switch(index)
        {
            case SPI_TYPE:
                return "$RR_MENU_HELP_SETTING_TYPE";
            case SPI_CVAR:
                return "$RR_MENU_HELP_SETTING_CVAR";
//<ExcludeFromHudPackage>
            case SPI_CATEGORY:
                return "$RR_MENU_HELP_SETTING_CATEGORY";
            case SPI_GAMEPLAY_IMPACT:
                return "$RR_MENU_HELP_SETTING_GAMEPLAY_IMPACT";
            case SPI_SCOPE_AFFECTS:
                return "$RR_MENU_HELP_SETTING_AFFECTS";
            case SPI_SCOPE_CHANGEABLE:
                return "$RR_MENU_HELP_SETTING_CHANGED";
//</ExcludeFromHudPackage>
        }

        ThrowAbortException("RR_OptionsMenuHelp: Invalid property name index specified.");
        return "";
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::GetSettingPropertyValue
    //
    // Returns the value of the describable property with the provided index.
    //
    //===========================================================================
    private string GetSettingPropertyValue(RR_Setting setting, ESettingPropertyIndex index) const
    {
        switch(index)
        {
            case SPI_TYPE:
                return setting.GetFriendlyTypeName();
            case SPI_CVAR:
                return setting.GetCvarName();
//<ExcludeFromHudPackage>
            case SPI_CATEGORY:
                return RR_MenuStringHelper.GetCategoryString(setting.GetCategory());
            case SPI_GAMEPLAY_IMPACT:
                return RR_MenuStringHelper.GetGameplayImpactString(setting.GetGameplayImpact());
            case SPI_SCOPE_AFFECTS:
                return RR_MenuStringHelper.GetAffectsString(setting.GetScope());
            case SPI_SCOPE_CHANGEABLE:
                return RR_MenuStringHelper.GetCanBeChangedString(setting.GetScope());
//</ExcludeFromHudPackage>
        }

        ThrowAbortException("%s: Invalid property name index specified.", GetClassName());
        return "";
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::Draw
    //
    // Draws a help page for the selected setting.
    //
    //===========================================================================
    void Draw()
    {
        if (_isEnabled)
        {
            _canvas.DrawView(_viewRoot);
        }
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::ScrollDescription
    //
    // Posts a DeltaScroller onto the main scroll view
    // to scroll the description.
    //
    //===========================================================================
    private void ScrollDescription(double delta)
    {
        double speed = Abs(delta)/SCROLL_TIME;
        WL_Scroller.Create(speed, WL_SCROLL_DELTA, delta).ObserveView(_scrollDesc);
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::IsEnabled
    //
    // Gets whether help is currently enabled.
    //
    //===========================================================================
    bool IsEnabled() const
    {
        return _isEnabled;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::SetHelpMode
    //
    // Enabled or disables the help mode.
    //
    //===========================================================================
    bool SetHelpMode(bool value)
    {
        if (value)
        {
            // Get the setting we're displaying help for.
            let helpSetting = _optionsMenu.GetSelectedSetting();

            // Make sure it's not null.
            if (helpSetting == null)
            {
                return false;
            }

            Menu.MenuSound("menu/choose");
            SetHelpSetting(helpSetting);
        }
        else
        {
            Menu.MenuSound("menu/backup");
        }

        _isEnabled = _optionsMenu.Animated = value;
        return true;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::CheckForHelpTrigger
    //
    // Checks if an UI event should trigger the help mode.
    //
    //===========================================================================
    bool CheckForHelpTrigger(UIEvent ev)
    {
        return ev.type == UIEvent.Type_KeyDown && ev.KeyChar == UIEvent.Key_F1;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::MenuEvent
    //
    // Handles keyboard events for special menu keys in help mode.
    //
    //===========================================================================
    bool MenuEvent(int mkey)
    {
        switch (mkey)
        {
            case Menu.MKEY_Up:
                ScrollDescription(-SCROLL_DIST_SHORT);
                return true;
            case Menu.MKEY_PageUp:
                ScrollDescription(-SCROLL_DIST_LONG);
                return true;
            case Menu.MKEY_Down:
                ScrollDescription(SCROLL_DIST_SHORT);
                return true;
            case Menu.MKEY_PageDown:
                ScrollDescription(SCROLL_DIST_LONG);
                return true;
            case Menu.MKEY_Back:
                return SetHelpMode(false);
        }

        return false;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::OnUIEvent
    //
    // Handles generic UI events in help mode.
    //
    //===========================================================================
    bool OnUIEvent(UIEvent ev)
    {
        // Help mode can be toggled off by pressing F1 again.
        if (CheckForHelpTrigger(ev))
        {
            return SetHelpMode(false);
        }

        // Handle mouse wheel events to scroll description text.
        if (ev.type == UIEvent.Type_WheelUp)
        {
            ScrollDescription(-SCROLL_DIST_SHORT);
            return true;
        }

        if (ev.type == UIEvent.Type_WheelDown)
        {
            ScrollDescription(SCROLL_DIST_SHORT);
            return true;
        }

        return false;
    }

    //===========================================================================
    //
    // RR_OptionsMenuHelp::MouseEvent
    //
    // Handles mouse events in help mode.
    //
    //===========================================================================
    bool MouseEvent(int type, int x, int y)
    {
        return false;
    }
}
