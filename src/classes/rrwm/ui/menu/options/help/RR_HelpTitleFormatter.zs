//===========================================================================
//
// RR_HelpTitleFormatter
//
// Used to format the title of the help menu.
//
//===========================================================================
class RR_HelpTitleFormatter : WL_TextFormatter
{
    //===========================================================================
    //
    // RR_HelpTitleFormatter::Create
    //
    // Creates a new instance of the RR_HelpTitleFormatter class.
    //
    //===========================================================================
    static RR_HelpTitleFormatter Create()
    {
        return new('RR_HelpTitleFormatter');
    }

    //===========================================================================
    //
    // RR_HelpTitleFormatter::FormatText
    //
    // Gets the help title format and formats the text using it.
    //
    //===========================================================================
    override string FormatText(string text)
    {
        string helpFormat = StringTable.Localize("$RR_MENU_HELP_TITLE");
        return string.Format(helpFormat, text);
    }
}
