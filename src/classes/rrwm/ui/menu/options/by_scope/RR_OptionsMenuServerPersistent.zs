//===========================================================================
//
// RR_OptionsMenuServerPersistent
//
// Displays option menu items for persistent settings only.
//
//===========================================================================
class RR_OptionsMenuServerPersistent : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuServerPersistent::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByCategory()
            .FilterByScope(RR_SS_SERVER_PERSISTENT)
            .Build();
    }
}
