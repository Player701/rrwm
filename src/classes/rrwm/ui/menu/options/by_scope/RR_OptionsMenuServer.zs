//===========================================================================
//
// RR_OptionsMenuServer
//
// Displays option menu items for server settings only.
//
//===========================================================================
class RR_OptionsMenuServer : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuServer::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByCategory()
            .FilterByScope(RR_SS_SERVER)
            .Build();
    }
}
