//===========================================================================
//
// RR_OptionsMenuUser
//
// Displays option menu items for user settings only.
//
//===========================================================================
class RR_OptionsMenuUser : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuUser::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByCategory()
            .FilterByScope(RR_SS_USER)
            .Build();
    }
}
