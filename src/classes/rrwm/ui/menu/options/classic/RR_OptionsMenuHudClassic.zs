//===========================================================================
//
// RR_OptionsMenuHudClassic
//
// Implements the classic (pre-1.2) HUD options menu.
//
//===========================================================================
class RR_OptionsMenuHudClassic : RR_OptionsMenuHud
{
    //===========================================================================
    //
    // RR_OptionsMenuHudClassic::GetTitle
    //
    // Returns the title of this menu.
    //
    //===========================================================================
    override string GetTitle() const
    {
        return "$RR_MENU_TITLE_HUD";
    }

    //===========================================================================
    //
    // RR_OptionsMenuHudClassic::GetHelpTitle
    //
    // Returns the title of this menu to be used on help pages
    // with the word "help" added to the end of it.
    //
    //===========================================================================
    override string GetHelpTitle() const
    {
        // For classic HUD options menu, this is the same as the normal title.
        return GetTitle();
    }
}
