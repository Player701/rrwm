//===========================================================================
//
// RR_OptionsMenuClassic
//
// Implements the classic (pre-1.2) RRWM options menu.
//
//===========================================================================
class RR_OptionsMenuClassic : RR_OptionsMenuBase
{
    //===========================================================================
    //
    // RR_OptionsMenuClassic::CreateParameters
    //
    // Creates an instance of RR_OptionsMenuParameters for this menu.
    //
    //===========================================================================
    override RR_OptionsMenuParameters CreateParameters()
    {
        return RR_OptionsMenuParametersBuilder.Create()
            .GroupByScope()
            .FilterByCategory(RR_SC_HUD)
            .NegateFilter()
            .Build();
    }

    //===========================================================================
    //
    // RR_OptionsMenuClassic::AddHeaderItems
    //
    // Adds a submenu item for the HUD options.
    //
    //===========================================================================
    override void AddHeaderItems()
    {
        Super.AddHeaderItems();

        AddGap();
        AddSubmenu(RR_MenuStringHelper.GetCategoryString(RR_SC_HUD), 'RR_OptionsMenuHudClassic');
    }
}
