//===========================================================================
//
// RR_MenuStringHelper
//
// Contains various helper methods to convert enum values
// used in the RRWM settings framework to user-friendly strings.
//
//===========================================================================
class RR_MenuStringHelper ui
{
    //===========================================================================
    //
    // RR_MenuStringHelper::GetCategoryString
    //
    // Returns a localizable description string
    // for the given category value.
    //
    //===========================================================================
    static string GetCategoryString(E_RR_SettingCategory category)
    {
        switch (category)
        {
            case RR_SC_GAMEPLAY:
                return "$RR_MENU_SETTING_CATEGORY_GAMEPLAY";
            case RR_SC_WEAPONS:
                return "$RR_MENU_SETTING_CATEGORY_WEAPONS";
            case RR_SC_COSMETIC:
                return "$RR_MENU_SETTING_CATEGORY_COSMETIC";
            case RR_SC_HUD:
                return "$RR_MENU_SETTING_CATEGORY_HUD";
        }

        ThrowAbortException("RR_MenuStringHelper: Unknown category value %d.", category);
        return "";
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetCategoryStringShort
    //
    // Returns a short localizable description string
    // for the given category value.
    //
    //===========================================================================
    static string GetCategoryStringShort(E_RR_SettingCategory category)
    {
        return GetShortString(GetCategoryString(category));
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetGameplayImpactString
    //
    // Returns a localizable description string
    // for the given gameplay impact value.
    //
    //===========================================================================
    static string GetGameplayImpactString(E_RR_SettingGameplayImpact gameplayImpact)
    {
        switch (gameplayImpact)
        {
            case RR_SGI_NONE:
                return "$RR_MENU_SETTING_GAMEPLAY_IMPACT_NONE";
            case RR_SGI_LOW:
                return "$RR_MENU_SETTING_GAMEPLAY_IMPACT_LOW";
            case RR_SGI_MEDIUM:
                return "$RR_MENU_SETTING_GAMEPLAY_IMPACT_MEDIUM";
            case RR_SGI_HIGH:
                return "$RR_MENU_SETTING_GAMEPLAY_IMPACT_HIGH";
        }

        ThrowAbortException("RR_MenuStringHelper: Unknown gameplay impact value %d.", gameplayImpact);
        return "";
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetScopeString
    //
    // Returns a localizable description string for the given setting scope.
    //
    //===========================================================================
    static string GetScopeString(E_RR_SettingScope scope)
    {
        switch(scope)
        {
            case RR_SS_USER:
                return "$RR_MENU_SETTING_SCOPE_USER";
            case RR_SS_SERVER:
                return "$RR_MENU_SETTING_SCOPE_SERVER";
            case RR_SS_SERVER_PERSISTENT:
                return "$RR_MENU_SETTING_SCOPE_SERVER_PERSISTENT";
        }

        ThrowAbortException("RR_MenuStringHelper: Unknown setting scope %d.", scope);
        return "";
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetScopeStringShort
    //
    // Returns a short localizable description string
    // for the given setting scope.
    //
    //===========================================================================
    static string GetScopeStringShort(E_RR_SettingScope scope)
    {
        return GetShortString(GetScopeString(scope));
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetShortString
    //
    // Returns the corresponding short string
    // for the given localizable string.
    //
    //===========================================================================
    private static string GetShortString(string str)
    {
        return string.Format("%s_SHORT", str);
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetAffectsString
    //
    // Returns a localizable description string for the given setting scope
    // which describes the game entity this setting has an effect on.
    //
    //===========================================================================
    static string GetAffectsString(E_RR_SettingScope scope)
    {
        switch (scope)
        {
            case RR_SS_USER:
                return "$RR_MENU_SETTING_AFFECTS_USER";
            case RR_SS_SERVER:
            case RR_SS_SERVER_PERSISTENT:
                return "$RR_MENU_SETTING_AFFECTS_ALL";
        }

        ThrowAbortException("RR_MenuStringHelper: Unknown setting scope %d.", scope);
        return "";
    }

    //===========================================================================
    //
    // RR_MenuStringHelper::GetCanBeChangedString
    //
    // Returns a localizable string for a given setting scope
    // which describes when the setting can be changed.
    //
    //===========================================================================
    static string GetCanBeChangedString(E_RR_SettingScope scope)
    {
        switch (scope)
        {
            case RR_SS_USER:
            case RR_SS_SERVER:
                return "$RR_MENU_SETTING_CHANGED_ANY_TIME";
            case RR_SS_SERVER_PERSISTENT:
                return "$RR_MENU_SETTING_CHANGED_NEW_GAME";
        }

        ThrowAbortException("RR_MenuStringHelper: Unknown setting scope %d.", scope);
        return "";
    }
}
