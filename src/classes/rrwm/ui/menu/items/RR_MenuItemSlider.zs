//===========================================================================
//
// RR_MenuItemSlider
//
// Implements a slider which controls the value of a setting.
//
//===========================================================================
class RR_MenuItemSlider : OptionMenuItemSlider
{
    // The setting this item controls.
    private readonly<RR_SettingNumber> _setting;

    //===========================================================================
    //
    // RR_MenuItemSlider::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_MenuItemSlider Create(readonly<RR_SettingNumber> setting)
    {
        let menuItem = new('RR_MenuItemSlider');
        menuItem.Init(setting);

        return menuItem;
    }

    //===========================================================================
    //
    // RR_MenuItemSlider::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(readonly<RR_SettingNumber> setting)
    {
        // Get the bounds and use them to initialize the slider proper.
        double minValue = setting.GetMinValue().AsDouble();
        double maxValue = setting.GetMaxValue().AsDouble();

        double step = (maxValue - minValue) / 100;
        int decimalPlaces = 0;

        if (setting.GetUiValue().GetType() == RR_SVT_DOUBLE)
        {
            decimalPlaces = 2;
        }
        else
        {
            step = Max(1, step);
        }

        Super.Init(setting.GetFriendlyName(), setting.GetCvarName(), minValue, maxValue, step, decimalPlaces);
        _setting = setting;
    }

    //===========================================================================
    //
    // RR_MenuItemSlider::Draw
    //
    // Draws the slider and the label depending on whether
    // the setting is available.
    //
    //===========================================================================
    override int Draw(OptionMenuDescriptor desc, int y, int indent, bool selected)
    {
        // Draw the label.
        drawLabel(indent, y, selected ? OptionMenuSettings.mFontColorSelection : OptionMenuSettings.mFontColor, !Selectable());
        mDrawX = indent + CursorSpace();

        // Draw the slider itself.
        DrawSlider(mDrawX, y, mMin, mMax, GetSliderValue(), mShowValue, indent, !Selectable());

        return indent;
    }

    //===========================================================================
    //
    // RR_MenuItemSlider::GetSliderValue
    //
    // Returns the value of the setting as a double.
    //
    //===========================================================================
    override double GetSliderValue()
    {
        return _setting.GetUiValue().AsDouble();
    }

    //===========================================================================
    //
    // RR_MenuItemSlider::Selectable
    //
    // Checks whether this menu item can be selected.
    //
    //===========================================================================
    override bool Selectable()
    {
        return _setting.IsEnabled();
    }
}
