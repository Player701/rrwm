//===========================================================================
//
// RR_MenuItemApplyChanges
//
// Implements a menu item that applies changes to persistent settings.
//
//===========================================================================
class RR_MenuItemApplyChanges : OptionMenuItem
{
    private Array<RR_Setting> _changes;

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_MenuItemApplyChanges Create()
    {
        let i = new('RR_MenuItemApplyChanges');
        i.Init();

        return i;
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::IsVisible
    //
    // Checks if this item should be visible.
    //
    //===========================================================================
    static bool IsVisible()
    {
        return multiplayer && players[consoleplayer].settings_controller;
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        Super.Init("$RR_MENU_APPLY_CHANGES", 'None');
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::CheckForChanges
    //
    // Checks for pending changes to persistent settings and returns true
    // if there is at least one change.
    //
    //===========================================================================
    private bool CheckForChanges()
    {
        let settings = RR_Settings.GetInstance();

        _changes.Clear();

        for (int i = 0; i < settings.Count(); i++)
        {
            let setting = settings.GetByIndex(i);

            if (setting.GetScope() != RR_SS_SERVER_PERSISTENT)
            {
                continue;
            }

            if (setting.GetUiValue().CompareTo(setting.GetCvarValue()) != 0)
            {
                _changes.Push(setting);
            }
        }

        return _changes.Size() > 0;
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::Selectable
    //
    // The item is selectable if there are any pending changes
    // and the level can actually be restarted.
    //
    //===========================================================================
    override bool Selectable()
    {
        return RR_LocalEventHandler.CanRestartLevel() && CheckForChanges();
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::MenuEvent
    //
    // Handles menu events for the confirmation prompt.
    //
    //===========================================================================
    override bool MenuEvent(int mkey, bool fromcontroller)
    {
        if (mkey == Menu.MKEY_MBYes)
        {
            // Avoid console error messages
            if (Selectable())
            {
                RR_LocalEventHandler.RequestRestartLevel();
            }

            return true;
        }

        return Super.MenuEvent(mkey, fromcontroller);
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::Activate
    //
    //===========================================================================
    override bool Activate()
    {
        if (!IsVisible() || !Selectable())
        {
            // The item isn't visible anymore, or there are no changes to apply,
            // or it's not possible anymore (e.g. a hub level or intermission).
            // Unlikely that we can end up here but let's handle this case too.
            return false;
        }

        Menu.MenuSound("menu/choose");

        string summary = "";
        foreach (change : _changes)
        {
            string changeStr = string.Format(
                "%s%s:\n%s%s%s \-> %s%s\n\n",
                TEXTCOLOR_GOLD,
                StringTable.Localize(change.GetFriendlyName()),
                TEXTCOLOR_SAPPHIRE,
                StringTable.Localize(change.GetValueAsString()),
                TEXTCOLOR_WHITE,
                StringTable.Localize(change.GetValueAsString(change.GetCvarValue())),
                TEXTCOLOR_NORMAL
            );

            summary = summary .. changeStr;
        }

        string msg = string.Format(
            "%s%s%s\n\n%s%s",
            TEXTCOLOR_WHITE,
            StringTable.Localize("$RR_MENU_APPLY_CHANGES_HEADER"),
            TEXTCOLOR_NORMAL,
            summary,
            StringTable.Localize("$RR_MENU_APPLY_CHANGES_WARNING")
        );

        Menu.StartMessage(msg, 0);
        return true;
    }

    //===========================================================================
    //
    // RR_MenuItemApplyChanges::Draw
    //
    // Draws the label, graying it out if the item is not selectable.
    //
    //===========================================================================
    override int Draw(OptionMenuDescriptor desc, int y, int indent, bool selected)
    {
        int clr = selected ? OptionMenuSettings.mFontColorSelection : OptionMenuSettings.mFontColorMore;
        drawLabel(indent, y, clr, !Selectable());

        return indent;
    }
}
