//===========================================================================
//
// RR_MenuItemOption
//
// Represents a menu item which controls a RRWM setting
// represented as an option or a switch.
//
//===========================================================================
class RR_MenuItemOption : OptionMenuItemOption
{
    // The setting this item controls.
    private readonly<RR_SettingOption> _setting;

    //===========================================================================
    //
    // RR_MenuItemOption::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_MenuItemOption Create(readonly<RR_SettingOption> setting)
    {
        let menuItem = new('RR_MenuItemOption');
        menuItem.Init(setting);

        return menuItem;
    }

    //===========================================================================
    //
    // RR_MenuItemOption::Init
    //
    // Performs first-time initialiation.
    //
    //===========================================================================
    protected void Init(readonly<RR_SettingOption> setting)
    {
        Super.Init(
            setting.GetFriendlyName(),
            setting.GetCvarName(),
            setting.GetOptionValues().GetName()
        );

        _setting = setting;
    }

    //===========================================================================
    //
    // RR_MenuItemOption::Draw
    //
    // Draws the label for this setting and its value.
    // For persistent settings, draws both the current value
    // and the pending value.
    //
    //===========================================================================
    override int Draw(OptionMenuDescriptor desc, int y, int indent, bool selected)
    {
        if (mCenter)
        {
            indent = (screen.GetWidth() / 2);
        }

        bool grayed = isGrayed();

        // Get the setting value.
        string text = _setting.GetValueAsString();
//<ExcludeFromHudPackage>

        // If the setting is persistent, check if the value is locked.
        // If we can control this setting, display the value in ice color.
        if (!grayed && _setting.GetScope() == RR_SS_SERVER_PERSISTENT && RR_PersistentSettings.AreValuesLocked())
        {
            text = string.Format("%s%s", TEXTCOLOR_SAPPHIRE, StringTable.Localize(text));
            let cvarValue = _setting.GetCvarValue();

            // Check if the value of the corresponding CVar differs
            // from the setting value, and display both in this case.
            if (_setting.GetUiValue().CompareTo(cvarValue) != 0)
            {
                text = string.Format("%s%s -> %s", text, TEXTCOLOR_NORMAL, StringTable.Localize(_setting.GetValueAsString(cvarValue)));
            }
        }
//</ExcludeFromHudPackage>

        // Draw the label.
        drawLabel(indent, y, selected ? OptionMenuSettings.mFontColorSelection : OptionMenuSettings.mFontColor, grayed);

        // Draw the option text.
        Menu.DrawOptionText(indent + CursorSpace(), y, OptionMenuSettings.mFontColorValue, text, grayed);

        return indent;
    }

    //===========================================================================
    //
    // RR_MenuItemOption::isGrayed
    //
    // Checks whether this item should be grayed out.
    //
    //===========================================================================
    override bool isGrayed()
    {
        return !_setting.IsEnabled();
    }
}
