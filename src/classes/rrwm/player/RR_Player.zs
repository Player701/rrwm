//===========================================================================
//
// RR_Player
//
// Default player class for RRWM.
// Used to set up starting inventory and weapon slots.
//
//===========================================================================
class RR_Player : DoomPlayer
{
    Default
    {
        Player.StartItem 'RR_Pistol', 1;
        Player.StartItem 'RR_Fist', 1;

        Player.WeaponSlot 1, 'RR_Fist', 'RR_Chainsaw';
        Player.WeaponSlot 2, 'RR_Pistol';
        Player.WeaponSlot 3, 'RR_Shotgun', 'RR_SuperShotgun';
        Player.WeaponSlot 4, 'RR_Chaingun';
        Player.WeaponSlot 5, 'RR_RocketLauncher', 'RR_Railgun';
        Player.WeaponSlot 6, 'RR_PlasmaRifle';
        Player.WeaponSlot 7, 'RR_BFG9000';
    }

    States
    {
        Melee:
            PLAY F 6 BRIGHT Light("ZOMBIEATK");
            Goto Missile;
    }

    //===========================================================================
    //
    // RR_Player::GiveDefaultInventory
    //
    // Since it is not possible to dynamically alter a StartItem amount,
    // we have to do that here to account for DEHACKED initial ammo change.
    //
    //===========================================================================
    override void GiveDefaultInventory()
    {
        // Let's check the starting inventory of the vanilla player
        let di = GetDefaultByType('DoomPlayer').GetDropItems();

        while (di != null)
        {
            if (di.Name == 'Clip')
            {
                // The following code has mostly been copy-pasted from gzdoom.pk3
                let newClip = FindInventory('RR_Clip');

                if (newClip != null)
                {
                    int amt = di.Amount > 0 ? di.Amount : newClip.Default.Amount;
                    newClip.Amount = Clamp(newClip.Amount + amt, 0, newClip.MaxAmount);
                }
                else
                {
                    newClip = Inventory(Spawn('RR_Clip'));

                    if (newClip != null)
                    {
                        newClip.bIgnoreSkill = true;
                        newClip.Amount = di.Amount;

                        if (!newClip.CallTryPickup(self))
                        {
                            newClip.Destroy();
                        }
                    }
                }

                // OK, no need to check anything else
                break;
            }

            di = di.Next;
        }

        // NB: Handle the built-in default inventory setup here, not in the beginning.
        // This ensures that ammo is given before weapons are (note that weapons are given without ammo here),
        // otherwise, if "switch on pickup" is disabled, better weapons won't be auto-selected.
        Super.GiveDefaultInventory();
    }
}
