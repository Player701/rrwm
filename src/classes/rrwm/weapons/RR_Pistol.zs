//===========================================================================
//
// RR_Pistol
//
// The pistol
// Sprite source: Zero Tolerance (Zero Propher)
// Sound source: Doom 3 (id Software)
//
//===========================================================================
class RR_Pistol : RR_HitscanWeapon replaces Pistol
{
    // Additional sounds for precaching
    const SND_LASER    = "rrwm/weapons/pistol/laser";
    const SND_RELOAD_1 = "rrwm/weapons/pistol/reload1";
    const SND_RELOAD_2 = "rrwm/weapons/pistol/reload2";

    // Is laser sight currently enabled?
    private bool _isSightOn;
    // Checks if laser sight was enabled at the moment
    // when the weapon was deselected.
    private bool _savedSightOn;
    // Is auto-fire currently enabled?
    private bool _canAutoFire;

    private RR_LaserDot _laserDot;

    Default
    {
        Scale 0.4;
        Obituary "$OB_RR_PISTOL";
        Tag "$TAG_RR_PISTOL";
        AttackSound "rrwm/weapons/pistol/fire";

        Inventory.Icon "2PI3A0";
        Inventory.PickupSound "rrwm/weapons/pistol/pickup";
        Inventory.PickupMessage "$PICKUP_RR_PISTOL";

        Weapon.AmmoType 'RR_Clip';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 20;
        Weapon.SelectionOrder 1900;
        Weapon.SlotNumber 2;
        +WEAPON.WIMPY_WEAPON;

        RR_Weapon.SelectOffset 55, 66;
        RR_Weapon.SelectSound "rrwm/weapons/pistol/up";
        RR_Weapon.AimRecoil 2.0, 2.5, 0.2;

        RR_ReloadableWeapon.ClipCapacity 10;
        RR_ReloadableWeapon.EmptyClipType 'RR_PistolEmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset 5.0, -13.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.1, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/pistol/dryfire";

        RR_HitscanWeapon.PuffType 'RR_PistolPuff';
        RR_HitscanWeapon.CasingType 'RR_PistolCasing';
        RR_HitscanWeapon.CasingSpawnOffset 1.0, -4.5;
        RR_HitscanWeapon.CasingSpawnVelocity 0.0, 2.0, 0.0;
        RR_HitscanWeapon.CasingVelocitySpread 0.1, 0.2, 0.0;
    }

    States(Actor)
    {
        Spawn:
            2PI3 A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            2PI2 F 1;
            2PI2 F 1 A_MoveWeaponSprite(-10, -10);
            2PI2 F 1 A_MoveWeaponSprite(-12, -20);
            2PI2 F 1 A_MoveWeaponSprite(-10, -15);
            2PI2 F 1 A_MoveWeaponSprite(-9, -10);
            2PI2 F 1 A_MoveWeaponSprite(-6, -5);
            2PI2 F 1 A_MoveWeaponSprite(-5, -4);
            2PI2 F 1 A_MoveWeaponSprite(-3, -2);
            2PI2 E 1 A_SelectSound;
            2PI2 DCBA 1;
            Goto Select;
        SelectAnimSighted:
            2PI2 F 1 A_SightOn;
            2PI2 F 1 A_MoveWeaponSprite(-10, -10);
            2PI2 F 1 A_MoveWeaponSprite(-12, -20);
            2PI2 F 1 A_MoveWeaponSprite(-10, -15);
            2PI2 F 1 A_MoveWeaponSprite(-9, -10);
            2PI2 F 1 A_MoveWeaponSprite(-6, -5);
            2PI2 F 1 A_MoveWeaponSprite(-5, -4);
            2PI2 F 1 A_MoveWeaponSprite(-3, -2);
            3PI2 E 1 A_SelectSound;
            3PI2 DCBA 1;
            Goto Select;
        Ready:
            2PIS A 1 A_PistolReady;
            Loop;
        ReadySighted:
            3PIS A 1 A_PistolReady;
            Loop;
        DeselectAnim:
            2PIS A 1 A_MoveWeaponSprite(10, 12);
            2PIS A 1 A_MoveWeaponSprite(12, 15);
            2PIS A 1 A_MoveWeaponSprite(15, 18);
            2PIS A 1 A_MoveWeaponSprite(18, 20);
            Goto Deselect;
        Fire:
            2PIS G 1 A_GunFlash;
            2PIS F 2 BRIGHT A_PistolAttack;
            2PIS J 1 A_EjectCasing;
            2PIS KJ 1;
            2PIS A 8;
            TNT1 A 0 A_ReFire;
            Goto Ready;
        FireSighted:
            3PIS G 1 A_GunFlash;
            3PIS F 2 BRIGHT A_PistolAttack;
            3PIS J 1 A_EjectCasing;
            3PIS KJ 1;
            TNT1 A 0 A_NoAutoFire;
            Goto ReadySighted;
        Flash:
            TNT1 A 1;
            TNT1 A 1 A_Light1;
            TNT1 A 1 A_Light2;
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            2PIS I 1;
            2PIS I 2 A_DryFire;
            2PIS JKJ 1;
            2PIS A 8;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        DryFireSighted:
            3PIS I 1;
            3PIS I 2 A_DryFire;
            3PIS JKJ 1;
            3PIS A 8;
            TNT1 A 0 A_DryReFire;
            Goto ReadySighted;
        Reload:
            2PIS I 1 A_PlayWeaponAuxSound(SND_RELOAD_1);
            2PIS JKL 1;
            2PIS L 11 A_EjectClip;
            2PIS M 1 A_PlayWeaponAuxSound(SND_RELOAD_2);
            2PIS NOPQ 1;
            2PIS R 4 A_ReloadFullClip;
            2PIS STUVXY 1;
            2PIS A 3;
            Goto Ready;
        ReloadSighted:
            3PIS I 1 A_PlayWeaponAuxSound(SND_RELOAD_1);
            3PIS JKL 1;
            3PIS L 11 A_EjectClip;
            3PIS M 1 A_PlayWeaponAuxSound(SND_RELOAD_2);
            3PIS NOPQ 1;
            3PIS R 4 A_ReloadFullClip;
            3PIS STUVXY 1;
            3PIS A 3;
            Goto ReadySighted;
        Zoom:
        SightOn:
            3PIS A 7 A_SightOn;
            Goto ReadySighted;
        SightOff:
            2PIS A 7 A_SightOff;
            Goto Ready;

    }

    //===========================================================================
    //
    // RR_Pistol::A_PistolReady
    //
    // Suppresses automatic fire capability when laser sight is enabled
    // and calls A_ReloadableWeaponReady.
    //
    // When laser sight is not allowed anymore, disables it immediately.
    //
    //===========================================================================
    action(Weapon) state A_PistolReady()
    {
        // If saved laser sight flag is set, reset it and proceed
        // to enable the laser sight. This particular situation
        // can happen if instant weapon switch is in effect.
        if (invoker.CheckSavedSightOn() && !invoker.IsDeselecting())
        {
            invoker.SetSavedSightOn(false);
            return ResolveState('SightOn');
        }

        // Check if laser sight is allowed.
        bool allowLaserSight = IsLaserSightAllowed();

        // If laser sight is still enabled but not allowed anymore,
        // make sure to turn it off now.
        if (!allowLaserSight && invoker.CheckSightOn())
        {
            return ResolveState('SightOff');
        }

        int flags = 0;

        // If automatic fire is disabled, do not allow firing or toggling laser
        // if the player is still holding either of these buttons.
        if ((player.cmd.buttons & (BT_ATTACK | BT_ZOOM)) && !invoker.CheckAutoFire())
        {
            flags |= WRF_NOFIRE;

            // Disable mugshot rampage face too,
            // it looks weird when we are not firing.
            player.attackdown = false;
        }
        else
        {
            // If laser sight is enabled, allow using the zoom key.
            if (allowLaserSight)
            {
                flags |= WRF_ALLOWZOOM;
            }

            // Automatic fire is now enabled again.
            invoker.SetAutoFire(true);
        }

        return A_ReloadableWeaponReady(flags);
    }

    //===========================================================================
    //
    // RR_Pistol::A_PistolAttack
    //
    // If laser sight is on, fires a shot that is always 100% accurate.
    // Otherwise, fires a shot with normal spread.
    //
    //===========================================================================
    action(Weapon) void A_PistolAttack()
    {
        if (invoker.CheckSightOn())
        {
            A_FireHitscan(5, 1, 0, 0);
        }
        else
        {
            A_FireHitscan(5, 1, 5.625);
        }
    }

    //===========================================================================
    //
    // RR_Pistol::A_NoAutoFire
    //
    // Prevents automatic fire when the weapon goes back to ready state.
    //
    //===========================================================================
    action(Weapon) void A_NoAutoFire()
    {
        invoker.SetAutoFire(false);
    }

    //===========================================================================
    //
    // RR_Pistol::A_SightOn
    //
    // Enables the laser sight. If laser sight is already on,
    // jumps to the SightOff state instead.
    //
    //===========================================================================
    action(Weapon) state A_SightOn()
    {
        // Check if laser sight is already enabled.
        if (invoker.CheckSightOn())
        {
            return ResolveState('SightOff');
        }

        // Enable the laser and disable autofire.
        A_NoAutoFire();
        invoker.SetSightOn(true);

        return null;
    }

    //===========================================================================
    //
    // RR_Pistol::A_SightOff
    //
    // Turns off the laser sight.
    //
    //===========================================================================
    action(Weapon) void A_SightOff()
    {
        A_NoAutoFire();
        invoker.SetSightOn(false);
    }

    //===========================================================================
    //
    // RR_Pistol::IsLaserSightAllowed
    //
    //===========================================================================
    private static bool IsLaserSightAllowed()
    {
        return RR_Settings.InPlay(RR_SETTING_SNIPING).IsOptionValue(RR_SM_LASERSIGHT);
    }

    //===========================================================================
    //
    // RR_Pistol::IsDeselecting
    //
    // Checks if this weapon is being deselected.
    //
    //===========================================================================
    private bool IsDeselecting()
    {
        return Owner != null && (Owner.player != null && Owner.player.PendingWeapon != WP_NOCHANGE || Owner.Health <= 0);
    }

    //===========================================================================
    //
    // RR_Pistol::CheckSightOn
    //
    // Checks whether the laser sight is currently enabled.
    //
    //===========================================================================
    private bool CheckSightOn()
    {
        return _isSightOn;
    }

    //===========================================================================
    //
    // RR_Pistol::SetSightOn
    //
    // Toggles the laser sight.
    //
    //===========================================================================
    private void SetSightOn(bool isSightOn)
    {
        // If the value has changed, play a sound.
        if (isSightOn != _isSightOn)
        {
            // If the weapon has been dropped, we may not have an Owner,
            // make sure the sound still plays in this case.
            let soundPlayer = Owner ? Owner : Actor(self);
            soundPlayer.A_StartSound(SND_LASER, RR_SoundChannels.CHAN_PISTOL_LASER);
        }

        // If deselecting, save the laser sight flag to another field
        // but only if the flag itself is true; otherwise, it's cleared.
        SetSavedSightOn(IsDeselecting() && _isSightOn);
        _isSightOn = isSightOn;

        // If laser sight is not enabled, remove the laser dot.
        if (!_isSightOn)
        {
            DestroyLaser();
        }
    }

    //===========================================================================
    //
    // RR_Pistol::CheckSavedSightOn
    //
    // Checks if the laser sight flag was saved before deselection.
    //
    //===========================================================================
    private bool CheckSavedSightOn()
    {
        return _savedSightOn;
    }

    //===========================================================================
    //
    // RR_Pistol::SetSavedSightOn
    //
    // Saves or resets the laser sight flag to remember its value
    // when the weapon is deselected.
    //
    //===========================================================================
    private void SetSavedSightOn(bool savedSightOn)
    {
        _savedSightOn = savedSightOn;
    }

    //===========================================================================
    //
    // RR_Pistol::DestroyLaser
    //
    // Removes the laser dot if it's still present.
    //
    //===========================================================================
    private void DestroyLaser()
    {
        if (_laserDot != null)
        {
            _laserDot.Destroy();
            _laserDot = null;
        }
    }

    //===========================================================================
    //
    // RR_Pistol::CheckAutoFire
    //
    // Checks if automatic fire is currently allowed.
    //
    //===========================================================================
    private bool CheckAutoFire()
    {
        return _canAutoFire;
    }

    //===========================================================================
    //
    // RR_Pistol::SetAutoFire
    //
    // Toggles the automatic fire capability.
    //
    //===========================================================================
    private void SetAutoFire(bool canAutoFire)
    {
        _canAutoFire = canAutoFire;
    }

    //===========================================================================
    //
    // RR_Pistol::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_LASER);
        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
    }

    //===========================================================================
    //
    // RR_Pistol::OnDestroy
    //
    // Make sure the laser dot doesn't stick around.
    //
    //===========================================================================
    override void OnDestroy()
    {
        DestroyLaser();
        Super.OnDestroy();
    }

    //===========================================================================
    //
    // RR_Pistol::Tick
    //
    // If laser sight is enabled, generates a laser dot
    // and maintains its position relative to the player's crosshair.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (!CheckSightOn() || Owner == null || Owner.player == null)
        {
            return;
        }

        FLineTraceData t;
        double traceZ = Owner.Height / 2 - Owner.FloorClip + Owner.player.mo.AttackZOffset * Owner.player.CrouchFactor - 2;

        if (Owner.LineTrace(Owner.Angle, PLAYERMISSILERANGE, Owner.BulletSlope(), TRF_NOSKY, traceZ, 0, 0, t))
        {
            bool spawned = false;

            if (_laserDot == null)
            {
                _laserDot = RR_LaserDot(Spawn('RR_LaserDot', t.HitLocation, ALLOW_REPLACE));
                spawned = true;
            }
            else
            {
                _laserDot.SetOrigin(t.HitLocation, true);
            }

            if (_laserDot != null)
            {
                double dist = Owner.Distance3D(_laserDot);

                // Move the dot slightly away from the wall
                let finalPos = RR_Math.OffsetToPosition(-2 * t.HitDir, _laserDot);
                _laserDot.SetOrigin(finalPos, !spawned);

                // Calculate and apply the scale
                double dotScale = Clamp(dist / 1350, 0.05, 1.0);
                _laserDot.Scale = (dotScale, dotScale);
            }
        }
        else
        {
            DestroyLaser();
        }
    }

    //===========================================================================
    //
    // RR_Pistol::CreateTossable
    //
    // When dropping the weapon, laser sight needs to be turned off.
    //
    //===========================================================================
    override Inventory CreateTossable(int amt)
    {
        let tossable = RR_Pistol(Super.CreateTossable(amt));

        if (tossable != null)
        {
            tossable.SetSightOn(false);
        }

        return tossable;
    }

    //===========================================================================
    //
    // RR_Pistol::ChooseState
    //
    // Returns the first state if the laser sight is currently on,
    // otherwise returns the second state.
    //
    //===========================================================================
    private state ChooseState(state ifSightOn, state ifSightOff, bool useSaved = false)
    {
        bool isSightOn = useSaved ? CheckSavedSightOn() : CheckSightOn();
        return isSightOn ? ifSightOn : ifSightOff;
    }

    //===========================================================================
    //
    // RR_Pistol::GetFireState
    //
    // If laser sight is on, returns the FireSighted state,
    // otherwise returns the normal fire state.
    //
    //===========================================================================
    override state GetFireState(bool altFire, EFireType type)
    {
        return ChooseState(ResolveState('FireSighted'), Super.GetFireState(altFire, type));
    }

    //===========================================================================
    //
    // RR_Pistol::GetDryFireState
    //
    // If laser sight is on, returns the DryFireSighted state,
    // otherwise returns the normal dry fire state.
    //
    //===========================================================================
    override state GetDryFireState(bool altFire, EDryFireType type)
    {
        return ChooseState(ResolveState('DryFireSighted'), Super.GetDryFireState(altFire, type));
    }

    //===========================================================================
    //
    // RR_Pistol::GetReloadState
    //
    // If laser sight is on, returns the ReloadSighted state,
    // otherwise returns the normal reload state.
    //
    //===========================================================================
    override state GetReloadState()
    {
        return ChooseState(ResolveState('ReloadSighted'), Super.GetReloadState());
    }

    //===========================================================================
    //
    // RR_Pistol::GetSelectAnimState
    //
    // If laser sight is on, returns the SelectAnimSighted state.
    // Otherwise, returns the normal select animation state.
    //
    //===========================================================================
    override state GetSelectAnimState()
    {
        return ChooseState(ResolveState('SelectAnimSighted'), Super.GetSelectAnimState(), true);
    }

    //===========================================================================
    //
    // RR_Pistol::GetDeselectStateReloadable
    //
    // If laser sight is on, returns the SightOff state,
    // otherwise returns the normal deselect state.
    //
    //===========================================================================
    override state GetDeselectStateReloadable(bool fromReload)
    {
        return ChooseState(ResolveState('SightOff'), Super.GetDeselectStateReloadable(fromReload));
    }

    //===========================================================================
    //
    // RR_Pistol::GetDeselectAnimStateReloadable
    //
    // If laser sight is on, falls back to GetDeselectStateReloadable,
    // otherwise returns the normal deselect animation state.
    //
    //===========================================================================
    override state GetDeselectAnimStateReloadable(bool fromReload)
    {
        // returning null makes us fall back to GetDeselectStateReloadable
        return ChooseState(null, Super.GetDeselectAnimStateReloadable(fromReload));
    }

    //===========================================================================
    //
    // RR_Pistol::PreRaise
    //
    // Resets the laser sight and automatic fire flags
    // when bringing up the weapon.
    //
    //===========================================================================
    override void PreRaise()
    {
        Super.PreRaise();

        // In case of map change, the laser sight flag will be true at the moment
        // when the weapon gets selected. Clear it and use the saved flag instead
        // to restore the laser sight properly.
        SetSavedSightOn((CheckSavedSightOn() || CheckSightOn()) && IsLaserSightAllowed());
        _isSightOn = false;

        SetAutoFire(true);
    }

    //===========================================================================
    //
    // RR_Pistol::GetReadyState
    //
    // If laser sight is on, returns the ReadySighted state.
    // Otherwise, returns the normal ready state.
    //
    //===========================================================================
    override state GetReadyState()
    {
        return ChooseState(ResolveState('ReadySighted'), Super.GetReadyState());
    }
}
