//===========================================================================
//
// RR_BFG9000
//
// The Disruptor Pulse Cannon, aka the BFG9000
// Sprite source: Zero Tolerance (Zero Prophet)
// Sound source: Doom 3 (id Software)
//
//===========================================================================
class RR_BFG9000 : RR_ReloadableWeapon replaces BFG9000
{
    // Additional sounds for precaching
    const SND_CHARGE   = "rrwm/weapons/bfg9000/charge";
    const SND_RELOAD_1 = "rrwm/weapons/bfg9000/reload1";
    const SND_RELOAD_2 = "rrwm/weapons/bfg9000/reload2";
    const SND_RELOAD_3 = "rrwm/weapons/bfg9000/reload3";

    Default
    {
        Scale 0.75;
        Tag "$TAG_RR_BFG9000";

        Inventory.Icon "PROPA0";
        Inventory.PickupSound "rrwm/weapons/bfg9000/pickup";
        Inventory.PickupMessage "$PICKUP_RR_BFG9000";

        Weapon.AmmoType 'RR_Cell';
        Weapon.AmmoUse 40;
        Weapon.AmmoGive 40;
        Weapon.SelectionOrder 2800;
        Weapon.SlotNumber 7;
        +WEAPON.NOAUTOFIRE;

        RR_Weapon.SelectOffset -47, 67;
        RR_Weapon.SelectSound "rrwm/weapons/bfg9000/up";
        RR_Weapon.PushbackRecoil 4, 6;
        RR_Weapon.AimRecoil 20, 30, 1;

        RR_ReloadableWeapon.ClipCapacity 80;
        RR_ReloadableWeapon.EmptyClipType 'RR_BFG9000EmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset 6.0, -13.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.1, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/bfg9000/dryfire";
    }

    States(Actor)
    {
        Spawn:
            PROP A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            PROX H 1;
            PROX H 1 A_MoveWeaponSprite(5, -20);
            PROX H 1 A_MoveWeaponSprite(5, -15);
            PROX H 1 A_MoveWeaponSprite(5, -10);
            PROX H 1 A_MoveWeaponSprite(10, -10);
            PROX H 1 A_MoveWeaponSprite(10, -5);
            PROX H 1 A_MoveWeaponSprite(7, -5);
            PROX H 1 A_MoveWeaponSprite(5, -2);
            PROX I 1 A_SelectSound;
            PROX JKLMN 1;
            Goto Select;
        Ready:
            PROG A 1 A_ReloadableWeaponReady;
            Loop;
        DeselectAnim:
            PROG A 1 A_MoveWeaponSprite(3, 2);
            PROG A 1 A_MoveWeaponSprite(7, 5);
            PROG A 1 A_MoveWeaponSprite(8, 10);
            PROG A 1 A_MoveWeaponSprite(10, 15);
            PROG A 1 A_MoveWeaponSprite(7, 20);
            PROG A 1 A_MoveWeaponSprite(5, 20);
            Goto Deselect;
        Fire:
            PROG A 20 A_StartSound(SND_CHARGE, CHAN_WEAPON);
        Hold:
            PROF A 1 BRIGHT A_BFGAttack;
            PROF AA 1 BRIGHT A_MoveWeaponSprite(8, 5);
            PROF AA 1 BRIGHT A_MoveWeaponSprite(4, 3);
            PROG AA 1 BRIGHT A_MoveWeaponSprite(-8, -5);
            PROG AA 1 BRIGHT A_MoveWeaponSprite(-4, -3);
            PROG A 11 A_MoveWeaponSprite(-4, -4);
            PROG A 0 A_ReFire;
            Goto Ready;
        Flash:
            TNT1 A 4 A_Light(10);
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            PROG A 16 A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        Reload:
            PROR ABCDE 1;
            PROR F 3;
            PROR G 1 A_PlayWeaponAuxSound(SND_RELOAD_1);
            PROR H 1;
            PROR I 3;
            PROR J 1;
            PROR K 5 A_EjectClip;
            PROR LMN 1;
            PROR O 1 A_PlayWeaponAuxSound(SND_RELOAD_2);
            PROG K 3;
            PROG LMNOPQ 1;
            PROG R 20;
            PROG S 1;
            PROG T 4;
            PROG U 1 A_PlayWeaponAuxSound(SND_RELOAD_3);
            PROG V 1 A_ReloadFullClip;
            PROG WXYZ 1;
            PROG A 5;
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_BFG9000::A_BFGAttack
    //
    // Fires the Disruptor Pulse Cannon, aka the BFG9000.
    //
    //===========================================================================
    action(Weapon) void A_BFGAttack()
    {
        if (invoker.DepleteAmmo(invoker.bAltFire, true))
        {
            A_GunFlash();
            A_AlertMonsters();
            SpawnPlayerMissile('RR_BFGBall', Angle, nofreeaim: sv_nobfgaim);
        }

        A_MoveWeaponSprite(4, 4);

        // Apply recoil
        invoker.DoRecoil();
    }

    //===========================================================================
    //
    // RR_BFG9000::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_CHARGE);
        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
        MarkSound(SND_RELOAD_3);
    }
}
