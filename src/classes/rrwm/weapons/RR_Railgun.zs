//===========================================================================
//
// RR_Railgun
//
// The Railgun. There is no direct Doom equivalent to this weapon,
// but it will replace the Rocket Launcher if the corresponding setting
// is enabled.
//
// NB! Adding the Railgun to the game may cause some levels
// to become unwinnable. For example, it will likely become impossible
// to kill the final bosses of Doom II and both Final Dooms.
// Similar situations may arise in custom megawads that use similar setups.
// In other words: excercise caution. You've been warned.
//
//===========================================================================
class RR_Railgun: RR_ReloadableWeapon
{
    // Additional sounds for precaching
    const SND_CHARGE   = "rrwm/weapons/railgun/charge";
    const SND_RELOAD_1 = "rrwm/weapons/railgun/reload1";
    const SND_RELOAD_2 = "rrwm/weapons/railgun/reload2";
    const SND_RELOAD_3 = "rrwm/weapons/railgun/reload3";
    const SND_ZOOM_IN  = "rrwm/weapons/railgun/zoomin";
    const SND_ZOOM_OUT = "rrwm/weapons/railgun/zoomout";

    // Determines whether the railgun can or should be zoomed in.
    enum EZoomCheckResult
    {
        // The railgun cannot be zoomed in due to a pending weapon
        // or the lack of ammo.
        ZCR_NoZoom,

        // The railgun can be zoomed in by request.
        ZCR_CanZoom,

        // The railgun was previously in a zoomed-in state and will be
        // zoomed in immediately to return to this state.
        ZCR_ZoomNow
    };

    // Are we currently zoomed in?
    private bool _isZoomed;

    Default
    {
        Decal "RailScorch";
        Obituary "$OB_RR_RAILGUN";
        Tag "$TAG_RR_RAILGUN";
        AttackSound "rrwm/weapons/railgun/fire";

        Inventory.PickupSound "rrwm/weapons/railgun/pickup";
        Inventory.PickupMessage '$PICKUP_RR_RAILGUN';
        Inventory.Icon "RAILA0";

        Weapon.AmmoType 'RR_RailAmmo';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 2;
        Weapon.SelectionOrder 200;  // Since the Railgun is not explosive, it should be preferred for auto-selection.
        Weapon.SlotNumber 5;

        RR_Weapon.SelectOffset 40, 64;
        RR_Weapon.SelectSound "rrwm/weapons/railgun/up";
        RR_Weapon.PushbackRecoil 2.5, 3.5;
        RR_Weapon.AimRecoil 9, 11, 0.15;

        RR_ReloadableWeapon.ClipCapacity 5;
        RR_ReloadableWeapon.EmptyClipType 'RR_RailgunEmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset 6.0, -13.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.1, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/railgun/dryfire";
    }

    States(Actor)
    {
        Spawn:
            RAIL A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            RAIL B 1;
            TNT1 A 0 A_SelectSound;
            RAIL BBBBBBBB 1 A_MoveWeaponSprite(-5, -8);
            Goto Select;
        Ready:
            RAIL B 1 A_RailgunReady;
            Loop;
        ReadyZoomed:
            RSC2 A 1 BRIGHT A_RailgunReadyZoomed;
            Loop;
        ReadyZoomedNoAmmo:
            RSCO A 1 BRIGHT A_RailgunReadyZoomed;
            Loop;
        DeselectAnim:
            RAIL BBBBBBBB 1 A_MoveWeaponSprite(5, 8);
            Goto Deselect;
        Fire:
            RALF A 2 BRIGHT;
            RALF B 5 BRIGHT A_RailgunAttack;
            RAIL C 5;
            RAIL B 15;
            RAIL B 30 A_RailgunRecharge;
            RAIL B 5;
            TNT1 A 0 A_ReFire;
            Goto Ready;
        FireZoomed:
            RSC2 B 2 BRIGHT;
            RSC2 B 5 BRIGHT A_RailgunAttack;
            RSCO H 20 BRIGHT;
            RSCO B 3 BRIGHT A_RailgunRecharge;
            RSCO CDEFG 3 BRIGHT;
            RSCO HGHGHG 5 BRIGHT;
            RSCO H 2 BRIGHT;
            TNT1 A 0 A_ReFire;
            Goto ReadyZoomed;
        Flash:
            TNT1 A 5 A_Light1;
            TNT1 A 5 A_Light2;
            TNT1 A 5 A_Light1;
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            RAIL B 16 A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        DryFireZoomed:
            RSCO H 16 BRIGHT A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto ReadyZoomed;
        ReloadZoomOut:
            RSC2 A 5 BRIGHT A_RailgunZoomOut(true);
            RAIL B 7;
            Goto Reload;
        ReloadZoomOutNoAmmo:
            RSCO A 5 BRIGHT A_RailgunZoomOut(true);
            RAIL B 7;
            Goto Reload;
        Reload:
            RAIL BBBBBBBBBBBBBBBB 1 A_MoveWeaponSprite(1, 1);
            RAIL B 2 A_PlayWeaponAuxSound(SND_RELOAD_1);
            RAIL B 18 A_EjectClip;
            RAIL B 2 A_PlayWeaponAuxSound(SND_RELOAD_2);
            RAIL B 8 A_ReloadFullClip;
            RAIL B 5 A_PlayWeaponAuxSound(SND_RELOAD_3);
            RAIL BBBBBBBBBBBBBBBB 1 A_MoveWeaponSprite(-1, -1);
            RAIL B 5;
            Goto Ready;
        Zoom:
            RSC2 A 5 BRIGHT A_RailgunZoomIn;
            RSC2 A 7 BRIGHT;
            Goto ReadyZoomed;
        ZoomOut:
            RSC2 A 5 BRIGHT A_RailgunZoomOut(false);
            RAIL B 8;
            Goto Ready;
        ZoomOutNoAmmo:
            RSCO A 5 BRIGHT A_RailgunZoomOut(false);
            RAIL B 8;
            Goto ReadyZoomedNoAmmo;
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunReady
    //
    // Jumps to the zoom-in state if the player has pressed the zoom button,
    // or if the railgun is supposed to be zoomed in (after a reload).
    // Otherwise, calls A_ReloadableWeaponReady.
    //
    //===========================================================================
    action(Weapon) state A_RailgunReady()
    {
        int flags = 0;

        // Check if we can zoom in now.
        let zoomCheck = invoker.CheckZoom();

        // If we should zoom in automatically, do so.
        if (zoomCheck == ZCR_ZoomNow)
        {
            return ResolveState('Zoom');
        }
        else if (zoomCheck == ZCR_CanZoom)
        {
            // Otherwise, add a flag to A_ReloadableWeaponReady
            // so that the zoom key will be processed.
            flags |= WRF_ALLOWZOOM;
        }

        return A_ReloadableWeaponReady(flags);
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunReadyZoomed
    //
    // Jumps to the zoom-out state if the player has pressed the zoom button.
    // Otherwise, calls A_ReloadableWeaponReady.
    //
    //===========================================================================
    action(Weapon) state A_RailgunReadyZoomed()
    {
        return A_ReloadableWeaponReady(WRF_NOBOB | WRF_ALLOWZOOM);
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunAttack
    //
    // Fires the Railgun.
    //
    //===========================================================================
    action(Weapon) void A_RailgunAttack()
    {
        // If we're not zoomed, we fire the railgun "from the hip",
        // so the attack doesn't come out of the center.
        // When we are zoomed, though,
        // we hold the railgun directly in front of us.
        int offsetX = invoker._isZoomed ? 0 : 7;

        A_GunFlash();
        A_AlertMonsters();
        A_RailAttack(175, offsetX, 1, "Orange", "Navy", RGF_FULLBRIGHT, 2, 'RR_RailgunPuffDummy');

        // A_RailAttack doesn't always spawn the puff at the spot where the attack hits.
        // For example, if the attack passes through a dormant monster, the puff will spawn there.
        // Here, we circumvent this logic and use LineTrace to spawn the puff ONLY on surfaces.
        FLineTraceData t;

        // This calculation is taken from P_RailAttack in p_map.cpp
        double traceZ = Height / 2 - FloatSpeed - FloorClip + player.mo.AttackZOffset * player.CrouchFactor;

        if (LineTrace(Angle, 8192, Pitch, TRF_NOSKY | TRF_THRUACTORS, traceZ, 0, offsetX, t))
        {
            let puff = Spawn('RR_RailgunPuff', t.HitLocation, ALLOW_REPLACE);

            if (puff)
            {
                puff.Target = self;

                // Copied from p_map.cpp, P_LineAttack
                puff.SetOrigin(RR_Math.OffsetToPosition(-4 * t.HitDir, puff), false);
                puff.Angle = ATan2(t.HitDir.Y, t.HitDir.X) + 180;
            }
        }

        // Apply recoil
        invoker.DoRecoil();
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunRecharge
    //
    // If there is still enough ammo for another shot, plays the charging sound.
    // Otherwise, calls A_ReFire and jumps to the ready state.
    //
    //===========================================================================
    action(Weapon) state A_RailgunRecharge()
    {
        if (!invoker.CheckClip())
        {
            A_ReFire();
            return invoker._isZoomed
                ? ResolveState('ReadyZoomedNoAmmo')
                : ResolveState('Ready');
        }

        A_PlayWeaponAuxSound(SND_CHARGE);
        return null;
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunZoomIn
    //
    // Plays the zoom-in sound and applies a zoom factor.
    //
    //===========================================================================
    action(Weapon) state A_RailgunZoomIn()
    {
        // If currently zoomed in, zoom out instead.
        if (invoker._isZoomed)
        {
            return ResolveState('ZoomOut');
        }

        A_PlayWeaponAuxSound(SND_ZOOM_IN);
        A_ZoomFactor(1.8);

        // We're now zoomed in.
        invoker._isZoomed = true;
        return null;
    }

    //===========================================================================
    //
    // RR_Railgun::A_RailgunZoomOut
    //
    // Plays the zoom-out sound and resets the zoom factor.
    //
    //===========================================================================
    action(Weapon) void A_RailgunZoomOut(bool temporary)
    {
        A_PlayWeaponAuxSound(SND_ZOOM_OUT);
        A_ZoomFactor(1.0);

        // We're now zoomed out, if this wasn't a temporary zoom-out.
        // The latter can happen when attempting to reload the weapon while zoomed.
        if (!temporary)
        {
            invoker._isZoomed = false;
        }
    }

    //===========================================================================
    //
    // RR_Railgun::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_CHARGE);
        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
        MarkSound(SND_RELOAD_3);
        MarkSound(SND_ZOOM_IN);
        MarkSound(SND_ZOOM_OUT);
    }

    //===========================================================================
    //
    // RR_Railgun::CreateTossable
    //
    // The Railgun cannot be dropped while zoomed in.
    // That'd cause all sorts of unexpected behavior when picking it up again.
    //
    //===========================================================================
    override Inventory CreateTossable()
    {
        if (_isZoomed)
        {
            return null;
        }

        return Super.CreateTossable();
    }

    //===========================================================================
    //
    // RR_Railgun::Travelled
    //
    // Resets the zoom when travelling to a new level.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();
        ResetZoom();
    }

    //===========================================================================
    //
    // RR_Railgun::OwnerDied
    //
    // Resets the zoom when the owner dies.
    //
    //===========================================================================
    override void OwnerDied()
    {
        ResetZoom();
        Super.OwnerDied();
    }

    //===========================================================================
    //
    // RR_Railgun::GetFireState
    //
    // Returns the correct fire state based on whether we're zoomed in.
    //
    //===========================================================================
    override state GetFireState(bool altFire, EFireType type)
    {
        return _isZoomed && !altFire && type != FT_FromReload
            ? ResolveState('FireZoomed')
            : Super.GetFireState(altFire, type);
    }

    //===========================================================================
    //
    // RR_Railgun::GetFireState
    //
    // Returns the correct dry-fire state based on whether we're zoomed in.
    //
    //===========================================================================
    override state GetDryFireState(bool altFire, EDryFireType type)
    {
        return _isZoomed && !altFire && type != DFT_Cooldown
            ? ResolveState('DryFireZoomed')
            : Super.GetDryFireState(altFire, type);
    }

    //===========================================================================
    //
    // RR_Railgun::GetReloadState
    //
    // Returns the correct reload state based on whether we're zoomed in
    // and how many ammo we currently have.
    //
    //===========================================================================
    override state GetReloadState()
    {
        if (_isZoomed)
        {
            return CheckClip()
                ? ResolveState('ReloadZoomOut')
                : ResolveState('ReloadZoomOutNoAmmo');
        }

        return Super.GetReloadState();
    }

    //===========================================================================
    //
    // RR_Railgun::GetDeselectStateReloadable
    //
    // Returns the correct deselect state based on whether
    // we're zoomed in and how many ammo we currently have.
    //
    //===========================================================================
    override state GetDeselectStateReloadable(bool fromReload)
    {
        return _isZoomed
            ? GetZoomOutState()
            : Super.GetDeselectStateReloadable(fromReload);
    }

    //===========================================================================
    //
    // RR_Railgun::GetDeselectAnimStateReloadable
    //
    // Falls back to GetDeselectStateReloadable if zoom is currently enabled.
    // Otherwise, returns the normal deselect animation state.
    //
    //===========================================================================
    override state GetDeselectAnimStateReloadable(bool fromReload)
    {
        return _isZoomed
            ? null // <- This makes us fall back to GetDeselectStateReloadable
            : Super.GetDeselectAnimStateReloadable(fromReload);
    }

    //===========================================================================
    //
    // RR_Railgun::GetZoomInState
    //
    // Returns the correct zoom-out state based on how many ammo we have.
    //
    //===========================================================================
    private state GetZoomOutState()
    {
        return CheckClip()
            ? ResolveState('ZoomOut')
            : ResolveState('ZoomOutNoAmmo');
    }

    //===========================================================================
    //
    // RR_Railgun::ResetZoom
    //
    // Instantly resets the zoom level and clears the zoom flags.
    // This is called when travelling to another level
    // and also when the owner dies.
    //
    //===========================================================================
    private void ResetZoom()
    {
        if (_isZoomed)
        {
            if (Owner && Owner.player)
            {
                Owner.player.FOV = Owner.player.DesiredFOV;
            }

            FOVScale = 1;
            _isZoomed = false;
        }
    }

    //===========================================================================
    //
    // RR_Railgun::CheckZoom
    //
    // Checks if zooming is currently allowed.
    //
    //===========================================================================
    private EZoomCheckResult CheckZoom()
    {
        bool hasPlayer = Owner != null && Owner.player != null;
        bool hasPendingWeapon = hasPlayer && Owner.player.PendingWeapon != WP_NOCHANGE;

        // We can zoom if we don't have a pending weapon and the clip is not empty.
        bool canZoom = !hasPendingWeapon && CheckClip();

        // Remember if we are supposed to be zoomed now.
        bool shouldZoomNow = _isZoomed;
        _isZoomed = false;

        // If we can't zoom, then we can't. Bummer.
        if (!canZoom)
        {
            return ZCR_NoZoom;
        }

        // If we are supposed to be zoomed, we'll zoom in automatically.
        // Otherwise, we can zoom in voluntarily.
        return shouldZoomNow ? ZCR_ZoomNow : ZCR_CanZoom;
    }
}
