//===========================================================================
//
// RR_RocketLauncher
//
// The Striker missile launcher, aka the Rocket Launcher.
// Sprite source: Zen Dynamics (Xaser)
// Sound source: Quake 4 (id Software),
//               Half-Life 2 (Valve Software),
//               Zen Dynamics (Xaser)
//
//===========================================================================
class RR_RocketLauncher : RR_ReloadableWeapon replaces RocketLauncher
{
    // Additional sounds for precaching
    const SND_RELOAD_1 = "rrwm/weapons/rocketlauncher/reload1";
    const SND_RELOAD_2 = "rrwm/weapons/rocketlauncher/reload2";

    Default
    {
        Tag "$TAG_RR_ROCKETLAUNCHER";

        Inventory.Icon "STRPA0";
        Inventory.PickupSound "rrwm/weapons/rocketlauncher/pickup";
        Inventory.PickupMessage "$PICKUP_RR_ROCKETLAUNCHER";

        Weapon.AmmoType 'RR_RocketAmmo';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 2;
        Weapon.SelectionOrder 2500;
        Weapon.SlotNumber 5;
        +WEAPON.NOAUTOFIRE;

        RR_Weapon.SelectOffset -30, 71;
        RR_Weapon.SelectSound "rrwm/weapons/rocketlauncher/up";
        RR_Weapon.PushbackRecoil 2.5, 3.5;
        RR_Weapon.AimRecoil 9, 11, 0.4;

        RR_ReloadableWeapon.ClipCapacity 5;
        RR_ReloadableWeapon.EmptyClipType 'RR_RocketLauncherEmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset 6.0, -13.0;
        RR_ReloadableWeapon.EmptyClipSpawnVelocity 0.0, -1, 0.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.2, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/rocketlauncher/dryfire";
    }

    States(Actor)
    {
        Spawn:
            STRP A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            STRG A 1;
            TNT1 A 0 A_SelectSound;
            STRG A 1 A_MoveWeaponSprite(5, -20);
            STRG A 1 A_MoveWeaponSprite(5, -15);
            STRG A 1 A_MoveWeaponSprite(5, -13);
            STRG A 1 A_MoveWeaponSprite(4, -7);
            STRG A 1 A_MoveWeaponSprite(3, -7);
            STRG A 1 A_MoveWeaponSprite(3, -4);
            STRG A 1 A_MoveWeaponSprite(2, -3);
            STRG A 1 A_MoveWeaponSprite(3, -2);
            Goto Select;
        Ready:
            STRG A 1 A_ReloadableWeaponReady;
            Loop;
        DeselectAnim:
            STRG A 1 A_MoveWeaponSprite(-3, 2);
            STRG A 1 A_MoveWeaponSprite(-2, 3);
            STRG A 1 A_MoveWeaponSprite(-3, 4);
            STRG A 1 A_MoveWeaponSprite(-3, 7);
            STRG A 1 A_MoveWeaponSprite(-4, 7);
            STRG A 1 A_MoveWeaponSprite(-5, 13);
            STRG A 1 A_MoveWeaponSprite(-5, 15);
            STRG A 1 A_MoveWeaponSprite(-5, 20);
            Goto Deselect;
        Fire:
            STRF A 2 A_RocketLauncherAttack;
            STRF BCDEF 2 BRIGHT;
            STRG A 8;
            TNT1 A 0 A_ReFire;
            Goto Ready;
        Flash:
            TNT1 A 4 A_Light1;
            TNT1 A 4 A_Light2;
            TNT1 A 4 A_Light1;
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            STRG A 16 A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        Reload:
            STRG A 1 A_MoveWeaponSprite(-5, 1);
            STRG AAAA 1 A_MoveWeaponSprite(-10, 2);
            STRG AA 1 A_MoveWeaponSprite(-10, 1);
            STRG A 1 A_MoveWeaponSprite(-10, 0);
            STRG A 1 A_MoveWeaponSprite(-10, -1);
            STRG A 1 A_MoveWeaponSprite(-5, -1);
            STRR ABCD 1;
            STRR E 1 A_PlayWeaponAuxSound(SND_RELOAD_1);
            STRR FGHIJKLMN 1;
            STRR O 15 A_EjectClip;
            STRR NMLKJIHG 1;
            STRR F 1 A_PlayWeaponAuxSound(SND_RELOAD_2);
            STRR E 1 A_ReloadFullClip;
            STRR DCBA 1;
            STRG A 1;
            STRG A 1 A_MoveWeaponSprite(5, 1);
            STRG A 1 A_MoveWeaponSprite(10, 1);
            STRG A 1 A_MoveWeaponSprite(10, 0);
            STRG AA 1 A_MoveWeaponSprite(10, -1);
            STRG AAAA 1 A_MoveWeaponSprite(10, -2);
            STRG A 1 A_MoveWeaponSprite(5, -1);
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_RocketLauncher::A_RocketLauncherAttack
    //
    // Fires the Striker missile launcher, aka the rocket launcher.
    //
    //===========================================================================
    action(Weapon) void A_RocketLauncherAttack()
    {
        A_GunFlash();
        A_AlertMonsters();
        A_FireProjectile('RR_Rocket');

        // Apply recoil
        invoker.DoRecoil();
    }

    //===========================================================================
    //
    // RR_RocketLauncher::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
    }
}
