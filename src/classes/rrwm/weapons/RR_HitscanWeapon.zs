//===========================================================================
//
// RR_HitscanWeapon
//
// A type of reloadable weapon which fires hitscan rounds
// and can produce casings.
// It also provides a function to fire hitscan rounds
// that takes the "Allow sniping" setting into account
// to determine whether or not the first shot is 100% accurate.
//
//===========================================================================
class RR_HitscanWeapon : RR_ReloadableWeapon abstract
{
    // Default vertical spread for bullet attacks,
    // if enabled. (from gzdoom.pk3)
    const DEFVERTSPREAD = 3.549;

    // The puff type this weapon uses.
    meta class<Actor> PuffType;

    // The type of the casing this weapon ejects when firing.
    // Note that casings are not ejected automatically,
    // A_EjectCasing should be used for that.
    meta class<RR_CasingBase> CasingType;

    // Defines the X and Y offsets the casings are spawned at.
    // These are relative to the screen,
    // though they aren't in the screen coordinate system.
    private meta double CasingSpawnOffsetX, CasingSpawnOffsetY;

    // The initial velocity of the spawned casing.
    private meta double CasingSpawnVelX, CasingSpawnVelY, CasingSpawnVelZ;

    // Random factors applied to the initial velocity of the casing.
    private meta double CasingVelSpreadX, CasingVelSpreadY, CasingVelSpreadZ;

    property PuffType : PuffType;
    property CasingType : CasingType;
    property CasingSpawnOffset : CasingSpawnOffsetX, CasingSpawnOffsetY;
    property CasingSpawnVelocity : CasingSpawnVelX, CasingSpawnVelY, CasingSpawnVelZ;
    property CasingVelocitySpread : CasingVelSpreadX, CasingVelSpreadY, CasingVelSpreadZ;

    Default
    {
        Decal "BulletChip";
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::A_EjectCasing
    //
    // Ejects a casing. The type of the casing and the spawn point
    // are determined by the corresponding properties.
    //
    //===========================================================================
    action(Weapon) void A_EjectCasing()
    {
        invoker.SpawnCasing(self);
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::A_FireHitscan
    //
    // Performs a hitscan (bullet) attack and alerts monsters.
    //
    //===========================================================================
    action(Weapon) void A_FireHitscan(int damageperbullet, int numbullets, double spread_xy, double spread_z = -1)
    {
        // If spread_z is negative (as by default),
        // add vertical spread if allowed
        // by the combination of RRWM and GZDoom settings.
        if (spread_z < 0)
        {
            spread_z = RR_Settings.InPlay(RR_SETTING_FORCEVERTSPREAD).IsOn() || !sv_novertspread && GetCVar('vertspread')
                ? DEFVERTSPREAD
                : 0;
        }

        // Sniping only affects 1-bullet shots.
        // If sniping is off, make it -1 so that it won't be 100% on first shot.
        if (numbullets == 1 && !RR_Settings.InPlay(RR_SETTING_SNIPING).IsOptionValue(RR_SM_VANILLALIKE))
        {
            numbullets = -1;
        }

        A_AlertMonsters();
        A_FireBullets(spread_xy, spread_z, numbullets, damageperbullet, invoker.PuffType, FBF_USEAMMO | FBF_NORANDOMPUFFZ);

        // Apply recoil
        invoker.DoRecoil();
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        // Precache the casing bounce sound.
        if (CasingType != null)
        {
            MarkSound(GetDefaultByType(CasingType).BounceSound);
        }
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::SpawnCasing
    //
    // Ejects a casing using from the source actor's weapon.
    //
    //===========================================================================
    virtual RR_CasingBase SpawnCasing(Actor source) const
    {
        return RR_CasingBase(SpawnEffect(
            CasingType,
            source,
            GetCasingSpawnOffset(),
            GetCasingSpawnVelocity(),
            GetCasingVelocitySpread()
        ));
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::GetCasingSpawnOffset
    //
    // Returns the casing spawn offset for this weapon.
    //
    //===========================================================================
    private vector2 GetCasingSpawnOffset() const
    {
        return (CasingSpawnOffsetX, CasingSpawnOffsetY);
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::GetCasingSpawnVelocity
    //
    // Returns the initial casing velocity for this weapon.
    //
    //===========================================================================
    private vector3 GetCasingSpawnVelocity() const
    {
        return (
            CasingSpawnVelX,
            CasingSpawnVelY,
            CasingSpawnVelZ
        );
    }

    //===========================================================================
    //
    // RR_HitscanWeapon::GetCasingVelocitySpread
    //
    // Returns the casing velocity spread for this weapon.
    //
    //===========================================================================
    private vector3 GetCasingVelocitySpread() const
    {
        return (
            CasingVelSpreadX,
            CasingVelSpreadY,
            CasingVelSpreadZ
        );
    }
}
