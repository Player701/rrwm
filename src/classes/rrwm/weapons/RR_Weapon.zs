//===========================================================================
//
// RR_Weapon
//
// Base class for all weapons in RRWM, including non-reloading ones.
// This class is used to implement logic
// for displaying new select / deselect animations.
//
//===========================================================================
class RR_Weapon : Weapon abstract
{
    // This value is used as the absolute minimum to compare against
    // when remembering the player's pitch before recoil.
    const RECOIL_MIN_PITCH = -90;

    // This factor is applied to pushback recoil force before it is divided
    // by the owner's mass.
    const RECOIL_MASS_FACTOR = 100;

    // Defines the offsets the weapon sprite is moved by
    // before the select animation is displayed.
    // Has no effect if there is no select animation.
    private meta int SelectOffsetX, SelectOffsetY;

    // This sound will be played upon calling A_SelectSound on the aux channel.
    private meta sound SelectSound;

    // Pushback weapon recoil min and max factors. These control
    // the amount of force to push the player back with when the weapon is fired.
    // The exact value is generated randomly each time.
    private meta double PushbackRecoilAmountMin, PushbackRecoilAmountMax;

    // Aim (pitch) recoil min and max amounts. These control
    // the value to shift the player's pitch by upwards when the weapon is fired.
    // The exact value is generated randomly each time.
    private meta double AimRecoilAmountMin, AimRecoilAmountMax;

    // Aim recoil reverse factor. This determines how fast the player's pitch
    // returns to its original value after a call to ApplyAimRecoil.
    private meta double AimRecoilReverseFactor;

    // These factors control shake recoil. This is essentially a random
    // pitch/angle adjustment with a mean value of 0 so that the pitch
    // and the angle fluctuate around their current values.
    private meta double ShakeRecoilX, ShakeRecoilY;

    // This value is increased each time pitch recoil is applied
    // provided that pitch recoil is enabled and set to "Reversible".
    // It is used to return the player's pitch to its initial value
    // slowly over the course of several game ticks.
    private double _aimRecoilAccumulatedAmount;

    // Max pitch value that should not be exceeded
    // when resetting the player's pitch after recoil.
    // Note that pitch increases when looking down.
    private double _maxRecoilPitch;

    property SelectOffset: SelectOffsetX, SelectOffsetY;
    property SelectSound: SelectSound;

    property PushbackRecoil: PushbackRecoilAmountMin, PushbackRecoilAmountMax;
    property AimRecoil: AimRecoilAmountMin, AimRecoilAmountMax, AimRecoilReverseFactor;
    property ShakeRecoil: ShakeRecoilX, ShakeRecoilY;

    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        // This is a kind of a hack.
        // It effectively disables normal sv_weapondrop handling
        // by (mis)directing it to another branch,
        // allowing us to implement our own weapon drop logic.
        // (We're talking about dropping weapons on death here)
        // The main problem is that DropInventory prevents dropping
        // items that have +INVENTORY.UNDROPPABLE / UNTOSSABLE,
        // but the normal sv_weapondrop handling doesn't care (bug?).
        // This creates a need to disable this handling completely
        // because otherwise it would drop a +UNDROPPABLE weapon
        // if our own logic failed to do so.
        DropItem 'None';

        self.SelectOffset 0, 32;
        self.PushbackRecoil 0, 0;
        self.AimRecoil 0, 0, 1;
        self.ShakeRecoil 0, 0;

        // All RRWM weapons alert monsters only when they actually fire,
        // not when entering the fire state.
        +WEAPON.NOALERT;
    }

    States(Weapon)
    {
        // The Select and Deselect states are used to instantly raise / lower the weapon.
        // However, they can be redefined if animations are not used.
        Select:
            TNT1 A 0 A_Raise;
            Loop;
        Deselect:
            // Reset offsets to normal before actually deselecting the weapon.
            TNT1 A 0 A_WeaponOffset();
            TNT1 A 0 A_Lower;
            Goto Deselect + 1;
    }

    //===========================================================================
    //
    // RR_Weapon::SpawnEffect
    //
    // Spawns an effect object so that it looks like
    // if it came from the source actor's weapon.
    //
    //===========================================================================
    protected static RR_EffectBase SpawnEffect(
        class<RR_EffectBase> objectClass,
        Actor source,
        vector2 spawnOffset, vector3 spawnVelocity, vector3 velocitySpread)
    {
        return RR_EffectBase.SpawnFromSourceWeapon(
            objectClass,
            source, 1.0, RR_Math.GetRelativeViewZ(source),
            spawnOffset, spawnVelocity, velocitySpread
        );
    }

    //===========================================================================
    //
    // RR_Weapon::A_MoveWeaponSprite
    //
    // Shifts the weapon sprite on the screen using the offsets provided.
    // This is equivalent to calling A_WeaponOffset(x, y, WOF_ADD).
    // Introduced to avoid typing "WOF_ADD" everywhere.
    //
    // Additional parameters can be used to instantly move the sprite
    // to an initial position before the shift.
    //
    //===========================================================================
    action(Weapon) void A_MoveWeaponSprite(int x, int y, int startX = 0, int startY = 0)
    {
        if (startX > 0 || startY > 0)
        {
            A_WeaponOffset(startX, startY + WEAPONTOP);
        }

        A_WeaponOffset(x, y, WOF_ADD);
    }

    //===========================================================================
    //
    // RR_Weapon::A_ResetWeaponSprite
    //
    // Resets the position of the weapon sprite on the screen to default.
    //
    //===========================================================================
    action(Weapon) void A_ResetWeaponSprite()
    {
        A_WeaponOffset(0, WEAPONTOP);
    }

    //===========================================================================
    //
    // RR_Weapon::A_PlayWeaponAuxSound
    //
    // Plays the specified sound on the CHAN_WEAPON_AUX channel.
    //
    //===========================================================================
    action(Weapon) void A_PlayWeaponAuxSound(sound whattoplay)
    {
        A_StartSound(whattoplay, RR_SoundChannels.CHAN_WEAPON_AUX);
    }

    //===========================================================================
    //
    // RR_Weapon::A_SelectSound
    //
    // Plays this weapon's select sound on the CHAN_WEAPON_AUX channel.
    //
    //===========================================================================
    action(Weapon) void A_SelectSound()
    {
        A_PlayWeaponAuxSound(invoker.SelectSound);
    }

    //===========================================================================
    //
    // RR_Weapon::BeginPlay
    //
    // Sets the saved pre-recoil pitch value to the minimum possible value.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        _maxRecoilPitch = RECOIL_MIN_PITCH;
    }

    //===========================================================================
    //
    // RR_Weapon::IsInfiniteAmmo
    //
    // Checks if infinite ammo is enabled either via DMFlags or a powerup.
    //
    //===========================================================================
    protected bool IsInfiniteAmmo()
    {
        return sv_infiniteammo || Owner && Owner.FindInventory('PowerInfiniteAmmo', true);
    }

    //===========================================================================
    //
    // RR_Weapon::MarkPrecacheSounds
    //
    // Marks the select sound for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(SelectSound);
    }

    //===========================================================================
    //
    // RR_Weapon::OwnerDied
    //
    // Special handling to drop the weapon after death in the proper way.
    // The proper way doesn't involve creating a copy of the weapon.
    // This way, all actions performed when the weapon is dropped normally
    // are processed.
    //
    //===========================================================================
    override void OwnerDied()
    {
        Super.OwnerDied();

        // Check if weapon dropping is enabled and the weapon is active.
        if (!sv_weapondrop || Owner == null || Owner.player == null || Owner.player.ReadyWeapon != self)
        {
            return;
        }

        // Owner will likely be nulled after the weapon has been dropped,
        // so we have to remember it here.
        let prevOwner = Owner;

        // Try to drop the weapon.
        let dropped = RR_Weapon(Owner.DropInventory(self));

        if (dropped == null)
        {
            // Whoops.
            return;
        }

        // Account for drop style (see A_DropItem)
        double spawnz = 0;

        if (!(Level.compatflags & COMPATF_NOTOSSDROPS))
        {
            int style = sv_dropstyle;
            if (style == 0)
            {
                style = gameinfo.defaultdropstyle;
            }
            if (style == 2)
            {
                spawnz = 24;
            }
            else
            {
                spawnz = prevOwner.Height / 2;
            }
        }

        // Override DropInventory
        dropped.SetOrigin(prevOwner.Pos + (0, 0, spawnz), false);
        dropped.Vel = (0, 0, 0);

        if (!(Level.compatflags & COMPATF_NOTOSSDROPS))
        {
            dropped.TossItem();

            // NB: Adjust angle based on new velocity.
            // Otherwise, it remains the same as the owner's angle.
            // (set by DropInventory)
            dropped.Angle = ATan2(dropped.Vel.Y, dropped.Vel.X);
        }

        // Transfer remaining ammo to the weapon.
        dropped.DroppedOnDeath(prevOwner);

        // Prevent other weapons from being dropped by clearing ReadyWeapon
        // (either via our logic or normal sv_weapondrop handling).
        prevOwner.player.ReadyWeapon = null;
    }

    //===========================================================================
    //
    // RR_Weapon::DroppedOnDeath
    //
    // Called after this weapon has been dropped as a result
    // of a player's death (with sv_weapondrop enabled).
    //
    // The default implementation transfers remaining ammo
    // from the previous owner to the weapon.
    //
    //===========================================================================
    protected virtual void DroppedOnDeath(Actor dropper)
    {
        // All ammo this weapon uses is transferred to the weapon.
        if (AmmoType1 != null)
        {
            let ammoitem = dropper.FindInventory(AmmoType1);

            if (ammoitem != null)
            {
                AmmoGive1 += ammoitem.Amount;
                ammoitem.Amount = 0;
            }
        }

        // Same for secondary ammo, if it is used.
        if (AmmoType2 != null)
        {
            let ammoitem = dropper.FindInventory(AmmoType2);

            if (ammoitem != null)
            {
                AmmoGive2 += ammoitem.Amount;
                ammoitem.Amount = 0;
            }
        }
    }

    //===========================================================================
    //
    // RR_Weapon::GetObituary
    //
    // Generates an obituary based on the weapon's tag and obituary verb.
    //
    //===========================================================================
    final override string GetObituary(Actor victim, Actor inflictor, Name mod, bool playerattack)
    {
        let subj = playerattack
            ? (inflictor != null ? inflictor.Target : null)
            : inflictor;

        return RR_Obituary.Select(GetRawObituary(), self, subj);
    }

    //===========================================================================
    //
    // RR_Weapon::GetRawObituary
    //
    // Returns the raw obituary for this weapon.
    //
    //===========================================================================
    protected virtual string GetRawObituary() const
    {
        return Obituary;
    }

    //===========================================================================
    //
    // RR_Weapon::GetUpState
    //
    // Returns the select animation state if it is defined
    // and the player doesn't have instant weapon switch cheat.
    // Otherwise, returns the normal select state.
    //
    //===========================================================================
    final override state GetUpState()
    {
        PreRaise();

        let selectAnim = GetSelectAnimState();

        // Only return select animation state if it exists.
        if (selectAnim != null && Owner != null && Owner.player != null && !(Owner.player.cheats & CF_INSTANTWEAPSWITCH))
        {
            // Move the weapon sprite in position for select animation.
            let weaponSprite = Owner.player.GetPSprite(PSP_WEAPON);

            if (weaponSprite != null)
            {
                weaponSprite.x = weaponSprite.oldx = SelectOffsetX;
                weaponSprite.y = weaponSprite.oldy =  WEAPONTOP + SelectOffsetY;
            }

            return selectAnim;
        }

        // Return normal select state.
        return GetSelectState();
    }

    //===========================================================================
    //
    // RR_Weapon::Travelled
    //
    // Called when the weapon has been moved to another level.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();

        if (Owner != null && Owner.player != null && Owner.player.ReadyWeapon == self)
        {
            // NB: the weapon is not always guaranteed to be raised again
            // on level change (e.g. if CHANGELEVEL_PRERAISEWEAPON flag is used),
            // so we have to make sure that PreRaise is still called.
            PreRaise();
        }
    }

    //===========================================================================
    //
    // RR_Weapon::PreRaise
    //
    // Called just before the weapon is raised.
    //
    //===========================================================================
    protected virtual void PreRaise()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // RR_Weapon::GetDownState
    //
    // Returns the deselect animation state if it is defined
    // and the player doesn't have instant weapon switch cheat.
    // Otherwise, returns the normal deselect state.
    //
    //===========================================================================
    override state GetDownState()
    {
        PreLower();

        let deselectAnim = GetDeselectAnimState();

        // Only return select animation state if it exists.
        if (deselectAnim != null && Owner != null && Owner.player != null && !(Owner.player.cheats & CF_INSTANTWEAPSWITCH))
        {
            return deselectAnim;
        }

        // Return normal deselect state.
        return GetDeselectState();
    }

    //===========================================================================
    //
    // RR_Weapon::PreLower
    //
    // Called just before the weapon is lowered.
    //
    //===========================================================================
    protected virtual void PreLower()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // RR_Weapon::GetDeselectState
    //
    // Returns the normal deselect state.
    //
    //===========================================================================
    protected virtual state GetDeselectState()
    {
        return Super.GetDownState();
    }

    //===========================================================================
    //
    // RR_Weapon::GetDeselectAnimState
    //
    // Returns the deselect animation state.
    //
    //===========================================================================
    protected virtual state GetDeselectAnimState()
    {
        return ResolveState('DeselectAnim');
    }

    //===========================================================================
    //
    // RR_Weapon::GetSelectState
    //
    // Returns the normal select state.
    //
    //===========================================================================
    protected virtual state GetSelectState()
    {
        return Super.GetUpState();
    }

    //===========================================================================
    //
    // RR_Weapon::GetSelectAnimState
    //
    // Returns the select animation state.
    //
    //===========================================================================
    protected virtual state GetSelectAnimState()
    {
        return ResolveState('SelectAnim');
    }

    //===========================================================================
    //
    // RR_Weapon::IsOwnerAliveAndUsingThisWeapon
    //
    // Checks that this weapon's owner is present and alive
    // and has this weapon currently selected.
    //
    //===========================================================================
    private bool IsOwnerAliveAndUsingThisWeapon()
    {
        return Owner != null && Owner.Health > 0 && Owner.player != null && Owner.player.ReadyWeapon == self;
    }

    //===========================================================================
    //
    // RR_Weapon::Tick
    //
    // Returns the owner's pitch to its previous value before the recoil
    // over the course of several ticks.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        bool resetMaxPitch = false;

        if (IsOwnerAliveAndUsingThisWeapon() && _aimRecoilAccumulatedAmount > 0)
        {
            double pitchShift = Clamp(AimRecoilReverseFactor, 0, _aimRecoilAccumulatedAmount);

            Owner.A_SetPitch(Min(_maxRecoilPitch, Owner.Pitch + pitchShift), SPF_INTERPOLATE);

            if (Owner.Pitch < _maxRecoilPitch)
            {
                // Not reached the initial pitch yet, so decrement the accumulated amount normally
                _aimRecoilAccumulatedAmount = Max(0, _aimRecoilAccumulatedAmount - pitchShift);
            }
            else
            {
                // Reset to the initial pitch, so also reset the accumulated amount
                _aimRecoilAccumulatedAmount = 0;
            }

            // As soon as accumulated amount reaches 0, also reset the pitch
            resetMaxPitch = _aimRecoilAccumulatedAmount == 0;
        }
        else
        {
            // Reset everything
            _aimRecoilAccumulatedAmount = 0;
            resetMaxPitch = true;
        }

        if (resetMaxPitch)
        {
            _maxRecoilPitch = RECOIL_MIN_PITCH;
        }
    }

    //===========================================================================
    //
    // RR_Weapon::DoRecoil
    //
    // Applies weapon recoil if it's enabled.
    //
    //===========================================================================
    protected void DoRecoil()
    {
        // Only apply recoil if the player is alive and using this weapon
        if (!IsOwnerAliveAndUsingThisWeapon() || !RR_Settings.InPlay(RR_SETTING_RECOIL).IsOn())
        {
            return;
        }

        if (Max(PushbackRecoilAmountMin, PushbackRecoilAmountMax) > 0)
        {
            // Apply pushback recoil
            double pushbackRecoilForce = FRandom[RR_Weapon__DoRecoil](PushbackRecoilAmountMin, PushbackRecoilAmountMax);
            Owner.A_Recoil(pushbackRecoilForce * Cos(Owner.Pitch));
        }

        if (Level.IsFreelookAllowed() && Max(AimRecoilAmountMin, AimRecoilAmountMax) > 0)
        {
            // Apply aim recoil
            double aimRecoilPitchShift = FRandom[RR_Weapon__DoRecoil](AimRecoilAmountMin, AimRecoilAmountMax);
            double oldPitch = Owner.Pitch;
            Owner.A_SetPitch(Owner.Pitch - aimRecoilPitchShift, SPF_INTERPOLATE);

            // Check for reversible recoil
            _aimRecoilAccumulatedAmount += oldPitch - Owner.Pitch;
            _maxRecoilPitch = Max(_maxRecoilPitch, oldPitch);
        }

        // Apply shake recoil
        double shakeAngShift = FRandom[RR_Weapon__DoRecoil](-1, 1) * ShakeRecoilX;
        Owner.A_SetAngle(Owner.Angle + shakeAngShift, SPF_INTERPOLATE);

        if (Level.IsFreelookAllowed())
        {
            double shakePitchShift = FRandom[RR_Weapon__DoRecoil](-1, 1) * ShakeRecoilY;
            Owner.A_SetPitch(Owner.Pitch + shakePitchShift, SPF_INTERPOLATE);
        }
    }
}
