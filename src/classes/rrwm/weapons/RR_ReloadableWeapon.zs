//===========================================================================
//
// RR_ReloadableWeapon
//
// Defines logic for reloadable weapons.
//
//===========================================================================
class RR_ReloadableWeapon : RR_Weapon abstract
{
    // How much time until the weapon auto-reloads once in its ready state.
    const AUTORELOAD_DELAY = 10;

    // Enumerates possible fire state types.
    // Used for convenience instead of bool flags.
    enum EFireType
    {
        // Normal fire.
        FT_Normal,

        // Refire (hold).
        FT_Hold,

        // Fire from an interrupted reloading sequence.
        FT_FromReload
    };

    // Enumerates possible dry-fire state types.
    // Used for convenience instead of bool flags.
    enum EDryFireType
    {
        // Normal dry-fire.
        DFT_Normal,

        // Dry hold (holding fire button while in dry fire).
        DFT_Hold,

        // Cooldown (holding fire button straight after normal fire).
        DFT_Cooldown
    };

    // Enumerates possible next states
    // this weapon can transition to from PlayerPawn.FireWeapon
    enum EAttackState
    {
        // Normal fire.
        AS_NormalFire,

        // Dry fire (or hold).
        AS_DryFireOrHold,

        // Dry fire (straight after normal fire, with the fire button still held).
        AS_DryFireCooldown,

        // Reload
        AS_Reload
    };

    // Enumerates the states of reloading sequence.
    // This is used only when reloading round by round to handle interrupts
    // (manual request by pressing reload button or simply firing the weapon).
    enum EReloadSequenceState
    {
        // Not reloading.
        RSS_None,

        // At least one round has been reloaded.
        RSS_InProgress,

        // Accepting interrupt requests from the user.
        RSS_InProgressInterruptible,

        // An interrupt request from the user has been received.
        RSS_Interrupted,

        // Reloaded and waiting to release reload button.
        RSS_Finished
    };

    // Defines this weapon's clip capacity.
    private meta int DefaultClipCapacity;

    // Defines the empty clip type for this weapon.
    private meta class<RR_DebrisBase> EmptyClipType;

    // Defines the X and Y offsets the empty clip is spawned at.
    // These are relative to the screen,
    // though they aren't in the screen coordinate system.
    private meta double EmptyClipOffsetX, EmptyClipOffsetY;

    // Defines the initial velocity the empty clip is spawned with.
    private meta double EmptyClipVelX, EmptyClipVelY, EmptyClipVelZ;

    // Defines the random factors applied to the empty clips' initial velocity.
    private meta double EmptyClipVelSpreadX, EmptyClipVelSpreadY, EmptyClipVelSpreadZ;

    // This sound will be played upon calling A_DryFire on the aux channel.
    private meta sound DryFireSound;

    // The weapon stores clip information here.
    private RR_WeaponClip _clip;
    // The value remembered by calling SetClipAmmoGive.
    private int _clipAmmoGive;
    // Stores the amount of ticks left before this weapon will automatically reload
    // (if automatic reloading is enabled).
    private int _autoReloadTimer;
    // Stores the current reload sequence state.
    private EReloadSequenceState _reloadSequenceState;
    // Determines which state the weapon will transition to from GetAtkState.
    private EAttackState _nextAttackState;
    // Has the clip been unloaded?
    private bool _clipUnloaded;
    // This flag is set if the weapon has been dropped on death.
    private bool _droppedOnDeath;

    property ClipCapacity: DefaultClipCapacity;
    property EmptyClipType: EmptyClipType;
    property EmptyClipSpawnOffset: EmptyClipOffsetX, EmptyClipOffsetY;
    property EmptyClipSpawnVelocity: EmptyClipVelX, EmptyClipVelY, EmptyClipVelZ;
    property EmptyClipVelocitySpread: EmptyClipVelSpreadX, EmptyClipVelSpreadY, EmptyClipVelSpreadZ;
    property DryFireSound: DryFireSound;

    // Enable the SpecialHandlePickup method to prevent auto-switching
    // to a non-empty weapon.
    mixin RR_HandlePickupHook;

    // Enable partial ammo pickup (additional setup in the code below).
    mixin RR_PartialPickupHook;
    mixin RR_PartialPickup;

    Default
    {
        Weapon.Kickback 100;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_ReloadableWeaponReady
    //
    // Handles the WeaponReady logic.
    //
    //===========================================================================
    action(Weapon) state A_ReloadableWeaponReady(int flags = 0)
    {
        // Do not propagate WRF_ALLOWRELOAD flag as we handle it ourselves
        flags &= ~WRF_ALLOWRELOAD;

        // Are we in a reloading sequence?
        bool isInReloadSequence = invoker.IsInReloadSequence(true);

        if (isInReloadSequence)
        {
            // No zoom or user-defined features are allowed while reloading.
            flags &= ~(WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER2 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4);

            // NB: cannot fire and/or alt-fire if no ammo
            // (DryFire from reloading is not supported)
            let [canFire, canAltFire] = invoker.CanFireInReloadSequence();

            if (!canFire)
            {
                flags |= WRF_NOPRIMARY;
            }

            if (!canAltFire)
            {
                flags |= WRF_NOSECONDARY;
            }
        }
        else if (player.PendingWeapon == WP_NOCHANGE && invoker.CheckForReloadRequest())
        {
            // If not in a reloading sequence AND no request to change weapons,
            // go to reload state if there is a request to reload.
            return invoker.BeginReload();
        }

        // Do the WeaponReady stuff.
        A_WeaponReady(flags);
        return null;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_ReloadFullClip
    //
    // Reloads the entirety of this weapon's clip.
    //
    //===========================================================================
    action(Weapon) void A_ReloadFullClip()
    {
        invoker.ReloadFullClip();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_ReloadOneRound
    //
    // Reloads one round of this weapon's clip.
    //
    //===========================================================================
    action(Weapon) void A_ReloadOneRound()
    {
        invoker.ReloadOneRound();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_CheckReloadDone
    //
    // Checks if the weapon is fully loaded and, if so,
    // jumps to the state defined by GetReloadDoneState.
    //
    //===========================================================================
    action(Weapon) state A_CheckReloadDone()
    {
        state result = null;

        if (invoker.CheckReloadDone())
        {
            result = invoker.GetReloadDoneState();

            if (result == null)
            {
                result = invoker.GetReadyState();
            }
        }

        return result;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_DryFire
    //
    // Plays this weapon's dry fire sound on the CHAN_WEAPON_AUX channel.
    //
    //===========================================================================
    action(Weapon) void A_DryFire()
    {
        A_PlayWeaponAuxSound(invoker.DryFireSound);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_DryReFire
    //
    // Calls A_ReFire if the weapon is currently in a dry-fire state.
    //
    //===========================================================================
    action(Weapon) void A_DryReFire()
    {
        let attackState = invoker._nextAttackState;

        if (attackState == AS_DryFireOrHold || attackState == AS_DryFireCooldown)
        {
            A_ReFire();
        }
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::A_EjectClip
    //
    // Spawns an empty clip.
    // The type of the clip and its spawn point and velocity
    // are determined by the corresponding property values.
    //
    //===========================================================================
    action(Weapon) void A_EjectClip()
    {
        invoker.SpawnEmptyClip(self);
        invoker._clipUnloaded = true;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::MarkPrecacheSounds
    //
    // Marks the dry fire sound for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(DryFireSound);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::Tick
    //
    // Handles the request to interrupt reloading.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (Owner == null || Owner.player == null || Owner.player.ReadyWeapon != self)
        {
            return;
        }

        bool reloadBtn = IsReloadButtonPressed();

        if (_reloadSequenceState == RSS_InProgress && !reloadBtn)
        {
            // If the reload button is no longer held,
            // we can now accept interrupt requests.
            _reloadSequenceState = RSS_InProgressInterruptible;
        }
        else if (_reloadSequenceState == RSS_InProgressInterruptible && reloadBtn)
        {
            // Reload button pressed again, interrupt current reloading sequence.
            _reloadSequenceState = RSS_Interrupted;
        }
        else if (_reloadSequenceState == RSS_Finished && !reloadBtn)
        {
            // Reload button released, exit from reloading sequence.
            _reloadSequenceState = RSS_None;
        }
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PreRaise
    //
    // Used to reset reload flag and timer.
    //
    //===========================================================================
    override void PreRaise()
    {
        // Reset auto-reloading timer
        _autoReloadTimer = 0;
        // Reset reload sequence state
        _reloadSequenceState = RSS_None;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PreLower
    //
    // Resets the player's "attack down" flag to ensure that +WEAPON.NOAUTOFIRE
    // keeps working for any other weapon we might switch to.
    //
    //===========================================================================
    override void PreLower()
    {
        if (Owner == null || Owner.player == null)
        {
            return;
        }

        // Reset the "attack down" flag so that weapons
        // with +WEAPON.NOAUTOFIRE do not break.
        let plr = Owner.player;
        plr.attackdown = plr.cmd.buttons & (BT_ATTACK | BT_ALTATTACK);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDeselectState
    //
    // Returns the correct deselect state
    // based on the reloading sequence state.
    //
    //===========================================================================
    final override state GetDeselectState()
    {
        return GetDeselectStateReloadable(IsInReloadSequence());
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDeselectStateReloadable
    //
    // If fromReload is true, returns the DeselectReload state if it exists.
    // Otherwise, returns the normal deselect state.
    //
    //===========================================================================
    protected virtual state GetDeselectStateReloadable(bool fromReload)
    {
        if (fromReload)
        {
            let deselectReload = ResolveState('DeselectReload');

            if (deselectReload != null)
            {
                return deselectReload;
            }
        }

        return Super.GetDeselectState();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDeselectAnimState
    //
    // Returns the correct deselect animation state
    // based on the reloading sequence state.
    //
    //===========================================================================
    final override state GetDeselectAnimState()
    {
        return GetDeselectAnimStateReloadable(IsInReloadSequence());
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDeselectAnimStateReloadable
    //
    // If fromReload is true, returns the DeselectAnimReload state if it exists.
    // Otherwise, returns the normal deselect animation state.
    //
    //===========================================================================
    protected virtual state GetDeselectAnimStateReloadable(bool fromReload)
    {
        // If ResolveState returns null, it will make
        // RR_Weapon fall back to GetDeselectState,
        // which will in turn call GetDeselectStateReloadable.
        return fromReload
            ? ResolveState('DeselectAnimReload')
            : Super.GetDeselectAnimState();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CheckAmmo
    //
    // Performs ammo-checking logic and selects the next attack state to send
    // the weapon to depending on whether there actually is enough ammo to shoot.
    //
    //===========================================================================
    final override bool CheckAmmo(int fireMode, bool autoSwitch, bool requireAmmo, int ammocount)
    {
        // Is this an alt-fire?
        bool altFire = fireMode == Weapon.AltFire;

        // Revert to default logic if checking for either fire
        // or if not using ammo for this fire mode.
        if (fireMode == Weapon.EitherFire || !UsesAmmo(altFire))
        {
            return Super.CheckAmmo(fireMode, autoSwitch, requireAmmo, ammocount);
        }

        // Are we using the clip for this fire mode?
        bool usesClip = UsesClip(altFire);
        // Do we have enough ammo in the clip for this fire mode?
        bool hasClip = CheckClip(fireMode, ammocount);
        // Do we have enough ammo for the corresponding fire mode?
        bool hasAmmo = HasEnoughAmmo(altFire, requireAmmo, ammocount);
        // Do we have ANY kind of ammo? (incl. clip)
        bool hasOtherModeAmmo = GetFireState(!altFire, FT_Normal) != null && HasEnoughAmmo(!altFire, requireAmmo);
        bool hasAnyAmmo = hasAmmo || hasOtherModeAmmo || CheckClip();

        // Normally, this is what we should return.
        bool result = hasAmmo || usesClip && hasClip;

        if (autoSwitch)
        {
            int attackBtn = altFire ? BT_ALTATTACK : BT_ATTACK;

            if (Owner != null && Owner.player != null && (Owner.player.cmd.buttons & attackBtn))
            {
                // This block is called from PlayerPawn.FireWeapon.

                // Can we fire the weapon?
                bool canFire = usesClip ? hasClip : hasAmmo;

                // Fire the weapon if we can.
                if (canFire)
                {
                    _nextAttackState = AS_NormalFire;
                    return true;
                }

                int refire = Owner.player.refire;

                // If not refiring and reloading mode is not manual,
                // check if we can reload now.
                if (!refire && !RR_Settings.InPlay(RR_SETTING_RELOADMODE, Owner.PlayerNumber()).IsOptionValue(RR_RM_MANUAL))
                {
                    // Can we reload the weapon?
                    if (usesClip && hasAmmo)
                    {
                        // If no ammo in clip and we are not re-firing,
                        // go to reload state.
                        _nextAttackState = AS_Reload;
                        return true;
                    }
                }

                // We can't (or won't) reload, so opt for dry fire instead.
                if (GetDryFireState(altFire, DFT_Normal))
                {
                    // If the last state was normal fire and we are re-firing,
                    // send to cooldown instead.
                    if (_nextAttackState == AS_NormalFire && refire)
                    {
                        _nextAttackState = AS_DryFireCooldown;
                    }
                    else
                    {
                        _nextAttackState = AS_DryFireOrHold;
                    }

                    return true;
                }

                // We shouldn't refire if we got here.
                Owner.player.refire = 0;

                // Do not fire the weapon.
                result = false;
            }

            // Fall through from above.
            // This block is also called from A_ReFire
            // when the player has released the fire button.

            // Auto-switch weapons if we don't have ammo
            // for any kind of attack.
            if (!hasAnyAmmo)
            {
                AutoSwitchWeapons();
                return false;
            }
        }

        //...and return the result.
        return result;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CheckClip
    //
    // Checks whether there is enough ammo in the clip.
    // If ammocount is -1, uses the default ammo use value based on fire mode.
    //
    //===========================================================================
    protected bool CheckClip(int fireMode = Weapon.EitherFire, int ammocount = -1)
    {
        if (fireMode == Weapon.EitherFire)
        {
            return CheckClip(Weapon.PrimaryFire, ammocount) || CheckClip(Weapon.AltFire, ammocount);
        }

        bool altFire = fireMode == Weapon.AltFire;
        return UsesClip(altFire) && _clip.CheckAmmo(GetAmmoUse(altFire, ammocount));
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetAmmoUse
    //
    // Returns the correct ammo usage value based on the provided value
    // and the alt-fire flag.
    //
    //===========================================================================
    private int GetAmmoUse(bool altFire, int ammocount)
    {
        // Default value -1 means "use value from clip", however,
        // since the clip only has one value, we should manually
        // fall back to AmmoUse2 if checking for alt fire.
        if (altFire && ammocount == -1)
        {
            ammocount = AmmoUse2;
        }

        return ammocount;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::UsesAmmo
    //
    // Checks if this weapon uses ammo for the corresponding fire mode.
    //
    //===========================================================================
    private bool UsesAmmo(bool altFire)
    {
        return !altFire || AmmoType2 != null && AmmoUse2 > 0;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::UsesClip
    //
    // Checks if this weapon uses its clip for the corresponding fire mode.
    //
    //===========================================================================
    private bool UsesClip(bool altFire)
    {
        return !altFire || UsesSameSecondaryAmmo();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::UsesSameSecondaryAmmo
    //
    // Checks if this weapon uses the same secondary ammo as the primary ammo.
    //
    //===========================================================================
    private bool UsesSameSecondaryAmmo()
    {
        return AmmoType2 == AmmoType1;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::HasEnoughAmmo
    //
    // Checks if there is enough ammo for the corresponding fire mode
    // by calling base implementation of CheckAmmo.
    //
    //===========================================================================
    private bool HasEnoughAmmo(bool altFire, bool requireAmmo = false, int ammocount = -1)
    {
        return !UsesAmmo(altFire) || Super.CheckAmmo(altFire ? Weapon.AltFire : Weapon.PrimaryFire, false, requireAmmo, ammocount);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::AutoSwitchWeapons
    //
    // Sets a new pending weapon to auto-switch weapons.
    //
    //===========================================================================
    private void AutoSwitchWeapons()
    {
        let pp = PlayerPawn(Owner);

        // Only auto-switch if the player doesn't already have a pending weapon
        if (pp != null && pp.player != null && pp.player.PendingWeapon == WP_NOCHANGE)
        {
            let bestWeapon = pp.BestWeapon(null);

            // Do not auto-switch if no weapon.
            if (bestWeapon != null)
            {
                pp.player.PendingWeapon = bestWeapon;
            }
        }
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::DepleteAmmo
    //
    // Depletes ammo from the clip if it is used in this fire mode.
    // Otherwise, uses base implementation.
    //
    //===========================================================================
    final override bool DepleteAmmo(bool altFire, bool checkEnough, int ammouse)
    {
        // Deplete ammo from the clip if we use it in this fire mode.
        if (UsesClip(altFire))
        {
            return _clip.DepleteAmmo(GetAmmoUse(altFire, ammouse));
        }

        return Super.DepleteAmmo(altFire, checkEnough, ammouse);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetNextAttackState
    //
    // Decides which state to go to when the attack button is held.
    //
    //===========================================================================
    private state GetNextAttackState(bool altFire, bool hold)
    {
        // Check for normal fire.
        if (!UsesAmmo(altFire) || _nextAttackState == AS_NormalFire)
        {
            // Check if we are exiting a reload sequence.
            bool fromReload = IsInReloadSequence();

            // Force reset reload sequence state here
            // so that we can reload again if the reload button is held.
            _reloadSequenceState = RSS_None;

            // Pick a normal fire or hold state.
            return fromReload
                ? GetFireState(altFire, FT_FromReload)
                : GetFireState(altFire, hold ? FT_Hold : FT_Normal);
        }

        if (_nextAttackState == AS_DryFireOrHold)
        {
            // Dry fire.
            return GetDryFireState(altFire, hold ? DFT_Hold : DFT_Normal);
        }

        if (_nextAttackState == AS_DryFireCooldown)
        {
            // Dry fire cooldown.
            return GetDryFireState(altFire, DFT_Cooldown);
        }

        if (_nextAttackState == AS_Reload)
        {
            // Reload.
            return BeginReload();
        }

        // We should not get here.
        ThrowAbortException("%s: Encountered invalid value %d of _nextAttackState.", GetClassName(), _nextAttackState);
        return null;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetAtkState
    //
    // Propagates the call to GetNextAttackState.
    //
    //===========================================================================
    final override state GetAtkState(bool hold)
    {
        return GetNextAttackState(false, hold);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetAltAtkState
    //
    // Propagates the call to GetNextAttackState.
    //
    //===========================================================================
    final override state GetAltAtkState(bool hold)
    {
        return GetNextAttackState(true, hold);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetFireState
    //
    // Used instead of GetAtkState to retrieve fire state.
    //
    //===========================================================================
    protected virtual state GetFireState(bool altFire, EFireType type)
    {
        if (type == FT_FromReload)
        {
            let fireReloadState = altFire
                ? ResolveState('AltFireReload')
                : ResolveState('FireReload');

            if (fireReloadState != null)
            {
                return fireReloadState;
            }
        }

        bool hold = type == FT_Hold;
        return altFire
            ? Super.GetAltAtkState(hold)
            : Super.GetAtkState(hold);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDryFireState
    //
    // Returns the dry-fire state for the corresponding fire mode and type.
    //
    //===========================================================================
    protected virtual state GetDryFireState(bool altFire, EDryFireType type)
    {
        if (type == DFT_Cooldown)
        {
            let cooldownState = altFire
                ? ResolveState('DryAltFireCooldown')
                : ResolveState('DryFireCooldown');

            if (cooldownState != null)
            {
                return cooldownState;
            }

            type = DFT_Hold;
        }

        if (type == DFT_Hold)
        {
            let holdState = altFire
                ? ResolveState('DryAltHold')
                : ResolveState('DryHold');

            if (holdState != null)
            {
                return holdState;
            }
        }

        return altFire
            ? ResolveState('DryAltFire')
            : ResolveState('DryFire');
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PreReload
    //
    // Called before reloading is about to begin.
    //
    //===========================================================================
    protected virtual void PreReload()
    {
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetReloadState
    //
    // Returns the reload state.
    //
    //===========================================================================
    protected virtual state GetReloadState()
    {
        return ResolveState('Reload');
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetReloadState
    //
    // Returns the state that concludes the reloading sequence.
    //
    //===========================================================================
    protected virtual state GetReloadDoneState()
    {
        return ResolveState('ReloadDone');
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetDeselectAnimReloadState
    //
    // Returns the deselect animation state used during reloading.
    //
    //===========================================================================
    protected virtual state GetDeselectAnimReloadState()
    {
        return ResolveState('DeselectAnimReload');
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CheckForReloadRequest
    //
    // Checks if the weapon should be reloaded right now.
    //
    //===========================================================================
    private bool CheckForReloadRequest()
    {
        // Deny all requests if we can't reload.
        if (!CanReload())
        {
            return false;
        }

        // Check for manual reload request (button).
        bool willReload = IsReloadButtonPressed();

        // Check for automatic reload request (timer).
        if (!willReload)
        {
            bool autoReload = false;

            if (Owner != null)
            {
                // Check for full auto mode.
                bool fullAuto = RR_Settings.InPlay(RR_SETTING_RELOADMODE, Owner.PlayerNumber()).IsOptionValue(RR_RM_FULLAUTO);

                // To reload automatically, reload mode must be full auto
                // and there must be enough ammo to satisfy the minimum threshold.
                autoReload = fullAuto && CheckAutoReloadThreshold();
            }

            bool shouldReload = autoReload && !CheckClip();

            if (shouldReload)
            {
                // We will reload automatically if the time's up.
                willReload = _autoReloadTimer >= AUTORELOAD_DELAY;

                // Otherwise, we keep counting.
                if (!willReload)
                {
                    _autoReloadTimer++;
                }
            }

            if (!shouldReload || willReload)
            {
                // Reset the timer if we will reload now
                // or if we can't reload at all.
                _autoReloadTimer = 0;
            }
        }

        return willReload;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CheckAutoReloadThreshold
    //
    // Checks that the owner has enough ammo to start an automatic reload.
    // The exact amount is controlled by the user via the corresponding setting.
    //
    //===========================================================================
    private bool CheckAutoReloadThreshold()
    {
        if (IsInfiniteAmmo())
        {
            // Account for infinite ammo.
            // (since it's infinite, we always have enough!)
            return true;
        }

        // Get the current ammo amount, minus the part
        // that cannot be used for reloading.
        int curAmmoAmount = Ammo1.Amount - Ammo1.Amount % AmmoUse1;

        // Get the required threshold from the settings.
        int threshold = RR_Settings.InPlay(RR_SETTING_AUTORELOAD_THRESHOLD, Owner.PlayerNumber()).GetInt();

        // To reload automatically, the current value
        // must be no less than the threshold.
        return GetValueAsClipPercentage(curAmmoAmount) >= threshold;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetValueAsClipPercentage
    //
    // Converts the specified value to a percentage of the clip capacity.
    //
    //===========================================================================
    private double GetValueAsClipPercentage(int value) const
    {
        return double(value) / GetClipCapacity() * 100;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::IsReloadButtonPressed
    //
    // Returns true if the reload button is pressed.
    //
    //===========================================================================
    private bool IsReloadButtonPressed()
    {
        return Owner != null && Owner.player != null && (Owner.player.buttons & BT_RELOAD);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::IsInReloadSequence
    //
    // Returns true if this weapon is currently in a reload sequence
    // (round by round reloading).
    //
    //===========================================================================
    private bool IsInReloadSequence(bool checkFinished = false)
    {
        return _reloadSequenceState != RSS_None && (_reloadSequenceState != RSS_Finished || checkFinished);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CanFireInReloadSequence
    //
    // Dry-fire is not supported in reloading sequences,
    // so check if we have enough ammo first.
    //
    //===========================================================================
    private bool, bool CanFireInReloadSequence()
    {
        // For alt-fire, check either the clip (if using it),
        // or normal ammo.
        bool canAltFire = UsesClip(Weapon.AltFire)
            ? CheckClip(Weapon.AltFire)
            : HasEnoughAmmo(true);

        return CheckClip(Weapon.PrimaryFire), canAltFire;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CanReload
    //
    // Checks if reloading is currently possible.
    //
    //===========================================================================
    private bool CanReload()
    {
        // Do not reload while dead
        if (Owner == null || Owner.health <= 0)
        {
            return false;
        }

        // We can always reload if the clip is not full and ammo is infinite OR optional.
        bool canAlwaysReload = (IsInfiniteAmmo() || bAmmo_Optional) && !_clip.IsFull();
        return Ammo1 != null && (canAlwaysReload || _clip.CanLoadAmmo(Ammo1.Amount));
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::BeginReload
    //
    // Resets some player values related to weapon state, and begins reloading.
    //
    //===========================================================================
    private state BeginReload()
    {
        if (Owner == null || Owner.player == null)
        {
            return null;
        }

        let plr = Owner.player;

        // Disable mugshot rampage face while reloading
        plr.attackdown = false;
        // Reset the refire counter just in case.
        plr.refire = 0;

        PreReload();

        // Now go to the reload state.
        return GetReloadState();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::DoReload
    //
    // Performs the actual reload routine.
    //
    //===========================================================================
    private void DoReload(int maxAmt)
    {
        // Ideally, we want to load as much ammo as requested.
        int amt = maxAmt;
        // Check for infinite ammo.
        bool isInfiniteAmmo = IsInfiniteAmmo();

        // If we DON'T have infinite ammo nor optional ammo, cap the amount
        // of ammo to reload to the current reserve amount.
        if (!isInfiniteAmmo && !bAmmo_Optional)
        {
            amt = Min(amt, Ammo1.Amount);
        }

        // Load ammo into the clip.
        int loaded = _clip.LoadAmmo(amt);

        // Unless we have infinite ammo, we will subtract the amount of ammo loaded
        // from the current amount.
        if (!isInfiniteAmmo)
        {
            // Max is needed in case of optional ammo
            Ammo1.Amount = Max(Ammo1.Amount - loaded, 0);

            // If the new amount is 0 and the player has switched to another weapon
            // that uses the same ammo, cancel the switch.
            // IMO, this is a GZDoom bug, regardless of what the devs say,
            // I shouldn't be testing values like sv_dontcheckammo manually.
            if (!sv_dontcheckammo && Ammo1.Amount == 0 && Owner != null && Owner.player != null)
            {
                let pw = Owner.player.PendingWeapon;

                if (pw != null && pw != WP_NOCHANGE && !pw.CheckAmmo(EitherFire, false))
                {
                    Owner.player.PendingWeapon = WP_NOCHANGE;
                }
            }
        }
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::ReloadFullClip
    //
    // Attempts to load as much ammo as possible into the clip,
    // up to its capacity.
    //
    //===========================================================================
    private void ReloadFullClip()
    {
        DoReload(_clip.GetCapacity());
        _clipUnloaded = false;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::ReloadOneRound
    //
    // Reloads AmmoUse1 rounds into the clip.
    //
    //===========================================================================
    private void ReloadOneRound()
    {
        DoReload(AmmoUse1);

        // Mark the reload sequence state as in progress.
        // Now if the user releases the reload button and presses it again,
        // an interrupt request will be generated to terminate the sequence.
        if (_reloadSequenceState == RSS_None)
        {
            _reloadSequenceState = RSS_InProgress;
        }
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CheckReloadDone
    //
    // Checks if a reloading sequence has been interrupted
    // or if reloading is no longer possible.
    //
    //===========================================================================
    protected bool CheckReloadDone()
    {
        bool interrupted = false;
        bool result = !CanReload() || (interrupted = _reloadSequenceState == RSS_Interrupted);

        if (result)
        {
            // If reloading sequence has been interrupted, set state to Finished
            // so that it doesn't restart right away.
            // Otherwise, it's safe to reset state straight to None,
            // since in this case we can only reload again if we find some ammo.
            _reloadSequenceState = interrupted ? RSS_Finished : RSS_None;
        }

        return result;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetClipCapacity
    //
    // Returns the clip capacity of this weapon.
    //
    //===========================================================================
    int GetClipCapacity() const
    {
        return _clip != null
            ? _clip.GetCapacity()
            : DefaultClipCapacity;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetClipAmount
    //
    // Returns the amount of ammo in clip for this weapon.
    //
    //===========================================================================
    int GetClipAmount() const
    {
        return _clip.GetAmount();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetClipAmountPercentage
    //
    // Returns the amount of ammo in clip as a percentage of its capacity.
    //
    //===========================================================================
    double GetClipAmountPercentage() const
    {
        return GetValueAsClipPercentage(GetClipAmount());
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::SetClipAmmoGive
    //
    // Normally, the weapon's clip will be loaded with as much ammo as possible
    // when picking it up for the first time.
    // This method overrides this behavior and sets a fixed amount to be loaded
    // into the weapon's clip when it is first picked up.
    // This could be used to properly transfer the clip amount
    // when a weapon is created upon a player's death.
    // Right now there is no function to transfer old weapon's properties
    // to the new one, but maybe one day it becomes possible.
    //
    //===========================================================================
    private void SetClipAmmoGive(int clipAmount)
    {
        _clipAmmoGive = clipAmount;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::BeginPlay
    //
    // Initializes the clip.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        // These flags are not supported for reloadable weapons,
        // in case they're set, make sure to abort now.
        if (bPrimary_Uses_Both || bAlt_Uses_Both)
        {
            ThrowAbortException("%s: Reloadable weapons do not support PRIMARY_USES_BOTH and ALT_USES_BOTH flags.", GetClassName());
        }

        // Make sure the weapon uses an ammo type certified for use
        // with reloadable weapons. This is only needed to fix auto-switch issues,
        // everything else works fine without it. Just enforcing consistency.
        if (!(AmmoType1 is 'RR_ReloadableAmmo'))
        {
            ThrowAbortException("%s: Primary ammo type must descend from RR_ReloadableAmmo.", GetClassName());
        }

        // Create the clip.
        _clip = RR_WeaponClip.Create(DefaultClipCapacity, AmmoUse1);
        // Initially, the entire clip will be loaded into the weapon
        // when it is picked up (if AmmoGive amounts allow this),
        // this can be overridden by calling SetClipAmmoGive right after spawning.
        _clipAmmoGive = _clip.GetCapacity();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PostBeginPlay
    //
    // Loads the clip if the weapon is part of initial inventory.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        if (Owner != null && Ammo1 != null)
        {
            // Check if this weapon is part of newly-given default inventory.
            // In this case, we want the clip to be initially loaded.
            // This is possible in two cases:
            // 1) Owner has just spawned (new game, multiplayer respawn)
            // 2) Owner has moved to another level via ChangeLevel
            //    with a flag to reset their inventory.
            //
            // NB: 2) will not work in GZDoom 4.4.2 and below.
            // This is a GZDoom bug that was fixed in newer versions.
            if (Owner.GetAge() == 0 || Level.maptime == 0)
            {
                // Load as much ammo as possible into the weapon's clip.
                Ammo1.Amount -= _clip.LoadAmmo(Ammo1.Amount);
            }
        }

        Super.PostBeginPlay();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PartialPickup_GetAmountOwned
    //
    // Used in partial pickup handling.
    // Returns the amount of primary ammo owned by the specified actor.
    //
    //===========================================================================
    private int PartialPickup_GetAmountOwned(Actor toucher)
    {
        return GetAmountOfItem(toucher, AmmoType1);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PartialPickup_GetAmountAvailable
    //
    // Used in partial pickup handling.
    // Returns the amount of ammo available in this weapon during pickup.
    //
    //===========================================================================
    private int PartialPickup_GetAmountAvailable()
    {
        // Weapons dropped on player's death do not generate residual pickups.
        if (_droppedOnDeath)
        {
            return 0;
        }

        // At this point, AmmoGive1 always accounts for the clip,
        // since it gets unloaded in both HandlePickup and AttachToOwner.
        int result = AmmoGive1;

        // Account for alternate ammo too, if it's the same.
        if (AmmoType2 == AmmoType1)
        {
            result += AmmoGive2;
        }

        // Account for skill factor now.
        if (!bIgnoreSkill)
        {
            result = AdjustAmmoBySkill(result);
        }

        // If this weapon now has an owner, the weapon's clip is loaded again,
        // so we have to account for that as well.
        if (Owner != null)
        {
            // ammo from weapon's clip was also transferred
            result -= _clip.GetAmount();
        }

        return result;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PartialPickup_GetResidualItemType
    //
    // Used in partial pickup handling. Returns the primary ammo type
    // for this weapon.
    //
    //===========================================================================
    private class<Inventory> PartialPickup_GetResidualItemType(int remainingAmount) const
    {
        return AmmoType1;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::PartialPickup_GetResidualItemAmount
    //
    // Used in partial pickup handling. Returns the default amount
    // of the primary ammo type of this weapon multiplied by the skill factor.
    // This ensures that the residual items generated on partial pickup
    // will not provide more ammo than normal pickups of the same type do.
    // If one pickup is not enough, multiple items will be generated.
    //
    //===========================================================================
    private int PartialPickup_GetResidualItemAmount(int remainingAmount) const
    {
        return AdjustAmmoBySkill(GetDefaultByType(AmmoType1).Amount);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CreateCopy
    //
    // Handles proper copying of the weapon's clip.
    //
    //===========================================================================
    override Inventory CreateCopy(Actor other)
    {
        let copy = RR_ReloadableWeapon(Super.CreateCopy(other));

        if (copy != null && copy != self)
        {
            // Normally, this will not change anything,
            // since the only time when a "real" copy is required
            // is when this weapon stays, and if it does,
            // then its clip ammo give value will be the default one.
            // However, logically, this is what should be done.
            // And it is possible that the clip ammo give parameter
            // will be exposed to mappers in the future.
            copy.SetClipAmmoGive(_clipAmmoGive);

            // And make sure the ammo in the clip is not lost, too.
            // Again, usually this is not needed,
            // but we want to make sure everything works 100% right,
            // even if it's a niche case.
            copy._clip.LoadAmmo(_clip.GetAmount());
        }

        return copy;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::CreateTossable
    //
    // Handles proper assignment of the weapon's properties
    // when dropping the weapon from inventory.
    //
    //===========================================================================
    override Inventory CreateTossable(int amount)
    {
        // Disallow dropping the weapon if the clip has been unloaded.
        if (_clipUnloaded)
        {
            return null;
        }

        let tossable = RR_ReloadableWeapon(Super.CreateTossable(amount));

        if (tossable != null)
        {
            // Not sure why this isn't the default for dropped weapons.
            tossable.bIgnoreSkill = true;

            // Make sure the new owner only gets this much ammo in the clip,
            // even if AmmoGive values are changed later.
            tossable.SetClipAmmoGive(_clip.GetAmount());

            // Reset the dropped on death flag
            // (it will later get re-enabled if necessary)
            tossable._droppedOnDeath = false;
        }

        return tossable;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::HandlePickup
    //
    // Handles the pickup of another weapon of the same type.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        if (item.GetClass() == GetClass())
        {
            // If picking up an extra weapon, make sure to unload its clip first.
            // This way, ammo from the clip will appear in owner's inventory.
            // Unfortunately, it is not possible to do this before actually
            // realizing if the weapon can be picked up or not.
            // This is because Weapon.PickupForAmmo is not overridable.
            // That's why we store the clip ammo give value separately
            // to transfer the ammo back into the clip
            // if someone else tries to pick it up.
            let weaponOnGround = RR_ReloadableWeapon(item);
            weaponOnGround.AmmoGive1 += weaponOnGround._clip.UnloadAmmo();
        }

        // Make sure we will not wrongly auto-switch to another weapon.
        // See the corresponding mixin (RR_HandlePickupHook) for details.
        return SpecialHandlePickup(item);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::Travelled
    //
    // Reloads the weapon if the clip has been unloaded.
    //
    //===========================================================================
    override void Travelled()
    {
        if (_clipUnloaded)
        {
            // We assume the player had enough time to reload their weapon
            // during the level transition.
            ReloadFullClip();
        }

        Super.Travelled();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::AttachToOwner
    //
    // Loads ammo into the clip before picking this weapon up for the first time.
    //
    //===========================================================================
    override void AttachToOwner(Actor other)
    {
        int prevAmount, prevMax;

        // Make room for extra clip ammo in the owner's inventory.
        //
        // OK, this is a small hack, but it's needed for skill ammo factors
        // to work properly. The problem is that all the multiplications happen
        // in AttachToOwner which calls AddAmmo, and we cannot override it.
        // AddAmmo works by (obviously) using the AmmoGive values, and it cannot
        // take the weapon's clip into account, since it doesn't know about it.
        // If we move ammo to the clip before AddAmmo is called, then AddAmmo
        // will have wrong values to work with.
        //
        // So we have to let all ammo to be given normally first, and only then
        // we transfer it to the clip. But since the clip confers extra capacity,
        // we have to make room for it by temporarily boosting the MaxAmount value
        // of the ammo itself.

        // Find the ammo item, or add it if it doesn't exist yet.
        let ammoitem = Ammo(other.FindInventory(AmmoType1));

        if (ammoitem == null)
        {
            ammoitem = Ammo(Spawn(AmmoType1));

            if (ammoitem)
            {
                ammoitem.Amount = 0; // !!!
                ammoitem.AttachToOwner(other);
            }
        }

        if (ammoitem != null)
        {
            if (!sv_unlimited_pickup)
            {
                // Remember the current MaxAmount to restore it later.
                prevMax = ammoitem.MaxAmount;

                // Temporarily boost its MaxAmount to accommodate for the clip ammo.
                // NB: if we're somehow already beyond the max amount,
                // make sure to account for it, or it can lead to loss of ammo!
                ammoitem.MaxAmount = Max(prevMax, ammoitem.Amount) + _clip.GetCapacity();
            }

            // Remember the current amount.
            prevAmount = ammoitem.Amount;

            // Unload all ammo from the clip.
            AmmoGive1 += _clip.UnloadAmmo();
        }

        // The weapon is picked up and ammo is given...
        Super.AttachToOwner(other);

        if (ammoitem != null)
        {
            // Load at most clip ammo give ammo to the clip, but do not deduct
            // from the ammo the owner had before picking up the weapon.
            int maxLoad = Min(_clipAmmoGive, ammoitem.Amount - prevAmount);

            // Transfer remaining ammo to the clip.
            ammoitem.Amount -= _clip.LoadAmmo(maxLoad);

            if (!sv_unlimited_pickup)
            {
                // Now remove the extra room we made earlier by resetting MaxAmount
                // to the value remembered initially.
                ammoitem.MaxAmount = prevMax;

                // Make sure we haven't ended up with extra ammo beyond the max amount.
                // This can happen if picking up a dropped weapon.
                // NB: if we were somehow already beyond the max amount,
                // make sure to account for it, or it can lead to loss of ammo!
                ammoitem.Amount = Min(ammoitem.Amount, Max(prevAmount, ammoitem.MaxAmount));
            }
        }

        // Reset the "clip unloaded" flag. This is needed if the weapon was
        // somehow removed from inventory in the middle of the reloading process;
        // if it's picked up again, make it droppable without waiting until
        // the user selects it.
        _clipUnloaded = false;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::OwnerDied
    //
    // Resets the "clip unloaded" flag so that the weapon can be dropped.
    // This is needed if the user died before the reloading process could finish.
    //
    //===========================================================================
    override void OwnerDied()
    {
        _clipUnloaded = false;
        Super.OwnerDied();
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::DroppedOnDeath
    //
    // Prevents ammo from being dropped if the corresponding RRWM setting
    // is turned on.
    //
    //===========================================================================
    override void DroppedOnDeath(Actor dropper)
    {
        let a1 = AmmoType1, a2 = AmmoType2;

        // Do we need to drop ammo?
        if (!RR_Settings.InPlay(RR_SETTING_WEAPONDROP_AMMO).IsOn())
        {
            // Temporarily set ammo type 1 to null
            // so that the parent method won't find any remaining ammo.
            AmmoType1 = null;

            // If alternate ammo is the same, also set it to null.
            // (The setting is relevant to primary ammo only.)
            if (AmmoType2 == AmmoType1)
            {
                AmmoType2 = null;
            }
        }

        // Defer to parent logic for alternate ammo (if any).
        Super.DroppedOnDeath(dropper);

        // Restore ammo types, because we might have set some of them to null.
        AmmoType1 = a1;
        AmmoType2 = a2;

        // Set the flag to disable partial pickup
        _droppedOnDeath = true;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::FindDropAmmoType
    //
    // Finds the most suitable ammo pickup type for the provided amount.
    //
    //===========================================================================
    private class<Ammo> FindDropAmmoType(int requiredAmount)
    {
        class<Ammo> best = null, biggest = null;
        int bestAmount, biggestAmount;

        foreach (cls : AllActorClasses)
        {
            let curAmmoType = (class<Ammo>)(cls);

            if (curAmmoType == null)
            {
                // Not an ammo type
                continue;
            }

            let def = GetDefaultByType(curAmmoType);

            // Make sure this is a subclass of the primary ammo type.
            if (def.GetParentAmmo() == AmmoType1)
            {
                // This type is suitable if it can hold
                // at least the provided amount of ammo.
                int defAmount = AdjustAmmoBySkill(def.Amount);
                bool isGood = defAmount >= requiredAmount;

                // We need to minimize this amount.
                if (isGood && (!best || defAmount < bestAmount))
                {
                    best = curAmmoType;
                    bestAmount = defAmount;
                }

                // At the same time, find the pickup type
                // that can hold the most ammo.
                if (!biggest || defAmount > biggestAmount)
                {
                    biggest = curAmmoType;
                    biggestAmount = defAmount;
                }
            }
        }

        // If we couldn't find a suitable pickup type for the provided amount,
        // simply return the type with the largest capacity.
        return best != null ? best : biggest;
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::SpawnEmptyClip
    //
    // Spawns an empty clip from the source actor's weapon.
    //
    //===========================================================================
    RR_DebrisBase SpawnEmptyClip(Actor source) const
    {
        // NB: this is needed for the super shotgun to work correctly,
        // as it calls A_EjectClip to set the flag,
        // but there is no clip to spawn.
        if (EmptyClipType == null)
        {
            return null;
        }

        return RR_DebrisBase(SpawnEffect(
            EmptyClipType,
            source,
            GetEmptyClipSpawnOffset(),
            GetEmptyClipSpawnVelocity(),
            GetEmptyClipVelocitySpread()
        ));
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetEmptyClipSpawnOffset
    //
    // Returns the empty clip's spawn offset for this weapon as a vector.
    //
    //===========================================================================
    private vector2 GetEmptyClipSpawnOffset() const
    {
        return (EmptyClipOffsetX, EmptyClipOffsetY);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetEmptyClipSpawnVelocity
    //
    // Returns the empty clip's initial velocity for this weapon as a vector.
    //
    //===========================================================================
    private vector3 GetEmptyClipSpawnVelocity() const
    {
        return (EmptyClipVelX, EmptyClipVelY, EmptyClipVelZ);
    }

    //===========================================================================
    //
    // RR_ReloadableWeapon::GetEmptyClipVelocitySpread
    //
    // Returns the empty clip's velocity spread for this weapon as a vector.
    //
    //===========================================================================
    private vector3 GetEmptyClipVelocitySpread() const
    {
        return (EmptyClipVelSpreadX, EmptyClipVelSpreadY, EmptyClipVelSpreadZ);
    }
}
