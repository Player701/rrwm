//===========================================================================
//
// RR_Chainsaw
//
// The Chainsaw, reborn!
// Sprite source: Zero Tolerance (Zero Prophet)
// Sound source: Doom 3 (id Software)
//
//===========================================================================
class RR_Chainsaw : RR_Weapon replaces Chainsaw
{
    // Additional sounds for precaching
    const SND_CHARGE  = "rrwm/weapons/chainsaw/charge";
    const SND_PULL    = "rrwm/weapons/chainsaw/pull";
    const SND_READY   = "rrwm/weapons/chainsaw/ready";
    const SND_RELEASE = "rrwm/weapons/chainsaw/release";
    const SND_START   = "rrwm/weapons/chainsaw/start";
    const SND_STOP    = "rrwm/weapons/chainsaw/stop";
    const SND_PROTECT = "rrwm/weapons/chainsaw/protect";
    const SND_UPGRADE = "rrwm/weapons/chainsaw/upgrade";

    // Ammo regeneration rate (units per second)
    const AMMO_REGEN_RATE = 3;

    // Base ammo drain rate (units per second)
    const AMMO_DRAIN_RATE = 1;

    // Multiplication factor to apply to ammo drain rate while attacking
    const AMMO_DRAIN_ATTACK_MULTIPLIER = 4;

    // Modifier to apply to the ammo drain rate with each uprade level.
    // This is a non-linear formula, yielding diminishing returns:
    // for any value less than 1, each next upgrade yields less and less benefit.
    // The lower the value, the less the effect from each upgrade overall.
    //
    // The following value doubles the total attack time
    // from 25 to 50 seconds with 4 chainsaws collected.
    const AMMO_DRAIN_UPGRADE_FACTOR = 0.5;

    // Maximum amount of ammo units available
    const MAX_AMMO_COUNT = 100;

    // All chainsaws in multiplayer have a fixed upgrade level
    // This values gives 75 seconds of total attack time.
    const MULTIPLAYER_UPGRADE_LEVEL = 9;

    // The number of ticks before the chainsaw can be fired again
    const COOLDOWN_DELAY_TICKS = 20;

    // The duration of the protection effect after hitting an enemy
    const PROTECTION_DURATION_TICKS = 50;

    //===========================================================================
    //
    // Enumerates the chainsaw's operating modes.
    //
    //===========================================================================
    enum EChainsawMode
    {
        // The chainsaw is not running and replenishing ammo
        CM_Idle,

        // The chainsaw is running and slowly draining ammo
        CM_Running,

        // The chainsaw is attacking and quickly draining ammo
        CM_Attacking
    };

    // Current mode of this chainsaw
    private EChainsawMode _mode;

    // Current ammo count of this chainsaw (NB: we do not use a separate ammo class)
    // Note that this is a fractional value!
    private double _ammoCount;

    // The amount of ticks left before the attack can be used again
    private int _cooldownTimer;

    // The amount of ticks left before the protection effect expires
    private int _protectionTimer;

    // Current upgrade level of this chainsaw (NB: starts with 1)
    private int _upgradeLevel;

    // Checks if this chainsaw can currently be dropped.
    // This flag is set while revving the chainsaw up
    // to avoid suddenly interrupting the animation.
    private bool _isDroppable;

    // Used to play sounds.
    private RR_ChainsawSoundPlayer _soundPlayer;

    // The setting that controls whether smoke is enabled.
    private transient RR_Setting _smokeSetting;

    Default
    {
        Decal "BulletChip";

        Scale 0.75;
        Obituary "$OB_RR_CHAINSAW";
        Tag "$TAG_RR_CHAINSAW";
        AttackSound "rrwm/weapons/chainsaw/attack";

        Inventory.Icon "ASAWA0";
        Inventory.PickupSound "rrwm/weapons/chainsaw/pickup";
        Inventory.PickupMessage "$PICKUP_RR_CHAINSAW";
        +INVENTORY.IGNORESKILL;

        Weapon.AmmoType1 'RR_ChainsawAmmo1';
        Weapon.AmmoType2 'RR_ChainsawAmmo2';
        Weapon.Kickback 0;
        Weapon.SlotNumber 1;
        Weapon.SelectionOrder 2200;
        +WEAPON.MELEEWEAPON;
        +WEAPON.AMMO_OPTIONAL;

        RR_Weapon.SelectOffset 30, 45;
    }

    States(Actor)
    {
        Spawn:
            ASAW AABBCC 1 NODELAY A_ChainsawOnGround;
            Loop;
    }

    States(Weapon)
    {
        SelectAnim:
            SAWI A 1;
            SAWI A 1 A_MoveWeaponSprite(-6, -10);
            SAWI A 1 A_MoveWeaponSprite(-4, -10);
            SAWI A 1 A_MoveWeaponSprite(-7, -9);
            SAWI A 1 A_MoveWeaponSprite(-3, -6);
            SAWI A 1 A_MoveWeaponSprite(-10, -10);
            Goto Select;
        SelectAnimReady:
            SAWI A 1;
            SAWI B 1 A_MoveWeaponSprite(-6, -10);
            SAWI C 1 A_MoveWeaponSprite(-4, -10);
            SAWI D 1 A_MoveWeaponSprite(-7, -9);
            SAWI E 1 A_MoveWeaponSprite(-3, -6);
            SAWI F 1 A_MoveWeaponSprite(-10, -10);
            Goto Select;
        Idle:
            SAWI A 1 A_ChainsawIdle;
            Loop;
        Ready:
            SAWI ABCDEF 1 A_ChainsawReady;
            Loop;
        DeselectAnim:
            SAWI A 1 A_MoveWeaponSprite(10, 10);
            SAWI A 1 A_MoveWeaponSprite(3, 6);
            SAWI A 1 A_MoveWeaponSprite(7, 9);
            SAWI A 1 A_MoveWeaponSprite(4, 10);
            SAWI A 1 A_MoveWeaponSprite(6, 10);
            Goto Deselect;
        RevUp:
            SAWY A 2;
            SAWZ BA 2;
            SAWP A 2 A_ChainsawPull;
            SAWP BCDEF 2;
            SAWP G 2 A_ChainsawStart;
            SAWP HJKLMNO 2;
            SAWI ABCDEF 2;
            Goto Ready;
        RevDown:
            SAWI A 15 A_ChainsawStop;
            Goto Idle;
        Fire:
            SAWH A 1 A_ChainsawCharge;
            SAWH BCD 1;
            // Fall through here
        Hold:
            SAWH E 1 A_ChainsawAttack;
            SAWH FGF 1;
            SAWH G 1 A_ChainsawReFire;
            SAWH E 1;
            SAWH D 1 A_ChainsawRelease;
            SAWH CBA 1;
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawOnGround
    //
    // Handles the chainsaw's behavior while on the ground depending on mode:
    //
    // - If the current mode is Idle, sets tics to -1.
    //
    // - If the current mode is Running, loops the running sound,
    //   or switches mode to Idle if there is no more ammo left.
    //
    // - If the current mode is Attacking, performs a weaker melee attack
    //   every 4 ticks, or, if the cooldown has ended, switches to Running.
    //
    //===========================================================================
    private void A_ChainsawOnGround()
    {
        if (GetMode() == CM_Attacking)
        {
            // If the cooldown has ended, switch to normal running mode.
            if (!IsInCooldown())
            {
                SetMode(CM_Running);
            }
            else if (Level.maptime % 4 == 0)
            {
                // Attack!
                // The code below is mostly copy-pasted from gzdoom.pk3 (A_M_Saw),
                // simplified from its original form.

                FTranslatedLineTarget t;

                int damage = Random[RR_Chainsaw__A_ChainsawGroundAttack](1, 10);
                double ang = angle + Random2[RR_Chainsaw__A_ChainsawGroundAttack]() * (5.625 / 256);

                // Note the LAF_TARGETISSOURCE flag.
                // This causes the damage to be reattributed to the chainsaw's target,
                // which in this case is its previous owner.
                LineAttack(angle, SAWRANGE, AimLineAttack (angle, SAWRANGE), damage, 'Melee', 'RR_ChainsawPuff', LAF_NORANDOMPUFFZ|LAF_TARGETISSOURCE, t);

                if (t.linetarget)
                {
                    // turn to face target
                    ang = t.angleFromSource;
                    double anglediff = deltaangle(angle, ang);

                    if (anglediff < 0.0)
                    {
                        if (anglediff < -4.5)
                            angle = ang + 90.0 / 21;
                        else
                            angle -= 4.5;
                    }
                    else
                    {
                        if (anglediff > 4.5)
                            angle = ang - 90.0 / 21;
                        else
                            angle += 4.5;
                    }
                }
            }
        }

        if (GetMode() == CM_Running && !IsInCooldown())
        {
            // If we've run out of ammo, switch to idle mode.
            if (!HasAmmo())
            {
                PlaySound(SND_STOP);
                SetMode(CM_Idle);
            }
            else
            {
                // Loop the ready sound
                PlaySound(SND_READY, true);
            }
        }

        if (GetMode() == CM_Idle)
        {
            // Stop animation in idle mode.
            tics = -1;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawIdle
    //
    // Calls A_WeaponReady and checks for manual rev-up request.
    //
    //===========================================================================
    action(Weapon) state A_ChainsawIdle()
    {
        bool hasAmmo = invoker.HasAmmo();

        // Rev up the chainsaw (if possible) by using the reload button.
        if (hasAmmo && player != null && (player.cmd.buttons & BT_RELOAD))
        {
            return invoker.GetRevUpState();
        }

        // Call A_WeaponReady, but do not fire if revving up is not possible.
        A_WeaponReady(hasAmmo ? 0 : WRF_NOFIRE);
        return null;
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawReady
    //
    // Sets the droppable flag. If not in cooldown, also calls A_WeaponReady
    // and handles both automatic and manual rev-down transitions.
    //
    //===========================================================================
    action(Weapon) state A_ChainsawReady()
    {
        invoker.SetDroppable(true);

        // If not in cooldown, handle rev-down state transition
        // and call A_WeaponReady if there is ammo.
        if (!invoker.IsInCooldown())
        {
            if (!invoker.HasAmmo() || (player.cmd.buttons & BT_RELOAD))
            {
                // Rev down the chainsaw due to manual request or if there is no more gasoline.
                return invoker.GetRevDownState();
            }

            invoker.PlaySound(SND_READY, true);
            A_WeaponReady();
        }

        return null;
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawPull
    //
    // Plays the pull sound and clears the droppable flag.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawPull()
    {
        // Do not allow dropping the chainsaw while revving up.
        invoker.SetDroppable(false);
        invoker.PlaySound(SND_PULL);
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawStart
    //
    // Plays the start sound and switches the mode to running.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawStart()
    {
        invoker.PlaySound(SND_START);
        invoker.SetMode(CM_Running);
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawCharge
    //
    // Plays the charging sound and switches the mode to attacking.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawCharge()
    {
        invoker.PlaySound(SND_CHARGE);
        invoker.SetMode(CM_Attacking);
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawAttack
    //
    // Performs the chainsaw attack.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawAttack()
    {
        // Play the attack sound
        invoker.PlaySound(invoker.AttackSound, true);

        // The code below is mostly copy-pasted from gzdoom.pk3 (A_Saw),
        // simplified from its original form.
        // This is needed because we have to know whether we've hit a target
        // in order to give the player a defense boost.

        FTranslatedLineTarget t;

        int dmg = 2 * Random[RR_Chainsaw__A_ChainsawAttack](1, 10);
        int range = int(MeleeRange + MELEEDELTA + (1. / 65536.)); // MBF21 SAWRANGE;

        double ang = angle + 2.8125 * (Random2[Saw]() / 255.);
        double slope = AimLineAttack(ang, range, t);

        let [puff, actualdamage] = LineAttack(ang, range, slope, dmg, 'Melee', 'RR_ChainsawPuff', LAF_NORANDOMPUFFZ, t);

        if (!t.linetarget)
        {
            return;
        }

        // Any damage inflicted causes the protection effect to kick in.
        if (actualdamage > 0)
        {
            invoker.SetProtection();
        }

        // turn to face target
        double anglediff = deltaangle(angle, t.angleFromSource);

        if (anglediff < 0.0)
        {
            if (anglediff < -4.5)
                angle = t.angleFromSource + 90.0 / 21;
            else
                angle -= 4.5;
        }
        else
        {
            if (anglediff > 4.5)
                angle = t.angleFromSource - 90.0 / 21;
            else
                angle += 4.5;
        }

        bJustAttacked = true;
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawReFire
    //
    // Calls A_ReFire if the chainsaw has gasoline.
    // Otherwise, calls A_ClearReFire.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawReFire()
    {
        if (invoker.HasAmmo())
        {
            A_ReFire();
        }
        else
        {
            A_ClearReFire();
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawRelease
    //
    // Plays the release sound, switches mode to running
    // and initiates the attack cooldown.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawRelease()
    {
        invoker.PlaySound(SND_RELEASE);
        invoker.SetMode(CM_Running);
        invoker.SetCooldown();
    }

    //===========================================================================
    //
    // RR_Chainsaw::A_ChainsawStop
    //
    // Plays the stop sound and switches the mode to idle.
    //
    //===========================================================================
    action(Weapon) void A_ChainsawStop()
    {
        invoker.PlaySound(SND_STOP);
        invoker.SetMode(CM_Idle);
        invoker.ResetProtection();
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetSelectAnimState
    //
    // Returns the correct select animation
    // based on the state the chainsaw is in.
    //
    //===========================================================================
    override state GetSelectAnimState()
    {
        return GetMode() == CM_Idle
            ? Super.GetSelectAnimState()
            : ResolveState('SelectAnimReady');
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetDeselectState
    //
    // Checks whether the chainsaw needs to be revved down before deselecting.
    //
    //===========================================================================
    override state GetDeselectState()
    {
        return GetMode() == CM_Idle
            ? Super.GetDeselectState()
            : GetRevDownState();
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetDeselectAnimState
    //
    // If the chainsaw is not in idle state, falls back to GetDeselectState.
    // Otherwise, returns the normal deselect animation state.
    //
    //===========================================================================
    override state GetDeselectAnimState()
    {
        return GetMode() == CM_Idle
            ? Super.GetDeselectAnimState()
            : null; // <- This makes us fall back to GetDeselectState
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetReadyState
    //
    // Returns the correct ready state based on the chainsaw state.
    //
    //===========================================================================
    override state GetReadyState()
    {
        return GetMode() == CM_Idle
            ? ResolveState('Idle')
            : Super.GetReadyState();
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetAtkState
    //
    // Checks whether the chainsaw needs to be revved down before firing.
    //
    //===========================================================================
    override state GetAtkState(bool hold)
    {
        // Rev up the chainsaw before firing.
        return GetMode() == CM_Idle
            ? GetRevUpState()
            : Super.GetAtkState(hold);
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetRevUpState
    //
    // Returns the rev up state.
    //
    //===========================================================================
    private state GetRevUpState()
    {
        return ResolveState('RevUp');
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetRevDownState
    //
    // Returns the rev down state.
    //
    //===========================================================================
    private state GetRevDownState()
    {
        return ResolveState('RevDown');
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetAmmoCount
    //
    // Returns the current ammo count.
    //
    //===========================================================================
    double GetAmmoCount() const
    {
        return _ammoCount;
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetMaxAmmoCount
    //
    // Returns the maximum ammo count.
    //
    //===========================================================================
    double GetMaxAmmoCount() const
    {
        return MAX_AMMO_COUNT;
    }

     //===========================================================================
    //
    // RR_Chainsaw::HasAmmo
    //
    // Returns true if the current ammo count is non-zero.
    //
    //===========================================================================
    private bool HasAmmo() const
    {
        return GetAmmoCount() > 0;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetAmmoCount
    //
    // Sets the ammo counter to an absolute value
    // or increments/decrements it by a relative value.
    //
    //===========================================================================
    private void SetAmmoCount(double newCount, bool relative = false)
    {
        double newAmmoCount = (relative ? _ammoCount : 0) + newCount;

        // Make sure we're not trying to set ammo to a negative value
        if (!relative && newAmmoCount < 0)
        {
            ThrowAbortException("%s: Ammo count cannot be negative.", GetClassName());
        }

        // Clamp the ammo count to the max value
        _ammoCount = Clamp(newAmmoCount, 0, MAX_AMMO_COUNT);

        // Synchronize with the HUD
        SyncAmmoCounters(true, false);
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetAmmoDelta
    //
    // Returns the amount of ammo to add/subtract during the current tick.
    //
    //===========================================================================
    private double GetAmmoDelta() const
    {
        let mode = GetMode();

        // Regenerate in idle mode and when infinite ammo is enabled.
        bool regen = mode == CM_Idle || IsInfiniteAmmo();

        double rate = regen
            ? AMMO_REGEN_RATE
            : AMMO_DRAIN_RATE;

        // Non-idle rates are affected by the upgrade level.
        if (!regen)
        {
            // Each next upgrade level multiplies the rate by a certain factor.
            // Since this factor is less than 1, the rate decreases.
            // Note the minus sign - this indicates that ammo is being drainwed.
            rate *= -(1.0/GetUpgradeLevel() ** AMMO_DRAIN_UPGRADE_FACTOR);

            // While attacking, ammo drains faster.
            if (mode == CM_Attacking)
            {
                rate *= AMMO_DRAIN_ATTACK_MULTIPLIER;
            }
        }

        // Convert units per second to units per tick
        return rate / GameTicRate;
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetUpgradeLevel
    //
    // Returns the current upgrade level.
    //
    //===========================================================================
    int GetUpgradeLevel() const
    {
        return _upgradeLevel;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetUpgradeLevel
    //
    // Sets the upgrade level to an absolute value
    // or increments/decrements it by a relative value.
    //
    //===========================================================================
    bool SetUpgradeLevel(int upgradeLevel, bool relative = false)
    {
        int newUpgradeLevel = (relative ? _upgradeLevel : 0) + upgradeLevel;

        if (newUpgradeLevel < 0)
        {
            ThrowAbortException("%s: Upgrade level cannot be negative.", GetClassName());
        }

        // In multiplayer, the upgrade level is clamped to a maximum.
        if (multiplayer)
        {
            newUpgradeLevel = Min(newUpgradeLevel, MULTIPLAYER_UPGRADE_LEVEL);
        }

        bool upgraded = newUpgradeLevel > _upgradeLevel;

        _upgradeLevel = newUpgradeLevel;
        SyncAmmoCounters(false, true);

        return upgraded;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SyncAmmoCounters
    //
    // Synchronizes the displayed ammo counters with actual values.
    //
    //===========================================================================
    private void SyncAmmoCounters(bool primary, bool upgrade)
    {
        if (primary && Ammo1 != null)
        {
            Ammo1.Amount = int(_ammoCount);
            Ammo1.MaxAmount = MAX_AMMO_COUNT;
        }

        if (upgrade && Ammo2 != null)
        {
            Ammo2.Amount = Ammo2.MaxAmount = _upgradeLevel;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::DestroyAmmo
    //
    // Removes attached ammo items, if any.
    //
    //===========================================================================
    private void DestroyAmmo()
    {
        if (Ammo1 != null)
        {
            Ammo1.Destroy();
            Ammo1 = null;
        }

        if (Ammo2 != null)
        {
            Ammo2.Destroy();
            Ammo2 = null;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetMode
    //
    // Gets the chainsaw's current mode of operation.
    //
    //===========================================================================
    private EChainsawMode GetMode() const
    {
        return _mode;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetMode
    //
    // Sets the chainsaw's current mode of operation.
    // This is used to control the ammo drain/regen
    // and also handle level transitions and correctly switch
    // between dropped/picked up states.
    //
    //===========================================================================
    private void SetMode(EChainsawMode newMode)
    {
        _mode = newMode;
    }

    //===========================================================================
    //
    // RR_Chainsaw::IsDroppable
    //
    // Gets whether the chainsaw can now be dropped.
    //
    //===========================================================================
    private bool IsDroppable() const
    {
        return _isDroppable;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetDroppable
    //
    // Sets whether the chainsaw can now be dropped.
    //
    //===========================================================================
    private void SetDroppable(bool droppable)
    {
        _isDroppable = droppable;
    }

    //===========================================================================
    //
    // RR_Chainsaw::IsInCooldown
    //
    // Gets whether the chainsaw is currently on cooldown
    // after an attack.
    //
    //===========================================================================
    private bool IsInCooldown() const
    {
        return _cooldownTimer > 0;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetCooldown
    //
    // Initiates the cooldown period.
    //
    //===========================================================================
    private void SetCooldown()
    {
        _cooldownTimer = COOLDOWN_DELAY_TICKS;
    }

    //===========================================================================
    //
    // RR_Chainsaw::ResetCooldown
    //
    // Resets the cooldown state.
    //
    //===========================================================================
    private void ResetCooldown()
    {
        _cooldownTimer = 0;
    }

    //===========================================================================
    //
    // RR_Chainsaw::HandleCooldown
    //
    // If the chainsaw is currently in cooldown, decrements the cooldown timer.
    //
    //===========================================================================
    private void HandleCooldown()
    {
        if (_cooldownTimer > 0)
        {
            _cooldownTimer--;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::IsProtectionActive
    //
    // Checks whether the protection effect is currently active.
    //
    //===========================================================================
    private bool IsProtectionActive(bool visualOnly = false) const
    {
        return _protectionTimer > (visualOnly ? 0 : PROTECTION_DURATION_TICKS);
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetProtectionRemainingDuration
    //
    // Gets the remaning protection duration as a fraction of the total
    // protection duration.
    //
    //===========================================================================
    private double GetProtectionRemainingDuration() const
    {
        return double(_protectionTimer) / PROTECTION_DURATION_TICKS / 2;
    }

    //===========================================================================
    //
    // RR_Chainsaw::SetProtection
    //
    // Activates the protection effect.
    //
    //===========================================================================
    private void SetProtection()
    {
        // NB: the duration here is doubled,
        // but the actual damage-protection effect lasts exactly as advertised.
        // The extra length is needed to properly fade the blend color.
        _protectionTimer = 2 * PROTECTION_DURATION_TICKS;
    }

    //===========================================================================
    //
    // RR_Chainsaw::HandleProtection
    //
    // If the protection effect is currently active, decrements the timer.
    //
    //===========================================================================
    private void HandleProtection()
    {
        if (_protectionTimer > 0)
        {
            _protectionTimer--;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::ResetProtection
    //
    // Resets the protection effect.
    //
    //===========================================================================
    private void ResetProtection()
    {
        _protectionTimer = 0;
    }

    //===========================================================================
    //
    // RR_Chainsaw::HandleSmoke
    //
    // If active, spawns some smoke periodically.
    //
    //===========================================================================
    private void HandleSmoke()
    {
        // Do not spawn when idle or if the corresponding setting
        // is turned off.
        if (GetMode() == CM_Idle || !RR_Settings.GetCached(RR_SETTING_CHAINSAWSMOKE, _smokeSetting).GetPlayValue().IsOn())
        {
            return;
        }

        // When attacking, spawn twice as much smoke.
        bool attacking = GetMode() == CM_Attacking;
        int freq = attacking ? 2 : 4;

        if ((Level.maptime & freq) > 0)
        {
            return;
        }

        if (Owner == null)
        {
            // Spawn directly
            let smokeOffset = (
                FRandom[RR_Chainsaw__HandleSmoke](-4, 4),
                FRandom[RR_Chainsaw__HandleSmoke](-4, 4),
                12
            );

            Spawn('RR_ChainsawSmoke', Pos + smokeOffset, ALLOW_REPLACE);
        }
        else
        {
            // Spawn from owner, using offset calculations
            let baseOffset = (8, -6);

            if (attacking)
            {
                // When attacking, sprite is shifted upwards and to the left
                baseOffset += (-2, 0.5);
            }

            SpawnEffect(
                'RR_ChainsawSmoke',
                Owner,
                (baseOffset.X + FRandom[RR_Chainsaw__HandleSmoke](-2, 2), baseOffset.Y),
                (0, 0, 0),
                (0, 0, 0)
            );
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::BeginPlay
    //
    // Sets up the initial state.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        // By default, ammo is at max and the chainsaw can be dropped
        SetAmmoCount(MAX_AMMO_COUNT);
        SetDroppable(true);
    }

    //===========================================================================
    //
    // RR_Chainsaw::PostBeginPlay
    //
    // Sets the initial upgrade level.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // Dropped chainsaws do not upgrade other chainsaws.
        // In multiplayer, all chainsaws are already upgraded
        // and cannot be upgraded any further.
        int newUpgradeLevel = bTossed
            ? 0
            : multiplayer ? MULTIPLAYER_UPGRADE_LEVEL : 1;

        SetUpgradeLevel(newUpgradeLevel);
    }

    //===========================================================================
    //
    // RR_Chainsaw::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_CHARGE);
        MarkSound(SND_PULL);
        MarkSound(SND_READY);
        MarkSound(SND_RELEASE);
        MarkSound(SND_START);
        MarkSound(SND_STOP);
        MarkSound(SND_PROTECT);
        MarkSound(SND_UPGRADE);
    }

    //===========================================================================
    //
    // RR_Chainsaw::Tick
    //
    // Handles ammo drain or regeneration, cooldown and protection effects.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        // Regenerate or drain ammo based on the current mode of operation
        SetAmmoCount(GetAmmoDelta(), true);

        // Synchronize the ammo counters
        SyncAmmoCounters(true, true);

        // Handle cooldown
        HandleCooldown();

        // Handle the protection effect
        HandleProtection();

        // Handle smoke spawning
        HandleSmoke();
    }

    //===========================================================================
    //
    // RR_Chainsaw::PreTravelled
    //
    // Before a level transition, an existing sound player needs to be removed,
    // otherwise it will persist if we return to this level later (in a hub).
    //
    //===========================================================================
    override void PreTravelled()
    {
        if (_soundPlayer != null)
        {
            _soundPlayer.Destroy();
            _soundPlayer = null;
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::Travelled
    //
    // Resets the state after a level transition.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();

        // Downgrade mode from attacking to running
        if (GetMode() == CM_Attacking)
        {
            SetMode(CM_Running);
        }

        // Reset the attack cooldown
        ResetCooldown();

        // Reset the protection effect
        ResetProtection();

        // Reset the droppable flag
        // (if the player exited the level during the rev-up animation)
        SetDroppable(true);
    }

    //===========================================================================
    //
    // RR_Chainsaw::AttachToOwner
    //
    // After picking up the chainsaw in deathmatch, ammo counters
    // become desynchronized, so synchronize them back.
    //
    //===========================================================================
    override void AttachToOwner(Actor other)
    {
        Super.AttachToOwner(other);
        SyncAmmoCounters(true, true);
    }

    //===========================================================================
    //
    // RR_Chainsaw::CreateTossable
    //
    // If the chainsaw can be dropped, removes it from the player's inventory
    // and switches to the ground state corresponding to the current mode.
    //
    //===========================================================================
    override Inventory CreateTossable(int amt)
    {
        // Make sure we can drop the chainsaw right now
        bool droppable = Owner != null && Owner.Health <= 0 || IsDroppable();

        if (droppable)
        {
            // Store the previous owner in the Target field.
            // This comes in handy later to properly attribute damage
            // to the ground attack function.
            Target = Owner;

            // NB: adjust angle here because the attack function will be called
            // immediately, before Actor::DropInventory performs the adjustment.
            Angle = Owner.Angle;

            // If attacking, set cooldown for ground attack
            if (GetMode() == CM_Attacking)
            {
                SetCooldown();
                PlaySound(SND_RELEASE);
            }

            // Reset the protection effect
            ResetProtection();

            // Reset the droppable flag
            SetDroppable(true);

            return Super.CreateTossable(amt);
        }

        return null;
    }

    //===========================================================================
    //
    // RR_Chainsaw::DetachFromOwner
    //
    // Adjusts the chainsaw's state accordingly before removal from inventory.
    //
    //===========================================================================
    override void DetachFromOwner()
    {
        // Destroy ammo items to avoid confusing AltHUD users.
        DestroyAmmo();

        // If a sound player is attached, make sure it now sticks
        // to the chainsaw itself instead of the player.
        // (If we're later destroyed, the player will be destroyed as well.)
        if (_soundPlayer != null)
        {
            _soundPlayer.SetParent(self);
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::OwnerDied
    //
    // Reset all effects upon death.
    //
    //===========================================================================
    override void OwnerDied()
    {
        Super.OwnerDied();

        // If we haven't been dropped, all effects need to be reset
        // in case the player keeps their weapons upon respawning.
        if (Owner != null)
        {
            // If we're running, stop now.
            if (GetMode() != CM_Idle)
            {
                PlaySound(SND_STOP);
                SetMode(CM_Idle);
            }

            // Reset protection and cooldown
            ResetProtection();
            ResetCooldown();
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::TryPickup
    //
    // Checks whether this chainsaw can be picked up.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        // The chainsaw cannot be picked up during ground attack
        return GetMode() != CM_Attacking && Super.TryPickup(toucher);
    }

    //===========================================================================
    //
    // RR_Chainsaw::HandlePickup
    //
    // Checks whether the player can pick up another chainsaw right now.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        let csaw = RR_Chainsaw(item);

        if (csaw != null && csaw.GetClass() == GetClass())
        {
            // To pick up another chainsaw, the following conditions should be met:
            // 0) The other chainsaw is not subject to weapon stay
            // 1) The other chainsaw is not in attacking state (handled in TryPickup)
            // 2) At least one of the chainsaws is not running
            let otherMode = csaw.GetMode();

            if (!csaw.ShouldStay() && (otherMode == CM_Idle || GetMode() == CM_Idle))
            {
                // Other chainsaw is picked up
                csaw.bPickupGood = true;

                // If the other chainsaw is running, stop it.
                if (otherMode == CM_Running)
                {
                    csaw.PlaySound(SND_STOP);
                }

                // Add this chainsaw's ammo to ours
                SetAmmoCount(csaw.GetAmmoCount(), true);

                // Add this chainsaw's technological distinctiveness to our own
                if (SetUpgradeLevel(csaw.GetUpgradeLevel(), true))
                {
                    Owner.A_StartSound(SND_UPGRADE, RR_SoundChannels.CHAN_CHAINSAW_UPGRADE, CHAN_MAYBE_LOCAL);
                }
            }

            // Pickup is handled regardless
            return true;
        }

        return Super.HandlePickup(item);
    }

    //===========================================================================
    //
    // RR_Chainsaw::ModifyDamage
    //
    // If protection is in effect, incoming damage is cut in half,
    // but never reduced to zero.
    //
    //===========================================================================
    override void ModifyDamage(int damage, Name damageType, out int newdamage, bool passive, Actor inflictor, Actor source, int flags)
    {
        if (!passive || damage == 0 || inflictor == null || Owner == null || !IsProtectionActive())
        {
            return;
        }

        bool directAttack = false;
        double ang;

        // First, check for explosion damage,
        // which is considered a direct attack.
        if (flags & DMG_EXPLOSION)
        {
            directAttack = true;
        }
        else if (flags & DMG_USEANGLE)
        {
            // Then, check for DMG_USEANGLE,
            // which corresponds to line attacks.

            if (flags & DMG_INFLICTOR_IS_PUFF)
            {
                // If the inflictor is a puff, the attack angle
                // is the angle from the source to the puff.
                if (source != null)
                {
                    ang = source.AngleTo(inflictor);
                }
                else
                {
                    // There is no source, so it is not possible
                    // to determine where the attack came from.
                    return;
                }
            }
            else
            {
                // Othewise, the attack is direct
                // from inflictor to owner.
                directAttack = true;
            }
        }
        else if (inflictor.bMissile)
        {
            // For missile-based attacks, simply use the missile angle.
            ang = inflictor.Angle;
        }
        else if (inflictor == source)
        {
            // If the inflictor and the source are one and the same,
            // it could be some kind of melee attack,
            // so assume it is direct from inflictor to owner.
            directAttack = true;
        }
        else
        {
            // Otherwise, it is not possible to reliably
            // determine the kind of attack, so we abort.
            return;
        }

        // Direct calculation for melee and non-puff-based line attacks.
        if (directAttack)
        {
            ang = inflictor.AngleTo(Owner);
        }

        // If the angle difference is greater than 90 degrees, that means the attack is frontal.
        if (Abs(deltaangle(Owner.Angle, ang)) > 90)
        {
            // Make sure we always get at least 1 damage.
            newdamage = Max(1, damage / 2);

            Owner.A_StartSound(SND_PROTECT, RR_SoundChannels.CHAN_CHAINSAW_SHIELD);
        }
    }

    //===========================================================================
    //
    // RR_Chainsaw::GetBlend
    //
    // Visualizes the remaining duration of the protection effect.
    //
    //===========================================================================
    override Color GetBlend()
    {
        if (IsProtectionActive(true))
        {
            int g = int(GetProtectionRemainingDuration() * 128);
            return Color(g, 64, 64, 64);
        }

        return Super.GetBlend();
    }

    //===========================================================================
    //
    // RR_Chainsaw::PlaySound
    //
    // Plays a sound via a separate source or the current chainsaw's owner
    // depending on the game mode (a sound source cannot be used in netgames
    // due to prediction issues).
    //
    //===========================================================================
    private void PlaySound(sound snd, bool looping = false)
    {
        if (_soundPlayer == null)
        {
            _soundPlayer = RR_ChainsawSoundPlayer(Spawn('RR_ChainsawSoundPlayer'));
        }

        if (_soundPlayer != null)
        {
            // Play the sound.
            _soundPlayer.SetParent(Owner != null ? Owner : Actor(self));
            _soundPlayer.PlaySound(snd, looping);
        }
    }
}
