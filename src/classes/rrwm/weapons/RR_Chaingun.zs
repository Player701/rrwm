//===========================================================================
//
// RR_Chaingun
//
// The ZEN-II SMG, aka the Chaingun.
// Sprite source: Zen Dynamics (Xaser)
// Sound source: Doom 3 (id Software)
//
//===========================================================================
class RR_Chaingun : RR_HitscanWeapon replaces Chaingun
{
    // Additional sounds for precaching
    const SND_RELOAD_1 = "rrwm/weapons/chaingun/reload1";
    const SND_RELOAD_2 = "rrwm/weapons/chaingun/reload2";

    Default
    {
        Scale 0.8;
        Obituary "$OB_RR_CHAINGUN";
        Tag "$TAG_RR_CHAINGUN";
        AttackSound "rrwm/weapons/chaingun/fire";

        Inventory.Icon "ZSMGA0";
        Inventory.PickupSound "rrwm/weapons/chaingun/pickup";
        Inventory.PickupMessage "$PICKUP_RR_CHAINGUN";

        Weapon.AmmoType 'RR_Clip';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 20;
        Weapon.SelectionOrder 700;
        Weapon.SlotNumber 4;

        RR_Weapon.SelectOffset 46, 66;
        RR_Weapon.SelectSound "rrwm/weapons/chaingun/up";
        RR_Weapon.AimRecoil 1.75, 2.0, 0.4;
        RR_Weapon.ShakeRecoil 1.5, 0;

        RR_ReloadableWeapon.ClipCapacity 50;
        RR_ReloadableWeapon.EmptyClipType 'RR_ChaingunEmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset 3.0, -13.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.1, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/chaingun/dryfire";

        RR_HitscanWeapon.PuffType 'RR_ChaingunPuff';
        RR_HitscanWeapon.CasingType 'RR_ChaingunCasing';
        RR_HitscanWeapon.CasingSpawnOffset 6.0, -3.0;
        RR_HitscanWeapon.CasingSpawnVelocity 0.0, -1.5, 1.5;
        RR_HitscanWeapon.CasingVelocitySpread 0.1, 0.3, 0.3;
    }

    States(Actor)
    {
        Spawn:
            ZSMG A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            ZENG A 1;
            TNT1 A 0 A_SelectSound;
            ZENG AA 1 A_MoveWeaponSprite(-10, -14);
            ZENG AA 1 A_MoveWeaponSprite(-8, -12);
            ZENG A 1 A_MoveWeaponSprite(-6, -8);
            ZENG A 1 A_MoveWeaponSprite(-4, -6);
            Goto Select;
        Ready:
            ZENG A 1 A_ReloadableWeaponReady;
            Loop;
        DeselectAnim:
            ZENG A 1 A_MoveWeaponSprite(4, 6);
            ZENG A 1 A_MoveWeaponSprite(6, 8);
            ZENG AA 1 A_MoveWeaponSprite(8, 12);
            ZENG AA 1 A_MoveWeaponSprite(10, 14);
            Goto Deselect;
        Fire:
            ZENG B 1 BRIGHT A_ChaingunAttack;
            ZENG C 2 BRIGHT A_EjectCasing;
            ZENG B 1 BRIGHT;
            TNT1 A 0 A_ReFire;
            Goto Ready;
        Flash:
            TNT1 A 1 A_Light1;
            TNT1 A 2 A_Light2;
            TNT1 A 1 A_Light1;
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            ZEND A 1 A_DryFire;
            ZEND B 2;
            ZEND A 1;
            ZENG A 8;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        Reload:
            ZENG A 5;
            ZENG D 1 A_PlayWeaponAuxSound(SND_RELOAD_1);
            ZENG EFGH 1;
            ZENG I 15 A_EjectClip;
            ZENG JKLM 1;
            ZENG NOP 2;
            ZENG Q 2 A_PlayWeaponAuxSound(SND_RELOAD_2);
            ZENG R 2 A_ReloadFullClip;
            ZENG STUVWXYZ 2;
            ZNG2 AB 2;
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_Chaingun::A_ChaingunAttack
    //
    // Fires the ZEN-II SMG, aka the Chaingun.
    //
    //===========================================================================
    action(Weapon) void A_ChaingunAttack()
    {
        A_GunFlash();
        A_FireHitscan(5, 1, 5.625);
    }

    //===========================================================================
    //
    // RR_Chaingun::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
    }
}
