//===========================================================================
//
// RR_PlasmaRifle
//
// The Hyperblaster, aka the Plasma Rifle.
// Sprite source: Zen Dynamics (Xaser)
// Sound source: Doom 3 (id Software), Quake 4 (id Software)
//
//===========================================================================
class RR_PlasmaRifle : RR_ReloadableWeapon replaces PlasmaRifle
{
    // Additional sounds for precaching
    const SND_CHARGE    = "rrwm/weapons/plasmarifle/charge";
    const SND_DISCHARGE = "rrwm/weapons/plasmarifle/discharge";
    const SND_RELOAD_1  = "rrwm/weapons/plasmarifle/reload1";
    const SND_RELOAD_2  = "rrwm/weapons/plasmarifle/reload2";

    Default
    {
        Scale 0.75;
        Tag "$TAG_RR_PLASMARIFLE";

        Inventory.Icon "HYPRA0";
        Inventory.PickupMessage "$PICKUP_RR_PLASMARIFLE";
        Inventory.PickupSound "rrwm/weapons/plasmarifle/pickup";

        Weapon.AmmoType 'RR_Cell';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 40;
        Weapon.SelectionOrder 100;
        Weapon.SlotNumber 6;

        RR_Weapon.SelectOffset -181, 4;
        RR_Weapon.SelectSound "rrwm/weapons/plasmarifle/up";
        RR_Weapon.ShakeRecoil 0.75, 0.75;

        RR_ReloadableWeapon.ClipCapacity 60;
        RR_ReloadableWeapon.EmptyClipType 'RR_PlasmaRifleEmptyClip';
        RR_ReloadableWeapon.EmptyClipSpawnOffset -3.0, -13.0;
        RR_ReloadableWeapon.EmptyClipSpawnVelocity 0.0, 1.5, 0.0;
        RR_ReloadableWeapon.EmptyClipVelocitySpread 0.1, 0.2, 0.0;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/plasmarifle/dryfire";
    }

    States(Actor)
    {
        Spawn:
            HYPR A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            HYPX A 1;
            HYPX A 1 A_MoveWeaponSprite(18, -7);
            HYPX A 1 A_MoveWeaponSprite(21, -5);
            HYPX A 1 A_MoveWeaponSprite(17, -7);
            HYPX A 1 A_MoveWeaponSprite(19, -3);
            HYPX A 1 A_MoveWeaponSprite(28, -1);
            HYPX A 1 A_MoveWeaponSprite(25, 2);
            TNT1 A 0 A_SelectSound;
            HYPX A 1 A_MoveWeaponSprite(24, 3);
            HYPX A 1 A_MoveWeaponSprite(17, 5);
            HYPX A 1 A_MoveWeaponSprite(12, 9);
            Goto Select;
        Ready:
            HYPR E 1 A_ReloadableWeaponReady;
            Loop;
        DeselectAnim:
            HYPX A 1;
            HYPX A 1 A_MoveWeaponSprite(-12, -9);
            HYPX A 1 A_MoveWeaponSprite(-17, -5);
            HYPX A 1 A_MoveWeaponSprite(-24, -3);
            HYPX A 1 A_MoveWeaponSprite(-25, -2);
            HYPX A 1 A_MoveWeaponSprite(-28, 1);
            HYPX A 1 A_MoveWeaponSprite(-19, 3);
            HYPX A 1 A_MoveWeaponSprite(-17, 7);
            HYPX A 1 A_MoveWeaponSprite(-21, 5);
            HYPX A 1 A_MoveWeaponSprite(-18, 7);
            Goto Deselect;
        Fire:
            HYPR B 4 BRIGHT A_PlasmaRifleCharge;
            HYPR C 4 BRIGHT;
            // Fall through here
        Firing:
            HYPR D 1 BRIGHT A_PlasmaRifleAttack;
            TNT1 A 0 A_ReFire;
            Goto DryFireCooldown;
        Hold:
            HYPR C 2 BRIGHT A_PlasmaRifleHold;
            Goto Firing;
        DryFireCooldown:
            HYPR C 4 BRIGHT A_PlasmaRifleDischarge;
            HYPR B 4 BRIGHT;
            HYPR E 4;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        DryFire:
            HYPR E 16 A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        Flash:
            TNT1 A 8 A_Light1;
            TNT1 A 1 A_Light2;
            TNT1 A 0 A_Light0;
            Stop;
        HoldFlash:
            TNT1 A 2 A_Light1;
            TNT1 A 1 A_Light2;
            TNT1 A 0 A_Light0;
            Stop;
        DischargeFlash:
            TNT1 A 8 A_Light1;
            TNT1 A 0 A_Light0;
            Stop;
        Reload:
            HYPR FGH 3;
            HYPR I 4 A_PlayWeaponAuxSound(SND_RELOAD_1);
            HYPR J 4;
            HYPR K 20 A_EjectClip;
            HYPR JL 3;
            HYPR M 3;
            HYPR N 2 A_PlayWeaponAuxSound(SND_RELOAD_2);
            HYPR N 2 A_ReloadFullClip;
            HYPR O 4;
            HYPR E 10;
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::A_PlasmaRifleCharge
    //
    // Starts the charge flash sequence, alerts monsters
    // and plays the charging sound.
    //
    //===========================================================================
    action(Weapon) void A_PlasmaRifleCharge()
    {
        A_GunFlash();
        A_StartSound(SND_CHARGE, CHAN_WEAPON, CHANF_LOOP);
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::A_PlasmaRifleAttack
    //
    // Fires the Hyperblaster, aka the Plasma Rifle.
    //
    //===========================================================================
    action(Weapon) void A_PlasmaRifleAttack()
    {
        A_AlertMonsters();
        A_FireProjectile('RR_PlasmaBall');

        // Apply recoil
        invoker.DoRecoil();
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::A_PlasmaRifleHold
    //
    // Starts an iteration of the hold flash sequence.
    //
    //===========================================================================
    action(Weapon) void A_PlasmaRifleHold()
    {
        A_GunFlash('HoldFlash');
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::A_PlasmaRifleDischarge
    //
    // Starts the discharge flash sequence, alerts monsters
    // and plays the discharge sound.
    //
    //===========================================================================
    action(Weapon) void A_PlasmaRifleDischarge()
    {
        A_GunFlash('DischargeFlash');
        A_StartSound(SND_DISCHARGE, CHAN_WEAPON);
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_CHARGE);
        MarkSound(SND_DISCHARGE);
        MarkSound(SND_RELOAD_1);
        MarkSound(SND_RELOAD_2);
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::IsCharging
    //
    // Checks if the charge sound is being currently played by the owner.
    //
    //===========================================================================
    private bool IsCharging()
    {
        return Owner != null
            && Owner.player != null
            && Owner.player.ReadyWeapon == self
            && Owner.IsActorPlayingSound(CHAN_WEAPON, SND_CHARGE);
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::CreateTossable
    //
    // If dropped while attacking, stop the charging sound
    // and play the discharge sound.
    //
    //===========================================================================
    override Inventory CreateTossable (int amt)
    {
        bool isLooping = IsCharging();
        let prevOwner = Owner;

        let result = Super.CreateTossable(amt);

        if (result != null && isLooping)
        {
            prevOwner.A_StopSound(CHAN_WEAPON);
            result.A_StartSound(SND_DISCHARGE, CHAN_WEAPON);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::OwnerDied
    //
    // If still held after death, stop the charging sound
    // and play the discharge sound.
    //
    //===========================================================================
    override void OwnerDied()
    {
        Super.OwnerDied();

        if (IsCharging())
        {
           // Play the discharge sound if we haven't been dropped
           // (otherwise it would be handled by CreateTossable)
           Owner.A_StartSound(SND_DISCHARGE, CHAN_WEAPON);
        }
    }

    //===========================================================================
    //
    // RR_PlasmaRifle::OnDestroy
    //
    // If destroyed, the sound needs to be stopped.
    //
    //===========================================================================
    override void OnDestroy()
    {
        if (IsCharging())
        {
            Owner.A_StopSound(CHAN_WEAPON);
        }

        Super.OnDestroy();
    }
}
