//===========================================================================
//
// RR_Fist
//
// The Marine Fists.
// Sprite source: ZDoom Advanced Mod
// Sound source: Doom 3
//
//===========================================================================
class RR_Fist : RR_Weapon
{
    Default
    {
        // For powered up fists only.
        Decal "BulletChip";

        Obituary "$OB_RR_FIST";
        Tag "$TAG_RR_FIST";

        // Fists cannot be dropped.
        +INVENTORY.UNTOSSABLE;

        Weapon.Kickback 100;
        Weapon.SelectionOrder 3700;
        Weapon.SlotNumber 1;
        +WEAPON.WIMPY_WEAPON;
        +WEAPON.MELEEWEAPON;

        RR_Weapon.SelectSound "rrwm/weapons/fist/up";
    }

    States(Weapon)
    {
        Select:
            FIST B 1 A_Raise(10);
            TNT1 A 0 A_SelectSound;
            FIST B 1 A_Raise(10);
            Goto Select+2;
        Deselect:
            FIST B 1 A_Lower(10);
            Loop;
        Ready:
            FIST B 1 A_WeaponReady;
            Loop;
        Fire:
            FIST C 4;
            FIST D 4 A_FistPunch;
            FIST C 4 A_ReFire;
            FIST B 8;
            Goto Ready;
        FirePowered:
            FIST C 3;
            FIST D 3 A_FistPunch;
            FIST C 3 A_ReFire;
            FIST B 3;
            Goto Ready;
        Hold:
            FIST EF 4;
            FIST G 4 A_FistPunch;
            FIST HI 4;
            FIST B 6 A_ClearReFire;
            Goto Ready;
        HoldPowered:
            FIST EF 3;
            FIST G 3 A_FistPunch;
            FIST HI 3;
            FIST B 3 A_ClearReFire;
            Goto Ready;
    }

    //===========================================================================
    //
    // RR_Fist::A_FistPunch
    //
    // Performs the punching attack with the damage and puff type
    // depending on whether the fist is currently powered or sped up.
    //
    //===========================================================================
    action(Weapon) void A_FistPunch()
    {
        invoker.UpdateDecalGenerator();

        // Calculate the damage.
        // NB: Doom's fist uses 1d10 randomization instead of the default 1d8
        int dmg = invoker.GetDamage() * Random[RR_Fist__A_FistPunch](1, 10);
        A_CustomPunch(dmg, true, CPF_NORANDOMPUFFZ, invoker.GetPuffType());
    }

    //===========================================================================
    //
    // RR_Fist::GetRawObituary
    //
    // Returns the correct raw obituary
    // based on whether the fists are currently powered.
    //
    //===========================================================================
    override string GetRawObituary() const
    {
        string obit = Super.GetRawObituary();
        return IsPowered(false) ? obit .. "_POWERED" : obit;
    }

    //===========================================================================
    //
    // RR_Fist::GetAtkState
    //
    // Returns the corresponding fire / hold state
    // depending on whether the player has the corresponding power-up.
    //
    //===========================================================================
    override state GetAtkState(bool hold)
    {
        if (!IsPowered(true))
        {
            return Super.GetAtkState(hold);
        }

        return hold
            ? ResolveState('HoldPowered')
            : ResolveState('FirePowered');
    }

    //===========================================================================
    //
    // RR_Fist::IsPowered
    //
    // Checks whether the player has the Adrenaline or Strength powerups.
    //
    //===========================================================================
    private bool IsPowered(bool fastAttack) const
    {
        bool isStrong = Owner.FindInventory('RR_PowerStrength') || Owner.FindInventory('PowerStrength');

        // If checking for fast attack, also need to check
        // for the Adrenaline powerup as well.
        return fastAttack
            ? isStrong && Owner.FindInventory('RR_PowerAdrenaline')
            : isStrong;
    }

    //===========================================================================
    //
    // RR_Fist::GetPuffType
    //
    // Returns the correct puff type based on whether the fist is currently
    // powered or sped up.
    //
    //===========================================================================
    private class<Actor> GetPuffType() const
    {
        // Super-powered punch!
        // Adrenaline is in effect.
        if (IsPowered(true))
        {
            return 'RR_FistPuffSuperPowered';
        }

        // Powered punch.
        // Adrenaline has worn off, but Strength is still in effect.
        if (IsPowered(false))
        {
            return 'RR_FistPuffPowered';
        }

        // Normal punch.
        return 'RR_FistPuff';
    }

    //===========================================================================
    //
    // RR_Fist::UpdateDecalGenerator
    //
    // Updates the decal generator based on whether the fist is powered.
    //
    //===========================================================================
    private void UpdateDecalGenerator()
    {
        DecalGenerator = IsPowered(false) ? Default.DecalGenerator : null;
    }

    //===========================================================================
    //
    // RR_Fist::GetDamage
    //
    // Returns the correct base damage value based on whether the fist
    // is currently powered.
    //
    //===========================================================================
    private int GetDamage() const
    {
        return IsPowered(false) ? 20 : 2;
    }
}
