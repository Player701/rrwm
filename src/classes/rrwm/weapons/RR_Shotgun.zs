//===========================================================================
//
// RR_Shotgun
//
// The shotgun.
// Sprite source: original source unknown, many PWADs have these sprites
// Sound source: Quake 4 (id Software)
//
//===========================================================================
class RR_Shotgun : RR_HitscanWeapon replaces Shotgun
{
    // Additional sounds for precaching
    const SND_PUMP_1 = "rrwm/weapons/shotgun/pump1";
    const SND_PUMP_2 = "rrwm/weapons/shotgun/pump2";
    const SND_RELOAD = "rrwm/weapons/shotgun/reload";

    // Enumerates possible stages
    // of the shotgun reloading sequence.
    enum EShotgunReloadStage
    {
        // The sequence has not yet started.
        SRS_Started,

        // The sequence is currently in progress.
        SRS_InProgress,

        // Pumping animation will be played
        // on the next A_ShotgunReloadLoop call.
        SRS_PrePump,

        // Pumping animation is playing now.
        SRS_InPump
    };

    // The current state of the realistic reloading sequence.
    private EShotgunReloadStage _curReloadStage;

    Default
    {
        Scale 0.5;
        Obituary "$OB_RR_SHOTGUN";
        Tag "$TAG_RR_SHOTGUN";
        AttackSound "rrwm/weapons/shotgun/fire";

        Inventory.Icon "OSH2A0";
        Inventory.PickupSound "rrwm/weapons/shotgun/pickup";
        Inventory.PickupMessage "$PICKUP_RR_SHOTGUN";

        Weapon.AmmoType 'RR_Shell';
        Weapon.AmmoUse 1;
        Weapon.AmmoGive 8;
        Weapon.SelectionOrder 1300;
        Weapon.SlotNumber 3;

        RR_Weapon.SelectOffset 70, 75;
        RR_Weapon.SelectSound "rrwm/weapons/shotgun/up";
        RR_Weapon.AimRecoil 6, 7, 0.15;

        RR_ReloadableWeapon.ClipCapacity 8;
        RR_ReloadableWeapon.DryFireSound "rrwm/weapons/shotgun/dryfire";

        RR_HitscanWeapon.PuffType 'RR_ShotgunPuff';
        RR_HitscanWeapon.CasingType 'RR_ShotgunCasing';
        RR_HitscanWeapon.CasingSpawnOffset 1.5, -5.5;
        RR_HitscanWeapon.CasingSpawnVelocity 0.0, 1.5, 1.0;
        RR_HitscanWeapon.CasingVelocitySpread 0.1, 0.2, 0.1;
    }

    States(Actor)
    {
        Spawn:
            OSH2 A -1;
            Stop;
    }

    States(Weapon)
    {
        SelectAnim:
            OSHT A 1;
            TNT1 A 0 A_SelectSound;
            OSHT A 1 A_MoveWeaponSprite(-14, -16);
            OSHT A 1 A_MoveWeaponSprite(-12, -13);
            OSHT A 1 A_MoveWeaponSprite(-11, -15);
            OSHT A 1 A_MoveWeaponSprite(-11, -14);
            OSHT A 1 A_MoveWeaponSprite(-9, -8);
            OSHT A 1 A_MoveWeaponSprite(-13, -9);
            Goto Select;
        Ready:
            OSHT A 1 A_ReloadableWeaponReady;
            Loop;
        DeselectAnim:
            OSHT A 1 A_MoveWeaponSprite(13, 9);
            OSHT A 1 A_MoveWeaponSprite(9, 8);
            OSHT A 1 A_MoveWeaponSprite(11, 14);
            OSHT A 1 A_MoveWeaponSprite(11, 15);
            OSHT A 1 A_MoveWeaponSprite(12, 13);
            OSHT A 1 A_MoveWeaponSprite(14, 16);
            Goto Deselect;
        DeselectReload:
            OSHT K 1 A_Lower(8);
            Loop;
        Fire:
            OSHT B 2 BRIGHT A_ShotgunAttack;
            OSHT B 2 BRIGHT A_MoveWeaponSprite(5, 5);
            OSHT A 2 A_MoveWeaponSprite(-5, -5);
            OSHT A 2 A_MoveWeaponSprite(-10, -10);
            OSHT CD 3;
            OSHT E 3 A_ShotgunPump1;
            OSHT F 5;
            OSHT E 3 A_ShotgunPump2(true);
            OSHT DC 3;
            OSHT A 6;
            TNT1 A 0 A_ReFire;
            Goto Ready;
        Flash:
            TNT1 A 2 A_Light1;
            TNT1 A 2 A_Light2;
            TNT1 A 0 A_Light0;
            Stop;
        DryFire:
            OSHT A 16 A_DryFire;
            TNT1 A 0 A_DryReFire;
            Goto Ready;
        Reload:
            OSHT C 3 A_ShotgunReloadStart;
            OSHT C 3 A_MoveWeaponSprite(-60, 20);
            OSHT A 3 A_MoveWeaponSprite(-11, 23);
            OSHT G 3 A_ResetWeaponSprite;
            // Fall through here
        ReloadRound:
            OSHT H 3 A_ShotgunReloadLoop;
            OSHT I 2 A_PlayWeaponAuxSound(SND_RELOAD);
            OSHT I 1 A_ShotgunReloadRound;
            OSHT JK 4;
            OSHT K 4 A_MaybeWeaponReady(false);
            Loop;
        ReloadDone:
            OSHT A 3 A_MoveWeaponSprite(11, -23, -71, 43);
            OSHT C 3 A_MoveWeaponSprite(60, -20);
            OSHT D 3;
            OSHT E 3 A_ShotgunPump1;
            OSHT F 5;
            OSHT E 3 A_ShotgunPump2(false);
            OSHT DC 3;
            OSHT A 6;
            // NB: both this and "Goto Reload" are needed for realistic reloading
            OSHT A 0 A_MaybeWeaponReady(true);
            Goto Reload;
        RealisticReloadDone:
            OSHT A 3 A_MoveWeaponSprite(11, -23, -71, 43);
            OSHT A 3 A_MoveWeaponSprite(60, -20);
            Goto Ready;
        FireReload:
            OSHT A 3 A_MoveWeaponSprite(11, -23, -71, 43);
            OSHT A 3 A_MoveWeaponSprite(60, -20);
            Goto Fire;
    }

    //===========================================================================
    //
    // RR_Shotgun::RealisticReloadEnabled
    //
    // Checks if realistic reload is currently enabled.
    //
    //===========================================================================
    static bool RealisticReloadEnabled()
    {
        return RR_Settings.InPlay(RR_SETTING_SHOTGUN_REALISTICRELOAD).IsOn();
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunAttack
    //
    // Fires the shotgun.
    //
    //===========================================================================
    action(Weapon) void A_ShotgunAttack()
    {
        A_MoveWeaponSprite(10, 10);
        A_GunFlash();
        A_FireHitscan(5, 7, 5.625);
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunPump1
    //
    // Plays the pumping sound.
    //
    //===========================================================================
    action(Weapon) void A_ShotgunPump1()
    {
        A_PlayWeaponAuxSound(SND_PUMP_1);
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunPump2
    //
    // Plays the second pumping sound.
    // Also ejects a casing if the provided argument's value is true.
    //
    //===========================================================================
    action(Weapon) void A_ShotgunPump2(bool ejectCasing)
    {
        A_PlayWeaponAuxSound(SND_PUMP_2);

        if (ejectCasing)
        {
            A_EjectCasing();
        }
    }

    //===========================================================================
    //
    // RR_Shotgun::A_MaybeWeaponReady
    //
    // Calls A_ReloadableWeaponReady if realistic reloading is disabled
    // or if it is enabled and the first round has already been loaded.
    // Otherwise, does nothing.
    //
    //===========================================================================
    action(Weapon) state A_MaybeWeaponReady(bool postPump)
    {
        return invoker.IsReadyInReload(postPump)
            ? A_ReloadableWeaponReady(WRF_NOBOB)
            : null;
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunReloadStart
    //
    // If reloading has already finished, jumps to the Ready state,
    // otherwise continues uninterrupted.
    //
    //===========================================================================
    action(Weapon) state A_ShotgunReloadStart()
    {
        return invoker.CheckReloadStart();
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunReloadLoop
    //
    // If realistic reloading is enabled, selects the next state accordingly,
    // otherwise calls A_CheckReloadDone.
    //
    //===========================================================================
    action(Weapon) state A_ShotgunReloadLoop()
    {
        state result = null;

        if (RealisticReloadEnabled())
        {
            result = invoker.CheckRealisticReloadSequence();
        }

        return result != null ? result : A_CheckReloadDone();
    }

    //===========================================================================
    //
    // RR_Shotgun::A_ShotgunReloadRound
    //
    // Reloads a single round and updates the internal state accordingly.
    //
    //===========================================================================
    action(Weapon) void A_ShotgunReloadRound()
    {
        A_ReloadOneRound();
        invoker.CheckResetPump();
    }

    //===========================================================================
    //
    // RR_Shotgun::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(SND_PUMP_1);
        MarkSound(SND_PUMP_2);
        MarkSound(SND_RELOAD);
    }

    //===========================================================================
    //
    // RR_Shotgun::PreReload
    //
    // Resets the internal state of the shotgun reloading sequence.
    //
    //===========================================================================
    override void PreReload()
    {
        _curReloadStage = SRS_Started;
    }

    //===========================================================================
    //
    // RR_Shotgun::GetFireState
    //
    // If realistic reloading is enabled and the first round has just been
    // loaded, returns the normal fire state to avoid sprite mismatch.
    // Otherwise, calls the parent definition without changing arguments.
    //
    //===========================================================================
    override state GetFireState(bool altFire, EFireType type)
    {
        if (!altFire && type == FT_FromReload && _curReloadStage == SRS_InPump)
        {
            // Just loaded the first round and pumped,
            // so display normal firing animation.
            type = FT_Normal;
        }

        return Super.GetFireState(altFire, type);
    }

    //===========================================================================
    //
    // RR_Shotgun::GetReloadDoneState
    //
    // Returns the correct state to conclude the reloading sequence
    // to match the currently displayed weapon sprite.
    //
    //===========================================================================
    override state GetReloadDoneState()
    {
        return RealisticReloadEnabled()
            ? ResolveState('RealisticReloadDone')
            : Super.GetReloadDoneState();
    }

    //===========================================================================
    //
    // RR_Shotgun::GetDeselectAnimStateReloadable
    //
    // Returns the correct deselect animation state used during reloading
    // depending on the current realistic reload sequence state.
    //
    //===========================================================================
    override state GetDeselectAnimStateReloadable(bool fromReload)
    {
        if (fromReload && _curReloadStage == SRS_InPump)
        {
            // The pumping sequence is playing or has just finished,
            // so display normal deselect animation.
            fromReload = false;
        }

        return Super.GetDeselectAnimStateReloadable(fromReload);
    }

    //===========================================================================
    //
    // RR_Shotgun::CheckReloadStart
    //
    // Implements the logic for A_ReloadStart.
    //
    //===========================================================================
    private state CheckReloadStart()
    {
        bool done = false;

        if (_curReloadStage == SRS_Started)
        {
            // Begin reloading
            _curReloadStage = SRS_InProgress;
        }
        else if (RealisticReloadEnabled())
        {
            done = CheckReloadDone();
        }
        else if (_curReloadStage == SRS_InProgress)
        {
            // Realistic reloading is disabled, so we must break the loop here
            // by going back to the ready state.
            done = true;
        }

        return done ? GetReadyState() : null;
    }

    //===========================================================================
    //
    // RR_Shotgun::CheckRealisticReloadSequence
    //
    // This is the main state selector for the realistic reloading sequence.
    //
    //===========================================================================
    private state CheckRealisticReloadSequence()
    {
        // Check if we need to play the pumping animation now.
        if (_curReloadStage == SRS_PrePump)
        {
            _curReloadStage = SRS_InPump;
            return Super.GetReloadDoneState();
        }

        // If we do not have any ammo and realistic reloading is enabled,
        // we should play the pumping animation next.
        if (GetClipAmount() == 0)
        {
            _curReloadStage = SRS_PrePump;
        }

        // Continue to A_CheckReloadDone
        return null;
    }

    //===========================================================================
    //
    // RR_Shotgun::CheckResetPump
    //
    // Signifies that the pumping sequence is no longer playing.
    //
    //===========================================================================
    private void CheckResetPump()
    {
        if (_curReloadStage == SRS_InPump)
        {
            _curReloadStage = SRS_InProgress;
        }
    }

    //===========================================================================
    //
    // RR_Shotgun::IsReadyInReload
    //
    // Checks if the shotgun is ready for firing during the reloading sequence.
    //
    //===========================================================================
    private bool IsReadyInReload(bool postPump)
    {
        // In realistic mode, we can fire the weapon immediately after pumping,
        // or if we don't have to play the pumping animation next.
        //
        // In non-realistic mode, we can always fire after each reload,
        // but not after pumping.
        bool isReady = RealisticReloadEnabled()
            ? postPump || _curReloadStage != SRS_PrePump
            : !postPump;

        return isReady;
    }
}
