//===========================================================================
//
// RR_ReloadableAmmo
//
// Base class for ammo types used by reloadable weapons.
//
//===========================================================================
class RR_ReloadableAmmo : RR_Ammo abstract
{
    // The ammo pickup type this ammo item will transform into
    // if partial pickup is enabled and the amount left in this item
    // is less than or equal to the default amount of the new type,
    // multiplied by the skill factor.
    private meta class<RR_ReloadableAmmo> ResidualPickupType;

    property ResidualPickupType: ResidualPickupType;

    // Enable the SpecialHandlePickup method to prevent auto-switching
    // to a non-empty weapon.
    mixin RR_HandlePickupHook;

    // Enable partial ammo pickup (additional setup in the code below).
    mixin RR_PartialPickupHook;
    mixin RR_PartialPickup;

    Default
    {
        RR_Ammo.BaseAmmoClass 'RR_ReloadableAmmo';
    }

    //===========================================================================
    //
    // RR_ReloadableAmmo::HandlePickup
    //
    // Peforms an additional check related to automatic weapon switching.
    // See the corresponding mixin (RR_HandlePickupHook) for details.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        return SpecialHandlePickup(item);
    }

    //===========================================================================
    //
    // RR_ReloadableAmmo::PartialPickup_GetAmountOwned
    //
    // Used in partial pickup handling.
    // Returns the amount of parent ammo owned by the specified actor.
    //
    //===========================================================================
    private int PartialPickup_GetAmountOwned(Actor toucher) const
    {
        return GetAmountOfItem(toucher, GetParentAmmo());
    }

    //===========================================================================
    //
    // RR_ReloadableAmmo::PartialPickup_GetAmountAvailable
    //
    // Used in partial pickup handling.
    // Returns the amount of ammo initially available in this item.
    //
    //===========================================================================
    private int PartialPickup_GetAmountAvailable() const
    {
        int result = Amount;

        if (!bIgnoreSkill)
        {
            result = AdjustAmmoBySkill(result);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_ReloadableAmmo::PartialPickup_GetResidualItemType
    //
    // Used in partial pickup handling. Returns the pre-set residual pickup type
    // if the amount of ammo left is less than or equal to the default amount
    // of that type multiplied by the skill factor.
    // Otherwise returns the current ammo class.
    //
    // This logic allows "big" ammo items to transform into "small" ones
    // when the amount of remaining ammo is less than a certain value.
    //
    //===========================================================================
    protected class<Inventory> PartialPickup_GetResidualItemType(int remainingAmount) const
    {
        let newType = ResidualPickupType;

        if (newType != null)
        {
            let def = GetDefaultByType(newType);

            // Ensure that we aren't transforming to a different ammo type
            if (def.GetParentAmmo() != GetParentAmmo())
            {
                ThrowAbortException("%s: Residual pickup type must have the same parent ammo type.", GetClassName());
            }

            // This is the highest possible amount at which we "transform"
            // into the residual type.
            if (remainingAmount <= AdjustAmmoBySkill(def.Amount))
            {
                return newType;
            }
        }

        // Otherwise, the type of pickup does not change.
        return GetClass();
    }

    //===========================================================================
    //
    // RR_ReloadableAmmo::PartialPickup_GetResidualItemAmount
    //
    // Used in partial pickup handling. Returns the maximum amount of ammo
    // per each residual pickup item.
    //
    //===========================================================================
    private int PartialPickup_GetResidualItemAmount(int remainingAmount) const
    {
        // Ammo pickups will not spawn extra residual items,
        // so always return the provided value.
        return remainingAmount;
    }
}
