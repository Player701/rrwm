//===========================================================================
//
// RR_ChainsawAmmo
//
// Base class for chainsaw ammo types.
// NB! These ammo types are only used in the HUD.
// The actual ammo counts are stored in the chainsaw itself.
//
//===========================================================================
class RR_ChainsawAmmo : RR_Ammo abstract
{
    Default
    {
        +INVENTORY.IGNORESKILL;
        RR_Ammo.BaseAmmoClass 'RR_ChainsawAmmo';
    }

    //===========================================================================
    //
    // RR_ChainsawAmmo::IsGameRelevant
    //
    // This is not really an ammo type, so do not show it in the table
    // unless the player has the corresponding weapon.
    //
    //===========================================================================
    override bool IsGameRelevant() const
    {
        return false;
    }

    //===========================================================================
    //
    // RR_ChainsawAmmo::DepleteOrDestroy
    //
    // Since this not really an ammo type, it also cannot be destroyed
    // or taken away by any means.
    //
    //===========================================================================
    override void DepleteOrDestroy()
    {
        // The only thing we'll do is keep our amount non-negative
        // since that is what could happen if more ammo is attempted
        // to be taken than we currently have.
        Amount = Max(Amount, 0);
    }

    //===========================================================================
    //
    // RR_ChainsawAmmo::Tick
    //
    // This ammo does not exist without an owner, or if they don't have
    // the chainsaw (e.g. if given by cheat).
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (Owner == null || Owner.FindInventory('RR_Chainsaw') == null)
        {
            Destroy();
        }
    }
}
