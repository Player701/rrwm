//===========================================================================
//
// RR_ShellBox
//
// Ammo for the shotgun - box of shells
//
//===========================================================================
class RR_ShellBox : RR_Shell replaces ShellBox
{
    Default
    {
        Inventory.Amount 20;
        Inventory.PickupMessage "$PICKUP_RR_SHELLBOX";

        RR_ReloadableAmmo.ResidualPickupType 'RR_Shell';
    }

    States
    {
        Spawn:
            SHA2 A -1;
            Stop;
    }
}
