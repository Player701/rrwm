//===========================================================================
//
// RR_ChainsawAmmo2
//
// Secondary ammo for the chainsaw.
// NB! This is only used to reflect the chainsaw upgrade level in the HUD.
// The actual upgrade level is stored in the chainsaw itself.
//
//===========================================================================
class RR_ChainsawAmmo2 : RR_ChainsawAmmo
{
    Default
    {
        Inventory.Icon "RR_CSWUH";
        Inventory.AltHUDIcon "RR_CSWUA";

        RR_Ammo.HudColor 'Red';
    }

    //===========================================================================
    //
    // RR_ChainsawAmmo2::IsInAmmoTable
    //
    // This is not really an ammo type, so do not add it to the table at all.
    // (The player can still see the primary ammo type in the table.)
    //
    //===========================================================================
    override bool IsInAmmoTable() const
    {
        return false;
    }
}
