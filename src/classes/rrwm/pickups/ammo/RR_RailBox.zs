//===========================================================================
//
// RR_RailBox
//
// Ammo for the Railgun - standard battery
//
//===========================================================================
class RR_RailBox : RR_RailAmmo
{
    Default
    {
        Scale 0.75;

        Inventory.PickupMessage "$PICKUP_RR_RAILBOX";
        Inventory.Amount 5;
    }

    States
    {
        Spawn:
            RCH1 A -1 BRIGHT Light("RR_RAILBOX");
            Stop;
    }
}
