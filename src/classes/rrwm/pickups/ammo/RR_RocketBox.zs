//===========================================================================
//
// RR_RocketBox
//
// Ammo for the Striker launcher - clip of rockets
//
//===========================================================================
class RR_RocketBox : RR_RocketAmmo replaces RocketBox
{
    Default
    {
        Scale 1.1;
        +FORCEXYBILLBOARD;

        Inventory.Amount 5;
        Inventory.PickupMessage "$PICKUP_RR_ROCKETBOX";

        RR_ReloadableAmmo.ResidualPickupType 'RR_RocketAmmo';
    }

    States
    {
        Spawn:
            SPAK A -1;
            Stop;
    }
}
