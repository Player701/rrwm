//===========================================================================
//
// RR_RailAmmo
//
// Ammo for the Railgun - single charge
//
//===========================================================================
class RR_RailAmmo : RR_ReloadableAmmo
{
    Default
    {
        Tag "$TAG_RR_AMMO_RAIL";

        Inventory.Amount 1;
        Inventory.MaxAmount 50;
        Inventory.Icon "RCH1A0";
        Inventory.PickupSound "rrwm/pickups/ammo/railgun";
        Inventory.PickupMessage "$PICKUP_RR_RAILAMMO";

        Ammo.BackpackAmount 1;
        Ammo.BackpackMaxAmount 100;

        RR_Ammo.HudColor 'DarkGreen';
        RR_Ammo.VanillaAmmoClass 'RocketAmmo';
    }

    States
    {
        Spawn:
            SRCH A -1 BRIGHT Light("RR_RAILAMMO");
            Stop;
    }

    //===========================================================================
    //
    // RR_RailAmmo::IsGameRelevant
    //
    // The rail ammo is relevant if the Railgun is enabled.
    //
    //===========================================================================
    override bool IsGameRelevant() const
    {
        return RR_Settings.InPlay(RR_SETTING_RAILGUN).IsOn();
    }
}
