//===========================================================================
//
// RR_CellPack
//
// Ammo for the Hyperblaster - cell charge (large)
//
//===========================================================================
class RR_CellPack : RR_Cell replaces CellPack
{
    Default
    {
        Scale 0.75;

        Inventory.Amount 100;
        Inventory.PickupMessage "$PICKUP_RR_CELLPACK";
    }

    States
    {
        Spawn:
            CHRG A -1 BRIGHT Light("RR_CELLPACK");
            Stop;
    }
}
