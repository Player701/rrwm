//===========================================================================
//
// RR_Ammo
//
// Base class for RRWM ammo pickups.
//
//===========================================================================
class RR_Ammo : Ammo abstract
{
    // Defines the HUD color for this ammo. It should either refer
    // to a built-in color, or to an entry defined in textcolors.txt.
    // This is used by RRWM's HUD.
    meta name HudColor;

    // Defines the vanilla ammo class that is the counterpart to this
    // RRWM ammo class. This is used by RR_PostprocessorHook
    // to adjust ammo capacities.
    meta class<Ammo> VanillaAmmoClass;

    // Defines the base ammo class that should be used when determining
    // parent ammo in GetParentAmmo. All direct descendants of this class
    // will be considered distinct ammo types.
    private meta class<Ammo> BaseAmmoClass;

    property BaseAmmoClass: BaseAmmoClass;
    property HudColor: HudColor;
    property VanillaAmmoClass: VanillaAmmoClass;

    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        self.BaseAmmoClass 'RR_Ammo';
    }

    //===========================================================================
    //
    // RR_Ammo::GetParentAmmo
    //
    // Determines the parent ammo class for this ammo.
    //
    //===========================================================================
    override class<Ammo> GetParentAmmo()
    {
        class<Object> cls = GetClass();

        while (cls != null && cls.GetParentClass() != BaseAmmoClass)
        {
            cls = cls.GetParentClass();
        }

        return (class<Ammo>)(cls);
    }

    //===========================================================================
    //
    // RR_Ammo::IsGameRelevant
    //
    // Checks whether this ammo type is relevant for the current game.
    //
    // This is used by the HUD ammo table widget as well as backpack code
    // to determine whether this type of ammo should be processed.
    //
    //===========================================================================
    virtual bool IsGameRelevant() const
    {
        return true;
    }

    //===========================================================================
    //
    // RR_Ammo::IsInAmmoTable
    //
    // Checks whether this ammo type is included in the ammo table.
    //
    // If this method returns false, this ammo type is always skipped,
    // even if the player has some ammo of this type.
    //
    //===========================================================================
    virtual bool IsInAmmoTable() const
    {
        return true;
    }
}
