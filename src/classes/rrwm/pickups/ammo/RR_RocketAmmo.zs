//===========================================================================
//
// RR_RocketAmmo
//
// Ammo for the Striker launcher - single rocket
//
//===========================================================================
class RR_RocketAmmo : RR_ReloadableAmmo replaces RocketAmmo
{
    Default
    {
        Scale 0.75;
        Tag "$TAG_RR_AMMO_ROCKETS";

        Inventory.Amount 1;
        Inventory.MaxAmount 50;
        Inventory.PickupMessage "$PICKUP_RR_ROCKETAMMO";
        Inventory.PickupSound "rrwm/pickups/ammo/rocketlauncher";
        Inventory.Icon "SPAKA0";

        Ammo.BackpackAmount 1;
        Ammo.BackpackMaxAmount 100;

        RR_Ammo.HudColor 'RR_RocketAmmo';
        RR_Ammo.VanillaAmmoClass 'RocketAmmo';
    }

    States
    {
        Spawn:
            SAMO A 50;
            SAMO B 15 BRIGHT Light("RR_ROCKETAMMO");
            Loop;
    }

    //===========================================================================
    //
    // RR_RocketAmmo::IsGameRelevant
    //
    // The rocket ammo is relevant if the Railgun is disabled.
    //
    //===========================================================================
    override bool IsGameRelevant() const
    {
        return !RR_Settings.InPlay(RR_SETTING_RAILGUN).IsOn();
    }
}
