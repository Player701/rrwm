//===========================================================================
//
// RR_Shell
//
// Ammo for the shotgun - 4 shells
//
//===========================================================================
class RR_Shell : RR_ReloadableAmmo replaces Shell
{
    Default
    {
        Scale 0.75;
        Tag "$AMMO_SHELLS";

        Inventory.Amount 4;
        Inventory.MaxAmount 50;
        Inventory.Icon "SHA1A0";
        Inventory.PickupSound "rrwm/pickups/ammo/common";
        Inventory.PickupMessage "$PICKUP_RR_SHELL";

        Ammo.BackpackAmount 4;
        Ammo.BackpackMaxAmount 100;

        RR_Ammo.HudColor 'Red';
        RR_Ammo.VanillaAmmoClass 'Shell';
    }

    States
    {
        Spawn:
            SHA1 A -1;
            Stop;
    }
}
