//===========================================================================
//
// RR_ChainsawAmmo1
//
// Primary ammo for the chainsaw.
// NB! This is only used to reflect the chainsaw ammo level in the HUD.
// The actual ammo count is stored in the chainsaw itself.
//
//===========================================================================
class RR_ChainsawAmmo1 : RR_ChainsawAmmo
{
    Default
    {
        // This is mostly for AltHUD
        Inventory.MaxAmount RR_Chainsaw.MAX_AMMO_COUNT;
        Inventory.Icon "RR_CSWA";

        RR_Ammo.HudColor 'Grey';
    }
}
