//===========================================================================
//
// RR_ClipBox
//
// Ammo for the pistol - box of clips
//
//===========================================================================
class RR_ClipBox : RR_Clip replaces ClipBox
{
    Default
    {
        Scale 0.65;

        Inventory.Amount 50;
        Inventory.PickupMessage "$PICKUP_RR_CLIPBOX";

        RR_ReloadableAmmo.ResidualPickupType 'RR_Clip';
    }

    States
    {
        Spawn:
            BBOX A -1;
            Stop;
    }
}
