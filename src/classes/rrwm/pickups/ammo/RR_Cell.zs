//===========================================================================
//
// RR_Cell
//
// Ammo for the Hyperblaster - cell charge (small)
//
//===========================================================================
class RR_Cell : RR_ReloadableAmmo replaces Cell
{
    Default
    {
        Scale 0.45;
        Tag "$TAG_RR_AMMO_CELLS";

        Inventory.Amount 20;
        Inventory.MaxAmount 300;
        Inventory.Icon "CHRGA0";
        Inventory.PickupSound "rrwm/pickups/ammo/plasmarifle";
        Inventory.PickupMessage "$PICKUP_RR_CELL";

        Ammo.BackpackAmount 20;
        Ammo.BackpackMaxAmount 600;

        RR_Ammo.HudColor 'Green';
        RR_Ammo.VanillaAmmoClass 'Cell';
    }

    States
    {
        Spawn:
            CHRG A -1 BRIGHT Light("RR_CELL");
            Stop;
    }
}
