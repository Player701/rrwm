//===========================================================================
//
// RR_Clip
//
// Ammo for the pistol - clip
//
//===========================================================================
class RR_Clip : RR_ReloadableAmmo replaces Clip
{
    Default
    {
        Scale 0.65;
        Tag "$AMMO_CLIP";

        Inventory.Amount 10;
        Inventory.MaxAmount 200;
        Inventory.Icon "BCLPA0";
        Inventory.PickupSound "rrwm/pickups/ammo/common";
        Inventory.PickupMessage "$PICKUP_RR_CLIP";

        Ammo.BackpackAmount 10;
        Ammo.BackpackMaxAmount 400;

        RR_Ammo.HudColor 'Red';
        RR_Ammo.VanillaAmmoClass 'Clip';
    }

    States
    {
        Spawn:
            BCLP A -1;
            Stop;
    }
}
