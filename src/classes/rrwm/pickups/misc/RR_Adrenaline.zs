//===========================================================================
//
// RR_Adrenaline
//
// The Adrenaline - essentially an improved Berserk.
//
//===========================================================================
class RR_Adrenaline : PowerupGiver replaces Berserk
{
    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        +COUNTITEM;
        Scale 0.65;

        // MaxAmount == 0 is needed to circumvent the auto-activation bug.
        // (as of GZDoom 3.6.0)
        Inventory.MaxAmount 0;
        Inventory.PickupSound "rrwm/pickups/adrenaline";
        Inventory.PickupMessage "$PICKUP_RR_ADRENALINE";
        +INVENTORY.AUTOACTIVATE;
        +INVENTORY.ISHEALTH;

        Powerup.Type "RR_PowerAdrenaline";
    }

    States
    {
        Spawn:
            ADRN A 35;
            ADRN B 35 BRIGHT Light("RR_ADRENALINE");
            Loop;
    }

    //===========================================================================
    //
    // RR_Adrenaline::Use
    //
    // Processes additional effects for the affected player.
    //
    //===========================================================================
    override bool Use(bool pickup)
    {
        bool result = Super.Use(pickup);

        if (result)
        {
            // Make sure we have a player as an owner.
            let player = Owner.player;

            if (player != null)
            {
                // Switch to the Fist weapon if the player has it.
                let fist = RR_Fist(Owner.FindInventory('RR_Fist'));

                if (fist != null && player.ReadyWeapon != fist)
                {
                    player.PendingWeapon = fist;
                }
            }

            // Increase max health.
            Owner.Stamina = Min(Owner.Stamina + 1, 100);
            // Heal the owner to full health.
            Owner.GiveBody(-100);
        }

        return result;
    }
}
