//===========================================================================
//
// RR_Backpack
//
// Implements partial ammo pickup for the backpack.
//
//===========================================================================
class RR_Backpack : Backpack replaces Backpack
{
    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    // Enable partial ammo pickup (additional setup in the code below).
    mixin RR_PartialPickup;

    Default
    {
        Inventory.PickupSound "rrwm/pickups/ammo/common";
    }

    //===========================================================================
    //
    // RR_Backpack::TryPickup
    //
    // Generates residual ammo items if partial ammo pickup is enabled.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        if (bDepleted)
        {
            // Prevent generating residual pickups if the backpack was dropped
            return Super.TryPickup(toucher);
        }

        Array<class<RR_ReloadableAmmo> > savedAmmos;
        Array<int> savedAmounts;

        // Save existing ammo amounts first.
        foreach (cls : AllActorClasses)
        {
            let ammoType = (class<RR_ReloadableAmmo>)(cls);

            if (ammoType != null)
            {
                let a = GetDefaultByType(ammoType);

                if (a.GetParentAmmo() == ammoType && a.IsGameRelevant())
                {
                    savedAmmos.Push(ammoType);
                    savedAmounts.Push(GetAmountOfItem(toucher, ammoType));
                }
            }
        }

        // The backpack is picked up...
        bool result = Super.TryPickup(toucher);

        if (result && IsPartialPickupEnabled())
        {
            Array<int> remainingAmounts;
            int cnt = 0;

            // Check how much ammo is left
            for (int i = 0; i < savedAmmos.Size(); i++)
            {
                let curAmmoType = savedAmmos[i];

                int newAmount = GetAmountOfItem(toucher, curAmmoType);
                int added = Max(0, newAmount - savedAmounts[i]);
                int remaining = GetDefaultByType(curAmmoType).BackpackAmount;

                // Account for skill settings if necessary
                if (!bIgnoreSkill)
                {
                    remaining = AdjustAmmoBySkill(remaining);
                }

                remaining -= added;
                remainingAmounts.Push(remaining);

                // If there is any ammo left, we will spawn a residual item
                if (remaining > 0)
                {
                    cnt++;
                }
            }

            // Now spawn residual items for all ammo types with remaining amounts
            for (int i = 0; i < savedAmmos.Size(); i++)
            {
                int remaining = remainingAmounts[i];

                if (remaining > 0)
                {
                    let newItem = SpawnResidualItem(savedAmmos[i], remaining, Pos, cnt > 1);
                }
            }
        }

        return result;
    }
}
