//===========================================================================
//
// RR_AllMap
//
// Provides a new pickup sound and scale for the Computer Map.
//
//===========================================================================
class RR_AllMap : AllMap replaces AllMap
{
    const SND_ALLMAP_EXTRA = "rrwm/effects/allmap";

    Default
    {
        Scale 0.75;
        Inventory.PickupSound "rrwm/pickups/generic";
    }

    //===========================================================================
    //
    // RR_AllMap::TryPickup
    //
    // Plays the special sound upon receiving the map.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        bool result = Super.TryPickup(toucher);

        if (result)
        {
            toucher.A_StartSound(SND_ALLMAP_EXTRA, RR_SoundChannels.CHAN_ALLMAP_AUX, CHAN_MAYBE_LOCAL);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_AllMap::MarkPrecacheSounds
    //
    // Marks additional sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(SND_ALLMAP_EXTRA);
    }
}
