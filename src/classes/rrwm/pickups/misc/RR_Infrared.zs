//===========================================================================
//
// RR_Infrared
//
// Provides a new pickup sound and scale for the Infrared Goggles.
//
//===========================================================================
class RR_Infrared : Infrared replaces Infrared
{
    const SND_INFRARED_EXTRA = "rrwm/effects/infrared";

    Default
    {
        Scale 0.75;
        Inventory.PickupSound "rrwm/pickups/generic";
    }

    //===========================================================================
    //
    // RR_Infrared::Use
    //
    // Plays the special sound upon activating the Infrared.
    //
    //===========================================================================
    override bool Use(bool pickup)
    {
        bool result = Super.Use(pickup);

        if (result)
        {
            Owner.A_StartSound(SND_INFRARED_EXTRA, RR_SoundChannels.CHAN_INFRARED_AUX, CHAN_MAYBE_LOCAL);
        }

        return result;
    }

    //===========================================================================
    //
    // RR_Infrared::MarkPrecacheSounds
    //
    // Marks additional sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(SND_INFRARED_EXTRA);
    }
}
