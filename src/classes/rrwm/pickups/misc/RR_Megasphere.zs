//===========================================================================
//
// RR_Megasphere
//
// This class exists because, unfortunately, it doesn't seem to be possible
// to replace the original Megasphere's armor type directly.
//
//===========================================================================
class RR_Megasphere : Inventory replaces Megasphere
{
    Default
    {
        +COUNTITEM;

        Inventory.PickupSound "misc/p_pkup";
        Inventory.PickupMessage "$GOTMSPHERE";
        +INVENTORY.ISARMOR;
        +INVENTORY.ISHEALTH;
    }

    States
    {
        Spawn:
            MEGA ABCD 6 BRIGHT Light("MEGASPHERE");
            Loop;
    }

    //===========================================================================
    //
    // RR_Megasphere::TryPickup
    //
    // Handles the Megasphere pickup.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        toucher.GiveBody(200, 200);
        toucher.GiveInventoryType('RR_HeavyArmor');
        GoAwayAndDie();

        return true;
    }
}
