//===========================================================================
//
// RR_HeavyArmor
//
// The heavy armor.
// Provides 200 units of armor and 50% protection.
// Replaces the blue armor.
//
//===========================================================================
class RR_HeavyArmor : RR_Armor replaces BlueArmor
{
    Default
    {
        Inventory.Icon "HARMA0";
        Inventory.PickupMessage "$PICKUP_RR_ARMOR_HEAVY";

        Armor.SavePercent 50;
        Armor.SaveAmount 200;
    }

    States
    {
        Spawn:
            HARM A -1;
            Stop;
    }
}
