//===========================================================================
//
// RR_ArmorBonus
//
// The normal armor bonus but with a new pickup sound.
//
//===========================================================================
class RR_ArmorBonus : ArmorBonus replaces ArmorBonus
{
    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        Inventory.PickupSound "rrwm/pickups/armorbonus";
    }

    States
    {
        Spawn:
            BON2 ABCDCB 6 Light("ARMORBONUS");
            Loop;
    }

    //===========================================================================
    //
    // RR_ArmorBonus::TryPickup
    //
    // If the player is maxed out on armor points in the primary slot,
    // replenishes the Super Armor in a separate slot, if present.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        let ba = BasicArmor(toucher.FindInventory('BasicArmor'));

        if (ba != null && ba.Amount >= MaxSaveAmount)
        {
            // Maxed out on normal armor, check the super armor
            let shield = toucher.FindInventory('RR_SuperArmorShieldPickup');

            if (shield != null)
            {
                // OK, replenish the super armor (but don't go over the maximum)
                shield.Amount = Min(shield.Amount + 1, shield.MaxAmount);

                GoAwayAndDie();
                return true;
            }
        }

        // Normal pickup logic
        return Super.TryPickup(toucher);
    }
}
