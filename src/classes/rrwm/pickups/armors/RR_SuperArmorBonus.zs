//===========================================================================
//
// RR_SuperArmorBonus
//
// This armor bonus has the same max amount as that of the Super Armor.
// It replaces normal armor bonuses in a map if extra armor is enabled,
// so that bonuses aren't wasted when picking them up with over 200 armor.
//
//===========================================================================
class RR_SuperArmorBonus : RR_ArmorBonus
{
    Default
    {
        Armor.MaxSaveAmount RR_SuperArmor.DEFSAVEAMOUNT;
    }
}
