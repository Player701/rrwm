//===========================================================================
//
// RR_SuperArmor
//
// The Super (or Shield) armor which provided 100% protection.
// Now with extra cool special effects!
// Can spawn instead of the normal blue armor if you're lucky
// and extra armors have been enabled.
//
//===========================================================================
class RR_SuperArmor : RR_Armor
{
    // This amount is used for armor bonuses as well
    const DEFSAVEAMOUNT = 300;

    Default
    {
        Inventory.Icon "AARMA0";
        Inventory.PickupMessage "$PICKUP_RR_ARMOR_SUPER";

        Armor.SavePercent 100;
        Armor.SaveAmount DEFSAVEAMOUNT;
    }

    States
    {
        Spawn:
            AARM A -1 BRIGHT Light("RR_ARMOR_SUPER");
            Stop;
    }

    //===========================================================================
    //
    // RR_SuperArmor::Use
    //
    // Initialize the shield effect.
    //
    //===========================================================================
    override bool Use(bool pickup)
    {
        bool result = Super.Use(pickup);

        if (result)
        {
            // Give the shield.
            Owner.GiveInventoryType('RR_SuperArmorShield');
        }

        return result;
    }

    //===========================================================================
    //
    // RR_SuperArmor::MarkPrecacheSounds
    //
    // Marks additional sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(RR_SuperArmorShieldPickup.SND_SHIELD);
    }
}
