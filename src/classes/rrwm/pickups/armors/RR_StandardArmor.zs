//===========================================================================
//
// RR_StandardArmor
//
// The standard armor.
// Provides 100 units of armor and 33% protection.
// Replaces the green armor.
//
//===========================================================================
class RR_StandardArmor : RR_Armor replaces GreenArmor
{
    Default
    {
        Inventory.Icon "LRM2A0";
        Inventory.PickupMessage "$PICKUP_RR_ARMOR_STANDARD";

        Armor.SavePercent 33.335;
        Armor.SaveAmount 100;
    }

    States
    {
        Spawn:
            LRM2 A -1;
            Stop;
    }
}
