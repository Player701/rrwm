//===========================================================================
//
// RR_LightArmor
//
// The light armor.
// Provides 50 units of armor and 25% protection.
// Can replace the green armor if you're unlucky
// and extra armors have been enabled.
//
//===========================================================================
class RR_LightArmor : RR_Armor
{
    Default
    {
        Inventory.Icon "LARMA0";
        Inventory.PickupMessage "$PICKUP_RR_ARMOR_LIGHT";

        Armor.SavePercent 25;
        Armor.SaveAmount 50;
    }

    States
    {
        Spawn:
            LARM A -1;
            Stop;
    }
}
