//===========================================================================
//
// RR_Armor
//
// Base class for RRWM armor pickups.
//
//===========================================================================
class RR_Armor : BasicArmorPickup abstract
{
    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        Height 16;
        Radius 20;

        Inventory.PickupSound "rrwm/pickups/armor";
    }
}
