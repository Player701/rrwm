//===========================================================================
//
// RR_SuperArmorShieldPickup
//
// Same as the ordinary super armor, but does not use up the armor slot.
// This special armor pickup is available when
// the "Separate slot for super armor" setting is turned on.
//
//===========================================================================
class RR_SuperArmorShieldPickup : Inventory
{
    const SND_SHIELD = "rrwm/effects/shield";

    // The minimum number of ticks the shield effect will stay on the screen.
    // This is needed so that there's enough time for it to be seen.
    const SHIELD_FX_DURATION_MIN = 20;
    // The maximum number of ticks the shield effect will stay on the screen.
    // This is needed so that it doesn't stay for very long if a lot of damage is received.
    const SHIELD_FX_DURATION_MAX = 50;

    // How many ticks left until the effect ends?
    private int _shieldDuration;

    // Enable pickup sounds to overlap.
    mixin RR_PickupSoundHook;

    Default
    {
        Height 16;
        Radius 20;

        Inventory.MaxAmount RR_SuperArmor.DEFSAVEAMOUNT;
        Inventory.Icon "AARMA0";
        Inventory.PickupMessage "$PICKUP_RR_ARMOR_SUPER";
        Inventory.PickupSound "rrwm/pickups/armor";

        +INVENTORY.ISARMOR;
        // Prevent removal of the item with 0 amount
        // because the shield effect might still be active.
        +INVENTORY.KEEPDEPLETED;
        // This item cannot be dropped manually,
        // just like normal armor.
        +INVENTORY.UNTOSSABLE;
    }

    States
    {
        Spawn:
            AARM A -1 BRIGHT Light("RR_ARMOR_SUPER");
            Stop;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::BeginPlay
    //
    // Sets the amount to be the same as max amount for convenience purposes.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        Amount = MaxAmount;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::MarkPrecacheSounds
    //
    // Marks additional sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();
        MarkSound(SND_SHIELD);
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::AttachToOwner
    //
    // Repositions this item in the inventory chain to be immediately before
    // an instance of BasicArmor. This is just in case so that any other
    // damage-absorbing powerups do their effect first.
    //
    //===========================================================================
    override void AttachToOwner(Actor other)
    {
        Super.AttachToOwner(other);

        // Find the new previous and next items in one loop
        Inventory newPrev = null;
        let ii = other.Inv;

        while (ii != null && !RepositionBefore(ii))
        {
            newPrev = ii;
            ii = ii.Inv;
        }

        let newNext = ii;

        if (newNext == null || Inv == newNext)
        {
            // We're already there
            return;
        }

        // Rearrange the link between the previous item and us
        if (Owner.Inv == self)
        {
            // Nothing before us, so set the owner's first item
            // to the next item in the chain.
            Owner.Inv = Inv;
        }
        else
        {
            // Find the item that directly precedes us
            let prev = Owner.Inv;

            while (prev.Inv != self)
            {
                prev = prev.Inv;
            }

            prev.Inv = Inv;
        }

        // Set up the remaining links
        if (newPrev == null)
        {
            // We should be the first item
            Owner.Inv = self;
        }
        else
        {
            // This is our new previous item
            newPrev.Inv = self;
        }

        Inv = newNext;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::RepositionBefore
    //
    // Checks if the provided item must be directly preceded by this item.
    //
    //===========================================================================
    private bool RepositionBefore(Inventory item)
    {
        // We also need to check the case where the player has both super armors,
        // presumably by cheating. Is one not enough??
        let cls = item.GetClass();
        return cls == 'RR_SuperArmorShield' && cls != GetClass() || cls == 'BasicArmor';
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::AbsorbDamage
    //
    // Absorbs as much incoming damage as possible and starts the shield effect.
    //
    //===========================================================================
    override void AbsorbDamage(int damage, Name damageType, out int newdamage)
    {
        // Only run this code if the damage is non-zero, not armor-piercing,
        // the armor is active and still has value.
        if (Owner == null || damage <= 0 || DamageTypeDefinition.IgnoreArmor(damageType) || !IsActive())
        {
            return;
        }

        int absorbing = Min(damage, GetAmount());
        newdamage -= ShieldAbsorb(absorbing);

        // Set the new duration based on the amount of damage dealt.
        let newShieldDuration = Clamp(
            absorbing,
            SHIELD_FX_DURATION_MIN,
            SHIELD_FX_DURATION_MAX
        );

        // Do not decrease it if it's higher than calculated.
        _shieldDuration = Max(_shieldDuration, newShieldDuration);

        // Play the shield sound effect.
        Owner.A_StartSound(SND_SHIELD, RR_SoundChannels.CHAN_ARMOR_SHIELD);

        // Spawn an afterimage to indicate that the player has the super armor.
        let afterimage = Spawn('RR_ShieldAfterimage', Owner.Pos);

        if (afterimage != null)
        {
            // Mostly copied from PowerSpeed::DoEffect (gzdoom.pk3)
            afterimage.Angle = Owner.Angle;
            afterimage.sprite = Owner.sprite;
            afterimage.Frame = Owner.frame;
            afterimage.FloorClip = Owner.FloorClip;
            afterimage.Scale = Owner.Scale;
            afterimage.bBright = true;
            afterimage.bInvisible = Owner == players[consoleplayer].camera && Owner.player != null && !(Owner.player.cheats & CF_CHASECAM);
        }
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::GetBlend
    //
    // Calculates the blend color based on the number of ticks left.
    //
    //===========================================================================
    override Color GetBlend()
    {
        return _shieldDuration > 0
            ? Color(Min(_shieldDuration * 5, 200), 191, 0, 0)
            : Super.GetBlend();
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::DoEffect
    //
    // Decrements the shield duration counter.
    //
    //===========================================================================
    override void DoEffect()
    {
        if (_shieldDuration > 0)
        {
            _shieldDuration--;
        }
        else if (!IsActive())
        {
            // There is no point in keeping this item around
            // if we don't have the Super Armor anymore.
            // Note that a similar check should be made in AbsorbDamage as well,
            // if we remove it from there then it might trigger the effect
            // when the blend is still up but the armor itself is no more.
            Destroy();
        }
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::HandlePickup
    //
    // Emulates the standard armor pickup logic.
    //
    //===========================================================================
    override bool HandlePickup(Inventory item)
    {
        bool handled = item.GetClass() == GetClass();

        if (handled)
        {
            // sv_unlimited_pickup is not respected by BasicArmorPickup,
            // and we shoudln't respect it either.
            if (Amount < MaxAmount)
            {
                // The amount is limited by our max amount.
                Amount = Min(Amount + item.Amount, MaxAmount);
                item.bPickupGood = true;
            }
        }

        return handled;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::Travelled
    //
    // Resets the shield duration upon level change.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();
        _shieldDuration = 0;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::ShieldAbsorb
    //
    // Deducts the provided amount from the player's shield and returns
    // the amount of damage that was absorbed by it.
    //
    //===========================================================================
    protected virtual int ShieldAbsorb(int absorbing)
    {
        Amount -= absorbing;
        return absorbing;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::GetAmount
    //
    // Returns the amount of shield the player has left.
    // By default, returns the current amount of this item.
    //
    //===========================================================================
    protected virtual int GetAmount() const
    {
        return Amount;
    }

    //===========================================================================
    //
    // RR_SuperArmorShieldPickup::IsActive
    //
    // Returns true if the amount is non-zero.
    //
    //===========================================================================
    protected virtual bool IsActive() const
    {
        return Amount > 0;
    }
}
