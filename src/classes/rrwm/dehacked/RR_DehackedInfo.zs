//===========================================================================
//
// RR_DehackedInfo
//
// Contains information about a class type that can be modified
// by DEHACKED.
//
//===========================================================================
class RR_DehackedInfo
{
    // The actor class.
    private class<Actor> _type;

    // Internal DEHACKED ID of this actor.
    private int _dehackedId;
    // ID of the actor's first frame in the DEHACKED table.
    private int _firstFrame;
    // ID of the actor's last frame in the DEHACKED table.
    private int _lastFrame;

    // Has this actor been modified?
    private bool _isModified;

    //===========================================================================
    //
    // RR_DehackedInfo::Create
    //
    // Creates a new instance of the RR_DehackedInfo class.
    //
    //===========================================================================
    static RR_DehackedInfo Create(class<Actor> type, int dehackedId, int firstFrame, int lastFrame)
    {
        let i = new('RR_DehackedInfo');
        i.Init(type, dehackedId, firstFrame, lastFrame);

        return i;
    }

    //===========================================================================
    //
    // RR_DehackedInfo::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(class<Actor> type, int dehackedId, int firstFrame, int lastFrame)
    {
        _type = type;
        _dehackedId = dehackedId;
        _firstFrame = firstFrame;
        _lastFrame = lastFrame;
    }

    //===========================================================================
    //
    // RR_DehackedInfo::GetType
    //
    // Returns the type this instance represents.
    //
    //===========================================================================
    class<Actor> GetType() const
    {
        return _type;
    }

    //===========================================================================
    //
    // RR_DehackedInfo::GetFriendlyTypeName
    //
    // Returns the friendly name of the type represented by this instance.
    //
    //===========================================================================
    string GetFriendlyTypeName() const
    {
        return GetDefaultByType(_type).GetTag();
    }

    //===========================================================================
    //
    // RR_DehackedInfo::IsModified
    //
    // Checks if this type has been modified by DEHACKED.
    //
    //===========================================================================
    bool IsModified() const
    {
        return _isModified;
    }

    //===========================================================================
    //
    // RR_DehackedInfo::CheckId
    //
    // Marks this type as modified if the provided ID matches the expected ID.
    //
    //===========================================================================
    void CheckId(int dehackedId)
    {
        if (_dehackedId == dehackedId)
        {
            _isModified = true;
        }
    }

    //===========================================================================
    //
    // RR_DehackedInfo::CheckFrame
    //
    // Marks this type as modified if the provided frame number
    // is in the expected range.
    //
    //===========================================================================
    void CheckFrame(int frameNum)
    {
        if (frameNum >= _firstFrame && frameNum <= _lastFrame)
        {
            _isModified = true;
        }
    }
}
