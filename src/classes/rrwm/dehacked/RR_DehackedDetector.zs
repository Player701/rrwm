//===========================================================================
//
// RR_DehackedDetector
//
// Used to detect DEHACKED modifications of monsters.
// RRWM will not replace monsters if they have been modified by DEHACKED.
//
//===========================================================================
class RR_DehackedDetector
{
    // This color is used for DEHACKED warning messages.
    const COLOR_WARNING = TEXTCOLOR_ORANGE;
    // This color is used for the summary message and monster names.
    const COLOR_SUMMARY = TEXTCOLOR_GREEN;

    private Array<RR_DehackedInfo> _infos;

    //===========================================================================
    //
    // RR_DehackedDetector::Create
    //
    // Creates a new instance of the RR_DehackedDetector class.
    //
    //===========================================================================
    static RR_DehackedDetector Create()
    {
        let d = new('RR_DehackedDetector');
        d.Init();

        return d;
    }

    //===========================================================================
    //
    // RR_DehackedDetector::GetInstance
    //
    // Returns the global instance of the DEHACKED detector.
    //
    //===========================================================================
    static RR_DehackedDetector GetInstance()
    {
        return RR_GlobalEventHandler.GetInstance().GetDehackedDetector();
    }

    //===========================================================================
    //
    // RR_DehackedDetector::Init
    //
    // Performs first-time initialization. Detects all actors and states
    // tampered with by DEHACKED.
    //
    //===========================================================================
    protected void Init()
    {
        // Initialize the array.
        _infos.Push(RR_DehackedInfo.Create('ZombieMan',         2, 174, 206));
        _infos.Push(RR_DehackedInfo.Create('ShotgunGuy',        3, 207, 240));
        _infos.Push(RR_DehackedInfo.Create('Revenant',          6, 321, 356));
        _infos.Push(RR_DehackedInfo.Create('ChaingunGuy',      11, 406, 441));
        _infos.Push(RR_DehackedInfo.Create('Cacodemon',        15, 502, 521));
        _infos.Push(RR_DehackedInfo.Create('BaronOfHell',      16, 527, 555));
        _infos.Push(RR_DehackedInfo.Create('HellKnight',       18, 556, 584));
        _infos.Push(RR_DehackedInfo.Create('LostSoul',         19, 585, 600));
        _infos.Push(RR_DehackedInfo.Create('SpiderMastermind', 20, 601, 631));
        _infos.Push(RR_DehackedInfo.Create('Cyberdemon',       22, 674, 700));
        _infos.Push(RR_DehackedInfo.Create('WolfensteinSS',    24, 726, 762));

        int index = 0;

        // Scan all DEHACKED lumps and check what actors have been tampered with.
        while ((index = Wads.FindLump("DEHACKED", index)) != -1)
        {
            ScanDehackedLump(index);
            index++;
        }

        PrintWarningMessages();
    }

    //===========================================================================
    //
    // RR_DehackedDetector::IsActorModified
    //
    // Checks if the provided actor class has been modified by DEHACKED.
    //
    //===========================================================================
    bool IsActorModified(class<Actor> type)
    {
        foreach (info : _infos)
        {
            if (info.GetType() == type)
            {
                return info.IsModified();
            }
        }

        // This is actually a programming error if we got here.
        // We MUST register a class for detection if we want to check it.
        ThrowAbortException("%s: Actor class %s is not in the list.", GetClassName(), type.GetClassName());
        return false;
    }

    //===========================================================================
    //
    // RR_DehackedDetector::ScanDehackedLump
    //
    // Scans a DEHACKED lump and checks if any actors are being modified.
    //
    //===========================================================================
    private void ScanDehackedLump(int index)
    {
        Array<string> lines;

        string contents = Wads.ReadLump(index);
        contents.Split(lines, "\n", TOK_SKIPEMPTY);

        foreach (line : lines)
        {
            Array<string> tokens;
            line.Split(tokens, " ", TOK_SKIPEMPTY);

            if (tokens.Size() < 2)
            {
                continue;
            }

            if (tokens[0] ~== "thing")
            {
                CheckId(tokens[1].ToInt());
            }
            else if (tokens[0] ~== "frame")
            {
                CheckFrame(tokens[1].ToInt());
            }
        }
    }

    //===========================================================================
    //
    // RR_DehackedDetector::PrintWarningMessages
    //
    // Prints warning messages about replaced actors to the console.
    //
    //===========================================================================
    private void PrintWarningMessages()
    {
        int cnt = 0;

        foreach (info : _infos)
        {
            if (info.IsModified())
            {
                string mtype = info.GetFriendlyTypeName();
                string warning  = string.Format(
                    StringTable.Localize("$RR_DEHACKED_WARNING"),
                    string.Format("%s%s%s", COLOR_SUMMARY, mtype, COLOR_WARNING)
                );

                Console.Printf("%s%s: %s", COLOR_WARNING, StringTable.Localize("$RR_WARNING"), warning);
                cnt++;
            }
        }

        if (cnt > 0)
        {
            string summary = string.Format(StringTable.Localize("$RR_DEHACKED_SUMMARY"), cnt);
            Console.Printf("%s%s", COLOR_SUMMARY, summary);
        }
    }

    //===========================================================================
    //
    // RR_DehackedDetector::CheckId
    //
    // Checks the provided DEHACKED ID against known IDs
    // and marks the corresponding type as modified if there's a match.
    //
    //===========================================================================
    private void CheckId(int id)
    {
        foreach (info : _infos)
        {
            info.CheckId(id);
        }
    }

    //===========================================================================
    //
    // RR_DehackedDetector::CheckFrame
    //
    // Checks the provided DEHACKED frame index against known ranges
    // and marks the corresponding type as modified if there's a match.
    //
    //===========================================================================
    private void CheckFrame(int f)
    {
        foreach (info : _infos)
        {
            info.CheckFrame(f);
        }
    }
}
