//===========================================================================
//
// RR_ChaingunGuyBase
//
// Base class for Chaingun Guy replacements.
// These enemies differ only by their appearance.
// Which monster will replace the Chaingun Guy is determined by the settings.
//
//===========================================================================
class RR_ChaingunGuyBase : RR_ChaingunMonsterBase abstract
{
    Default
    {
        Health 70;
        Radius 20;
        Height 56;
        Mass 100;
        Speed 8;
        PainChance 170;

        SeeSound "chainguy/sight";
        PainSound "chainguy/pain";
        DeathSound "chainguy/death";
        ActiveSound "chainguy/active";

        Dropitem 'RR_Chaingun';
    }
}
