//===========================================================================
//
// RR_ShotgunGuy
//
// A version of Doom's Shotgun Guy
// equipped with the RRWM version of the Shotgun.
//
//===========================================================================
class RR_ShotgunGuy : RR_HitscanMonster
{
    // Enumerates possible stages of the Shotgun Guy reloading sequence.
    enum EReloadSequenceStage
    {
        // The sequence has just started.
        RSS_Started,

        // The sequence is progressing.
        RSS_InProgress,

        // Need to play the pumping animation now.
        RSS_Pump,

        // The sequence has finished.
        RSS_Finished
    };

    // The current reloading sequence state.
    private EReloadSequenceStage _curReloadStage;

    Default
    {
        Health 30;
        Radius 20;
        Height 56;
        Mass 100;
        Speed 8;
        PainChance 170;

        SeeSound "shotguy/sight";
        PainSound "shotguy/pain";
        DeathSound "shotguy/death";
        ActiveSound "shotguy/active";

        Tag "$TAG_RR_SHOTGUY";

        DropItem 'RR_Shotgun';
        RR_ReloadingMonster.WeaponType 'RR_Shotgun';
    }

    States
    {
        Spawn:
            SPOS AB 10 A_Look;
            Loop;
        See:
            SPOS AABBCCDD 3 A_ChaseOrReload;
            Loop;
        Missile:
            SPOS E 10 A_FaceTarget;
            SPOS F 10 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            SPOS E 2;
            SPOS E 8 A_ShotgunPump1;
            TNT1 A 0 A_ShotgunPump2(true);
            Goto See;
        ReloadWeapon:
            SPOS E 12 A_ReloadStart;
            // Fall through here
        ReloadRound:
            SPOS E 3 A_ReloadLoop;
            SPOS E 2 A_MaybeReloadRound;
            SPOS E 13;
            Loop;
        ReloadDone:
            SPOS E 9;
            SPOS E 8 A_ShotgunPump1;
            SPOS E 15 A_ShotgunPump2(false);
            Goto ReloadWeapon; // NB: needed for realistic reload
        RealisticReloadDone:
            SPOS E 6;
            Goto See;
        Pain:
            SPOS G 3;
            SPOS G 3 A_Pain;
            Goto See;
        Death:
            SPOS H 5;
            SPOS I 5 A_Scream;
            SPOS J 5 A_NoBlocking;
            SPOS K 5;
            SPOS L -1;
            Stop;
        XDeath:
            SPOS M 5;
            SPOS N 5 A_XScream;
            SPOS O 5 A_NoBlocking;
            SPOS PQRST 5;
            SPOS U -1;
            Stop;
        Raise:
            SPOS L 5;
            SPOS KJIH 5;
            Goto See;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::WeaponAttack
    //
    // Fires the zombie's shotgun.
    //
    //===========================================================================
    override void WeaponAttack()
    {
        HitscanAttack(Random[RR_ShotgunGuy__WeaponAttack](1, 5) * 3, 3, 22.5);
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(RR_Shotgun.SND_PUMP_1);
        MarkSound(RR_Shotgun.SND_PUMP_2);
        MarkSound(RR_Shotgun.SND_RELOAD);
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::PreReload
    //
    // Signifies that reloading has now begun.
    //
    //===========================================================================
    override void PreReload()
    {
        _curReloadStage = RSS_Started;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunPump1
    //
    // Plays the first half of the shotgun pumping sound sequence.
    //
    //===========================================================================
    void A_ShotgunPump1()
    {
        A_PlayWeaponAuxSound(RR_Shotgun.SND_PUMP_1);
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunPump2
    //
    // Plays the second shotgun pumping sound, and optionally ejects a casing.
    //
    //===========================================================================
    void A_ShotgunPump2(bool ejectCasing)
    {
        A_PlayWeaponAuxSound(RR_Shotgun.SND_PUMP_2);

        if (ejectCasing)
        {
            A_EjectCasing();
        }
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ReloadStart
    //
    // If starting a reloading sequence, immediately reloads one round.
    // If the reloading sequence has already finished, jumps to the See state.
    // Otherwise, continues uninterrupted.
    //
    //===========================================================================
    state A_ReloadStart()
    {
        if (_curReloadStage == RSS_Started)
        {
            // Avoid stun-locking in the beginning of the reloading sequence
            // by immediately loading a single round.
            A_ReloadOneRound();
        }
        else if (_curReloadStage == RSS_Finished)
        {
            // Realistic reload is disabled, we are returning from ReloadDone,
            // so jump to the See state now.
            return SeeState;
        }

        // Otherwise, continue
        return null;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ReloadLoop
    //
    // Performs reloading and/or jumps between reloading state sequences
    // according to the current internal state and whether realistic reloading
    // is enabled.
    //
    //===========================================================================
    state A_ReloadLoop()
    {
        bool playPump = _curReloadStage == RSS_Pump;
        bool reloadDone = ShouldStopReloading();

        if (reloadDone)
        {
            // Mark the sequence as finished if we stop reloading
            _curReloadStage = RSS_Finished;
        }

        // Check if we need to play the pumping animation now.
        if (playPump)
        {
            if (!reloadDone)
            {
                // Ensure the animation is not played again
                _curReloadStage = RSS_InProgress;
            }

            return ResolveState('ReloadDone');
        }

        // If we can no longer reload,
        // jump to the appropriate ReloadDone state.
        if (reloadDone)
        {
            // Jump to the appropriate state
            return RR_Shotgun.RealisticReloadEnabled()
                ? ResolveState('RealisticReloadDone')
                : ResolveState('ReloadDone');
        }

        // Look for targets while we are reloading,
        // but don't jump to any other states.
        // This is needed if the monster is reloading its weapon
        // after the target has died.
        if (Target == null || Target.Health <= 0)
        {
            A_LookEx(LOF_NOJUMP);
        }

        return null;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_MaybeReloadRound
    //
    // First, plays the shotgun reloading sound. Then, reloads a single round
    // unless it has already been loaded at the beginning of the sequence.
    //
    // Also, if reloading is being done from an empty clip,
    // sets the pumping animation to play next if realistic reloading is enabled.
    //
    //===========================================================================
    void A_MaybeReloadRound()
    {
        A_PlayWeaponAuxSound(RR_Shotgun.SND_RELOAD);

        if (_curReloadStage == RSS_InProgress)
        {
            A_ReloadOneRound();
        }
        else
        {
            // Skip loading because we've done this at the very start.
            // Instead, set pumping animation to play if loading an empty clip.
            if (RR_Shotgun.RealisticReloadEnabled())
            {
                let clip = GetClip();

                if (clip != null && clip.GetAmount() == GetDefaultByType(WeaponType).AmmoUse1)
                {
                    _curReloadStage = RSS_Pump;
                    return;
                }
            }

            // Load following rounds normally.
            _curReloadStage = RSS_InProgress;
        }
    }
}
