//===========================================================================
//
// RR_SpiderMastermind
//
// A variant of Doom's Spider Mastermind which behaves exactly
// like the vanilla one, except that it has a different attack sound
// and it also ejects shotgun casings while firing.
//
//===========================================================================
class RR_SpiderMastermind : SpiderMastermind
{
    States
    {
        Missile:
            SPID A 20 BRIGHT A_FaceTarget;
            SPID G 1 BRIGHT A_SpiderdemonAttack;
            SPID G 3 BRIGHT A_EjectCasing;
            SPID H 1 BRIGHT A_SpiderdemonAttack;
            SPID H 3 BRIGHT A_EjectCasing;
            SPID H 1 BRIGHT A_SpidRefire;
            Goto Missile+1;
    }

    //===========================================================================
    //
    // RR_SpiderMastermind::A_SpiderdemonAttack
    //
    // Plays the attack sound and performs the Spider Mastermind's attack.
    //
    //===========================================================================
    void A_SpiderdemonAttack()
    {
        // The decal generator may have been reset upon loading a save,
        // so we should check every time if it's null and set it again.
        if (DecalGenerator == null)
        {
            DecalGenerator = GetDefaultByType('RR_Shotgun').DecalGenerator;
        }

        // Set the attack sound.
        // NB: set every time in case it changes in future versions,
        // so as to avoid breaking save files.
        AttackSound = GetDefaultByType('RR_Shotgun').AttackSound;

        // Attack!
        int dmg = Random[RR_SpiderMastermind__A_SpiderdemonAttack](1, 5) * 3;
        RR_HitscanMonster.StaticHitscanAttack(self, dmg, 3, 22.5, GetDefaultByType('RR_Shotgun').PuffType);
    }

    //===========================================================================
    //
    // RR_SpiderMastermind::A_EjectCasing
    //
    // Ejects a shotgun casing.
    //
    //===========================================================================
    void A_EjectCasing()
    {
        if (RR_Settings.InPlay(RR_SETTING_SPIDERDEMON_CASINGS).IsOn())
        {
            // Note that we pass a radius factor of 0.5
            // because the Spider Mastermind is really big.
            // If we don't do this, it will look as if casings
            // are being spawned out of thin air.
            RR_EffectBase.SpawnFromSourceWeapon(
                GetDefaultByType('RR_Shotgun').CasingType,
                self, 0.5, 32 - FloorClip,
                (-6, 0), (-0.5, 2, 0), (0.5, 1.0, 0)
            );
        }
    }
}
