//===========================================================================
//
// RR_ChaingunMonsterBase
//
// Base class for monsters wielding the RRWM version of the Chaingun
// or a similar weapon.
//
//===========================================================================
class RR_ChaingunMonsterBase : RR_HitscanMonster abstract
{
    Default
    {
        RR_ReloadingMonster.WeaponType 'RR_Chaingun';
    }

    //===========================================================================
    //
    // RR_ChaingunMonsterBase::WeaponAttack
    //
    // Fires the monster's Zen-II handgun.
    //
    //===========================================================================
    override void WeaponAttack()
    {
        HitscanAttack(Random[RR_ChaingunMonsterBase__WeaponAttack](1, 5) * 3, 1, 22.5);
    }

    //===========================================================================
    //
    // RR_ChaingunMonsterBase::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(RR_Chaingun.SND_RELOAD_1);
        MarkSound(RR_Chaingun.SND_RELOAD_2);
    }

    //===========================================================================
    //
    // RR_ChaingunMonsterBase::A_ChaingunReload1
    //
    // Plays the first half of the chaingun reloading sound sequence.
    //
    //===========================================================================
    void A_ChaingunReload1()
    {
        A_PlayWeaponAuxSound(RR_Chaingun.SND_RELOAD_1);
    }

    //===========================================================================
    //
    // RR_ChaingunMonsterBase::A_ChaingunReload2
    //
    // Plays the second half of the chaingun reloading sound sequence.
    //
    //===========================================================================
    void A_ChaingunReload2()
    {
        A_PlayWeaponAuxSound(RR_Chaingun.SND_RELOAD_2);
    }
}
