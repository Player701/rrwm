//===========================================================================
//
// RR_ReloadingMonster
//
// Base class for monsters carrying reloadable weapons.
//
// To use:
//
// - Assign a class descending from RR_ReloadableWeapon
//   to the WeaponType property.
//
// - Use A_ChaseOrReload instead of A_Chase in the monster's See state.
//   This will make the monster jump to its ReloadWeapon state
//   if the weapon is empty.
//
// - Use A_WeaponAttack in the monster's Attack state.
//   This will deplete the weapon's clip when firing.
//
// - Override the WeaponAttack virtual function to perform the actual attack.
//
// - Use A_EjectCasing if there is a need to eject a casing.
//
// - Use A_ReloadFullClip in the ReloadWeapon state.
//
//===========================================================================
class RR_ReloadingMonster : Actor abstract
{
    // The type of the weapon this monster wields.
    // It is used to read propertly values such as casing and empty clip type,
    // their initial offsets, velocities etc.
    // It is NOT used for the damage or state duration calculations.
    protected meta class<RR_ReloadableWeapon> WeaponType;

    // An instance of RR_WeaponClip used to store the current clip amount.
    private RR_WeaponClip _clip;

    property WeaponType: WeaponType;

    Default
    {
        Monster;
        +FLOORCLIP;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_ChaseOrReload
    //
    // Jumps to the monster's ReloadWeapon state if the monster's weapon
    // needs reloading. Otherwise, calls A_Chase.
    //
    //===========================================================================
    state A_ChaseOrReload()
    {
        if (ShouldReload())
        {
            return BeginReload();
        }

        A_Chase();
        return null;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_WeaponAttack
    //
    // Jumps to the monster's ReloadWeapon state if the monster's weapon
    // needs reloading. Otherwise, depletes ammo in the weapon's clip
    // and calls WeaponAttack to perform the actual attack.
    //
    //===========================================================================
    state A_WeaponAttack()
    {
        if (_clip != null)
        {
            // If we somehow got here without reloading first, we have to reload.
            // Sorry... the computer ain't gonna be a cheating bastard this time.
            if (!_clip.CheckAmmo())
            {
                return BeginReload();
            }

            // Deplete ammo in the clip.
            _clip.DepleteAmmo();
        }

        // Play the attack sound...
        A_StartSound(AttackSound, CHAN_WEAPON);

        // ...and perform the actual attack.
        WeaponAttack();

        return null;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_PlayWeaponAuxSound
    //
    // Plays the specified sound on the CHAN_WEAPON_AUX channel.
    //
    //===========================================================================
    void A_PlayWeaponAuxSound(sound whattoplay)
    {
        A_StartSound(whattoplay, RR_SoundChannels.CHAN_WEAPON_AUX);
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_ReloadFullClip
    //
    // Reloads the monster's weapon.
    //
    //===========================================================================
    void A_ReloadFullClip()
    {
        if (_clip != null)
        {
            _clip.LoadAmmo(_clip.GetCapacity());
        }
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_ReloadOneRound
    //
    // Reloads one round of the monster's weapon clip.
    //
    //===========================================================================
    void A_ReloadOneRound()
    {
        if (_clip != null)
        {
            _clip.LoadAmmo(GetDefaultByType(WeaponType).AmmoUse1);
        }
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::A_EjectClip
    //
    // Ejects an empty clip.
    //
    //===========================================================================
    void A_EjectClip()
    {
        GetDefaultByType(WeaponType).SpawnEmptyClip(self);
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::BeginPlay
    //
    // Checks that the weapon type is assigned, and if not, throws an error.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        if (WeaponType == null)
        {
            ThrowAbortException("%s: Weapon type must not be null.", GetClassName());
        }

        // Set the attack sound to be the same as the weapon's attack sound.
        AttackSound = GetDefaultByType(WeaponType).AttackSound;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::PostBeginPlay
    //
    // Initializes the weapon's clip if monster reloading is enabled.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        if (RR_Settings.InPlay(RR_SETTING_MONSTERRELOAD).IsOn())
        {
            let weapon = GetDefaultByType(WeaponType);

            _clip = RR_WeaponClip.Create(weapon.GetClipCapacity(), weapon.AmmoUse1);
            _clip.LoadAmmo(_clip.GetCapacity());
        }
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::GetObituary
    //
    // Generates an obituary based on the properties of the weapon
    // the monster wields.
    //
    //===========================================================================
    final override string GetObituary(Actor victim, Actor inflictor, Name mod, bool playerattack)
    {
        // For projectile-based weapons, the projectile's obituary is retrieved.
        // (Not that we have any monsters with such weapons, though...)
        if (inflictor is 'RR_Projectile')
        {
            return inflictor.GetObituary(victim, self, mod, false);
        }

        // Use the weapon's obituary unless the damage was dealt in melee.
        if (mod != 'Melee')
        {
            let weap = GetDefaultByType(WeaponType);
            return weap.GetObituary(victim, self, mod, false);
        }

        // Otherwise, use default behavior.
        return Super.GetObituary(victim, inflictor, mod, playerattack);
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::WeaponAttack
    //
    // Overridden in derived classes to implement the monster's actual attack.
    //
    //===========================================================================
    protected abstract void WeaponAttack();

    //===========================================================================
    //
    // RR_ReloadingMonster::GetClip
    //
    // Gets the clip.
    //
    //===========================================================================
    protected RR_WeaponClip GetClip()
    {
        return _clip;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::ShouldStopReloading
    //
    // Checks if this monster should interrupt the reload sequence.
    //
    //===========================================================================
    protected bool ShouldStopReloading()
    {
        // Don't do anything if we have no clip.
        if (_clip != null)
        {
            // Always stop if the clip is full.
            if (_clip.IsFull())
            {
                return true;
            }

            // Never stop if the clip is empty, or if we have no target,
            // or if the target is dead or not around us.
            if (!_clip.CheckAmmo() || Target == null || Target.Health <= 0 || !CheckIfTargetInLOS())
            {
                return false;
            }

            // Otherwise, decide randomly.
            return RR_Math.RandomRoll(GetChanceToStopReloading());
        }

        return false;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::PreReload
    //
    // Called before reloading begins.
    //
    //===========================================================================
    protected virtual void PreReload()
    {
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::BeginReload
    //
    // Calls PreReload and returns the ReloadWeapon state.
    //
    //===========================================================================
    private state BeginReload()
    {
        PreReload();
        return ResolveState('ReloadWeapon');
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::ShouldReload
    //
    // Checks if this monster should reload its weapon.
    //
    //===========================================================================
    private bool ShouldReload()
    {
        // Don't reload if we have no clip.
        if (_clip != null)
        {
            // We will, of course, always reload if the clip is empty.
            if (!_clip.CheckAmmo())
            {
                return true;
            }

            int amount = _clip.GetAmount();
            int halfCapacity = _clip.GetCapacity() / 2;

            // Never reload if the clip is more than half full, or if we have a target,
            // and the target is not dead and around us.
            if (amount > halfCapacity || Target != null && Target.Health > 0 && CheckIfTargetInLOS())
            {
                return false;
            }

            // If we lack a target, or if it is dead, we will reload.
            // If we got here, this also means that the clip is less than half empty.
            // This is what we want.
            if (Target == null || Target.Health <= 0)
            {
                return true;
            }

            // If we got here, none of the above checks have passed,
            // so decide randomly.
            return RR_Math.RandomRoll(GetChanceToReload(amount, halfCapacity));
        }

        return false;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::GetChanceToReload
    //
    // Computes the current chance for this monster to reload its weapon.
    //
    //===========================================================================
    private double GetChanceToReload(int amount, int halfCapacity)
    {
        // Compute the initial chance to NOT start reloading.
        double chance = RR_Math.LogisticCurve(double(amount) / halfCapacity, 0.1);

        // The chance will be further attenuated by the distance to the target.
        chance = AttenuateChanceByDistance(chance);

        // And this is the result.
        return 1 - chance;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::GetChanceToStopReloading
    //
    // Computes the current chance for this monster to stop reloading
    // during a reload sequence. This only applies to sequential reloading,
    // i.e. when rounds are reloaded one by one.
    //
    //===========================================================================
    private double GetChanceToStopReloading()
    {
        // Compute the initial chance to stop reloading.
        double chance = RR_Math.LogisticCurve(double(_clip.GetAmount()) / _clip.GetCapacity(), 0.5);
        // The chance will be further attenuated by the distance to the target.
        chance = AttenuateChanceByDistance(chance);

        return chance;
    }

    //===========================================================================
    //
    // RR_ReloadingMonster::AttenuateChanceByDistance
    //
    // Decreases the chance to stop reloading, or to not start reloading
    // based on the distance between this monster and its target.
    //
    //===========================================================================
    private double AttenuateChanceByDistance(double chance)
    {
        double distance = Distance3D(Target);
        return chance * (1 - RR_Math.LogisticCurve(distance, 2048));
    }
}
