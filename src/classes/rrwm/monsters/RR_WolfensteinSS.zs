//===========================================================================
//
// RR_WolfensteinSS
//
// A version of Doom II's Wolfenstein SS
// equipped with the RRWM version of the Chaingun, the Zen-II handgun.
// The weapon's firing speed is the same as that of the vanilla enemy's,
// so we could consider its handgun to be an older model
// (It doesn't drop anyhow).
//
//===========================================================================
class RR_WolfensteinSS : RR_ChaingunMonsterBase
{
    Default
    {
        Health 50;
        Radius 20;
        Height 56;
        Speed 8;
        PainChance 170;

        SeeSound "wolfss/sight";
        PainSound "wolfss/pain";
        DeathSound "wolfss/death";
        ActiveSound "wolfss/active";
        AttackSound "wolfss/attack";

        Tag "$TAG_RR_WOLFSS";

        Dropitem 'RR_Clip';
    }

    States
    {
        Spawn:
            SSWV AB 10 A_Look;
            Loop;
        See:
            SSWV AABBCCDD 3 A_ChaseOrReload;
            Loop;
        Missile:
            SSWV E 10 A_FaceTarget;
            SSWV F 10 A_FaceTarget;
            SSWV G 1 BRIGHT A_WeaponAttack;
            SSWV G 3 BRIGHT A_EjectCasing;
            SSWV F 6 A_FaceTarget;
            SSWV G 1 BRIGHT A_WeaponAttack;
            SSWV G 3 BRIGHT A_EjectCasing;
            SSWV F 1 A_CPosRefire;
            Goto Missile+1;
        ReloadWeapon:
            SSWV E 5 A_ReloadFullClip;
            SSWV E 5 A_ChaingunReload1;
            SSWV E 25 A_EjectClip;
            SSWV E 24 A_ChaingunReload2;
            Goto See;
        Pain:
            SSWV H 3;
            SSWV H 3 A_Pain;
            Goto See;
        Death:
            SSWV I 5;
            SSWV J 5 A_Scream;
            SSWV K 5 A_NoBlocking;
            SSWV L 5;
            SSWV M -1;
            Stop;
        XDeath:
            SSWV N 5 ;
            SSWV O 5 A_XScream;
            SSWV P 5 A_NoBlocking;
            SSWV QRSTU 5;
            SSWV V -1;
            Stop;
        Raise:
            SSWV M 5;
            SSWV LKJI 5;
            Goto See;
    }
}
