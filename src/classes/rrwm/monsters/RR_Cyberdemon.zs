//===========================================================================
//
// RR_Cyberdemon
//
// A variant of Doom's Cyberdemon which behaves exactly the same
// as the vanilla Cyberdemon, except that the type of missile it shoots
// is controlled by a RRWM setting.
//
//===========================================================================
class RR_Cyberdemon : Cyberdemon
{
    States
    {
        Missile:
            CYBR E 6 A_FaceTarget;
            CYBR F 12 A_CyberdemonAttack;
            CYBR E 12 A_FaceTarget;
            CYBR F 12 A_CyberdemonAttack;
            CYBR E 12 A_FaceTarget;
            CYBR F 12 A_CyberdemonAttack;
            Goto See;
    }

    //===========================================================================
    //
    // RR_Cyberdemon::A_CyberdemonAttack
    //
    // Shoots a normal rocket or a Striker missile,
    // based on the corresponding setting's value.
    //
    //===========================================================================
    void A_CyberdemonAttack()
    {
        if (Target != null)
        {
            A_FaceTarget();

            // Choose the missile type based on the value of the setting.
            let missileType = RR_Settings.InPlay(RR_SETTING_CYBERDEMON_NEWROCKETS).IsOn()
                ? 'RR_Rocket'
                : 'Rocket';

            SpawnMissile(Target, missileType);
        }
    }
}
