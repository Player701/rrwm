//===========================================================================
//
// RR_BaronOfHell
//
// A version of the Baron of Hell which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_BaronOfHell : BaronOfHell
{
    Default
    {
        // See my comment on this in RR_HellKnight.zs
        BloodColor "DarkGreen";
    }
}
