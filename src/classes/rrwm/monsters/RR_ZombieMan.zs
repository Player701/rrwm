//===========================================================================
//
// RR_ZombieMan
//
// A version of Doom's Zombieman
// equipped with the RRWM version of the Pistol.
//
//===========================================================================
class RR_ZombieMan : RR_HitscanMonster
{
    Default
    {
        Health 20;
        Radius 20;
        Height 56;
        Speed 8;
        PainChance 200;
        Monster;

        SeeSound "grunt/sight";
        PainSound "grunt/pain";
        DeathSound "grunt/death";
        ActiveSound "grunt/active";

        Tag "$TAG_RR_ZOMBIE";

        DropItem 'RR_Clip';
        RR_ReloadingMonster.WeaponType 'RR_Pistol';
    }

    States
    {
        Spawn:
            POSS AB 10 A_Look;
            Loop;
        See:
            POSS AABBCCDD 4 A_ChaseOrReload;
            Loop;
        Missile:
            POSS E 10 A_FaceTarget;
            POSS F 2 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            POSS F 8 BRIGHT Light("ZOMBIEATK") A_EjectCasing;
            POSS E 8;
            Goto See;
        ReloadWeapon:
            TNT1 A 0 A_ReloadFullClip;
            POSS E 4 A_PlayWeaponAuxSound(RR_Pistol.SND_RELOAD_1);
            POSS E 11 A_EjectClip;
            POSS E 18 A_PlayWeaponAuxSound(RR_Pistol.SND_RELOAD_2);
            Goto See;
        Pain:
            POSS G 3;
            POSS G 3 A_Pain;
            Goto See;
        Death:
            POSS H 5;
            POSS I 5 A_Scream;
            POSS J 5 A_NoBlocking;
            POSS K 5;
            POSS L -1;
            Stop;
        XDeath:
            POSS M 5;
            POSS N 5 A_XScream;
            POSS O 5 A_NoBlocking;
            POSS PQRST 5;
            POSS U -1;
            Stop;
        Raise:
            POSS K 5;
            POSS JIH 5;
            Goto See;
    }

    //===========================================================================
    //
    // RR_ZombieMan::WeaponAttack
    //
    // Fires the zombie's pistol.
    //
    //===========================================================================
    override void WeaponAttack()
    {
        HitscanAttack(Random[RR_ZombieMan__WeaponAttack](1, 5) * 3, 1, 22.5);
    }

    //===========================================================================
    //
    // RR_ZombieMan::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        MarkSound(RR_Pistol.SND_RELOAD_1);
        MarkSound(RR_Pistol.SND_RELOAD_2);
    }
}
