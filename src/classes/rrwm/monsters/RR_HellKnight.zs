//===========================================================================
//
// RR_HellKnight
//
// A version of the Hell Knight which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_HellKnight : HellKnight
{
    Default
    {
        // Too bad we can't change it directly on an actor instance,
        // then these replacement actors wouldn't be needed at all...
        BloodColor "DarkGreen";
    }
}
