//===========================================================================
//
// RR_ChaingunGuyNoExtraSprites
//
// A version of the Chaingun Guy that is used when the sprite set
// for this enemy has been replaced by a custom PWAD.
//
// Since RRWM's Chaingun Guy uses some additional sprites,
// it would look weird if the original sprites are replaced.
// RRWM will thus detect this situation and use this version
// instead of the normal Chaingun Guy when sprite replacements are found.
//
//===========================================================================
class RR_ChaingunGuyNoExtraSprites : RR_ChaingunGuy
{
    States
    {
        Missile:
            CPOS E 10 A_FaceTarget;
            Goto Super::Missile+1;
        ReloadWeapon:
            CPOS A 5 A_ReloadFullClip;
            CPOS A 5 A_ChaingunReload1;
            CPOS A 25 A_EjectClip;
            CPOS A 24 A_ChaingunReload2;
            Goto See;
    }
}
