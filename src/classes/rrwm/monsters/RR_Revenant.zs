//===========================================================================
//
// RR_Revenant
//
// A version of the Revenant which is identical to the original,
// except that it doesn't bleed.
//
//===========================================================================
class RR_Revenant : Revenant
{
    Default
    {
        +NOBLOOD;
    }
}
