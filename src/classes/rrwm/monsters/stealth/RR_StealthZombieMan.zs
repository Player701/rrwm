//===========================================================================
//
// RR_StealthZombieMan
//
// A stealth version of the Doom's Zombieman
// equipped with the RRWM version of the Pistol.
//
//===========================================================================
class RR_StealthZombieMan : RR_ZombieMan
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
