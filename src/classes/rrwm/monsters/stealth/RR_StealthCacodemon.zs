//===========================================================================
//
// RR_StealthCacodemon
//
// A stealh version of the Cacodemon which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_StealthCacodemon : RR_Cacodemon
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
