//===========================================================================
//
// RR_StealthHellKnight
//
// A stealh version of the Hell Knight which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_StealthHellKnight : RR_HellKnight
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
