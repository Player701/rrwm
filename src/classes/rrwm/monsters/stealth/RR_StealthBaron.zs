//===========================================================================
//
// RR_StealthBaron
//
// A stealth version of the Bar of Hell which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_StealthBaron : RR_BaronOfHell
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
