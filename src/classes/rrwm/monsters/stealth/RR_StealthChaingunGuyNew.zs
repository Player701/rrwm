//===========================================================================
//
// RR_StealthChaingunGuyNew
//
// A stealth version of the Doom's Chaingun Guy
// equipped with the RRWM version of the Chaingun.
//
//===========================================================================
class RR_StealthChaingunGuyNew : RR_ChaingunGuyNew
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
