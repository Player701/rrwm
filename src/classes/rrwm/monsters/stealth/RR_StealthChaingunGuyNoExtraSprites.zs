//===========================================================================
//
// RR_StealthChaingunGuyNoExtraSprites
//
// A stealth version of the Chaingun Guy
// that doesn't use RRWM's extra sprites.
//
//===========================================================================
class RR_StealthChaingunGuyNoExtraSprites : RR_ChaingunGuyNoExtraSprites
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
