//===========================================================================
//
// RR_StealthShotgunGuy
//
// A stealth version of the Doom's Shotgun Guy
// equipped with the RRWM version of the Shotgun.
//
//===========================================================================
class RR_StealthShotgunGuy : RR_ShotgunGuy
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
