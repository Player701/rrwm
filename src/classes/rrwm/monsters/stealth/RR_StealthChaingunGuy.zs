//===========================================================================
//
// RR_StealthChaingunGuy
//
// A stealth version of the Doom's Chaingun Guy
// equipped with the RRWM version of the Chaingun.
//
//===========================================================================
class RR_StealthChaingunGuy : RR_ChaingunGuy
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
