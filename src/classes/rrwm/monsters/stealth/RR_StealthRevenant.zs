//===========================================================================
//
// RR_StealthRevenant
//
// A stealth version of the Revenant which is identical to the original,
// except that it doesn't bleed.
//
//===========================================================================
class RR_StealthRevenant : RR_Revenant
{
    Default
    {
        +STEALTH;
        RenderStyle "Translucent";
        Alpha 0;
    }
}
