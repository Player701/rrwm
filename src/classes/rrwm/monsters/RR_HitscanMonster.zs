//===========================================================================
//
// RR_HitscanMonster
//
// Base class for monsters wielding hitscan reloadable weapons.
//
//===========================================================================
class RR_HitscanMonster : RR_ReloadingMonster abstract
{
    // The type of hitscan weapon used by this monster.
    private class<RR_HitscanWeapon> _hitscanWeaponType;

    //===========================================================================
    //
    // RR_HitscanMonster::StaticHitscanAttack
    //
    // Performs a bullet attack with the provided parameters.
    // Spread is adjusted based on whether vertical bullet spread is enabled.
    //
    // This static method is also used in RR_SpiderMastermind,
    // which does not derive from this class.
    //
    //===========================================================================
    static void StaticHitscanAttack(Actor source, int damageperbullet, int numbullets, double spread, class<Actor> puffType)
    {
        double spread_z = RR_Settings.InPlay(RR_SETTING_MONSTERVERTSPREAD).IsOn()
            ? RR_HitscanWeapon.DEFVERTSPREAD
            : 0;

        source.A_CustomBulletAttack(spread, spread_z, numbullets, damageperbullet, puffType, 0, CBAF_NORANDOM | CBAF_NORANDOMPUFFZ);
    }

    //===========================================================================
    //
    // RR_HitscanMonster::A_EjectCasing
    //
    // Ejects a casing. The type of the casing and its
    // spawn offset and velocity are determined by the weapon's properties.
    //
    //===========================================================================
    void A_EjectCasing()
    {
        GetDefaultByType(GetHitscanWeaponType()).SpawnCasing(self);
    }

    //===========================================================================
    //
    // RR_HitscanMonster::GetHitscanWeaponType
    //
    // Returns the monster's weapon type as a hitscan weapon type,
    // or throws an exception if the weapon type is wrong.
    //
    //===========================================================================
    private class<RR_HitscanWeapon> GetHitscanWeaponType()
    {
        if (_hitscanWeaponType == null)
        {
            _hitscanWeaponType = (class<RR_HitscanWeapon>)(WeaponType);

            if (_hitscanWeaponType == null)
            {
                ThrowAbortException("%s: Weapon type must descend from RR_HitscanWeapon.", GetClassName());
            }
        }

        return _hitscanWeaponType;
    }

    //===========================================================================
    //
    // RR_HitscanMonster::MarkPrecacheSounds
    //
    // Marks additional weapon sounds for precaching.
    //
    //===========================================================================
    override void MarkPrecacheSounds()
    {
        Super.MarkPrecacheSounds();

        // Precache the casing bounce sound.
        let casingType = GetDefaultByType(GetHitscanWeaponType()).CasingType;

        if (casingType != null)
        {
            MarkSound(GetDefaultByType(casingType).BounceSound);
        }
    }

    //===========================================================================
    //
    // RR_HitscanMonster::HitscanAttack
    //
    // Performs a hitscan attack. This is similar to A_CustomBulletAttack
    // with some parameters predefined and the puff type retrieved
    // from the assigned weapon type (if supported).
    //
    // Spread is adjusted based on whether vertical bullet spread is enabled.
    //
    //===========================================================================
    protected void HitscanAttack(int damageperbullet, int numbullets, double spread)
    {
        class<Actor> puffType = null;

        // Get the puff type.
        puffType = GetDefaultByType(GetHitscanWeaponType()).PuffType;

        // The decal generator may have been reset upon loading a save,
        // so we should check every time if it's null and set it again.
        if (DecalGenerator == null)
        {
            DecalGenerator = GetDefaultByType('RR_Shotgun').DecalGenerator;
        }

        // Perform the actual attack.
        StaticHitscanAttack(self, damageperbullet, numbullets, spread, puffType);
    }
}
