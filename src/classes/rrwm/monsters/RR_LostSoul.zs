//===========================================================================
//
// RR_LostSoul
//
// A version of the Lost Soul which is identical to the original,
// except that it doesn't bleed.
//
//===========================================================================
class RR_LostSoul : LostSoul
{
    Default
    {
        +NOBLOOD;
    }

    States
    {
        Spawn:
            SKUL AB 10 BRIGHT Light("LOSTSOUL") A_Look;
            Loop;
        See:
            SKUL AB 6 BRIGHT Light("LOSTSOUL") A_Chase;
            Loop;
        Missile:
            SKUL C 10 BRIGHT Light("LOSTSOUL") A_FaceTarget;
            SKUL D 4 BRIGHT Light("LOSTSOUL") A_SkullAttack;
            SKUL CD 4 BRIGHT Light("LOSTSOUL");
            Goto Missile+2;
        Pain:
            SKUL E 3 BRIGHT Light("LOSTSOUL");
            SKUL E 3 BRIGHT Light("LOSTSOUL") A_Pain;
            Goto See;
        Death:
            SKUL F 6 BRIGHT Light("LOSTSOUL");
            SKUL G 6 BRIGHT Light("LOSTSOUL") A_Scream;
            SKUL H 6 BRIGHT Light("LOSTSOUL_X1");
            SKUL I 6 BRIGHT Light("LOSTSOUL_X2") A_NoBlocking;
            SKUL J 6 Light("LOSTSOUL_X3");
            SKUL K 6 Light("LOSTSOUL_X4");
            Stop;
    }
}
