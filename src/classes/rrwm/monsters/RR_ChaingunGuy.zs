//===========================================================================
//
// RR_ChaingunGuy
//
// A version of Doom II's Chaingun Guy
// equipped with the RRWM version of the Chaingun, the Zen-II handgun.
// It replaces the Chaingun Guy by default.
//
// Credits (CPOSU* sprites): Zdoom Advanced Mod 0.72h
//
//===========================================================================
class RR_ChaingunGuy : RR_ChaingunGuyBase
{
    Default
    {
        Tag "$TAG_RR_CHAINGUY";
    }

    States
    {
        Spawn:
            CPOS AB 10 A_Look;
            Loop;
        See:
            CPOS AABBCCDD 3 A_ChaseOrReload;
            Loop;
        Missile:
            CPOS U 10 A_FaceTarget;
            CPOS F 1 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            CPOS F 1 BRIGHT Light("ZOMBIEATK") A_EjectCasing;
            CPOS E 2 BRIGHT Light("ZOMBIEATK");
            CPOS F 1 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            CPOS F 1 BRIGHT Light("ZOMBIEATK") A_EjectCasing;
            CPOS E 2 BRIGHT Light("ZOMBIEATK");
            CPOS F 1 BRIGHT Light("ZOMBIEATK") A_CPosRefire;
            Goto Missile+1;
       ReloadWeapon:
            CPOS U 5 A_ReloadFullClip;
            CPOS U 5 A_ChaingunReload1;
            CPOS U 25 A_EjectClip;
            CPOS U 24 A_ChaingunReload2;
            Goto See;
        Pain:
            CPOS G 3;
            CPOS G 3 A_Pain;
            Goto See;
        Death:
            CPOS H 5;
            CPOS I 5 A_Scream;
            CPOS J 5 A_NoBlocking;
            CPOS KLM 5;
            CPOS N -1;
            Stop;
        XDeath:
            CPOS O 5;
            CPOS P 5 A_XScream;
            CPOS Q 5 A_NoBlocking;
            CPOS RS 5;
            CPOS T -1;
            Stop;
        Raise:
            CPOS N 5;
            CPOS MLKJIH 5;
            Goto See;
    }
}
