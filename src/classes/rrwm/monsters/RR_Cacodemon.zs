//===========================================================================
//
// RR_Cacodemon
//
// A version of the Cacodemon which is identical to the original,
// but has a more suitable blood color.
//
//===========================================================================
class RR_Cacodemon : Cacodemon
{
    Default
    {
        // See my comment on this in RR_HellKnight.zs
        BloodColor "Blue";
    }
}
