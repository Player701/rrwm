//===========================================================================
//
// RR_ChaingunGuyNew
//
// A zombified marine wielding a Zen-II handgun.
// It replaces the Chaingun Guy if the corresponding setting is enabled.
//
//===========================================================================
class RR_ChaingunGuyNew : RR_ChaingunGuyBase
{
    Default
    {
        Tag "$TAG_RR_CHAINGUY_NEW";
    }

    States
    {
        Spawn:
            UDM2 AB 10 A_Look;
            Loop;
        See:
            UDM2 AABBCCDD 3 A_ChaseOrReload;
            Loop;
        Missile:
            UDM2 E 10 A_FaceTarget;
            UDM2 F 1 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            UDM2 F 1 BRIGHT Light("ZOMBIEATK") A_EjectCasing;
            UDM2 E 2;
            UDM2 F 1 BRIGHT Light("ZOMBIEATK") A_WeaponAttack;
            UDM2 F 1 BRIGHT Light("ZOMBIEATK") A_EjectCasing;
            UDM2 E 2;
            UDM2 E 1 A_CPosRefire;
            Goto Missile + 1;
        ReloadWeapon:
            UDM2 E 5 A_ReloadFullClip;
            UDM2 E 5 A_ChaingunReload1;
            UDM2 E 25 A_EjectClip;
            UDM2 E 24 A_ChaingunReload2;
            Goto See;
        Pain:
            UDM2 G 3;
            UDM2 G 3 A_Pain;
            Goto See;
        Death:
            UDM2 H 5;
            UDM2 I 5 A_Scream;
            UDM2 J 5 A_NoBlocking;
            UDM2 K 5;
            UDM2 L -1;
            Stop;
        XDeath:
            UDM2 M 5;
            UDM2 N 5 A_XScream;
            UDM2 O 5 A_NoBlocking;
            UDM2 PQRST 5;
            UDM2 U -1;
            Stop;
        Raise:
            UDM2 KJIH 5;
            Goto See;
    }
}
