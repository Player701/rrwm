//===========================================================================
//
// RR_ShieldAfterimage
//
// An afterimage, drawn as a single color, that appears when a player
// is damaged while having the Super Armor equipped.
//
//===========================================================================
class RR_ShieldAfterimage : RR_EffectBase
{
    Default
    {
        RenderStyle "Stencil";
        StencilColor "BF 00 00";
        -FORCEXYBILLBOARD;
    }

    //===========================================================================
    //
    // RR_ShieldAfterimage::Tick
    //
    // Decreases the actor's alpha and increases scale. When alpha reaches 0,
    // the actor is destroyed.
    //
    //===========================================================================
    override void Tick()
    {
        Alpha -= 0.0625;
        Scale *= 1.01;

        if (Alpha <= 0)
        {
            Destroy();
        }
    }
}
