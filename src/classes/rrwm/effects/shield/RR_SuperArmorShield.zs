//===========================================================================
//
// RR_SuperArmorShield
//
// Implements shield effects (color and sound) for RR_SuperArmor.
//
// This is actually a stripped down version of RR_SuperArmorShieldPickup
// that only does the effects.
//
//===========================================================================
class RR_SuperArmorShield : RR_SuperArmorShieldPickup
{
    Default
    {
        Inventory.MaxAmount 1;
    }

    //===========================================================================
    //
    // RR_SuperArmorShield::ShieldAbsorb
    //
    // Because this item is only for cosmetic effects, this method does nothing
    // and always returns 0.
    //
    //===========================================================================
    override int ShieldAbsorb(int absorbing)
    {
        return 0;
    }

    //===========================================================================
    //
    // RR_SuperArmorShield::IsActive
    //
    // Checks if the player has the super armor and its amount is non-zero.
    //
    //===========================================================================
    override bool IsActive() const
    {
        // Check that the owner has the super armor.
        let ba = BasicArmor(Owner.FindInventory('BasicArmor'));
        return ba != null && ba.ArmorType == 'RR_SuperArmor' && ba.Amount > 0;
    }

    //===========================================================================
    //
    // RR_SuperArmorShield::GetAmount
    //
    // Returns the amount of armor the player have left.
    //
    //===========================================================================
    override int GetAmount() const
    {
        return Owner.FindInventory('BasicArmor').Amount;
    }
}
