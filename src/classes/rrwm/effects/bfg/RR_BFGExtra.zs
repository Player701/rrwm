//===========================================================================
//
// RR_BFGExtra
//
// Special effect spawned to signify targets that were hit
// by an explosion of RR_BFGBall.
//
//===========================================================================
class RR_BFGExtra : RR_EffectBase
{
    private double _angleDelta;

    Default
    {
        Alpha 0.5;
        Scale 0.35;
        RenderStyle "Add";

        +FLATSPRITE;
    }

    States
    {
        Spawn:
            SSPR ABCDEFGH 1   BRIGHT Light("RR_BFGBALL");
            SSPR IJKLMNOPQR 2 BRIGHT Light("RR_BFGBALL_EXPLODE_1");
            Stop;
    }

    //===========================================================================
    //
    // RR_BFGExtra::PostBeginPlay
    //
    // Randomizes the pitch and the angle and sets up a random angle delta.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        Angle = FRandom[RR_BFGExtra__PostBeginPlay](-180, 180);
        Pitch = 90 + FRandom[RR_BFGExtra__PostBeginPlay](-45, 45);

        _angleDelta = FRandom[RR_BFGExtra__PostBeginPlay](-10, 10);
    }

    //===========================================================================
    //
    // RR_BFGExtra::Tick
    //
    // Adjusts the angle by the random delta previously set up in PostBeginPlay.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();
        A_SetAngle(Angle + _angleDelta, SPF_INTERPOLATE);
    }
}
