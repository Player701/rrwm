//===========================================================================
//
// RR_BFGTrail
//
// Afterimage produced by RR_BFGBall in flight.
//
//===========================================================================
class RR_BFGTrail : RR_EffectBase
{
    Default
    {
        Alpha 0.5;
        Scale 0.75;
        RenderStyle "Add";
    }

    States
    {
        Spawn:
            PPRJ "#" 1 BRIGHT A_BFGTrailLoop;
            Loop;
    }

    //===========================================================================
    //
    // RR_BFGTrail::A_BFGTrailLoop
    //
    // Increases the size of the trail while simulaneously decreasing alpha.
    // Destroys the trail when alpha reaches 0.
    //
    //===========================================================================
    void A_BFGTrailLoop()
    {
        Alpha -= 0.025;
        Scale += (0.0125, 0.0125);

        if (Alpha <= 0)
        {
            Destroy();
        }
    }
}
