//===========================================================================
//
// RR_PistolEmptyClip
//
// An empty clip ejected when reloading the Pistol.
//
//===========================================================================
class RR_PistolEmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 0.6;
        -FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            SCL1 ABCDEFG 2 A_DebrisInFlight;
            Loop;
    }

    //===========================================================================
    //
    // RR_PistolEmptyClip::CheckDeathFrame
    //
    // Decides which frame to put the clip to when it comes to a rest.
    //
    //===========================================================================
    override int CheckDeathFrame(int curFrame)
    {
        // Easier to look at the sprites themselves than to explain everything here.
        // What I would say is that this algorithm is not entirely correct,
        // though it's not really that important.
        // What's more important is that provided the input is uniformly distributed,
        // the output is uniformly distributed as well.
        // In other words, we will not have one frame appear more often than others.
        switch (curFrame)
        {
            case "A":
            case "C":
            case "E":
            case "G":
                curFrame = Random[RR_PistolEmptyClip__CheckDeathFrame]("I", "L");
                break;
            case "B":
                curFrame = "I";
                break;
            case "D":
                curFrame = "K";
                break;
            case "F":
                curFrame = "L";
                break;
            case "H":
                curFrame = "J";
                break;
        }

        return curFrame;
    }
}
