//===========================================================================
//
// RR_PlasmaRifleEmptyClip
//
// An empty clip ejected when reloading the Hyperblaster
// (aka the Plasma Rifle).
//
//===========================================================================
class RR_PlasmaRifleEmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 0.5;
        -FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            HCLP A -1;
            Stop;
    }
}
