//===========================================================================
//
// RR_BFG9000EmptyClip
//
// An empty clip ejected when reloading the Disruptor Pulse Cannon
// (aka the BFG9000).
//
//===========================================================================
class RR_BFG9000EmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 0.45;
    }

    States
    {
        Spawn:
            PUCL A -1;
            Stop;
    }
}
