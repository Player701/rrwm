//===========================================================================
//
// RR_RailgunEmptyClip
//
// An empty clip ejected by the Railgun.
//
//===========================================================================
class RR_RailgunEmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 0.75;
        -FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            REMP ABCD 2 A_DebrisInFlight;
            Loop;
    }

    //===========================================================================
    //
    // RR_RailgunEmptyClip::CheckDeathFrame
    //
    // Decides which frame to put the clip to when it comes to a rest.
    //
    //===========================================================================
    override int CheckDeathFrame(int curFrame)
    {
        switch (curFrame)
        {
            case "B":
            case "D":
                // The clip is positioned vertically.
                // Choose one of the horizontal frames randomly.
                curFrame = RandomPick[RR_RailgunEmptyClip__CheckDeathFrame]("A", "C");
                break;
        }

        return curFrame;
    }
}
