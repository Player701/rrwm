//===========================================================================
//
// RR_ChaingunEmptyClip
//
// An empty clip ejected when reloading the Zen-II Handgun
// (aka the Chaingun).
//
//===========================================================================
class RR_ChaingunEmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 0.75;
    }

    States
    {
        Spawn:
            SCL2 ABCDEFDGH 2 A_DebrisInFlight;
            Loop;
    }

    //===========================================================================
    //
    // RR_ChaingunEmptyClip::CheckDeathFrame
    //
    // Decides which frame to put the clip to when it comes to a rest.
    //
    //===========================================================================
    override int CheckDeathFrame(int curFrame)
    {
        // Again as in RR_PistolEmptyClip, look at the sprites to see the explanation.
        // And again, this is not entirely correct, but provides a uniform distribution
        // of resulting frames.
        switch (curFrame)
        {
            case "A":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("L", "K");
                break;
            case "B":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("I", "L");
                break;
            case "C":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("I", "L");
                break;
            case "D":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("I", "L");
                break;
            case "E":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("I", "J");
                break;
            case "F":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("J", "K");
                break;
            case "G":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("J", "K");
                break;
            case "H":
                curFrame = RandomPick[RR_ChaingunEmptyClip__CheckDeathFrame]("J", "K");
                break;
        }

        return curFrame;
    }
}
