//===========================================================================
//
// RR_RocketLauncherEmptyClip
//
// An empty clip ejected when reloading the Striker Missile Launcher
// (aka the Rocket Launcher).
//
//===========================================================================
class RR_RocketLauncherEmptyClip : RR_DebrisBase
{
    Default
    {
        Scale 1.1;
    }

    States
    {
        Spawn:
            SCLP A -1;
            Stop;
    }
}
