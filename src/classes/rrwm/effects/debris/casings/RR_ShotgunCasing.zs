//===========================================================================
//
// RR_ShotgunCasing
//
// A casing that is ejected by the Shotgun and the Super Shotgun.
//
//===========================================================================
class RR_ShotgunCasing : RR_CasingBase
{
    Default
    {
        BounceSound "rrwm/weapons/shotgun/casing";
        Scale 0.5;
    }

    States
    {
        Spawn:
            SCAS ABCDEFGH 1 A_DebrisInFlight;
            Loop;
    }
}
