//===========================================================================
//
// RR_ChaingunCasing
//
// A casing that is ejected by the Zen-II Handgun (aka the Chaingun).
//
//===========================================================================
class RR_ChaingunCasing : RR_CasingBase
{
    Default
    {
        BounceSound "rrwm/weapons/chaingun/casing";
        Scale 0.15;
    }

    States
    {
        Spawn:
            MCAS ABCDE 1 A_DebrisInFlight;
            Loop;
    }
}
