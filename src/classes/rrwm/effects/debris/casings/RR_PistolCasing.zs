//===========================================================================
//
// RR_PistolCasing
//
// A casing that is ejected by the Pistol.
//
//===========================================================================
class RR_PistolCasing : RR_CasingBase
{
    Default
    {
        BounceSound "rrwm/weapons/pistol/casing";
        Scale 0.2;
    }

    States
    {
        Spawn:
            PCAS ABCDE 1 A_DebrisInFlight;
            Loop;
    }
}
