//===========================================================================
//
// RR_CasingBase
//
// Base class for casings. It is used to define their physics,
// as well as their smoke-spawning behavior.
//
//===========================================================================
class RR_CasingBase : RR_DebrisBase abstract
{
    // By default, the casing will keep emitting smoke for 3 seconds.
    const SMOKE_COUNT_DEFAULT = 105;

    // Defines how many ticks the casing will keep spawning smoke for.
    private int _smokeCount;

    // The setting that controls whether smoke is enabled.
    private transient RR_Setting _smokeSetting;

    Default
    {
        BounceType "Doom";
        +BOUNCEONACTORS;
        +DONTBOUNCEONSKY;
    }

    //===========================================================================
    //
    // RR_CasingBase::SetSmokeCount
    //
    // Sets the new smoke count.
    //
    //===========================================================================
    void SetSmokeCount(int newCount)
    {
        if (newCount < 0)
        {
            ThrowAbortException("%s: Smoke count cannot be negative.", GetClassName());
        }

        _smokeCount = newCount;
    }

    //===========================================================================
    //
    // RR_CasingBase::BeginPlay
    //
    // Sets the casing's DeathSound to be the same as its BounceSound.
    // This way, there's no need to define the same sound twice.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        DeathSound = BounceSound;
        _smokeCount = SMOKE_COUNT_DEFAULT;
    }

    //===========================================================================
    //
    // RR_CasingBase::Tick
    //
    // Spawns casing smoke as long as the smoke supply is non-zero.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (!bDestroyed && _smokeCount > 0)
        {
            _smokeCount--;

            // Don't actually spawn smoke if it isn't enabled.
            // We do a check here because we need to maintain
            // the correct smoke supply value at all times.
            if (!IsSmokeEnabled())
            {
                return;
            }

            let smokeOffset = (
                FRandom[RR_CasingBase__Tick](-1, 1),
                FRandom[RR_CasingBase__Tick](-1, 1),
                0.5
            );

            Spawn('RR_CasingSmoke', RR_Math.OffsetToPosition(smokeOffset, self), ALLOW_REPLACE);
        }
    }

    //===========================================================================
    //
    // RR_CasingBase::ReturnedToSpawnState
    //
    // Resets all bounce flags so that the casing can bounce again.
    //
    //===========================================================================
    override void ReturnedToSpawnState()
    {
        // Now this is madness, since we have one property in the default block
        // but it cannot be easily changed in the code. Bad design.
        bBounceOnWalls = true;
        bBounceOnFloors = true;
        bBounceOnCeilings = true;
        bAllowBounceOnActors = true;
        bBounceAutoOff = true;
    }

    //===========================================================================
    //
    // RR_CasingBase::IsSmokeEnabled
    //
    // Returns whether casing smoke is currently enabled.
    //
    //===========================================================================
    private bool IsSmokeEnabled() const
    {
        return RR_Settings.GetCached(RR_SETTING_CASINGSMOKE, _smokeSetting)
            .GetPlayValue()
            .IsOn();
    }
}
