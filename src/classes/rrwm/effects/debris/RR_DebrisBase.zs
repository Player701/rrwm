//===========================================================================
//
// RR_DebrisBase
//
// Base class for debris objects, such as casings and spent clips.
// These objects are placed in a queue to manage their lifetime.
//
//===========================================================================
class RR_DebrisBase : RR_MovingEffectBase abstract
{
    // Maximum amount of time the debris can spend in active state.
    // It goes inactive either as soon as A_SettleDebris is called,
    // or when this timeout is reached.
    const DEBRIS_ACTIVE_TICKS_MAX = 455;

    // How long the debris stays before disappearing?
    const DEBRIS_DEQUEUED_TICKS = 175;

    // How long it takes for the debris to completely disappear?
    const DEBRIS_FADE_TICKS = 70;

    // Maximum number of state jumps before erroring out
    // if we can't find the right frame.
    const MAX_STATE_JUMPS = 1000;

    // These are the possible states our object can be in.
    // This is used in the Tick method to check what to do.
    enum EDebrisState
    {
        // The debris object has just been spawned.
        // Used to avoid enqueueing and immediately dequeuing the object
        // if it is spawned and then instantly destroyed.
        DS_None,

        // The debris object is currently active.
        // This state lasts until it settles for the first time,
        // or when the activity timeout expires.
        DS_Active,

        // The debris object is no longer active.
        // This happens as soon as it settles for the first time,
        // or when the activity timer runs out.
        // This state lasts until the object has been dequeued,
        // and can thus last indefinitely if the queue is unlimited.
        DS_Inactive,

        // The debris object has been dequeued.
        // Now there is only a short timeout before it starts disappearing.
        DS_Dequeued,

        // The debris object has been dequeued and is disappearing.
        // As soon as it disappears completely, it will be destroyed.
        DS_Disappearing
    };

    // Points to a newer debris object in the queue,
    // i.e. the object that was created immediately after this one.
    // If this is null, this means the object is the newest on the level.
    RR_DebrisBase NextDebris;

    // Points to an older debris object in the queue,
    // i.e. the object that was created immediately before this one.
    // If this is null, this means the object is the oldest on the level.
    RR_DebrisBase PrevDebris;

    // Stores the current state of the object
    // so that the Tick method can decide what to do.
    private EDebrisState _curState;

    // Holds the pointer to the global RR_DebrisManager instance
    // so that we don't need to get it again on each tick.
    private RR_DebrisManager _debrisManager;

    // Has this object been dequeued? (Set via a callback by DebrisManager)
    private bool _isDequeued;

    // The amount of ticks left before this object starts disappearing
    // (in Dequeued state) or disappears completely (in Disappearing state).
    private int _timer;

    // Stores the position value corresponding to the last map boundary check.
    private vector3 _oldPos;

    // Stores the last result of the map boundary check.
    private bool _isOutsideOfMap;

    Default
    {
        -NOGRAVITY;
        -THRUACTORS;
        +MISSILE;
    }

    States
    {
        // No point in defining this in every subclass.
        Death:
            #### # -1 A_SettleDebris;
            Stop;
    }

    //===========================================================================
    //
    // RR_DebrisBase::A_DebrisInFlight
    //
    // Adjusts the current state duration based on the absolute velocity.
    // This is used to slow down the animation when the object is moving slowly.
    // This often happens if the object is underwater, where the de-sync
    // between the animation and the real speed would be very noticeable
    // if not for this logic.
    //
    //===========================================================================
    void A_DebrisInFlight()
    {
        // Don't do anything if this is a 0 or infinite-duration state.
        if (tics > 0)
        {
            tics += int(4 / Max(vel.Length(), 1));
        }
    }

    //===========================================================================
    //
    // RR_DebrisBase::A_SettleDebris
    //
    // Moves the debris object to inactive state, provided that its activity
    // timeout hasn't already expired.
    // Additionally, sets the MOVEWITHSECTOR flag and calls CheckDeathFrame
    // to ensure the correct appearance of the object in its Death state.
    //
    //===========================================================================
    void A_SettleDebris()
    {
        // Set the MOVEWITHSECTOR flag.
        // Otherwise, the object will not move along with platforms
        // and will simply hang in the air instead.
        // Let all the weirdness stay in Gravity Falls...
        bMoveWithSector = true;

        if (_curState == DS_Active)
        {
            // Change the state to signify the object can now be dequeued.
            _curState = DS_Inactive;
        }

        // Set the correct death frame.
        frame = CheckDeathFrame(frame + int("A")) - int("A");
    }

    //===========================================================================
    //
    // RR_DebrisBase::BeginPlay
    //
    // Randomizes the spawn frame.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        // Randomize the frame.
        SetRandomFrame();

        // Set up initial values for position check caching
        _oldPos = Pos;
        _isOutsideOfMap = Level.IsPointInLevel(Pos);
    }

    //===========================================================================
    //
    // RR_DebrisBase::PostBeginPlay
    //
    // Registers this object in the debris queue.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // The object is now active.
        _curState = DS_Active;

        // Add this object to the queue.
        GetDebrisManager(true).Enqueue(self);
    }

    //===========================================================================
    //
    // RR_DebrisBase::Grind
    //
    // Debris objects should be destroyed by crushers and closing doors.
    //
    //===========================================================================
    override bool Grind(bool items)
    {
        Destroy();
        return false;
    }

    //===========================================================================
    //
    // RR_DebrisBase::Tick
    //
    // Checks the current state and performs the necessary actions.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        // If we're dead, check if we're no longer on the floor
        // and are sufficiently high above it.
        // In this case, we should jump back to the Spawn state
        // so that we can show the animation before settling again.
        if (!bMissile)
        {
            if (!bOnMobj && ShouldFall() && ReturnToSpawnState())
            {
                bMissile = true;
                bMoveWithSector = false;
                ReturnedToSpawnState();
            }
        }
        else if (GetGravity() > 0 && bOnMobj && RR_Math.IsAlmostZero(Vel.Z))
        {
            // Force stop on 3D bridges if our Z velocity is zero.
            // Not handling this case appears to cause some weird
            // sliding glitches and potentially getting stuck
            // in an animation loop.
            Die(null, null);
        }

        // Check if we haven't been dequeued and are outside of the map.
        bool inQueueAndOutside = !_isDequeued && IsOutsideOfMap();

        // Change state to inactive if we've ended up outside of the map
        // or if the active state timeout has expired.
        if (inQueueAndOutside || _curState == DS_Active && GetAge() >= DEBRIS_ACTIVE_TICKS_MAX)
        {
            _curState = DS_Inactive;

            if (inQueueAndOutside)
            {
                // Tell the debris manager to remove us from the queue now
                GetDebrisManager(true).Remove(self);
                Dequeued();
            }
        }

        // If we're inactive, check if we've been dequeued.
        if (_curState == DS_Inactive && _isDequeued)
        {
            _curState = DS_Dequeued;
            _timer = 0;
        }

        if (_curState == DS_Dequeued)
        {
            // We've been dequeued, let's count down.
            // (actually up, but hey, this is a metaphor, all right?)
            _timer++;

            if (_timer > DEBRIS_DEQUEUED_TICKS)
            {
                _curState = DS_Disappearing;
                _timer = 0;
                A_SetRenderStyle(1, STYLE_Translucent);
            }
        }

        if (_curState == DS_Disappearing)
        {
            // We're disappearing, let's count down as well.
            _timer++;

            if (_timer > DEBRIS_FADE_TICKS)
            {
                // Time's up...
                Destroy();
            }
            else
            {
                // Fade ourselves out.
                Alpha -= 1.0 / DEBRIS_FADE_TICKS;
            }
        }
    }

    //===========================================================================
    //
    // RR_DebrisBase::IsOutsideOfMap
    //
    // A small optimization of Level.IsPointInLevel to avoid expensive checks
    // every tick if the position hasn't changed, which doesn't happen often.
    //
    //===========================================================================
    private bool IsOutsideOfMap()
    {
        if (_oldPos != Pos)
        {
            _isOutsideOfMap = !Level.IsPointInLevel(Pos);
            _oldPos = Pos;
        }

        return _isOutsideOfMap;
    }

    //===========================================================================
    //
    // RR_DebrisBase::OnDestroy
    //
    // Removes this object from the queue unconditionally
    // so that the links within the queue do not become corrupted.
    //
    //===========================================================================
    override void OnDestroy()
    {
        // Unless we've been dequeued, we need to request the debris manager
        // to remove us from the queue unconditionally.
        if (!_isDequeued && _curState != DS_None)
        {
            let debrisManager = GetDebrisManager(false);

            // The debris manager might get destroyed before this object
            // if the level is being freed, so take precautions here.
            // (maybe check the source code later and try to fix it?)
            if (debrisManager != null)
            {
                debrisManager.Remove(self);
            }
        }

        Super.OnDestroy();
    }

    //===========================================================================
    //
    // RR_DebrisBase::CheckDeathFrame
    //
    // Given the current frame as a character literal,
    // allows changing it so that the object does not appear
    // to have come to rest in an improbable position.
    //
    //===========================================================================
    virtual int CheckDeathFrame(int curFrame)
    {
        return curFrame;
    }

    //===========================================================================
    //
    // RR_DebrisBase::Dequeued
    //
    // The object has been dequeued, so its state has to be changed accordingly.
    //
    //===========================================================================
    void Dequeued()
    {
        _isDequeued = true;
    }

    //===========================================================================
    //
    // RR_DebrisBase::ReturnedToSpawnState
    //
    // Called when the debris returns back to the Spawn state
    // from the Death state.
    //
    //===========================================================================
    virtual void ReturnedToSpawnState()
    {
        // Back from the dead!
    }

    //===========================================================================
    //
    // RR_DebrisBase::GetDebrisManager
    //
    // Caches and returns the global instance of RR_DebrisManager.
    //
    //===========================================================================
    private RR_DebrisManager GetDebrisManager(bool required = true)
    {
        if (_debrisManager == null)
        {
            _debrisManager = RR_DebrisManager.GetInstance();

            if (_debrisManager == null && required)
            {
                ThrowAbortException("%s: Could not find the instance of the debris manager.", GetClassName());
            }
        }

        return _debrisManager;
    }

    //===========================================================================
    //
    // RR_DebrisBase::SetRandomFrame
    //
    // Jumps to a random state frame within the Spawn state sequence.
    // We could use A_Jump in subclasses, but what's the fun in that?
    // Also, it is a violation of the DRY principle:
    // we want the same behavior to manifest in all subclasses,
    // so we shouldn't define it in each subclass.
    // In other words: Don't Repeat Yourself!
    //
    //===========================================================================
    private void SetRandomFrame()
    {
        // First check if we have a valid spawn state.
        if (SpawnState == null)
        {
            return;
        }

        // Then determine how many frames there are.
        let st = SpawnState;
        int cnt = 0, cntMax = MAX_STATE_JUMPS;

        do
        {
            // Safe guard against incorrectly defined state sequences:
            // this will likely happen if the spawn state sequence doesn't
            // loop back to the spawn state.
            if (cnt == cntMax)
            {
                ThrowAbortException("%s: Unable to loop back to the spawn state after %d states.", GetClassName(), cntMax);
            }

            st = st.NextState;
            cnt++;

        } while (st != null && st != SpawnState);

        // Now get a random value
        int newFrame = Random[RR_DebrisBase__SetRandomFrame](0, cnt - 1);

        // Get the state.
        st = SpawnState;

        for (int i = 0; i < newFrame; i++)
        {
            st = st.NextState;
        }

        // Finally, set the state.
        SetState(st, true);
    }

    //===========================================================================
    //
    // RR_DebrisBase::ReturnToSpawnState
    //
    // Finds a state in the spawn state sequence that matches the current
    // sprite frame and jumps to it.
    //
    //===========================================================================
    private bool ReturnToSpawnState()
    {
        if (SpawnState == null)
        {
            return false;
        }

        let st = SpawnState;
        int cnt = 0;

        while (st != null && st.Frame != frame && cnt < MAX_STATE_JUMPS)
        {
            if (cnt > 0 && st == SpawnState)
            {
                // Looped back to the initial state, do not continue.
                break;
            }

            st = st.NextState;
            cnt++;
        }

        if (st == null || st.Frame != frame)
        {
            // Couldn't find a suitable state, fall back to the first one.
            st = SpawnState;
        }

        SetState(st);
        return true;
    }
}
