//===========================================================================
//
// RR_DebrisManager
//
// Implements a queue where debris objects are kept.
//
//===========================================================================
class RR_DebrisManager : Thinker
{
    // The statnum used by the debris manager.
    const DM_STATNUM = STAT_USER;

    // The maximum possible value for the queue size limit setting.
    // Note that it is also possible to disable the queue entirely:
    // objects will still be enqueued, but not dequeued.
    const LIMIT_MAX = 2000;

    // Total number of objects in the queue.
    private int _count;

    // The first object in the queue.
    private RR_DebrisBase _first;

    // The last object in the queue.
    private RR_DebrisBase _last;

    //===========================================================================
    //
    // RR_DebrisManager::Init
    //
    // Initializes the debris manager for the current level.
    //
    //===========================================================================
    static void Init()
    {
        // Don't initialize twice. This can happen since the WorldLoaded event
        // is also called when loading a save game.
        if (GetInstance() == null)
        {
            InitNew();
        }
    }

    //===========================================================================
    //
    // RR_DebrisManager::GetInstance
    //
    // Returns the instance of this class for the current level.
    //
    //===========================================================================
    static clearscope RR_DebrisManager GetInstance()
    {
        let it = ThinkerIterator.Create('RR_DebrisManager', DM_STATNUM);
        return RR_DebrisManager(it.Next(true));
    }

    //===========================================================================
    //
    // RR_DebrisManager::InitNew
    //
    // Initializes a new instance of this class.
    //
    //===========================================================================
    private static void InitNew()
    {
        let debrisManager = new('RR_DebrisManager');
        debrisManager.ChangeStatNum(DM_STATNUM);
    }

    //===========================================================================
    //
    // RR_DebrisManager::Tick
    //
    // Checks if the current queue size exceeds the limit,
    // and dequeues objects until it is at the limit again.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        // Dequeue objects until the queue is at capacity.
        while (IsLimitExceeded())
        {
            Dequeue();
        }
    }

    //===========================================================================
    //
    // RR_DebrisManager::IsLimitEnabled
    //
    // Checks whether the debris queue limit is enabled.
    //
    //===========================================================================
    private bool IsLimitEnabled() const
    {
        return RR_Settings.InPlay(RR_SETTING_DEBRISQUEUE_ENABLED).IsOnMultiplayerAware();
    }

    //===========================================================================
    //
    // RR_DebrisManager::GetLimitValue
    //
    // Returns the value of the debris queue limit.
    //
    //===========================================================================
    private int GetLimitValue() const
    {
        return RR_Settings.InPlay(RR_SETTING_DEBRISQUEUE_LIMIT).GetInt();
    }

    //===========================================================================
    //
    // RR_DebrisManager::IsLimitExceeded
    //
    // Checks if the queue size has exceeded the limit.
    //
    //===========================================================================
    private bool IsLimitExceeded() const
    {
        return IsLimitEnabled() && _count > GetLimitValue();
    }

    //===========================================================================
    //
    // RR_DebrisManager::PrintStatusInfo
    //
    // Prints status information to the console.
    //
    //===========================================================================
    clearscope void PrintStatusInfo()
    {
        string limitStr = IsLimitEnabled()
            ? string.Format("%d", GetLimitValue())
            : StringTable.Localize("$RR_EVENT_DEBRISQUEUE_DISABLED");

        Console.Printf(StringTable.Localize("$RR_EVENT_DEBRISQUEUE_STATUS"), _count, limitStr);
    }

    //===========================================================================
    //
    // RR_DebrisManager::Enqueue
    //
    // Adds an object to the end of the queue.
    //
    //===========================================================================
    void Enqueue(RR_DebrisBase debris)
    {
        if (_last != null)
        {
            // The calling object is now linked.
            debris.NextDebris = _last;
            // The (soon-to-be-former) last object is now linked as well.
            _last.PrevDebris = debris;
        }
        else
        {
            // Special case: in an empty queue,
            // the last object becomes the first object.
            _first = debris;
        }

        // The calling object is now the last in the queue.
        _last = debris;

        // We have one more now.
        _count++;
    }

    //===========================================================================
    //
    // RR_DebrisManager::IsInQueue
    //
    // Checks if an object is still in the queue.
    //
    //===========================================================================
    bool IsInQueue(RR_DebrisBase debris)
    {
        return debris.PrevDebris != null || debris.NextDebris != null || _first == debris;
    }

    //===========================================================================
    //
    // RR_DebrisManager::GetCount
    //
    // Returns the amount of objects in the queue.
    //
    //===========================================================================
    int GetCount()
    {
        return _count;
    }

    //===========================================================================
    //
    // RR_DebrisManager::Dequeue
    //
    // Removes the last object from the beginning on the queue.
    //
    //===========================================================================
    private void Dequeue()
    {
        // Make sure we have an object to dequeue.
        let obj = _first;

        if (obj == null)
        {
            ThrowAbortException("%s: Attempt to dequeue from an empty queue.", GetClassName());
        }

        // Set the head of the queue to the preceding object (if any).
        _first = obj.PrevDebris;

        // The calling object is now unlinked.
        // Note that NextDebris should already be null.
        obj.PrevDebris = null;

        // Special case: in a single-element queue,
        // the last object and the first object are one and the same.
        if (_first == null)
        {
            _last = null;
        }
        else
        {
            // Make sure the next element
            // doesn't point to anything ahead of it anymore.
            _first.NextDebris = null;
        }

        // We have one less now.
        _count--;

        // Tell the object it has been dequeued.
        obj.Dequeued();
    }

    //===========================================================================
    //
    // RR_DebrisManager::Remove
    //
    // Removes an object from any point in the queue.
    // This is called by RR_DebrisBase in OnDestroy.
    //
    //===========================================================================
    void Remove(RR_DebrisBase debris)
    {
        // First check if we need to remove the object from the queue.
        if (!IsInQueue(debris))
        {
            return;
        }

        let prev = debris.PrevDebris;
        let next = debris.NextDebris;

        // Re-link in the forward direction.
        // If the object to remove had a previous link,
        // connect it to the object's next link.
        if (prev != null)
        {
            prev.NextDebris = next;
        }
        else
        {
            // Otherwise, the object was the last in the queue,
            // so make its next link the new last object.
            _last = next;
        }

        // Re-link in the backward direction.
        // If the object to remove had a next link,
        // connect it to the object's previous link.
        if (next != null)
        {
            next.PrevDebris = prev;
        }
        else
        {
            // Otherwise, the object was the first in the queue,
            // so make its previous link the new first obvject.
            _first = prev;
        }

        // We have one less now.
        _count--;
    }
}
