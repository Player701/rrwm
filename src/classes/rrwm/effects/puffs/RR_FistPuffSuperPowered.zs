//===========================================================================
//
// RR_FistPuffSuperPowered
//
// A type of puff produced by the Fist when it is both powered and sped up
// by the Adrenaline powerup.
//
//===========================================================================
class RR_FistPuffSuperPowered : RR_FistPuffPowered
{
    Default
    {
        // Super-fast fists always gib enemies.
        +EXTREMEDEATH;

        // The super-powered puff produces slightly more sparks.
        RR_SparkPuff.SparkCount 1, 3;
    }
}
