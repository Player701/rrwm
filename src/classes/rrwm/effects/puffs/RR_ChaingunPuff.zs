//===========================================================================
//
// RR_ChaingunPuff
//
// A type of spark puff used by the Chaingun (aka the Zen-II handgun).
//
//===========================================================================
class RR_ChaingunPuff : RR_SparkPuff
{
    Default
    {
        RR_SparkPuff.SparkCount 2, 4;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.5;
        RR_SparkPuff.SparkScaleFactor 1.0, 1.5;
        RR_SparkPuff.SparkSpeed 4.5, 6.0;
    }
}
