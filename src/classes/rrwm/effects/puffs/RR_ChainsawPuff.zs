//===========================================================================
//
// RR_ChainsawPuff
//
// A type of spark puff produced by the Chainsaw.
//
//===========================================================================
class RR_ChainsawPuff : RR_SparkPuff
{
    Default
    {
        // We use +PUFFONACTORS and make the puff play the chainsaw hit sound
        // instead of the chainsaw, because the latter would override
        // an already playing sound. Puffs, on the other hand,
        // are spawned for each hit and playback is not interrupted.
        +PUFFONACTORS;

        SeeSound "rrwm/weapons/chainsaw/hit";
        // This sound is played when the puff hits a wall.
        AttackSound "rrwm/weapons/chainsaw/impact";

        RR_SparkPuff.SparkCount 1, 2;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.5;
        RR_SparkPuff.SparkScaleFactor 1.0, 1.25;
        RR_SparkPuff.SparkSpeed 3.0, 4.5;
    }
}
