//===========================================================================
//
// RR_FistPuff
//
// A type of puff produced by the Fist when neither Adrenaline nor Strength
// powerups are available.
//
//===========================================================================
class RR_FistPuff : RR_SparkPuff
{
    Default
    {
        +PUFFONACTORS;

        // This sound is played when the Fist hits a solid wall.
        SeeSound "rrwm/weapons/fist/punch";
        // This sound is played when the Fist hits an object or a monster.
        AttackSound "rrwm/weapons/fist/impact";
        // This sound is played when the Fist doesn't hit anything.
        ActiveSound "rrwm/weapons/fist/up";

        // Sparks are added in subclasses.
        RR_SparkPuff.SparkCount 0, 0;
    }

    States
    {
        // The Fist doesn't alert monsters unless it hits a wall
        // or some kind of object. Striking the air will not make
        // monsters go after you, unlike in vanilla Dooom.
        Crash:
        Melee:
            TNT1 A 1 A_AlertMonsters(0, AMF_EMITFROMTARGET);
            Stop;
    }
}
