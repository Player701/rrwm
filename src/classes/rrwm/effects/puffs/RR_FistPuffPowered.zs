//===========================================================================
//
// RR_FistPuffPowered
//
// Implements a puff for the powered up Fist, just to show off
// how cool it is. This puff is produced when the fist is powered,
// but not sped up (Strength is in effect but no Adrenaline).
//
//===========================================================================
class RR_FistPuffPowered : RR_FistPuff
{
    Default
    {
        RR_SparkPuff.SparkCount 1, 2;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.25;
        RR_SparkPuff.SparkScaleFactor 0.75, 1.0;
        RR_SparkPuff.SparkSpeed 2.0, 3.0;
    }
}
