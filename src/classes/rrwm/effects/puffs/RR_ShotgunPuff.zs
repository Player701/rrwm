//===========================================================================
//
// RR_ShotgunPuff
//
// A type of spark-based puff used by the Shotgun.
//
//===========================================================================
class RR_ShotgunPuff : RR_SparkPuff
{
    Default
    {
        RR_SparkPuff.SparkCount 1, 2;
        RR_SparkPuff.SparkLifeSpan 1.25, 1.75;
        RR_SparkPuff.SparkScaleFactor 1.25, 1.5;
        RR_SparkPuff.SparkSpeed 4.5, 6.0;
    }
}
