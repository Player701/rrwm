//===========================================================================
//
// RR_PistolPuff
//
// A type of spark-based puff used by the Pistol.
//
//===========================================================================
class RR_PistolPuff : RR_SparkPuff
{
    Default
    {
        RR_SparkPuff.SparkCount 1, 3;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.5;
        RR_SparkPuff.SparkScaleFactor 1.0, 1.5;
        RR_SparkPuff.SparkSpeed 4.0, 5.0;
    }
}
