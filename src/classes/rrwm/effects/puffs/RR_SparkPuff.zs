//===========================================================================
//
// RR_SparkPuff
//
// Implements a bullet puff represented by a group of sparks.
//
//===========================================================================
class RR_SparkPuff : RR_EffectBase
{
    // The minimum and maximum number of spawned sparks.
    private meta int SparkCountMin, SparkCountMax;

    // The minimum and maximum lifespan of spawned sparks.
    private meta double SparkLifeSpanMin, SparkLifeSpanMax;

    // The minimum and maximum scale factor of spawned sparks.
    private meta double SparkScaleFactorMin, SparkScaleFactorMax;

    // The minimum and maximum speed of spawned sparks.
    private meta double SparkSpeedMin, SparkSpeedMax;

    property SparkCount : SparkCountMin, SparkCountMax;
    property SparkLifeSpan : SparkLifeSpanMin, SparkLifeSpanMax;
    property SparkScaleFactor : SparkScaleFactorMin, SparkScaleFactorMax;
    property SparkSpeed : SparkSpeedMin, SparkSpeedMax;

    Default
    {
        +PUFFGETSOWNER;
        +HITTRACER;

        self.SparkCount 1, 1;
        self.SparkLifeSpan 1.0, 1.0;
        self.SparkScaleFactor 1.0, 1.0;
        self.SparkSPeed 1.0, 1.0;
    }

    //===========================================================================
    //
    // RR_SparkPuff::PostBeginPlay
    //
    // Spawns sparks at this actor's position. The number of sparks,
    // their lifespan and speed are determined by the values
    // of the corresponding properties.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // The PUFFGETSOWNER flag ensures that we have a target,
        // but we still should check if it exists.
        // We also need to check that we either didn't hit anything,
        // or we hit a non-bleeding actor, otherwise we don't spawn sparks.
        if (Target != null && (Tracer == null || Tracer.bNoBlood || Tracer.bDormant || Tracer.bInvulnerable))
        {
            // We just pass all parameters to RR_Spark::SpawnManyAt
            // which does the actual spawning.
            RR_Spark.SpawnManyAt(
                self, Target,
                Random[RR_SparkPuff__PostBeginPlay](SparkCountMin, SparkCountMax),
                SparkLifeSpanMin, SparkLifeSpanMax,
                SparkScaleFactorMin, SparkScaleFactorMax,
                SparkSpeedMin, SparkSpeedMax
            );
        }

        // We shouldn't stick around any longer once we've done our job.
        Destroy();
    }
}
