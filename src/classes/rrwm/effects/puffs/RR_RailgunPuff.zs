//===========================================================================
//
// RR_RailgunPuff
//
// A type of spark puff produced by the Railgun.
//
//===========================================================================
class RR_RailgunPuff : RR_SparkPuff
{
    Default
    {
        RR_SparkPuff.SparkCount 3, 5;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.25;
        RR_SparkPuff.SparkScaleFactor 1.5, 1.75;
        RR_SparkPuff.SparkSpeed 6.0, 9.0;
    }
}
