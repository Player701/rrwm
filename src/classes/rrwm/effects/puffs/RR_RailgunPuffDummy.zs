//===========================================================================
//
// RR_RailgunPuffDummy
//
// A dummy puff used with the A_RailAttack call in the Railgun.
//
// This is needed because it otherwise spawns a default puff
// whenever a dormant monster is encountered, and we don't want that.
//
//===========================================================================
class RR_RailgunPuffDummy : RR_EffectBase
{
    Default
    {
        -ALLOWPARTICLES;
    }

    //===========================================================================
    //
    // RR_RailgunPuffDummy::PostBeginPlay
    //
    // Do not persist the actor in the map.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Destroy();
    }
}
