//===========================================================================
//
// RR_Bubble
//
// A water bubble. These bubbles are spawned underwater instead of smoke.
//
//===========================================================================
class RR_Bubble : RR_MovingEffectBase
{
    Default
    {
        Height 4;
        RenderStyle "Translucent";
    }

    States
    {
        Spawn:
            BUBL A -1;
            Stop;
    }

    //===========================================================================
    //
    // RR_Bubble::BeginPlay
    //
    // Sets a random vertical velocity, translucency and scale.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        // This is literally the sole reason
        // the effect looks that great, IMO.
        Vel.z = FRandom[RR_Bubble__BeginPlay](0.5, 1) / SafeGetGravity();
        Alpha = FRandom[RR_Bubble__BeginPlay](0.25, 0.35);
        A_SetScale(FRandom[RR_Bubble__BeginPlay](0.05, 0.15));
    }

    //===========================================================================
    //
    // RR_Bubble::Tick
    //
    // Destroys itself if not underwater, or if near the ceiling.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (WaterLevel < 3 || !ShouldFall())
        {
            Destroy();
        }
    }
}
