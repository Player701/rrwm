//===========================================================================
//
// RR_LaserDot
//
// Represents a laser dot for the pistol's laser sight.
//
//===========================================================================
class RR_LaserDot : RR_EffectBase
{
    Default
    {
        RenderStyle "Add";
    }

    States
    {
        Spawn:
            LASR A -1 BRIGHT;
            Stop;
    }
}
