//===========================================================================
//
// RR_RocketExhaust
//
// This is the "back" part of the Striker Missile
// that travels along with it.
//
//===========================================================================
class RR_RocketExhaust : RR_EffectBase
{
    Default
    {
        Alpha 0.75;
        Scale 0.75;
        RenderStyle "Add";
    }

    States
    {
        Spawn:
            RXST A -1 BRIGHT Light("RR_ROCKET");
            Stop;
    }

    //===========================================================================
    //
    // RR_RocketExhaust::Tick
    //
    // Destroys this actor if it has no target assigned.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        let rocket = RR_Rocket(Target);

        // Don't stick around if spawned manually.
        // This will also trigger if the rocket gets suddenly destroyed
        // for some reason, e.g. by hitting sky.
        if (rocket == null)
        {
            Destroy();
        }
        else
        {
            // Signify that the exhaust's tick has been processed.
            // If the rocket has also ticked, a synchronization check is due.
            rocket.SyncExhaust(self);
        }
    }
}
