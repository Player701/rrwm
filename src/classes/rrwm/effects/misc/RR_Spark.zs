//===========================================================================
//
// RR_Spark
//
// A spark which is spawned when a bullet hits a solid object
// or when a rocket explodes.
// Sparks are spawned in groups via the SpawnManyAt static method.
//
//===========================================================================
class RR_Spark : RR_MovingEffectBase
{
    // The life span of the spark.
    // 1 unit of life span equals 8 ticks.
    private double _lifeSpan;

    Default
    {
        -NOGRAVITY;
        Scale 0.04;
        RenderStyle "Add";
    }

    States
    {
        Spawn:
            BPUF ABCD 1 BRIGHT;
            Loop;
    }

    //===========================================================================
    //
    // RR_Spark::SpawnAt
    //
    // Spawns a spark at the provided position and sets it lifespan
    // to the given value.
    //
    //===========================================================================
    static RR_Spark SpawnAt(vector3 pos, double lifeSpan)
    {
        let spark = RR_Spark(Spawn('RR_Spark', pos));

        if (spark != null)
        {
            spark.SetLifeSpan(lifeSpan);
        }

        return spark;
    }

    //===========================================================================
    //
    // RR_Spark::SpawnManyAt
    //
    // Spawns sparks at the given actor's location, randomizing their life span
    // and initial velocities. Directions the sparks will fly at are also
    // randomized.
    //
    //===========================================================================
    static void SpawnManyAt(
        Actor source, Actor shooter, int count,
        double minLifeSpan, double maxLifeSpan,
        double minScaleFactor, double maxScaleFactor,
        double minSpeed, double maxSpeed)
    {
        let pos = source.Pos;
        double height = GetDefaultByType('RR_Spark').Height;

        // Check if the source actor's position would make the sparks get stuck
        // and try to avoid this situation if possible.
        if (pos.z + height >= source.CeilingZ)
        {
            pos = (pos.x, pos.y, source.CeilingZ - height);
        }

        // Calculate the rotation parameters once
        // before spawning all sparks.
        double baseYaw = GetBaseYaw(source, shooter);
        double basePitch = GetBasePitch(source, shooter);
        double baseRoll = GetBaseRoll(source, shooter);

        for (int i = 0; i < count; i++)
        {
            SpawnRandomizedAt(
                pos,
                baseYaw, basePitch, baseRoll,
                minLifeSpan, maxLifeSpan,
                minScaleFactor, maxScaleFactor,
                minSpeed, maxSpeed
            );
        }
    }

    //===========================================================================
    //
    // RR_Spark::SpawnRandomizedAt
    //
    // This method does the actual spawning. Uses the shooter actor as reference
    // to set up the correct plane the angle of the spark's initial velocity
    // will be relative to. The life span and the initial speed are generated
    // randomly within the provided ranges.
    //
    //===========================================================================
    private static void SpawnRandomizedAt(
        vector3 pos,
        double baseYaw, double basePitch, double baseRoll,
        double minLifeSpan, double maxLifeSpan,
        double minScaleFactor, double maxScaleFactor,
        double minSpeed, double maxSpeed)
    {
        // Generate a random angle and a pitch.
        // These are relative to the plane defined by the shooter's pitch, yaw and roll.
        double sparkYaw = FRandom[RR_Spark__SpawnRandomizedAt](-75, 75);
        double sparkPitch = FRandom[RR_Spark__SpawnRandomizedAt](-75, 75);

        // Generate a random speed and life span.
        double sparkSpeed = FRandom[RR_Spark__SpawnRandomizedAt](minSpeed, maxSpeed);
        double sparkLifeSpan = FRandom[RR_Spark__SpawnRandomizedAt](minLifeSpan, maxLifeSpan);

        // Calculate the initial velocity vector.
        let sparkVel = RR_Math.RotateVector((sparkSpeed, 0, 0), sparkYaw, sparkPitch, 0);

        // Transform to shooter's plane and negate
        // because the spark flies towards the shooter, not away from them.
        sparkVel = -RR_Math.RotateVector(sparkVel, baseYaw, basePitch, baseRoll);

        // Finally, spawn the spark and assign its velocity.
        let spark = SpawnAt(pos, sparkLifeSpan);

        if (spark != null)
        {
            spark.Vel = sparkVel;
            spark.Scale *= FRandom[RR_Spark__SpawnRandomizedAt](minScaleFactor, maxScaleFactor);
        }
    }

    //===========================================================================
    //
    // RR_Spark::GetBaseYaw
    //
    // Gets the base yaw value for spark velocity transformation.
    //
    //===========================================================================
    private static double GetBaseYaw(Actor source, Actor shooter)
    {
        // Special case: If shooter is present and not the same as the source,
        // check if they are at the same 2D position,
        // and use the shooter's angle in this case.
        // This is because bullet puffs do not set their angle correctly
        // if they are spawned directly above the shooter.
        if (shooter != null && shooter != source)
        {
            double dist = shooter.Distance2D(source);

            if (RR_Math.IsAlmostZero(dist))
            {
                return shooter.Angle;
            }
        }

        // If the source is a missile, take the angle of the source,
        // otherwise take the angle of the source and invert it
        // (because bullet puffs face the source).
        return source.bMissile ? source.Angle : source.Angle + 180;
    }

    //===========================================================================
    //
    // RR_Spark::GetBasePitch
    //
    // Gets the base pitch value for spark velocity transformation.
    //
    //===========================================================================
    private static double GetBasePitch(Actor source, Actor shooter)
    {
        // If the source is a missile, get its pitch.
        // RR_Projectile calculates it based on its velocity.
        if (source.bMissile || shooter == null)
        {
            return source.Pitch;
        }

        // The following calculations are from GZDoom's source code
        // (inline double AActor::AttackOffset)
        double zOffset = shooter is 'PlayerPawn' && shooter.player != null
            ? PlayerPawn(shooter).AttackZOffset * shooter.player.crouchfactor
            : 8;

        // Add half of shooter's height because the offset is from the center
        zOffset += RR_Math.GetNonZeroValue(shooter.Height, shooter.Default.Height) / 2;

        // NB: PitchTo accounts for floorclip
        // (the source, being a puff or missile, does not have any)
        return shooter.PitchTo(source, zOffset);
    }

    //===========================================================================
    //
    // RR_Spark::GetBaseRoll
    //
    // Gets the base roll value for spark velocity transformation.
    //
    //===========================================================================
    private static double GetBaseRoll(Actor source, Actor shooter)
    {
        // If the source is a missile, use its roll,
        // otherwise use the shooter's roll.
        return source.bMissile || shooter == null
            ? source.Roll
            : shooter.Roll;
    }

    //===========================================================================
    //
    // RR_Spark::SetLifeSpan
    //
    // Sets the life span of this spark to the provided value.
    //
    //===========================================================================
    private void SetLifeSpan(double lifeSpan)
    {
        if (lifeSpan <= 0)
        {
            ThrowAbortException("%s: Life span must be positive.", GetClassName());
        }

        _lifeSpan = lifeSpan;
    }

    //===========================================================================
    //
    // RR_Spark::BeginPlay
    //
    // Initializes the lifespan value in case it is not initialized later on.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        SetLifeSpan(1);
    }

    //===========================================================================
    //
    // RR_Spark::Tick
    //
    // Decreases the spark's transparency value
    // proportionally to its lifespan.
    // When the spark becomes fully invisible, it is destroyed.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        if (GetAge() > 0)
        {
            Alpha -= 0.125 / _lifeSpan;

            if (Alpha <= 0)
            {
                Destroy();
            }
        }
    }
}
