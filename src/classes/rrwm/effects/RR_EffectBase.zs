//===========================================================================
//
// RR_EffectBase
//
// Base class for non-interactive effect actors.
//
//===========================================================================
class RR_EffectBase : Actor abstract
{
    Default
    {
        +NOINTERACTION;
        +NOTONAUTOMAP;
        +FORCEXYBILLBOARD;
    }

    //===========================================================================
    //
    // RR_EffectBase::ShouldFall
    //
    // Checks whether this object should be in free-fall.
    //
    //===========================================================================
    protected bool ShouldFall()
    {
        if (bNoGravity || bNoInteraction)
        {
            // For no-gravity actors, this depends on velocity only
            return Vel.Z > 0
                ? Pos.Z + Height < CeilingZ - Height
                : Pos.Z > FloorZ + Height;
        }

        // Otherwise, it depends on gravity
        // NB: negative gravity IS a thing, even if it's not used often.
        double grav = GetGravity();

        return grav > 0
            ? Pos.Z > FloorZ + Height
            : Pos.Z + Height < CeilingZ - Height;
    }

    //===========================================================================
    //
    // RR_EffectBase::SpawnFromSourceWeapon
    //
    // Spawns an effect object using a 2D offset relative to the provided actor,
    // making it look as if it comes out from that actor's weapon.
    //
    //===========================================================================
    static RR_EffectBase SpawnFromSourceWeapon(
        class<RR_EffectBase> spawnClass,
        Actor source, double radiusFactor, double spawnHeight,
        vector2 spawnOffset, vector3 spawnVelocity, vector3 velocitySpread)
    {
        let [spawnPos, spawnVel] = RR_Math.GetPosAndVelForWeaponObject(
            source, radiusFactor, spawnHeight,
            spawnOffset, spawnVelocity, velocitySpread
        );

        let obj = RR_EffectBase(Spawn(spawnClass, spawnPos));

        if (obj != null && obj.bMissile)
        {
            // If the object is a projectile, we set its target
            // so that it doesn't collide with the source.
            obj.Target = source;
            obj.Vel = spawnVel;
        }

        return obj;
    }
}
