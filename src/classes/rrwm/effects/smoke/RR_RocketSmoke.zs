//===========================================================================
//
// RR_RocketSmoke
//
// The smoke which is spawned by a Striker rocket in flight.
//
//===========================================================================
class RR_RocketSmoke : RR_SmokeBase
{
    Default
    {
        Health 7;
        Height 6;

        Alpha 0.1;
        Scale 0.35;

        RR_SmokeBase.VelRange 0.9, 1.1;
    }

    States
    {
        Death:
            CASM BCD 1;
            CASM E 1 A_FadeOut;
            Wait;
    }
}
