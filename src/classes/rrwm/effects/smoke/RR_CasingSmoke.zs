//===========================================================================
//
// RR_CasingSmoke
//
// Smoke effect for casings.
//
//===========================================================================
class RR_CasingSmoke : RR_SmokeBase
{
    Default
    {
        Health 35;
        Height 2;

        Alpha 0.4;
        Scale 0.075;

        RR_SmokeBase.VelRange 0.4, 0.6;
    }

    States
    {
        Death:
            CASM BCD 2;
            CASM E 2 A_FadeOut;
            Wait;
    }
}
