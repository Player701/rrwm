//===========================================================================
//
// RR_ChainsawSmoke
//
// Smoke effect for the chainsaw.
//
//===========================================================================
class RR_ChainsawSmoke : RR_SmokeBase
{
    Default
    {
        Health 70;
        Height 5;

        RenderStyle "Translucent";
        Alpha 0.5;
        Scale 0.25;

        RR_SmokeBase.VelRange 0.2, 0.4;
    }

    States
    {
        Death:
            CASM A 3 A_ChainsawSmokeFade;
            Wait;
    }

    //===========================================================================
    //
    // RR_ChainsawSmoke::A_ChainsawSmokeFade
    //
    // Slowly fades out from existence, but also grows in size.
    //
    //===========================================================================
    void A_ChainsawSmokeFade()
    {
        A_FadeOut(0.05);
        A_SetScale(Scale.X * 1.05, Scale.Y * 1.05);
    }
}
