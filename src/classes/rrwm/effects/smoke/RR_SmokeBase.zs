//===========================================================================
//
// RR_SmokeBase
//
// Base class for smoke actors.
// Their health is counted down each tick until they die.
// They also die if they hit the ceiling or a water surface.
// They're supposed to be constantly moving upwards.
//
//===========================================================================
class RR_SmokeBase : RR_MovingEffectBase abstract
{
    // Vertical speed range for random adjustment.
    private meta double VelMin, VelMax;

    property VelRange: VelMin, VelMax;

    Default
    {
        RenderStyle "Add";
    }

    States
    {
        Spawn:
            CASM A -1;
            Stop;
    }

    //===========================================================================
    //
    // RR_SmokeBase::BeginPlay
    //
    // Sets a random vertical speed.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        Vel.Z = FRandom[RR_SmokeBase__BeginPlay](VelMin, VelMax) / SafeGetGravity();
    }

    //===========================================================================
    //
    // RR_SmokeBase::PostBeginPlay
    //
    // Replaces this smoke object with a bubble if spawned underwater.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // Smoke can't exist underwater and is replaced by bubbles instead.
        if (WaterLevel > 2)
        {
            Spawn('RR_Bubble', Pos, ALLOW_REPLACE);
            Destroy();
        }
    }

    //===========================================================================
    //
    // RR_SmokeBase::Tick
    //
    // Counts down the actor's health. If it reaches 0,
    // the actor is put into its Death state.
    // This also happens if the actor hits the ceiling or a water surface.
    //
    //===========================================================================
    override void Tick()
    {
        Super.Tick();

        // Do the checks only if we're not dead.
        if (Health > 0)
        {
            // Check if we're underwater or if we've hit the ceiling.
            if (WaterLevel > 0 || !ShouldFall())
            {
                Health = 0;
                Vel.z = 0;
            }
            else
            {
                // If not, just count down our health.
                Health--;
            }

            // Go to the Death state if health is zero.
            if (Health == 0)
            {
                SetStateLabel('Death');
            }
        }
    }
}
