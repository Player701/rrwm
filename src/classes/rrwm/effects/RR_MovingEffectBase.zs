//===========================================================================
//
// RR_MovingEffectBase
//
// Base class for effect actors that move.
//
//===========================================================================
class RR_MovingEffectBase : RR_EffectBase abstract
{
    Default
    {
        // These parameters are important.
        // The weapon object spawner expects the objects
        // to have this size, as it otherwise cannot ensure
        // that the objects spawned will be within
        // the source actor's bounding box.
        // Sparks also need them, as they actually collide with walls
        // and their sprites are small.
        Radius 1;
        Height 2;

        -NOINTERACTION;
        +DROPOFF;
        +NOBLOCKMAP;
        +NOGRAVITY;
        +NOTELEPORT;
        +THRUACTORS;
        +WINDTHRUST;
    }

    //===========================================================================
    //
    // RR_MovingEffectBase::SafeGetGravity
    //
    // Returns the current gravity regardless of the NOGRAVITY flag.
    //
    //===========================================================================
    protected double SafeGetGravity()
    {
        bool oldNoGravity = bNoGravity;
        bNoGravity = false;

        double result = GetGravity();
        bNoGravity = oldNoGravity;

        return result;
    }

    //===========================================================================
    //
    // RR_MovingEffectBase::BeginPlay
    //
    // Ensures that the actor has some horizontal velocity.
    //
    //===========================================================================
    override void BeginPlay()
    {
        // Add minimal horizontal velocity.
        // This is needed because otherwise the CeilingZ value is not updated.
        Vel.x = MinVel;

        Super.BeginPlay();
    }
}
