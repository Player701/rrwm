//===========================================================================
//
// RR_Obituary
//
// Provides a static method to localize and format obituaries.
//
//===========================================================================
class RR_Obituary
{
    //===========================================================================
    //
    // RR_Obituary::Select
    //
    // Selects the correct obituary based on the nature of the source of damage
    // (the object), and also the nature of the killer (the subject).
    //
    //===========================================================================
    static string Select(string obit, Actor obj, Actor subj)
    {
        // obj - the object used to kill the target (i.e. weapon or projectile)
        // subj - the subject who killed the target (i.e. monster or player)

        // Is the subject is a monster?
        bool isMonsterAttack = subj != null && subj.player == null;

        string result = obit;

        // If the obituary is non-localizable, skip the entire process.
        if (RR_StringTools.IsLabel(obit))
        {
            // Store the list of possible matches here.
            // Non-localized matches will be discarded.
            Array<string> matches;

            // Check the normal obituary (for player attacks).
            matches.Push(StringTable.Localize(obit));

            // is the subject a monster?
            if (isMonsterAttack)
            {
                // Check the monster obituary.
                matches.Push(RR_StringTools.MaybeLocalize(obit .. "_MONSTER"));

                // Is the subject a stealth monster?
                if (subj.bStealth)
                {
                    // Check the stealth monster obituary.
                    matches.Push(RR_StringTools.MaybeLocalize(obit .. "_MONSTER_STEALTH"));
                }
            }

            // Get the best match by iterating over the array in reverse order.
            // Non-localized matches are skipped.
            int i = matches.Size() - 1;

            while (i > 0 && RR_StringTools.IsLabel(matches[i]))
            {
                i--;
            }

            // This is our result...
            result = matches[i];
        }

        // ... but if the subject is a monster,
        // also substitute "%k" for its tag.
        // For player attacks, the engine does this for us.
        if (isMonsterAttack)
        {
            result.Replace("%k", subj.GetTag());
        }

        // ...yeah.
        return result;
    }
}
