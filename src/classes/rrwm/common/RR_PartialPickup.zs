//===========================================================================
//
// RR_PartialPickup
//
// Implements common methods for partial pickup handling.
//
//===========================================================================
mixin class RR_PartialPickup
{
    //===========================================================================
    //
    // RR_PartialPickup::IsPartialPickupEnabled
    //
    // Checks if partial pickup is enabled for this item.
    //
    //===========================================================================
    private bool IsPartialPickupEnabled() const
    {
        return RR_Settings.InPlay(RR_SETTING_PARTIALPICKUP).IsOn() && !ShouldStay() && !WillRespawn() && GetAge() > 0;
    }

    //===========================================================================
    //
    // RR_PartialPickup::SpawnResidualItem
    //
    // Spawns an inventory item of the provided type at the specified location.
    // Sets its amount to the provided value and copies the DROPPED flag
    // from the this item.
    //
    //===========================================================================
    private Inventory SpawnResidualItem(class<Inventory> itemType, int itemAmount, vector3 spawnPos, bool addRandomVel)
    {
        let newItem = Inventory(Actor.Spawn(itemType, spawnPos));

        if (newItem != null)
        {
            newItem.Vel = Vel;
            newItem.Angle = Angle;
            newItem.Pitch = Pitch;
            newItem.Roll = Roll;
            newItem.FloatBobPhase = FloatBobPhase;

            newItem.Amount = itemAmount;
            newItem.bIgnoreSkill = true;
            newItem.bDropped = bDropped;

            if (addRandomVel)
            {
                newItem.Vel += GenRandomVelocity();
            }
        }

        return newItem;
    }

    //===========================================================================
    //
    // RR_PartialPickup::GenRandomVelocity
    //
    // Generates a random velocity for spawned residual ammo pickups.
    //
    //===========================================================================
    private vector3 GenRandomVelocity()
    {
        double ang = FRandom[RR_PartialPickup__GenRandomVelocity](0, 360);
        double coeff = FRandom[RR_PartialPickup__GenRandomVelocity](0, 1);

        return (Cos(ang), Sin(ang), 0) * (coeff + 0.15) * Radius / 15;
    }

    //===========================================================================
    //
    // RR_PartialPickup::WillRespawn
    //
    // Checks if this item can (and will eventually) respawn.
    //
    //===========================================================================
    private bool WillRespawn() const
    {
        return !bDropped && ShouldRespawn();
    }

    //===========================================================================
    //
    // RR_PartialPickup::GetAmountOfItem
    //
    // Returns the amount of the item owned by the specified actor,
    // or 0 if there is no such item available.
    //
    //===========================================================================
    private static int GetAmountOfItem(Actor toucher, class<Inventory> what)
    {
        let found = toucher.FindInventory(what);
        return found != null ? found.Amount : 0;
    }

    //===========================================================================
    //
    // RR_PartialPickup::AdjustAmmoBySkill
    //
    // Adjusts the provided ammo amount by the skill factor, if applicable.
    //
    //===========================================================================
    private static int AdjustAmmoBySkill(int amt)
    {
        return int(amt * G_SkillPropertyFloat(SKILLP_AmmoFactor) * sv_ammofactor);
    }
}
