//===========================================================================
//
// RR_Math
//
// Contains some helper math functions
//
//===========================================================================
class RR_Math
{
    // This is used to compare floating point numbers
    const EPS = 1e-5;

    //===========================================================================
    //
    // RR_Math::AreAlmostEqual
    //
    // Checks if two values are almost equal to each other.
    //
    //===========================================================================
    static bool AreAlmostEqual(double x, double y)
    {
        return Abs(x - y) < EPS;
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::AreAlmostEqualV
    //
    // Checks if two vectors are almost equal to each other.
    //
    //===========================================================================
    static bool AreAlmostEqualV(vector3 v1, vector3 v2)
    {
        return AreAlmostEqual(v1.x, v2.x)
            && AreAlmostEqual(v1.y, v2.y)
            && AreAlmostEqual(v1.z, v2.z);
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::GetNonZeroValue
    //
    // Returns the first value if it's non-zero,
    // otherwise returns the second value.
    //
    //===========================================================================
    static double GetNonZeroValue(double value, double valueIfZero)
    {
        return IsAlmostZero(value) ? valueIfZero : value;
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::GetPosAndVelForWeaponObject
    //
    // Given the initial offset in 2D, velocity and its spread,
    // returns initial position in 3D and a randomized velocity
    // for an object to be spawned from an actor's weapon,
    // in absolute coordinates.
    //
    //===========================================================================
    static vector3, vector3 GetPosAndVelForWeaponObject(
        Actor source, double radiusFactor, double spawnHeight,
        vector2 spawnOffset, vector3 spawnVelocity, vector3 velocitySpread)
    {
        // Calculate the position and the velocity.
        // NB! Depending on the pitch, the resulting position, as seen on the screen, will differ.
        // It is also known that it depends on which renderer is used (software v. OpenGL),
        // so attempting to approximate it is probably not worth it.
        let spawnPos = GetOffsetForWeaponObject(spawnOffset, source, radiusFactor);
        spawnPos = OffsetToPosition(spawnPos, source);

        let spawnVel = RandomizeVector(spawnVelocity, velocitySpread);
        spawnVel = GetRotatedOffset(spawnVel, source);

        return (spawnPos.X, spawnPos.Y, spawnPos.Z + spawnHeight), spawnVel;
    }

    //===========================================================================
    //
    // RR_Math::GetRelativeViewZ
    //
    // Gets the actual (for players) or the supposed (for monsters)
    // view height of the source actor. Used when spawning debris objects
    // from the source actor's weapon.
    //
    //===========================================================================
    static double GetRelativeViewZ(Actor source)
    {
        let player = source.player;

        // Is it a player? Then get its ViewZ value
        // and subtract the Z coordinate of the position vector
        // to get the relative value.
        if (player != null)
        {
            // NB! Player's ViewZ value accounts for FloorClip.
            return player.ViewZ - source.Pos.Z;
        }

        // Otherwise, calculate the view height proportionally
        // based on the player's default view height.
        let h = GetNonZeroValue(source.Height, source.Default.Height);
        let def = GetDefaultByType('RR_Player');

        return h * def.ViewHeight / def.Height - source.FloorClip;
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::GetRotatedOffset
    //
    // Given a vector in the coordinate system
    // relative to an actor's position, yaw, pitch and roll,
    // transforms it into a vector in the coordinate system
    // relative to just the actor's position (i.e. a position offset).
    //
    // Example purposes of this function:
    //
    // - transform the result to absolute coordinates
    //   to spawn something directly (via Spawn);
    //
    // - use the result as a velocity;
    //
    // - use the result as an offset passed to another spawn function
    //   (i.e. A_SpawnParticle)
    //
    //===========================================================================
    static vector3 GetRotatedOffset(vector3 v, Actor origin)
    {
        return RotateVector(v, origin.Angle, origin.Pitch, origin.Roll);
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::GetOffsetForWeaponObject
    //
    // Given an offset in 2D coordinates, returns a 3D vector
    // representing the position that would make a spawned object
    // appear to have come out of a weapon the actor is carrying.
    // For players, this means the object will be seen on the screen
    // if the Y value is not too low.
    //
    //===========================================================================
    static vector3 GetOffsetForWeaponObject(vector2 v, Actor source, double radiusFactor)
    {
        // Radius may be 0 for ghost monsters.
        // In this case, fall back to actor defaults of the source.
        double radius = GetNonZeroValue(
            source.Radius,
            source.Default.Radius
        );

        // Ensure the object does not spawn more than 1.5 units from the edge
        // of the source's collision box.
        // Radius factor is usually 1, only the Spider Mastermind uses
        // a factor of 0.5 because its radius is much bigger than it should be.
        double dist = radius * radiusFactor - 1.5;
        double resultX = -1;

        if (dist > 0)
        {
            // We need to find such X for the resulting vector
            // that would make its length equal to the desired distance.
            double sqr = dist * dist - v.LengthSquared();

            // NB: ensure the square is positive
            if (sqr > 0)
            {
                resultX = Sqrt(sqr);
            }
        }

        if (resultX < 0)
        {
            Console.Printf("RR_Math.GetOffsetForWeaponObject: unsafe 2D offsets (%f, %f) for source with radius %f", v.X, v.Y, radius);
            resultX = 0;
        }

        return (resultX, -v.X, v.Y);
    }

    //===========================================================================
    //
    // RR_Math::OffsetToPosition
    //
    // Same as GetRotatedOffset, but returns absolute coordinates.
    //
    //===========================================================================
    static vector3 OffsetToPosition(vector3 v, Actor origin)
    {
        let offset = GetRotatedOffset(v, origin);
        return origin.Vec3Offset(offset.x, offset.y, offset.z);
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::IsAlmostZero
    //
    // Checks if the value is almost zero.
    //
    //===========================================================================
    static bool IsAlmostZero(double x)
    {
        return AreAlmostEqual(x, 0);
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::IsAlmostZeroV
    //
    // Checks if the vector's length is almost zero.
    //
    //===========================================================================
    static bool IsAlmostZeroV(vector3 v)
    {
        return IsAlmostZero(v.Length());
    }

    //===========================================================================
    //
    // RR_Math::LogisticCurve
    //
    // Returns the value of a logistic function.
    // The function is defined to be almost 0 with x approaching 0.
    // As x approaches mid, the value approaches 0.5, and when x == mid,
    // the value is exactly 0.5. Larges values of x result in values closer to 1.
    //
    //===========================================================================
    static double LogisticCurve(double x, double mid)
    {
        double e = Exp(8*(x / mid - 1));
        return 1.0 / (1.0 + 1.0/e);
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::RotateVector
    //
    // This function does the real work for GetRotatedOffset,
    // it can also be used independently if the values needed
    // to transform the vector are not part of an actor.
    //
    //===========================================================================
    static vector3 RotateVector(vector3 v, double yaw, double pitch, double roll)
    {
        double sinAlpha = Sin(yaw);
        double cosAlpha = Cos(yaw);
        double sinBeta = Sin(pitch);
        double cosBeta = Cos(pitch);
        double sinGamma = Sin(roll);
        double cosGamma = Cos(roll);

        // These are the rows of the rotation matrix, not the columns.
        // They are in this form just for readability.

        let a1 = (
            cosAlpha * cosBeta,
            cosAlpha * sinBeta * sinGamma - sinAlpha * cosGamma,
            cosAlpha * sinBeta * cosGamma + sinAlpha * sinGamma
        );

        let a2 = (
            sinAlpha * cosBeta,
            sinAlpha * sinBeta * sinGamma + cosAlpha * cosGamma,
            sinAlpha * sinBeta * cosGamma - cosAlpha * sinGamma
        );

        let a3 = (
           -sinBeta,
           cosBeta * sinGamma,
           cosBeta * cosGamma
        );

        return (v dot a1, v dot a2, v dot a3);
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::RandomizeVector
    //
    // Randomizes the vector by adding a random value
    // generated in the specified ranges to each component.
    //
    //===========================================================================
    static vector3 RandomizeVector(vector3 v, vector3 spread)
    {
        return (
            v.X + FRandom[RR_Math__RandomizeVector](-spread.X, spread.X),
            v.Y + FRandom[RR_Math__RandomizeVector](-spread.Y, spread.Y),
            v.Z + FRandom[RR_Math__RandomizeVector](-spread.Z, spread.Z)
        );
    }

    //===========================================================================
    //
    // RR_Math::RandomRoll
    //
    // Returns true if a random value between 0 and 1 turns out
    // to be less or equal to the provided chance.
    //
    //===========================================================================
    static bool RandomRoll(double chance)
    {
        // FRandom's upper bound is exclusive.
        // This has been verified by inspecting GZDoom's sources.
        // Therefore, only one "<" check is required:
        // for 0, it will always fail, and for 1 it will always succeed.
        return FRandom[RR_Math__RandomRoll](0, 1) < chance;
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_Math::TicsToSeconds
    //
    // Converts the provided amount of time in tics
    // to an equivalent amount of time in seconds.
    //
    //===========================================================================
    static int TicsToSeconds(int tics)
    {
        return int(Ceil(tics / double(GameTicRate)));
    }
}
