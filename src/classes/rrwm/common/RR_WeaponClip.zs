//===========================================================================
//
// RR_WeaponClip
//
// This class is used to abstract away weapon clip logic.
// This is used both in reloadable weapons and monster replacements.
//
//===========================================================================
class RR_WeaponClip
{
    // How much ammo is used for a single shot.
    private int _ammoUse;
    // Current amount of ammo in the clip.
    private int _amount;
    // Capacity of the clip. Not changed.
    private int _capacity;

    //============================================================================
    //
    // RR_WeaponClip::Create
    //
    // Creates a new empty clip with the specified capacity and ammo usage.
    // By default, the clip uses 1 ammo.
    //
    //============================================================================
    static RR_WeaponClip Create(int capacity, int ammoUse = 1)
    {
        let clip = new('RR_WeaponClip');
        clip.Init(capacity, ammoUse);

        return clip;
    }

    //===========================================================================
    //
    // RR_WeaponClip::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(int capacity, int ammoUse)
    {
        // Check for bad clip capacity values
        if (capacity <= 0)
        {
            ThrowAbortException("%s: Capacity must be positive.", GetClassName());
        }

        // Check for bad ammo usage values
        if (ammoUse <= 0)
        {
            ThrowAbortException("%s: Ammo usage must be positive.", GetClassName());
        }

        // To work correctly, capacity must be a multiple of ammoUse.
        if (capacity % ammoUse > 0)
        {
            ThrowAbortException("%s: Capacity must be a multiple of ammo usage.", GetClassName());
        }

        _capacity = capacity;
        _ammoUse = ammoUse;
    }

    //===========================================================================
    //
    // RR_WeaponClip::GetAmmoUse
    //
    // Returns the amount of ammo this clip uses for a single shot.
    //
    //===========================================================================
    int GetAmmoUse() const
    {
        return _ammoUse;
    }

    //============================================================================
    //
    // RR_WeaponClip::GetAmount
    //
    // Returns the current amount of ammo in this clip.
    //
    //============================================================================
    int GetAmount() const
    {
        return _amount;
    }

    //============================================================================
    //
    // RR_WeaponClip::GetCapacity
    //
    // Returns the capacity of this clip.
    //
    //============================================================================
    int GetCapacity() const
    {
        return _capacity;
    }

    //===========================================================================
    //
    // RR_WeaponClip::IsFull
    //
    // Checks if this clip is full.
    //
    //===========================================================================
    bool IsFull() const
    {
        return _amount == _capacity;
    }

    //============================================================================
    //
    // RR_WeaponClip::CanLoadAmmo
    //
    // Checks if ammo can be loaded in the clip based on the amount provided.
    //
    //============================================================================
    bool CanLoadAmmo(int amount) const
    {
        return !IsFull() && amount >= _ammoUse;
    }

    //===========================================================================
    //
    // RR_WeaponClip::CheckAmmo
    //
    // Checks whether the clip has enough ammo in it.
    //
    //===========================================================================
    bool CheckAmmo(int ammouse = -1) const
    {
        return _amount >= NormalizeAmount(GetAmmoUseFromArg(ammouse));
    }

    //============================================================================
    //
    // RR_WeaponClip::DepleteAmmo
    //
    // Subtracts ammoUse amount of ammo from the clip,
    // but only if there is enough ammo.
    //
    //============================================================================
    bool DepleteAmmo(int ammouse = -1)
    {
        ammouse = NormalizeAmount(GetAmmoUseFromArg(ammouse));

        if (_amount >= ammouse)
        {
            _amount -= ammouse;
            return true;
        }

        return false;
    }

    //============================================================================
    //
    // RR_WeaponClip::LoadAmmo
    //
    // Loads ammo into the clip based on the total amount of ammo provided.
    // Returns the amount of ammo that was loaded.
    //
    //============================================================================
    int LoadAmmo(int amount)
    {
        // Make sure we don't load an uneven amount of ammo.
        amount = NormalizeAmount(amount);

        int loaded = Min(_capacity - _amount, amount);
        _amount += loaded;

        return loaded;
    }

    //============================================================================
    //
    // RR_WeaponClip::UnloadAmmo
    //
    // Unloads all ammo from the clip.
    // Returns the amount of ammo that was unloaded.
    //
    //============================================================================
    int UnloadAmmo()
    {
        int unloaded = _amount;
        _amount = 0;

        return unloaded;
    }

    //===========================================================================
    //
    // RR_WeaponClip::GetAmmoUseFromArg
    //
    // If the provided value is positive, returns it unchanged.
    // If the value is equal to -1, returns the default ammo usage value.
    // Otherwise, throws an exception.
    //
    //===========================================================================
    private int GetAmmoUseFromArg(int ammouse)
    {
        if (ammouse == -1)
        {
            ammouse = _ammoUse;
        }
        else if (ammouse <= 0)
        {
            ThrowAbortException("%s: Ammo usage must be positive.", GetClassName());
        }

        return ammouse;
    }

    //===========================================================================
    //
    // RR_WeaponClip::NormalizeAmount
    //
    // Normalizes the provided amount value to be a multiple of ammouse.
    //
    //===========================================================================
    private int NormalizeAmount(int amount) const
    {
        return amount - amount % _ammoUse;
    }
}
