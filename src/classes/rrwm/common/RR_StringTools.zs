//===========================================================================
//
// RR_StringTools
//
// Contains some non-specific methods for string manupulation.
//
//===========================================================================
class RR_StringTools
{
    //===========================================================================
    //
    // RR_StringTools::TryLocalize
    //
    // Attempts to localize the provided string. If successful, the result
    // is put in the out parameter. Returns true if the localization
    // has succeeded, and false otherwise.
    //
    //===========================================================================
    static bool TryLocalize(string label, out string localized)
    {
        if (!IsLabel(label))
        {
            ThrowAbortException("RR_StringTools: Invalid label.");
        }

        localized = StringTable.Localize(label);
        return label.Mid(1) != localized;
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_StringTools::MaybeLocalize
    //
    // Tries to localize a string if it is localizable, and returns
    // the localized string on success. If the string cannot be localized,
    // returns the original string instead.
    //
    //===========================================================================
    static string MaybeLocalize(string label)
    {
        string localized;
        return TryLocalize(label, localized) ? localized : label;
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_StringTools::TryStripPrefix
    //
    // If the string starts with the provided prefix, removes it,
    // puts the result in the out parameter, and returns true.
    // Otherwise, returns false.
    //
    //===========================================================================
    static bool TryStripPrefix(string str, string prefix, out string result)
    {
        bool hasPrefix = str.IndexOf(prefix) == 0;

        if (hasPrefix)
        {
            result = str.Mid(prefix.Length());
        }

        return hasPrefix;
    }

    //===========================================================================
    //
    // RR_StringTools::MaybeStringPrefix
    //
    // If the string starts with the provided prefix, removes it and returns
    // the result. Otherwise, returns the original string without changes.
    //
    //===========================================================================
    static string MaybeStringPrefix(string str, string prefix)
    {
        string result;
        return TryStripPrefix(str, prefix, result) ? result : str;
    }

    //===========================================================================
    //
    // RR_StringTools::IsLabel
    //
    // Checks if the provided string is a placeholder intended to be localized.
    //
    //===========================================================================
    static bool IsLabel(string str)
    {
        return str.Length() > 0 && str.ByteAt(0) == "$";
    }
}
