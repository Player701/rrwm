//===========================================================================
//
// RR_ActorEventHandler
//
// Handles dynamic replacement and adjustments of certain actors.
//
//===========================================================================
class RR_ActorEventHandler : EventHandler
{
    private Array<RR_ReplacementInfo> _replacements;
    private bool _initDone;

    const EXTRA_ARMOR_CHANCE = 0.2;

    //===========================================================================
    //
    // RR_ActorEventHandler::GetInstance
    //
    // Returns the instance of the actor event handler.
    //
    //===========================================================================
    static clearscope RR_ActorEventHandler GetInstance()
    {
        let handler = RR_ActorEventHandler(EventHandler.Find('RR_ActorEventHandler'));

        if (handler == null)
        {
            ThrowAbortException("Could not find the actor event handler.");
        }

        return handler;
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::OnRegister
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    override void OnRegister()
    {
        if (!_initDone)
        {
            InitReplacementTable();

            // Only initialize once (this will also work in old save files).
            _initDone = true;
        }
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::InitReplacementTable
    //
    // Initializes the replacement table.
    //
    //===========================================================================
    private void InitReplacementTable()
    {
        // First, add replacements not dependent on DEHACKED detection
        // or other factors.

        // Replace armor bonuses with super armor bonuses
        // if extra armor is enabled and not in a separate slot
        // to allow raising armor value to 300.
        if (RR_Settings.InPlay(RR_SETTING_EXTRAARMOR).IsOn() && RR_Settings.InPlay(RR_SETTING_SUPERARMORSLOT).IsOff())
        {
            AddReplacement('ArmorBonus', 'RR_SuperArmorBonus');
        }

        // If railgun is enabled, add the corresponding replacements.
        if (RR_Settings.InPlay(RR_SETTING_RAILGUN).IsOn())
        {
            AddReplacement('RocketLauncher', 'RR_Railgun');
            AddReplacement('RocketAmmo',     'RR_RailAmmo');
            AddReplacement('RocketBox',      'RR_RailBox');
        }

        // Now, on to monster replacements.
        let dd = RR_DehackedDetector.GetInstance();

        // Get replacement class names for chaingun guys.
        // This needs to be done only once.
        bool newChaingunGuy = RR_Settings.InPlay(RR_SETTING_NEWCHAINGUNGUY).IsOn();

        // Have chaingun guy sprites been replaced?
        bool chaingunGuySpritesReplaced = RR_GlobalEventHandler.GetInstance().AreChaingunGuySpritesReplaced();

        let cguyTarget = newChaingunGuy
            ? 'RR_ChaingunGuyNew'
            : (chaingunGuySpritesReplaced ? 'RR_ChaingunGuyNoExtraSprites' : 'RR_ChaingunGuy');

        let cguyTargetStealth = newChaingunGuy
            ? 'RR_StealthChaingunGuyNew'
            : (chaingunGuySpritesReplaced ? 'RR_StealthChaingunGuyNoExtraSprites' : 'RR_StealthChaingunGuy');

        // Initialize monster replacements.
        CheckMonsterReplacement(dd, 'ZombieMan',        'RR_ZombieMan',   'StealthZombieMan',   'RR_StealthZombieMan');
        CheckMonsterReplacement(dd, 'ShotgunGuy',       'RR_ShotgunGuy',  'StealthShotgunGuy',  'RR_StealthShotgunGuy');
        CheckMonsterReplacement(dd, 'Revenant',         'RR_Revenant',    'StealthRevenant',    'RR_StealthRevenant');
        CheckMonsterReplacement(dd, 'ChaingunGuy',       cguyTarget,      'StealthChaingunGuy',  cguyTargetStealth);
        CheckMonsterReplacement(dd, 'Cacodemon',        'RR_Cacodemon',   'StealthCacodemon',   'RR_StealthCacodemon');
        CheckMonsterReplacement(dd, 'BaronOfHell',      'RR_BaronOfHell', 'StealthBaron',       'RR_StealthBaron');
        CheckMonsterReplacement(dd, 'HellKnight',       'RR_HellKnight',  'StealthHellKnight',  'RR_StealthHellKnight');
        CheckMonsterReplacement(dd, 'LostSoul',         'RR_LostSoul');
        CheckMonsterReplacement(dd, 'SpiderMastermind', 'RR_SpiderMastermind');
        CheckMonsterReplacement(dd, 'Cyberdemon',       'RR_Cyberdemon');
        CheckMonsterReplacement(dd, 'WolfensteinSS',    'RR_WolfensteinSS');
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::CheckMonsterReplacement
    //
    // Adds entries to the replacement table if the source type has not been
    // modified by DEHACKED.
    //
    //===========================================================================
    private void CheckMonsterReplacement(RR_DehackedDetector dehackedDetector, class<Actor> source, class<Actor> target, class<Actor> stealthSource = null, class<Actor> stealthTarget = null)
    {
        if (dehackedDetector.IsActorModified(source))
        {
            // Source modified by DEHACKED, do not replace
            return;
        }

        // Replace normal monsters.
        AddReplacement(source, target);

        // Replace stealth monsters, if applicable.
        if (stealthSource != null && stealthTarget != null)
        {
            AddReplacement(stealthSource, stealthTarget);
        }
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::AddReplacement
    //
    // Adds an entry to the replacement table.
    //
    //===========================================================================
    private void AddReplacement(class<Actor> source, class<Actor> target)
    {
        _replacements.Push(RR_ReplacementInfo.Create(source, target));
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::CheckReplacement
    //
    // Checks whether certain actors need to be replaced.
    //
    //===========================================================================
    override void CheckReplacement(ReplaceEvent e)
    {
        // Check the replacement table first.
        if (CheckReplacementTable(e))
        {
            return;
        }

        // Replace armors randomly
        // if the corresponding setting is enabled.
        if (RR_Settings.InPlay(RR_SETTING_EXTRAARMOR).IsOn())
        {
            let replaceeName = e.Replacee.GetClassName();

            if (replaceeName == 'GreenArmor' && RollForExtraArmor())
            {
                // Replace the green armor with a light armor.
                // This is an unlucky outcome.
                e.Replacement = 'RR_LightArmor';
            }
            else if (replaceeName == 'BlueArmor' && RollForExtraArmor())
            {
                // Replace the blue armor with a super armor.
                // This is a lucky outcome.
                e.Replacement = RR_Settings.InPlay(RR_SETTING_SUPERARMORSLOT).IsOn()
                    ? 'RR_SuperArmorShieldPickup'
                    : 'RR_SuperArmor';
            }
        }
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::CheckReplacementTable
    //
    // Checks the replacement table for an entry with the corresponding
    // source type. If found, performs the replacement and returns true.
    // Otherwise, returns false.
    //
    //===========================================================================
    private bool CheckReplacementTable(ReplaceEvent e)
    {
        foreach (replacement : _replacements)
        {
            if (replacement.GetSource() == e.Replacee)
            {
                e.Replacement = replacement.GetTarget();
                return true;
            }
        }

        return false;
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::CheckReplacee
    //
    // Checks for replaced types and returns the correct source type.
    // This is necessary for A_BossDeath to work properly!
    //
    //===========================================================================
    override void CheckReplacee(ReplacedEvent e)
    {
        foreach (replacement : _replacements)
        {
            if (replacement.GetTarget() == e.Replacement)
            {
                e.Replacee = replacement.GetSource();
                return;
            }
        }
    }

    // Some rant about WorldThingSpawned:
    // it is an ideal solution for changing pickup sounds,
    // but for changing something that would alter the appearance
    // of the actor (e.g. scale), WorldThingSpawned is less than perfect.
    // The reason is that it is called after PostBeginPlay,
    // and at this point the actor's sprite has already been drawn once.
    // It would be more appropriate to change these values before it happens,
    // but there doesn't seem to be an entry point for that.
    // I guess it would be called PreWorldThingSpawned if it existed.

    //===========================================================================
    //
    // RR_ActorEventHandler::WorldThingSpawned
    //
    // Adjusts miscellaneous properties of some items.
    //
    //===========================================================================
    override void WorldThingSpawned(WorldEvent e)
    {
        let item = Inventory(e.Thing);

        // Check that it is an inventory item.
        if (item == null)
        {
            return;
        }

        // Peform changes based on class name, so that inherited classes
        // aren't affected.
        let itemName = item.GetClassName();

        if (itemName == 'RadSuit')
        {
            // Change the pickup sound of radiation suits.
            item.PickupSound = "rrwm/pickups/radsuit";
        }
        else if (itemName == 'BlueCard' || itemName == 'YellowCard' || itemName == 'RedCard')
        {
            // Change the pickup sound of key cards.
            item.PickupSound = "rrwm/pickups/keycard";
        }
        else if (itemName == 'BlueSkull' || itemName == 'YellowSkull' || itemName == 'RedSkull')
        {
            // Change the pickup sound of skull keys.
            // They normally use misc/k_pkup, and we want the generic pickup sound instead.
            // We don't want to redefine misc/k_pkup,
            // that's why we check for these skull keys explicitly.
            item.PickupSound = "rrwm/pickups/generic";
        }
    }

    //===========================================================================
    //
    // RR_ActorEventHandler::RollForExtraArmor
    //
    // Rolls a random value to determine if an armor is to be replaced
    // by an extra armor type.
    //
    //===========================================================================
    private static bool RollForExtraArmor()
    {
        return RR_Math.RandomRoll(EXTRA_ARMOR_CHANCE);
    }
}
