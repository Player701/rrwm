//===========================================================================
//
// RR_ReplacementInfo
//
// Contains information about an actor replacement.
//
//===========================================================================
class RR_ReplacementInfo
{
    // The source class (being replaced).
    private class<Actor> _source;

    // The target class (replaced with).
    private class<Actor> _target;

    //===========================================================================
    //
    // RR_ReplacementInfo::Create
    //
    // Creates a new instance of the RR_ReplacementInfo class.
    //
    //===========================================================================
    static RR_ReplacementInfo Create(class<Actor> source, class<Actor> target)
    {
        let ri = new('RR_ReplacementInfo');
        ri.Init(source, target);

        return ri;
    }

    //===========================================================================
    //
    // RR_ReplacementInfo::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(class<Actor> source, class<Actor> target)
    {
        if (source == null)
        {
            ThrowAbortException("%s: Source cannot be null.", GetClassName());
        }

        if (target == null)
        {
            ThrowAbortException("%s: Target cannot be null.", GetClassName());
        }

        _source = source;
        _target = target;
    }

    //===========================================================================
    //
    // RR_ReplacementInfo::GetSource
    //
    // Returns the source class (being replaced).
    //
    //===========================================================================
    class<Actor> GetSource() const
    {
        return _source;
    }

    //===========================================================================
    //
    // RR_ReplacementInfo::GetTarget
    //
    // Returns the target class (replaced with).
    //
    //===========================================================================
    class<Actor> GetTarget() const
    {
        return _target;
    }
}
