//===========================================================================
//
// RR_VersionSyncHandler
//
// Handles RRWM version synchronization in multiplayer games.
//
//===========================================================================
class RR_VersionSyncHandler : StaticEventHandler
{
    // Name of the network event for version synchronization.
    const VERSION_SYNC_EVENT = "rrwm_syncversion";

    // Have we started the version sync check?
    private bool _versionSyncStarted;
    // Flags to check that we've synchronized RRWM version
    // across all players in a multiplayer game.
    private bool _versionSynced[MAXPLAYERS];
    // How long we've been waiting for to synchronize the version?
    private int _versionSyncTimer;
    // How long to wait for all players to synchronize RRWM version?
    private int _versionSyncDelay;

    // List of error messages to display
    private Array<string> _errorMessages;

    //===========================================================================
    //
    // RR_VersionSyncHandler::OnRegister
    //
    // Sets the synchronization delay to 1 second.
    //
    //===========================================================================
    override void OnRegister()
    {
        _versionSyncDelay = GameTicRate;
    }

    //===========================================================================
    //
    // RR_VersionSyncHandler::WorldLoaded
    //
    // Sets flags for players currently in the game to synchronize RRWM version
    // with them.
    //
    //===========================================================================
    override void WorldLoaded(WorldEvent e)
    {
        if (multiplayer && !_versionSyncStarted)
        {
            for (int i = 0; i < MAXPLAYERS; i++)
            {
                _versionSynced[i] = !playeringame[i];
            }

            _versionSyncStarted = true;
        }
    }

    //===========================================================================
    //
    // RR_VersionSyncHandler::WorldTick
    //
    // Send a network event to synchronize RRWM version across all players
    // and also verifies that everyone else has done that too
    // once the synchronization timer is up. This is necessary to avoid
    // multiplayer incompatibilities  when someone is using a version
    // older than 1.3.0.
    //
    //===========================================================================
    override void WorldTick()
    {
        if (!multiplayer || _versionSyncTimer > _versionSyncDelay)
        {
            // all done
            return;
        }

        // Starting timer - send event to synchronize version
        if (_versionSyncTimer == 0)
        {
            string eventStr = string.Format("%s:%s", VERSION_SYNC_EVENT, RR_VersionInfo.VERSION_STRING);
            EventHandler.SendNetworkEvent(eventStr);
        }

        _versionSyncTimer++;

        // Time's up - verify that we've synced with everyone else
        if (_versionSyncTimer == _versionSyncDelay)
        {
            for (int i = 0; i < MAXPLAYERS; i++)
            {
                if (i == consoleplayer)
                {
                    // skip this player
                    continue;
                }

                if (!_versionSynced[i])
                {
                    // Oops, missing version
                    string errorMsg = StringTable.Localize("$RRWM_VERSION_SYNC_MISSING");
                    errorMsg = string.Format(errorMsg, players[i].GetUserName(), i);

                    AddErrorMessage(errorMsg);
                }
            }

            // Check for errors and abort if there are any
            CheckSync();
        }
    }

    //===========================================================================
    //
    // RR_VersionSyncHandler::NetworkProcess
    //
    // Processes version synchronization events in multiplayer.
    //
    //===========================================================================
    override void NetworkProcess(ConsoleEvent e)
    {
        if (!multiplayer || e.Player == consoleplayer || e.IsManual)
        {
            return;
        }

        // Verify command
        Array<string> tokens;
        e.Name.Split(tokens, ":", TOK_SKIPEMPTY);

        if (tokens.Size() != 2 || tokens[0] != VERSION_SYNC_EVENT)
        {
            return;
        }

        // Get the player number
        int pnum = e.Player;

        // Check if not already synced
        if (_versionSynced[pnum])
        {
            return;
        }

        // Compare versions
        string otherVersion = tokens[1];

        if (otherVersion != RR_VersionInfo.VERSION_STRING)
        {
            // Oops
            string errorMsg = StringTable.Localize("$RRWM_VERSION_SYNC_MISMATCH");
            errorMsg = string.Format(errorMsg, players[pnum].GetUserName(), pnum, otherVersion);

            AddErrorMessage(errorMsg);
        }

        // Make sure we do not sync again with this player
        _versionSynced[pnum] = true;
    }

    //===========================================================================
    //
    // RR_VersionSyncHandler::AddErrorMessage
    //
    // Adds an error message to the list of messages.
    //
    //===========================================================================
    private void AddErrorMessage(string msg)
    {
        _errorMessages.Push(msg);
    }

    //===========================================================================
    //
    // RR_VersionSyncHandler::CheckSync
    //
    // If any synchronization errors are found, prints them all to the console
    // and aborts the current game.
    //
    //===========================================================================
    private void CheckSync()
    {
        int n = _errorMessages.Size();

        if (n == 0)
        {
            // everything is OK
            return;
        }

        // Print header
        Console.Printf("%s%s", TEXTCOLOR_ORANGE, StringTable.Localize("$RRWM_VERSION_SYNC_FAILED"));

        // Print current version
        string yourVer = StringTable.Localize("$RRWM_VERSION_SYNC_YOURVER");
        yourVer = string.Format(yourVer, RR_VersionInfo.VERSION_STRING);
        Console.Printf("%s%s", TEXTCOLOR_GREEN, yourVer);

        // Print all error messages
        for (int i = 0; i < n; i++)
        {
            Console.Printf("%s%s", TEXTCOLOR_GREEN, _errorMessages[i]);
        }

        // Error!
        ThrowAbortException(StringTable.Localize("$RRWM_VERSION_SYNC_ERROR"));
    }
}
