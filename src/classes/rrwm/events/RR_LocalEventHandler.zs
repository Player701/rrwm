//===========================================================================
//
// RR_LocalEventHandler
//
// Handles per-level events not related to individual actors.
// This includes settings synchronization and initialization
// of the debris queue.
//
//===========================================================================
class RR_LocalEventHandler : EventHandler
{
    // The local instance of the persistent settings container.
    private RR_PersistentSettings _persistentSettings;
    // RRWM version string saved for the current level.
    private string _versionString;
    // Are we restarting the level?
    private transient bool _restartingLevel;

    //===========================================================================
    //
    // RR_LocalEventHandler::GetInstance
    //
    // Returns the instance of the local event handler.
    //
    //===========================================================================
    static clearscope RR_LocalEventHandler GetInstance()
    {
        let handler = RR_LocalEventHandler(EventHandler.Find('RR_LocalEventHandler'));

        if (handler == null)
        {
            ThrowAbortException("Could not find the local event handler.");
        }

        return handler;
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::OnRegister
    //
    // Queries the global settings event handler to synchronize level settings
    // with global settings.
    //
    //===========================================================================
    override void OnRegister()
    {
        // If a container has not been created, we need to create it first.
        if (_persistentSettings == null)
        {
            _persistentSettings = RR_PersistentSettings.Create();
            _persistentSettings.SyncWith(RR_PersistentSettings.GetGlobalInstance());
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::WorldLoaded
    //
    // Initializes the debris manager on world load.
    //
    //===========================================================================
    override void WorldLoaded(WorldEvent e)
    {
        // Initialize the debris manager.
        RR_DebrisManager.Init();
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::EventErrorMessage
    //
    // Prints an error message from an event to the console.
    //
    //===========================================================================
    private static clearscope void EventErrorMessage(string msg, int playerNumber = -1)
    {
        if (playerNumber == -1 || playerNumber == consoleplayer)
        {
            string errorStr = StringTable.Localize("$RR_EVENT_ERROR");
            Console.Printf(errorStr, StringTable.Localize(msg));
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::ConsoleProcess
    //
    // Implements a console command to query the status of the debris queue.
    //
    //===========================================================================
    override void ConsoleProcess(ConsoleEvent e)
    {
        if (e.Name ~== "rrwm_debrisqueue_status")
        {
            let debrisManager = RR_DebrisManager.GetInstance();

            if (debrisManager == null)
            {
                EventErrorMessage("$RR_EVENT_DEBRISQUEUE_ERROR");
            }
            else
            {
                debrisManager.PrintStatusInfo();
            }
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::NetworkProcess
    //
    // Implements a console command to restart the current level.
    //
    //===========================================================================
    override void NetworkProcess(ConsoleEvent e)
    {
        if (e.Name ~== "rrwm_restartlevel")
        {
            int pNum = e.Player;

            if (!multiplayer)
            {
                EventErrorMessage("$RR_EVENT_LEVELRESTART_ERROR_MULTIPLAYER", pNum);
            }
            else if (!players[e.Player].settings_controller)
            {
                EventErrorMessage("$RR_EVENT_LEVELRESTART_ERROR_CONTROLLER", pNum);
            }
            else if (!CanRestartLevel())
            {
                EventErrorMessage("$RR_EVENT_LEVELRESTART_ERROR_HUB", pNum);
            }
            else
            {
                // Print info message for all players
                string msg = StringTable.Localize("$RR_EVENT_LEVELRESTART");
                Console.Printf(msg, players[pNum].GetUserName());

                RestartLevel();
            }
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::WorldTick
    //
    // Restarts the current level if it has been requested.
    //
    //===========================================================================
    override void WorldTick()
    {
        if (_restartingLevel)
        {
            Level.ChangeLevel(Level.mapname, flags: CHANGELEVEL_NOINTERMISSION|CHANGELEVEL_RESETHEALTH|CHANGELEVEL_RESETINVENTORY);
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::WorldUnloaded
    //
    // If the level is not being restarted, synchronizes global persistent
    // settings with stored values. This allows the correct values to be
    // retrieved on the next level. Otherwise, synchronizes them with the CVAR
    // values to apply changes before the level is restarted.
    //
    //===========================================================================
    override void WorldUnloaded(WorldEvent e)
    {
        // Check if we are restarting the level.
        if (_restartingLevel)
        {
            // Restarting level - sync persistent settings with actual values.
            RR_PersistentSettings.GetGlobalInstance().SyncWithActualValues();
        }
        else
        {
            // Normal level transition - sync with current values to carry them
            // over to the next level.
            RR_PersistentSettings.GetGlobalInstance().SyncWith(_persistentSettings);
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::OnUnregister
    //
    // Destroys the local persistent settings container.
    //
    //===========================================================================
    override void OnUnregister()
    {
        _persistentSettings.Destroy();
        _persistentSettings = null;
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::GetPersistentSettings
    //
    // Returns the local instance of the persistent settings container.
    //
    //===========================================================================
    RR_PersistentSettings GetPersistentSettings() const
    {
        return _persistentSettings;
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::CheckVersion
    //
    // Compares the version string stored in the handler
    // with the actual version string. If they do not match,
    // a warning message is printed.
    //
    //===========================================================================
    void CheckVersion()
    {
        if (_versionString != RR_VersionInfo.VERSION_STRING)
        {
            string warning = string.Format(
                "%s%s: %s%s\n%s%s",
                TEXTCOLOR_FIRE,
                StringTable.Localize("$RR_WARNING"),
                TEXTCOLOR_GRAY,
                StringTable.Localize("$RR_VERSION_MISMATCH_WARNING"),
                TEXTCOLOR_GRAY,
                StringTable.Localize("$RR_VERSION_MISMATCH_WARNING_STABILITY")
            );

            // If version strings don't match, print a disclaimer warning.
            Console.MidPrint(null, warning, false);
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::SetVersion
    //
    // Sets the version string in the handler to match the current version.
    //
    //===========================================================================
    void SetVersion()
    {
        // Save the version string in either case so that the warning
        // isn't printed more than once.
        _versionString = RR_VersionInfo.VERSION_STRING;
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::RestartLevel
    //
    // Requests a restart of the current level and sync of all persistent
    // settings with their CVAR values. This is useful in multiplayer games
    // to avoid restarting GZDoom.
    //
    //===========================================================================
    private void RestartLevel()
    {
        if (CanRestartLevel())
        {
            _restartingLevel = true;
        }
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::CanRestartLevel
    //
    // Checks if a level restart is possible.
    //
    //===========================================================================
    static clearscope bool CanRestartLevel()
    {
        // We can only restart levels that are not part of a hub.
        return gamestate == GS_LEVEL && !(Level.clusterflags & LevelLocals.CLUSTER_HUB);
    }

    //===========================================================================
    //
    // RR_LocalEventHandler::RequestRestartLevel
    //
    // Sends a network event to restart the level.
    //
    //===========================================================================
    static clearscope void RequestRestartLevel()
    {
        EventHandler.SendNetworkEvent("rrwm_restartlevel");
    }
}
