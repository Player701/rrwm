//===========================================================================
//
// RR_GlobalEventHandler
//
// Initializes and stores the global instances of the settings container.
// Handles re-initialization of persistent settings for a new game.
//
//===========================================================================
class RR_GlobalEventHandler : StaticEventHandler
{
    // All metadata on the game settings is stored here.
    // This way, they can be queried even when not in a level.
    private RR_Settings _settings;

    // Flag to check if we've initialized CVAR descriptions yet.
    private ui bool _descriptionsAdded;
//<ExcludeFromHudPackage>

    // The DehackedDetector is used to prevent replacing monsters
    // that have been modified by DEHACKED patches.
    private RR_DehackedDetector _dehackedDetector;

    // This flag is set when Chaingun Guy sprites have been replaced
    // so that the actor event handler can pick the correct version
    // of the enemy while doing replacements.
    private bool _chaingunGuySpritesReplaced;

    // This flag is set when the LevelPostProcessor hook has modified
    // the ammo capacities for RRWM ammo types, so that it is only done once.
    private bool _ammoCapacitiesModified;
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_GlobalEventHandler::GetInstance
    //
    // Returns the instance of the global event handler.
    //
    //===========================================================================
    static clearscope RR_GlobalEventHandler GetInstance()
    {
        let handler = RR_GlobalEventHandler(StaticEventHandler.Find('RR_GlobalEventHandler'));

        if (handler == null)
        {
            ThrowAbortException("Could not find the global event handler.");
        }

        return handler;
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::OnRegister
    //
    // Initialzies RRWM settings and prints RRWM version to the console.
    //
    //===========================================================================
    override void OnRegister()
    {
        _settings = RR_Settings.Create();
//<ExcludeFromHudPackage>

        // Perhaps not the best place to do it, but I'm not sure if creating
        // another event handler just to print this message is a good idea.
        string startupMsg = string.Format(StringTable.Localize("$RR_STARTUP_MESSAGE"),
            RR_VersionInfo.VERSION_STRING,
            RR_VersionInfo.VERSION_TIMESTAMP
        );

        Console.Printf("%s%s", TEXTCOLOR_GREEN, startupMsg);

        // Initialize the DEHACKED detector.
        _dehackedDetector = RR_DehackedDetector.Create();

        // Check for Chaingun Guy sprite replacements.
        CheckChaingunGuySprites();
//</ExcludeFromHudPackage>
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::UiTick
    //
    // Initialized the descriptions for RRWM setting CVARs,
    // unless it's already been done.
    //
    //===========================================================================
    override void UiTick()
    {
        if (_descriptionsAdded)
        {
            return;
        }

        // NB: This is somewhat of a hack, but I've seen much, much worse.
        // Since the RRWM options menu is populated dynamically,
        // CVAR descriptions will not appear until the user
        // has opened the menu for the first time.
        // The following code is thus necessary to initialize them explicitly.
        for (int i = 0; i < _settings.Count(); i++)
        {
            // Creating and initializing the menu item
            // will call SetCVarDescription in OptionMenuItemOption.
            _settings.GetByIndex(i).GetMenuItem();
        }

        // Prevent redundant initialization
        _descriptionsAdded = true;
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_GlobalEventHandler::CheckChaingunGuySprites
    //
    // Checks if sprites for chaingun guys have been replaced.
    // This is necessary because if they have, then we shouldn't use
    // the chaingun guy that features the sprite CPOSU provided by RRWM.
    //
    //===========================================================================
    private void CheckChaingunGuySprites()
    {
        int cnt = 0;
        int index = 0;

        while ((index = Wads.FindLump("CPOSA1", index, Wads.AnyNamespace)) != -1)
        {
            index++;
            cnt++;
        }

        _chaingunGuySpritesReplaced = cnt > 1;
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::AreChaingunGuySpritesReplaced
    //
    // Returns true if sprites for chaingun guys have been replaced,
    // false otherwise.
    //
    //===========================================================================
    bool AreChaingunGuySpritesReplaced() const
    {
        return _chaingunGuySpritesReplaced;
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::SetAmmoCapacitiesModified
    //
    // Sets the flag that indicates that ammo capacities have been modified.
    //
    //===========================================================================
    void SetAmmoCapacitiesModified()
    {
        _ammoCapacitiesModified = true;
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::AreAmmoCapacitiesModified
    //
    // Returns true if the ammo capacities have already been modified,
    // false otherwise.
    //
    //===========================================================================
    bool AreAmmoCapacitiesModified() const
    {
        return _ammoCapacitiesModified;
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::NewGame
    //
    // Re-initializes all settings for a new game.
    //
    //===========================================================================
    override void NewGame()
    {
        _settings.GetGlobalPersistentSettings().SyncWithActualValues();
    }
//</ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_GlobalEventHandler::WorldLoaded
    //
    // Clears the settings cache.
    //
    //===========================================================================
    override void WorldLoaded(WorldEvent e)
    {
        // It appears that calling this in NewGame is not enough,
        // we have to call it each time a level loads.
        // Fortunately, only user CVARs are affected by this.
        _settings.ClearCache();
//<ExcludeFromHudPackage>

        // See if we have to run the version check
        let localEventHandler = RR_LocalEventHandler.GetInstance();

        if (e.IsSaveGame)
        {
            // Trigger the version check
            localEventHandler.CheckVersion();
        }

        // Reset version string to current so that it's not
        // checked for again on this level.
        localEventHandler.SetVersion();
//</ExcludeFromHudPackage>
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::GetSettings
    //
    // Returns the global instance of RR_Settings.
    // This method is used by external sources to access settings.
    //
    //===========================================================================
    RR_Settings GetSettings() const
    {
        return _settings;
    }
//<ExcludeFromHudPackage>

    //===========================================================================
    //
    // RR_GlobalEventHandler::GetDehackedDetector
    //
    // Returns the global instance of the DEHACKED detector.
    //
    //===========================================================================
    RR_DehackedDetector GetDehackedDetector() const
    {
        return _dehackedDetector;
    }
//</ExcludeFromHudPackage>
}
