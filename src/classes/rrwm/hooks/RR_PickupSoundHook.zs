//===========================================================================
//
// RR_PickupSoundHook
//
// Injected into RRWM inventory item classes to provide overlapping
// playback of pickup sounds.
//
//===========================================================================
mixin class RR_PickupSoundHook
{
    //===========================================================================
    //
    // RR_PickupSoundHook::PlayPickupSound
    //
    // Plays this item's pickup sound.
    //
    //===========================================================================
    override void PlayPickupSound (Actor toucher)
    {
        if (toucher == null)
        {
            return;
        }

        // Route the sound to the sound handler.
        RR_PickupSoundHandler.Get(toucher).PlayPickupSound(self);
    }
}
