//===========================================================================
//
// RR_PartialPickupHook
//
// Implements a partial pickup method for weapons, ammo, and health.
//
//===========================================================================
mixin class RR_PartialPickupHook
{
    //===========================================================================
    //
    // RR_PartialPickupHook::TryPickup
    //
    // Checks if there is still some amount of this item available
    // after the pickup, and if there is, spawns a residual item.
    //
    //===========================================================================
    override bool TryPickup(in out Actor toucher)
    {
        let newState = CurState;
        int newTics = tics;

        int owned = PartialPickup_GetAmountOwned(toucher);
        bool result = Super.TryPickup(toucher);

        if (result && IsPartialPickupEnabled())
        {
            int added = Max(0, PartialPickup_GetAmountOwned(toucher) - owned);
            int remaining = PartialPickup_GetAmountAvailable() - added;

            if (remaining > 0)
            {
                let residualItemType = PartialPickup_GetResidualItemType(remaining);
                bool isSameType = residualItemType == GetClass();

                // Calculate the number of items to spawn.
                int amountPerItem = PartialPickup_GetResidualItemAmount(remaining);
                int cnt = ((remaining - 1) / amountPerItem) + 1;

                if (isSameType && cnt == 1)
                {
                    // The item is replaced with 1 item of the same type,
                    // it's as if it doesn't disappear at all...
                    PickupFlash = null;
                }

                // Spawn all residual items.
                // Add random velocity if there is more than one item to spawn.
                for (int i = 0; i < cnt; i++)
                {
                    int newAmount = Min(amountPerItem, remaining);
                    let newItem = SpawnResidualItem(residualItemType, newAmount, Pos, cnt > 1);

                    // Copy the current state and tics, if applicable
                    if (isSameType && newItem != null && newItem.SetState(newState, true))
                    {
                        newItem.tics = newTics;
                    }

                    remaining -= newAmount;
                }
            }
        }

        return result;
    }
}
