//===========================================================================
//
// RR_PostprocessorHook
//
// Utilizes the power of LevelPostProcessor to change ammo capacities
// of RRWM ammo types to account for potential DEHACKED modifications.
//
//===========================================================================
class RR_PostprocessorHook : LevelPostProcessor
{
    //===========================================================================
    //
    // RR_PostprocessorHook::Apply
    //
    // Adjusts ammo capacities of RRWM ammo types
    // based on their vanilla counterparts.
    //
    //===========================================================================
    void Apply(name checksum, string mapname)
    {
        let globalEventHandler = RR_GlobalEventHandler.GetInstance();

        if (globalEventHandler.AreAmmoCapacitiesModified())
        {
            // Already done
            return;
        }

        foreach (cls : AllActorClasses)
        {
            let ammotype = (class<RR_Ammo>)(cls);

            if (ammotype == null || GetDefaultByType(ammotype).GetParentAmmo() != ammotype)
            {
                // Not an RRWM base ammo type
                continue;
            }

            // OK: get the writeable default actor
            let newAmmo = RR_Ammo(GetDefaultActor(ammotype.GetClassName()));

            // OK: get the vanilla ammo class
            if (newAmmo.VanillaAmmoClass == null)
            {
                // Does not have a vanilla counterpart
                continue;
            }

            // OK: get the vanilla ammo and modify the capacities
            let vanillaAmmo = GetDefaultByType(newAmmo.VanillaAmmoClass);

            newAmmo.MaxAmount = vanillaAmmo.MaxAmount;
            newAmmo.BackpackMaxAmount = vanillaAmmo.BackpackMaxAmount;
        }

        globalEventHandler.SetAmmoCapacitiesModified();
    }
}
