//===========================================================================
//
// RRWM bot support definitions for weapons
//
//===========================================================================

RR_Pistol, 25000000
RR_Shotgun, 24000000
RR_SuperShotgun, 15000000
RR_Chaingun, 27000000
RR_RocketLauncher, 18350080, BOT_REACTION_SKILL_THING, BOT_EXPLOSIVE, RR_Rocket
RR_Railgun, 27000000, BOT_REACTION_SKILL_THING
RR_PlasmaRifle, 27000000, RR_PlasmaBall
RR_BFG9000, 10000000, BOT_REACTION_SKILL_THING, BOT_BFG, RR_BFGBall
