//===========================================================================
//
// RRWM KEYCONF file
//
// Contains aliases for RRWM console and network events
//
//===========================================================================

alias "rrwm_debrisqueue_status" "event rrwm_debrisqueue_status"
alias "rrwm_restartlevel" "netevent rrwm_restartlevel"
