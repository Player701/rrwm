//===========================================================================
//
// ZScript entry point for RRWM
//
// Includes all class files.
//
//===========================================================================
version "4.13"

// Common classes
#include "classes/rrwm/common/RR_VersionInfo.zs"
#include "classes/rrwm/common/RR_Math.zs"
#include "classes/rrwm/common/RR_StringTools.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/common/RR_Obituary.zs"
#include "classes/rrwm/common/RR_WeaponClip.zs"
#include "classes/rrwm/common/RR_PartialPickup.zs"

// DEHACKED detection
#include "classes/rrwm/dehacked/RR_DehackedDetector.zs"
#include "classes/rrwm/dehacked/RR_DehackedInfo.zs"
//</ExcludeFromHudPackage>

// Settings
#include "classes/rrwm/settings/RR_SettingPostprocessorCache.zs"
#include "classes/rrwm/settings/RR_Settings.zs"
#include "classes/rrwm/settings/RR_SettingsList.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/settings/RR_PersistentSettings.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/settings/RR_SettingsConstants.zs"
#include "classes/rrwm/settings/RR_SettingValue.zs"
#include "classes/rrwm/settings/RR_OptionValues.zs"
#include "classes/rrwm/settings/RR_CvarHelper.zs"
#include "classes/rrwm/settings/builders/RR_SettingBuilder.zs"
#include "classes/rrwm/settings/builders/RR_OptionBuilder.zs"
#include "classes/rrwm/settings/builders/RR_SwitchBuilder.zs"
#include "classes/rrwm/settings/builders/RR_NumberBuilder.zs"
#include "classes/rrwm/settings/dependencies/RR_SettingDependency.zs"
#include "classes/rrwm/settings/dependencies/RR_SingleDependency.zs"
#include "classes/rrwm/settings/dependencies/RR_MultiDependency.zs"
#include "classes/rrwm/settings/dependencies/RR_ValueDependency.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/settings/dependencies/RR_MultiplayerAwareDependency.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/settings/types/RR_Setting.zs"
#include "classes/rrwm/settings/types/RR_SettingOption.zs"
#include "classes/rrwm/settings/types/RR_SettingSwitch.zs"
#include "classes/rrwm/settings/types/RR_SettingNumber.zs"
#include "classes/rrwm/settings/valueproviders/RR_ValueProvider.zs"
#include "classes/rrwm/settings/valueproviders/RR_ValueProviderUser.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/settings/valueproviders/RR_ValueProviderServer.zs"
#include "classes/rrwm/settings/valueproviders/RR_ValueProviderServerPersistent.zs"
//</ExcludeFromHudPackage>

// Events
#include "classes/rrwm/events/RR_GlobalEventHandler.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/events/RR_VersionSyncHandler.zs"
#include "classes/rrwm/events/RR_LocalEventHandler.zs"
#include "classes/rrwm/events/RR_ActorEventHandler.zs"
#include "classes/rrwm/events/RR_ReplacementInfo.zs"

// Hooks
#include "classes/rrwm/hooks/RR_HandlePickupHook.zs"
#include "classes/rrwm/hooks/RR_PickupSoundHook.zs"
#include "classes/rrwm/hooks/RR_PartialPickupHook.zs"
#include "classes/rrwm/hooks/RR_PostprocessorHook.zs"

// Sound handling
#include "classes/rrwm/sounds/RR_ChainsawSoundPlayer.zs"
#include "classes/rrwm/sounds/RR_PickupSoundHandler.zs"
#include "classes/rrwm/sounds/RR_SoundChannels.zs"

// Weapons
#include "classes/rrwm/weapons/RR_Weapon.zs"
#include "classes/rrwm/weapons/RR_Fist.zs"
#include "classes/rrwm/weapons/RR_Chainsaw.zs"
#include "classes/rrwm/weapons/RR_ReloadableWeapon.zs"
#include "classes/rrwm/weapons/RR_HitscanWeapon.zs"
#include "classes/rrwm/weapons/RR_Pistol.zs"
#include "classes/rrwm/weapons/RR_Shotgun.zs"
#include "classes/rrwm/weapons/RR_SuperShotgun.zs"
#include "classes/rrwm/weapons/RR_Chaingun.zs"
#include "classes/rrwm/weapons/RR_RocketLauncher.zs"
#include "classes/rrwm/weapons/RR_Railgun.zs"
#include "classes/rrwm/weapons/RR_PlasmaRifle.zs"
#include "classes/rrwm/weapons/RR_BFG9000.zs"

// Ammo
#include "classes/rrwm/pickups/ammo/RR_Ammo.zs"
#include "classes/rrwm/pickups/ammo/RR_ChainsawAmmo.zs"
#include "classes/rrwm/pickups/ammo/RR_ChainsawAmmo1.zs"
#include "classes/rrwm/pickups/ammo/RR_ChainsawAmmo2.zs"
#include "classes/rrwm/pickups/ammo/RR_ReloadableAmmo.zs"
#include "classes/rrwm/pickups/ammo/RR_Clip.zs"
#include "classes/rrwm/pickups/ammo/RR_ClipBox.zs"
#include "classes/rrwm/pickups/ammo/RR_Shell.zs"
#include "classes/rrwm/pickups/ammo/RR_ShellBox.zs"
#include "classes/rrwm/pickups/ammo/RR_RocketAmmo.zs"
#include "classes/rrwm/pickups/ammo/RR_RocketBox.zs"
#include "classes/rrwm/pickups/ammo/RR_RailAmmo.zs"
#include "classes/rrwm/pickups/ammo/RR_RailBox.zs"
#include "classes/rrwm/pickups/ammo/RR_Cell.zs"
#include "classes/rrwm/pickups/ammo/RR_CellPack.zs"

// Armors
#include "classes/rrwm/pickups/armors/RR_Armor.zs"
#include "classes/rrwm/pickups/armors/RR_LightArmor.zs"
#include "classes/rrwm/pickups/armors/RR_StandardArmor.zs"
#include "classes/rrwm/pickups/armors/RR_HeavyArmor.zs"
#include "classes/rrwm/pickups/armors/RR_SuperArmor.zs"
#include "classes/rrwm/pickups/armors/RR_SuperArmorShieldPickup.zs"
#include "classes/rrwm/pickups/armors/RR_ArmorBonus.zs"
#include "classes/rrwm/pickups/armors/RR_SuperArmorBonus.zs"

// Other pickups
#include "classes/rrwm/pickups/misc/RR_Adrenaline.zs"
#include "classes/rrwm/pickups/misc/RR_AllMap.zs"
#include "classes/rrwm/pickups/misc/RR_Infrared.zs"
#include "classes/rrwm/pickups/misc/RR_Megasphere.zs"
#include "classes/rrwm/pickups/misc/RR_Backpack.zs"

// Powerups
#include "classes/rrwm/powerups/RR_Powerup.zs"
#include "classes/rrwm/powerups/RR_AdrenalinePowerupBase.zs"
#include "classes/rrwm/powerups/RR_PowerAdrenaline.zs"
#include "classes/rrwm/powerups/RR_PowerAdrenalineProtection.zs"
#include "classes/rrwm/powerups/RR_PowerStrength.zs"

// Projectiles
#include "classes/rrwm/projectiles/RR_Projectile.zs"
#include "classes/rrwm/projectiles/RR_Rocket.zs"
#include "classes/rrwm/projectiles/RR_PlasmaBall.zs"
#include "classes/rrwm/projectiles/RR_BFGBall.zs"

// Effects
#include "classes/rrwm/effects/RR_EffectBase.zs"
#include "classes/rrwm/effects/RR_MovingEffectBase.zs"
#include "classes/rrwm/effects/bfg/RR_BFGTrail.zs"
#include "classes/rrwm/effects/bfg/RR_BFGExtra.zs"
#include "classes/rrwm/effects/debris/RR_DebrisBase.zs"
#include "classes/rrwm/effects/debris/RR_DebrisManager.zs"
#include "classes/rrwm/effects/debris/casings/RR_CasingBase.zs"
#include "classes/rrwm/effects/debris/casings/RR_PistolCasing.zs"
#include "classes/rrwm/effects/debris/casings/RR_ShotgunCasing.zs"
#include "classes/rrwm/effects/debris/casings/RR_ChaingunCasing.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_PistolEmptyClip.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_ChaingunEmptyClip.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_RocketLauncherEmptyClip.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_RailgunEmptyClip.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_PlasmaRifleEmptyClip.zs"
#include "classes/rrwm/effects/debris/emptyclips/RR_BFG9000EmptyClip.zs"
#include "classes/rrwm/effects/misc/RR_Bubble.zs"
#include "classes/rrwm/effects/misc/RR_RocketExhaust.zs"
#include "classes/rrwm/effects/misc/RR_Spark.zs"
#include "classes/rrwm/effects/misc/RR_LaserDot.zs"
#include "classes/rrwm/effects/puffs/RR_FistPuff.zs"
#include "classes/rrwm/effects/puffs/RR_FistPuffPowered.zs"
#include "classes/rrwm/effects/puffs/RR_FistPuffSuperPowered.zs"
#include "classes/rrwm/effects/puffs/RR_ChainsawPuff.zs"
#include "classes/rrwm/effects/puffs/RR_SparkPuff.zs"
#include "classes/rrwm/effects/puffs/RR_PistolPuff.zs"
#include "classes/rrwm/effects/puffs/RR_ShotgunPuff.zs"
#include "classes/rrwm/effects/puffs/RR_ChaingunPuff.zs"
#include "classes/rrwm/effects/puffs/RR_RailgunPuff.zs"
#include "classes/rrwm/effects/puffs/RR_RailgunPuffDummy.zs"
#include "classes/rrwm/effects/shield/RR_ShieldAfterimage.zs"
#include "classes/rrwm/effects/shield/RR_SuperArmorShield.zs"
#include "classes/rrwm/effects/smoke/RR_SmokeBase.zs"
#include "classes/rrwm/effects/smoke/RR_CasingSmoke.zs"
#include "classes/rrwm/effects/smoke/RR_ChainsawSmoke.zs"
#include "classes/rrwm/effects/smoke/RR_RocketSmoke.zs"

// Player class
#include "classes/rrwm/player/RR_Player.zs"

// Monsters
#include "classes/rrwm/monsters/RR_ReloadingMonster.zs"
#include "classes/rrwm/monsters/RR_HitscanMonster.zs"
#include "classes/rrwm/monsters/RR_ZombieMan.zs"
#include "classes/rrwm/monsters/RR_ShotgunGuy.zs"
#include "classes/rrwm/monsters/RR_ChaingunMonsterBase.zs"
#include "classes/rrwm/monsters/RR_ChaingunGuyBase.zs"
#include "classes/rrwm/monsters/RR_ChaingunGuy.zs"
#include "classes/rrwm/monsters/RR_ChaingunGuyNoExtraSprites.zs"
#include "classes/rrwm/monsters/RR_ChaingunGuyNew.zs"
#include "classes/rrwm/monsters/RR_WolfensteinSS.zs"
#include "classes/rrwm/monsters/RR_LostSoul.zs"
#include "classes/rrwm/monsters/RR_Cacodemon.zs"
#include "classes/rrwm/monsters/RR_Revenant.zs"
#include "classes/rrwm/monsters/RR_HellKnight.zs"
#include "classes/rrwm/monsters/RR_BaronOfHell.zs"
#include "classes/rrwm/monsters/RR_SpiderMastermind.zs"
#include "classes/rrwm/monsters/RR_Cyberdemon.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthZombieMan.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthShotgunGuy.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthChaingunGuy.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthChaingunGuyNoExtraSprites.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthChaingunGuyNew.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthCacodemon.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthRevenant.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthHellKnight.zs"
#include "classes/rrwm/monsters/stealth/RR_StealthBaron.zs"
//</ExcludeFromHudPackage>

// HUD
#include "classes/rrwm/ui/hud/RR_Hud.zs"
#include "classes/rrwm/ui/hud/RR_HudHooks.zs"
#include "classes/rrwm/ui/hud/RR_HudStyleContext.zs"
#include "classes/rrwm/ui/hud/RR_HudFragment.zs"
#include "classes/rrwm/ui/hud/RR_CrosshairCanvas.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/RR_AltHud.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/RR_DoomStatusBar.zs"
#include "classes/rrwm/ui/hud/fragments/RR_VitalsFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_AmmoFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_WeaponSlotsFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_WeaponSlotFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_LevelStatsFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_MugshotFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_InventoryFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_ItemFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_FragsFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_AirSupplyFragment.zs"
#include "classes/rrwm/ui/hud/fragments/RR_EnemyTracer.zs"
#include "classes/rrwm/ui/hud/fragments/RR_EnemyHealthFragment.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/fragments/RR_LowAmmoFragment.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/adapters/RR_KeysAdapter.zs"
#include "classes/rrwm/ui/hud/adapters/RR_AmmoTableAdapter.zs"
#include "classes/rrwm/ui/hud/adapters/RR_AmmoTypeHolder.zs"
#include "classes/rrwm/ui/hud/adapters/RR_WeaponSlotAdapter.zs"
#include "classes/rrwm/ui/hud/adapters/RR_PowerupsAdapter.zs"
#include "classes/rrwm/ui/hud/adapters/RR_PowerupHolder.zs"
#include "classes/rrwm/ui/hud/counters/stats/RR_LevelStatsCounter.zs"
#include "classes/rrwm/ui/hud/counters/stats/RR_LevelStatsCounterState.zs"
#include "classes/rrwm/ui/hud/counters/stats/RR_LevelStatsCounterKills.zs"
#include "classes/rrwm/ui/hud/counters/stats/RR_LevelStatsCounterItems.zs"
#include "classes/rrwm/ui/hud/counters/stats/RR_LevelStatsCounterSecrets.zs"
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounter.zs"
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterState.zs"
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterHealth.zs"
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterArmor.zs"
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterHexenArmor.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterShield.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/hud/counters/vitals/RR_VitalsCounterAir.zs"

// Menu
#include "classes/rrwm/ui/menu/RR_MenuBase.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/RR_MenuStringHelper.zs"
#include "classes/rrwm/ui/menu/RR_RootMenu.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/options/RR_OptionsMenuBase.zs"
#include "classes/rrwm/ui/menu/options/RR_OptionsMenuParameters.zs"
#include "classes/rrwm/ui/menu/options/RR_OptionsMenuParametersBuilder.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/options/by_category/RR_OptionsMenuGameplay.zs"
#include "classes/rrwm/ui/menu/options/by_category/RR_OptionsMenuWeapons.zs"
#include "classes/rrwm/ui/menu/options/by_category/RR_OptionsMenuCosmetic.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/options/by_category/RR_OptionsMenuHud.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/options/by_scope/RR_OptionsMenuUser.zs"
#include "classes/rrwm/ui/menu/options/by_scope/RR_OptionsMenuServer.zs"
#include "classes/rrwm/ui/menu/options/by_scope/RR_OptionsMenuServerPersistent.zs"
#include "classes/rrwm/ui/menu/options/classic/RR_OptionsMenuClassic.zs"
//</ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/options/classic/RR_OptionsMenuHudClassic.zs"
#include "classes/rrwm/ui/menu/options/help/RR_OptionsMenuHelp.zs"
#include "classes/rrwm/ui/menu/options/help/RR_HelpTitleFormatter.zs"
#include "classes/rrwm/ui/menu/items/RR_MenuItemOption.zs"
#include "classes/rrwm/ui/menu/items/RR_MenuItemSlider.zs"
//<ExcludeFromHudPackage>
#include "classes/rrwm/ui/menu/items/RR_MenuItemApplyChanges.zs"
//</ExcludeFromHudPackage>
