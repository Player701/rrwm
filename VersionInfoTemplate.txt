//!
//! This is a template for the build system to produce a ZScript file
//! containing version informaton. The resulting file is named RR_VersionInfo.zs
//! and put in the "classes" subfolder in the archive.
//! The template file itself will not be included in the PK3.
//!
//! Also, all lines starting with "//!" will be be ignored when generating the file.
//!
//===========================================================================
//
// RR_VersionInfo
//
// Contains RRWM version information.
//
// This file was generated automatically by the build system.
// Do not edit it directly. To change the structure of this file,
// edit %VERSION_TEMPLATE_NAME% in the root folder and rebuild.
//
//===========================================================================
class RR_VersionInfo
{
    // The version string.
    const VERSION_STRING    = "%VERSION_STRING%";

    // The timestamp of the version.
    const VERSION_TIMESTAMP = "%VERSION_TIMESTAMP%";
}
