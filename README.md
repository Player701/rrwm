# RRWM: Modern approach to classic gameplay

## Nutshell

* Vanilla-like weapons with reloading capability
* Slightly improved special effects
* Lots of extra options to tweak the gameplay to your personal preference
* Highly customizable HUD (also available standalone)
* Multiplayer support
* Compatible with mostly any PWAD that doesn't replace existing weapons or items (see below for details)

## Requirements

RRWM requires GZDoom 4.13.2 or later. For the optimal experience, it is recommended to always use the latest version of GZDoom. Get the latest GZDoom version from the [official download page](https://zdoom.org/downloads). See the [official wiki](https://zdoom.org/wiki/Installation_and_execution_of_ZDoom) for instructions on how to install GZDoom and play the mod.

LZDoom is deprecated and will not be supported anymore. Sorry about that.

## Core features

RRWM is a weapon mod that provides direct replacements for vanilla Doom weapons that function more or less the same, except for the reloading feature. It is intended especially for those who want a largely familiar, classic Doom experience, but also something new at the same time. The need to reload promotes a somewhat more cautious and less aggressive play style.

Additional changes to the gameplay include:

* The chainsaw now uses ammo, which is built-in and regenerates automatically when its engine is stopped. Use the reload key to turn the engine on and off (don't start it if you want to remain silent!). Each time you deal damage to a target, you receive a short-lived 50% damage reduction effect against frontal attacks, which takes place before damage is absorbed by armor. Additionally, you can improve engine efficiency by finding extra chainsaws to salvage for parts, which also immediately refills your ammo to maximum capacity. In single player, there is no limit on how many times you can upgrade, while in multiplayer all chainsaws spawn with a fixed number of upgrades already applied, and upgrading further is not possible.
* Berserk has been replaced with Adrenaline. In addition to increased melee damage that lasts until the end of the current level, it also makes melee attacks much faster and absorbs 50% of all incoming damage _after_ it has been reduced by armor. This effect lasts for 30 seconds and can be extended by picking up additional Adrenaline items. In an homage to Quake II, Adrenaline also boosts the player's normal maximum health (reachable with medikits and stimpacks only) by one extra point, up to 200.

## Optional features

RRWM also provides several optional features that can be adjusted to tailor the gameplay experience to your liking. Most of the core features are also customizable. All configuration is done through a special options menu, which is accessible directly from the main game menu. **To quickly access RRWM options, press Esc, R, and Enter keys in sequence.**

![Screenshot](/rrwm-options.png?raw=true)

Some of the options available in the menu only provide visual enhancements, while others have a certain impact on the gameplay. You can learn exactly what an option does by accessing its in-game help page by selecting the option and pressing F1.

Note that there are several features that can only be customized before starting a new game. This is done for a variety of reasons, but mostly to avoid needless complication of the code by making it account for sudden changes of the setting. The corresponding options are called "persistent" and have their own group in the options menu, so it's very easy to recognize them. Think of each of these options as of a mini-addon. Just don't forget that you have to adjust them before you start a new game (e.g. on the title screen).

## HUD

RRWM provides a replacement for the standard fullscreen HUD built into GZDoom. Due to some aspects of the reloading system implementation, a custom HUD is required to display the amount of ammo in the current weapon's clip. RRWM's HUD is highly customizable, multiplayer-ready, and is also available as a standalone package.

If you don't like the new HUD, the original Doom status bar, rewritten from scratch to support RRWM weapons, is provided as an alternative. Failing that, it is also possible to use GZDoom's "Alternative HUD", updated for RRWM to provide a clip counter. Third-party HUDs will not work properly with new weapons and are thus not recommended for use.

## Multiplayer

RRWM fully supports multiplayer and has been extensively playtested in cooperative mode. Competitive modes are supported as well but may have balance issues.

Starting from version 1.3.0, RRWM will check that all players are running the same version of the mod and abort the game with a user-friendly error message if it detects a version mismatch. If your network game de-syncs, verify that everyone is running the same GZDoom version and loading the same files in the same order. If you haven't found any inconsistencies, please report a bug. Other multiplayer glitches not related to synchronization should also be considered bugs and reported. Please see the "Reporting bugs" section below for how to report bugs.

**NB:** To apply changes to persistent options in a multiplayer game without restarting GZDoom, the settings menu has an additional command in multiplayer mode (aptly named "Apply changes"), which only becomes available when any changes to persistent options have been made. Note that the current level will be restarted and all players will have their health and inventory reset. Also, due to engine restrictions, this feature does not work in [hubs](https://zdoom.org/wiki/Hub). Changes can also be applied from the game console (default "~" key): type ```rrwm_restartlevel``` and press Enter.

## Compatibility

RRWM is **guaranteed** to be compatible with vanilla and limit-removing maps. Please report a bug if you are experiencing issues. If there is also a DEHACKED patch, please read the rest of this section for further information.

RRWM **will probably work** with other mods that replace existing enemies. Note that RRWM options that affect monsters will obviously do nothing if these monsters are replaced by a third-party mod. Starting from version 1.1, RRWM also detects DEHACKED modifications to monsters and allows them to take priority over its own replacements. Please report a bug if you are experiencing compatibility issues with a monster-altering DEHACKED patch.

RRWM **will probably work** with GZDoom-specific maps unless they have ACS scripts that manipulate the player's inventory. Unfortunately, it cannot be fixed in RRWM because the corresponding ACS functions do not take replacements into account. Custom inventory items, weapons, and enemies that do not replace any existing actors should not interfere with RRWM. Please report a bug if you are experiencing compatibility issues with a GZDoom map.

RRWM is **not guaranteed** to be compatible with other mods that replace existing weapons or items. This also applies to DEHACKED patches. Please do not report bugs if you encounter compatibility issues with such mods, unless you are able to reliably reproduce these issues without them.

**NB:** Starting from version 1.4.0, RRWM will detect and apply the following DEHACKED modifications:

* Ammo capacities
* Initial bullets

Note that the patch must not modify the weapons themselves, since these modifications cannot be carried over to RRWM weapons.

## Recommended map packs

Any PWAD with vanilla or Boom-compatible maps will generally play well with RRWM. However, "slaughtermaps" will become a lot more difficult since you can no longer keep shooting non-stop until you run out of ammo. The need to reload can be a serious tactical disadvantage when you don't have enough room to retreat; because of this, it is recommended to avoid levels that unleash numerous strong enemies on the player in confined spaces. Though if you still want to try, no one is stopping you -- but if you play without saves, you're in for a lot of frustration. Well, you practically asked for it, didn't you?

RRWM has been playtested in single player on the Ultra-Violence diffculty with the following IWADs and PWADs, which have been found to be completable without the use of cheat codes:

* Ultimate Doom
* Final Doom: TNT Evilution
* Final Doom: The Plutonia Experiment
* [Plutonia 2](https://www.doomworld.com/idgames/levels/doom2/megawads/pl2) (yes, even MAP32)
* [Alien Vendetta](https://www.doomworld.com/idgames/levels/doom2/megawads/av)
* [Armadosia](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/amdosia)
* [Hadephobia](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/h_phobia)
* [Plutonia: Revisited Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/prcp)
* [Unholy Realms](https://www.doomworld.com/idgames/levels/doom2/megawads/ur)
* [Reverie](https://www.doomworld.com/idgames/levels/doom2/megawads/reverie)
* [Jenesis](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/jenesis)
* [Ancient Aliens](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/aaliens)
* [NOVA: The Birth](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/nova)
* [NOVA II: New Dawn](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/nova2)
* [Doom 2 Unleashed](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/pc_cp)
* [Back to Saturn X E1](https://www.doomworld.com/idgames/levels/doom2/megawads/btsx_e1)
* [Back to Saturn X E2](https://www.doomworld.com/idgames/levels/doom2/megawads/btsx_e2)
* [Mutiny](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/mutiny)
* [Dark 7](https://www.doomworld.com/idgames/levels/doom2/Ports/d-f/dark7) and [mission pack](https://www.doomworld.com/idgames/levels/doom2/Ports/d-f/dark7mpk)
* [Akeldama](https://www.doomworld.com/idgames/levels/doom2/megawads/akeldama)
* [Double Impact](https://www.doomworld.com/idgames/levels/doom/Ports/d-f/dbimpact)
* [NOVA III](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/nova3)
* [EPIC](https://www.doomworld.com/idgames/levels/doom2/Ports/d-f/epic)
* [Warphouse](https://www.doomworld.com/idgames/levels/doom2/Ports/m-o/ma_wrp)
* [UAC Invasion: The Supply Depot](https://www.doomworld.com/idgames/levels/doom2/Ports/s-u/supplydp)
* [Port Glacia](https://www.doomworld.com/idgames/levels/doom2/Ports/p-r/portglacia)
* [Valhalla](https://www.doomworld.com/idgames/levels/doom2/Ports/m-o/ma_val)
* [TNT: Revilution](https://www.doomworld.com/idgames/levels/doom2/megawads/tntr)
* [The Darkening](https://www.doomworld.com/idgames/levels/doom2/megawads/darken)
* [The Darkening Episode 2](https://www.doomworld.com/idgames/levels/doom2/megawads/darken2)
* [Speed of Doom](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/sodfinal) (**NB:** disable rocket smoke or turn off "Replace Cyberdemon's rockets" on MAP30)
* [Resurgence](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/resurge)
* [007: Licence to Spell DooM](https://www.doomworld.com/idgames/levels/doom/Ports/0-9/007ltsd)
* [EPIC 2](https://www.doomworld.com/idgames/levels/doom2/megawads/epic2)
* [Moonblood](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/moonbld)
* [Sunlust](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/sunlust) (**NB:** some fights are very difficult and/or require unconventional tactics)
* [Memento Mori](https://www.doomworld.com/idgames/themes/mm/mm_allup)
* [Memento Mori II](https://www.doomworld.com/idgames/themes/mm/mm2)
* [Requiem](https://www.doomworld.com/idgames/levels/doom2/megawads/requiem)
* [2048 Unleashed](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/pc_cp2)
* [Corrupted Cistern](https://www.doomworld.com/idgames/?file=levels/doom2/Ports/a-c/cis-lutz)
* [Plutonia Revisited Community Project 2](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/prcp2)
* [Shotgun Symphony](https://www.doomworld.com/idgames/levels/doom/Ports/s-u/sgnsym)
* [Going Down](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/gd)
* [Newdoom Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/ndcp)
* [Newdoom Community Project 2 (fixed version)](https://www.wad-archive.com/wad/096840f579321ca852b05438b162ae5d5579ed2b)

Additionally, the following PWADs are recommended if you're looking for a map pack to try out RRWM with:

* Community Chest series ([first](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest), [second](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest2), [third](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest3), [fourth](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest4))
* [Icarus: Alien Vanguard](https://www.doomworld.com/idgames/themes/TeamTNT/icarus/icarus)
* [Whispers of Satan](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/wos)
* [Zones of Fear](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/zof)
* [Japanese Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/jpcp)

## Reporting bugs

Despite countless hours invested into playtesting, it is still possible that you may encounter a bug. If it looks to you that something in RRWM is not working as it should, then it's probably a bug and you're encouraged to report it so that it can be fixed.

[**Click here to report a bug.**](https://bitbucket.org/Player701/rrwm/issues/new)

When reporting bugs, please follow the standard GZDoom bug reporting guidelines. Due to the nature of this project, it's usually not necessary to construct a test map for each report. However, please provide a detailed list of reproduction steps -- what exactly should I do to experience buggy behavior? Screenshots and videos are optional and not a substitute for a proper testing procedure. If you are playing with some other mod (mind the compatibility notes above!), please check whether the issue appears only when you include that mod, or if the issue can be reproduced without the mod. Please provide links to all third-party content you are using, including the map pack (if any).

## Sprite contributions

RRWM suffers from the lack of its own original sprites. The current sprites were initially used as placeholders, but I've grown used to them over time. Yet, it would be much better if there were some original artwork in the mod.

Unfortunately, I'm not an artist and can only do very simple picture editing at best (e.g. add an alpha channel, crop an image, perform minor per-pixel edits), so anyone who wants to contribute sprites to this project will be welcomed with open arms. However, please note that not every sprite set can be included in RRWM. Due to the specific goals pursued by this project, the following requirements must be met for weapon sprites:

* They must have widescreen support. There are some entries on Realm667 I could've used in RRWM if not for this problem -- the sprites look incomplete in widescreen resolutions because the sprite author apparently thought that no one plays Doom in widescreen.
* They must resemble the original weapon at least on the very basic level. For example, a sniper rifle will not work as a replacement for any of the weapons, because there is simply no such thing as a sniper rifle in Doom. On the other hand, a bulky, futuristic-looking energy weapon could be a fitting replacement for the BFG. The only exception is the Railgun, which is an RRWM addition, but then again, a sniper rifle would probably not pass for a railgun either. (Also, there is no requirement for a Railgun replacement to have a scope on it.)
* They must be drawn in "Doom stlye". At the very least, this implies that standard Doomguy hands must be present (if there are any frames with hands, of course). The style of the weapon itself matters too, as it must be kept consistent with that of other weapons. The current BFG replacement is a good example of how far the style can deviate from the average.
* There has to be a distinguishable reloading animation. Sorry, I wish I could draw one myself, but, like I said, I'm not an artist... Also, the animation should not be limited to simply moving the sprite down and to the right (or left), indicating that the actual manipulations with the weapon are happening off-screen. It doesn't look convincing in nearly every case. Again, really sorry for that, but I have tried such things before and they all look terrible -- the current sprite set for the Railgun is a perfect example. The Chainsaw and the Fist are, of course, exempt from this requirement.
* In addition to the above. Examples of a good reloading animation in the current version of RRWM: Rocket Launcher, Plasma Rifle, Super Shotgun, Shotgun, Chaingun. The Pistol is only slightly worse than the others since we don't actually see the clip being ejected, but it's still fairly decent and convincing. The BFG is the worst example in the current version, but even then, there is some real animation there and not just moving the sprite around. (No offense here, just personal opinion, and I'm in no way saying that the sprites or the animations are bad, quite the contrary!)
* It is preferable if the contributed sprite set also has sprites for ammo pickups. This applies mainly to pistol, chaingun, rocket launcher and railgun replacements (cell ammo has a very fitting pickup sprite right now IMO, but if you have a better option to suggest, go ahead!). Same for empty clip sprites -- they are specific for each weapon and it's difficult to re-use them with other sprite sets.

If anyone has a sprite set they wish to contribute to RRWM, there is a fairly high chance that it will eventually make it to the next stable release, provided it meets all of the above requirements. The author of the sprites will be properly credited in the RRWM credits list file bundled with the next release.
