//===========================================================================
//
// AxisVertical
//
// Represents the vertical axis.
//
//===========================================================================
class WL_AxisVertical : WL_Axis
{
    //===========================================================================
    //
    // AxisVertical::CreateCross
    //
    // Returns the horizontal axis.
    //
    //===========================================================================
    override WL_Axis CreateCross()
    {
        return new('WL_AxisHorizontal');
    }

    //===========================================================================
    //
    // AxisVertical::GetOrientation
    //
    // Returns the vertical orientation.
    //
    //===========================================================================
    override WL_Orientation GetOrientation() const
    {
        return WL_O_Vertical;
    }

    //===========================================================================
    //
    // AxisVertical::GetPosition
    //
    // Returns the Y coordinate of the provided vector.
    //
    //===========================================================================
    override double GetPosition(vector2 pos) const
    {
        return pos.Y;
    }

    //===========================================================================
    //
    // AxisVertical::GetLength
    //
    // Returns the height of a Size instance.
    //
    //===========================================================================
    override double GetLength(WL_Size size) const
    {
        return size.GetHeight();
    }

    //===========================================================================
    //
    // AxisVertical::SetLength
    //
    // Sets the height of a Size instance along this axis to the specified value.
    //
    //===========================================================================
    override void SetLength(WL_Size size, double newLength, out WL_Size result) const
    {
        size.SetHeight(newLength, result);
    }

    //===========================================================================
    //
    // AxisVertical::GetRectPos
    //
    // Returns the Y coordinate of the starting point of a Rectangle instance.
    //
    //===========================================================================
    override double GetRectPos(WL_Rectangle rect) const
    {
        return rect.GetTop();
    }

    //===========================================================================
    //
    // AxisVertical::SetSizeAndPosition
    //
    // Sets the size of a Rectangle instance to the provided value
    // and also chages its position along the vertical axis.
    //
    //===========================================================================
    override void SetSizeAndPosition(WL_Rectangle rect, double newPos, WL_Size newSize, out WL_Rectangle result) const
    {
        rect.SetTopAndSize(newPos, newSize, result);
    }
}
