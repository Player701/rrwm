//===========================================================================
//
// Axis
//
// Provides a common interface for geometric operations
// on a single axis.
//
//===========================================================================
class WL_Axis abstract ui
{
    private WL_Axis _cross;

    //===========================================================================
    //
    // Axis::Horizontal
    //
    // Creates an Axis instance representing the horizontal axis.
    //
    //===========================================================================
    static WL_Axis Horizontal()
    {
        return WL_Axis.FromOrientation(WL_O_Horizontal);
    }

    //===========================================================================
    //
    // Axis::Vertical
    //
    // Creates an Axis instance representing the vertical axis.
    //
    //===========================================================================
    static WL_Axis Vertical()
    {
        return WL_Axis.FromOrientation(WL_O_Vertical);
    }

    //===========================================================================
    //
    // Axis::FromOrientation
    //
    // Returns an Axis instance that corresponds to the provided
    // orientation value.
    //
    //===========================================================================
    static WL_Axis FromOrientation(WL_Orientation orientation)
    {
        WL_Axis result;

        switch(orientation)
        {
            case WL_O_Horizontal:
                result = new('WL_AxisHorizontal');
                break;
            case WL_O_Vertical:
                result = new('WL_AxisVertical');
                break;
            default:
                ThrowAbortException("WL_Axis: Invalid orientation value %d.", orientation);
                return null;
        }

        // Link the cross axis to the created one
        // to avoid allocating it anew every time.
        let ca = result.CreateCross();
        result.Init(ca);
        ca.Init(result);

        return result;
    }

    //===========================================================================
    //
    // Axis::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_Axis ca)
    {
        _cross = ca;
    }

    //===========================================================================
    //
    // Axis::CreateCross
    //
    // Creates an Axis instance that corresponds to the cross (e.g. orthogonal)
    // axis.
    //
    //===========================================================================
    protected abstract WL_Axis CreateCross() const;

    //===========================================================================
    //
    // Axis::GetCross
    //
    // Returns an Axis instance that corresponds to the cross (e.g. orthogonal)
    // axis.
    //
    //===========================================================================
    WL_Axis GetCross() const
    {
        return _cross;
    }

    //===========================================================================
    //
    // Axis::GetOrientation
    //
    // Returns the orientation this axis corresponds to.
    //
    //===========================================================================
    abstract WL_Orientation GetOrientation() const;

    //===========================================================================
    //
    // Axis::GetPosition
    //
    // Returns the position of the provided vector
    // along this axis.
    //
    //===========================================================================
    abstract double GetPosition(vector2 pos) const;

    //===========================================================================
    //
    // Axis::GetLength
    //
    // Returns the length of a Size instance along this axis.
    //
    //===========================================================================
    abstract double GetLength(WL_Size size) const;

    //===========================================================================
    //
    // Axis::SetLength
    //
    // Sets the length of a Size instance along this axis to the specified value.
    //
    //===========================================================================
    abstract void SetLength(WL_Size size, double newLength, out WL_Size result = null) const;

    //===========================================================================
    //
    // Axis::GetRectLength
    //
    // Returns the length of a Rectangle instance along this axis.
    //
    //===========================================================================
    double GetRectLength(WL_Rectangle rect) const
    {
        WL_Size sz;
        rect.GetSize(sz);
        return GetLength(sz);
    }

    //===========================================================================
    //
    // Axis::GetRectPos
    //
    // Returns the position of a Rectangle instance along this axis.
    //
    //===========================================================================
    abstract double GetRectPos(WL_Rectangle rect) const;

    //===========================================================================
    //
    // Axis::RepositionAndResize
    //
    // Changes the size and position of a Rectangle instance along this axis
    // to the provided values.
    //
    //===========================================================================
    void RepositionAndResize(WL_Rectangle rect, double newPos, double newLength, out WL_Rectangle result = null) const
    {
        WL_Size sz;
        rect.GetSize(sz);

        SetLength(sz, newLength);
        SetSizeAndPosition(rect, newPos, sz, result);
    }

    //===========================================================================
    //
    // Axis::SetSizeAndPosition
    //
    // Sets the size of a Rectangle instance to the provided value
    // and also chages its position along this axis.
    //
    //===========================================================================
    protected abstract void SetSizeAndPosition(WL_Rectangle rect, double newPos, WL_Size newSize, out WL_Rectangle result = null) const;

    //===========================================================================
    //
    // Axis::Pad
    //
    // Returns the length of a Size instance along this axis with applied padding.
    //
    //===========================================================================
    double Pad(WL_Size size, WL_Margins padding)
    {
        WL_Size sz;
        size.Pad(padding, sz);
        return GetLength(sz);
    }

    //===========================================================================
    //
    // Axis::Unpad
    //
    // Returns the length of a Size instance along this axis
    // with applied negative padding.
    //
    //===========================================================================
    double Unpad(WL_Size size, WL_Margins padding)
    {
        WL_Size sz;
        size.Unpad(padding, sz);
        return GetLength(sz);
    }
}
