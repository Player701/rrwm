//===========================================================================
//
// AxisHorizontal
//
// Represents the horizontal axis.
//
//===========================================================================
class WL_AxisHorizontal : WL_Axis
{
    //===========================================================================
    //
    // AxisHorizontal::CreateCross
    //
    // Returns the vertical axis.
    //
    //===========================================================================
    override WL_Axis CreateCross() const
    {
        return new('WL_AxisVertical');
    }

    //===========================================================================
    //
    // AxisHorizontal::GetOrientation
    //
    // Returns the horizontal orientation.
    //
    //===========================================================================
    override WL_Orientation GetOrientation() const
    {
        return WL_O_Horizontal;
    }

    //===========================================================================
    //
    // AxisHorizontal::GetPosition
    //
    // Returns the X coordinate of the provided vector.
    //
    //===========================================================================
    override double GetPosition(vector2 pos) const
    {
        return pos.X;
    }

    //===========================================================================
    //
    // AxisHorizontal::GetLength
    //
    // Returns the width of a Size instance.
    //
    //===========================================================================
    override double GetLength(WL_Size size) const
    {
        return size.GetWidth();
    }

    //===========================================================================
    //
    // AxisHorizontal::SetLength
    //
    // Sets the width of a Size instance along this axis to the specified value.
    //
    //===========================================================================
    override void SetLength(WL_Size size, double newLength, out WL_Size result) const
    {
        size.SetWidth(newLength, result);
    }

    //===========================================================================
    //
    // AxisHorizontal::GetRectPos
    //
    // Returns the X coordinate of the starting point of a Rectangle instance.
    //
    //===========================================================================
    override double GetRectPos(WL_Rectangle rect) const
    {
        return rect.GetLeft();
    }

    //===========================================================================
    //
    // AxisHorizontal::SetSizeAndPosition
    //
    // Sets the size of a Rectangle instance to the provided value
    // and also chages its position along the horizontal axis.
    //
    //===========================================================================
    override void SetSizeAndPosition(WL_Rectangle rect, double newPos, WL_Size newSize, out WL_Rectangle result) const
    {
        rect.SetLeftAndSize(newPos, newSize, result);
    }
}
