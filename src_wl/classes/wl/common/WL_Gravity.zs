//===========================================================================
//
// Gravity
//
// Stores values for a view's gravity.
//
//===========================================================================
class WL_Gravity ui
{
    private WL_HorizontalGravity _horizontal;
    private WL_VerticalGravity _vertical;

    //===========================================================================
    //
    // Gravity::Create
    //
    // Creates a new instance of the Gravity class.
    //
    //===========================================================================
    static WL_Gravity Create(WL_HorizontalGravity horizontal, WL_VerticalGravity vertical)
    {
        let g = new('WL_Gravity');
        g.Init(horizontal, vertical);

        return g;
    }

    //===========================================================================
    //
    // Gravity::Center
    //
    // Creates a new instance of the Gravity class
    // with both horizontal and vertical gravity set to center.
    //
    //===========================================================================
    static WL_Gravity Center()
    {
        return Create(WL_HG_Center, WL_VG_Center);
    }

    //===========================================================================
    //
    // Gravity::Undefined
    //
    // Creates a new instance of the Gravity class
    // with both horizontal and vertical gravity set to undefined.
    //
    //===========================================================================
    static WL_Gravity Unspecified()
    {
        return Create(WL_HG_Unspecified, WL_VG_Unspecified);
    }

    //===========================================================================
    //
    // Gravity::Horizontal
    //
    // Creates a new instance of the Gravity class
    // with the specified horizontal gravity and the default vertical gravity.
    //
    //===========================================================================
    static WL_Gravity Horizontal(WL_HorizontalGravity value)
    {
        return Create(value, WL_VG_Unspecified);
    }

    //===========================================================================
    //
    // Gravity::Vertical
    //
    // Creates a new instance of the Gravity class
    // with the default horizontal gravity and the specified vertical gravity.
    //
    //===========================================================================
    static WL_Gravity Vertical(WL_VerticalGravity value)
    {
        return Create(WL_HG_Unspecified, value);
    }

    //===========================================================================
    //
    // Gravity::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_HorizontalGravity horizontal, WL_VerticalGravity vertical)
    {
        _horizontal = horizontal;
        _vertical = vertical;
    }

    //===========================================================================
    //
    // Gravity::GetHorizontal
    //
    // Returns the horizontal gravity.
    //
    //===========================================================================
    WL_HorizontalGravity GetHorizontal() const
    {
        return _horizontal;
    }

    //===========================================================================
    //
    // Gravity::GetVertical
    //
    // Returns the vertical gravity.
    //
    //===========================================================================
    WL_VerticalGravity GetVertical() const
    {
        return _vertical;
    }

    //===========================================================================
    //
    // Gravity::SpecifyHorizontal
    //
    // Returns the horizontal gravity unless it is unspecified,
    // in which case returns the horizontal gravity
    // of the provided other instance.
    //
    //===========================================================================
    WL_HorizontalGravity SpecifyHorizontal(WL_Gravity other) const
    {
        return _horizontal == WL_HG_Unspecified ? other._horizontal : _horizontal;
    }

    //===========================================================================
    //
    // Gravity::SpecifyVertical
    //
    // Returns the vertical gravity unless it is unspecified,
    // in which case returns the vertical gravity
    // of the provided other instance.
    //
    //===========================================================================
    WL_HorizontalGravity SpecifyVertical(WL_Gravity other) const
    {
        return _vertical == WL_VG_Unspecified ? other._vertical : _vertical;
    }

    //===========================================================================
    //
    // Gravity::Specify
    //
    // Returns a new Gravity instance, where horizontal and vertical gravity
    // are the same as this instance's except if they are set to Unspecified,
    // in which case the values from the other instance are used.
    //
    //===========================================================================
    WL_Gravity Specify(WL_Gravity other)
    {
        if (other == null)
        {
            ThrowAbortException("%s: Specifying gravity cannot be null.", GetClassName());
        }

        return WL_Gravity.Create(
            SpecifyHorizontal(other),
            SpecifyVertical(other)
        );
    }

    //===========================================================================
    //
    // Gravity::IsSpecified
    //
    // Returns true if neither horizontal nor vertical gravity is unspecified.
    //
    //===========================================================================
    bool IsSpecified() const
    {
        return _horizontal != WL_HG_Unspecified && _vertical != WL_VG_Unspecified;
    }

    //===========================================================================
    //
    // Gravity::Equals
    //
    // Checks if this Gravity instance is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Gravity other) const
    {
        return other != null && (self == other || _horizontal == other._horizontal && _vertical == other._vertical);
    }
//<DebugOnly>

    //===========================================================================
    //
    // Gravity::ToString
    //
    // Returns the string representation of this Gravity value.
    //
    //===========================================================================
    string ToString() const
    {
        return string.Format("[hor: %s, ver: %s]", HorizontalGravityToString(), VerticalGravityToString());
    }

    //===========================================================================
    //
    // Gravity::HorizontalGravityToString
    //
    // Returns the string representation of the horizontal gravity value.
    //
    //===========================================================================
    private string HorizontalGravityToString()
    {
        switch(_horizontal)
        {
            case WL_HG_Unspecified:
                return "unspecified";
            case WL_HG_Left:
                return "left";
            case WL_HG_Right:
                return "right";
            case WL_HG_Center:
                return "center";
        }

        ThrowAbortException("%s: Unknown horizontal gravity type %d.", GetClassName(), _horizontal);
        return "";
    }

    //===========================================================================
    //
    // Gravity::VerticalGravityToString
    //
    // Returns the string representation of the vertical gravity value.
    //
    //===========================================================================
    private string VerticalGravityToString()
    {
        switch(_vertical)
        {
            case WL_VG_Unspecified:
                return "unspecified";
            case WL_VG_Top:
                return "top";
            case WL_VG_Bottom:
                return "bottom";
            case WL_VG_Center:
                return "center";
        }

        ThrowAbortException("%s: Unknown vertical gravity type %d.", GetClassName(), _vertical);
        return "";
    }
//</DebugOnly>
}

//===========================================================================
//
// HorizontalGravity
//
// Enumerates possible values of horizontal gravity for a view.
//
//===========================================================================
enum WL_HorizontalGravity
{
    // No gravity (position determined by parent).
    WL_HG_Unspecified,

    // Anchors the view to the left edge of the parent.
    WL_HG_Left,

    // Anchors the view to the right edge of the parent.
    WL_HG_Right,

    // Centers the view horizontally.
    WL_HG_Center
};

//===========================================================================
//
// VerticalGravity
//
// Enumerates possible values of vertical gravity for a view.
//
//===========================================================================
enum WL_VerticalGravity
{
    // No gravity (position determined by parent).
    WL_VG_Unspecified,

    // Anchors the view to the top edge of the parent.
    WL_VG_Top,

    // Anchors the view to the bottom edge of the parent.
    WL_VG_Bottom,

    // Centers the view vertically.
    WL_VG_Center
};
