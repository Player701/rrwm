//===========================================================================
//
// VectorMath
//
// Contains some useful vector math functions.
//
//===========================================================================
class WL_VectorMath ui
{
    //===========================================================================
    //
    // VectorMath::Reciprocal
    //
    // Returns the reciprocal of the provided vector.
    //
    //===========================================================================
    static vector2 Reciprocal(vector2 v)
    {
        return (1.0 / v.X, 1.0 / v.y);
    }

    //===========================================================================
    //
    // VectorMath::IsPositive
    //
    // Returns true if both coordinates of the provided vector are positive.
    //
    //===========================================================================
    static bool IsPositive(vector2 v)
    {
        return v.X > 0 && v.Y > 0;
    }

    //===========================================================================
    //
    // VectorMath::MultiplyScales
    //
    // Multiplies the scaling factors and returns the result.
    //
    //===========================================================================
    static vector2 MultiplyScales(vector2 scale1, vector2 scale2)
    {
        return (scale1.X * scale2.X, scale1.Y * scale2.Y);
    }
}
