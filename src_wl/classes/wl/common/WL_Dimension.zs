//===========================================================================
//
// Dimension
//
// Represents a widget's dimension (width or height).
//
//===========================================================================
class WL_Dimension ui
{
    private WL_DimensionType _type;
    private double _value;

    //===========================================================================
    //
    // Dimension::Create
    //
    // Creates a new instance of the Dimension class.
    //
    //===========================================================================
    private static WL_Dimension Create(WL_DimensionType type, double value = 0)
    {
        let d = new('WL_Dimension');
        d.Init(type, value);

        return d;
    }

    //===========================================================================
    //
    // Dimension::Fixed
    //
    // Returns a Dimension instance with a fixed value.
    //
    //===========================================================================
    static WL_Dimension Fixed(double value)
    {
        return Create(WL_DT_Fixed, value);
    }

    //===========================================================================
    //
    // Dimension::MatchParent
    //
    // Returns a Dimension instance representing a value
    // matching the same value of the parent widget.
    //
    //===========================================================================
    static WL_Dimension MatchParent()
    {
        return Create(WL_DT_MatchParent);
    }

    //===========================================================================
    //
    // Dimension::WrapContent
    //
    // Returns a Dimension instance representing a value
    // determined by the widget's content.
    //
    //===========================================================================
    static WL_Dimension WrapContent()
    {
        return Create(WL_DT_WrapContent);
    }

    //===========================================================================
    //
    // Dimension::AtMost
    //
    // Returns a Dimension instance representing a value
    // determined by the widget's content, but also limited to a fixed value.
    //
    //===========================================================================
    static WL_Dimension AtMost(double value)
    {
        return Create(WL_DT_AtMost, value);
    }

    //===========================================================================
    //
    // Dimension::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_DimensionType type, double value)
    {
        if (value < 0)
        {
            ThrowAbortException("%s: Value cannot be negative.", GetClassName());
        }

        _type = type;
        _value = value;
    }

    //===========================================================================
    //
    // Dimension::GetType
    //
    // Returns the type of this dimension.
    //
    //===========================================================================
    WL_DimensionType GetType() const
    {
        return _type;
    }

    //===========================================================================
    //
    // Dimension::GetValue
    //
    // Returns the value of this dimension, provided it is of constant type.
    //
    //===========================================================================
    double GetValue() const
    {
        if (_type != WL_DT_Fixed && _type != WL_DT_AtMost)
        {
            ThrowAbortException("%s: Attempt to GetValue of a non-constant Dimension.", GetClassName());
        }

        return _value;
    }

    //===========================================================================
    //
    // Dimension::Equals
    //
    // Checks if this Dimension instance is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Dimension other) const
    {
        return other != null && (self == other
            || _type == other._type
            && ((_type != WL_DT_Fixed && _type != WL_DT_AtMost) || _value == other._value));
    }
//<DebugOnly>

    //===========================================================================
    //
    // Dimension::ToString
    //
    // Returns the string representation of this Dimension value.
    //
    //===========================================================================
    string ToString() const
    {
        switch(_type)
        {
            case WL_DT_Fixed:
                return string.Format("fixed (%f)", _value);
            case WL_DT_MatchParent:
                return "match parent";
            case WL_DT_WrapContent:
                return "wrap content";
            case WL_DT_AtMost:
                return string.Format("at most %f", _value);
        }

        ThrowAbortException("%s: Unknown dimension type %d.", GetClassName(), _type);
        return "";
    }
//</DebugOnly>
}

//===========================================================================
//
// DimensionType
//
// Enumerates possible values returned by Dimension::GetType.
//
//===========================================================================
enum WL_DimensionType
{
    // Represents a fixed size dimension.
    WL_DT_Fixed,

    // Represents a dimension with a value
    // matching the same dimension of the parent widget.
    WL_DT_MatchParent,

    // Represents a dimension with a value
    // determined by the widget's content.
    WL_DT_WrapContent,

    // Same as DT_WrapContent, but cannot exceed a fixed value.
    WL_DT_AtMost
};
