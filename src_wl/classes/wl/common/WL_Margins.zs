//===========================================================================
//
// Margins
//
// Represents a set of four values used to determine a widget's margins.
// Also used for layout padding.
//
//===========================================================================
class WL_Margins ui
{
    private double _left;
    private double _top;
    private double _right;
    private double _bottom;

    //===========================================================================
    //
    // Margins::Create
    //
    // Creates a new instance of the Margins class.
    //
    //===========================================================================
    static WL_Margins Create(double left, double top, double right, double bottom)
    {
        let m = new('WL_Margins');
        m.Init(left, top, right, bottom);

        return m;
    }

    //===========================================================================
    //
    // Margins::Horizontal
    //
    // Creates a new instance of the Margins class
    // with the specified horizontal margins and no vertical margins.
    //
    //===========================================================================
    static WL_Margins Horizontal(double left, double right)
    {
        return Create(left, 0, right, 0);
    }

    //===========================================================================
    //
    // Margins::BothHorizontal
    //
    // Creates a new instance of the Margins class
    // with the specified horizontal margins set to the same value
    // and no vertical margins.
    //
    //===========================================================================
    static WL_Margins BothHorizontal(double value)
    {
        return Horizontal(value, value);
    }

    //===========================================================================
    //
    // Margins::Vertical
    //
    // Creates a new instance of the Margins class
    // with no horizontal margins and the specified vertical margins.
    //
    //===========================================================================
    static WL_Margins Vertical(double top, double bottom)
    {
        return Create(0, top, 0, bottom);
    }

    //===========================================================================
    //
    // Margins::BothVertical
    //
    // Creates a new instance of the Margins class
    // with no horizontal margins
    // and the specified vertical margins set to the same value.
    //
    //===========================================================================
    static WL_Margins BothVertical(double value)
    {
        return Vertical(value, value);
    }

    //===========================================================================
    //
    // Margins::Left
    //
    // Creates a new instance of the Margins class
    // with the left margin set to the specified value
    // and all other margins set to zero.
    //
    //===========================================================================
    static WL_Margins Left(double value)
    {
        return Horizontal(value, 0);
    }

    //===========================================================================
    //
    // Margins::Right
    //
    // Creates a new instance of the Margins class
    // with the right margin set to the specified value
    // and all other margins set to zero.
    //
    //===========================================================================
    static WL_Margins Right(double value)
    {
        return Horizontal(0, value);
    }

    //===========================================================================
    //
    // Margins::Top
    //
    // Creates a new instance of the Margins class
    // with the top margin set to the specified value
    // and all other margins set to zero.
    //
    //===========================================================================
    static WL_Margins Top(double value)
    {
        return Vertical(value, 0);
    }

    //===========================================================================
    //
    // Margins::Bottom
    //
    // Creates a new instance of the Margins class
    // with the bottom margin set to the specified value
    // and all other margins set to zero.
    //
    //===========================================================================
    static WL_Margins Bottom(double value)
    {
        return Vertical(0, value);
    }

    //===========================================================================
    //
    // Margins:All
    //
    // Creates a new instance of the Margins class
    // with all margins set to the specified value.
    //
    //===========================================================================
    static WL_Margins All(double value)
    {
        return Create(value, value, value, value);
    }

    //===========================================================================
    //
    // Margins::Zero
    //
    // Creates a new instance of the Margins class
    // with all margins set to zero.
    //
    //===========================================================================
    static WL_Margins Zero()
    {
        return All(0);
    }

    //===========================================================================
    //
    // Margins::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(double left, double top, double right, double bottom)
    {
        _left = left;
        _top = top;
        _right = right;
        _bottom = bottom;
    }

    //===========================================================================
    //
    // Margins::GetLeft
    //
    // Returns the left margin.
    //
    //===========================================================================
    double GetLeft() const
    {
        return _left;
    }

    //===========================================================================
    //
    // Margins::GetTop
    //
    // Returns the top margin.
    //
    //===========================================================================
    double GetTop() const
    {
        return _top;
    }

    //===========================================================================
    //
    // Margins::GetRight
    //
    // Returns the right margin.
    //
    //===========================================================================
    double GetRight() const
    {
        return _right;
    }

    //===========================================================================
    //
    // Margins::GetBottom
    //
    // Returns the bottom margin.
    //
    //===========================================================================
    double GetBottom() const
    {
        return _bottom;
    }

    //===========================================================================
    //
    // Margins::Equals
    //
    // Checks if this Margins instance is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Margins other) const
    {
        return other != null && (self == other
            || _left == other._left
            && _top == other._top
            && _right == other._right
            && _bottom == other._bottom);
    }
//<DebugOnly>

    //===========================================================================
    //
    // Margins::ToString
    //
    // Returns the string representation of this Margins value.
    //
    //===========================================================================
    string ToString() const
    {
        return string.Format("[left: %f, top: %f, right: %f, bottom: %f]", _left, _top, _right, _bottom);
    }
//</DebugOnly>
}
