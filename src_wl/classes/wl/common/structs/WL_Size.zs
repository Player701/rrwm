//===========================================================================
//
// Size
//
// Represents a pair of width and height values.
//
//===========================================================================
struct WL_Size ui
{
    private bool _isDefined;

    private double _width;
    private double _height;

    //===========================================================================
    //
    // Size::Create
    //
    // Creates a Size with the specified parameters.
    //
    //===========================================================================
    void Create(double width, double height)
    {
        if (width < 0)
        {
            Object.ThrowAbortException("WL_Size: Width cannot be negative.");
        }

        if (height < 0)
        {
            Object.ThrowAbortException("WL_Size: Height cannot be negative.");
        }

        _isDefined = true;
        _width = width;
        _height = height;
    }

    //===========================================================================
    //
    // Size::CreateFrom
    //
    // Creates a Size by copying it from the provided value.
    //
    //===========================================================================
    void CreateFrom(WL_Size other)
    {
        if (other._isDefined)
        {
            Create(other._width, other._height);
        }
        else
        {
            Undefined();
        }
    }

    //===========================================================================
    //
    // Size::Undefined
    //
    // Restores this Size value to its undefined state.
    //
    //===========================================================================
    void Undefined()
    {
        _isDefined = false;
    }

    //===========================================================================
    //
    // Size::IsDefined
    //
    // Checks if this Size value is defined.
    //
    //===========================================================================
    bool IsDefined() const
    {
        return _isDefined;
    }

    //===========================================================================
    //
    // Size::CheckDefined
    //
    // Throws an abort exception if this Size value is undefined.
    //
    //===========================================================================
    private void CheckDefined()
    {
        if (!_isDefined)
        {
            Object.ThrowAbortException("WL_Size: Attempt to get contents of an undefined value.");
        }
    }

    //===========================================================================
    //
    // Size::Zero
    //
    // Creates a new Size with both width and heigth set to zero.
    //
    //===========================================================================
    void Zero()
    {
        Create(0, 0);
    }

    //===========================================================================
    //
    // Size::GetMax
    //
    // Creates a new Size that has the maximum width and height
    // among the two values provided.
    //
    //===========================================================================
    void GetMax(WL_Size size1, WL_Size size2)
    {
        Create(
            Max(size1.GetWidth(), size2.GetWidth()),
            Max(size1.GetHeight(), size2.GetHeight())
        );
    }

    //===========================================================================
    //
    // Size::GetWidth
    //
    // Returns the width.
    //
    //===========================================================================
    double GetWidth() const
    {
        CheckDefined();
        return _width;
    }

    //===========================================================================
    //
    // Size::GetHeight
    //
    // Returns the height.
    //
    //===========================================================================
    double GetHeight() const
    {
        CheckDefined();
        return _height;
    }

    //===========================================================================
    //
    // Size::GetArea
    //
    // Returns the area that this Size encompasses.
    //
    //===========================================================================
    double GetArea() const
    {
        CheckDefined();
        return _width * _height;
    }

    //===========================================================================
    //
    // Size::ToVector
    //
    // Returns a vector containing this Size's width and height values.
    //
    //===========================================================================
    vector2 ToVector() const
    {
        CheckDefined();
        return (_width, _height);
    }

    //===========================================================================
    //
    // Size::ClipBy
    //
    // Clips this Size's dimensions according the provided bounds.
    //
    //===========================================================================
    void ClipBy(WL_Size bounds, out WL_Size result = null)
    {
        (result != null ? result : self).Create(
            Min(GetWidth(), bounds.GetWidth()),
            Min(GetHeight(), bounds.GetHeight())
        );
    }

    //===========================================================================
    //
    // Size::Pad
    //
    // Applies padding to this Size.
    //
    //===========================================================================
    void Pad(WL_Margins padding, out WL_Size result = null)
    {
        DoPad(padding, 1, result);
    }

    //===========================================================================
    //
    // Size::Unpad
    //
    // Applies negative padding to this Size.
    //
    //===========================================================================
    void Unpad(WL_Margins padding, out WL_Size result = null)
    {
        DoPad(padding, -1, result);
    }

    //===========================================================================
    //
    // Size::DoPad
    //
    // Applies padding to this Size, multiplied by the provided factor.
    //
    //===========================================================================
    private void DoPad(WL_Margins padding, double factor, out WL_Size result = null)
    {
        double newWidth = Max(0, GetWidth() - factor * (padding.GetLeft() + padding.GetRight()));
        double newHeight = Max(0, GetHeight() - factor * (padding.GetTop() + padding.GetBottom()));

        (result != null ? result : self).Create(newWidth, newHeight);
    }

    //===========================================================================
    //
    // Size::ScaleBy
    //
    // Multiplies this Size's dimensions by the provided scaling factors.
    //
    //===========================================================================
    void ScaleBy(vector2 scale, out WL_Size result = null)
    {
        (result != null ? result : self).Create(GetWidth() * scale.X, GetHeight() * scale.Y);
    }

    //===========================================================================
    //
    // Size::ScaleTo
    //
    // Scales this Size's dimensions proportionally so that at least one of them
    // is equal to the other Size's corresponding dimension, and the other one
    // is not greater than the other Size's corresponding dimension.
    //
    //===========================================================================
    void ScaleTo(WL_Size to, out WL_Size result = null)
    {
        double c = Min(
            to.GetWidth() / GetWidth(),
            to.GetHeight() / GetHeight()
        );

        ScaleBy((c, c), result);
    }

    //===========================================================================
    //
    // Size::SetWidth
    //
    // Changes the width of this Size, but keeps the height.
    //
    //===========================================================================
    void SetWidth(double newWidth, out WL_Size result = null)
    {
        (result != null ? result : self).Create(newWidth, GetHeight());
    }

    //===========================================================================
    //
    // Size::SetHeight
    //
    // Changes theheight of this size, but keeps the width.
    //
    //===========================================================================
    void SetHeight(double newHeight, out WL_Size result = null)
    {
        (result != null ? result : self).Create(GetWidth(), newHeight);
    }

    //===========================================================================
    //
    // Size::SetAxisLength
    //
    // Changes either the width or height of this Size depending on
    // the orientation of the provided axis.
    //
    //===========================================================================
    void SetAxisLength(WL_Axis axis, double value, out WL_Size result = null)
    {
        axis.SetLength(self, value, result);
    }

    //===========================================================================
    //
    // Size::IsLessThan
    //
    // Returns true if both dimensions of this Size instance
    // are less than those of the other Size instance.
    //
    //===========================================================================
    bool IsLessThan(WL_Size other) const
    {
        return GetWidth() < other.Getwidth() && GetHeight() < other.GetHeight();
    }

    //===========================================================================
    //
    // Size::Equals
    //
    // Checks if this Size is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Size other) const
    {
        return other != null && _isDefined == other._isDefined && (!_isDefined
            || _width == other._width && _height == other._height);
    }
//<DebugOnly>

    //===========================================================================
    //
    // Size::ToString
    //
    // Returns the string representation of this Size.
    //
    //===========================================================================
    string ToString() const
    {
        return _isDefined ? string.Format("%fx%f", _width, _height) : "(undefined)";
    }
//</DebugOnly>
}
