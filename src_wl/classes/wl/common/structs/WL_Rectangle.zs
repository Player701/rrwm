//===========================================================================
//
// Rectangle
//
// Represents a rectangle with fixed on-screen coordinates and size.
//
//===========================================================================
struct WL_Rectangle ui
{
    private bool _isDefined;

    vector2 _pos;
    WL_Size _size;

    //===========================================================================
    //
    // Rectangle::Create
    //
    // Creates a Rectangle with the specified parameters.
    //
    //===========================================================================
    void Create(vector2 pos, WL_Size size)
    {
        if (size == null || !size.IsDefined())
        {
            Object.ThrowAbortException("WL_Rectangle: Size cannot be null or undefined.");
        }

        _isDefined = true;
        _pos = pos;
        _size.CreateFrom(size);
    }

    //===========================================================================
    //
    // Rectangle::CreateFrom
    //
    // Creates a Rectangle by copying it from the provided value.
    //
    //===========================================================================
    void CreateFrom(WL_Rectangle other)
    {
        if (other._isDefined)
        {
            Create(other._pos, other._size);
        }
        else
        {
            Undefined();
        }
    }

    //===========================================================================
    //
    // Rectangle::Undefined
    //
    // Restores this Rectangle value to its undefined state.
    //
    //===========================================================================
    void Undefined()
    {
        _isDefined = false;
    }

    //===========================================================================
    //
    // Rectangle::IsDefined
    //
    // Checks if this Rectangle value is defined.
    //
    //===========================================================================
    bool IsDefined() const
    {
        return _isDefined;
    }

    //===========================================================================
    //
    // Rectangle::CheckDefined
    //
    // Throws an abort exception if this Rectangle value is undefined.
    //
    //===========================================================================
    private void CheckDefined()
    {
        if (!_isDefined)
        {
            Object.ThrowAbortException("WL_Rectangle: Attempt to get contents of an undefined value.");
        }
    }

    //===========================================================================
    //
    // Rectangle::CreateLT
    //
    // Creates a Rectangle with the specified left and top coordinates and size.
    //
    //===========================================================================
    void CreateLT(double left, double top, WL_Size size)
    {
        Create((left, top), size);
    }

    //===========================================================================
    //
    // Rectangle::CreateLTWH
    //
    // Creates a Rectangle with the specified left and top coordinates
    // and width and height.
    //
    //===========================================================================
    void CreateLTWH(double left, double top, double width, double height)
    {
        WL_Size sz;
        sz.Create(width, height);
        Create((left, top), sz);
    }

    //===========================================================================
    //
    // Rectangle::Zero
    //
    // Creates a Rectangle that has a zero starting point and a zero size.
    //
    //===========================================================================
    void Zero()
    {
        WL_Size sz;
        sz.Zero();
        CreateLT(0, 0, sz);
    }

    //===========================================================================
    //
    // Rectangle::GetScreen
    //
    // Returns a rectangle that represents the screen.
    //
    //===========================================================================
    void GetScreen()
    {
        CreateLTWH(0, 0, Screen.GetWidth(), Screen.GetHeight());
    }

    //===========================================================================
    //
    // Rectangle::GetLeft
    //
    // Returns the X coordinate of the top left point of this rectangle.
    //
    //===========================================================================
    double GetLeft() const
    {
        CheckDefined();
        return _pos.X;
    }

    //===========================================================================
    //
    // Rectangle::GetTop
    //
    // Returns the Y coordinate of the top left point of this rectangle.
    //
    //===========================================================================
    double GetTop() const
    {
        CheckDefined();
        return _pos.Y;
    }

    //===========================================================================
    //
    // Rectangle::GetRight
    //
    // Returns the X coordinate of the bottom right point of this rectangle.
    //
    //===========================================================================
    double GetRight() const
    {
        return GetLeft() + GetWidth();
    }

    //===========================================================================
    //
    // Rectangle::GetBottom
    //
    // Returns the Y coordinate of the bottom right point of this rectangle.
    //
    //===========================================================================
    double GetBottom() const
    {
        return GetTop() + GetHeight();
    }

    //===========================================================================
    //
    // Rectangle::GetSize
    //
    // Returns the size of this rectangle.
    //
    //===========================================================================
    void GetSize(out WL_Size result) const
    {
        CheckDefined();
        result.CreateFrom(_size);
    }

    //===========================================================================
    //
    // Rectangle::GetWidth
    //
    // Returns the width of this rectangle.
    //
    //===========================================================================
    double GetWidth() const
    {
        CheckDefined();
        return _size.GetWidth();
    }

    //===========================================================================
    //
    // Rectangle::GetHeight
    //
    // Returns the height of this rectangle.
    //
    //===========================================================================
    double GetHeight() const
    {
        CheckDefined();
        return _size.GetHeight();
    }

    //===========================================================================
    //
    // Rectangle::GetTopLeft
    //
    // Returns the top left point of this rectangle.
    //
    //===========================================================================
    vector2 GetTopLeft() const
    {
        CheckDefined();
        return _pos;
    }

    //===========================================================================
    //
    // Rectangle::GetArea
    //
    // Returns the area of this rectangle.
    //
    //===========================================================================
    double GetArea() const
    {
        CheckDefined();
        return _size.GetArea();
    }

    //===========================================================================
    //
    // Rectangle::ClipBy
    //
    // Clips this Rectangle by the dimensions of the bounding rectangle.
    //
    //===========================================================================
    void ClipBy(WL_Rectangle bounds, out WL_Rectangle result = null)
    {
        double newLeft = Max(GetLeft(), bounds.GetLeft());
        double newTop = Max(GetTop(), bounds.GetTop());
        double newRight = Min(GetRight(), bounds.GetRight());
        double newBottom = Min(GetBottom(), bounds.GetBottom());

        (result != null ? result : self).CreateLTWH(newLeft, newTop, Max(0, newRight - newLeft), Max(0, newBottom - newTop));
    }

    //===========================================================================
    //
    // Rectangle::Pad
    //
    // Applies padding to this Rectangle.
    //
    //===========================================================================
    void Pad(WL_Margins padding, out WL_Rectangle result = null)
    {
        WL_Size sz;
        _size.Pad(padding, sz);

        (result != null ? result : self).CreateLT(
            GetLeft() + padding.GetLeft(),
            GetTop() + padding.GetTop(),
            sz
        );
    }

    //===========================================================================
    //
    // Rectangle::Resize
    //
    // Changes the size of this Rectangle, but keeps the position.
    //
    //===========================================================================
    void Resize(WL_Size newSize, out WL_Rectangle result = null)
    {
        (result != null ? result : self).Create(_pos, newSize);
    }

    //===========================================================================
    //
    // Rectangle::SetLeftAndSize
    //
    // Changes the left position and size of this Rectangle,
    // but keeps the top position.
    //
    //===========================================================================
    void SetLeftAndSize(double newLeft, WL_Size newSize, out WL_Rectangle result = null)
    {
        (result != null ? result : self).CreateLT(newLeft, GetTop(), newSize);
    }

    //===========================================================================
    //
    // Rectangle::SetTopAndSize
    //
    // Changes the top position and size of this Rectangle,
    // but keeps the left position.
    //
    //===========================================================================
    void SetTopAndSize(double newTop, WL_Size newSize, out WL_Rectangle result = null)
    {
        (result != null ? result : self).CreateLT(GetLeft(), newTop, newSize);
    }

    //===========================================================================
    //
    // Rectangle::ScaleBy
    //
    // Multiplies the size and position of this Rectangle by the provided
    // scaling factors.
    //
    //===========================================================================
    void ScaleBy(vector2 scale, out WL_Rectangle result = null)
    {
        WL_Size sz;
        _size.ScaleBy(scale, sz);

        (result != null ? result : self).CreateLT(
            GetLeft() * scale.X,
            GetTop() * scale.Y,
            sz
        );
    }

    //===========================================================================
    //
    // Rectangle::Gravitate
    //
    // Repositions this Rectangle within the specified bounds
    // according to gravity.
    //
    //===========================================================================
    void Gravitate(WL_Gravity gravity, WL_Rectangle bounds, out WL_Rectangle result = null)
    {
        DoGravitate(gravity.GetHorizontal(), gravity.GetVertical(), bounds, result);
    }

    //===========================================================================
    //
    // Rectangle::GravitateSpecified
    //
    // Repositions this Rectangle within the specified bounds
    // according to gravity, which is also specified by another gravity value.
    //
    //===========================================================================
    void GravitateSpecified(WL_Gravity primary, WL_Gravity secondary, WL_Rectangle bounds, out WL_Rectangle result = null)
    {
        DoGravitate(
            primary.SpecifyHorizontal(secondary),
            primary.SpecifyVertical(secondary),
            bounds,
            result
        );
    }

    //===========================================================================
    //
    // Rectangle::Gravitate
    //
    // Repositions this Rectangle within the specified bounds
    // according to the provided horizontal and vertical gravity values.
    //
    //===========================================================================
    private void DoGravitate(WL_HorizontalGravity hg, WL_VerticalGravity vg, WL_Rectangle bounds, out WL_Rectangle result)
    {
        double newLeft = GetLeft();
        double newTop = GetTop();

        switch(hg)
        {
            case WL_HG_Unspecified:
                break;
            case WL_HG_Left:
                newLeft = bounds.GetLeft();
                break;
            case WL_HG_Right:
                newLeft = bounds.GetLeft() + bounds.GetWidth() - GetWidth();
                break;
            case WL_HG_Center:
                newLeft = bounds.GetLeft() + (bounds.GetWidth() - GetWidth()) / 2;
                break;
            default:
                Object.ThrowAbortException("WL_Rectangle: Invalid horizontal gravity value %d.", hg);
                break;
        }

        switch(vg)
        {
            case WL_VG_Unspecified:
                break;
            case WL_VG_Top:
                newTop = bounds.GetTop();
                break;
            case WL_VG_Bottom:
                newTop = bounds.GetTop() + bounds.GetHeight() - GetHeight();
                break;
            case WL_VG_Center:
                newTop = bounds.GetTop() + (bounds.GetHeight() - GetHeight()) / 2;
                break;
            default:
                Object.ThrowAbortException("WL_Rectangle: Invalid vertical gravity value %d.", vg);
                break;
        }

        (result != null ? result : self).CreateLT(newLeft, newTop, _size);
    }

    //===========================================================================
    //
    // Rectangle::Equals
    //
    // Checks if this Rectangle is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Rectangle other) const
    {
        return other != null && _isDefined == other._isDefined && (!_isDefined
            || _pos == other._pos && _size.Equals(other._size));
    }
//<DebugOnly>

    //===========================================================================
    //
    // Rectangle::ToString
    //
    // Returns the string representation of this Rectangle.
    //
    //===========================================================================
    string ToString() const
    {
        return _isDefined ? string.Format("[%s at (%f; %f)]", _size.ToString(), _pos.X, _pos.Y) : "(undefined)";
    }
//</DebugOnly>
}
