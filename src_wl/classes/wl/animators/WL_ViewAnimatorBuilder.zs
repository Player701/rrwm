//===========================================================================
//
// ViewAnimatorBuilder
//
// A helper class that is used to build compound view animators.
//
//===========================================================================
class WL_ViewAnimatorBuilder ui
{
    private WL_ViewAnimator _current;

    //===========================================================================
    //
    // ViewAnimatorBuilder::Create
    //
    // Creates a new instance of the ViewAnimatorBuilder class.
    //
    //===========================================================================
    static WL_ViewAnimatorBuilder Create(WL_ViewAnimator animator)
    {
        let b = new('WL_ViewAnimatorBuilder');
        b.Init(animator);

        return b;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_ViewAnimator animator)
    {
        if (animator == null)
        {
            ThrowAbortException("%s: Animator cannot be null.", GetClassName());
        }

        _current = animator;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::GetResult
    //
    // Returns the resulting animator.
    //
    //===========================================================================
    WL_ViewAnimator GetResult() const
    {
        return _current;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::ObserveView
    //
    // A convenience method that immediately calls ObserveView
    // on the resulting animator without returning it.
    //
    //===========================================================================
    WL_ViewAnimator ObserveView(WL_View view) const
    {
        _current.ObserveView(view);
        return _current;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::ThenRun
    //
    // Combines the current animator with another animator, making them execute
    // in sequence.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder ThenRun(WL_ViewAnimator next)
    {
        _current = WL_SequentialAnimator.Create(_current, next);
        return self;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::ThenWaitFor
    //
    // Combines the current animator with a Waiter that will wait
    // for the provided number of cycles.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder ThenWaitFor(int duration)
    {
        return ThenRun(WL_Waiter.Create(duration));
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::CombineWith
    //
    // Combines the current animator with another animator, making them both
    // execute at the same time.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder CombineWith(WL_ViewAnimator other)
    {
        _current = WL_SimultaneousAnimator.Create(_current, other);
        return self;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::Repeat
    //
    // Wraps the current animator in a RepeatAnimator that will repeat
    // the animation the provided number of times.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder Repeat(int times)
    {
        _current = WL_RepeatAnimator.Create(_current, times);
        return self;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::RunFor
    //
    // Wraps the current animator in a DurationAnimator that will let
    // the animation run for the provided number of milliseconds.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder RunFor(double duration)
    {
        _current = WL_DurationAnimator.Create(_current, duration);
        return self;
    }

    //===========================================================================
    //
    // ViewAnimatorBuilder::RunIndefinitely
    //
    // Wraps the current animator in a Looper that will let the animation
    // run in an infinite loop.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder LoopForever()
    {
        _current = WL_Looper.Create(_current);
        return self;
    }
}
