//===========================================================================
//
// ViewAnimator
//
// Base class for view animators.
// Animators are a special type of ViewObserver that can also be combined
// with each other.
//
//===========================================================================
class WL_ViewAnimator : WL_ViewObserver abstract
{
    // Has the timed animation begun?
    private bool _animationRunning;

    // Last measured timer value
    private double _lastTimeF;

    // Quick access to animation speed CVar
    private CVar _animSpeed;

    //===========================================================================
    //
    // ViewAnimator::Observe
    //
    // Observes the PostResolveSize event by calling TryAnimate.
    // This method is called from outside the animator chain
    // (i.e. ViewObserverList).
    //
    //===========================================================================
    final override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        if (eventType == WL_VET_PostResolveSize)
        {
            return TryAnimate(view);
        }

        return true;
    }

    //===========================================================================
    //
    // ViewAnimator::TryAnimate
    //
    // Calls CanAnimate and if the result is true, also calls Animate,
    // then returns the result.
    //
    //===========================================================================
    bool TryAnimate(WL_View view)
    {
        bool result = CanAnimate(view);

        if (view.IsAnimationEnabled() && result)
        {
            if (!_animationRunning)
            {
                // Measure current time to calculate delta for the next step
                _lastTimeF = MSTimeF();
                _animationRunning = true;
            }
            else
            {
                double timeDelta = GetDeltaMs() * GetAnimationSpeedFactor();
                Animate(view, timeDelta);
            }
        }
        else if (_animationRunning)
        {
            // We will need to take new measurements again next time
            _animationRunning = false;
        }

        return result;
    }

    //===========================================================================
    //
    // ViewAnimator::GetDeltaMs
    //
    // Returns the amount of time in milliseconds that has passed
    // since the previous animation cycle.
    //
    //===========================================================================
    private double GetDeltaMs()
    {
        if (!_animationRunning)
        {
            ThrowAbortException("%s: Animation is not yet running.", GetClassName());
        }

        // Get current timer value
        double timeF = MSTimeF();

        // Measure result
        double result = timeF - _lastTimeF;

        // Remember last measured time
        _lastTimeF = timeF;

        return result;
    }

    //===========================================================================
    //
    // ViewAnimator::GetAnimationSpeedFactor
    //
    // Returns the animation speed factor.
    //
    //===========================================================================
    private double GetAnimationSpeedFactor()
    {
        if (_animSpeed == null)
        {
            _animSpeed = CVar.FindCVar('wl_animspeed');
        }

        if (_animSpeed != null)
        {
            // NB: ensure the value is positive
            return Max(_animSpeed.GetFloat(), 0.01);
        }

        return 1;
    }

    //===========================================================================
    //
    // ViewAnimator::CanAnimate
    //
    // Checks if this ViewAnimator can run at least one cycle of animation.
    //
    //===========================================================================
    protected virtual bool CanAnimate(WL_View view) const
    {
        return true;
    }

    //===========================================================================
    //
    // ViewAnimator::Animate
    //
    // Performs the actual animation. This should be called only if CanAnimate
    // has returned true.
    //
    //===========================================================================
    protected abstract void Animate(WL_View view, double timeDelta);

    //===========================================================================
    //
    // ViewAnimator::Reset
    //
    // Resets the internal state of this animator.
    //
    //===========================================================================
    virtual void Reset()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // ViewAnimator::BuildUpon
    //
    // Returns a ViewAnimatorBuilder to create a composite animator based upon
    // this ViewAnimator instance.
    //
    //===========================================================================
    WL_ViewAnimatorBuilder BuildUpon()
    {
        return WL_ViewAnimatorBuilder.Create(self);
    }
}
