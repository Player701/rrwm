//===========================================================================
//
// SimultaneousAnimator
//
// A type of SequentialAnimator that calls each inner animator
// simultaneously.
//
//===========================================================================
class WL_SimultaneousAnimator : WL_CompositeAnimator
{
    //===========================================================================
    //
    // SimultaneousAnimator::Create
    //
    // Creates a new instance of the SimultaneousAnimator class.
    //
    //===========================================================================
    static WL_SimultaneousAnimator Create(WL_ViewAnimator first, WL_ViewAnimator second)
    {
        let a = new('WL_SimultaneousAnimator');
        a.Init(first, second);

        return a;
    }

    //===========================================================================
    //
    // SimultaneousAnimator::CanAnimate
    //
    // Checks if at least one inner animator is capable of animating.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        bool result = false;

        for (int i = 0; i < GetCount(); i++)
        {
            result = GetInnerAnimator(i).CanAnimate(view);

            if (result)
            {
                break;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // SimultaneousAnimator::Animate
    //
    // Calls every inner animator simultaneously.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        int n = GetCount();

        for (int i = 0; i < n; i++)
        {
            let inner = GetInnerAnimator(i);

            if (inner.CanAnimate(view))
            {
                inner.Animate(view, timeDelta);
            }
        }
    }
}
