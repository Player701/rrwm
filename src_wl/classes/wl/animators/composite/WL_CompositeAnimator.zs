//===========================================================================
//
// CompositeAnimator
//
// A ViewAnimator that contains one or more inner animators.
//
//===========================================================================
class WL_CompositeAnimator : WL_ViewAnimator abstract
{
    private Array<WL_ViewAnimator> _innerAnimators;

    //===========================================================================
    //
    // CompositeAnimator::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_ViewAnimator first, WL_ViewAnimator second)
    {
        AddAnimatorOrContents(first);
        AddAnimatorOrContents(second);
    }

    //===========================================================================
    //
    // CompositeAnimator::AddAnimatorOrContents
    //
    // Adds the provided animator or its contents to the sequence.
    //
    //===========================================================================
    private void AddAnimatorOrContents(WL_ViewAnimator animator)
    {
        if (animator == null)
        {
            ThrowAbortException("%s: Animator cannot be null.", GetClassName());
        }

        if (GetClass() == animator.GetClass())
        {
            WL_CompositeAnimator(animator).AddTo(self);
        }
        else
        {
            Add(animator);
        }
    }

    //===========================================================================
    //
    // CompositeAnimator::AddTo
    //
    // Adds contents of this CompositeAnimator to another CompositeAnimator.
    //
    //===========================================================================
    private void AddTo(WL_CompositeAnimator other)
    {
        foreach (inner : _innerAnimators)
        {
            other.AddAnimatorOrContents(inner);
        }
    }

    //===========================================================================
    //
    // CompositeAnimator::Add
    //
    // Adds an animator to the sequence.
    //
    //===========================================================================
    private void Add(WL_ViewAnimator animator)
    {
        _innerAnimators.Push(animator);
    }

    //===========================================================================
    //
    // CompositeAnimator::GetInnerAnimator
    //
    // Returns the inner animator at the specified index.
    //
    //===========================================================================
    protected WL_ViewAnimator GetInnerAnimator(int index)
    {
        return _innerAnimators[index];
    }

    //===========================================================================
    //
    // CompositeAnimator::GetCount
    //
    // Returns the total number of inner animators.
    //
    //===========================================================================
    protected int GetCount()
    {
        return _innerAnimators.Size();
    }

    //===========================================================================
    //
    // CompositeAnimator::Reset
    //
    // Resets all inner animators.
    //
    //===========================================================================
    override void Reset()
    {
        foreach (inner : _innerAnimators)
        {
            inner.Reset();
        }
    }
}
