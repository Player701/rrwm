//===========================================================================
//
// RepeatAnimator
//
// A ViewAnimator that repeats the provided animation
// a fixed number of times.
//
//===========================================================================
class WL_RepeatAnimator : WL_WrapperAnimator
{
    private int _times;
    private int _count;

    //===========================================================================
    //
    // RepeatAnimator::Create
    //
    // Creates a new instance of the RepeatAnimator class.
    //
    //===========================================================================
    static WL_RepeatAnimator Create(WL_ViewAnimator inner, int times)
    {
        let a = new('WL_RepeatAnimator');
        a.Init(inner, times);

        return a;
    }

    //===========================================================================
    //
    // RepeatAnimator::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_ViewAnimator inner, int times)
    {
        Super.Init(inner);

        if (times < 0)
        {
            ThrowAbortException("%s: Number of repeats cannot be negative.", GetClassName());
        }

        _times = times;
    }

    //===========================================================================
    //
    // RepeatAnimator::Reset
    //
    // Resets the inner animator and sets the iteration count to 0.
    //
    //===========================================================================
    override void Reset()
    {
        Super.Reset();
        _count = 0;
    }

    //===========================================================================
    //
    // RepeatAnimator::CanAnimate
    //
    // Checks if the iteration counter has not run out and also that
    // the inner animator is capable of animating.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return _count < _times && Super.CanAnimate(view);
    }

    //===========================================================================
    //
    // RepeatAnimator::Animate
    //
    // Calls the inner animator, first resetting it fi CanAnimate returns false.
    // Each reset increases the iteration counter.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        let inner = GetInnerAnimator();

        if (!inner.CanAnimate(view))
        {
            inner.Reset();
            _count++;
        }

        inner.Animate(view, timeDelta);
    }
}
