//===========================================================================
//
// SequentialAnimator
//
// A ViewAnimator that contains a sequence of other animators.
// These animators are executed in sequence.
//
//===========================================================================
class WL_SequentialAnimator : WL_CompositeAnimator
{
    private int _curIndex;

    //===========================================================================
    //
    // SequentialAnimator::Create
    //
    // Creates a new instance of the SequentialAnimator class.
    //
    //===========================================================================
    static WL_SequentialAnimator Create(WL_ViewAnimator first, WL_ViewAnimator second)
    {
        let a = new('WL_SequentialAnimator');
        a.Init(first, second);

        return a;
    }

    //===========================================================================
    //
    // SequentialAnimator::Reset
    //
    // Resets each animator in the sequence and makes the animation
    // start from the first animator again.
    //
    //===========================================================================
    override void Reset()
    {
        Super.Reset();
        _curIndex = 0;
    }

    //===========================================================================
    //
    // SequentialAnimator::CanAnimate
    //
    // Checks if at least one inner animator, starting from the current index,
    // is capable of animating.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        bool result = false;

        for (int i = _curIndex; i < GetCount(); i++)
        {
            result = GetInnerAnimator(i).CanAnimate(view);

            if (result)
            {
                break;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // SequentialAnimator::Animate
    //
    // Calls the next animator that is capable of animating.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        int i = _curIndex;
        WL_ViewAnimator cur;

        // Find the first animator that is capable of animating
        // (We know it exists because if we're here, CanAnimate has returned true)
        while (!(cur = GetInnerAnimator(i)).CanAnimate(view))
        {
            i++;
        }

        cur.Animate(view, timeDelta);
        _curIndex = i;
    }
}
