//===========================================================================
//
// Looper
//
// A type of ViewAnimator that runs its inner animator indefinitely,
// resetting it if it returns false.
//
//===========================================================================
class WL_Looper : WL_WrapperAnimator
{
    //===========================================================================
    //
    // Looper::Create
    //
    // Creates a new instance of the Looper class.
    //
    //===========================================================================
    static WL_Looper Create(WL_ViewAnimator inner)
    {
        let a = new('WL_Looper');
        a.Init(inner);

        return a;
    }

    //===========================================================================
    //
    // Looper::CanAnimate
    //
    // Always returns true;.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return true;
    }

    //===========================================================================
    //
    // Looper::Animate
    //
    // Calls the inner animator and resets it if it returns false,
    // while always returning true.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        let inner = GetInnerAnimator();

        if (inner.CanAnimate(view))
        {
            inner.Animate(view, timeDelta);
        }
        else
        {
            // Reset the animator if it can no longer animate
            inner.Reset();
        }
    }
}
