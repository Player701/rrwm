//===========================================================================
//
// DurationAnimator
//
// Runs the inner animator for the specified number of milliseconds
// or until it can no longer perform the animation,
// depending on what comes first.
//
//===========================================================================
class WL_DurationAnimator : WL_WrapperAnimator
{
    private WL_ViewAnimator _waiter;

    //===========================================================================
    //
    // DurationAnimator::Create
    //
    // Creates a new instance of the DurationAnimator class.
    //
    //===========================================================================
    static WL_DurationAnimator Create(WL_ViewAnimator inner, double duration)
    {
        let a = new('WL_DurationAnimator');
        a.Init(inner, duration);

        return a;
    }

    //===========================================================================
    //
    // DurationAnimator::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_ViewAnimator inner, double duration)
    {
        Super.Init(inner);
        _waiter = WL_Waiter.Create(duration);
    }

    //===========================================================================
    //
    // DurationAnimator::Reset
    //
    // Resets the inner animator and sets the iteration count to zero.
    //
    //===========================================================================
    override void Reset()
    {
        Super.Reset();
        _waiter.Reset();
    }

    //===========================================================================
    //
    // DurationAnimator::CanAnimate
    //
    // Checks if the iteration counter has not run out and also that
    // the inner animator is capable of animating.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return _waiter.CanAnimate(view) && Super.CanAnimate(view);
    }

    //===========================================================================
    //
    // DurationAnimator::Animate
    //
    // Calls the inner animator and increments the iteration count.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        Super.Animate(view, timeDelta);
        _waiter.Animate(view, timeDelta);
    }
}
