//===========================================================================
//
// WrapperAnimator
//
// A type of ViewAnimator that contains an inner animator.
//
//===========================================================================
class WL_WrapperAnimator : WL_ViewAnimator abstract
{
    private WL_ViewAnimator _inner;

    //===========================================================================
    //
    // WrapperAnimator::Init
    //
    // Performs first-time initialiation.
    //
    //===========================================================================
    protected void Init(WL_ViewAnimator inner)
    {
        if (inner == null)
        {
            ThrowAbortException("%s: Inner animator cannot be null.", GetClassName());
        }

        _inner = inner;
    }

    //===========================================================================
    //
    // WrapperAnimator::GetInnerAnimator
    //
    // Returns the inner animator.
    //
    //===========================================================================
    protected WL_ViewAnimator GetInnerAnimator() const
    {
        return _inner;
    }

    //===========================================================================
    //
    // WrapperAnimator::CanAnimate
    //
    // Checks that the inner animator is capable of animating.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return _inner.CanAnimate(view);
    }

    //===========================================================================
    //
    // WrapperAnimator::Animate
    //
    // Calls Animate on the inner animator.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        _inner.Animate(view, timeDelta);
    }

    //===========================================================================
    //
    // WrapperAnimator::Reset
    //
    // Resets the inner animator to its initial state.
    //
    //===========================================================================
    override void Reset()
    {
        _inner.Reset();
    }
}
