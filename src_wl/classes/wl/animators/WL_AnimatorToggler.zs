//===========================================================================
//
// AnimatorToggler
//
// A special ViewObserver that can be used to toggle an animator on and off.
// The animator should implement one iteration of a closed loop,
// so that when animation is turned off, the view will have the same
// visual state as the one it had before turning the animation on.
//
//===========================================================================
class WL_AnimatorToggler : WL_ViewObserver
{
    //===========================================================================
    //
    // AnimatorTogglerState
    //
    // Enumerates possible states of the AnimatorToggler.
    //
    //===========================================================================
    enum AnimatorTogglerState
    {
        // Animation is off, and the animator is not called.
        ATS_Off,

        // Animation is on, and the animator is called
        // and will be reset when it can no longer animate.
        ATS_On,

        // Animation if turning off, and the animator is called
        // but only until it can no longer animate the view.
        ATS_TurningOff
    };

    private WL_ViewAnimator _animator;
    private AnimatorTogglerState _state;

    //===========================================================================
    //
    // AnimatorToggler::Create
    //
    // Creates a new instance of the AnimatorToggler class.
    //
    //===========================================================================
    static WL_AnimatorToggler Create(WL_ViewAnimator animator)
    {
        let o = new('WL_AnimatorToggler');
        o.Init(animator);

        return o;
    }

    //===========================================================================
    //
    // AnimatorToggler::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_ViewAnimator animator)
    {
        if (animator == null)
        {
            ThrowAbortException("%s: Animator cannot be null.", GetClassName());
        }

        _animator = animator;
    }

    //===========================================================================
    //
    // AnimatorToggler::Toggle
    //
    // Toggles the animation on or off.
    //
    //===========================================================================
    void Toggle(bool value, bool instant = false)
    {
        if (value)
        {
            // Turning the animator on
            // If the animator was off, reset it
            if (_state == ATS_Off)
            {
                _animator.Reset();
            }

            // Unconditional state switch to On
            _state = ATS_On;
        }
        else if (_state != ATS_Off)
        {
            // Turning the animator off
            if (instant)
            {
                // Instant turn-off: reset state
                _animator.Reset();
                _state = ATS_Off;
            }
            else
            {
                // Gradual turn-off
                _state = ATS_TurningOff;
            }
        }
    }

    //===========================================================================
    //
    // AnimatorToggler::Observe
    //
    // Triggers the animation and resets it according to the current state
    // of the AnimatorToggler.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        // Observe PostResolveSize and only do something if not Off
        if (eventType == WL_VET_PostResolveSize && _state != ATS_Off)
        {
            // Try to animate the view
            bool result = _animator.TryAnimate(view);

            // See what to do if the view can no longer be animated
            if (!result)
            {
                // The animation is turning off,
                // so set state to Off
                if (_state == ATS_TurningOff)
                {
                    _state = ATS_Off;
                }
                else
                {
                    // The animation is still on,
                    // so reset the animator.
                    _animator.Reset();
                }
            }
        }

        return true;
    }
}
