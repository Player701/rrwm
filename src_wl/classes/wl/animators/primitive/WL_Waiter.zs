//===========================================================================
//
// Waiter
//
// A type of ViewAnimator that does not animate anything,
// but rather simply waits for a predefined amount of time, in milliseconds.
//
//===========================================================================
class WL_Waiter : WL_ViewAnimator
{
    private double _duration;
    private double _timePassed;

    //===========================================================================
    //
    // Waiter::Create
    //
    // Creates a new instance of the Waiter class.
    //
    //===========================================================================
    static WL_Waiter Create(double duration)
    {
        let a = new('WL_Waiter');
        a.Init(duration);

        return a;
    }

    //===========================================================================
    //
    // Waiter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(double duration)
    {
        if (duration < 0)
        {
            ThrowAbortException("%s: Duration cannot be negative.", GetClassName());
        }

        _duration = duration;
    }

    //===========================================================================
    //
    // Waiter::Reset
    //
    // Resets the iteration counter.
    //
    //===========================================================================
    override void Reset()
    {
        _timePassed = 0;
    }

    //===========================================================================
    //
    // Waiter::CanAnimate
    //
    // Returns true if the iteration counter hasn't run out.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return _timePassed < _duration;
    }

    //===========================================================================
    //
    // Waiter::Animate
    //
    // Increases the amount of passed time by the value of the animation speed.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        _timePassed += timeDelta;
    }
}
