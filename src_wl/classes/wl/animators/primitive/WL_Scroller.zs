//===========================================================================
//
// Scroller
//
// Performs a scrolling action on a ScrollView.
// Scrolling speed is measured in units per millisecond.
//
//===========================================================================
class WL_Scroller : WL_SpeedAnimator
{
    private WL_ScrollType _type;
    private double _value;

    //===========================================================================
    //
    // Scroller::Create
    //
    // Creates a new instance of the Scroller class.
    //
    //===========================================================================
    static WL_Scroller Create(double speed, WL_ScrollType type, double value)
    {
        let s = new('WL_Scroller');
        s.Init(speed, type, value);

        return s;
    }

    //===========================================================================
    //
    // Scroller::ToStart
    //
    // Creates a new instance of Scroller class which scrolls to an edge
    // of the ScrollView with the provided absolute speed value.
    //
    // A positive speed value will scroll to the end of the ScrollView,
    // while a positive value will scroll to the start.
    //
    //===========================================================================
    static WL_Scroller ToEdge(double speed)
    {
        return Create(Abs(speed), WL_SCROLL_EDGE, speed);
    }

    //===========================================================================
    //
    // Scroller::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(double speed, WL_ScrollType type, double value)
    {
        // Cannot scroll to a negative target
        if (type == WL_SCROLL_TARGET && value < 0)
        {
            ThrowAbortException("%s: Target value cannot be negative.", GetClassName());
        }

        // If scrolling to an edge, the value must not be zero
        if (type == WL_SCROLL_EDGE && value == 0)
        {
            ThrowAbortException("%s: Scrolling direction cannot be zero.", GetClassName());
        }

        Super.Init(speed);
        _type = type;
        _value = value;
    }

    //===========================================================================
    //
    // Scroller::GetType
    //
    // Gets the type of this scroller.
    //
    //===========================================================================
    private WL_ScrollType GetType() const
    {
        return _type;
    }

    //===========================================================================
    //
    // Scroller::GetValue
    //
    // Gets the value of this scroller.
    //
    //===========================================================================
    private double GetValue() const
    {
        return _value;
    }

    //===========================================================================
    //
    // Scroller::CheckEdge
    //
    // Checks if the ScrollView's position along the scroll axis length
    // is at its minimum or maximum (depending on the sign of direction).
    //
    //===========================================================================
    private bool CheckEdge(WL_ScrollView scrollView, double direction)
    {
        double pos = scrollView.GetScrollPos();

        return direction < 0
            ? pos > 0
            : pos < scrollView.GetScrollPosMax();
    }

    //===========================================================================
    //
    // Scroller::CanAnimate
    //
    // Checks that the provided view is a ScrollView
    // and that scrolling it is possible according to its setup
    // as well as this Scroller's properties.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        let sv = WL_ScrollView(view);

        if (sv != null)
        {
            switch (_type)
            {
                case WL_SCROLL_DELTA:
                    return Abs(_value) > 0 && CheckEdge(sv, _value);
                case WL_SCROLL_TARGET:
                    double maxPos = sv.GetScrollPosMax();
                    return Abs(sv.GetScrollPos() - Min(maxPos, _value)) > 0;
                case WL_SCROLL_EDGE:
                    return CheckEdge(sv, _value);
            }

            // NB: this should not be reachable
            ThrowAbortException("%s: Invalid scroll type: %d.", GetClassName(), _type);
        }

        return false;
    }

    //===========================================================================
    //
    // Scroller::Animate
    //
    // Adjusts the view's scrolling position as needed.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        let sv = WL_ScrollView(view);

        if (sv == null)
        {
            // NB: this should not happen since CanAnimate
            // must have returned true by this point
            return;
        }

        double dl = GetSpeed() * timeDelta;

        switch (_type)
        {
            case WL_SCROLL_DELTA:
                // Ensure we do not scroll more than necessary
                dl = Min(dl, Abs(_value));
                // Scroll backward if distance is negative
                if (_value < 0) dl = -dl;
                // Subtract current delta from total distance
                _value -= dl;
                break;
            case WL_SCROLL_TARGET:
                // Ensure we do not overshoot
                double curPos = sv.GetScrollPos();
                dl = Min(dl, Abs(curPos - _value));
                // Scroll backward if we're past the target
                if (curPos > _value) dl = -dl;
                break;
            case WL_SCROLL_EDGE:
                // Check if scrolling backward
                if (_value < 0) dl = -dl;
                break;
        }

        sv.SetScrollPos(dl, true);
    }

    //===========================================================================
    //
    // Scroller::Accept
    //
    // Checks if the other observer is also a scroller with the same orientation,
    // and if it is, either overrides the current scroller's effect,
    // or, if possible, combines them.
    //
    //===========================================================================
    override bool Accept(WL_ViewObserver other)
    {
        let s = WL_Scroller(other);

        if (s == null)
        {
            return false;
        }

        if (!TryCombineWith(s))
        {
            // Scrollers cannot be combined, so replace
            // the current scrolling effect with the new one.
            SetSpeed(s.GetSpeed());
            _type = s.GetType();
            _value = s.GetValue();
        }

        return true;
    }

    //===========================================================================
    //
    // Scroller::TryCombineWith
    //
    // Attempts to combine the effects of the two scrollers,
    // and returns true if successful.
    //
    //===========================================================================
    private bool TryCombineWith(WL_Scroller other)
    {
        // We can only combine scrollers if they have matching types
        // and scrolling directions.
        if (_type == other.GetType() && (_value > 0) == (other.GetValue() > 0))
        {
            if (_type == WL_SCROLL_EDGE)
            {
                // Edge scrollers can be combined
                // by adding up their speeds.
                SetSpeed(GetSpeed() + other.GetSpeed());
                return true;
            }

            if (_type == WL_SCROLL_DELTA)
            {
                // Delta scrollers can be combined
                // by adding up their delta values
                // as well as averaging their speeds.
                SetSpeed((GetSpeed() + other.GetSpeed())/2);
                _value += other.GetValue();
                return true;
            }
        }

        return false;
    }
}

//===========================================================================
//
// ScrollType
//
// Enumerates possible scroll types.
//
//===========================================================================
enum WL_ScrollType
{
    // Scroll by the provided distance value.
    // Positive values scroll forward,
    // negative values scroll backward.
    WL_SCROLL_DELTA,

    // Scroll to the specified position value.
    // The value must be non-negative.
    WL_SCROLL_TARGET,

    // Scrolls to the ScrollView's edge.
    // Positive values scroll to the end,
    // negative values scroll to the start.
    WL_SCROLL_EDGE
};
