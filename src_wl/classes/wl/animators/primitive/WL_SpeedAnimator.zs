//===========================================================================
//
// SpeedAnimator
//
// Base class for animators that have a speed.
//
//===========================================================================
class WL_SpeedAnimator : WL_ViewAnimator abstract
{
    private double _speed;

    //===========================================================================
    //
    // SpeedAnimator::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(double speed)
    {
        SetSpeed(speed);
    }

    //===========================================================================
    //
    // SpeedAnimator::GetSpeed
    //
    // Returns the unadjusted speed.
    //
    //===========================================================================
    protected double GetSpeed() const
    {
        return _speed;
    }

    //===========================================================================
    //
    // SpeedAnimator::SetSpeed
    //
    // Sets the speed.
    //
    //===========================================================================
    protected void SetSpeed(double speed)
    {
        if (speed <= 0)
        {
            ThrowAbortException("%s: Speed must be positive.", GetClassName());
        }

        _speed = speed;
    }
}
