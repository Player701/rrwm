//===========================================================================
//
// Blinker
//
// A ViewAnimator that toggles the visibility of a view
// from visisble to hidden and vice-versa.
//
//===========================================================================
class WL_Blinker : WL_ViewAnimator
{
    private bool _done;

    //===========================================================================
    //
    // Blinker::Create
    //
    // Creates a new instance of the Blinker class.
    //
    //===========================================================================
    static WL_Blinker Create()
    {
        return new('WL_Blinker');
    }

    //===========================================================================
    //
    // Blinker::CanAnimate
    //
    // Returns true until Animate has been called at least once,
    // then always returns false.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view)
    {
        return !_done;
    }

    //===========================================================================
    //
    // Blinker::Reset
    //
    // Resets the state of this animator.
    //
    //===========================================================================
    override void Reset()
    {
        _done = false;
    }

    //===========================================================================
    //
    // Blinker::Animate
    //
    // Switches the view's visibility.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        let vis = view.GetVisibility();
        view.SetVisibility(vis == WL_V_Visible ? WL_V_Hidden : WL_V_Visible);

        _done = true;
    }
}
