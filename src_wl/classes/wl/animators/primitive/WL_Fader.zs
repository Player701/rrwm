//===========================================================================
//
// Fader
//
// Gradually alters a view's alpha towards the provided value.
// The speed is measured in alpha per millisecond.
//
//===========================================================================
class WL_Fader : WL_SpeedAnimator
{
    private double _dest;
    private double _time;

    //===========================================================================
    //
    // Fader::Create
    //
    // Creates a new instance of the Fader class.
    //
    //===========================================================================
    static WL_Fader Create(double dest, double speed)
    {
        let a = new('WL_Fader');
        a.Init(dest, speed);

        return a;
    }

    //===========================================================================
    //
    // Fader::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(double dest, double speed)
    {
        Super.Init(speed);

        if (dest < 0 || dest > 1)
        {
            ThrowAbortException("%s: Destination alpha must be between 0 and 1.", GetClassName());
        }

        _dest = dest;
    }

    //===========================================================================
    //
    // Fader::GetDest
    //
    // Returns the destination alpha value of this fader.
    //
    //===========================================================================
    protected double GetDest() const
    {
        return _dest;
    }

    //===========================================================================
    //
    // Fader::CanAnimate
    //
    // Checks if the view can be animated.
    //
    //===========================================================================
    override bool CanAnimate(WL_View view) const
    {
        return view.GetAlpha() != _dest;
    }

    //===========================================================================
    //
    // Fader::Animate
    //
    // Changes the view's alpha to get closer to the desired value.
    //
    //===========================================================================
    override void Animate(WL_View view, double timeDelta)
    {
        double curAlpha = view.GetAlpha();
        double diff = _dest - curAlpha;

        double da = Min(abs(diff), GetSpeed() * timeDelta);

        if (diff < 0)
        {
            da = -da;
        }

        view.SetAlpha(curAlpha + da);
    }

    //===========================================================================
    //
    // Fader::Accept
    //
    // Checks if the other observer is also a Fader, and if it is,
    // copies its destination value and speed.
    //
    //===========================================================================
    override bool Accept(WL_ViewObserver other)
    {
        let f = WL_Fader(other);

        if (f != null)
        {
            _dest = f.GetDest();
            SetSpeed(f.GetSpeed());

            return true;
        }

        return false;
    }
}
