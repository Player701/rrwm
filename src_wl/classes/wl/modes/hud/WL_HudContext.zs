//===========================================================================
//
// HudContext
//
// A context that is provided to all HudFragment instances
// for IsPresent and Update calls by the HudFragmentObserver.
//
//===========================================================================
class WL_HudContext ui
{
    private BaseStatusBar _statusBar;
    private bool _isFullscreen;

    //===========================================================================
    //
    // HudContext::Create
    //
    // Creates a new instance of the HudContext class.
    //
    //===========================================================================
    static WL_HudContext Create(BaseStatusBar statusBar)
    {
        let c = new('WL_HudContext');
        c.Init(statusBar);

        return c;
    }

    //===========================================================================
    //
    // HudContext::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(BaseStatusBar statusBar)
    {
        if (statusBar == null)
        {
            ThrowAbortException("%s: Status bar cannot be null.", GetClassName());
        }

        _statusBar = statusBar;
    }

    //===========================================================================
    //
    // HudContext::IsFullscreen
    //
    // Gets whether the HUD is in fullscreen state.
    //
    //===========================================================================
    bool IsFullscreen() const
    {
        return _isFullscreen;
    }

    //===========================================================================
    //
    // HudContext::SetFullscreen
    //
    // Sets whether the HUD is in fullscreen state.
    //
    //===========================================================================
    void SetFullscreen(bool value)
    {
        _isFullscreen = value;
    }

    //===========================================================================
    //
    // HudContext::GetStatusBar
    //
    // Returns the status bar. This can be used to access various convenience
    // methods of the BaseStatusBar class.
    //
    //===========================================================================
    BaseStatusBar GetStatusBar() const
    {
        return _statusBar;
    }

    //===========================================================================
    //
    // HudContext::GetPlayer
    //
    // Returns the player info for the current player.
    // This is a shortcut for GetStatusBar().CPlayer
    //
    //===========================================================================
    PlayerInfo GetPlayer() const
    {
        return _statusBar.CPlayer;
    }
}
