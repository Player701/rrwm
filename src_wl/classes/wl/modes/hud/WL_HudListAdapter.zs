//===========================================================================
//
// HudListAdapter
//
// A special type of ListAdapter that updates every time the HUD updates.
//
//===========================================================================
class WL_HudListAdapter : WL_ListAdapter abstract
{
    private int _itemCount;

    //===========================================================================
    //
    // HudListAdapter::Update
    //
    // Generates the list items to display and returns whether the number
    // of items generated is non-zero.
    //
    //===========================================================================
    bool Update(WL_HudContext context)
    {
        _itemCount = GenerateItems(context);
        return _itemCount > 0;
    }

    //===========================================================================
    //
    // HudListAdapter::GenerateItems
    //
    // Generates the list items for the HUD to display.
    // This method should return the number of items generated.
    //
    //===========================================================================
    protected abstract int GenerateItems(WL_HudContext context);

    //===========================================================================
    //
    // HudListAdapter::GetItemCount
    //
    // Returns the number of items.
    //
    //===========================================================================
    override int GetItemCount()
    {
        return _itemCount;
    }
}
