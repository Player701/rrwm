//===========================================================================
//
// HudFragment
//
// Base class for fragments that compose parts of a HUD.
// These fragments can be added both to fullscreen and status bar HUDs.
//
//===========================================================================
class WL_HudFragment : WL_Fragment abstract
{
    private bool _isFullscreenOnly;

    //===========================================================================
    //
    // HudFragment::IsFullscreenOnly
    //
    // Checks if this fragment should only be displayed in fullscreen mode.
    // By default, HUD fragments are visible in both modes.
    //
    //===========================================================================
    bool IsFullscreenOnly() const
    {
        return _isFullscreenOnly;
    }

    //===========================================================================
    //
    // HudFragment::SetFullscreenOnly
    //
    // Sets whether this fragment should only be displayed in fullscreen mode.
    //
    //===========================================================================
    void SetFullscreenOnly(bool value)
    {
        _isFullscreenOnly = value;
    }

    //===========================================================================
    //
    // HudFragment::Update
    //
    // Checks if the fragment needs to update itself.
    // If the fragment should not be displayed at all, this method should
    // return false. Otherwise, it should make the necessary adjustments
    // to update its state and return true.
    //
    //===========================================================================
    abstract bool Update(WL_HudContext context);
}
