//===========================================================================
//
// HudFont
//
// Wraps a HUDFont instance for the HudCanvas.
//
//===========================================================================
class WL_HudFont : WL_Font
{
    private HUDFont _hudFont;
    private int _shadowX;
    private int _shadowY;

    //===========================================================================
    //
    // HudFont::Create
    //
    // Creates a new instance of the HudFont class.
    //
    //===========================================================================
    static WL_HudFont Create(Font fnt, EMonospacing monospacing = Mono_Off, int spacing = 0, double lineSpacingTop = 0, double lineSpacingBottom = 0, vector2 baseScale = (1, 1), int shadowX = 0, int shadowY = 0)
    {
        let w = new('WL_HudFont');
        w.Init(fnt, monospacing, spacing, lineSpacingTop, lineSpacingBottom, baseScale, shadowX, shadowY);

        return w;
    }

    //===========================================================================
    //
    // HudFont::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(Font fnt, EMonospacing monospacing, int spacing, double lineSpacingTop, double lineSpacingBottom, vector2 baseScale, int shadowX, int shadowY)
    {
        Super.Init(fnt, monospacing, spacing, lineSpacingTop, lineSpacingBottom, baseScale);

        _hudFont = HUDFont.Create(fnt, spacing, monospacing, shadowX, shadowY);
        _shadowX = shadowX;
        _shadowY = shadowY;
    }

    //===========================================================================
    //
    // HudFont::GetHUDFont
    //
    // Returns the HUDFont instance.
    //
    //===========================================================================
    HUDFont GetHudFont() const
    {
        return _hudFont;
    }

    //===========================================================================
    //
    // HudFont::GetShadowX
    //
    // Returns the X offset of the shadow.
    //
    //===========================================================================
    int GetShadowX() const
    {
        return _shadowX;
    }

    //===========================================================================
    //
    // HudFont::GetShadowY
    //
    // Returns the Y offset of the shadow.
    //
    //===========================================================================
    int GetShadowY() const
    {
        return _shadowY;
    }
}
