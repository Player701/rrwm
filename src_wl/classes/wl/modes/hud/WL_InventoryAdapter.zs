//===========================================================================
//
// InventoryAdapter
//
// Implements a list adapter for inventory items.
//
//===========================================================================
class WL_InventoryAdapter : WL_HudListAdapter
{
    private WL_InventoryBarFragment _parentFragment;

    private Array<Inventory> _items;
    private int _selectedIndex;
    private bool _checkScroll;

    //===========================================================================
    //
    // InventoryAdapter::Create
    //
    // Creates a new instance of the InventoryAdapter class.
    //
    //===========================================================================
    static WL_InventoryAdapter Create(WL_InventoryBarFragment parentFragment)
    {
        let a = new('WL_InventoryAdapter');
        a.Init(parentFragment);

        return a;
    }

    //===========================================================================
    //
    // InventoryAdapter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_InventoryBarFragment parentFragment)
    {
        _parentFragment = parentFragment;
    }

    //===========================================================================
    //
    // InventoryAdapter::GenerateItems
    //
    // Generates the list items for the player's inventory.
    //
    //===========================================================================
    override int GenerateItems(WL_HudContext context)
    {
        int oldCount = _items.Size();
        int oldIndex = _selectedIndex;

        _items.Clear();

        let player = context.GetPlayer();

        for (let ii = player.mo.FirstInv(); ii != null; ii = ii.NextInv())
        {
            if (ii == player.mo.InvSel)
            {
                _selectedIndex = _items.Size();
            }

            _items.Push(ii);
        }

        _checkScroll = oldCount != _items.Size() || oldIndex != _selectedIndex;
        return _items.Size();
    }

    //===========================================================================
    //
    // InventoryAdapter::GetSelectedIndex
    //
    // Returns the index of the selected item.
    //
    //===========================================================================
    int GetSelectedIndex() const
    {
        return _selectedIndex;
    }

    //===========================================================================
    //
    // InventoryAdapter::IsScrollCheckRequired
    //
    // Returns true if the number of items or the selected index have changed,
    // thus necessitating a scrolling check.
    //
    //===========================================================================
    bool IsScrollCheckRequired() const
    {
        return _checkScroll;
    }

    //===========================================================================
    //
    // InventoryAdapter::GetOrCreateView
    //
    // Binds an existing InventoryItemFragment or creates a new one
    // from the parent InventoryBarFragment.
    //
    //===========================================================================
    override WL_View GetOrCreateView(int index, WL_View convertView)
    {
        let f = WL_InventoryItemFragment(convertView);

        if (f == null)
        {
            f = _parentFragment.CreateItemFragment();
        }

        let item = _items[index];
        f.BindItem(item, index == _selectedIndex);

        return f;
    }
}
