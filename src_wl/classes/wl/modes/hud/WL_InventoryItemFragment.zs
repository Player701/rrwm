//===========================================================================
//
// InventoryItemFragment
//
// Base class for fragments that represent inventory items.
//
//===========================================================================
class WL_InventoryItemFragment : WL_Fragment abstract
{
    //===========================================================================
    //
    // InventoryItemFragment::BindItem
    //
    // Updates this fragment's state to display the provided inventory item.
    //
    //===========================================================================
    abstract void BindItem(Inventory item, bool isSelected);
}
