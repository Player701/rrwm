//===========================================================================
//
// HudBase
//
// Base class for widget-based HUDs.
//
//===========================================================================
class WL_HudBase : BaseStatusBar
{
    private WL_HudRenderer _renderer;
    private WL_HudFragmentObserver _hudObserver;

    private bool _isNativePowerupsHudEnabled;

    //===========================================================================
    //
    // HudBase::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    override void Init()
    {
        Super.Init();

        SetSize(0, 0, 0);
        _hudObserver = WL_HudFragmentObserver.Create(self);
        _isNativePowerupsHudEnabled = true;

        let canvas = WL_HudCanvas.Create(self);
        _renderer = WL_HudRenderer.Create(canvas, CreateRootView(canvas));
    }

    //===========================================================================
    //
    // HudBase::CreateRootView
    //
    // Initializes and creates the view to display on the fullscreen HUD.
    // This method is only called once during initialization.
    //
    //===========================================================================
    protected virtual WL_View CreateRootView(WL_HudCanvas canvas)
    {
        return null;
    }

    //===========================================================================
    //
    // HudBase::SetNativePowerupsHudEnabled
    //
    // Controls whether the built-in method to draw powerups is enabled.
    // The native powerups HUD should be disabled if the custom HUD
    // implements its own powerups drawer.
    //
    //===========================================================================
    protected void SetNativePowerupsHudEnabled(bool value)
    {
        _isNativePowerupsHudEnabled = value;
    }

    //===========================================================================
    //
    // HudBase::Draw
    //
    // Draws the HUD on the screen.
    //
    //===========================================================================
    override void Draw(int state, double TicFrac)
    {
        Super.Draw(state, TicFrac);

        int debugLevel = GetDebugLevel();

        // Communicate the current state to all HUD fragments.
        _hudObserver.SetFullscreen(state == HUD_Fullscreen);

        // Draw the fullscreen HUD, if applicable
        if (_renderer != null)
        {
            _renderer.Render(state, GetDebugLevel());
        }
    }

    //===========================================================================
    //
    // HudBase::DrawPowerups
    //
    // Uses the built-in powerup drawing routine only if the native powerups HUD
    // is enabled.
    //
    //===========================================================================
    final override void DrawPowerups()
    {
        if (_isNativePowerupsHudEnabled)
        {
            Super.DrawPowerups();
        }
    }

    //===========================================================================
    //
    // HudBase::RegisterHudFragment
    //
    // Adds an obverser to the provided HudFragment so that the fragment
    // can receive updates to its state.
    //
    //===========================================================================
    protected WL_HudFragment RegisterHudFragment(WL_HudFragment fragment)
    {
        if (fragment == null)
        {
            ThrowAbortException("%s: Attempt to register a null HudFragment.", GetClassName());
        }

        fragment.GetObserverList().Add(_hudObserver);
        return fragment;
    }

    //===========================================================================
    //
    // HudBase::GetDebugLevel
    //
    // Returns the HUD debug level.
    //
    //===========================================================================
    protected virtual int GetDebugLevel() const
    {
        return -1;
    }
}
