//===========================================================================
//
// HudCanvas
//
// Implements rendering logic for HUDs.
//
//===========================================================================
class WL_HudCanvas : WL_Canvas
{
    private BaseStatusBar _statusBar;

    //===========================================================================
    //
    // HudCanvas::Create
    //
    // Creates a new instance of the HudCanvas class.
    //
    //===========================================================================
    static WL_HudCanvas Create(BaseStatusBar statusBar)
    {
        let c = new('WL_HudCanvas');
        c.Init(statusBar);

        return c;
    }

    //===========================================================================
    //
    // HudCanvas::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(BaseStatusBar statusBar)
    {
        Super.Init();
        _statusBar = statusBar;
    }

    //===========================================================================
    //
    // HudCanvas::GetStatusBar
    //
    // Returns the status bar.
    //
    //===========================================================================
    protected BaseStatusBar GetStatusBar() const
    {
        return _statusBar;
    }

    //===========================================================================
    //
    // HudCanvas::GetScaleFactors
    //
    // Returns the HUD scaling factors.
    //
    //===========================================================================
    override vector2 GetScaleFactors() const
    {
        return GetStatusBar().GetHUDScale();
    }

    //===========================================================================
    //
    // HudCanvas::PreDraw
    //
    // Prepares to draw the fullscreen HUD.
    //
    //===========================================================================
    override void PreDraw()
    {
        GetStatusBar().BeginHUD();
    }

    //===========================================================================
    //
    // HudCanvas::IsAnimationEnabled
    //
    // Checks whether animations for this canvas are currently enabled.
    //
    //===========================================================================
    override bool IsAnimationEnabled() const
    {
        // Animations are disabled while the game is paused.
        // Game is also paused in menus (except for netplay).
        return !(paused > 0 || !netgame && Menu.GetCurrentMenu() != null);
    }

    //===========================================================================
    //
    // HudCanvas::SetClipRect
    //
    // Calls SetClipRect on the status bar.
    //
    //===========================================================================
    override void SetClipRect(WL_Rectangle rect)
    {
        _statusBar.SetClipRect(rect.GetLeft(), rect.GetTop(), rect.GetWidth(), rect.GetHeight());
    }

    //===========================================================================
    //
    // HudCanvas::ClearClipRect
    //
    // Calls ClearClipRect on the status bar.
    //
    //===========================================================================
    override void ClearClipRect()
    {
        _statusBar.ClearClipRect();
    }

    //===========================================================================
    //
    // HudCanvas::FillColorInternal
    //
    // Fills the provided rectangle with the specified color.
    //
    //===========================================================================
    override void FillColorInternal(Color col, WL_Rectangle rect)
    {
        _statusBar.Fill(col, rect.GetLeft(), rect.GetTop(), rect.GetWidth(), rect.GetHeight());
    }

    //===========================================================================
    //
    // HudCanvas::DrawTexture
    //
    // Draws a texture within the specified bounds.
    //
    //===========================================================================
    override void DrawTexture(TextureID tex, WL_Rectangle bounds, WL_TextureDrawFlags flags)
    {
        int dtFlags = ( (flags & WL_TDF_TRANSLATABLE) ? BaseStatusBar.DI_TRANSLATABLE : 0)
                    | (!(flags & WL_TDF_ANIMATED)     ? BaseStatusBar.DI_DONTANIMATE  : 0)
                    | ( (flags & WL_TDF_FLIPPED)      ? BaseStatusBar.DI_MIRROR       : 0)
                    | ( (flags & WL_TDF_DIM)          ? BaseStatusBar.DI_DIM          : 0)
                    | BaseStatusBar.DI_FORCEFILL
                    | BaseStatusBar.DI_SCREEN_LEFT
                    | BaseStatusBar.DI_SCREEN_TOP
                    | BaseStatusBar.DI_ITEM_LEFT
                    | BaseStatusBar.DI_ITEM_TOP;

        WL_Size sz;
        bounds.GetSize(sz);
        _statusBar.DrawTexture(tex, bounds.GetTopLeft(), dtFlags, GetAlpha(), sz.ToVector());
    }

    //===========================================================================
    //
    // HudCanvas::DrawString
    //
    // Draws a string at the specified coordinates.
    //
    //===========================================================================
    override void DrawString(string text, WL_Font fnt, vector2 pos, int color, vector2 scale)
    {
        let wrapper = WL_HudFont(fnt);

        if (wrapper == null)
        {
            ThrowAbortException("%s: Invalid FontWrapper class passed to DrawString.", GetClassName());
        }

        // Temporarily extend clipping bounds to avoid cutting off text shadow

        int shadowX = wrapper.GetShadowX();
        int shadowY = wrapper.GetShadowY();

        WL_Rectangle clipRect, newClipRect;

        GetClipRect(clipRect);

        if (clipRect.GetArea() == 0)
        {
            // Clip area is zero, so don't bother
            return;
        }

        newClipRect.CreateLTWH(
            clipRect.GetLeft() + Min(0, shadowX),
            clipRect.GetTop() + Min(0, shadowY),
            clipRect.GetWidth() + Max(0, shadowX),
            clipRect.GetHeight() + Max(0, shadowY)
        );

        SetClipRect(newClipRect);

        let hudFont = wrapper.GetHUDFont();
        let baseScale = fnt.GetBaseScale();
        let finalScale = WL_VectorMath.MultiplyScales(baseScale, scale);

        // Fix right shift for non-monospaced HUD fonts
        double shift = wrapper.IsMonospaced() ? 0 : 1;

        pos = (pos.x - shift, pos.y + fnt.GetLineSpacing() * scale.Y);

        int flags = BaseStatusBar.DI_TEXT_ALIGN_LEFT
                  | BaseStatusBar.DI_SCREEN_LEFT
                  | BaseStatusBar.DI_SCREEN_TOP;

        _statusBar.DrawString(hudFont, text, pos, flags, color, GetAlpha(), linespacing: 0, scale: finalScale);

        SetClipRect(clipRect);
    }

    //===========================================================================
    //
    // HudCanvas::BuildFontUpon
    //
    // Returns an instance of HudFontBuilder that builds instances
    // of FontWrapper suitable for use on this canvas.
    //
    //===========================================================================
    override WL_HudFontBuilder BuildFontUpon(Font fnt) const
    {
        return WL_HudFontBuilder.Create(fnt);
    }
}
