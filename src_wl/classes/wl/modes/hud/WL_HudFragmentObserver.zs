//===========================================================================
//
// HudFragmentObserver
//
// Used to track state changes to HudFragments.
//
//===========================================================================
class WL_HudFragmentObserver : WL_ViewObserver
{
    private WL_HudContext _hudContext;

    //===========================================================================
    //
    // HudFragmentObserver::Create
    //
    // Creates a new instance of the HudFragmentObserver class.
    //
    //===========================================================================
    static WL_HudFragmentObserver Create(BaseStatusBar statusBar)
    {
        let o = new('WL_HudFragmentObserver');
        o.Init(statusBar);

        return o;
    }

    //===========================================================================
    //
    // HudFragmentObserver::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(BaseStatusBar statusBar)
    {
        _hudContext = WL_HudContext.Create(statusBar);
    }

    //===========================================================================
    //
    // HudFragmentObserver::SetPlayer
    //
    // Updates the state that is communicated to observed HudFragments.
    //
    //===========================================================================
    void SetFullscreen(bool isFullscreen)
    {
        _hudContext.SetFullscreen(isFullscreen);
    }

    //===========================================================================
    //
    // HudFragmentObserver::Observe
    //
    // If the observed view is not a HudFragment, returns false.
    // Otherwise, if the observed event is PreResolveSize,
    // calls Update on the HudFragment instance, alters its visibility
    // as necessary, and always returns true.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let hf = WL_HudFragment(view);

        if (hf == null)
        {
            return false;
        }

        if (eventType == WL_VET_PreResolveSize)
        {
            // Check presence of normal HUD fragments.
            bool isPresent = hf.Update(_hudContext);

            // If the fragment is present, it can still be hidden if needed.
            if (isPresent && !_hudContext.IsFullscreen() && hf.IsFullscreenOnly())
            {
                hf.SetVisibility(WL_V_Hidden);
            }
            else
            {
                hf.SetVisibility(isPresent ? WL_V_Visible : WL_V_Gone);
            }
        }

        return true;
    }

    //===========================================================================
    //
    // HudFragmentObserver::Accept
    //
    // Returns true if the other ViewObserver is also a HudFragmentObserver
    // (only one HudFragmentObserver can observe a fragment at any time).
    //
    //===========================================================================
    override bool Accept(WL_ViewObserver other)
    {
        return other is 'WL_HudFragmentObserver';
    }
}
