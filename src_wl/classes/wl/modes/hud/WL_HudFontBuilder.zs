//===========================================================================
//
// HudFontBuilder
//
// A type of TextFontBuilder that produces HudFontWrapper instances.
// Used by HudCanvas.
//
//===========================================================================
class WL_HudFontBuilder : WL_FontBuilder
{
    private int _shadowX;
    private int _shadowY;

    //===========================================================================
    //
    // HudFontBuilder::Create
    //
    // Creates a new instance of the HudFontBuilder class.
    //
    //===========================================================================
    static WL_HudFontBuilder Create(Font fnt)
    {
        let b = new('WL_HudFontBuilder');
        b.Init(fnt);

        return b;
    }

    //===========================================================================
    //
    // HudFontBuilder::SetShadowXY
    //
    // Sets the shadow offsets for the font.
    //
    //===========================================================================
    WL_HudFontBuilder SetShadowXY(int x, int y)
    {
        _shadowX = x;
        _shadowY = y;

        return self;
    }

    //===========================================================================
    //
    // HudFontBuilder::SetShadowUniform
    //
    // Sets both shadow offsets for the font to the provided value.
    //
    //===========================================================================
    WL_HudFontBuilder SetShadowUniform(int value)
    {
        return SetShadowXY(value, value);
    }

    //===========================================================================
    //
    // HudFontBuilder::CreateFontWrapper
    //
    // Creates a HudFontWrapper object with the provided parameters
    // out of the provided font instance.
    //
    //===========================================================================
    override WL_HudFont CreateFontWrapper(Font fnt, EMonospacing monospacing, int spacing, double lineSpacingTop, double lineSpacingBottom, vector2 baseScale) const
    {
        return WL_HudFont.Create(fnt, monospacing, spacing, lineSpacingTop, lineSpacingBottom, baseScale, _shadowX, _shadowY);
    }
}
