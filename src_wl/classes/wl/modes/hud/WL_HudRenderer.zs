//===========================================================================
//
// HudRenderer
//
// Used by HudBase to render the fullscreen HUD.
//
// The HudRenderer class encapsulates the following three aspects:
// 1) The canvas to draw on.
// 2) The view to draw on the canvas.
// 3) Behavior to determine whether that view should be visible.
//
//===========================================================================
class WL_HudRenderer ui
{
    private WL_Canvas _canvas;
    private WL_View _view;

    private WL_VisibilityChanger _visRoot;

    //===========================================================================
    //
    // HudRenderer::Create
    //
    // Creates a new instance of the HudRenderer class.
    //
    //===========================================================================
    static WL_HudRenderer Create(WL_Canvas canvas, WL_View view)
    {
        let r = new('WL_HudRenderer');
        r.Init(canvas, view);

        return r;
    }

    //===========================================================================
    //
    // HudRenderer::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_Canvas canvas, WL_View view)
    {
        _canvas = canvas;
        _view = view;

        if (_view != null)
        {
            _visRoot = WL_VisibilityChanger.Create();
            _visRoot.SetReduceOnly(true);
            _visRoot.ObserveView(view);
        }
    }

    //===========================================================================
    //
    // HudRenderer::Render
    //
    // Draws the HUD if it should be visible.
    //
    //===========================================================================
    void Render(int hudState, int debugLevel)
    {
        if (_view == null || hudState == BaseStatusBar.HUD_AltHud)
        {
            return;
        }

        // Set the initial visibility to visible.
        // This is needed because we've set ReduceOnly
        // on our visibility changer ot true.
        _view.SetVisibility(WL_V_Visible);

        // Now set the target visibility for the visibility changer.
        // In case there is a HudFragmentObserver observing the view,
        // we will not make the view more visible that it needs to be.
        _visRoot.SetVisibility(IsViewVisible(hudState) ? WL_V_Visible : WL_V_Hidden);

        _canvas.SetDebugLevel(debugLevel);
        _canvas.DrawView(_view);
    }

    //===========================================================================
    //
    // HudRenderer::IsViewVisible
    //
    // Checks if the view should be visible.
    //
    //===========================================================================
    private bool IsViewVisible(int hudState) const
    {
        // NB: some HUD fragments might be visible in status bar mode.
        return !automapactive && (hudState == BaseStatusBar.HUD_StatusBar || hudState == BaseStatusBar.HUD_Fullscreen);
    }
}
