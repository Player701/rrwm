//===========================================================================
//
// FullscreenHudRenderer
//
// Used by HudBase to render the fullscreen HUD.
//
//===========================================================================
class WL_FullscreenHudRenderer : WL_HudRenderer
{
    //===========================================================================
    //
    // FullscreenHudRenderer::Create
    //
    // Creates a new instance of the FullscreenHudRenderer class.
    //
    //===========================================================================
    static WL_FullscreenHudRenderer Create(WL_Canvas canvas, WL_View view)
    {
        let r = new('WL_FullscreenHudRenderer');
        r.Init(canvas, view);

        return r;
    }

    //===========================================================================
    //
    // FullscreenHudRenderer::IsViewVisible
    //
    // Checks if the fullscreen view is visible.
    //
    //===========================================================================
    override bool IsViewVisible(int hudState) const
    {
        // NB: some HUD fragments might be visible in status bar mode.
        return !automapactive && (hudState == BaseStatusBar.HUD_StatusBar || hudState == BaseStatusBar.HUD_Fullscreen);
    }
}
