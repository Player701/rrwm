//===========================================================================
//
// HudListFragment
//
// A special type of HudFragment that displays a ListView
// that is constantly updated via a HudListAdapter.
// This class should be used when the only thing the fragment displays
// is a list, so that boilerplate code can be avoided
// and only the adapter is subclassed.
//
//===========================================================================
class WL_HudListFragment : WL_HudFragment
{
    private WL_HudListAdapter _adapter;
    private WL_LayoutManager _layoutManager;

    //===========================================================================
    //
    // HudListFragment::Create
    //
    // Creates a new instance of the HudListFragment class.
    //
    //===========================================================================
    static WL_HudListFragment Create(WL_HudListAdapter adapter, WL_LayoutManager layoutManager)
    {
        let f = new('WL_HudListFragment');
        f.Init(adapter, layoutManager);

        return f;
    }

    //===========================================================================
    //
    // HudListFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_HudListAdapter adapter, WL_LayoutManager layoutManager)
    {
        if (adapter == null)
        {
            ThrowAbortException("%s: Adapter cannot be null.", GetClassName());
        }

        _adapter = adapter;
        _layoutManager = layoutManager;

        Super.Init();
    }

    //===========================================================================
    //
    // HudListFragment::CreateView
    //
    // Creates the ListView for this fragment.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        let lv = WL_ListView.Create();

        lv.SetAdapter(_adapter);
        lv.SetLayoutManager(_layoutManager);

        return lv;
    }

    //===========================================================================
    //
    // HudListFragment::Update
    //
    // Updates the state of the list adapter that is attached to the ListView.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        bool result = _adapter.Update(context);

        if (result)
        {
            _adapter.NotifyDataChanged();
        }

        return result;
    }
}
