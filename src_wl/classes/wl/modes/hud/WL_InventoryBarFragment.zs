//===========================================================================
//
// InventoryBarFragment
//
// Base class for HUD fragments that display an inventory bar.
// Inherit from this class to create a customized inventory bar.
//
//===========================================================================
class WL_InventoryBarFragment : WL_HudListFragment abstract
{
    private WL_ListView _listView;

    private WL_Size _lastListSize;
    private double _itemScrollTime;

    private WL_LinearLayoutManager _lm;

    //===========================================================================
    //
    // InventoryBarFragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        _lm = WL_LinearLayoutManager.Create(WL_O_Horizontal, scrollingEnabled: true);
        Super.Init(WL_InventoryAdapter.Create(self), _lm);
    }

    //===========================================================================
    //
    // InventoryBarFragment::GetItemScrollTime
    //
    // Returns the time to scroll between items.
    //
    //===========================================================================
    double GetItemScrollTime() const
    {
        return _itemScrollTime;
    }

    //===========================================================================
    //
    // InventoryBarFragment::SetItemScrollTime
    //
    // Sets the time to scroll between items.
    //
    //===========================================================================
    void SetItemScrollTime(double scrollTime)
    {
        if (scrollTime < 0)
        {
            ThrowAbortException("%s: Scroll time cannot be negative.", GetClassName());
        }

        _itemScrollTime = scrollTime;
    }

    //===========================================================================
    //
    // InventoryBarFragment::SetListHorizontalGravity
    //
    // Sets the horizontal gravity for the inventory list view,
    // changing its alignment.
    //
    //===========================================================================
    protected void SetListHorizontalGravity(WL_HorizontalGravity value)
    {
        if (_listView == null)
        {
            ThrowAbortException("%s: Cannot set horizontal gravity before the list view has been created.", GetClassName());
        }

        let params = _listView.GetLayoutParams();
        let oldGravity = params.GetGravity();

        if (oldGravity.GetHorizontal() != value)
        {
            params.SetGravity(WL_Gravity.Create(value, oldGravity.GetVertical()));
        }
    }

    //===========================================================================
    //
    // InventoryBarFragment::CreateView
    //
    // Initializes the layout for the inventory bar.
    //
    //===========================================================================
    override WL_View CreateView()
    {
        _listView = WL_ListView(Super.CreateView());
        let arrowLeft = CreateLeftArrow();
        let arrowRight = CreateRightArrow();

        // Enable scroll arrow toggling
        _lm.AddScrollArrows(arrowLeft, arrowRight);

        return Compose(_listView, arrowLeft, arrowRight);
    }

    //===========================================================================
    //
    // InventoryBarFragment::Compose
    //
    // Creates the inventory bar layout and places the provided list view
    // and views for left and right arrows (if present) onto it.
    //
    // This method can be overridden in derived classes to compose a custom
    // layout.
    //
    //===========================================================================
    protected virtual WL_View Compose(WL_View listView, WL_View arrowLeft, WL_View arrowRight)
    {
        let ll = WL_LinearLayout.Create();
        ll.SetOrientation(WL_O_Horizontal);
        ll.SetGravity(WL_Gravity.Vertical(WL_VG_Center));

        // Add the left arrow (if present).
        if (arrowLeft != null)
        {
            ll.AddView(arrowLeft);
        }

        // Add the list.
        listView.SetLayoutParams(WL_LinearLayout.CreateParams(weight: 1));
        ll.AddView(listView);

        // Add the right arrow (if present).
        if (arrowRight != null)
        {
            ll.AddView(arrowRight);
        }

        return ll;
    }

    //===========================================================================
    //
    // InventoryBarFragment::CreateLeftArrow
    //
    // Creates a view for the arrow to display to the left of the item list.
    // The arrow will only be shown when scrolling to the left is possible.
    //
    //===========================================================================
    protected virtual WL_View CreateLeftArrow()
    {
        return null;
    }

    //===========================================================================
    //
    // InventoryBarFragment::CreateRightArrow
    //
    // Creates a view for the arrow to display to the right of the item list.
    // The arrow will only be shown when scrolling to the right is possible.
    //
    //===========================================================================
    protected virtual WL_View CreateRightArrow()
    {
        return null;
    }

    //===========================================================================
    //
    // InventoryBarFragment::CreateItemFragment
    //
    // Returns a new InventoryFragment that will be used to display an item.
    //
    //===========================================================================
    abstract WL_InventoryItemFragment CreateItemFragment();

    //===========================================================================
    //
    // InventoryBarFragment::Update
    //
    // Checks whether inventory bar should be enabled, and if it should,
    // updates the inventory list.
    //
    //===========================================================================
    override bool Update(WL_HudContext context)
    {
        if (Level.NoInventoryBar)
        {
            return false;
        }

        return Super.Update(context);
    }

    //===========================================================================
    //
    // InventoryBarFragment::OnPostPosition
    //
    // Checks for state changes and triggers a scroll to the selected item
    // if it is no longer visible.
    //
    //===========================================================================
    override void OnPostPosition()
    {
        WL_Size listSize;
        _listView.GetResolvedSize(listSize);

        let adapter = WL_InventoryAdapter(_listView.GetAdapter());

        bool checkScroll = adapter.GetItemCount() > 0
            && (adapter.IsScrollCheckRequired()
                || !_lastListSize.IsDefined()
                || _lastListSize.GetWidth() != listSize.GetWidth());

        _lastListSize.CreateFrom(listSize);

        if (checkScroll)
        {
            int selIndex = adapter.GetSelectedIndex();
            _listView.ScrollToItem(selIndex, _itemScrollTime);
        }

        Super.OnPostPosition();
    }
}
