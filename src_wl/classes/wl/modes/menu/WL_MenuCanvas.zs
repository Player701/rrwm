//===========================================================================
//
// MenuCanvas
//
// Implements bottom-level rendering logic for menus.
//
//===========================================================================
class WL_MenuCanvas : WL_ScreenCanvas
{
    //===========================================================================
    //
    // MenuCanvas::Create
    //
    // Creates a new instance of the MenuCanvas class.
    //
    //===========================================================================
    static WL_MenuCanvas Create()
    {
        let c = new('WL_MenuCanvas');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // MenuCanvas::GetScaleFactors
    //
    // Returns the scaling factors for this canvas.
    //
    //===========================================================================
    override vector2 GetScaleFactors()
    {
        return (CleanXfac_1, CleanYfac_1);
    }
}
