//===========================================================================
//
// ScreenFontBuilder
//
// A type of TextFontBuilder that produces ScreenFont instances.
// Used by ScreenCanvas and MenuCanvas.
//
//===========================================================================
class WL_ScreenFontBuilder : WL_FontBuilder
{
    //===========================================================================
    //
    // ScreenFontBuilder::Create
    //
    // Creates a new instance of the ScreenFontBuilder class.
    //
    //===========================================================================
    static WL_ScreenFontBuilder Create(Font fnt)
    {
        let b = new('WL_ScreenFontBuilder');
        b.Init(fnt);

        return b;
    }

    //===========================================================================
    //
    // ScreenFontBuilder::CreateFontWrapper
    //
    // Creates a SimpleFontWrapper object with the provided parameters
    // out of the provided font instance.
    //
    //===========================================================================
    override WL_Font CreateFontWrapper(Font fnt, EMonospacing monospacing, int spacing, double lineSpacingTop, double lineSpacingBottom, vector2 baseScale) const
    {
        return WL_ScreenFont.Create(fnt, monospacing, spacing, lineSpacingTop, lineSpacingBottom, baseScale);
    }
}
