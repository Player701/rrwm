//===========================================================================
//
// ScreenFont
//
// Wraps a Font instance and exposes methods GetFont, GetMonospacing
// and GetSpacing for ScreenCanvas and MenuCanvas.
//
//===========================================================================
class WL_ScreenFont : WL_Font
{
    //===========================================================================
    //
    // ScreenFont::Create
    //
    // Creates a new instance of the ScreenFont class.
    //
    //===========================================================================
    static WL_ScreenFont Create(Font fnt, EMonospacing monospacing = Mono_Off, int spacing = 0, double lineSpacingTop = 0, double lineSpacingBottom = 0, vector2 baseScale = (1, 1))
    {
        let w = new('WL_ScreenFont');
        w.Init(fnt, monospacing, spacing, lineSpacingTop, lineSpacingBottom, baseScale);

        return w;
    }

    //===========================================================================
    //
    // ScreenFont::GetFont
    //
    // Returns the font.
    //
    //===========================================================================
    Font GetFont() const
    {
        return Super.GetFont();
    }

    //===========================================================================
    //
    // ScreenFont::GetMonospacing
    //
    // Returns the monospacing mode.
    //
    //===========================================================================
    EMonospacing GetMonospacing() const
    {
        return Super.GetMonospacing();
    }

    //===========================================================================
    //
    // ScreenFont::GetSpacing
    //
    // Returns the spacing value.
    //
    //===========================================================================
    int GetSpacing() const
    {
        return Super.GetSpacing();
    }
}
