//===========================================================================
//
// ScreenCanvas
//
// Implements bottom-level rendering logic for the whole screen.
//
//===========================================================================
class WL_ScreenCanvas : WL_Canvas
{
    //===========================================================================
    //
    // ScreenCanvas::Create
    //
    // Creates a new instance of the MenuCanvas class.
    //
    //===========================================================================
    static WL_ScreenCanvas Create()
    {
        let c = new('WL_ScreenCanvas');
        c.Init();

        return c;
    }

    //===========================================================================
    //
    // ScreenCanvas::GetTranslation
    //
    // Gets the translation for the console player.
    //
    //===========================================================================
    protected static TranslationID GetTranslation()
    {
        return Translation.MakeID((
            gameinfo.gametype & GAME_Raven) ? TRANSLATION_PlayersExtra : TRANSLATION_Players,
            players[consoleplayer].mo.PlayerNumber()
        );
    }

    //===========================================================================
    //
    // ScreenCanvas::SetClipRect
    //
    // Sets the clipping rectangle.
    //
    //===========================================================================
    override void SetClipRect(WL_Rectangle rect)
    {
        WL_Rectangle scaled;
        rect.ScaleBy(GetScaleFactors(), scaled);
        Screen.SetClipRect(
            int(scaled.GetLeft()),
            int(scaled.GetTop()),
            int(Ceil(scaled.GetWidth())),
            int(Ceil(scaled.GetHeight()))
        );
    }

    //===========================================================================
    //
    // ScreenCanvas::ClearClipRect
    //
    // Clears the clipping rectangle.
    //
    //===========================================================================
    override void ClearClipRect()
    {
        Screen.ClearClipRect();
    }

    //===========================================================================
    //
    // ScreenCanvas::FillColorInternal
    //
    // Fills the provided rectangle with the specified color.
    //
    //===========================================================================
    override void FillColorInternal(Color col, WL_Rectangle rect)
    {
        WL_Rectangle scaled;
        rect.ScaleBy(GetScaleFactors(), scaled);
        Screen.Dim(
            col, double(col.A)/255,
            int(scaled.GetLeft()),
            int(scaled.GetTop()),
            int(Ceil(scaled.GetWidth())),
            int(Ceil(scaled.GetHeight()))
        );
    }

    //===========================================================================
    //
    // ScreenCanvas::DrawTexture
    //
    // Draws a texture on the screen.
    //
    //===========================================================================
    override void DrawTexture(TextureID tex, WL_Rectangle bounds, WL_TextureDrawFlags flags)
    {
        WL_Rectangle renderArea;
        GetCurRenderArea(renderArea);

        Screen.DrawTexture(
            tex,
            flags & WL_TDF_ANIMATED,
            bounds.GetLeft(),
            bounds.GetTop(),
            DTA_KeepRatio, true,
            DTA_VirtualWidth, int(renderArea.GetWidth()),
            DTA_VirtualHeight, int(renderArea.GetHeight()),
            DTA_LeftOffset, 0,
            DTA_TopOffset, 0,
            DTA_DestWidthF, bounds.GetWidth(),
            DTA_DestHeightF, bounds.GetHeight(),
            DTA_TranslationIndex, (flags & WL_TDF_TRANSLATABLE) ? GetTranslation() : 0,
            DTA_FlipX, flags & WL_TDF_FLIPPED,
            DTA_Alpha, GetAlpha(),
            DTA_ColorOverlay, (flags & WL_TDF_DIM) ? Color(170, 0, 0, 0) : 0
        );
    }

    //===========================================================================
    //
    // ScreenCanvas::DrawString
    //
    // Draws a string on the screen.
    //
    //===========================================================================
    override void DrawString(string text, WL_Font fnt, vector2 pos, int color, vector2 scale)
    {
        let wrapper = WL_ScreenFont(fnt);

        if (wrapper == null)
        {
            ThrowAbortException("%s: Invalid font class passed to DrawString.", GetClassName());
        }

        WL_Rectangle renderArea;
        GetCurRenderArea(renderArea);
        let baseScale = fnt.GetBaseScale();
        let finalScale = WL_VectorMath.MultiplyScales(baseScale, scale);

        Screen.DrawText(
            wrapper.GetFont(),
            color,
            pos.x / finalScale.X,
            pos.y / finalScale.Y + fnt.GetLineSpacing() / baseScale.Y,
            text,
            DTA_KeepRatio, true,
            DTA_VirtualWidth, int(renderArea.GetWidth() / finalScale.X),
            DTA_VirtualHeight, int(renderArea.GetHeight() / finalScale.Y),
            DTA_Alpha, GetAlpha(),
            DTA_Monospace, wrapper.GetMonospacing(),
            DTA_Spacing, wrapper.GetSpacing()
        );
    }

    //===========================================================================
    //
    // ScreenCanvas::BuildFontUpon
    //
    // Returns an instance of ScreenFontBuilder that builds instances
    // of FontWrapper suitable for use on this canvas.
    //
    //===========================================================================
    override WL_FontBuilder BuildFontUpon(Font fnt) const
    {
        return WL_ScreenFontBuilder.Create(fnt);
    }
}
