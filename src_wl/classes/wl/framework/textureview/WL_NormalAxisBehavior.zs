//===========================================================================
//
// NormalAxisBehavior
//
// Implements a TextureAxisBehavior that provides a single texture
// without changing its size.
//
// This is the default behavior used by the TextureView.
//
//===========================================================================
class WL_NormalAxisBehavior : WL_TextureAxisBehavior
{
    //===========================================================================
    //
    // NormalAxisBehavior::Create
    //
    // Creates a new instance of the NormalAxisBehavior class.
    //
    //===========================================================================
    static WL_NormalAxisBehavior Create()
    {
        return new('WL_NormalAxisBehavior');
    }

    //===========================================================================
    //
    // NormalAxisBehavior::GetContentLength
    //
    // Returns the total length of the TextureView's content along this axis.
    //
    //===========================================================================
    override double GetContentLength(double textureLength, double maxLength) const
    {
        return textureLength;
    }

    //===========================================================================
    //
    // NormalAxisBehavior::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    override double GetTextureLength(double textureLength, double maxLength) const
    {
        return textureLength;
    }

    //===========================================================================
    //
    // NormalAxisBehavior::GetTextureCount
    //
    // Returns the number of textures that the TextureView should draw
    // along this axis.
    //
    //===========================================================================
    override int GetTextureCount(double textureLength, double maxLength) const
    {
        return 1;
    }
}
