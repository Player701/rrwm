//===========================================================================
//
// TextureAxisParams
//
// Stores parameters calcluated after applying a TextureAxisBehavior.
//
//===========================================================================
class WL_TextureAxisParams ui
{
    private double _contentLength;
    private double _textureLength;
    private int _textureCount;

    //===========================================================================
    //
    // TextureAxisParams::Create
    //
    // Creates a new instance of the TextureAxisParams class.
    //
    //===========================================================================
    static WL_TextureAxisParams Create()
    {
        return new('WL_TextureAxisParams');
    }

    //===========================================================================
    //
    // TextureAxisParams::ApplyBehavior
    //
    // Applies the provided TextureAxisBehavior to this TextureAxisParams
    // instance, using the provided texture length and maximum length
    // as parameters.
    //
    //===========================================================================
    void ApplyBehavior(WL_TextureAxisBehavior behavior, double textureLength, double maxLength)
    {
        _contentLength = behavior.GetContentLength(textureLength, maxLength);
        _textureLength = behavior.GetTextureLength(textureLength, maxLength);
        _textureCount = behavior.GetTextureCount(textureLength, maxLength);
    }

    //===========================================================================
    //
    // TextureAxisParams::GetContentLength
    //
    // Returns the total length of content along this axis.
    //
    //===========================================================================
    double GetContentLength() const
    {
        return _contentLength;
    }

    //===========================================================================
    //
    // TextureAxisParams::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    double GetTextureLength() const
    {
        return _textureLength;
    }

    //===========================================================================
    //
    // TextureAxisParams::GetTextureCount
    //
    // Returns the number of textures to draw along this axis.
    //
    //===========================================================================
    int GetTextureCount() const
    {
        return _textureCount;
    }
}
