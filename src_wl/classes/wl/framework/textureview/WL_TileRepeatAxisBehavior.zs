//===========================================================================
//
// TileRepeatAxisBehavior
//
// Implements a TextureAxisBehavior that provides a fixed number of textures
// that either take their natural size or divide the content length evenly.
//
//===========================================================================
class WL_TileRepeatAxisBehavior : WL_TextureAxisBehavior
{
    private int _count;
    private bool _partition;

    //===========================================================================
    //
    // TileRepeatAxisBehavior::Create
    //
    // Creates a new instance of the TileRepeatAxisBehavior class.
    //
    //===========================================================================
    static WL_TileRepeatAxisBehavior Create(int count, bool partition)
    {
        let b = new('WL_TileRepeatAxisBehavior');
        b.Init(count, partition);

        return b;
    }

    //===========================================================================
    //
    // TileRepeatAxisBehavior::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(int count, bool partition)
    {
        if (count <= 0)
        {
            ThrowAbortException("%s: Number of tiles must be positive.", GetClassName());
        }

        _count = count;
        _partition = partition;
    }

    //===========================================================================
    //
    // TileRepeatAxisBehavior::GetContentLength
    //
    // Returns the total length of the TextureView's content along this axis.
    //
    //===========================================================================
    override double GetContentLength(double textureLength, double maxLength) const
    {
        return _partition ? maxLength : textureLength * _count;
    }

    //===========================================================================
    //
    // TileRepeatAxisBehavior::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    override double GetTextureLength(double textureLength, double maxLength) const
    {
        return _partition ? maxLength / _count : textureLength;
    }

    //===========================================================================
    //
    // TileRepeatAxisBehavior::GetTextureCount
    //
    // Returns the number of textures that the TextureView should draw
    // along this axis.
    //
    //===========================================================================
    override int GetTextureCount(double textureLength, double maxLength) const
    {
        return _count;
    }

    //===========================================================================
    //
    // TileRepeatAxisBehavior::Equals
    //
    // Checks if this TileRepeatAxisBehavior instance
    // is equal to the provided one.
    //
    //===========================================================================
    override bool Equals(WL_TextureAxisBehavior other) const
    {
        if (Super.Equals(other))
        {
            let tileRepeatOther = WL_TileRepeatAxisBehavior(other);
            return tileRepeatOther._count == _count && tileRepeatOther._partition == _partition;
        }

        return false;
    }
}
