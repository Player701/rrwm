//===========================================================================
//
// TextureAxisBehavior
//
// Controls the drawing process in a TextureView along a single axis.
//
//===========================================================================
class WL_TextureAxisBehavior abstract ui
{
    //===========================================================================
    //
    // TextureAxisBehavior::Normal
    //
    // Returns a TextureAxisBehavior instance that is used by the TextureView
    // by default. This behavior produces a single texture of normal size.
    //
    //===========================================================================
    static WL_TextureAxisBehavior Normal()
    {
        return WL_NormalAxisBehavior.Create();
    }

    //===========================================================================
    //
    // TextureAxisBehavior::Stretch
    //
    // Returns a TextureAxisBehavior instance that produces a single texture
    // which gets stretched to the maximum possible size.
    //
    //===========================================================================
    static WL_TextureAxisBehavior Stretch()
    {
        return WL_StretchAxisBehavior.Create();
    }

    //===========================================================================
    //
    // TextureAxisBehavior::TileRepeat
    //
    // Returns a TextureAxisBehavior instance that generates a fixed number
    // of textures. If partition is false, normal texture size is used.
    // Otherwise, the maximum allowed length is divided evenly by the number
    // of textures to display.
    //
    //===========================================================================
    static WL_TextureAxisBehavior TileRepeat(int count, bool partition = false)
    {
        return WL_TileRepeatAxisBehavior.Create(count, partition);
    }

    //===========================================================================
    //
    // TextureAxisBehavior::TileFill
    //
    // Returns a TextureAxisBehavior instance that generates as many textures
    // as possible. If exact is false, then the last texture may get cut off,
    // but the TextureView will use the maximum possible size. Otherwise,
    // if a texture can get cut off, it will not appear, and the resulting
    // total size might be slightly reduced.
    //
    //===========================================================================
    static WL_TextureAxisBehavior TileFill(bool exact = false)
    {
        return WL_TileFillAxisBehavior.Create(exact);
    }

    //===========================================================================
    //
    // TextureAxisBehavior::GetContentLength
    //
    // Returns the total length of the TextureView's content along this axis.
    //
    //===========================================================================
    abstract double GetContentLength(double textureLength, double maxLength) const;

    //===========================================================================
    //
    // TextureAxisBehavior::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    abstract double GetTextureLength(double textureLength, double maxLength) const;

    //===========================================================================
    //
    // TextureAxisBehavior::GetTextureCount
    //
    // Returns the number of textures that the TextureView should draw
    // along this axis.
    //
    //===========================================================================
    abstract int GetTextureCount(double textureLength, double maxLength) const;

    //===========================================================================
    //
    // TextureAxisBehavior::Equals
    //
    // Checks if this TextureAxisBehavior instance is equal to the provided one.
    //
    //===========================================================================
    virtual bool Equals(WL_TextureAxisBehavior other) const
    {
        return other != null && (self == other || other.GetClass() == GetClass());
    }
}
