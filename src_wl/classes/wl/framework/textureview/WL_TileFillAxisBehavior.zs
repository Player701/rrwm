//===========================================================================
//
// TileFillAxisBehavior
//
// Implements a TextureAxisBehavior that fills the content length
// with as many textures as possible. If the last texture does not fully
// fit, it can be left out.
//
//===========================================================================
class WL_TileFillAxisBehavior : WL_TextureAxisBehavior
{
    private bool _exact;

    //===========================================================================
    //
    // TileFillAxisBehavior::Create
    //
    // Creates a new instance of the TileFillAxisBehavior class.
    //
    //===========================================================================
    static WL_TileFillAxisBehavior Create(bool exact)
    {
        let b = new('WL_TileFillAxisBehavior');
        b.Init(exact);

        return b;
    }

    //===========================================================================
    //
    // TileFillAxisBehavior::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(bool exact)
    {
        _exact = exact;
    }

    //===========================================================================
    //
    // TileFillAxisBehavior::GetContentLength
    //
    // Returns the total length of the TextureView's content along this axis.
    //
    //===========================================================================
    override double GetContentLength(double textureLength, double maxLength) const
    {
        return _exact ? Floor(maxLength / textureLength) * textureLength : maxLength;
    }

    //===========================================================================
    //
    // TileFillAxisBehavior::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    override double GetTextureLength(double textureLength, double maxLength) const
    {
        return textureLength;
    }

    //===========================================================================
    //
    // TileFillAxisBehavior::GetTextureCount
    //
    // Returns the number of textures that the TextureView should draw
    // along this axis.
    //
    //===========================================================================
    override int GetTextureCount(double textureLength, double maxLength) const
    {
        return int(_exact ? maxLength / textureLength : Ceil(maxLength / textureLength));
    }

    //===========================================================================
    //
    // TileFillAxisBehavior::Equals
    //
    // Checks if this TileFillAxisBehavior instance is equal to the provided one.
    //
    //===========================================================================
    override bool Equals(WL_TextureAxisBehavior other) const
    {
        return Super.Equals(other) && WL_TileFillAxisBehavior(other)._exact == _exact;
    }
}
