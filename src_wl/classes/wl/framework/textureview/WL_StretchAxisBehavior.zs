//===========================================================================
//
// StretchAxisBehavior
//
// Implements a TextureAxisBehavior that provides a single texture
// which is stretched to maximum size, disregarding proportions.
//
//
//===========================================================================
class WL_StretchAxisBehavior : WL_TextureAxisBehavior
{
    //===========================================================================
    //
    // StretchAxisBehavior::Create
    //
    // Creates a new instance of the StretchAxisBehavior class.
    //
    //===========================================================================
    static WL_StretchAxisBehavior Create()
    {
        return new('WL_StretchAxisBehavior');
    }

    //===========================================================================
    //
    // StretchAxisBehavior::GetContentLength
    //
    // Returns the total length of the TextureView's content along this axis.
    //
    //===========================================================================
    override double GetContentLength(double textureLength, double maxLength) const
    {
        return maxLength;
    }

    //===========================================================================
    //
    // StretchAxisBehavior::GetTextureLength
    //
    // Returns the length of a single texture along this axis.
    //
    //===========================================================================
    override double GetTextureLength(double textureLength, double maxLength) const
    {
        return maxLength;
    }

    //===========================================================================
    //
    // StretchAxisBehavior::GetTextureCount
    //
    // Returns the number of textures that the TextureView should draw
    // along this axis.
    //
    //===========================================================================
    override int GetTextureCount(double textureLength, double maxLength) const
    {
        return 1;
    }
}
