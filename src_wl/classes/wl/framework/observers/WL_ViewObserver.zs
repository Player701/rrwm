//===========================================================================
//
// ViewObserver
//
// Base class for objects that can be attached to views
// to alter their behavior and / or properties over time.
//
//===========================================================================
class WL_ViewObserver abstract ui
{
    //===========================================================================
    //
    // ViewObserver::Observe
    //
    // Called every time a particular event happens to the view
    // that is attached to this ViewObserver.
    // This method must return true if the ViewObserver is to continue observing,
    // or false if the ViewObserver is done observing, in which case
    // it will be removed from the observer list.
    //
    //===========================================================================
    abstract bool Observe(WL_View view, WL_ViewEventType eventType);

    //===========================================================================
    //
    // ViewObserver::Accept
    //
    // Checks if this ViewObserver should accept another ViewObserver
    // instead of letting it get added to the list separately.
    // If the other ViewObserver gets accepted, it is not added to the list
    // and is instead discarded. This can be used for cumulative effects
    // like over-time scrolling.
    //
    //===========================================================================
    virtual bool Accept(WL_ViewObserver other)
    {
        return false;
    }

    //===========================================================================
    //
    // ViewObserver::ObserveView
    //
    // Adds this ViewObserver to the observer list of the provided view.
    //
    //===========================================================================
    void ObserveView(WL_View view)
    {
        view.GetObserverList().Add(self);
    }

    //===========================================================================
    //
    // ViewObserver::ObservePrimitiveViews
    //
    // Adds this ViewObserver to every PrimitiveView in the hierarchy,
    // regardless of what level they are on.
    //
    //===========================================================================
    void ObservePrimitiveViews(WL_View view)
    {
        let viewGroup = WL_ViewGroup(view);

        if (viewGroup != null)
        {
            for (int i = 0; i < viewGroup.GetCount(); i++)
            {
                ObservePrimitiveViews(viewGroup.GetViewAt(i));
            }
        }
        else if (view is 'WL_PrimitiveView')
        {
            ObserveView(view);
        }

        let bg = view.GetBackgroundView();

        if (bg != null)
        {
            ObservePrimitiveViews(bg);
        }
    }
}
