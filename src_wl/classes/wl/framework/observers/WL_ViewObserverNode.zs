//===========================================================================
//
// ViewObserverNode
//
// A linked list node that holds a reference to ViewObserver.
//
//===========================================================================
class WL_ViewObserverNode ui
{
    private WL_ViewObserver _obverser;

    private WL_ViewObserverNode _prev;
    private WL_ViewObserverNode _next;

    //===========================================================================
    //
    // ViewObserverNode::Create
    //
    // Creates a new instance of the ViewObserverNode class.
    //
    //===========================================================================
    static WL_ViewObserverNode Create(WL_ViewObserver observer)
    {
        let n = new('WL_ViewObserverNode');
        n.Init(observer);

        return n;
    }

    //===========================================================================
    //
    // ViewObserverNode::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_ViewObserver observer)
    {
        _obverser = observer;
    }

    //===========================================================================
    //
    // ViewObserverNode::GetObserver
    //
    // Returns the ViewObserver contained in this node.
    //
    //===========================================================================
    WL_ViewObserver GetObserver() const
    {
        return _obverser;
    }

    //===========================================================================
    //
    // ViewObserverNode::GetPrev
    //
    // Returns the previous node in the list.
    //
    //===========================================================================
    WL_ViewObserverNode GetPrev() const
    {
        return _prev;
    }

    //===========================================================================
    //
    // ViewObserverNode::SetPrev
    //
    // Sets the previous node in the list.
    //
    //===========================================================================
    void SetPrev(WL_ViewObserverNode prev)
    {
        CheckReferenceIntegrity(prev, _next);
        _prev = prev;
    }

    //===========================================================================
    //
    // ViewObserverNode::GetNext
    //
    // Gets the next node in the list.
    //
    //===========================================================================
    WL_ViewObserverNode GetNext() const
    {
        return _next;
    }

    //===========================================================================
    //
    // ViewObserverNode::SetNext
    //
    // Sets the next node in the list.
    //
    //===========================================================================
    void SetNext(WL_ViewObserverNode next)
    {
        CheckReferenceIntegrity(next, _prev);
        _next = next;
    }

    //===========================================================================
    //
    // ViewObserverNode::CheckReferenceIntegrity
    //
    // Checks for simple reference integrity violations in the node list.
    //
    //===========================================================================
    private void CheckReferenceIntegrity(WL_ViewObserverNode newRef, WL_ViewObserverNode otherRef)
    {
        if (newRef == self)
        {
            ThrowAbortException("%s: ViewObserverNode cannot reference itself.", GetClassName());
        }

        if (newRef != null && newRef == otherRef)
        {
            ThrowAbortException("%s: Circular reference detected in ViewObserver list.", GetClassName());
        }
    }
}
