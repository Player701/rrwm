//===========================================================================
//
// ViewController
//
// Can be attached to multiple views to coordinate changes in their behavior.
// This is used to avoid storing many references to multiple views,
// or an array of them, when there is a need to alter the state of all views
// at once.
//
//===========================================================================
class WL_ViewController : WL_ViewObserver abstract
{
    //===========================================================================
    //
    // ViewController::Accept
    //
    // Throws an exception if the other ViewObserver has the same class,
    // since only one ViewController of each type can be attached to a view.
    // Attempting to attach a duplicate controller is pointless and is probably
    // the result of a bug in user code.
    //
    //===========================================================================
    override bool Accept(WL_ViewObserver other)
    {
        if (GetClass() == other.GetClass())
        {
            ThrowAbortException("%s: Only one %s can be attached to a view at a time.", GetClassName(), GetClassName());
        }

        return false;
    }
}
