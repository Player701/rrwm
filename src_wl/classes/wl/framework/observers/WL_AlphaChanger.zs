//===========================================================================
//
// AlphaChanger
//
// A type of ViewController that changes the alpha value of the observed
// views.
//
//===========================================================================
class WL_AlphaChanger : WL_ViewController
{
    private double _alpha;

    //===========================================================================
    //
    // AlphaChanger::Create
    //
    // Creates a new instance of the AlphaChanger class.
    //
    //===========================================================================
    static WL_AlphaChanger Create()
    {
        return new('WL_AlphaChanger');
    }

    //===========================================================================
    //
    // AlphaChanger::SetAlpha
    //
    // Sets the alpha.
    //
    //===========================================================================
    void SetAlpha(double alpha)
    {
        _alpha = alpha;
    }

    //===========================================================================
    //
    // AlphaChanger::Observe
    //
    // Observes the OnResolveSize event and changes the view's alpha value.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let p = WL_PrimitiveView(view);

        if (p == null)
        {
            return false;
        }

        if (eventType == WL_VET_OnResolveSize)
        {
            p.SetAlpha(_alpha);
        }

        return true;
    }
}
