//===========================================================================
//
// TextureViewCollapser
//
// A type of ViewController that automatically sets a TextureView's
// visibility to Gone if it does not have a valid texture.
//
//===========================================================================
class WL_TextureViewCollapser : WL_ViewController
{
    //===========================================================================
    //
    // TextureViewCollapser::Create
    //
    // Creates a new instance of the TextureViewCollapser class.
    //
    //===========================================================================
    static WL_TextureViewCollapser Create()
    {
        return new('WL_TextureViewCollapser');
    }

    //===========================================================================
    //
    // TextureViewCollapser::Observe
    //
    // Checks that the observed view is a TextureView.
    // Observes the PreResolveSize event, and toggles the view's visibility
    // between Visible and Gone depending on whether the texture is valid.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let iv = WL_TextureView(view);

        if (iv == null)
        {
            return false;
        }

        if (eventType == WL_VET_PreResolveSize)
        {
            iv.SetVisibility(iv.IsValidTexture() ? WL_V_Visible : WL_V_Gone);
        }

        return true;
    }
}
