//===========================================================================
//
// ScrollArrowToggler
//
// A type of ViewObserver that observes a ScrollView. The ScrollArrowToggler
// manages the visibility of the two views passed to it, showing each of them
// whenever it finds out that the ScrollView can be scrolled
// in the corresponding directions. It is not required that the provided
// views representing the arrows are both non-null.
//
//===========================================================================
class WL_ScrollArrowToggler : WL_ViewObserver
{
    private WL_View _arrowBack;
    private WL_View _arrowForward;

    //===========================================================================
    //
    // ScrollArrowToggler::Create
    //
    // Creates a new instance of the WL_ScrollArrowToggler class.
    //
    //===========================================================================
    static WL_ScrollArrowToggler Create(WL_View arrowBack, WL_View arrowForward)
    {
        let o = new('WL_ScrollArrowToggler');
        o.Init(arrowBack, arrowForward);

        return o;
    }

    //===========================================================================
    //
    // ScrollArrowToggler::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_View arrowBack, WL_View arrowForward)
    {
        _arrowBack = arrowBack;
        _arrowForward = arrowForward;
    }

    //===========================================================================
    //
    // ScrollArrowToggler::Observe
    //
    // Observes the PostResolveSize event. If the observed view
    // is a ScrollView, alters the visibility of the arrows (indicators)
    // depending on whether the ScrollView can be scroll in the corresponding
    // directions.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let sv = WL_ScrollView(view);

        if (sv == null)
        {
            return false;
        }

        if (eventType == WL_VET_PostPosition)
        {
            double curPos = sv.GetScrollPos();
            double maxPos = sv.GetScrollPosMax();

            if (_arrowBack != null) _arrowBack.SetVisibility(curPos > 0 ? WL_V_Visible : WL_V_Hidden);
            if (_arrowForward != null) _arrowForward.SetVisibility(curPos < maxPos ? WL_V_Visible : WL_V_Hidden);
        }

        return true;
    }
}
