//===========================================================================
//
// ViewObserverList
//
// Maintains a linked list of ViewObserver objects for a view.
//
//===========================================================================
class WL_ViewObserverList ui
{
    private WL_ViewObserverNode _start;
    private WL_ViewObserverNode _end;

    //===========================================================================
    //
    // ViewObserverList::Create
    //
    // Creates a new instance of the ViewObserverList class.
    //
    //===========================================================================
    static WL_ViewObserverList Create()
    {
        let l = new('WL_ViewObserverList');
        l.Init();

        return l;
    }

    //===========================================================================
    //
    // ViewObserverList::Create
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        _start = WL_ViewObserverNode.Create(null);
        _end = WL_ViewObserverNode.Create(null);

        Clear();
    }

    //===========================================================================
    //
    // ViewObserverList::GetFirst
    //
    // Gets the first node after the sentinel start node.
    //
    //===========================================================================
    private WL_ViewObserverNode GetFirst() const
    {
        return _start.GetNext();
    }

    //===========================================================================
    //
    // ViewObserverList::IsLast
    //
    // Checks if the provided node is the sentinel end node.
    //
    //===========================================================================
    private bool IsEnd(WL_ViewObserverNode node) const
    {
        return node == _end;
    }

    //===========================================================================
    //
    // ViewObserverList::Add
    //
    // Adds a new ViewObserver to the list.
    //
    //===========================================================================
    void Add(WL_ViewObserver observer)
    {
        if (observer == null)
        {
            ThrowAbortException("%s: Attempt to add a null ViewObserver to this ViewObserverList.", GetClassName());
        }

        for (let cur = GetFirst(); !IsEnd(cur); cur = cur.GetNext())
        {
            if (cur.GetObserver().Accept(observer))
            {
                // If the observer is accepted, do not add it
                return;
            }
        }

        let n = WL_ViewObserverNode.Create(observer);
        let last = _end.GetPrev();

        // Link new node
        n.SetPrev(last);
        n.SetNext(_end);

        // Link the sentinel end node and the (former) last node
        last.SetNext(n);
        _end.SetPrev(n);
    }

    //===========================================================================
    //
    // ViewObserverList::RemoveNode
    //
    // Removes the provided ViewObserverNode from the list.
    //
    //===========================================================================
    private void RemoveNode(WL_ViewObserverNode node)
    {
        let prev = node.GetPrev();
        let next = node.GetNext();

        prev.SetNext(next);
        next.SetPrev(prev);
    }

    //===========================================================================
    //
    // ViewObserverList::Remove
    //
    // Removes the provided ViewObserver instance from this list,
    // provided that it exists.
    //
    //===========================================================================
    bool Remove(WL_ViewObserver observer)
    {
        for (let cur = GetFirst(); !IsEnd(cur); cur = cur.GetNext())
        {
            // Check if this observer is done observing
            if (cur.GetObserver() == observer)
            {
                RemoveNode(cur);
                return true;
            }
        }

        return false;
    }

    //===========================================================================
    //
    // ViewObserverList::RemoveByType
    //
    // Removes all ViewObserver instances of the specified type from this list.
    // Type check can be exact or with inheritance enabled.
    //
    //===========================================================================
    bool RemoveByType(class<WL_ViewObserver> observerClass, bool exact = false)
    {
        int removed = 0;

        for (let cur = GetFirst(); !IsEnd(cur); cur = cur.GetNext())
        {
            if (CheckObserver(cur.GetObserver(), observerClass, exact))
            {
                RemoveNode(cur);
                removed++;
            }
        }

        return removed > 0;
    }

    //===========================================================================
    //
    // ViewObserverList::CheckObserver
    //
    // Checks if the provided ViewObserver instance is of the specified type
    // (either exactly or via inheritance).
    //
    //===========================================================================
    private static bool CheckObserver(WL_ViewObserver observer, class<WL_ViewObserver> observerClass, bool exact)
    {
        return exact
            ? observer.GetClass() == observerClass
            : observer is observerClass;
    }

    //===========================================================================
    //
    // ViewObserverList::Clear
    //
    // Removes all observers from this list.
    //
    //===========================================================================
    void Clear()
    {
        _start.SetNext(_end);
        _end.SetPrev(_start);
    }

    //===========================================================================
    //
    // ViewObserverList::ObserveView
    //
    // Calls Observe on all added ViewObserver instances, passing the provided
    // view as the argument.
    //
    //===========================================================================
    void ObserveView(WL_View view, WL_ViewEventType eventType)
    {
        if (view == null)
        {
            ThrowAbortException("%s: Attempt to observe a null View.", GetClassName());
        }

        for (let cur = GetFirst(); !IsEnd(cur); cur = cur.GetNext())
        {
            // Check if this observer is done observing
            if (!cur.GetObserver().Observe(view, eventType))
            {
                RemoveNode(cur);
            }
        }
    }
}
