//===========================================================================
//
// VisibilityChanger
//
// A type of ViewController that changes the visibility of all views
// that it observes. Visibility is changed on PreResolveSize event.
// This can be useful to avoid tracking a lot of references
// to various views when switching their visibility on and off.
//
//===========================================================================
class WL_VisibilityChanger : WL_ViewController
{
    private WL_Visibility _visibility;
    private bool _reduceOnly;

    //===========================================================================
    //
    // VisibilityChanger::Create
    //
    // Creates a new instance of the VisibilityChanger class.
    //
    //===========================================================================
    static WL_VisibilityChanger Create()
    {
        return new('WL_VisibilityChanger');
    }

    //===========================================================================
    //
    // VisibilityChanger::SetVisibility
    //
    // Sets the visibility.
    //
    //===========================================================================
    void SetVisibility(WL_Visibility visibility)
    {
           _visibility = visibility;
    }

    //===========================================================================
    //
    // VisibilityChanger::SetReduceOnly
    //
    // Sets whether this VisibilityChanger should only reduce visibility.
    // If enabled, the visibility of the observed view will not be altered
    // if it causes the view to become more visible than it was
    // (e.g. Gone to Hidden, or Hidden to Visible, or Gone to Visible).
    // This is useful if the visibility is controlled by another source,
    // for example, a HudFragmentObserver.
    //
    //===========================================================================
    void SetReduceOnly(bool reduceOnly)
    {
        _reduceOnly = reduceOnly;
    }

    //===========================================================================
    //
    // VisibilityChanger::Observe
    //
    // Observes the PreResolveSize and changes the visibility of the view.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        if (eventType == WL_VET_PreResolveSize)
        {
            let targetVis = _visibility;

            // Do not increase visibility if reduceOnly is true
            if (_reduceOnly)
            {
                targetVis = Max(targetVis, view.GetVisibility());
            }

            view.SetVisibility(_visibility);
        }

        return true;
    }
}
