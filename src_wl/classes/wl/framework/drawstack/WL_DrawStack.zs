//===========================================================================
//
// DrawStack
//
// Implements a stack of WL_DrawStackFrame objects
// to keep track of the draw operation chain.
//
//===========================================================================
class WL_DrawStack ui
{
    private Array<WL_DrawStackFrame> _items;
    private int _size, _maxDepth;

    // Keep at least this number of frames allocated
    // to avoid pushing the GC too much.
    const MIN_SIZE = 100;

    //===========================================================================
    //
    // DrawStack::Create
    //
    // Creates a new instance of the DrawStack class.
    //
    //===========================================================================
    static WL_DrawStack Create()
    {
        return new('WL_DrawStack');
    }

    //===========================================================================
    //
    // DrawStack::IsEmpty
    //
    // Checks whether the stack is empty.
    //
    //===========================================================================
    bool IsEmpty() const
    {
        return _size == 0;
    }

    //===========================================================================
    //
    // DrawStack::GetSize
    //
    // Returns the number of elements in the stack.
    //
    //===========================================================================
    int GetSize() const
    {
        return _size;
    }

    //===========================================================================
    //
    // DrawStack::GetTop
    //
    // Returns the top element of the stack.
    //
    //===========================================================================
    WL_DrawStackFrame GetTop() const
    {
        if (IsEmpty())
        {
            ThrowAbortException("%s: Attempt to GetTop from an empty stack.", GetClassName());
        }

        return _items[_size - 1];
    }

    //===========================================================================
    //
    // DrawStack::Push
    //
    // Pushes a new frame onto the stack.
    //
    //===========================================================================
    WL_DrawStackFrame Push(WL_Rectangle rect, WL_Margins padding, double alpha)
    {
        let parentFrame = _size > 0 ? _items[_size - 1] : null;
        WL_DrawStackFrame result;

        if (_items.Size() == _size)
        {
            // Create a new frame
            result = WL_DrawStackFrame.Create(parentFrame, rect, padding, alpha);
            _items.Push(result);
        }
        else
        {
            // Reuse an existing frame
            result = _items[_size];
            result.Init(parentFrame, rect, padding, alpha);
        }

        _size++;
        _maxDepth = Max(_size, _maxDepth);

        return result;
    }

    //===========================================================================
    //
    // DrawStack::Pop
    //
    // Pops a frame from the stack.
    //
    //===========================================================================
    void Pop()
    {
        if (IsEmpty())
        {
            ThrowAbortException("%s: Attempt to Pop an empty stack.", GetClassName());
        }

        _size--;

        // Check if there are too many items compared to the maximum
        // reached last time
        int n = _items.Size();

        if (_size == 0 && n > MIN_SIZE && n > _maxDepth * 2)
        {
            // Keep at least MIN_SIZE entries, remove extras
            _items.Resize(Min(MIN_SIZE, _maxDepth));
        }

        // Reset this value for a new calculation
        _maxDepth = 0;
    }
}
