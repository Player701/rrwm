//===========================================================================
//
// DrawStackFrame
//
// Represents the stack frame for the current draw operation.
// Stores the current draw area, clip area, and alpha (for easy compositing).
//
//===========================================================================
class WL_DrawStackFrame ui
{
    private WL_Rectangle _drawRect;
    private WL_Rectangle _drawRectClipped;

    private WL_Rectangle _clipRect;
    private double _alpha;

    //===========================================================================
    //
    // DrawStackFrame::Create
    //
    // Creates a new instance of the WL_DrawStackFrame class.
    //
    //===========================================================================
    static WL_DrawStackFrame Create(WL_DrawStackFrame parentFrame, WL_Rectangle rect, WL_Margins padding, double alpha)
    {
        let f = new('WL_DrawStackFrame');
        f.Init(parentFrame, rect, padding, alpha);

        return f;
    }

    //===========================================================================
    //
    // DrawStackFrame::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    void Init(WL_DrawStackFrame parentFrame, WL_Rectangle rect, WL_Margins padding, double alpha)
    {
        _drawRect.CreateFrom(rect);

        if (padding != null)
        {
            _drawRect.Pad(padding);
        }

        _drawRectClipped.CreateFrom(_drawRect);

        _clipRect.CreateFrom(rect);
        _alpha = alpha;

        if (parentFrame != null)
        {
            WL_Rectangle parentClipRect;
            parentFrame.GetClipRect(parentClipRect);

            _drawRectClipped.ClipBy(parentClipRect);
            _clipRect.ClipBy(parentClipRect);
            _alpha *= parentFrame.GetAlpha();
        }
    }

    //===========================================================================
    //
    // DrawStackFrame::GetDrawRect
    //
    // Returns the rectangle which represents the current draw area.
    //
    //===========================================================================
    void GetDrawRect(out WL_Rectangle result) const
    {
        result.CreateFrom(_drawRect);
    }

    //===========================================================================
    //
    // DrawStackFrame::GetDrawRectClipped
    //
    // Returns the rectangle which represents the current draw area,
    // clipped by the current clip area.
    //
    //===========================================================================
    void GetDrawRectClipped(out WL_Rectangle result) const
    {
        result.CreateFrom(_drawRectClipped);
    }

    //===========================================================================
    //
    // DrawStackFrame::GetClipRect
    //
    // Returns the rectangle which represents the current clipping area.
    //
    //===========================================================================
    void GetClipRect(out WL_Rectangle result) const
    {
        result.CreateFrom(_clipRect);
    }

    //===========================================================================
    //
    // DrawStackFrame::GetAlpha
    //
    // Returns the alpha value in the current draw area.
    //
    //===========================================================================
    double GetAlpha() const
    {
        return _alpha;
    }
}
