//===========================================================================
//
// ViewIterator
//
// Implements an iteration through a collection of ViewGroup's views.
//
//===========================================================================
struct WL_ViewIterator ui
{
    private readonly<WL_ViewGroup> _viewGroup;
    private int _groupVersion, _curIndex;
    
    // NB: https://github.com/ZDoom/gzdoom/issues/2814
    private Function<void> _filter;

    //===========================================================================
    //
    // ViewIterator::Create
    //
    // Creates a new ViewIterator.
    //
    //===========================================================================
    void Create(readonly<WL_ViewGroup> viewGroup, Function<ui bool(WL_View)> filter = null)
    {
        if (viewGroup == null)
        {
            Object.ThrowAbortException("WL_ViewIterator: View group cannot be null.");
        }

        _viewGroup = viewGroup;
        _filter = filter;
        _groupVersion = viewGroup.GetVersion();
        _curIndex = -1;
    }

    //===========================================================================
    //
    // ViewIterator::CheckInitialized
    //
    // Throws an abort exception if this iterator has not been initialized.
    //
    //===========================================================================
    private void CheckInitialized()
    {
        if (_viewGroup == null)
        {
            Object.ThrowAbortException("WL_ViewIterator: Attempt to use an iterator before initialization.");
        }
    }

    //===========================================================================
    //
    // ViewIterator::IsAtEnd
    //
    // Checks whether this iterator has reached the end.
    //
    //===========================================================================
    private bool IsAtEnd() const
    {
        CheckInitialized();
        return _curIndex == _viewGroup.GetCount();
    }

    //===========================================================================
    //
    // ViewIterator::ApplyFilter
    //
    // Applies the filter's predicate to the current view and returns the result.
    // If there is no filter, always returns true.
    //
    //===========================================================================
    private bool ApplyFilter() const
    {
        return _filter == null || ((Function<ui bool(WL_View)>)(_filter)).Call(GetView()); // >_<
    }

    //===========================================================================
    //
    // ViewIterator::MoveNextUnfiltered
    //
    // Tries to move to the next view of the view group.
    // Returns true if the iterator is pointing to a view, false otherwise.
    //
    //===========================================================================
    private bool MoveNextUnfiltered()
    {
        if (_groupVersion != _viewGroup.GetVersion())
        {
            Object.ThrowAbortException("WL_ViewIterator: Children view collection of the view group was modified.");
        }

        if (!IsAtEnd())
        {
            _curIndex++;
        }

        return !IsAtEnd();
    }

    //===========================================================================
    //
    // ViewIterator::MoveNext
    //
    // Tries to move to the next view of the view group,
    // skipping views that do not match the filter's predicate.
    // Returns true if the iterator has still not reached the end,
    // false otherwise.
    //
    //===========================================================================
    bool MoveNext()
    {
        bool result;

        while ((result = MoveNextUnfiltered()))
        {
            if (ApplyFilter())
            {
                break;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // ViewIterator::GetView
    //
    // Returns the current view pointed at by this iterator.
    //
    //===========================================================================
    WL_View GetView() const
    {
        if (IsAtEnd() || _curIndex == -1)
        {
            Object.ThrowAbortException("WL_ViewIterator: Invalid position for GetView.");
        }

        return _viewGroup.GetViewAt(_curIndex);
    }
}
