//===========================================================================
//
// Canvas
//
// Stores the context of a drawing operation and provides methods
// to draw primitives and views on the screen.
//
//===========================================================================
class WL_Canvas abstract ui
{
    // Contains information about the current series of draw operations.
    private WL_DrawStack _drawStack;
    // Defines the area where rendering takes place
    // in the current draw operation.
    private WL_Rectangle _curRenderArea;

    // Current debug level (used to fill drawing/clipping areas with debug colors)
    private int _debugLevel;

    //===========================================================================
    //
    // Canvas::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        _drawStack = WL_DrawStack.Create();
    }

    //===========================================================================
    //
    // Canvas::GetClipRect
    //
    // Returns the clipping rectangle for the current draw frame.
    //
    //===========================================================================
    protected void GetClipRect(out WL_Rectangle result) const
    {
        _drawStack.GetTop().GetClipRect(result);
    }

    //===========================================================================
    //
    // Canvas::GetAlpha
    //
    // Returns the alpha value for the current draw frame.
    //
    //===========================================================================
    protected double GetAlpha() const
    {
        return _drawStack.GetTop().GetAlpha();
    }

    //===========================================================================
    //
    // Canvas::GetRenderArea
    //
    // Returns the rendering area for this canvas.
    //
    //===========================================================================
    protected virtual void GetRenderArea(out WL_Rectangle result) const
    {
        result.GetScreen();
    }

    //===========================================================================
    //
    // Canvas::GetScaleFactors
    //
    // Returns the factors to scale the rendering area by.
    //
    //===========================================================================
    protected virtual vector2 GetScaleFactors() const
    {
        return (1, 1);
    }

    //===========================================================================
    //
    // Canvas::SetClipRect
    //
    // Sets the clipping rectangle for the current drawing operation.
    //
    //===========================================================================
    protected abstract void SetClipRect(WL_Rectangle rect);

    //===========================================================================
    //
    // Canvas::ClearClipRect
    //
    // Clears the clipping rectangle.
    //
    //===========================================================================
    protected abstract void ClearClipRect();

    //===========================================================================
    //
    // Canvas::GetCurRenderArea
    //
    // Returns the current rendering area with scaling accounted for.
    //
    //===========================================================================
    protected void GetCurRenderArea(out WL_Rectangle result) const
    {
        if (!_curRenderArea.IsDefined())
        {
            ThrowAbortException("%s: Attempt to GetCurRenderArea outside of a drawing operation.", GetClassName());
        }

        result.CreateFrom(_curRenderArea);
    }

    //===========================================================================
    //
    // Canvas::PushFrame
    //
    // Creates and pushes a new frame onto the draw stack
    // and sets the new clipping bounds,
    // signifying the start of a drawing operation.
    //
    //===========================================================================
    private void PushFrame(WL_Rectangle rect, WL_Margins padding, double alpha)
    {
        let frame = _drawStack.Push(rect, padding, alpha);

        WL_Rectangle clipRect;
        frame.GetClipRect(clipRect);

        SetClipRect(clipRect);

        // If debug mode is enabled, fill the rectangle with a debug color.
        if (_debugLevel > 0)
        {
            FillDebug(clipRect, false);

            WL_Rectangle drawRectClipped;
            frame.GetDrawRectClipped(drawRectClipped);
            FillDebug(drawRectClipped, true);
        }
    }

    //===========================================================================
    //
    // Canvas::PopFrame
    //
    // Pops the draw stack and restores the previous clipping bounds,
    // signifying the end of a drawing operation.
    //
    //===========================================================================
    private void PopFrame()
    {
        // Pop the stack.
        _drawStack.Pop();

        // If the clip stack is not empty,
        // restore the previous clipping rectangle.
        if (!_drawStack.IsEmpty())
        {
            WL_Rectangle clipRect;
            GetClipRect(clipRect);
            SetClipRect(clipRect);
        }
        else
        {
            // Otherwise, clear the clipping rectangle.
            ClearClipRect();
        }
    }

    //===========================================================================
    //
    // Canvas::FillDebug
    //
    // Fills a rectangle with a debug color.
    //
    //===========================================================================
    private void FillDebug(WL_Rectangle rect, bool isPadded)
    {
        int lv = Min(20, _drawStack.GetSize() - _debugLevel);

        if (lv < 0)
        {
            return;
        }

        int x = 255 * lv / 20;
        let col = isPadded
            ? Color(255, x, 255 - x, 0)
            : Color(255, 255 - x, 0, 0);

        FillColorInternal(col, rect);
    }

    //===========================================================================
    //
    // Canvas::SetDebugLevel
    //
    // Sets the debug level (fills bounding rectangles with solid colors).
    //
    //===========================================================================
    void SetDebugLevel(int debugLevel)
    {
        _debugLevel = debugLevel;
    }

    //===========================================================================
    //
    // Canvas::StartDraw
    //
    // Starts a new drawing operation using the provided bounding rectangle,
    // padding, and alpha value.
    //
    // Additionally, if debug mode is on, fills the rectangle with a debug color
    // and accounts for the provided padding to add another, contrasting color.
    //
    //===========================================================================
    void StartDraw(WL_Rectangle bounds, WL_Margins padding = null, double alpha = 1)
    {
        PushFrame(bounds, padding, alpha);
    }

    //===========================================================================
    //
    // Canvas::EndDraw
    //
    // Clears the current bounding rectangle, padding, and alpha value.
    //
    // This should always be called after all draw operations
    // within the bounding rectangle have been completed.
    // Failure to do this will result in error.
    //
    //===========================================================================
    void EndDraw()
    {
        PopFrame();
    }

    //===========================================================================
    //
    // Canvas::DrawView
    //
    // Draws the provided view witthin the specified rendering area.
    //
    //===========================================================================
    void DrawView(WL_View view)
    {
        // Set the current render area
        GetRenderArea(_curRenderArea);
        _curRenderArea.ScaleBy(WL_VectorMath.Reciprocal(GetScaleFactors()));

        // Draw the view
        view.ResolveAndDraw(self, _curRenderArea);

        // Check for mismatched calls to Start/EndDraw
        if (!_drawStack.IsEmpty())
        {
            ThrowAbortException("%s: Draw stack has been unbalanced. Check your code for mismatched StartDraw / EndDraw calls.", GetClassName());
        }

        // Clear the current render area
        _curRenderArea.Undefined();
    }

    //===========================================================================
    //
    // Canvas::IsAnimationEnabled
    //
    // Checks whether animations for this canvas are currently enabled.
    //
    //===========================================================================
    virtual bool IsAnimationEnabled() const
    {
        return true;
    }

    //===========================================================================
    //
    // Canvas::PreDraw
    //
    // This is called in View::ResolveAndDraw just before the view gets drawn.
    // This should be used to prepare the canvas for drawing, if necessary.
    //
    //===========================================================================
    virtual void PreDraw()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // Canvas::FillColor
    //
    // Fills the provided rectangle with the specified color.
    // Adjusts the alpha value to account for the current drawing operation.
    //
    //===========================================================================
    void FillColor(Color clr, WL_Rectangle bounds)
    {
        int newAlpha = int(clr.A * GetAlpha());
        let clr = Color(newAlpha, clr.R, clr.G, clr.B);

        FillColorInternal(clr, bounds);
    }

    //===========================================================================
    //
    // Canvas::FillColorInternal
    //
    // Fills the provided rectangle with the specified color.
    //
    //===========================================================================
    protected abstract void FillColorInternal(Color clr, WL_Rectangle bounds);

    //===========================================================================
    //
    // Canvas::DrawTexture
    //
    // Draws a texture within the specified bounds.
    //
    //===========================================================================
    abstract void DrawTexture(TextureID tex, WL_Rectangle bounds, WL_TextureDrawFlags flags);

    //===========================================================================
    //
    // Canvas::DrawString
    //
    // Draws a string at the specified coordinates.
    //
    //===========================================================================
    abstract void DrawString(string text, WL_Font fnt, vector2 pos, int color, vector2 scale);

    //===========================================================================
    //
    // Canvas::BuildFontUpon
    //
    // Returns an instance of FontBuilder that builds instances
    // of FontWrapper suitable for use on this canvas.
    //
    //===========================================================================
    abstract WL_FontBuilder BuildFontUpon(Font fnt) const;
}

//===========================================================================
//
// TextureDrawFlags
//
// Enumerates flags for texture drawing options.
//
//===========================================================================
enum WL_TextureDrawFlags
{
    // Makes the texture assume the console player's translation.
    WL_TDF_TRANSLATABLE = 1,

    // Makes the texture animated. This is set by default.
    WL_TDF_ANIMATED     = 1 << 1,

    // Flips the texture horizontally.
    WL_TDF_FLIPPED      = 1 << 2,

    // Dims the texture slightly.
    WL_TDF_DIM          = 1 << 3
};
