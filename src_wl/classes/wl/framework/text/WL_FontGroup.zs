//===========================================================================
//
// FontGroup
//
// Encapsulates a pair of font objects for easy font fallback.
//
//===========================================================================
class WL_FontGroup ui
{
    private WL_Font _mainFont;
    private WL_Font _backupFont;

    //===========================================================================
    //
    // FontGroup::Create
    //
    // Creates a new instance of the FontGroup class.
    //
    //===========================================================================
    static WL_FontGroup Create(WL_Font mainFont, WL_Font backupFont = null)
    {
        let p = new('WL_FontGroup');
        p.Init(mainFont, backupFont);

        return p;
    }

    //===========================================================================
    //
    // FontGroup::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init(WL_Font mainFont, WL_Font backupFont)
    {
        _mainFont = mainFont;
        _backupFont = backupFont;
    }

    //===========================================================================
    //
    // FontGroup::GetMainFont
    //
    // Returns the main font.
    //
    //===========================================================================
    WL_Font GetMainFont() const
    {
        return _mainFont;
    }

    //===========================================================================
    //
    // FontGroup::GetBackupFont
    //
    // Returns the fallback font, or the main font if the fallback font is null.
    //
    //===========================================================================
    WL_Font GetBackupFont() const
    {
        return _backupFont != null ? _backupFont : _mainFont;
    }

    //===========================================================================
    //
    // FontGroup::IsFallbackRequired
    //
    // Checks if fallback font needs to be used to print the provided text.
    //
    //===========================================================================
    bool IsFallbackRequired(string text) const
    {
        return !_mainFont.CanPrint(text);
    }

    //===========================================================================
    //
    // FontGroup::PickFont
    //
    // Automatically selects and returns the correct font
    // to print the provided text.
    //
    //===========================================================================
    WL_Font PickFont(string text) const
    {
        return IsFallbackRequired(text)
            ? GetBackupFont()
            : _mainFont;
    }
}
