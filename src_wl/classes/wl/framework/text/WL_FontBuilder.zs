//===========================================================================
//
// FontBuilder
//
// Base class for fluid API providers that create instances
// of FontWrapper and TextFont.
//
//===========================================================================
class WL_FontBuilder abstract ui
{
    private Font _mainFont;
    private Font _backupFont;

    private EMonospacing _monospacing;
    private int _spacing;
    private bool _monospaceForDigits;

    private double _lineSpacingTop, _lineSpacingBottom;
    private double _backupLineSpacingTop, _backupLineSpacingBottom;
    private bool _backupLineSpacingEnabled;

    private vector2 _baseScale;

    //===========================================================================
    //
    // FontBuilder::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(Font fnt)
    {
        if (fnt == null)
        {
            ThrowAbortException("%s: Font cannot be null.", GetClassName());
        }

        _mainFont = fnt;
        _baseScale = (1, 1);
    }

    //===========================================================================
    //
    // FontBuilder::UseBackupFont
    //
    // Uses the provided font as the backup font.
    //
    //===========================================================================
    WL_FontBuilder UseBackupFont(Font fnt)
    {
        if (fnt == null)
        {
            ThrowAbortException("%s: Font cannot be null.", GetClassName());
        }

        _backupFont = fnt;
        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetMonospacingMode
    //
    // Sets the monospacing mode. By default, monospacing is off.
    //
    //===========================================================================
    WL_FontBuilder SetMonospacingMode(EMonospacing monospacing)
    {
        _monospacing = monospacing;

        if (monospacing == Mono_Off)
        {
            _spacing = 0;
            _monospaceForDigits = false;
        }

        return self;
    }

    //===========================================================================
    //
    // FontBuilder::MonospaceForDigits
    //
    // Sets the spacing value so that it will be automatically calculated
    // based on the digit characters and the minus sign.
    //
    //===========================================================================
    WL_FontBuilder MonospaceForDigits(int extraSpacing = 0)
    {
        if (_monospacing == Mono_Off)
        {
            ThrowAbortException("%s: Monospacing must be enabled first (call SetMonospacingMode).", GetClassName());
        }

        if (extraSpacing < 0)
        {
            ThrowAbortException("%s: Extra spacing cannot be negative.", GetClassName());
        }

        _monospaceForDigits = true;
        _spacing = extraSpacing;

        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetSpacing
    //
    // Sets the spacing value.
    //
    //===========================================================================
    WL_FontBuilder SetSpacing(int spacing)
    {
        if (_monospacing == Mono_Off)
        {
            ThrowAbortException("%s: Monospacing must be enabled first (call SetMonospacingMode).", GetClassName());
        }

        if (spacing <= 0)
        {
            ThrowAbortException("%s: Spacing must be positive.", GetClassName());
        }

        _spacing = spacing;
        _monospaceForDigits = false;

        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetLineSpacing
    //
    // Sets the line spacing.
    //
    //===========================================================================
    WL_FontBuilder SetLineSpacing(double lineSpacingTop, double lineSpacingBottom)
    {
        _lineSpacingTop = lineSpacingTop;
        _lineSpacingBottom = lineSpacingBottom;

        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetBackupLineSpacing
    //
    // Sets the line spacing value for backup font.
    //
    //===========================================================================
    WL_FontBuilder SetBackupLineSpacing(double lineSpacingTop, double lineSpacingBottom)
    {
        if (_backupFont == null)
        {
            ThrowAbortException("%s: Cannot set linespacing for backup font, backup font is not yet set.", GetClassName());
        }

        _backupLineSpacingEnabled = true;
        _backupLineSpacingTop = lineSpacingTop;
        _backupLineSpacingBottom = lineSpacingBottom;

        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetBaseScaleXY
    //
    // Sets the X and Y base scale for the font to the provided values.
    //
    //===========================================================================
    WL_FontBuilder SetBaseScaleXY(double x, double y)
    {
        _baseScale = (x, y);
        return self;
    }

    //===========================================================================
    //
    // FontBuilder::SetBaseScaleUniform
    //
    // Sets both the X and Y base scale for the font to the provided value.
    //
    //===========================================================================
    WL_FontBuilder SetBaseScaleUniform(double value)
    {
        return SetBaseScaleXY(value, value);
    }

    //===========================================================================
    //
    // FontBuilder::CreateFontGroup
    //
    // Returns a FontGroup instance composed of the main font
    // and the backup font, if it was specified. Both fonts will have the same
    // properties.
    //
    //===========================================================================
    WL_FontGroup CreateFontGroup() const
    {
        let mainFontWrapper = WrapFont(_mainFont);
        let backupFontWrapper = _backupFont != null ? WrapFont(_backupFont) : null;

        return WL_FontGroup.Create(mainFontWrapper, backupFontWrapper);
    }

    //===========================================================================
    //
    // FontBuilder::CreateSingleFont
    //
    // Creates a single FontWrapper instance out of the main font.
    // This can only be called if the backup font has not been set.
    //
    //===========================================================================
    WL_Font CreateSingleFont() const
    {
        if (_backupFont != null)
        {
            ThrowAbortException("%s: Cannot create a single font out of two fonts.", GetClassName());
        }

        return WrapFont(_mainFont);
    }

    //===========================================================================
    //
    // FontBuilder::CalcSpacingForDigits
    //
    // Calculates the spacing value to use for digit characters
    // and the minus sign.
    //
    //===========================================================================
    private static int CalcSpacingForDigits(Font fnt)
    {
        int result = 0;

        for (int i = 0; i < 10; i++)
        {
            result = Max(result, fnt.GetCharWidth(int("0") + i));
        }

        return Max(result, fnt.GetCharWidth("-"));
    }

    //===========================================================================
    //
    // FontBuilder::WrapFont
    //
    // Creates a FontWrapper object with the configured parameters
    // out of the provided font instance.
    //
    //===========================================================================
    private WL_Font WrapFont(Font fnt) const
    {
        int spacing = _spacing;

        if (_monospacing != Mono_Off)
        {
            if (_monospaceForDigits)
            {
                spacing += CalcSpacingForDigits(fnt);
            }
            else if (_spacing == 0)
            {
                ThrowAbortException("%s: Spacing must be set.", GetClassName());
            }
        }

        bool useBackupLineSpacing = _backupLineSpacingEnabled && fnt == _backupFont;
        double lineSpacingTop = useBackupLineSpacing ? _backupLineSpacingTop : _lineSpacingTop;
        double lineSpacingBottom = useBackupLineSpacing ? _backupLineSpacingBottom : _lineSpacingBottom;

        return CreateFontWrapper(fnt, _monospacing, spacing, lineSpacingTop, lineSpacingBottom, _baseScale);
    }

    //===========================================================================
    //
    // FontBuilder::CreateFontWrapper
    //
    // Creates a FontWrapper object with the provided parameters
    // out of the provided font instance.
    //
    //===========================================================================
    protected abstract WL_Font CreateFontWrapper(Font fnt, EMonospacing monospacing, int spacing, double lineSpacingTop, double lineSpacingBottom, vector2 baseScale) const;
}
