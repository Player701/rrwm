//===========================================================================
//
// TextFormatter
//
// Base class for implementing custom formats for TextView.
//
//===========================================================================
class WL_TextFormatter abstract ui
{
    //===========================================================================
    //
    // TextFormatter::FormatText
    //
    // Given the input text, produces the appropriate formatted text.
    // If the TextView is localized, the input text will also be localized.
    //
    //===========================================================================
    abstract string FormatText(string text);

    //===========================================================================
    //
    // TextFormatter::Equals
    //
    // Checks if this TextFormatter instance is equal to the provided one.
    //
    //===========================================================================
    virtual bool Equals(WL_TextFormatter other) const
    {
        return other != null && (self == other || other.GetClass() == GetClass());
    }
}
