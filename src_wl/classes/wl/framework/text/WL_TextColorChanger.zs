//===========================================================================
//
// TextColorChanger
//
// A type of ViewController that can attach to an arbitrary number
// of TextViews and change their color all at once.
//
//===========================================================================
class WL_TextColorChanger : WL_ViewController
{
    private int _color;

    //===========================================================================
    //
    // TextColorChanger::Create
    //
    // Creates a new instance of the TextColorChanger class.
    //
    //===========================================================================
    static WL_TextColorChanger Create()
    {
        let o = new('WL_TextColorChanger');
        o.Init();

        return o;
    }

    //===========================================================================
    //
    // TextColorChanger::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        _color = Font.CR_UNTRANSLATED;
    }

    //===========================================================================
    //
    // TextColorChanger::SetColor
    //
    // Sets the text color.
    //
    //===========================================================================
    void SetColor(int color)
    {
        _color = color;
    }

    //===========================================================================
    //
    // TextColorChanger::SetColorName
    //
    // Sets the text color by name.
    //
    //===========================================================================
    void SetColorName(name colorName)
    {
        SetColor(Font.FindFontColor(colorName));
    }

    //===========================================================================
    //
    // TextColorChanger::Observe
    //
    // Observes the OnResolveSize event and changes text color
    // of all observed text views.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let tv = WL_TextViewBase(view);

        if (tv == null)
        {
            return false;
        }

        if (eventType == WL_VET_OnResolveSize)
        {
            tv.SetColor(_color);
        }

        return true;
    }
}
