//===========================================================================
//
// FontFallbackCoordinator
//
// A type of ViewController that can observe multiple TextViews.
// If at least one of the observed views needs to use the backup font
// to print its text, the FontFallbackCoordinator will automatically
// switch all previously and hereinafter observed TextViews
// to also use the backup font.
//
// FontFallbackCoordinator is useful to enforce a single text style
// among a group of text views, without the need to control it manually.
//
//===========================================================================
class WL_FontFallbackCoordinator : WL_ViewController
{
    private Array<WL_TextViewBase> _textViews;
    private bool _needFallback;

    //===========================================================================
    //
    // FontFallbackCoordinator::Create
    //
    // Creates a new instance of the FontFallbackCoordinator class.
    //
    //===========================================================================
    static WL_FontFallbackCoordinator Create()
    {
        return new('WL_FontFallbackCoordinator');
    }

    //===========================================================================
    //
    // FontFallbackCoordinator::Observe
    //
    // Observs the PreResolveSize and PostResolveSize events
    // and changes font use mode on the observed TextViews as necessary.
    //
    //===========================================================================
    override bool Observe(WL_View view, WL_ViewEventType eventType)
    {
        let tv = WL_TextViewBase(view);

        if (tv == null)
        {
            return false;
        }

        if (eventType == WL_VET_OnResolveSize)
        {
            // If there is no fallback yet, check if we might need it.
            if (!_needFallback)
            {
                bool fallback = tv.GetFont().IsFallbackRequired(tv.GetText());

                // If we need to fall back to backup font,
                // tell this to all previously observed views.
                if (fallback)
                {
                    foreach (textView : _textViews)
                    {
                        textView.SetFontUseMode(WL_FUM_UseBackupFont);
                    }
                }
                else
                {
                    // Otherwise, reset the observed view's font use mode
                    // (we might have changed it in the past)
                    // and save it so that we can communicate with it later.
                    tv.SetFontUseMode(WL_FUM_UseMainFont);
                    _textViews.Push(tv);
                }

                _needFallback = fallback;
            }

            // If fallback is required, just say that to the view,
            // no need to put it in the array.
            if (_needFallback)
            {
                tv.SetFontUseMode(WL_FUM_UseBackupFont);
            }
        }
        else if (eventType == WL_VET_PostResolveSize)
        {
            // On PostResolveSize, reset the fallback flag
            // and clear the array (We don't care if we do this multiple times).
            _needFallback = false;
            _textViews.Clear();
        }

        return true;
    }
}
