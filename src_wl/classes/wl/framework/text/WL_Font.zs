//===========================================================================
//
// Font
//
// Base class for font wrappers that incorporate extra font parameters
// and expose the necessary methods in subclasses for use
// by the corresponding Canvas type.
//
//===========================================================================
class WL_Font abstract ui
{
    private Font _fnt;
    private EMonospacing _monospacing;
    private int _spacing;
    private double _lineSpacingTop, _lineSpacingBottom;
    private vector2 _baseScale;

    //===========================================================================
    //
    // Font::Init
    //
    // Performs firs-time initialization.
    //
    //===========================================================================
    protected void Init(Font fnt, EMonospacing monospacing, int spacing, double lineSpacingTop, double lineSpacingBottom, vector2 baseScale)
    {
        if (!WL_VectorMath.IsPositive(baseScale))
        {
            ThrowAbortException("%s: Base scale must be positive.", GetClassName());
        }

        _fnt = fnt;
        _monospacing = monospacing;
        _spacing = spacing;
        _lineSpacingTop = lineSpacingTop;
        _lineSpacingBottom = lineSpacingBottom;
        _baseScale = baseScale;
    }

    //===========================================================================
    //
    // Font::GetSpacing
    //
    // Returns the calculated spacing value.
    //
    //===========================================================================
    protected int GetSpacing() const
    {
        return _spacing;
    }

    //===========================================================================
    //
    // Font::GetFont
    //
    // Returns the font instance wrapped by this Font.
    //
    //===========================================================================
    protected Font GetFont() const
    {
        return _fnt;
    }

    //===========================================================================
    //
    // Font::GetMonospacing
    //
    // Returns the monospacing mode.
    //
    //===========================================================================
    protected EMonospacing GetMonospacing() const
    {
        return _monospacing;
    }

    //===========================================================================
    //
    // Font::BreakLines
    //
    // Breaks the provided string in several lines limited by the provided
    // maximum width.
    //
    //===========================================================================
    BrokenLines BreakLines(string str, double maxWidth) const
    {
        if (IsMonospaced())
        {
            ThrowAbortException("%s: BreakLines is not supported for monospaced fonts.", GetClassName());
        }

        return _fnt.BreakLines(str, int(maxWidth / _baseScale.X));
    }

    //===========================================================================
    //
    // Font::IsMonospaced
    //
    // Checks if this font is monospaced.
    //
    //===========================================================================
    bool IsMonospaced() const
    {
        return _monospacing != Mono_Off;
    }

    //===========================================================================
    //
    // Font::CanPrint
    //
    // Checks if this font can print the provided string.
    //
    //===========================================================================
    bool CanPrint(string str) const
    {
        return _fnt.CanPrint(str);
    }

    //===========================================================================
    //
    // Font::GetStringWidth
    //
    // Returns the string width.
    //
    //===========================================================================
    double GetStringWidth(string str) const
    {
        return (IsMonospaced()
            ? str.Length() * _spacing
            : _fnt.StringWidth(str)) * _baseScale.X;
    }

    //===========================================================================
    //
    // Font::GetHeight
    //
    // Returns the height of this font.
    //
    //===========================================================================
    double GetHeight() const
    {
        return _fnt.GetHeight() * _baseScale.Y + _lineSpacingTop + _lineSpacingBottom;
    }

    //===========================================================================
    //
    // Font::GetLineSpacing
    //
    // Returns the line spacing.
    //
    //===========================================================================
    double, double GetLineSpacing() const
    {
        return _lineSpacingTop, _lineSpacingBottom;
    }

    //===========================================================================
    //
    // Font::GetBaseScale
    //
    // Returns the base scale for this font.
    //
    //===========================================================================
    vector2 GetBaseScale() const
    {
        return _baseScale;
    }

    //===========================================================================
    //
    // Font::GetGlyphHeight
    //
    // Returns the height of the specified character.
    //
    //===========================================================================
    double GetGlyphHeight(int ch) const
    {
        return _fnt.GetGlyphHeight(ch) * _baseScale.Y;
    }

    //===========================================================================
    //
    // Font::GetDisplayTopOffset
    //
    // Returns the top offset of the specified character.
    //
    //===========================================================================
    double GetDisplayTopOffset(int ch) const
    {
        return _fnt.GetDisplayTopOffset(ch) * _baseScale.Y;
    }

    //===========================================================================
    //
    // Font::Equals
    //
    // Checks if this Font instance is equal to the provided one.
    //
    //===========================================================================
    bool Equals(WL_Font other) const
    {
        return other != null && (other == self
            || _fnt == other._fnt
            && _monospacing == other._monospacing
            && _spacing == other._spacing
            && _lineSpacingTop == other._lineSpacingTop
            && _lineSpacingBottom == other._lineSpacingBottom
            && _baseScale == other._baseScale);
    }
}
