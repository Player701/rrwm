//===========================================================================
//
// ColonAppender
//
// A type of TextFormatter that appends a colon to the text.
//
//===========================================================================
class WL_ColonAppender : WL_TextFormatter
{
    //===========================================================================
    //
    // ColonAppender::Create
    //
    // Creates a new instance of the WL_ColonAppender class.
    //
    //===========================================================================
    static WL_ColonAppender Create()
    {
        return new('WL_ColonAppender');
    }

    //===========================================================================
    //
    // ColonAppender::FormatText
    //
    // Appends a colon to the text.
    //
    //===========================================================================
    override string FormatText(string text)
    {
        return text .. ":";
    }
}
