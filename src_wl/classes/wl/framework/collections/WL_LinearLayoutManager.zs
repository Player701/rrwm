//===========================================================================
//
// LinearLayoutManager
//
// Implements a LayoutManager for the ListView that provides a LinearLayout.
//
//===========================================================================
class WL_LinearLayoutManager : WL_OrientedLayoutManager
{
    //===========================================================================
    //
    // LinearLayoutManager::Create
    //
    // Creates a new instance of the LinearLayoutManager class.
    //
    //===========================================================================
    static WL_LinearLayoutManager Create(WL_Orientation orientation, bool reversed = false, bool scrollingEnabled = false)
    {
        let m = new('WL_LinearLayoutManager');
        m.Init(orientation, reversed, scrollingEnabled);

        return m;
    }

    //===========================================================================
    //
    // LinearLayoutManager::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_Orientation orientation, bool reversed, bool scrollingEnabled)
    {
        let ll = WL_LinearLayout.Create();
        ll.SetReversed(reversed);

        Super.Init(ll, orientation, scrollingEnabled);
    }
}
