//===========================================================================
//
// LayoutManager
//
// Base class for layout managers used by ListView.
//
//===========================================================================
class WL_LayoutManager abstract ui
{
    private WL_ViewGroup _layout;

    //===========================================================================
    //
    // LayoutManager::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_ViewGroup layout)
    {
        _layout = layout;
    }

    //===========================================================================
    //
    // LayoutManager::GetLayout
    //
    // Returns the layout.
    //
    //===========================================================================
    WL_ViewGroup GetLayout() const
    {
        return _layout;
    }

    //===========================================================================
    //
    // LayoutManager::GetView
    //
    // Returns the view to be rendered by the ListView,
    // which should contain the layout.
    //
    //===========================================================================
    virtual WL_View GetView() const
    {
        return GetLayout();
    }

    //===========================================================================
    //
    // LayoutManager::ScrollToView
    //
    // If the view with the provided index is not currently visible,
    // scrolls the layout so that it comes into view.
    //
    //===========================================================================
    virtual void ScrollToView(int index, double time)
    {
        // Does nothing by default
    }
}
