//===========================================================================
//
// OrientedLayoutManager
//
// Base class for layout managers that create a subtype of OrientedLayout.
//
//===========================================================================
class WL_OrientedLayoutManager : WL_LayoutManager abstract
{
    private WL_ScrollView _scrollView;

    //===========================================================================
    //
    // OrientedLayoutManager::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_OrientedLayout layout, WL_Orientation orientation, bool scrollingEnabled)
    {
        Super.Init(layout);

        layout.SetOrientation(orientation);

        if (scrollingEnabled)
        {
            _scrollView = WL_ScrollView.Create();
            _scrollView.SetOrientation(orientation);
            _scrollView.SetContent(layout);
        }
    }

    //===========================================================================
    //
    // OrientedLayoutManager::AddScrollArrows
    //
    // Sets up a ScrollArrowToggler that will toggle the visibility
    // of the provided two views when the layout can be scrolled
    // in the corresponding directions.
    //
    //===========================================================================
    void AddScrollArrows(WL_View arrowBack, WL_View arrowForward)
    {
        if (_scrollView != null)
        {
            WL_ScrollArrowToggler.Create(
                arrowBack,
                arrowForward
            ).ObserveView(_scrollView);
        }
    }

    //===========================================================================
    //
    // OrientedLayoutManager::GetView
    //
    // Returns the layout or the ScrollView that wraps it if scrolling is on.
    //
    //===========================================================================
    override WL_View GetView() const
    {
        return _scrollView != null
            ? WL_View(_scrollView)
            : Super.GetView();
    }

    //===========================================================================
    //
    // OrientedLayoutManager::ScrollToView
    //
    // If the view with the provided index is not currently visible,
    // scrolls the layout so that it comes into view.
    //
    //===========================================================================
    override void ScrollToView(int index, double time)
    {
        if (_scrollView == null)
        {
            // Scrolling is not supported
            return;
        }

        let v = GetLayout().GetViewAt(index);

        // Scrolling to views that are gone is not supported,
        // but should probably not be a fatal error
        if (!v.IsPresent())
        {
            return;
        }

        // Get the length and position of the target view.
        WL_Size sz;
        v.GetResolvedSize(sz);

        let ma = WL_OrientedLayout(GetLayout()).GetMainAxis();

        double len = ma.GetLength(sz);
        double pos = ma.GetPosition(v.GetResolvedPosition());

        // Get the length and position of the scroll view.
        WL_Rectangle scrollViewBounds;
        _scrollView.GetResolvedBounds(true, scrollViewBounds);

        double svPos = ma.GetRectPos(scrollViewBounds);
        double svLen = ma.GetRectLength(scrollViewBounds);

        // This is the position of the scroll view's entire contents.
        double startPos = ma.GetPosition(_scrollView.GetContent().GetResolvedPosition());

        // This is the position of the view's starting edge
        // relative to the scroll view's position.
        double relPos = pos - svPos;

        if (relPos < 0)
        {
            // The beginning of the view is not in view,
            // so scroll backward until it is visible.
            Scroll(time, pos - startPos);
        }
        else if (relPos + len > svLen)
        {
            // The end of the view is not in view,
            // so scroll forward until it is visible.
            // Note that it may not be possible to scroll all the way
            // if the view does not fit entirely in the scroll window.
            // This is fine.
            Scroll(time, relPos + len - svLen, true);
        }

        // Otherwise, the view is fully visible, and no scrolling is needed.
    }

    //===========================================================================
    //
    // OrientedLayoutManager::Scroll
    //
    // Scrolls to the specified position or by the provided distance
    // in a fixed amount of time, measured in milliseconds
    // (assuming default animation speed).
    //
    //===========================================================================
    private void Scroll(double time, double value, bool relative = false)
    {
        if (_scrollView == null)
        {
            return;
        }

        if (time < 0)
        {
            ThrowAbortException("%s: Time cannot be negative.", GetClassName());
        }

        if (time == 0)
        {
            // Instant scrolling: set position immediately
            _scrollView.SetScrollPos(value, relative);
        }
        else
        {
            // NB: always use a target scroller here because it is semantically the correct choice.
            double curPos = _scrollView.GetScrollPos();
            double maxPos = _scrollView.GetScrollPosMax();

            value = Clamp(value + (relative ? curPos : 0), 0, maxPos);
            double speed = Abs(value - curPos)/time;

            if (speed > 0)
            {
                WL_Scroller.Create(speed, WL_SCROLL_TARGET, value).ObserveView(_scrollView);
            }
        }
    }
}
