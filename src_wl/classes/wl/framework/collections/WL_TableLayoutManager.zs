//===========================================================================
//
// TableLayoutManager
//
// Implements a LayoutManager for the ListView that provides a TableLayout.
//
//===========================================================================
class WL_TableLayoutManager : WL_OrientedLayoutManager
{
    //===========================================================================
    //
    // TableLayoutManager::Create
    //
    // Creates a new instance of the TableLayoutManager class.
    //
    //===========================================================================
    static WL_TableLayoutManager Create(WL_Orientation orientation, bool reversed = false, bool columnsReversed = false, bool scrollingEnabled = false)
    {
        let m = new('WL_TableLayoutManager');
        m.Init(orientation, reversed, columnsReversed, scrollingEnabled);

        return m;
    }

    //===========================================================================
    //
    // TableLayoutManager::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_Orientation orientation, bool reversed, bool columnsReversed, bool scrollingEnabled)
    {
        let tl = WL_TableLayout.Create();
        tl.SetReversed(reversed);
        tl.SetColumnsReversed(columnsReversed);

        Super.Init(tl, orientation, scrollingEnabled);
    }

    //===========================================================================
    //
    // TableLayoutManager::SetColumnWeight
    //
    // Sets the weight of the specified column of the layout
    // to the provided value.
    //
    //===========================================================================
    void SetColumnWeight(int column, int weight)
    {
        WL_TableLayout(GetLayout()).SetColumnWeight(column, weight);
    }
}
