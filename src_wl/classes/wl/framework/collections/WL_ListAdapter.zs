//===========================================================================
//
// ListAdapter
//
// Base class for adapters that attach can attach to a ListView
// to provide views to render ListView's items.
//
//===========================================================================
class WL_ListAdapter abstract ui
{
    private WL_ListView _listView;

    //===========================================================================
    //
    // ListAdapter::LinkToListView
    //
    // Links to a ListView and immediately calls NotifyDataChanged.
    //
    //===========================================================================
    void LinkToListView(WL_ListView listView)
    {
        if (listView == null)
        {
            ThrowAbortException("%s: Attempt to link to a null ListView.", GetClassName());
        }

        if (_listView != null)
        {
            ThrowAbortException("%s: Attempt to link this adapter to more than one ListView.", GetClassName());
        }

        _listView = listView;
        NotifyDataChanged();
    }

    //===========================================================================
    //
    // ListAdapter::UnlinkFromListView
    //
    // Unlinks from the ListView this adapter is currently linked to.
    //
    //===========================================================================
    void UnlinkFromListView()
    {
        _listView = null;
    }

    //===========================================================================
    //
    // ListAdapter::NotifyDataChanged
    //
    // Synchronizes the contents of the ListView with this adapter.
    //
    //===========================================================================
    void NotifyDataChanged()
    {
        if (_listView != null)
        {
            _listView.SyncWithAdapter();
        }
    }

    //===========================================================================
    //
    // ListAdapter::GetItemCount
    //
    // Returns the number of items provided by this adapter.
    //
    //===========================================================================
    abstract int GetItemCount() const;

    //===========================================================================
    //
    // ListAdapter::GetOrCreateView
    //
    // Creates a new view or re-purposes an existing view for the item
    // at the specified position. The old view to re-purpose can be null.
    //
    //===========================================================================
    abstract WL_View GetOrCreateView(int index, WL_View convertView);
}
