//===========================================================================
//
// GridLayoutManager
//
// Implements a LayoutManager for the ListView that provides a GridLayout.
//
//===========================================================================
class WL_GridLayoutManager : WL_OrientedLayoutManager
{
    //===========================================================================
    //
    // GridLayoutManager::Create
    //
    // Creates a new instance of the GridLayoutManager class.
    //
    //===========================================================================
    static WL_GridLayoutManager Create(WL_Orientation orientation, int columnCount, bool reversed = false, bool columnsReversed = false, bool scrollingEnabled = false)
    {
        let m = new('WL_GridLayoutManager');
        m.Init(orientation, columnCount, reversed, columnsReversed, scrollingEnabled);

        return m;
    }

    //===========================================================================
    //
    // GridLayoutManager::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init(WL_Orientation orientation, int columnCount, bool reversed, bool columnsReversed, bool scrollingEnabled)
    {
        let gl = WL_GridLayout.Create();
        gl.SetColumnCount(columnCount);
        gl.SetReversed(reversed);
        gl.SetColumnsReversed(columnsReversed);

        Super.Init(gl, orientation, scrollingEnabled);
    }
}
