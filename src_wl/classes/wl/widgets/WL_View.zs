//===========================================================================
//
// View
//
// Base class for HUD widgets and widget groups.
//
//===========================================================================
class WL_View abstract ui
{
    private WL_Dimension _width;
    private WL_Dimension _height;
    private WL_Margins _padding;
    private WL_Gravity _gravity;
    private double _alpha;

    private WL_Visibility _visibility;
    private WL_LayoutParams _layoutParams;

    private WL_Size _resolvedSize;
    private WL_Size _resolvedContentSize;
    private vector2 _resolvedPosition;
    private bool _isPositioned;

    private WL_ViewObserverList _observerList;
    private WL_View _parent;
    private WL_View _backgroundView;

    private bool _animationEnabled;

    private bool _isChanged;
    private bool _isContentChanged;

    // Cached values size and bounds resolution
    private WL_Size _lastInputMaxSize, _lastOutputMaxSize, _lastTestSize;
    private WL_Size _lastContentSize, _lastResolvedSize;

    private WL_Rectangle _resolvedBounds, _resolvedBoundsPadded;

    //===========================================================================
    //
    // View::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        _width = WL_Dimension.WrapContent();
        _height = WL_Dimension.WrapContent();
        _padding = WL_Margins.Zero();
        _gravity = WL_Gravity.Unspecified();
        _alpha = 1;
        _observerList = WL_ViewObserverList.Create();
    }

    //===========================================================================
    //
    // View::GetWidth
    //
    // Returns the width of this view.
    //
    //===========================================================================
    WL_Dimension GetWidth() const
    {
        return _width;
    }

    //===========================================================================
    //
    // View::SetWidth
    //
    // Sets a new width for this view.
    //
    //===========================================================================
    void SetWidth(WL_Dimension width)
    {
        if (width == null)
        {
            ThrowAbortException("%s: Width cannot be null.", GetClassName());
        }

        CheckChanged(width.Equals(_width)/*<DebugOnly>*/, string.Format("width change, from %s to %s", _width.ToString(), width.ToString())/*</DebugOnly>*/);
        _width = width;
    }

    //===========================================================================
    //
    // View::GetHeight
    //
    // Returns the height of this view.
    //
    //===========================================================================
    WL_Dimension GetHeight() const
    {
        return _height;
    }

    //===========================================================================
    //
    // View::SetHeight
    //
    // Sets a new height for this view.
    //
    //===========================================================================
    void SetHeight(WL_Dimension height)
    {
        if (height == null)
        {
            ThrowAbortException("%s: Height cannot be null.", GetClassName());
        }

        CheckChanged(height.Equals(_height)/*<DebugOnly>*/, string.Format("height change, from %s to %s", _height.ToString(), height.ToString())/*</DebugOnly>*/);
        _height = height;
    }

    //===========================================================================
    //
    // View::GetPadding
    //
    // Returns the padding of this view group.
    //
    //===========================================================================
    WL_Margins GetPadding() const
    {
        return _padding;
    }

    //===========================================================================
    //
    // View::SetPadding
    //
    // Sets the padding for this view group.
    //
    //===========================================================================
    void SetPadding(WL_Margins padding)
    {
        if (padding == null)
        {
            ThrowAbortException("%s: Padding cannot be null.", GetClassName());
        }

        bool equals = padding.Equals(_padding);

        if (!equals)
        {
            _resolvedBoundsPadded.Undefined();
        }

        CheckChanged(equals/*<DebugOnly>*/, string.Format("padding change, from %s to %s", _padding.ToString(), padding.ToString())/*</DebugOnly>*/);
        _padding = padding;
    }

    //===========================================================================
    //
    // View::GetGravity
    //
    // Returns the gravity for this view's content.
    //
    //===========================================================================
    WL_Gravity GetGravity() const
    {
        return _gravity;
    }

    //===========================================================================
    //
    // View::SetGravity
    //
    // Sets the gravity for this view's content.
    //
    //===========================================================================
    void SetGravity(WL_Gravity gravity)
    {
        if (gravity == null)
        {
            ThrowAbortException("%s: Gravity cannot be null.", GetClassName());
        }

        if (!gravity.Equals(_gravity))
        {
            OnGravityChanged();
        }

        _gravity = gravity;
    }

    //===========================================================================
    //
    // View::OnGravityChanged
    //
    // Called when the view's gravity gets changed.
    //
    //===========================================================================
    protected virtual void OnGravityChanged()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::GetAlpha
    //
    // Returns the alpha.
    //
    //===========================================================================
    double GetAlpha() const
    {
        return _alpha;
    }

    //===========================================================================
    //
    // View::SetAlpha
    //
    // Sets the alpha.
    //
    //===========================================================================
    void SetAlpha(double value)
    {
        if (value < 0 || value > 1)
        {
            ThrowAbortException("%s: Alpha must be between 0 and 1.", GetClassName());
        }

        _alpha = value;
    }

    //===========================================================================
    //
    // View::GetVisibility
    //
    // Returns the visibility of this view.
    //
    //===========================================================================
    WL_Visibility GetVisibility() const
    {
        return _visibility;
    }

    //===========================================================================
    //
    // View::IsPresent
    //
    // Returns true if the view's visibility is not set to Gone.
    //
    //===========================================================================
    bool IsPresent() const
    {
        return _visibility != WL_V_Gone;
    }

    //===========================================================================
    //
    // View::IsVisible
    //
    // Returns true if the view's visibility is set to Visible.
    //
    //===========================================================================
    bool IsVisible() const
    {
        return _visibility == WL_V_Visible;
    }

    //===========================================================================
    //
    // View::SetVisibility
    //
    // Sets a new visibility for this view.
    //
    //===========================================================================
    void SetVisibility(WL_Visibility visibility)
    {
        bool oldPresent = IsPresent();
        _visibility = visibility;

        CheckParentContentChanged(oldPresent == IsPresent()/*<DebugOnly>*/, string.Format("presentation change, from %s to %s", oldPresent ? "present" : "not present", IsPresent() ? "present" : "not present")/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // View::GetMargins
    //
    // Returns the layout parameters of this view.
    //
    //===========================================================================
    WL_LayoutParams GetLayoutParams() const
    {
        return _layoutParams;
    }

    //===========================================================================
    //
    // View::SetLayoutParams
    //
    // Sets the new layout parameters for this view.
    //
    //===========================================================================
    void SetLayoutParams(WL_LayoutParams layoutParams)
    {
        if (_layoutParams != null)
        {
            _layoutParams.UnsubscribeView(self);
        }

        // Mark parent content as changed if this is a new instance
        CheckParentContentChanged(layoutParams == _layoutParams/*<DebugOnly>*/, "layout params change"/*</DebugOnly>*/);

        if (layoutParams != null)
        {
            layoutParams.SubscribeView(self);
        }

        _layoutParams = layoutParams;
    }

    //===========================================================================
    //
    // View::GetObserverList
    //
    // Returns the observer list for this view.
    //
    //===========================================================================
    WL_ViewObserverList GetObserverList() const
    {
        return _observerList;
    }

    //===========================================================================
    //
    // View::ObservedBy
    //
    // Adds the provided ViewObserver to this view's observer list,
    // and returns the view instance for further method chaining.
    //
    //===========================================================================
    WL_View ObservedBy(WL_ViewObserver observer)
    {
        if (observer == null)
        {
            ThrowAbortException("%s: Observer cannot be null.", GetClassName());
        }

        observer.ObserveView(self);
        return self;
    }

    //===========================================================================
    //
    // View::GetParent
    //
    // Returns the parent view of this view.
    // This can be null if the view is not attached to any parent.
    //
    //===========================================================================
    WL_View GetParent() const
    {
        return _parent;
    }

    //===========================================================================
    //
    // View::SetParent
    //
    // Attaches this view to a parent.
    //
    //===========================================================================
    protected void SetParent(WL_View parent)
    {
        if (parent == null)
        {
            ThrowAbortException("%s: Parent cannot be null.", GetClassName());
        }

        if (parent == self)
        {
            ThrowAbortException("%s: A view cannot have itself as its parent.", GetClassName());
        }

        if (_parent != null)
        {
            ThrowAbortException("%s: Attempt to attach this View to more than one parent.", GetClassName());
        }

        _parent = parent;
        CheckParentContentChanged(false/*<DebugOnly>*/, string.Format("child %s added", GetClassName())/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // View::ClearParent
    //
    // Clears the parent view.
    //
    //===========================================================================
    protected void ClearParent()
    {
        CheckParentContentChanged(false/*<DebugOnly>*/, string.Format("child %s removed", GetClassName())/*</DebugOnly>*/);
        _parent = null;
    }

    //===========================================================================
    //
    // View::GetBackgroundView
    //
    // Returns the background view.
    //
    //===========================================================================
    WL_View GetBackgroundView() const
    {
        return _backgroundView;
    }

    //===========================================================================
    //
    // View::SetBackgroundView
    //
    // Sets the background view.
    //
    //===========================================================================
    void SetBackgroundView(WL_View backgroundView)
    {
        // Clear parent of old background view, if present
        if (_backgroundView != null)
        {
            _backgroundView.ClearParent();
        }

        _backgroundView = backgroundView;

        // The size of the background view should always be the same.
        _backgroundView.SetWidth(WL_Dimension.MatchParent());
        _backgroundView.SetHeight(WL_Dimension.MatchParent());

        // Make this view the parent of the background view
        _backgroundView.SetParent(self);
    }

    //===========================================================================
    //
    // View::IsAnimationEnabled
    //
    // Checks if this view has animations enabled.
    //
    //===========================================================================
    bool IsAnimationEnabled() const
    {
        return _animationEnabled;
    }

    //===========================================================================
    //
    // View::CheckChanged
    //
    // Marks the basic properties of this view as changed if the provided
    // value is false, forcing a recalculation during the ResolveSize call.
    //
    //===========================================================================
    private void CheckChanged(bool equals/*<DebugOnly>*/, string reason/*</DebugOnly>*/)
    {
        if (!_isChanged && !equals)
        {
//<DebugOnly>
            Console.Printf("[%s] Changed due to %s", GetClassName(), reason);

//</DebugOnly>
            _isChanged = true;
            _resolvedSize.Undefined();
            _resolvedContentSize.Undefined();
            CheckParentContentChanged(false/*<DebugOnly>*/, reason/*</DebugOnly>*/);
        }
    }

    //===========================================================================
    //
    // View::CheckContentChanged
    //
    // Marks the content of this view as changed if the provided value is false,
    // forcing a call to ResolveContentSize next time before rendering.
    //
    //===========================================================================
    protected void CheckContentChanged(bool equals/*<DebugOnly>*/, string reason/*</DebugOnly>*/)
    {
        if (!_isContentChanged && !equals)
        {
//<DebugOnly>
            Console.Printf("[%s] Content changed due to %s", GetClassName(), reason);

//</DebugOnly>
            _isContentChanged = true;
            _resolvedSize.Undefined();
            _resolvedContentSize.Undefined();

            MarkParentContentChanged(/*<DebugOnly>*/reason/*</DebugOnly>*/);
            OnContentChanged();
        }
    }

    //===========================================================================
    //
    // View::OnContentChanged
    //
    // Called when this view's content gets changed.
    //
    //===========================================================================
    protected virtual void OnContentChanged()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::CheckParentContentChanged
    //
    // Marks the content of the parent view as changed if the provided
    // value is false.
    //
    //===========================================================================
    private void CheckParentContentChanged(bool equals/*<DebugOnly>*/, string reason/*</DebugOnly>*/)
    {
        if (!equals && _parent != null)
        {
            _parent.CheckContentChanged(false/*<DebugOnly>*/, string.Format("child %s --> %s", GetClassName(), reason)/*</DebugOnly>*/);
        }
    }

    //===========================================================================
    //
    // View::MarkParentContentChanged
    //
    // Marks the content of the parent view as changed.
    //
    //===========================================================================
    void MarkParentContentChanged(/*<DebugOnly>*/string reason/*</DebugOnly>*/)
    {
        CheckParentContentChanged(false/*<DebugOnly>*/, reason/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // View::PreResolveSize
    //
    // Observes the PreResolveSize event, and calls OnResolveSize,
    // but only if the visibility of the view is not Gone.
    //
    //===========================================================================
    protected void PreResolveSize()
    {
        _resolvedSize.Undefined();
        _resolvedContentSize.Undefined();

        ObserveEvent(WL_VET_PreResolveSize);

        if (IsPresent())
        {
            // Make sure the background view also observes these events.
            if (_backgroundView != null)
            {
                _backgroundView.PreResolveSize();
            }

            ObserveEvent(WL_VET_OnResolveSize);
            OnResolveSize();
        }
    }

    //===========================================================================
    //
    // View::OnResolveSize
    //
    // This is called after the OnResolveSize event has been observed
    // but before actual size resolution starts.
    // At this point, the view should call PreResolveSize
    // for all its inner views to determine their final visibility.
    //
    //===========================================================================
    protected virtual void OnResolveSize()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::ResolveSize
    //
    // Resolves this view's size
    // based on the maximum possible width and height.
    //
    //===========================================================================
    void ResolveSize(WL_Size maxSize, out WL_Size result = null)
    {
        if (!IsPresent())
        {
            ThrowAbortException("%s: Illegal visibility change: Visibility cannot be set to Gone after OnResolveSize.", GetClassName());
        }

        WL_Size testSize, contentSize;
        bool mustCalculateTest = _isChanged || !maxSize.Equals(_lastInputMaxSize);

        if (mustCalculateTest)
        {
             // Get the max width and height to resolve against
            _lastOutputMaxSize.Create(
                GetMaxLength(maxSize.GetWidth(), GetWidth()),
                GetMaxLength(maxSize.GetHeight(), GetHeight())
            );

            // Cache the last input max size
            _lastInputMaxSize.CreateFrom(maxSize);

            // Reset the changed flag
            _isChanged = false;
        }

        // Check if we can resolve dimensions without resolving content
        double width, height;
        bool widthResolved = EasyResolveDimension(GetWidth(), _lastOutputMaxSize.GetWidth(), width);
        bool heightResolved = EasyResolveDimension(GetHeight(), _lastOutputMaxSize.GetHeight(), height);

         // Account for padding when resolving content size
        let p = GetPadding();

        if (mustCalculateTest || !_lastTestSize.IsDefined())
        {
            // Generate test size to resolve content against
            testSize.Create(
                widthResolved ? width : _lastOutputMaxSize.GetWidth(),
                heightResolved ? height : _lastOutputMaxSize.GetHeight()
            );
            testSize.Pad(p);
        }
        else
        {
            // Restore test size from cache
            testSize.CreateFrom(_lastTestSize);
        }
//<DebugOnly>

        if (GetParent() == null && _isContentChanged)
        {
            Console.Printf("[%s] content changed", GetClassName());
        }
//</DebugOnly>

        // Check if we have to resolve content
        bool mustResolveContent = _isContentChanged || !testSize.Equals(_lastTestSize);
        // Cache the last test size
        _lastTestSize.CreateFrom(testSize);

        // Only do the following calculation if missing cache, of if any of the input parameters have changed
        if (mustResolveContent || !_lastContentSize.IsDefined())
        {
            // Resolve content size
            ResolveContentSize(testSize, _resolvedContentSize);

            // Reset the content changed flag
            _isContentChanged = false;
        }
        else
        {
            // Restore resolved content size from cache
            _resolvedContentSize.CreateFrom(_lastContentSize);
        }

        if (!_resolvedContentSize.Equals(_lastContentSize))
        {
            OnContentSizeChanged();
        }

        // Check if we must re-resolve the size
        bool mustResolveSize = mustCalculateTest || !_resolvedContentSize.Equals(_lastContentSize);
        // Cache the last content size
        _lastContentSize.CreateFrom(_resolvedContentSize);

        if (mustResolveSize || !_lastResolvedSize.IsDefined())
        {
            // If we are going to use resolved content size for final size,
            // we need to add padding to it.
            WL_Size resolvedContentUnpadded;
            _resolvedContentSize.Unpad(p, resolvedContentUnpadded);

            // Set resolved size of the view
            _resolvedSize.Create(
                widthResolved ? width : resolvedContentUnpadded.GetWidth(),
                heightResolved ? height : resolvedContentUnpadded.GetHeight()
            );
            _resolvedSize.ClipBy(_lastOutputMaxSize);

            // Cache the resolved size
            // First check if they're not equal and clear the cached bounds then.
            if (!_resolvedSize.Equals(_lastResolvedSize))
            {
                _resolvedBounds.Undefined();
                _resolvedBoundsPadded.Undefined();
            }

            // Update the cache.
            _lastResolvedSize.CreateFrom(_resolvedSize);
        }
        else
        {
            // Use resolved size from cache
            _resolvedSize.CreateFrom(_lastResolvedSize);
        }

         // Resolve the size of the background
        // based on the resolved size of this view
        if (_backgroundView != null && _backgroundView.IsPresent())
        {
            _backgroundView.ResolveSize(_resolvedSize);
        }

        // Write the output value, if necessary
        if (result != null)
        {
            result.CreateFrom(_resolvedSize);
        }
    }

    //===========================================================================
    //
    // View::OnContentSizeChanged
    //
    // Called when the resolved content size of the view has changed.
    //
    //===========================================================================
    protected virtual void OnContentSizeChanged()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::GetMaxLength
    //
    // Returns the provided value, or the minimum of the provided value
    // and the dimension's value, if the dimension's type is AtMost.
    //
    //===========================================================================
    private static double GetMaxLength(double value, WL_Dimension dimension)
    {
        double result = value;

        if (dimension.GetType() == WL_DT_AtMost)
        {
            result = Min(result, dimension.GetValue());
        }

        return result;
    }

    //===========================================================================
    //
    // View::EasyResolveDimension
    //
    // Attempts to resolve a dimension without calculating the content size.
    //
    // Returns true if the resolution succeeded,
    // and the result argument contains the resolved value.
    //
    // Otherwise, returns false, and the result must be calculated
    // by calling GetContentWidth or GetContentHeight respectively.
    //
    //===========================================================================
    private bool EasyResolveDimension(WL_Dimension dimension, double maxValue, out double result)
    {
        if (dimension == null)
        {
            ThrowAbortException("%s: Dimension is null, did you forget to call Init?", GetClassName());
        }

        switch(dimension.GetType())
        {
            case WL_DT_Fixed:
                result = Min(dimension.GetValue(), maxValue);
                break;
            case WL_DT_MatchParent:
                result = maxValue;
                break;
            case WL_DT_WrapContent:
            case WL_DT_AtMost: // Same as WrapContent for this step
                return false;
            default:
                ThrowAbortException("%s: Dimension::GetType returned an invalid value.", GetClassName());
                break;
        }

        return true;
    }

    //===========================================================================
    //
    // View::GetResolvedSize
    //
    // Returns this view's last resolved size.
    //
    //===========================================================================
    void GetResolvedSize(out WL_Size result) const
    {
        if (!_resolvedSize.IsDefined())
        {
            ThrowAbortException("%s: Attempt to GetResolvedSize prior to actual resolution.", GetClassName());
        }

        result.CreateFrom(_resolvedSize);
    }

    //===========================================================================
    //
    // View::ResolveContentSize
    //
    // Resolves the size of the view's content.
    //
    //===========================================================================
    protected abstract void ResolveContentSize(WL_Size maxSize, out WL_Size result);

    //===========================================================================
    //
    // View::GetResolvedContentSize
    //
    // Returns this view's last resolved content size.
    //
    //===========================================================================
    protected void GetResolvedContentSize(out WL_Size result) const
    {
        if (!_resolvedContentSize.IsDefined())
        {
            ThrowAbortException("%s: Attempt to GetResolvedContentSize prior to actual resolution.", GetClassName());
        }

        result.CreateFrom(_resolvedContentSize);
    }

    //===========================================================================
    //
    // View::PostResolveSize
    //
    // Observes the PostResolveSize event and calls OnPostResolveSize.
    //
    //===========================================================================
    protected void PostResolveSize(bool animationEnabled)
    {
        if (!IsPresent())
        {
            return;
        }

        // Restore resolved sizes from cache
        if (!_resolvedSize.IsDefined()) _resolvedSize.CreateFrom(_lastResolvedSize);
        if (!_resolvedContentSize.IsDefined()) _resolvedContentSize.CreateFrom(_lastContentSize);

        _animationEnabled = animationEnabled;

        ObserveEvent(WL_VET_PostResolveSize);
        OnPostResolveSize(animationEnabled);

        if (_backgroundView != null && _backgroundView.IsPresent())
        {
            _backgroundView.PostResolveSize(animationEnabled);
        }
    }

    //===========================================================================
    //
    // View::OnResolveSize
    //
    // Called after the size of the view and its content has been resolved,
    // and before the PostResolveSize event is observed.
    // This can be used in fragments to start animations
    // and perform other actions that require the knowledge of the view's
    // actual size.
    //
    //===========================================================================
    protected virtual void OnPostResolveSize(bool animationEnabled)
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::Position
    //
    // Positions the view and all its contents at the specified position.
    // This is called as soon as the view's size has been resolved.
    //
    //===========================================================================
    protected void Position(vector2 pos)
    {
        if (pos != _resolvedPosition)
        {
            _resolvedBounds.Undefined();
            _resolvedBoundsPadded.Undefined();
        }

        _resolvedPosition = pos;
        _isPositioned = true;

        // Position the background view at the same point
        if (_backgroundView != null && _backgroundView.IsPresent())
        {
            _backgroundView.Position(pos);
        }

        // Resolve position of content, children etc.
        WL_Rectangle bounds;
        GetResolvedBounds(true, bounds);
        OnPosition(bounds);
    }

    //===========================================================================
    //
    // View::PostPosition
    //
    // Calls OnPostPosition and observes the PostPosition event.
    //
    ///===========================================================================
    protected void PostPosition()
    {
        OnPostPosition();
        ObserveEvent(WL_VET_PostPosition);
    }

    //===========================================================================
    //
    // View::OnPostPosition
    //
    // Called as soon as the view's position, as well as the position of
    // all of its children and content has been resolved.
    // In this method, both resolved size and positions of the view
    // and all its children are available, so this method can be used
    // to alter the view's state based on this data.
    //
    //===========================================================================
    protected virtual void OnPostPosition()
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::OnPosition
    //
    // Called as soon as the view's position has been resolved.
    // In this method, the view should call Position on any child views
    // it contains, as well as position any content, if necessary.
    //
    //===========================================================================
    protected virtual void OnPosition(WL_Rectangle bounds)
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // View::GetResolvedPosition
    //
    // Returns the resolved position of the view.
    // Note that this method can only be called after OnPosition has been called
    // (but not necessarily returned), and resolved positions of the view's
    // children is only available when PostPosition is called.
    //
    //===========================================================================
    vector2 GetResolvedPosition() const
    {
        if (!_isPositioned)
        {
            ThrowAbortException("%s: Attempt to GetResolvedPosition before positioning of the view.", GetClassName());
        }

        return _resolvedPosition;
    }

    //===========================================================================
    //
    // View::GetResolvedBounds
    //
    // Returns the rectangle specifying the view's position and size
    // (the bounding box). This method is only available after the view's
    // size and position have been resolved.
    //
    //===========================================================================
    void GetResolvedBounds(bool padded, out WL_Rectangle result)
    {
        GetResolvedBoundsUnpadded(result);

        if (padded)
        {
            if (!_resolvedBoundsPadded.IsDefined())
            {
                result.Pad(GetPadding(), _resolvedBoundsPadded);
            }

            result.CreateFrom(_resolvedBoundsPadded);
        }
    }

    //===========================================================================
    //
    // View::GetResolvedBoundsUnpadded
    //
    // Returns the resolved bounds or reuses the existing ones from cache.
    //
    //===========================================================================
    private void GetResolvedBoundsUnpadded(out WL_Rectangle result)
    {
        if (!_resolvedBounds.IsDefined())
        {
            WL_Size sz;
            GetResolvedSize(sz);
            _resolvedBounds.Create(GetResolvedPosition(), sz);
        }

        result.CreateFrom(_resolvedBounds);
    }

    //===========================================================================
    //
    // View::ObserveEvent
    //
    // Calls Observe on all ViewObserver instances attached to this view.
    //
    //===========================================================================
    private void ObserveEvent(WL_ViewEventType eventType)
    {
        _observerList.ObserveView(self, eventType);
    }

    //===========================================================================
    //
    // View::Draw
    //
    // Draws this view on the screen.
    //
    //===========================================================================
    protected abstract void Draw(WL_Canvas canvas);

    //===========================================================================
    //
    // View::DrawOn
    //
    // Draws the view on the provided canvas.
    //
    //===========================================================================
    protected void DrawOn(WL_Canvas canvas)
    {
        WL_Rectangle bounds;
        GetResolvedBounds(false, bounds);

        if (bounds.GetArea() == 0)
        {
            // Do not draw with zero area
            return;
        }

        canvas.StartDraw(bounds, GetPadding(), GetAlpha());

        // Draw the background view first
        if (_backgroundView != null && _backgroundView.IsVisible())
        {
            _backgroundView.DrawOn(canvas);
        }

        Draw(canvas);
        canvas.EndDraw();
    }

    //===========================================================================
    //
    // View::ResolveAndDraw
    //
    // Resolves the size of this view and draws it on the provided canvas
    // within the specified render area.
    //
    //===========================================================================
    void ResolveAndDraw(WL_Canvas canvas, WL_Rectangle renderArea)
    {
        // Fire PreResolveSize and OnResolveSize events.
        PreResolveSize();

        // Do not resolve size or draw the view if it's gone.
        if (!IsPresent())
        {
            return;
        }

        // Resolve size against the rendering bounds.
        WL_Size sz;
        renderArea.GetSize(sz);
        ResolveSize(sz);

        // Fire the PostResolveSize events.
        PostResolveSize(canvas.IsAnimationEnabled());

        // Position the view.
        // This will also fire OnPosition events.
        Position(renderArea.GetTopLeft());

        // Fire the PostPosition events.
        PostPosition();

        // Draw the view if it is visible.
        if (IsVisible())
        {
            canvas.PreDraw();
            DrawOn(canvas);
        }
    }
}

//===========================================================================
//
// Visibility
//
// Enumerates possible values returned by View::GetVisibility.
//
//===========================================================================
enum WL_Visibility
{
    // The view is visible and is drawn on the screen as normal.
    WL_V_Visible,

    // The view is not drawn on the screen, but still takes space.
    WL_V_Hidden,

    // The view is not drawn on the screen and does not take any space,
    // considered to be absent from the view hierarchy
    // for all intents and purposes.
    WL_V_Gone
};

//===========================================================================
//
// ViewEventType
//
// Enumerates possible view event types.
//
//===========================================================================
enum WL_ViewEventType
{
    // This event happens before the view size resolution starts.
    // Before and during this event, the view's visibility can be toggled
    // between Gone and Visible/Hidden.
    WL_VET_PreResolveSize,

    // This event happens when the view size resolution starts.
    // It is only fired if the view is not Gone.
    // At this moment, visibility can only be toggled between Visible/Hidden,
    // and the view's actual size has not been determined yet.
    WL_VET_OnResolveSize,

    // This event happens as soon as the view size has been resolved.
    // At this point, visibility can still be toggled between Visible/Hidden,
    // and GetResolvedSize / GetResolvedContentSize can be called.
    WL_VET_PostResolveSize,

    // This event happens as soon as the position of the view,
    // as well as all its children and content, has been resolved.
    // At this point, visibility can still be toggled between Visible/Hidden,
    // and GetResolvedSize, GetResolvedContentSize, GetResolvedPosition
    // and GetResolvedBounds (size + position) can be called.
    WL_VET_PostPosition
};
