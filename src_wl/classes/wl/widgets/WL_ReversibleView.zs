//===========================================================================
//
// ReversibleView
//
// Defines functionality for views that can be reversed.
//
//===========================================================================
mixin class WL_ReversibleView
{
    private bool _isReversed;

    //===========================================================================
    //
    // ReversibleView::IsReversed
    //
    // Returns whether the contents of this view are reversed.
    //
    //===========================================================================
    bool IsReversed() const
    {
        return _isReversed;
    }

    //===========================================================================
    //
    // ReversibleView::SetReversed
    //
    // Sets whether the contents of this view are reversed.
    //
    //===========================================================================
    void SetReversed(bool isReversed)
    {
        CheckContentChanged(isReversed == _isReversed/*<DebugOnly>*/, string.Format("reversed flag change, from %s to %s", _isReversed ? "true" : "false", isReversed ? "true" : "false")/*</DebugOnly>*/);
        _isReversed = isReversed;
    }
}
