//===========================================================================
//
// OrientedView
//
// Defines functionality for views that have an orientation.
//
//===========================================================================
mixin class WL_OrientedView
{
    private WL_Axis _mainAxis;

    //===========================================================================
    //
    // OrientedView::GetOrientation
    //
    // Returns this view's orientation.
    //
    //===========================================================================
    WL_Orientation GetOrientation() const
    {
        return GetMainAxis().GetOrientation();
    }

    //===========================================================================
    //
    // OrientedView::SetOrientation
    //
    // Sets this view's orientation.
    //
    //===========================================================================
    void SetOrientation(WL_Orientation orientation)
    {
        bool equals = _mainAxis != null && _mainAxis.GetOrientation() == orientation;

        if (!equals)
        {
            _mainAxis = WL_Axis.FromOrientation(orientation);
        }

        CheckContentChanged(equals/*<DebugOnly>*/, string.Format("orientation change, to %d", orientation)/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // OrientedView::GetMainAxis
    //
    // Returns the axis that corresponds to this view's orientation.
    //
    //===========================================================================
    WL_Axis GetMainAxis() const
    {
        if (_mainAxis == null)
        {
            ThrowAbortException("%s: Orientation must be set first.", GetClassName());
        }

        return _mainAxis;
    }
}

//===========================================================================
//
// Orientation
//
// Enumerates possible values for view orientation.
//
//===========================================================================
enum WL_Orientation
{
    // Horizontal orientation.
    WL_O_Horizontal,

    // Vertical orientation.
    WL_O_Vertical
};
