//===========================================================================
//
// ListView
//
// The ListView is used to display a collection of items, which does not
// necessarily take the form of a list. The ListView operates
// with a view group that is provided to it by a LayoutManager.
//
// The source of the ListView's items comes from a ListAdapter,
// which tells the ListView how many items it has and generates the views
// that correspond to individual items.
//
// The ListView supports scrolling, but it only works if the LayoutManager
// supports it too, since the ListView does not have any knowledge
// of the layout's structure or its orientation (or even if it has one).
// The methods to check item visibility and scroll to an item
// are convenience methods to avoid maintaining a separate reference
// to the ListView's LayoutManager for the sole purpose of calling them.
//
//===========================================================================
class WL_ListView : WL_WrapperView
{
    private WL_ListAdapter _adapter;
    private WL_LayoutManager _layoutManager;

    private Array<WL_View> _extraViews;

    //===========================================================================
    //
    // ListView::Create
    //
    // Creates a new instance of the ListView class.
    //
    //===========================================================================
    static WL_ListView Create()
    {
        let v = new('WL_ListView');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // ListView::GetAdapter
    //
    // Returns the adapter used by this ListView.
    //
    //===========================================================================
    WL_ListAdapter GetAdapter() const
    {
        return _adapter;
    }

    //===========================================================================
    //
    // ListView::SetAdapter
    //
    // Sets the adapter used by this ListView.
    //
    //===========================================================================
    void SetAdapter(WL_ListAdapter adapter)
    {
        if (_adapter != null)
        {
            _adapter.UnlinkFromListView();
            ClearContents();
        }

        _adapter = adapter;
        _adapter.LinkToListView(self);
    }

    //===========================================================================
    //
    // ListView::GetLayoutManager
    //
    // Returns the layout manager used by this ListView.
    //
    //===========================================================================
    WL_LayoutManager GetLayoutManager() const
    {
        return _layoutManager;
    }

    //===========================================================================
    //
    // ListView::SetLayoutManager
    //
    // Sets the layout manager for this ListView.
    //
    //===========================================================================
    void SetLayoutManager(WL_LayoutManager layoutManager)
    {
        if (_layoutManager != null)
        {
            _layoutManager.GetView().ClearParent();
        }

        _layoutManager = layoutManager;
        _layoutManager.GetView().SetParent(self);
    }

    //===========================================================================
    //
    // ListView::GetLayout
    //
    // Returns the layout for this ListView.
    //
    //===========================================================================
    private WL_ViewGroup GetLayout() const
    {
        if (_layoutManager != null)
        {
            return _layoutManager.GetLayout();
        }

        return null;
    }

    //===========================================================================
    //
    // ListView::SyncWithAdapter
    //
    // Synchronizes this ListView's contents with its adapter.
    //
    //===========================================================================
    void SyncWithAdapter()
    {
        let adapter = GetAdapter();
        let layout = GetLayout();

        if (adapter == null || layout == null)
        {
            return;
        }

        int ac = adapter.GetItemCount();
        int lc = layout.GetCount();

        for (int i = 0; i < ac; i++)
        {
            WL_View oldView = null;

            if (i < lc)
            {
                oldView = layout.GetViewAt(i);
            }
            else
            {
                // Check if we have any extra views available
                int n = _extraViews.Size();

                if (n > 0)
                {
                    oldView = _extraViews[n - 1];
                    _extraViews.Delete(n - 1);
                }
            }

            let itemView = _adapter.GetOrCreateView(i, oldView);

            // Check if we need to add or replace the view
            if (i < lc)
            {
                // Replacing view in slot
                layout.SetViewAt(i, itemView);

                // If views differ, we need to do something with the extra view
                if (oldView != itemView)
                {
                    // Put the extra view on ice for now
                    _extraViews.Push(oldView);
                }
            }
            else
            {
                // Adding new view
                layout.AddView(itemView);
                lc++;
            }
        }

        int extraCount = lc - ac;

        // Remove extra views in layout and put them on ice
        int n = _extraViews.Size();

        if (extraCount > 0)
        {
            for (int i = lc - 1; i >= ac; i--)
            {
                let extraView = layout.RemoveViewAt(i);

                if (n < ac)
                {
                    _extraViews.Push(extraView);
                    n++;
                }
            }
        }

        // Delete extra views if we have too many
        if (n >= ac)
        {
            for (int i = n - 1; i >= ac; i--)
            {
                _extraViews.Delete(i);
            }
        }
    }

    //===========================================================================
    //
    // ListView::ScrollToItem
    //
    // If the item with the provided index is not currently visible,
    // scrolls the list so that it comes into view.
    //
    //===========================================================================
    void ScrollToItem(int index, double time)
    {
        if (_layoutManager == null)
        {
            return;
        }

        _layoutManager.ScrollToView(index, time);
    }

    //===========================================================================
    //
    // ListView::ClearContents
    //
    // Clears the contents of this ListView.
    //
    //===========================================================================
    private void ClearContents()
    {
        let layout = GetLayout();

        if (layout == null)
        {
            layout.Clear();
        }
    }

    //===========================================================================
    //
    // ListView::GetInnerView
    //
    // Returns the inner view for this ListView to display.
    //
    //===========================================================================
    override WL_View GetInnerView()
    {
        if (_layoutManager != null)
        {
            return _layoutManager.GetView();
        }

        return null;
    }
}
