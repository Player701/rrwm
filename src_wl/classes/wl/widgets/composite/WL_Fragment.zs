//===========================================================================
//
// Fragment
//
// Fragment is the base class for custom views with a self-contained
// view hierarchy. A Fragment's purpose is to create a reusable piece
// of interface that serves a single purpose.
//
// A prime example of practical fragment usage is the HudFragment class,
// which is used to display a piece of the HUD. HudFragments can be
// rearranged within their parent layout without disrupting or otherwise
// affecting their functionality.
//
//===========================================================================
class WL_Fragment : WL_WrapperView abstract
{
    private WL_View _root;

    //===========================================================================
    //
    // Fragment::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _root = CreateView();

        if (_root != null)
        {
            _root.SetParent(self);
        }
    }

    //===========================================================================
    //
    // Fragment::GetInnerView
    //
    // Returns the inner view of this fragment.
    //
    //===========================================================================
    final override WL_View GetInnerView()
    {
        return _root;
    }

    //===========================================================================
    //
    // Fragment::CreateView
    //
    // Creates the view that this fragment should display.
    //
    //===========================================================================
    protected abstract WL_View CreateView();
}
