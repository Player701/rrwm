//===========================================================================
//
// WrapperView
//
// Base class for views that wrap a single inner view.
//
//===========================================================================
class WL_WrapperView : WL_View abstract
{
    //===========================================================================
    //
    // WrapperView::GetInnerView
    //
    // Returns the inner view for this WrapperView.
    //
    //===========================================================================
    protected abstract WL_View GetInnerView();

    //===========================================================================
    //
    // WrapperView::IsInnerViewPresent
    //
    // Checks if the inner view is present.
    //
    //===========================================================================
    protected bool IsInnerViewPresent() const
    {
        let inner = GetInnerView();
        return inner != null && inner.IsPresent();
    }

    //===========================================================================
    //
    // WrapperView::IsInnerViewVisible
    //
    // Checks if the inner view is present and visible.
    //
    //===========================================================================
    protected bool IsInnerViewVisible() const
    {
        let inner = GetInnerView();
        return inner != null && inner.IsVisible();
    }

    //===========================================================================
    //
    // WrapperView::OnResolveSize
    //
    // Calls PreResolveSize for the content view, if it exists.
    //
    //===========================================================================
    override void OnResolveSize()
    {
        let inner = GetInnerView();

        if (inner != null)
        {
            inner.PreResolveSize();
        }
    }

    //===========================================================================
    //
    // WrapperView::OnPostResolveSize
    //
    // Calls PostResolveSize for the inner view, if present.
    //
    //===========================================================================
    override void OnPostResolveSize(bool animationEnabled)
    {
        let inner = GetInnerView();

        if (inner != null)
        {
            inner.PostResolveSize(animationEnabled);
        }
    }

    //===========================================================================
    //
    // WrapperView::ResolveContentSize
    //
    // Resolves the size of the inner view, if it's present.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        if (IsInnerViewPresent())
        {
            GetInnerView().ResolveSize(maxSize, result);
        }
        else
        {
            result.Zero();
        }
    }

    //===========================================================================
    //
    // WrapperView::OnPosition
    //
    // Calls PositionInnerView if the inner view is present.
    //
    //===========================================================================
    override void OnPosition(WL_Rectangle bounds)
    {
        if (IsInnerViewPresent())
        {
            PositionInnerView(GetInnerView(), bounds);
        }
    }

    //===========================================================================
    //
    // WrapperView::OnPostPosition
    //
    // Calls PostPosition for the inner view if present.
    //
    //===========================================================================
    override void OnPostPosition()
    {
        if (IsInnerViewPresent())
        {
            GetInnerView().PostPosition();
        }
    }

    //===========================================================================
    //
    // WrapperView::PositionInnerView
    //
    // Positions the inner view.
    //
    //===========================================================================
    protected virtual void PositionInnerView(WL_View innerView, WL_Rectangle bounds)
    {
        innerView.Position(bounds.GetTopLeft());
    }

    //===========================================================================
    //
    // WrapperView::Draw
    //
    // Draws the inner view, if it is visible.
    //
    //===========================================================================
    override void Draw(WL_Canvas canvas)
    {
        if (IsInnerViewVisible())
        {
            GetInnerView().DrawOn(canvas);
        }
    }
}
