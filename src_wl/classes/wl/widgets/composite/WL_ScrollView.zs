//===========================================================================
//
// ScrollView
//
// The ScrollView is used to display content that can be of an arbitrarily
// large size. To allow that, the ScrollView displays only a part
// of the content, and the exact part that gets dispayed is controlled
// by the ScrollView's parameters.
//
// Scrolling can be horizontal or vertical depending on orientation.
// Be sure to set  the corresponding dimension to either MatchParent,
// Fixed, or AtMost, since WrapContent will simply cause the ScrollView
// to stretch along with the content, and no scrolling will be possible.
//
// Do note that to resolve the size of the content along the scrollable axes,
// the ScrollView effectively removes any limit on the content's length
// along those axes. Therefore, it is required that the content's dimensions
// along the scrollable axes are limited. Setting such a dimension
// to MatchParent or adding weighted views or columns to a Linear/Grid/Table
// layout along one of the scrollable axes without limiting the layout's
// size will result in the ScrollView throwing an exception.
//
// The ScrollView supports several animators that can change the scrolling
// position: EdgeScroller, DeltaScroller and TargetScroller.
// See the respective class files for details on how they work.
//
//===========================================================================
class WL_ScrollView : WL_WrapperView
{
    private WL_View _content;
    private double _scrollPos, _scrollPosMax;

    private WL_Rectangle _innerBounds;

    private double _scrollPosPending;
    private bool _contentPositioned;

    // Enable orientation functionality
    mixin WL_OrientedView;

    // Cannot use double because some functions (like BreakLines)
    // accept an int width, and it will cause them to break.
    const LENGTH_LIMIT = int.max;

    //===========================================================================
    //
    // ScrollView::Create
    //
    // Creates a new instance of the ScrollView class.
    //
    //===========================================================================
    static WL_ScrollView Create()
    {
        let v = new('WL_ScrollView');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // ScrollView::Init
    //
    // Peforms first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();

        SetOrientation(WL_O_Horizontal);
        _scrollPosMax = LENGTH_LIMIT;
    }

    //===========================================================================
    //
    // ScrollView::GetContent
    //
    // Returns this view's content.
    //
    //===========================================================================
    WL_View GetContent() const
    {
        return _content;
    }

    //===========================================================================
    //
    // ScrollView::SetContent
    //
    // Sets this view's content.
    //
    //===========================================================================
    void SetContent(WL_View content)
    {
        if (_content != null)
        {
            _content.ClearParent();
        }

        _content = content;
        _content.SetParent(self);
    }

    //===========================================================================
    //
    // ScrollView::GetScrollPos
    //
    // Returns the scrolling position.
    //
    //===========================================================================
    double GetScrollPos() const
    {
        return _scrollPos;
    }

    //===========================================================================
    //
    // ScrollView::GetScrollPosMax
    //
    // Returns the maximum scrolling position.
    //
    // This method should not be called until the ScrollView's size
    // has been resolved, as it will always return 0.
    //
    //===========================================================================
    double GetScrollPosMax() const
    {
        return IsInnerViewPresent() ? _scrollPosMax : 0;
    }

    //===========================================================================
    //
    // ScrollView::SetScrollPos
    //
    // Sets the scrolling position.
    //
    // Note that this method will not update the value of GetScrollPos
    // until the next time the ScrollView is rendered.
    //
    //===========================================================================
    void SetScrollPos(double value, bool relative = false)
    {
        if (value < 0 && !relative)
        {
            ThrowAbortException("%s: Absolute scroll position cannot be negative.", GetClassName());
        }

         _scrollPosPending = Clamp(
            value + (relative ? _scrollPos : 0),
            0,
            GetScrollPosMax()
        );
    }

    //===========================================================================
    //
    // ScrollView::GetInnerView
    //
    // Returns the inner view of this ScrollView.
    //
    //===========================================================================
    final override WL_View GetInnerView()
    {
        return GetContent();
    }

    //===========================================================================
    //
    // ScrollView::ResolveContentSize
    //
    // Resolves the size of the view's content.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        WL_Size testSize;

        testSize.Create(maxSize.GetWidth(), maxSize.GetHeight());
        testSize.SetAxisLength(GetMainAxis(), LENGTH_LIMIT);

        Super.ResolveContentSize(testSize, result);

        if (result.GetWidth() >= LENGTH_LIMIT)
        {
            ThrowAbortException("%s: Scrollable content's width is too large. Make sure children's width is not set to DT_MatchParent.", GetClassName());
        }

        if (result.GetHeight() >= LENGTH_LIMIT)
        {
            ThrowAbortException("%s: Scrollable content's height is too large. Make sure children's height is not set to DT_MatchParent.", GetClassName());
        }
    }

    //===========================================================================
    //
    // ScrollView::OnPostResolveSize
    //
    // Calculates the maximum scrolling positions
    // after the size has been resolved.
    //
    //===========================================================================
    override void OnPostResolveSize(bool animationEnabled)
    {
        WL_Size size, contentSize;
        GetResolvedSize(size);
        GetResolvedContentSize(contentSize);

        // Update the maximum scrolling position and clamp the current position
        let ma = GetMainAxis();
        _scrollPosMax = Max(0, ma.GetLength(contentSize) - ma.Pad(size, GetPadding()));
        _scrollPosPending = Min(_scrollPosPending, _scrollPosMax);

        _contentPositioned = false;
        Super.OnPostResolveSize(animationEnabled);
    }

    //===========================================================================
    //
    // ScrollView::OnPosition
    //
    // Positions the ScrollView's content and sets the content positioned flag.
    //
    //===========================================================================
    override void OnPosition(WL_Rectangle bounds)
    {
        Super.OnPosition(bounds);
        _contentPositioned = true;
    }

    //===========================================================================
    //
    // ScrollView::PositionInnerView
    //
    // Positions the ScrollView's content.
    //
    //===========================================================================
    override void PositionInnerView(WL_View innerView, WL_Rectangle bounds)
    {
        // Now that we are actually positioning the view, the scroll position
        // is no longer considered pending.
        _scrollPos = _scrollPosPending;

        // Position the inner view here and now.
        let innerView = GetInnerView();
        let orientation = GetOrientation();

        innerView.Position((
            bounds.GetLeft() - (orientation == WL_O_Horizontal ? _scrollPos : 0),
            bounds.GetTop() - (orientation == WL_O_Vertical ? _scrollPos : 0)
        ));
    }

    //===========================================================================
    //
    // ScrollView::OnPostPosition
    //
    // Positions the ScrollView's content if that wasn't done during OnPosition
    // because of caching.
    //
    //===========================================================================
    override void OnPostPosition()
    {
        if (!_contentPositioned)
        {
            WL_Rectangle bounds;
            GetResolvedBounds(true, bounds);
            OnPosition(bounds);
        }

        Super.OnPostPosition();
    }

    //===========================================================================
    //
    // ScrollView::Draw
    //
    // Draws the content of this ScrollView.
    //
    //===========================================================================
    override void Draw(WL_Canvas canvas)
    {
        WL_Rectangle bounds;
        GetResolvedBounds(true, bounds);

        // Set bounds to avoid the inner view clipping over
        // the ScrollView's padding.
        canvas.StartDraw(bounds);
        Super.Draw(canvas);
        canvas.EndDraw();
    }
}
