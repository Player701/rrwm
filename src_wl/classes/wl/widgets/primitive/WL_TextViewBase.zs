//===========================================================================
//
// TextViewBase
//
// Base class for views which render text.
//
//===========================================================================
class WL_TextViewBase : WL_PrimitiveView abstract
{
    private int _color;
    private WL_FontGroup _font;
    private WL_FontUseMode _fontUseMode;

    private string _textToRender;
    private WL_Font _pickedFont;

    //===========================================================================
    //
    // TextViewBase::GetTopOffsetAndHeight
    //
    // Returns the maximum top offset and height among all the printable
    // characters in the provided text.
    // Used to implement trimming for Counter and TextView.
    //
    //===========================================================================
    protected static double, double GetTopOffsetAndHeight(WL_Font fnt, string text)
    {
        double offs = -double.infinity, height = -double.infinity;
        int ch, pos = 0, n = text.Length();
        bool hasChars = false;

        while (pos < n)
        {
            [ch, pos] = text.GetNextCodePoint(pos);

            // Skip text color codes
            if (ch == "\034")
            {
                if (pos == n)
                {
                    // No characters left
                    break;
                }

                // Find next character
                ch = text.ByteAt(pos++);

                if (ch != "[")
                {
                    // Skip single-character code
                    continue;
                }

                // Skip multi-character code
                while (pos < n)
                {
                    ch = text.ByteAt(pos++);

                    if (ch == "]")
                    {
                        // Found end of sequence
                        break;
                    }
                }
            }
            else if (fnt.GetGlyphHeight(ch) > 0)
            {
                offs = Max(offs, fnt.GetDisplayTopOffset(ch));
                height = Max(height, fnt.GetGlyphHeight(ch));

                hasChars = true;
            }
        }

        if (!hasChars)
        {
            return 0, 0;
        }

        // Account for line spacing
        double lsTop, lsBottom;
        [lsTop, lsBottom] = fnt.GetLineSpacing();

        return offs, height + lsTop + lsBottom;
    }

    //===========================================================================
    //
    // TextViewBase::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _color = Font.CR_UNTRANSLATED;
    }

    //===========================================================================
    //
    // TextViewBase::GetFontForRendering
    //
    // Gets the font instance to use for drawing text.
    //
    //===========================================================================
    private WL_Font GetFontForRendering()
    {
        switch(_fontUseMode)
        {
            case WL_FUM_AutoPick:
                return _font.PickFont(GetText());
            case WL_FUM_UseMainFont:
                return _font.GetMainFont();
            case WL_FUM_UseBackupFont:
                return _font.GetBackupFont();
        }

        ThrowAbortException("%s: Invalid FontUseMode value: %d.", GetClassName(), _fontUseMode);
        return null;
    }

    //===========================================================================
    //
    // TextViewBase::UpdateCurrentFont
    //
    // Sets the font to be used for rendering according to the current use mode
    // and text.
    //
    //===========================================================================
    private void UpdateCurrentFont(/*<DebugOnly>*/string reason/*</DebugOnly>*/)
    {
        if (_font == null)
        {
            return;
        }

        let pickedFont = GetFontForRendering();

        if (!pickedFont.Equals(_pickedFont))
        {
            CheckContentChanged(false/*<DebugOnly>*/, "current font change due to " .. reason/*</DebugOnly>*/);
            OnCurrentFontChanged(pickedFont);
        }

        _pickedFont = pickedFont;
    }

    //===========================================================================
    //
    // TextViewBase::OnCurrentFontChanged
    //
    // Called when
    //
    //===========================================================================
    protected virtual void OnCurrentFontChanged(WL_Font newFont)
    {
        // Does nothing by default
    }

    //===========================================================================
    //
    // TextViewBase::GetText
    //
    // Returns the text.
    //
    //===========================================================================
    abstract string GetText() const;

    //===========================================================================
    //
    // TextViewBase::GetFont
    //
    // Returns the font.
    //
    //===========================================================================
    WL_FontGroup GetFont() const
    {
        return _font;
    }

    //===========================================================================
    //
    // TextViewBase::SetFont
    //
    // Sets the font.
    //
    //===========================================================================
    void SetFont(WL_FontGroup value)
    {
        _font = value;
        UpdateCurrentFont(/*<DebugOnly>*/"font change"/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // TextViewBase::GetFontUseMode
    //
    // Gets the font use mode of this text view.
    //
    //===========================================================================
    WL_FontUseMode GetFontUseMode() const
    {
        return _fontUseMode;
    }

    //===========================================================================
    //
    // TextViewBase::SetFontUseMode
    //
    // Sets the font use mode for this text view.
    //
    //===========================================================================
    void SetFontUseMode(WL_FontUseMode fontUseMode)
    {
        _fontUseMode = fontUseMode;
        UpdateCurrentFont(/*<DebugOnly>*/"font use mode change"/*</DebugOnly>*/);
    }

    //===========================================================================
    //
    // TextViewBase::GetColor
    //
    // Gets the text color.
    //
    //===========================================================================
    int GetColor() const
    {
        return _color;
    }

    //===========================================================================
    //
    // TextViewBase::SetColor
    //
    // Sets the text color.
    //
    //===========================================================================
    void SetColor(int value)
    {
        _color = value;
    }

    //===========================================================================
    //
    // TextViewBase::SetColorName
    //
    // Sets the text color by name.
    //
    //===========================================================================
    void SetColorName(name colorName)
    {
        SetColor(Font.FindFontColor(colorName));
    }

    //===========================================================================
    //
    // TextViewBase::ResolveContentSize
    //
    // Resolves the size of this view's content.
    //
    //===========================================================================
    final override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        // The font must be set before drawing
        if (_font == null)
        {
            ThrowAbortException("%s: Font not set.", GetClassName());
        }

        string text = GetText();

        // Update current font if the text has changed
        if (_textToRender != text)
        {
            _pickedFont = GetFontForRendering();
            _textToRender = text;
            OnCurrentFontChanged(_pickedFont);
        }

        ResolveTextSize(maxSize, _textToRender, _pickedFont, result);
        result.ScaleBy(GetScale());
    }

    //===========================================================================
    //
    // TextViewBase::ResolveTextSize
    //
    // Resolves the size of the text while using the provided font.
    //
    //===========================================================================
    protected abstract void ResolveTextSize(WL_Size maxSize, string text, WL_Font fnt, out WL_Size result);

    //===========================================================================
    //
    // TextViewBase::Draw
    //
    // Calls DrawText with the text and font parameters determined previously.
    //
    //===========================================================================
    final override void DrawContent(WL_Canvas canvas, WL_Rectangle bounds)
    {
        DrawText(canvas, _textToRender, _pickedFont, bounds);
    }

    //===========================================================================
    //
    // TextViewBase::DrawText
    //
    // Draws the text on the screen within the specified bounds.
    //
    //===========================================================================
    protected abstract void DrawText(WL_Canvas canvas, string text, WL_Font fnt, WL_Rectangle textBounds);
}

//===========================================================================
//
// FontUseMode
//
// Enumerates possible values for TextView font use modes.
//
//===========================================================================
enum WL_FontUseMode
{
    // Switch between main and backup fonts automatically depending on text.
    // This is the default behavior.
    WL_FUM_AutoPick,

    // Always use the main font.
    WL_FUM_UseMainFont,

    // Always use the backup font.
    WL_FUM_UseBackupFont
};
