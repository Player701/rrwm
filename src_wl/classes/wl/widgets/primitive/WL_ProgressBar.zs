//===========================================================================
//
// ProgressBar
//
// A simple progress bar with a background and a foreground color.
//
// The bar fills a certain part of its bounds with the foreground color
// depending on its orientation. By default, horizontal bars are filled
// from left to right is left to right, while vertical ones are filled
// from bottom to top. The direction can be reversed by calling SetReversed.
//
// The foreground color is interpolated on a linear gradient,
// but a single color can also be used.
//
//===========================================================================
class WL_ProgressBar : WL_ProgressColorIndicatorBase
{
    private Color _clrBackground;

    // Enable the orientation functionality
    mixin WL_OrientedView;

    // Enable reversibility
    mixin WL_ReversibleView;

    //===========================================================================
    //
    // ProgressBar::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static WL_ProgressBar Create()
    {
        let v = new('WL_ProgressBar');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // ProgressBar::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    private void Init()
    {
        Super.Init();

        SetOrientation(WL_O_Horizontal);

        // Default colors are white for foreground,
        // fully transparent for background,
        // and the same for the border.
        SetColor(Color(255, 255, 255, 255));
        SetBackgroundColor(Color(0, 0, 0, 0));
    }

    //===========================================================================
    //
    // ProgressBar::SetColor
    //
    // Sets the color to display the main bar in.
    //
    //===========================================================================
    void SetColor(Color clr)
    {
        ClearGradientColors();
        AddGradientColor(clr);
    }

    //===========================================================================
    //
    // ProgressBar::GetBackgroundColor
    //
    // Returns the current background color.
    //
    //===========================================================================
    Color GetBackgroundColor() const
    {
        return _clrBackground;
    }

    //===========================================================================
    //
    // ProgressBar::SetBackgroundColor
    //
    // Sets the background color.
    //
    //===========================================================================
    void SetBackgroundColor(Color backgroundColor)
    {
        _clrBackground = backgroundColor;
    }

    //===========================================================================
    //
    // ProgressBar::DrawContent
    //
    // Draws the content of this view.
    //
    //===========================================================================
    override void DrawContent(WL_Canvas canvas, WL_Rectangle contentBounds)
    {
        // Fill with border color first
        canvas.FillColor(_clrBackground, contentBounds);

        // Determine the fill area
        let ma = GetMainAxis();

        double curLength = ma.GetRectLength(contentBounds);
        double newLength = curLength * GetValue();

        // NB! Vertical reversal means top to bottom
        bool reversed = IsReversed() ^ (GetOrientation() == WL_O_Vertical);

        double curPos = ma.GetRectPos(contentBounds);
        double newPos = curPos + (reversed ? curLength - newLength : 0);

        // Fill with foreground color and remember the background
        WL_Rectangle foreground;
        ma.RepositionAndResize(contentBounds, newPos, newLength, foreground);

        canvas.FillColor(GetInterpolatedColor(), foreground);
    }
}
