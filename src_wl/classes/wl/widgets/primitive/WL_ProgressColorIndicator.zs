//===========================================================================
//
// ProgressColorIndicator
//
// Fills the content area with the color corresponding to the current value,
// interpolated on a linear gradient.
//
//===========================================================================
class WL_ProgressColorIndicator : WL_ProgressColorIndicatorBase
{
    //===========================================================================
    //
    // ProgressColorIndicator::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static WL_ProgressColorIndicator Create()
    {
        let v = new('WL_ProgressColorIndicator');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // ProgressColorIndicator::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();

        // By default, let's interpolate between black and white
        AddGradientColor(Color(255, 0, 0, 0));
        AddGradientColor(Color(255, 255, 255, 255));
    }

    //===========================================================================
    //
    // ProgressColorIndicator::DrawContent
    //
    // Draws the content of this view.
    //
    //===========================================================================
    override void DrawContent(WL_Canvas canvas, WL_Rectangle contentBounds)
    {
        // Fill everything with interpolated color
        canvas.FillColor(GetInterpolatedColor(), contentBounds);
    }
}
