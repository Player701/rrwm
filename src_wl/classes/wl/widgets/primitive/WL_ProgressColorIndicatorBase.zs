//===========================================================================
//
// ProgressColorIndicatorBase
//
// Base class for colored progress indicators.
//
//===========================================================================
class WL_ProgressColorIndicatorBase : WL_ProgressIndicator abstract
{
    private Array<Color> _colors;

    //===========================================================================
    //
    // ProgressColorIndicatorBase::AddGradientColor
    //
    // Adds a color to the gradient.
    // Colors are evenly distributed across the entire scale between 0 and 1.
    //
    //===========================================================================
    void AddGradientColor(Color clr)
    {
        _colors.Push(clr);
    }

    //===========================================================================
    //
    // ProgressColorIndicatorBase::ClearGradientColors
    //
    // Clears all gradient colors.
    //
    //===========================================================================
    void ClearGradientColors()
    {
        _colors.Clear();
    }

    //===========================================================================
    //
    // ProgressColorIndicatorBase::SetAlphaGradient
    //
    // Derives the color gradient from a single color and interpolates
    // between that color's alpha and the provided alpha value.
    //
    //===========================================================================
    void SetAlphaGradient(Color mainColor, double endAlpha)
    {
        ClearGradientColors();
        AddGradientColor(mainColor);
        AddGradientColor(Color(int(endAlpha * 255), mainColor.R, mainColor.G, mainColor.B));
    }

    //===========================================================================
    //
    // ProgressColorIndicatorBase::GetInterpolatedColor
    //
    // Interpolates the color based on the current value.
    //
    //===========================================================================
    protected Color GetInterpolatedColor()
    {
        int cc = _colors.Size();

        if (cc == 0)
        {
            ThrowAbortException("%s: At least one color must be defined.", GetClassName());
        }

        if (cc == 1)
        {
            // Use the only color we have
            return _colors[0];
        }

        // Get the color segment corresponding to the current value.
        double value = GetValue();
        int ind = int(Floor(value * (cc - 1)));

        if (ind == cc - 1)
        {
            // Use the last color
            return _colors[ind];
        }

        // Interpolate between first and second colors
        let c1 = _colors[ind], c2 = _colors[ind + 1];
        value = value*(cc-1) - ind;

        // Calculate the color.
        return Color(
            Interpolate(c1.A, c2.A, value),
            Interpolate(c1.R, c2.R, value),
            Interpolate(c1.G, c2.G, value),
            Interpolate(c1.B, c2.B, value)
        );
    }

    //===========================================================================
    //
    // ProgressColorIndicatorBase::Interpolate
    //
    // Linearly interpolates betwen the provided values.
    //
    //===========================================================================
    private static int Interpolate(int a, int b, double value)
    {
        return int(a + (b - a) * value);
    }
}
