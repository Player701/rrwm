//===========================================================================
//
// ColorIndicator
//
// Fills the content area with the specified color.
//
//===========================================================================
class WL_ColorIndicator : WL_PrimitiveView
{
    // The fill color.
    private Color _clr;

    //===========================================================================
    //
    // ColorIndicator::Create
    //
    // Creates a new instance of the ColorIndicator class.
    //
    //===========================================================================
    static WL_ColorIndicator Create()
    {
        let v = new('WL_ColorIndicator');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // ColorIndicator::GetColor
    //
    // Returns the fill color.
    //
    //===========================================================================
    Color GetColor() const
    {
        return _clr;
    }

    //===========================================================================
    //
    // ColorIndicator::SetColor
    //
    // Sets the fill color.
    //
    //===========================================================================
    void SetColor(Color clr)
    {
        _clr = clr;
    }

    //===========================================================================
    //
    // ColorIndicator::ResolveContentSize
    //
    // The indicator does not derive its content size from any internal state,
    // so the original value should be returned unchanged.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        result.CreateFrom(maxSize);
    }

    //===========================================================================
    //
    // ColorIndicator::DrawContent
    //
    // Draws the content of this view at the specified bounds.
    //
    //===========================================================================
    override void DrawContent(WL_Canvas canvas, WL_Rectangle contentBounds)
    {
        canvas.FillColor(_clr, contentBounds);
    }
}
