//===========================================================================
//
// Counter
//
// The Counter is a type of text view that displays a numeric value.
//
// These are the advantages of using Counter instead of a normal TextView
// when working with numbers:
// - No need to manually call string.Format(...) when setting a value
// - Ability to limit the Counter's minimum width with SetMinDecimalPlaces
//
// The Counter supports coordinated font fallback via FontFallbackCoordinator
// just like the normal TextView.
//
// It is always recommended to use a monospaced font with this widget,
// or SetMinDecimalPlaces may not work correctly.
//
//===========================================================================
class WL_Counter : WL_TextViewBase
{
    private int _value;
    private string _paddedValueStr;
    private int _maxDecimalPlaces;

    // Offset and height of text (for trimming)
    private double _textOffset, _textHeight;

    //===========================================================================
    //
    // Counter::Create
    //
    // Creates a new instance of the Counter class.
    //
    //===========================================================================
    static WL_Counter Create()
    {
        let v = new('WL_Counter');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // Counter::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        SetMinDecimalPlaces(1);
    }

    //===========================================================================
    //
    // Counter::GetValue
    //
    // Returns the numeric value to display.
    //
    //===========================================================================
    int GetValue() const
    {
        return _value;
    }

    //===========================================================================
    //
    // Counter::SetValue
    //
    // Sets the numeric value to display.
    //
    //===========================================================================
    void SetValue(int value)
    {
        CheckContentChanged(value == _value/*<DebugOnly>*/, string.Format("value change, from %d to %d", _value, value)/*</DebugOnly>*/);
        _value = value;
    }

    //===========================================================================
    //
    // Counter::GetText
    //
    // Returns the text.
    //
    //===========================================================================
    override string GetText() const
    {
        return BaseStatusBar.FormatNumber(_value, maxSize: _maxDecimalPlaces);
    }

    //===========================================================================
    //
    // Counter::GetMinDecimalPlaces
    //
    // Gets the minimal number of decimal places to display.
    //
    //===========================================================================
    int GetMinDecimalPlaces() const
    {
        return _paddedValueStr.Length();
    }

    //===========================================================================
    //
    // Counter::SetMinDecimalPlaces
    //
    // Sets the minimal number of decimal places to display.
    // If the actual value is shorter, extra space will be added to the left.
    //
    // Only positive values are allowed.
    //
    //===========================================================================
    void SetMinDecimalPlaces(int minDecimalPlaces)
    {
        if (minDecimalPlaces <= 0)
        {
            ThrowAbortException("%s: Minimum number of decimal places must be positive.", GetClassName());
        }

        if (_maxDecimalPlaces > 0 && minDecimalPlaces > _maxDecimalPlaces)
        {
            ThrowAbortException("%s: Minimum number of decimal places must not be greater than the maximum number (if positive).", GetClassName());
        }

        if (GetMinDecimalPlaces() != minDecimalPlaces)
        {
            string paddedValueStr = string.Format("%0*d", minDecimalPlaces, 0);

            CheckContentChanged(false/*<DebugOnly>*/, string.Format("min decimal places change, from %d to %d", GetMinDecimalPlaces(), paddedValueStr.Length())/*</DebugOnly>*/);
            _paddedValueStr = paddedValueStr;
        }
    }

    //===========================================================================
    //
    // Counter::GetMaxDecimalPlaces
    //
    // Gets the maximum number of decimal places to display.
    //
    //===========================================================================
    int GetMaxDecimalPlaces() const
    {
        return _maxDecimalPlaces;
    }

    //===========================================================================
    //
    // Counter::SetMaxDecimalPlaces
    //
    // Sets the maximum number of decimal places to display.
    // If the actual value takes more decimal places, it will be clamped
    // to this number of places.
    //
    // Setting this value to 0 removes the limit on decimal places.
    // This is the default value. Negative values are not allowed.
    //
    //===========================================================================
    void SetMaxDecimalPlaces(int maxDecimalPlaces)
    {
        if (maxDecimalPlaces < 0)
        {
            ThrowAbortException("%s: Number of max decimal places must be non-negative.", GetClassName());
        }

        if (maxDecimalPlaces > 0 && maxDecimalPlaces < GetMinDecimalPlaces())
        {
            ThrowAbortException("%s: Maximum number of decimal places (if positive) must not be less than the minimum number.", GetClassName());
        }

        CheckContentChanged(maxDecimalPlaces == _maxDecimalPlaces/*<DebugOnly>*/, string.Format("max decimal places change, from %d to %d", _maxDecimalPlaces, maxDecimalPlaces)/*</DebugOnly>*/);
        _maxDecimalPlaces = maxDecimalPlaces;
    }

    //===========================================================================
    //
    // Counter::MinDecimalPlacesFromValue
    //
    // Sets the minimal number of decimal places the same as the length
    // of the provided number. Note that the value can still exceed
    // the provided value, which will make the counter to stretch.
    //
    //===========================================================================
    void MinDecimalPlacesFromValue(int value)
    {
        SetMinDecimalPlaces(string.Format("%d", value).Length());
    }

    //===========================================================================
    //
    // Counter::OnCurrentFontChanged
    //
    // Recalculates the top offset and height of rendered text.
    //
    //===========================================================================
    override void OnCurrentFontChanged(WL_Font newFont)
    {
        // Recalculate the height and offset to trim excess space
        [_textOffset, _textHeight] = GetTopOffsetAndHeight(newFont, "-0123456789");
    }

    //===========================================================================
    //
    // Counter::ResolveTextSize
    //
    // Resolves the size of the text.
    //
    //===========================================================================
    override void ResolveTextSize(WL_Size maxSize, string text, WL_Font fnt, out WL_Size result)
    {
        result.Create(
            Max(fnt.GetStringWidth(text), fnt.GetStringWidth(_paddedValueStr)),
            _textHeight
        );
    }

    //===========================================================================
    //
    // Counter::DrawText
    //
    // Draws the text on the screen within the specified bounds.
    //
    //===========================================================================
    override void DrawText(WL_Canvas canvas, string text, WL_Font fnt, WL_Rectangle textBounds)
    {
        let scale = GetScale();
        double x = textBounds.GetRight() - fnt.GetStringWidth(text) * scale.X;
        double y = textBounds.GetTop() + _textOffset * scale.Y;

        canvas.DrawString(text, fnt, (x, y), GetColor(), GetScale());
    }
}
