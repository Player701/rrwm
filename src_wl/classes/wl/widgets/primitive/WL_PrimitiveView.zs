//===========================================================================
//
// PrimitiveView
//
// Base class for views representing primitives (image, text etc.).
//
//===========================================================================
class WL_PrimitiveView : WL_View abstract
{
    private vector2 _scale;
    private WL_Rectangle _contentBounds, _lastBounds;

    //===========================================================================
    //
    // PrimitiveView::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _scale = (1, 1);
    }

    //===========================================================================
    //
    // PrimitiveView::GetScale
    //
    // Returns scale of this view's content.
    //
    //===========================================================================
    vector2 GetScale() const
    {
        return _scale;
    }

    //===========================================================================
    //
    // PrimitiveView::SetScale
    //
    // Sets the scale of this view's content.
    //
    //===========================================================================
    void SetScaleXY(double x, double y)
    {
        if (x < 0 || y < 0)
        {
            ThrowAbortException("%s: Scale must not be negative.", GetClassName());
        }

        CheckContentChanged(_scale == (x, y)/*<DebugOnly>*/, string.Format("scale change, from (%f, %f) to (%f, %f)", _scale.X, _scale.Y, x, y)/*</DebugOnly>*/);
        _scale = (x, y);
    }

    //===========================================================================
    //
    // PrimitiveView::SetScaleUniform
    //
    // Sets both the X and Y scale of the view's content to the provided value.
    //
    //===========================================================================
    void SetScaleUniform(double scale)
    {
        SetScaleXY(scale, scale);
    }

    //===========================================================================
    //
    // PrimitiveView::OnGravityChanged
    //
    // Clears the cached content bounds.
    //
    //===========================================================================
    override void OnGravityChanged()
    {
        _contentBounds.Undefined();
    }

    //===========================================================================
    //
    // PrimitiveView::OnContentSizeChanged
    //
    // Clears the cached content bounds.
    //
    //===========================================================================
    override void OnContentSizeChanged()
    {
        _contentBounds.Undefined();
    }

    //===========================================================================
    //
    // PrimitiveView::OnPosition
    //
    // Calculates the position of this view's content
    // according to its size and gravity.
    //
    //===========================================================================
    final override void OnPosition(WL_Rectangle bounds)
    {
        if (_contentBounds.IsDefined() && bounds.Equals(_lastBounds))
        {
            return;
        }

        WL_Size sz;
        GetResolvedContentSize(sz);

        bounds.Resize(sz, _contentBounds);
        _contentBounds.Gravitate(GetGravity(), bounds);

        _lastBounds.CreateFrom(bounds);
    }

    //===========================================================================
    //
    // PrimitiveView::Draw
    //
    // Calls DrawContent to draw this view's content.
    //
    //===========================================================================
    final override void Draw(WL_Canvas canvas)
    {
        // Do not bother drawing if any of the scale factors are zero.
        if (WL_VectorMath.IsPositive(GetScale()))
        {
            DrawContent(canvas, _contentBounds);
        }
    }

    //===========================================================================
    //
    // PrimitiveView::DrawContent
    //
    // Draws the content of this view at the specified bounds.
    //
    //===========================================================================
    protected abstract void DrawContent(WL_Canvas canvas, WL_Rectangle contentBounds);
}
