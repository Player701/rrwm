//===========================================================================
//
// ProgressIndicator
//
// Base class for views that can indicate progress.
//
// "This is a progress indicator. It indicates progress."
// (sorry, couldn't resist myself)
//
//===========================================================================
class WL_ProgressIndicator : WL_PrimitiveView abstract
{
    // Progress value (goes from 0 to 1).
    private double _value;

    //===========================================================================
    //
    // ProgressIndicator::GetValue
    //
    // Returns the current value.
    //
    //===========================================================================
    double GetValue() const
    {
        return _value;
    }

    //===========================================================================
    //
    // ProgressIndicator::SetValue
    //
    // Sets the value.
    //
    //===========================================================================
    void SetValue(double value)
    {
        value = Clamp(value, 0, 1);

        CheckContentChanged(value == _value/*<DebugOnly>*/, string.Format("value change, from %f to %f", _value, value)/*</DebugOnly>*/);
        _value = value;
    }

    //===========================================================================
    //
    // ProgressIndicator::SetRelativeValue
    //
    // Derives the value from the provided current, minimum, and maximum values.
    //
    //===========================================================================
    void SetRelativeValue(double curValue, double minValue, double maxValue)
    {
        if (minValue >= maxValue)
        {
            ThrowAbortException("%s: Minimum value must be less than the maximum value.", GetClassName());
        }

        SetValue((curValue - minValue) / (maxValue - minValue));
    }

    //===========================================================================
    //
    // ProgressIndicator::ResolveContentSize
    //
    // The indicator does not derive its content size from any internal state,
    // so the original value should be returned unchanged.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        result.CreateFrom(maxSize);
    }
}
