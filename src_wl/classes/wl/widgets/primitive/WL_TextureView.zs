//===========================================================================
//
// TextureView
//
// The TextureView is used to display a texture.
// The texture it displays can be set directly from a TextureID,
// or from a known texture name string via SetTextureName.
//
// The following flags are supported to change the texture's appearance:
// - Animation (enabled by default)
// - Horizontal mirroring
// - Translation to player's color scheme
//
// The texture drawing algorithm for the TextureView is as follows:
//
// 1) The texture's size is compared to the maximum size the TextureView
//    can take. Provided that the corresponding options (controlled with
//    SetUpscaleEnabled / SetDownscaleEnabled) are enabled, the texture's
//    initial size then gets adjusted to match the size limit.
//
// 2) If offsets are enabled (see SetUseOffsets), then the initial size
//    is increased by the absolute values of the offsets.
//
// 3) The initial size along each of the two axes is then passed
//    to the corresponding TextureAxisBehaviors. See the list of available
//    behaviors further below. The default behavior simply returns
//    the unchanged texture size.
//
// 4) If offsets are enabled, the texture's outer frame will be increased
//    by the corresponding values, and the texture will be shifted within
//    the frame. Positive offsets shift to the left and top, negative
//    offsets shift to the right and bottom.
//
// 5) The TextureView draws a grid of textures based on the values
//    returned by the TextureAxisBehavior for each axis. The default
//    behavior generates a single texture. The order of drawing
//    is affected by gravity (left/right and top/bottom).
//
//
// Here is a list of all available TextureAxisBehaviors:
//
// - Normal:
// Generates one texture and does not change the texture size.
// This behavior is enabled by default.
//
// - Stretch:
// Generates one texture and makes the texture size as large as possible.
//
// - TileRepeat:
// Generates a pre-defined number of textures, either with their normal size,
// or by dividing the maximum size evenly.
//
// - TileFill:
// Generates as many textures as possible, with the option to remove the
// last texture if it fits only partially.
//
//
// In addition to all of the above, the TextureView can be scaled
// with SetScaleXY / SetScaleUniform methods.
//
//===========================================================================
class WL_TextureView : WL_PrimitiveView
{
    private TextureID _textureId;
    private bool _isValidTexture;

    private WL_Size _textureSize;
    private vector2 _textureOffsets;

    private WL_TextureDrawFlags _flags;
    private bool _upscaleEnabled;
    private bool _downscaleEnabled;
    private bool _useOffsets;

    private WL_TextureAxisBehavior _axisBehaviorX;
    private WL_TextureAxisBehavior _axisBehaviorY;

    private WL_TextureAxisParams _axisParamsX;
    private WL_TextureAxisParams _axisParamsY;

    private WL_Size _texBaseSize, _texInnerSize;
    private WL_Gravity _texOffsetGravity;

    //===========================================================================
    //
    // TextureView::Create
    //
    // Returns a new instance of the TextureView class.
    //
    //===========================================================================
    static WL_TextureView Create()
    {
        let v = new('WL_TextureView');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // TextureView::CheckValidTexture
    //
    // Checks that the provided texture is valid.
    //
    //===========================================================================
    private static bool CheckValidTexture(TextureID tex)
    {
        // Check for TNT1A0. For some reason, it is considered a valid texture
        // but for all intents and purposes it is not.
        return tex.IsValid() && TexMan.GetName(tex) != "TNT1A0";
    }

    //===========================================================================
    //
    // TextureView::GetTextureData
    //
    // Returns the size and offsets for the provided texture.
    //
    //===========================================================================
    private static vector2 GetTextureData(TextureID tex, out WL_Size result)
    {
        let scaledSize = TexMan.GetScaledSize(tex);
        result.Create(scaledSize.X, scaledSize.Y);
        return TexMan.GetScaledOffset(tex);
    }

    //===========================================================================
    //
    // TextureView::Init
    //
    // Peforms first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();

        // in 99.9% of all cases, images need to be centered within the bounds.
        SetGravity(WL_Gravity.Center());

        _flags |= WL_TDF_ANIMATED;
        _downscaleEnabled = true;

        _axisBehaviorX = WL_TextureAxisBehavior.Normal();
        _axisBehaviorY = WL_TextureAxisBehavior.Normal();

        _axisParamsX = WL_TextureAxisParams.Create();
        _axisParamsY = WL_TextureAxisParams.Create();
    }

    //===========================================================================
    //
    // TextureView::IsValidTexture
    //
    // Checks if this TextureView contains a valid texture.
    //
    //===========================================================================
    bool IsValidTexture() const
    {
        return _isValidTexture;
    }

    //===========================================================================
    //
    // TextureView::GetTexture
    //
    // Returns the texture ID.
    //
    //===========================================================================
    TextureID GetTexture() const
    {
        return _textureId;
    }

    //===========================================================================
    //
    // TextureView::SetTexture
    //
    // Sets the texture ID.
    //
    //===========================================================================
    void SetTexture(TextureID tex)
    {
        _textureId = tex;
        bool isValidTexture = CheckValidTexture(tex);

        if (isValidTexture != _isValidTexture)
        {
            CheckContentChanged(false/*<DebugOnly>*/, "change between invalid and valid texture"/*</DebugOnly>*/);
        }

        _isValidTexture = isValidTexture;

        if (isValidTexture)
        {
            WL_Size textureSize;
            let textureOffsets = GetTextureData(tex, textureSize);

            CheckContentChanged(textureSize.Equals(_textureSize)/*<DebugOnly>*/, "texture size change"/*</DebugOnly>*/);
            CheckContentChanged(textureOffsets == _textureOffsets/*<DebugOnly>*/, "texture offsets change"/*</DebugOnly>*/);

            _textureSize.CreateFrom(textureSize);
            _textureOffsets = textureOffsets;
        }
        else
        {
            _textureSize.Undefined();
        }
    }

    //===========================================================================
    //
    // TextureView::SetTextureName
    //
    // Sets the texture ID generated from the provided texture name.
    //
    //===========================================================================
    void SetTextureName(string texName, int usetype = TexMan.Type_Any)
    {
        SetTexture(TexMan.CheckForTexture(texName, usetype));
    }

    //===========================================================================
    //
    // TextureView::SetInventoryIcon
    //
    // Sets the texture ID generated from the icon
    // of the provided inventory item.
    //
    //===========================================================================
    void SetInventoryIcon(Inventory item, int flags = 0)
    {
        SetTexture(BaseStatusBar.GetInventoryIcon(item, flags));
    }

    //===========================================================================
    //
    // TextureView::GetTextureSize
    //
    // Returns the original size of the texture.
    // If the texture is either not set or not valid, an exception is thrown.
    //
    //===========================================================================
    double, double GetTextureSize() const
    {
        if (!_textureSize.IsDefined())
        {
            ThrowAbortException("%s: Attempt to get texture size for an undefined texture.", GetClassName());
        }

        return _textureSize.GetWidth(), _textureSize.GetHeight();
    }

    //===========================================================================
    //
    // TextureView::IsUpscaleEnabled
    //
    // Checks whether automatic upscaling is enabled.
    //
    //===========================================================================
    bool IsUpscaleEnabled() const
    {
        return _upscaleEnabled;
    }

    //===========================================================================
    //
    // TextureView::SetUpscaleEnabled
    //
    // Sets whether automatic upscaling is enabled.
    //
    //===========================================================================
    void SetUpscaleEnabled(bool upscaleEnabled)
    {
        CheckContentChanged(upscaleEnabled == _upscaleEnabled/*<DebugOnly>*/, string.Format("upscale flag change, from %s to %s", _upscaleEnabled ? "true" : "false", upscaleEnabled ? "true" : "false")/*</DebugOnly>*/);
        _upscaleEnabled = upscaleEnabled;
    }

    //===========================================================================
    //
    // TextureView::IsDownscaleEnabled
    //
    // Checks whether automatic downscaling is enabled.
    // By default, textures will be downscaled if they are too large to fit.
    //
    //===========================================================================
    bool IsDownscaleEnabled() const
    {
        return _downscaleEnabled;
    }

    //===========================================================================
    //
    // TextureView::SetDownscaleEnabled
    //
    // Sets whether automatic downscaling is enabled.
    //
    //===========================================================================
    void SetDownscaleEnabled(bool downscaleEnabled)
    {
        CheckContentChanged(downscaleEnabled == _downscaleEnabled/*<DebugOnly>*/, string.Format("downscale flag change, from %s to %s", _downscaleEnabled ? "true" : "false", downscaleEnabled ? "true" : "false")/*</DebugOnly>*/);
        _downscaleEnabled = downscaleEnabled;
    }

    //===========================================================================
    //
    // TextureView::GetUseOffsets
    //
    // Checks whether texture offsets are used.
    //
    //===========================================================================
    bool GetUseOffsets() const
    {
        return _useOffsets;
    }

    //===========================================================================
    //
    // TextureView::SetUseOffsets
    //
    // Sets whether texture offsets are used.
    // Positive offsets will add extra space to the bottom and right,
    // while negative offsets will add extra space to the left and top.
    //
    //===========================================================================
    void SetUseOffsets(bool useOffsets)
    {
        CheckContentChanged(useOffsets == _useOffsets/*<DebugOnly>*/, string.Format("offset flag change, from %s to %s", _useOffsets ? "true" : "false", useOffsets ? "true" : "false")/*</DebugOnly>*/);
        _useOffsets = useOffsets;
    }

    //===========================================================================
    //
    // TextureView::GetAxisBehaviorX
    //
    // Returns the X axis behavior for this TextureView.
    //
    //===========================================================================
    WL_TextureAxisBehavior GetAxisBehaviorX() const
    {
        return _axisBehaviorX;
    }

    //===========================================================================
    //
    // TextureView::SetAxisBehaviorX
    //
    // Sets the X axis behavior for this TextureView.
    //
    //===========================================================================
    void SetAxisBehaviorX(WL_TextureAxisBehavior axisBehaviorX)
    {
        if (axisBehaviorX == null)
        {
            ThrowAbortException("%s: Axis behavior cannot be null.", GetClassName());
        }

        CheckContentChanged(axisBehaviorX.Equals(_axisBehaviorX)/*<DebugOnly>*/, "axis behavior X change"/*</DebugOnly>*/);
        _axisBehaviorX = axisBehaviorX;
    }

    //===========================================================================
    //
    // TextureView::GetAxisBehaviorY
    //
    // Returns the Y axis behavior for this TextureView.
    //
    //===========================================================================
    WL_TextureAxisBehavior GetAxisBehaviorY() const
    {
        return _axisBehaviorY;
    }

    //===========================================================================
    //
    // TextureView::SetAxisBehaviorY
    //
    // Sets the Y axis behavior for this TextureView.
    //
    //===========================================================================
    void SetAxisBehaviorY(WL_TextureAxisBehavior axisBehaviorY)
    {
        if (axisBehaviorY == null)
        {
            ThrowAbortException("%s: Axis behavior cannot be null.", GetClassName());
        }

        CheckContentChanged(axisBehaviorY.Equals(_axisBehaviorY)/*<DebugOnly>*/, "axis behavior Y change"/*</DebugOnly>*/);
        _axisBehaviorY = axisBehaviorY;
    }

    //===========================================================================
    //
    // TextureView::ToggleDrawFlag
    //
    // Toggles one of the draw flags.
    //
    //===========================================================================
    private void ToggleDrawFlag(WL_TextureDrawFlags flag, bool value)
    {
        _flags = value ? (_flags | flag) : (_flags & ~flag);
    }

    //===========================================================================
    //
    // TextureView::IsTranslatable
    //
    // Checks whether the texture is translatable.
    //
    //===========================================================================
    bool IsTranslatable() const
    {
        return _flags & WL_TDF_TRANSLATABLE;
    }

    //===========================================================================
    //
    // TextureView::SetTranslatable
    //
    // Sets the translatable option.
    // Translated textures will use the console player's translation.
    //
    //===========================================================================
    void SetTranslatable(bool value)
    {
        ToggleDrawFlag(WL_TDF_TRANSLATABLE, value);
    }

    //===========================================================================
    //
    // TextureView::IsAnimted
    //
    // Checks whether the texture is animated.
    //
    //===========================================================================
    bool IsAnimated() const
    {
        return _flags & WL_TDF_ANIMATED;
    }

    //===========================================================================
    //
    // TextureView::SetTranslatable
    //
    // Sets whether the texture is animated.
    // TextureView is animted by default.
    //
    //===========================================================================
    void SetAnimated(bool value)
    {
        ToggleDrawFlag(WL_TDF_ANIMATED, value);
    }

    //===========================================================================
    //
    // TextureView::IsFlipped
    //
    // Checks whether the texture is flipped.
    //
    //===========================================================================
    bool IsFlipped() const
    {
        return _flags & WL_TDF_FLIPPED;
    }

    //===========================================================================
    //
    // TextureView::SetFlipped
    //
    // Sets whether the texture is flipped.
    //
    //===========================================================================
    void SetFlipped(bool value)
    {
        ToggleDrawFlag(WL_TDF_FLIPPED, value);
    }

    //===========================================================================
    //
    // TextureView::SetDimmed
    //
    // Sets whether the texture is dimmed.
    //
    //===========================================================================
    void SetDimmed(bool value)
    {
        ToggleDrawFlag(WL_TDF_DIM, value);
    }

    //===========================================================================
    //
    // TextureView::IsDimmed
    //
    // Gets whether the texture is dimmed.
    //
    //===========================================================================
    bool IsDimmed() const
    {
        return _flags & WL_TDF_DIM;
    }

    //===========================================================================
    //
    // TextureView::ResolveContentSize
    //
    // Resolves the size of the view's content.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        if (!IsValidTexture())
        {
            result.Zero();
            return;
        }

        WL_Size txSize;
        txSize.CreateFrom(_textureSize);

        // Add offsets if we use them
        if (_useOffsets)
        {
            txSize.Create(
                txSize.GetWidth() + abs(_textureOffsets.X),
                txSize.GetHeight() + abs(_textureOffsets.Y)
            );
        }

        // Account for scale
        txSize.ScaleBy(GetScale());

        // Check if we need scaling.
        bool sizeLess = txSize.IsLessThan(maxSize);
        bool doScale = sizeLess && _upscaleEnabled
                   || !sizeLess && _downscaleEnabled;

        if (doScale)
        {
            txSize.ScaleTo(maxSize);
        }

        // Resolve axis parameters
        _axisParamsX.ApplyBehavior(_axisBehaviorX, txSize.GetWidth(), maxSize.GetWidth());
        _axisParamsY.ApplyBehavior(_axisBehaviorY, txSize.GetHeight(), maxSize.GetHeight());

        result.Create(
            _axisParamsX.GetContentLength(),
            _axisParamsY.GetContentLength()
        );
    }

    //===========================================================================
    //
    // TextureView::OnContentChanged
    //
    // Clears the cached values used to draw this TextureView's content.
    //
    //===========================================================================
    override void OnContentChanged()
    {
        _texBaseSize.Undefined();
        _texInnerSize.Undefined();
        _texOffsetGravity = null;
    }

    //===========================================================================
    //
    // TextureView::DrawContent
    //
    // Draws the content of this view.
    //
    //===========================================================================
    override void DrawContent(WL_Canvas canvas, WL_Rectangle bounds)
    {
        if (!IsValidTexture())
        {
            return;
        }

        // If textures repeat, make sure they do not overflow our padding
        canvas.StartDraw(bounds);

        // This is the final base size for one texture
        if (!_texBaseSize.IsDefined())
        {
            _texBaseSize.Create(
                _axisParamsX.GetTextureLength(),
                _axisParamsY.GetTextureLength()
            );
        }

        // "t" is for TEXTURE size/position/offsets
        double tw = _texBaseSize.GetWidth();
        double th = _texBaseSize.GetHeight();

        // Check if we need to use offsets
        if (_useOffsets && (_texOffsetGravity == null || !_texInnerSize.IsDefined()))
        {
            // Get offsets
            double ox = _textureOffsets.X;
            double oy = _textureOffsets.Y;

            // Adjust offsets to match the scaled texture size
            ox = ox * tw / (_textureSize.GetWidth() + abs(ox));
            oy = oy * th / (_textureSize.GetHeight() + abs(oy));

            // Calculate gravity based on offset signs
            _texOffsetGravity = WL_Gravity.Create(
                ox > 0 ? WL_HG_Left : WL_HG_Right,
                oy > 0 ? WL_VG_Top : WL_VG_Bottom
            );

            // Get the inner texture size (without offsets)
            _texInnerSize.Create(tw - abs(ox), th - abs(oy));
        }

        // Get the number of texture to draw along each axis
        // and the corresponding gravity.
        // Gravity affects the drawing order.
        int n = _axisParamsX.GetTextureCount();
        let hg = GetGravity().GetHorizontal();

        int m = _axisParamsX.GetTextureCount();
        let vg = GetGravity().GetVertical();

        // "b" is for BOUNDS size/position
        double bw = bounds.GetWidth();
        double bh = bounds.GetHeight();
        vector2 bp = bounds.GetTopLeft();

        // Draw the texture as many times as necessary
        double x = 0;

        for (int i = 0; i < n; i++, x += tw)
        {
            double y = 0;

            for (int j = 0; j < m; j++, y += th)
            {
                int index = i * m + j;

                vector2 tp = (
                    hg == WL_HG_Right  ? bw - x - tw : x,
                    vg == WL_VG_Bottom ? bh - y - th : y
                ) + bp;

                WL_Rectangle tb;
                tb.Create(tp, _texBaseSize);

                if (_texInnerSize.IsDefined())
                {
                    // Gravitate actual texture box w/offsets
                    WL_Rectangle outer;
                    outer.Create(tp, _texInnerSize);
                    outer.Gravitate(_texOffsetGravity, tb, tb);
                }

                canvas.DrawTexture(_textureId, tb, _flags);
            }
        }

        canvas.EndDraw();
    }
}
