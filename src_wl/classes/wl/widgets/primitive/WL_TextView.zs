//===========================================================================
//
// TextView
//
// The TextView is used to display arbitrary text that can be wrapped.
// The text it displays can also be automatically localized, if necessary.
//
// Note that the TextView will always take as much width as possible,
// and only use enough height afterwards to render all lines of the text.
//
// The TextView supports coordinated font fallback
// via FontFallbackCoordinator.
//
//===========================================================================
class WL_TextView : WL_TextViewBase
{
    // Enumerates flags that affect TextView behavior.
    enum TextViewFlags
    {
        // Makes the TextView localizable.
        TVF_LOCALIZED    = 1,

        // Automatically collapses the TextView when there is no text.
        // Otherwise, it will still use up the height of its font.
        TVF_AUTOCOLLAPSE = 1 << 1
    };

    private string _text, _realText;
    private string _language;
    private bool _lineTrimming;

    private TextViewFlags _flags;
    private WL_TextFormatter _formatter;

    private BrokenLines _bl;
    private Array<double> _lineOffsets, _lineHeights;

    //===========================================================================
    //
    // TextView::Create
    //
    // Creates a new instance of the TextView object.
    //
    //===========================================================================
    static WL_TextView Create()
    {
        let v = new('WL_TextView');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // TextView::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _language = language;
    }

    //===========================================================================
    //
    // TextView::GetText
    //
    // Returns the text.
    //
    //===========================================================================
    override string GetText() const
    {
        string result = _realText;

        // If there is a formatter, use it
        if (_formatter != null)
        {
            result = _formatter.FormatText(result);
        }

        return result;
    }

    //===========================================================================
    //
    // TextView::SetText
    //
    // Sets the text.
    //
    //===========================================================================
    void SetText(string text)
    {
        _text = text;

        // Update the actual text too
        SetRealText();
    }

    //===========================================================================
    //
    // TextView::SetRealText
    //
    // Sets the actual text to render in this view.
    //
    //===========================================================================
    private void SetRealText()
    {
        // Check if the text is localized
        string realText = IsLocalized() ? StringTable.Localize(_text) : _text;

        CheckContentChanged(realText == _realText/*<DebugOnly>*/, string.Format("text change, from \"%s\" to \"%s\"", _realText, realText)/*</DebugOnly>*/);
        _realText = realText;
    }

    //===========================================================================
    //
    // TextView::OnResolveSize
    //
    // Updates the text if the language has changed.
    //
    //===========================================================================
    override void OnResolveSize()
    {
        // If the text is localized, it has to be updated every time
        // when the language changes.
        if (IsLocalized() && language != _language)
        {
            SetRealText();
            _language = language;
        }
    }

    //===========================================================================
    //
    // TextView::IsLocalized
    //
    // Checks if this TextView is localizable.
    //
    //===========================================================================
    bool IsLocalized() const
    {
        return _flags & TVF_LOCALIZED;
    }

    //===========================================================================
    //
    // TextView::SetLocalized
    //
    // Sets the localizable flag for this TextView.
    //
    //===========================================================================
    void SetLocalized(bool value)
    {
        ToggleFlag(TVF_LOCALIZED, value);

        // Update the real text according to localization
        SetRealText();
    }

    //===========================================================================
    //
    // TextView::IsAutoCollapse
    //
    // Checks if this TextView collapses automatically when its text is empty.
    // If auto-collapse if off, TextView will still use up some height.
    //
    //===========================================================================
    bool IsAutoCollapse() const
    {
        return _flags & TVF_AUTOCOLLAPSE;
    }

    //===========================================================================
    //
    // TextView::SetAutoCollapse
    //
    // Sets the auto-collapse value for this TextView.
    //
    //===========================================================================
    void SetAutoCollapse(bool value)
    {
        ToggleFlag(TVF_AUTOCOLLAPSE, value);
    }

    //===========================================================================
    //
    // TextView::ToggleFlag
    //
    // Toggles one of the TextView's flags.
    //
    //===========================================================================
    private void ToggleFlag(TextViewFlags flag, bool value)
    {
        TextViewFlags flags = value ? (_flags | flag) : (_flags & ~flag);

        CheckContentChanged(flags == _flags/*<DebugOnly>*/, "flags change"/*</DebugOnly>*/);
        _flags = flags;
    }

    //===========================================================================
    //
    // TextView::GetFormatter
    //
    // Returns the text formatter set for this TextView.
    //
    //===========================================================================
    WL_TextFormatter GetFormatter() const
    {
        return _formatter;
    }

    //===========================================================================
    //
    // TextView::SetFormatter
    //
    // Sets the text formatter for this TextView.
    //
    //===========================================================================
    void SetFormatter(WL_TextFormatter formatter)
    {
        CheckContentChanged(formatter.Equals(_formatter)/*<DebugOnly>*/, "formatter change"/*</DebugOnly>*/);
        _formatter = formatter;
    }

    //===========================================================================
    //
    // TextView::GetLineTrimming
    //
    // Checks if line trimming is enabled for this TextView.
    //
    //===========================================================================
    bool GetLineTrimming() const
    {
        return _lineTrimming;
    }

    //===========================================================================
    //
    // TextView::SetLineTrimming
    //
    // Enables or disables line trimming for this TextView.
    //
    //===========================================================================
    void SetLineTrimming(bool lineTrimming)
    {
        CheckContentChanged(lineTrimming == _lineTrimming/*<DebugOnly>*/, "line trimming change"/*</DebugOnly>*/);
        _lineTrimming = lineTrimming;
    }

    //===========================================================================
    //
    // TextView::ResolveTextSize
    //
    // Resolves the size of the text based on the provided maximum size.
    // Additionally, returns the BrokenLines instance
    // that was used to determine the size.
    //
    //===========================================================================
    override void ResolveTextSize(WL_Size maxSize, string text, WL_Font fnt, out WL_Size result)
    {
        if (text.Length() == 0)
        {
            double height = IsAutoCollapse() ? 0 : fnt.GetHeight();
            result.Create(0, height);
            return;
        }

        double width, offs, height;
        bool trim = GetLineTrimming();
        let scale = GetScale();

        // If line trimming is disabled, use the font height for every line.
        if (!trim)
        {
            offs = 0;
            height = fnt.GetHeight();
        }

        _lineOffsets.Clear();
        _lineHeights.Clear();

        // Do not divide by 0, or the universe will explode (VM abort)
        if (!fnt.IsMonospaced() && scale.X > 0)
        {
            _bl = fnt.BreakLines(text, maxSize.GetWidth() / scale.X);

            width = 0;

            for (int i = 0; i < _bl.Count(); i++)
            {
                string curLine = _bl.StringAt(i);
                width = Max(width, fnt.GetStringWidth(curLine));

                if (trim)
                {
                    [offs, height] = GetTopOffsetAndHeight(fnt, curLine);
                }

                _lineOffsets.Push(offs);
                _lineHeights.Push(height);
            }
        }
        else
        {
            _bl = null;
            width = fnt.GetStringWidth(text);

            if (trim)
            {
                [offs, height] = GetTopOffsetAndHeight(fnt, text);
            }

            _lineOffsets.Push(offs);
            _lineHeights.Push(height);
        }

        // Calculate the total height as the sum of heights of all lines
        height = 0;

        for (int i = 0; i < _lineHeights.Size(); i++)
        {
            height += _lineHeights[i];
        }

        result.Create(width, height);
    }

    //===========================================================================
    //
    // TextView::DrawText
    //
    // Draws the text on the screen within the specified bounds.
    //
    //===========================================================================
    override void DrawText(WL_Canvas canvas, string text, WL_Font fnt, WL_Rectangle textBounds)
    {
        if (text.Length() == 0)
        {
            return;
        }

        double x = textBounds.GetLeft();
        double y = textBounds.GetTop();
        let scale = GetScale();

        if (_bl != null)
        {
            double height = fnt.GetHeight();

            for (int i = 0; i < _bl.Count(); i++)
            {
                canvas.DrawString(_bl.StringAt(i), fnt, (x, y + _lineOffsets[i] * scale.Y), GetColor(), scale);
                y += _lineHeights[i];
            }
        }
        else
        {
            canvas.DrawString(text, fnt, (x, y + _lineOffsets[0] * scale.Y), GetColor(), scale);
        }
    }
}
