//===========================================================================
//
// ViewGroup
//
// Base class for widgets used to group other widgets.
//
//===========================================================================
class WL_ViewGroup : WL_View abstract
{
    private Array<WL_View> _children;
    private int _version;

    //===========================================================================
    //
    // ViewGroup::AddView
    //
    // Adds a new view to this group.
    //
    //===========================================================================
    void AddView(WL_View view)
    {
        if (view == null)
        {
            ThrowAbortException("%s: Attempt to add a null View to this ViewGroup.", GetClassName());
        }

        _children.Push(view);
        _version++;
        view.SetParent(self);
    }

    //===========================================================================
    //
    // ViewGroup::RemoveView
    //
    // Removes a view from this group.
    //
    //===========================================================================
    bool RemoveView(WL_View view)
    {
        bool result = false;

        for (int i = 0; i < GetCount(); i++)
        {
            if (GetViewAt(i) == view)
            {
                RemoveViewAt(i);
                _version++;

                result = true;
                break;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // ViewGroup::RemoveViewAt
    //
    // Removes a view at the specified index and returns it.
    //
    //===========================================================================
    WL_View RemoveViewAt(int index)
    {
        let result = _children[index];
        result.ClearParent();

        _children.Delete(index);
        _version++;

        return result;
    }

    //===========================================================================
    //
    // ViewGroup::Clear
    //
    // Removes all child views from this group.
    //
    //===========================================================================
    void Clear()
    {
        if (GetCount() == 0)
        {
            return;
        }

        foreach (child : _children)
        {
            child.ClearParent();
        }

        _children.Clear();
        _version++;
    }

    //===========================================================================
    //
    // ViewGroup::GetViewAt
    //
    // Returns a child view with the specified index.
    //
    //===========================================================================
    WL_View GetViewAt(int index) const
    {
        return _children[index];
    }

    //===========================================================================
    //
    // ViewGroup::SetViewAt
    //
    // Sets the child view at the specified index.
    //
    //===========================================================================
    void SetViewAt(int index, WL_View view)
    {
        if (view == null)
        {
            ThrowAbortException("%s: Attempt to add a null View to this ViewGroup.", GetClassName());
        }

        let oldView = _children[index];

        // Check that we are not replacing a view with itself
        if (oldView == view)
        {
            return;
        }

        oldView.ClearParent();
        view.SetParent(self);

        _children[index] = view;
        _version++;
    }

    //===========================================================================
    //
    // ViewGroup::GetCount
    //
    // Returns the number of child views in this view group.
    //
    //===========================================================================
    int GetCount() const
    {
        return _children.Size();
    }

    //===========================================================================
    //
    // ViewGroup::GetVersion
    //
    // Returns a number indicating the current version of the collection.
    // This will change each time a view is added, changed, or deleted.
    // This method is used by ViewIterator to detect if the collection changes
    // in the middle of an iteration, which should invalidate the iterator.
    //
    //===========================================================================
    int GetVersion() const
    {
        return _version;
    }

    //===========================================================================
    //
    // ViewGroup::GetAllChildren
    //
    // Returns a ViewIterator that iterates over all of this group's children.
    //
    //===========================================================================
    void GetAllChildren(out WL_ViewIterator result) const
    {
        result.Create(self);
    }

    //===========================================================================
    //
    // ViewGroup::GetPresentChildren
    //
    // Returns a ViewIterator that iterates over all of the present children
    // of this group (visibility != V_Gone).
    //
    //===========================================================================
    void GetPresentChildren(out WL_ViewIterator result) const
    {
        result.Create(self, WL_View.IsPresent);
    }

    //===========================================================================
    //
    // ViewGroup::GetVisibleChildren
    //
    // Returns a ViewIterator that iterates over all visible children
    // of this group.
    //
    //===========================================================================
    void GetVisibleChildren(out WL_ViewIterator result) const
    {
        result.Create(self, WL_View.IsVisible);
    }

    //===========================================================================
    //
    // ViewGroup::OnResolveSize
    //
    // Calls PreResolveSize for all child views.
    //
    //===========================================================================
    override void OnResolveSize()
    {
        WL_ViewIterator it;
        GetAllChildren(it);

        while (it.MoveNext())
        {
            it.GetView().PreResolveSize();
        }
    }

    //===========================================================================
    //
    // ViewGroup::OnPostResolveSize
    //
    // Calls PostResolveSize for all child views.
    //
    //===========================================================================
    override void OnPostResolveSize(bool animationEnabled)
    {
        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            it.GetView().PostResolveSize(animationEnabled);
        }
    }

    //===========================================================================
    //
    // ViewGroup::OnPostPosition
    //
    // Calls PostPosition for all child views.
    //
    //===========================================================================
    override void OnPostPosition()
    {
        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            it.GetView().PostPosition();
        }
    }

    //===========================================================================
    //
    // ViewGroup::Draw
    //
    // Draws all children of this ViewGroup at their resolved positions.
    //
    //===========================================================================
    override void Draw(WL_Canvas canvas)
    {
        WL_ViewIterator it;
        GetVisibleChildren(it);

        while (it.MoveNext())
        {
            it.GetView().DrawOn(canvas);
        }
    }
}
