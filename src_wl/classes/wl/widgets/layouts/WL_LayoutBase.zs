//===========================================================================
//
// LayoutBase
//
// Base class for layouts.
//
//===========================================================================
class WL_LayoutBase : WL_ViewGroup abstract
{
    private WL_LayoutParams _defaultLayoutParams;

    //===========================================================================
    //
    // LayoutBase::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _defaultLayoutParams = CreateDefaultLayoutParams();
    }

    //===========================================================================
    //
    // LayoutBase::CreateDefaultLayoutParams
    //
    // Creates the default layout parameters for this type of layout.
    //
    //===========================================================================
    protected abstract WL_LayoutParams CreateDefaultLayoutParams() const;

    //===========================================================================
    //
    // LayoutBase::GetLayoutParamsOrDefault
    //
    // Returns the layout parameters for the view, checking if they match
    // the type of the default parameters.
    //
    //===========================================================================
    protected WL_LayoutParams GetLayoutParamsOrDefault(WL_View view)
    {
        let def = _defaultLayoutParams;

        if (def == null)
        {
            ThrowAbortException("%s: Default layout parameters not set.", GetClassName());
        }

        let lp = view.GetLayoutParams();

        if (!(lp is def.GetClass()))
        {
            lp = def;
        }

        return lp;
    }
}
