//===========================================================================
//
// TableLayout
//
// The TableLayout is similar to a LinearLayout, but it should be used
// to group several TableRows instead of individual views.
// Individual views are also supported, but if you do not need any
// table functionality, LinearLayout should be used instead.
//
// All TableRows within the TableLayout will have their column lengths
// synchronized with each other. Columns can also have weights, like
// in GridLayout. But unlike in GridLayout, there is no pre-defined number
// of columns: it is computed automatically from the number of children
// in each TableRow and from their parameters.
//
// TableLayout should be used when each column houses elements of different
// nature, and every row represents a whole item or other kind of object.
// If each column contains elements representing the same type of item,
// GridLayout should be used instead.
//
//===========================================================================
class WL_TableLayout : WL_GridLayoutBase
{
    //===========================================================================
    //
    // TableLayout::Create
    //
    // Creates a new instance of the TableLayout class.
    //
    //===========================================================================
    static WL_TableLayout Create()
    {
        let v = new('WL_TableLayout');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // TableLayout::MarkRowContentChanged
    //
    // Mark all rows' content as changed.
    // This is called by one of the children rows when a single row's content
    // has changed because other rows might need to be recalculated
    // even if the total size is still the same.
    //
    //===========================================================================
    void MarkRowsContentChanged(WL_TableRow originator)
    {
        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();

            if (v is 'WL_TableRow' && v != originator)
            {
                v.CheckContentChanged(false/*<DebugOnly>*/, "content change of sibling TableRow"/*</DebugOnly>*/);
            }
        }
    }

    //===========================================================================
    //
    // TableLayout::ResolveContentSize
    //
    // Resolves the size of this TableLayout's rows and other elements.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        let ma = GetMainAxis();
        let ca = ma.GetCross();

        Array<double> columnLengths;
        int mc = GetMinColumnCount();
        int lc = 0;
        double maxLength = ma.GetLength(maxSize);
        double sumRow = 0;

        WL_ViewIterator it;
        GetPresentChildren(it);

        // Step 1: Resolve all non-weighted columns of all rows
        while (it.MoveNext())
        {
            let row = WL_TableRow(it.GetView());

            if (row == null)
            {
                continue;
            }

            row.SetSpecifiedGravity(GetGravity());
            row.SetOrientation(ca.GetOrientation());
            row.SetReversed(GetColumnsReversed());
            row.ClearData();

            for (int i = 0; i < Max(mc, lc); i++)
            {
                row.SetColumnWeight(i, GetColumnWeight(i));

                if (i < lc)
                {
                    row.SetColumnLength(i, columnLengths[i]);
                }
            }

            // Compute preliminary length of row
            WL_Size testSize;
            maxSize.SetAxisLength(ma, maxLength - sumRow, testSize);

            row.ResolveNonWeightedColumns(testSize);
            sumRow += row.GetCurDepth();

            for (int i = 0; i < row.GetColumnCount(); i++)
            {
                double colLength = row.GetColumnLength(i);

                if (i < lc)
                {
                    columnLengths[i] = colLength;
                }
                else
                {
                    columnLengths.Push(colLength);
                    lc++;
                }
            }
        }

        double maxDepth = ca.GetLength(maxSize);
        double curDepth = 0;

        GetPresentChildren(it);

        // Step 2: Resolve all weighted columns and thus the total size
        while (it.MoveNext())
        {
            let v = it.GetView();

            let row = WL_TableRow(v);

            if (row != null)
            {
                // Equalize column lengths
                for (int i = 0; i < lc; i++)
                {
                    row.SetColumnLength(i, columnLengths[i]);
                }

                // Compute real length and depth of row
                double prevLength = row.GetCurDepth();

                WL_Size testSize;
                testSize.Zero();
                testSize.SetAxisLength(ma, prevLength + maxLength - sumRow);
                testSize.SetAxisLength(ca, maxDepth);

                WL_Size resolved;
                row.ResolveSize(testSize, resolved);

                // If the resulting length is larger than previous,
                // add to it.
                sumRow += Max(0, ma.GetLength(resolved) - prevLength);
                curDepth = Max(curDepth, ca.GetLength(resolved));
            }
            else
            {
                // Non-row views are resolved as in FrameLayout.

                let m = GetLayoutParams(v).GetMargins();
                WL_Size testSize;
                testSize.Zero();
                testSize.SetAxisLength(ma, maxLength - sumRow);
                testSize.SetAxisLength(ca, maxDepth);
                testSize.Pad(m);

                WL_Size resolved;
                v.ResolveSize(testSize, resolved);
                resolved.Unpad(m);
                resolved.ClipBy(testSize);

                sumRow += ma.GetLength(resolved);
                curDepth = Max(curDepth, ca.GetLength(resolved));
            }
        }

        result.Zero();
        result.SetAxisLength(ma, sumRow);
        result.SetAxisLength(ca, curDepth);
    }

    //===========================================================================
    //
    // TableLayout::OnPositionNoCache
    //
    // Positions the children of this layout.
    //
    //===========================================================================
    override void OnPositionNoCache(WL_Rectangle bounds)
    {
        let ma = GetMainAxis();

        double curPos = 0;
        double boundPos = ma.GetRectPos(bounds);
        double boundLength = ma.GetRectLength(bounds);
        bool reversed = IsReversed();

        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            let lp = GetLayoutParams(v);

            bool isRow = v is 'WL_TableRow';
            WL_Size sz;
            v.GetResolvedSize(sz);

            // Rows don't have margins and gravity, but other elements do
            double cellLength = isRow
                ? ma.GetLength(sz)
                : ma.Unpad(sz, GetLayoutParams(v).GetMargins());

            double pos = reversed
                ? boundLength - curPos - cellLength
                : curPos;

            WL_Rectangle cell;
            ma.RepositionAndResize(bounds, pos + boundPos, cellLength, cell);

            if (isRow)
            {
                v.Position(cell.GetTopLeft());
            }
            else
            {
                PositionChildView(v, cell);
            }

            curPos += cellLength;
        }
    }
}
