//===========================================================================
//
// LinearLayout
//
// The LinearLayout is a view group that draws its children in a horizontal
// or vertical stack. Orientation can be specified with SetOrientation,
// and the order the children are placed in can also be reversed if needed.
//
// Each child view of the LinearLayout forms a cell around it. The view can
// have margins or gravity relative to its cell, just like in FrameLayout.
// The gravity of the LinearLayout itself dictates the default gravity
// of all its children, which can also be overridden individually.
//
// Each view can also have a weight (default 0), which means that the length
// of its cell (in terms of orientation) relates to the total length
// of all weighted cells in the same way its weight relates to the sum
// of all weights in the layout. A weighted view with the size of MatchParent
// will take the entirety of the cell along the LinearLayout's orientation.
//
// Note that if there is at least one child view with non-zero weight,
// the layout's total length (in terms of orientation) will always be equal
// to its parent's length, even if the corresponding dimension is set
// to WrapContent. The length of weighted cells is computed only after
// the length of all non-weighted cells has been resolved, which means that
// all non-weighted views will take as much space as they need to,
// and the rest will be proportionally distributed.
//
//===========================================================================
class WL_LinearLayout : WL_OrientedLayout
{
    private double _unitLength;

    //===========================================================================
    //
    // LinearLayout::Create
    //
    // Creates a new instance of the LinearLayout class.
    //
    //===========================================================================
    static WL_LinearLayout Create()
    {
        let v = new('WL_LinearLayout');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // LinearLayout::CreateParams
    //
    // Creates an instance of LinearLayoutParams with the specified values.
    //
    //===========================================================================
    static WL_LinearLayoutParams CreateParams(WL_Margins margins = null, WL_Gravity gravity = null, int weight = 0)
    {
        let lp = WL_LinearLayoutParams.Create();

        if (margins != null)
        {
            lp.SetMargins(margins);
        }

        if (gravity != null)
        {
            lp.SetGravity(gravity);
        }

        lp.SetWeight(weight);

        return lp;
    }

    //===========================================================================
    //
    // LinearLayout::CreateDefaultLayoutParams
    //
    // Creates the default instance of the LinearLayoutParams class.
    //
    //===========================================================================
    override WL_LayoutParams CreateDefaultLayoutParams() const
    {
        return WL_LinearLayoutParams.Create();
    }

    //===========================================================================
    //
    // LinearLayout::GetLayoutParams
    //
    // Returns the layout parameters of the specified view.
    //
    //===========================================================================
    protected WL_LinearLayoutParams GetLayoutParams(WL_View view)
    {
        return WL_LinearLayoutParams(GetLayoutParamsOrDefault(view));
    }

    //===========================================================================
    //
    // LinearLayout::ResolveContentSize
    //
    // Resolves the size of the view's content.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        let ma = GetMainAxis();
        let ca = ma.GetCross();

        double maxLength = ma.GetLength(maxSize);
        double sumLength = 0;
        double curDepth = 0;

        int totalWeight = 0;

        WL_ViewIterator it;
        GetPresentChildren(it);

        // First, resolve size of non-weighted views
        while (it.MoveNext())
        {
            let v = it.GetView();
            let lp = GetLayoutParams(v);
            int weight = lp.GetWeight();

            if (weight > 0)
            {
                totalWeight += weight;
                continue;
            }

            let m = lp.GetMargins();

            WL_Size testSize;
            maxSize.SetAxisLength(ma, maxLength - sumLength, testSize);
            testSize.Pad(m);

            WL_Size resolved;
            v.ResolveSize(testSize, resolved);
            resolved.Unpad(m);
            resolved.ClipBy(testSize);

            sumLength += ma.GetLength(resolved);
            curDepth = Max(curDepth, ca.GetLength(resolved));
        }

        // Now, resolve length of weighted views
        if (totalWeight > 0)
        {
            _unitLength = (maxLength - sumLength) / totalWeight;
            sumLength = maxLength;

            GetPresentChildren(it);

            while (it.MoveNext())
            {
                let v = it.GetView();
                let lp = GetLayoutParams(v);
                int weight = lp.GetWeight();

                if (weight == 0)
                {
                    continue;
                }

                let m = lp.GetMargins();

                WL_Size testSize;
                maxSize.SetAxisLength(ma, _unitLength * weight, testSize);
                testSize.Pad(m);

                WL_Size resolved;
                v.ResolveSize(testSize, resolved);
                resolved.Unpad(m);
                resolved.ClipBy(testSize);

                curDepth = Max(curDepth, ca.GetLength(resolved));
            }
        }

        result.Zero();
        result.SetAxisLength(ma, sumLength);
        result.SetAxisLength(ca, curDepth);
    }

    //===========================================================================
    //
    // LinearLayout::OnPositionNoCache
    //
    // Positions the children of this layout.
    //
    //===========================================================================
    override void OnPositionNoCache(WL_Rectangle bounds)
    {
        let ma = GetMainAxis();

        double curPos = 0;
        double boundPos = ma.GetRectPos(bounds);
        double boundLength = ma.GetRectLength(bounds);
        bool reversed = IsReversed();

        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            let lp = GetLayoutParams(v);

            int weight = lp.GetWeight();
            double cellLength;

            if (weight > 0)
            {
                cellLength = _unitLength * weight;
            }
            else
            {
                let m = lp.GetMargins();
                WL_Size sz;
                v.GetResolvedSize(sz);

                cellLength = ma.Unpad(sz, m);
            }

            double pos = reversed
                ? boundLength - curPos - cellLength
                : curPos;

            WL_Rectangle cell;
            ma.RepositionAndResize(bounds, pos + boundPos, cellLength, cell);
            PositionChildView(v, cell);

            curPos += cellLength;
        }
    }
}
