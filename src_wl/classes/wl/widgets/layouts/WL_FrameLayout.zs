//===========================================================================
//
// FrameLayout
//
// The FrameLayout is a view group that draws all its children on top
// of each other, in the order they were added.
//
// The size of the FrameLayout, when set to WrapContent, is equal to the
// size of its largest child, minus its margins.
//
// Along with margins, children of the FrameLayout can be anchored
// to the sides, corners, or the center of the layout by changing gravity.
// The gravity of the FrameLayout itself dictates the default gravity
// of its children, but any child view can override it by setting its own
// gravity.
//
//===========================================================================
class WL_FrameLayout : WL_LayoutBase
{
    private WL_Rectangle _lastBounds;
    private bool _childrenPositionsChanged;

    //===========================================================================
    //
    // FrameLayout::Create
    //
    // Creates a new instance of the FrameLayout class.
    //
    //===========================================================================
    static WL_FrameLayout Create()
    {
        let v = new('WL_FrameLayout');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // FrameLayout::CreateParams
    //
    // Creates an instance of FrameLayoutParams with the specified values.
    //
    //===========================================================================
    static WL_FrameLayoutParams CreateParams(WL_Margins margins = null, WL_Gravity gravity = null)
    {
        let lp = WL_FrameLayoutParams.Create();

        if (margins != null)
        {
            lp.SetMargins(margins);
        }

        if (gravity != null)
        {
            lp.SetGravity(gravity);
        }

        return lp;
    }

    //===========================================================================
    //
    // FrameLayout::CreateDefaultLayoutParams
    //
    // Creates the default instance of the FrameLayoutParams class.
    //
    //===========================================================================
    override WL_LayoutParams CreateDefaultLayoutParams() const
    {
        return WL_FrameLayoutParams.Create();
    }

    //===========================================================================
    //
    // FrameLayout::GetLayoutParams
    //
    // Returns the layout parameters of the specified view.
    //
    //===========================================================================
    protected WL_FrameLayoutParams GetLayoutParams(WL_View view)
    {
        return WL_FrameLayoutParams(GetLayoutParamsOrDefault(view));
    }

    //===========================================================================
    //
    // FrameLayout::OnGravityChanged
    //
    // Sets the flag to reposition the children.
    //
    //===========================================================================
    override void OnGravityChanged()
    {
        _childrenPositionsChanged = true;
    }

    //===========================================================================
    //
    // FrameLayout::OnContentChanged
    //
    // Sets the flag to reposition the children.
    //
    //===========================================================================
    override void OnContentChanged()
    {
        _childrenPositionsChanged = true;
    }

    //===========================================================================
    //
    // FrameLayout::DrawChildView
    //
    // Positions the specified view in the provided cell according to its margins
    // and gravity, then draws it on the provided canvas.
    //
    //===========================================================================
    protected void PositionChildView(WL_View v, WL_Rectangle cell)
    {
        let lp = GetLayoutParams(v);

        WL_Rectangle outerBounds;
        cell.Pad(lp.GetMargins(), outerBounds);

        WL_Size sz;
        v.GetResolvedSize(sz);

        WL_Rectangle viewBounds;
        outerBounds.Resize(sz, viewBounds);
        viewBounds.ClipBy(outerBounds);
        viewBounds.GravitateSpecified(lp.GetGravity(), GetGravity(), outerBounds);

        v.Position(viewBounds.GetTopLeft());
    }

    //===========================================================================
    //
    // FrameLayout::ResolveContentSize
    //
    // Resolves the size of the view's content.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        result.Zero();

        WL_ViewIterator it;
        GetPresentChildren(it);

        bool moved = false;

        while (it.MoveNext())
        {
            moved = true;

            let v = it.GetView();
            let m = GetLayoutParams(v).GetMargins();

            WL_Size testSize;
            maxSize.Pad(m, testSize);

            WL_Size resolved;
            v.ResolveSize(testSize, resolved);
            resolved.Unpad(m);

            result.GetMax(result, resolved);
        }
    }

    //===========================================================================
    //
    // FrameLayout::OnPosition
    //
    // If repositioning is necessary, positions the children of this layout.
    //
    //===========================================================================
    final override void OnPosition(WL_Rectangle bounds)
    {
        if (!_childrenPositionsChanged && bounds.Equals(_lastBounds))
        {
            return;
        }

        OnPositionNoCache(bounds);

        _lastBounds.CreateFrom(bounds);
        _childrenPositionsChanged = false;
    }

    //===========================================================================
    //
    // FrameLayout::OnPositionNoCache
    //
    // Positions the children of this layout.
    //
    //===========================================================================
    protected virtual void OnPositionNoCache(WL_Rectangle bounds)
    {
        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            PositionChildView(it.GetView(), bounds);
        }
    }
}
