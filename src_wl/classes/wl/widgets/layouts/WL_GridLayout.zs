//===========================================================================
//
// GridLayout
//
// The GridLayout arranges its children in a grid that extends along
// the layout's orientation. The GridLayout has a predefined number
// of columns, and as many rows as needed. In horizontal orientation,
// the meanings of rows and columns are reversed, but they are still called
// the same to avoid confusion.
//
// Like in LinearLayout, each child view is placed in a cell,
// and its placement is affected by margins and gravity. A column
// can also have a weight, which works similarly to LinearLayout.
//
// Each child of the GridLayout can also have a column number explicitly
// assigned. This can be practical if there is more than one row,
// to leave a cell empty.
//
//===========================================================================
class WL_GridLayout : WL_GridLayoutBase
{
    private int _columnCount;

    private Array<double> _columnLengths;
    private Array<double> _rowLengths;

    //===========================================================================
    //
    // GridLayout::Create
    //
    // Creates a new instance of the GridLayout class.
    //
    //===========================================================================
    static WL_GridLayout Create()
    {
        let v = new('WL_GridLayout');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // GridLayout::CreateParams
    //
    // Creates an instance of GridLayoutParams with the specified values.
    //
    //===========================================================================
    static WL_GridLayoutParams CreateParams(WL_Margins margins = null, WL_Gravity gravity = null, int columnNumber = -1)
    {
        let lp = WL_GridLayoutParams.Create();

        if (margins != null)
        {
            lp.SetMargins(margins);
        }

        if (gravity != null)
        {
            lp.SetGravity(gravity);
        }

        lp.SetColumnNumber(columnNumber);

        return lp;
    }

    //===========================================================================
    //
    // GridLayout::CreateDefaultLayoutParams
    //
    // Creates the default instance of the GridLayoutParams class.
    //
    //===========================================================================
    override WL_LayoutParams CreateDefaultLayoutParams() const
    {
        return WL_GridLayoutParams.Create();
    }

    //===========================================================================
    //
    // GridLayout::GetLayoutParams
    //
    // Returns the layout parameters of the specified view.
    //
    //===========================================================================
    protected WL_GridLayoutParams GetLayoutParams(WL_View view)
    {
        return WL_GridLayoutParams(GetLayoutParamsOrDefault(view));
    }

    //===========================================================================
    //
    // GridLayout::GetColumnNumber
    //
    // Returns the column number for the provided view, or -1
    // if the column number is not set explicitly.
    //
    //===========================================================================
    int GetViewColumnNumber(WL_View view)
    {
        return GetLayoutParams(view).GetColumnNumber();
    }

    //===========================================================================
    //
    // GridLayout::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _columnCount = 1;
    }

    //===========================================================================
    //
    // GridLayout::GetColumnCount
    //
    // Returns the column count for this layout.
    //
    //===========================================================================
    int GetColumnCount() const
    {
        return _columnCount;
    }

    //===========================================================================
    //
    // GridLayout::SetColumnCount
    //
    // Sets the column count for this layout.
    // The value must be positive.
    //
    //===========================================================================
    void SetColumnCount(int value)
    {
        if (value <= 0)
        {
            ThrowAbortException("%s: Column count must be positive.", GetClassName());
        }

        _columnCount = value;
    }

    //===========================================================================
    //
    // GridLayout::GetColumnWeight
    //
    // Gets the weight of the column with the provided index.
    //
    //===========================================================================
    override int GetColumnWeight(int column) const
    {
        if (column >= _columnCount)
        {
            ThrowAbortException("%s: Invalid column number %d. This layout has only %d column(s).", GetClassName());
        }

        return Super.GetColumnWeight(column);
    }

    //===========================================================================
    //
    // GridLayout::SetColumnWeight
    //
    // Sets the weight of the column with the provided index
    // to the specified value.
    //
    //===========================================================================
    override void SetColumnWeight(int column, int weight)
    {
        if (column >= _columnCount)
        {
            ThrowAbortException("%s: Invalid column number %d. This layout has only %d column(s).", GetClassName());
        }

        Super.SetColumnWeight(column, weight);
    }

    //===========================================================================
    //
    // GridLayout::GetResolvedRowLength
    // Returns the resolved length of the row with the provided index.
    //
    //===========================================================================
    double GetResolvedRowLength(int row) const
    {
        return _rowLengths[row];
    }

    //===========================================================================
    //
    // GridLayout::GetColumnNumber
    //
    // Returns the column number of a view, defaulting to the provided
    // current value if necessary, and also returns whether the column
    // is located in a new row (relative to the current column number).
    //
    //===========================================================================
    private int, bool GetNextColumnNumber(WL_GridLayoutParams lp, int current = 0)
    {
        int columnNumber = lp.GetColumnNumber();

        if (columnNumber >= _columnCount)
        {
            ThrowAbortException("%s: Invalid column number %d. This layout has only %d column(s).", GetClassName(), columnNumber, _columnCount);
        }

        int result = columnNumber >= 0 ? columnNumber : current % _columnCount;
        return result, result < current;
    }

    //===========================================================================
    //
    // GridLayout::ResolveContentSize
    //
    // Calculates the total content size of this GridLayout,
    // based on the provided maximum size.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        let ma = GetMainAxis();
        let ca = ma.GetCross();

        // Reset the length arrays
        _columnLengths.Clear();
        _rowLengths.Clear();

        for (int i = 0; i < _columnCount; i++)
        {
            _columnLengths.Push(0);
        }

        double maxCol = ca.GetLength(maxSize);
        double maxRow = ma.GetLength(maxSize);
        double sumCol = 0;
        double sumRow = 0;

        // Step 1: Resolve length of all non-weighted columns.
        int col = 0;
        double curRow = 0;

        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            let lp = GetLayoutParams(v);
            int nextCol;
            bool advanceRow;

            // Get column number and check if we need to advance the row
            [nextCol, advanceRow] = GetNextColumnNumber(lp, col);

            if (advanceRow)
            {
                col = 0;
                sumCol = 0;
                sumRow += curRow;

                _rowLengths.Push(curRow);
                curRow = 0;
            }

            // Add lengths of columns that we might have missed
            for (int i = col; i < nextCol; i++)
            {
                sumCol += _columnLengths[i];
            }

            col = nextCol;

            // Only process non-weighted columns at this time
            if (GetColumnWeight(col) == 0)
            {
                let m = lp.GetMargins();

                WL_Size testSize;
                testSize.Zero();
                testSize.SetAxisLength(ca, maxCol - sumCol);
                testSize.SetAxisLength(ma, maxRow - sumRow);
                testSize.Pad(m);

                WL_Size resolved;
                v.ResolveSize(testSize, resolved);
                resolved.Unpad(m);
                resolved.ClipBy(testSize);

                double curCol = Max(_columnLengths[col], ca.GetLength(resolved));

                _columnLengths[col] = curCol;
                sumCol += curCol;
                curRow = Max(curRow, ma.GetLength(resolved));
            }

            // Move on...
            col++;
        }

        // Finalize data for current row step 1
        sumRow += curRow;
        _rowLengths.Push(curRow);

        // Prepare data for next step
        int totalWeight = 0;
        sumCol = 0;

        for (int i = 0; i < _columnCount; i++)
        {
            int weight = GetColumnWeight(i);

            if (weight > 0)
            {
                totalWeight += GetColumnWeight(i);
            }
            else
            {
                sumCol += _columnLengths[i];
            }
        }

        // Step 2: Resolve length of weighted columns (if any).
        if (totalWeight > 0)
        {
            double unitLength = (maxCol - sumCol) / totalWeight;
            sumCol = maxCol;

            // Set length of weighted columns to draw them easily
            for (int i = 0; i < _columnCount; i++)
            {
                int weight = GetColumnWeight(i);

                if (weight > 0)
                {
                    _columnLengths[i] = unitLength * weight;
                }
            }

            col = 0;
            int row = 0;

            GetPresentChildren(it);

            while (it.MoveNext())
            {
                let v = it.GetView();
                let lp = GetLayoutParams(v);
                bool advanceRow;

                // Get column number and check if we need to advance the row
                [col, advanceRow] = GetNextColumnNumber(lp, col);

                if (advanceRow)
                {
                    row++;
                }

                int weight = GetColumnWeight(col);

                if (weight > 0)
                {
                    double rowLength = _rowLengths[row];

                    let m = lp.GetMargins();

                    WL_Size testSize;
                    testSize.Zero();
                    testSize.SetAxisLength(ca, _columnLengths[col]);
                    testSize.SetAxisLength(ma, rowLength + maxRow - sumRow);
                    testSize.Pad(m);

                    WL_Size resolved;
                    v.ResolveSize(testSize, resolved);
                    resolved.Unpad(m);
                    resolved.ClipBy(testSize);

                    // Adjust row length if necessary
                    double rowAdd = Max(0, ma.GetLength(resolved) - rowLength);

                    _rowLengths[row] += rowAdd;
                    sumRow += rowAdd;
                }

                col++;
            }
        }

        result.Zero();
        result.SetAxisLength(ca, sumCol);
        result.SetAxisLength(ma, sumRow);
    }

    //===========================================================================
    //
    // GridLayout::OnPositionNoCache
    //
    // Positions the children of this layout.
    //
    //===========================================================================
    override void OnPositionNoCache(WL_Rectangle bounds)
    {
        let ma = GetMainAxis();
        let ca = ma.GetCross();

        double colPos = 0;
        double rowPos = 0;

        double startCol = ca.GetRectPos(bounds);
        double startRow = ma.GetRectPos(bounds);
        double boundLengthCol = ca.GetRectLength(bounds);
        double boundLengthRow = ma.GetRectLength(bounds);

        bool reversedRows = IsReversed();
        bool reversedCols = GetColumnsReversed();

        int col = 0;
        int row = 0;

        WL_ViewIterator it;
        GetPresentChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            int nextCol;
            bool advanceRow;

            [nextCol, advanceRow] = GetNextColumnNumber(GetLayoutParams(v), col);

            if (advanceRow)
            {
                col = 0;
                colPos = 0;
                rowPos += _rowLengths[row];
                row++;
            }

            // Skip columns until the current column
            for (int i = col; i < nextCol; i++)
            {
                colPos += _columnLengths[i];
            }

            col = nextCol;

            double colLength = _columnLengths[col];
            double rowLength = _rowLengths[row];

            double colPosActual = reversedCols
                ? boundLengthCol - colPos - colLength
                : colPos;

            double rowPosActual = reversedRows
                ? boundLengthRow - rowPos - rowLength
                : rowPos;

            WL_Rectangle cell;
            cell.Zero();
            ca.RepositionAndResize(cell, startCol + colPosActual, colLength);
            ma.RepositionAndResize(cell, startRow + rowPosActual, rowLength);

            PositionChildView(v, cell);

            colPos += colLength;
            col++;
        }
    }
}
