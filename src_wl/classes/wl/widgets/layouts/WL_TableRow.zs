//===========================================================================
//
// TableRow
//
// A TableRow represents one row in TableLayout.
//
// TableRows cannot be contained in anything other than a TableLayout,
// and trying to put a TableRow in a different type of view group will result
// in an exception being thrown.
//
// Each child view of a TableRow has margins and gravity just like
// in FrameLayout. Do note that unless the gravity is explicitly overridden
// by children, the gravity that will take effect will come from the parent
// TableLayout, unless the TableRow specified its own gravity.
//
// It is not recommended to set TableRow's padding along the parent
// TableLayout's orientation, and the width of the row (or height, if
// the parent's orientation is horizontal) should be set to WrapContent
// to achieve the expected result.
//
//===========================================================================
class WL_TableRow : WL_OrientedLayout
{
    private Array<int> _columnWeights;
    private Array<double> _columnLengths;

    private double _curDepth;
    private bool _nonWeightedLengthResolved;
    private bool _isGravitySpecified;

    //===========================================================================
    //
    // TableRow::Create
    //
    // Creates a new instance of the TableRow class.
    //
    //===========================================================================
    static WL_TableRow Create()
    {
        let v = new('WL_TableRow');
        v.Init();

        return v;
    }

    //===========================================================================
    //
    // TableRow::CreateParams
    //
    // Creates an instance of TableRowParams with the specified values.
    //
    //===========================================================================
    static WL_TableRowParams CreateParams(WL_Margins margins = null, WL_Gravity gravity = null, int columnNumber = -1)
    {
        let lp = WL_TableRowParams.Create();

        if (margins != null)
        {
            lp.SetMargins(margins);
        }

        if (gravity != null)
        {
            lp.SetGravity(gravity);
        }

        lp.SetColumnNumber(columnNumber);

        return lp;
    }

    //===========================================================================
    //
    // TableRow::CreateDefaultLayoutParams
    //
    // Creates the default instance of the TableRowParams class.
    //
    //===========================================================================
    override WL_LayoutParams CreateDefaultLayoutParams() const
    {
        return WL_TableRowParams.Create();
    }

    //===========================================================================
    //
    // TableRow::GetLayoutParams
    //
    // Returns the layout parameters of the specified view.
    //
    //===========================================================================
    protected WL_TableRowParams GetLayoutParams(WL_View view)
    {
        return WL_TableRowParams(GetLayoutParamsOrDefault(view));
    }

    //===========================================================================
    //
    // TableRow::SetColumnLength
    //
    // Sets the minimum width of the specified column.
    //
    //===========================================================================
    void SetColumnLength(int column, double width)
    {
        for (int i = _columnLengths.Size(); i <= column; i++)
        {
            _columnLengths.Push(0);
        }

        _columnLengths[column] = width;
    }

    //===========================================================================
    //
    // TableRow::GetColumnLength
    //
    // Returns the minimum width of the specified column.
    //
    //===========================================================================
    double GetColumnLength(int column) const
    {
        return column < _columnLengths.Size()
            ? _columnLengths[column]
            : 0;
    }

    //===========================================================================
    //
    // TableRow::SetColumnWeight
    //
    // Sets the weight of the specified column.
    //
    //===========================================================================
    void SetColumnWeight(int column, int weight)
    {
        if (weight < 0)
        {
            ThrowAbortException("%s: Weight cannot be negative.", GetClassName());
        }

        for (int i = _columnWeights.Size(); i <= column; i++)
        {
            _columnWeights.Push(0);
        }

        _columnWeights[column] = weight;
    }

    //===========================================================================
    //
    // TableRow::GetColumnCount
    //
    // Returns the number of columns.
    //
    //===========================================================================
    int GetColumnCount() const
    {
        return Max(_columnLengths.Size(), _columnWeights.Size());
    }

    //===========================================================================
    //
    // TableRow::GetCurDepth
    //
    // Returns the partially resolved depth of this row.
    //
    //===========================================================================
    double GetCurDepth() const
    {
        return _curDepth;
    }

    //===========================================================================
    //
    // TableRow::ClearData
    //
    // Clears the column data (lengths / weights) and depth or this row.
    //
    //===========================================================================
    void ClearData()
    {
        _columnWeights.Clear();
        _columnLengths.Clear();
        _curDepth = 0;
        _nonWeightedLengthResolved = false;
    }

    //===========================================================================
    //
    // TableRow::SetSpecifiedGravity
    //
    // Specifies the gravity with the provided gravity, unless this has already
    // been done and the gravity hasn't changed since then.
    // This method is intended to be called from the parent TableLayout.
    //
    //===========================================================================
    void SetSpecifiedGravity(WL_Gravity gravity)
    {
        if (!_isGravitySpecified)
        {
            SetGravity(GetGravity().Specify(gravity));
            _isGravitySpecified = true;
        }
    }

    //===========================================================================
    //
    // TableRow::OnGravityChanged
    //
    // If the newly set gravity is not fully specified,
    // resets the flag to have SetSpecifiedGravity take effect during next call.
    //
    //===========================================================================
    override void OnGravityChanged()
    {
        Super.OnGravityChanged();
        _isGravitySpecified = GetGravity().IsSpecified();
    }

    //===========================================================================
    //
    // TableRow::OnContentChanged
    //
    // If this row is part of a TableLayout,
    // signals to it to mark all rows' content as changed.
    //
    //===========================================================================
    override void OnContentChanged()
    {
        Super.OnContentChanged();

        let parent = WL_TableLayout(GetParent());

        if (parent != null)
        {
            parent.MarkRowsContentChanged(self);
        }
    }

    //===========================================================================
    //
    // TableRow::GetColumnWeight
    //
    // Gets the weight of the specified column.
    //
    //===========================================================================
    private int GetColumnWeight(int column) const
    {
        return column < _columnWeights.Size()
            ? _columnWeights[column]
            : 0;
    }

    //===========================================================================
    //
    // TableRow::GetColumnNumber
    //
    // Gets the column number from LayoutParams, or -1 if not defined.
    //
    //===========================================================================
    private int GetColumnNumberOrDefault(WL_TableRowParams lp, int defaultValue) const
    {
        int result = lp.GetColumnNumber();

        if (result == -1)
        {
            result = defaultValue;
        }
        else if (result < defaultValue)
        {
            ThrowAbortException("%s: Column numbers must come in ascending order.", GetClassName());
        }

        return result;
    }

    //===========================================================================
    //
    // TableRow::ResolveNonWeightedColumns
    //
    // Resolves the length of non-weighted columns.
    //
    //===========================================================================
    void ResolveNonWeightedColumns(WL_Size maxSize)
    {
        let ma = GetMainAxis();
        let ca = ma.GetCross();

        double maxCol = ma.GetLength(maxSize);
        double sumCol = 0;
        int col = 0;

        WL_ViewIterator it;
        GetAllChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            let lp = GetLayoutParams(v);
            int nextCol = GetColumnNumberOrDefault(lp, col);

            // Add length of columns that we might have skipped
            for (int i = col; i < nextCol; i++)
            {
                sumCol += GetColumnLength(i);
            }

            col = nextCol;

            if (v.IsPresent() && GetColumnWeight(col) == 0)
            {
                // Resolve lengths only for non-weighted columns at this time
                let m = lp.GetMargins();

                WL_Size testSize;
                maxSize.SetAxisLength(ma, maxCol - sumCol, testSize);
                testSize.Pad(m);

                WL_Size resolved;
                v.ResolveSize(testSize, resolved);
                resolved.Unpad(m);
                resolved.ClipBy(testSize);

                double curCol = Max(GetColumnLength(col), ma.GetLength(resolved));

                SetColumnLength(col, curCol);
                sumCol += curCol;
                _curDepth = Max(_curDepth, ca.GetLength(resolved));
            }

            // Move on...
            col++;
        }

        _nonWeightedLengthResolved = true;
    }

    //===========================================================================
    //
    // TableRow::ResolveContentSize
    //
    // Resolves the size of this view's content. Should only be called after
    // ResolveColumnLengths, since it only resolved weighted columns.
    //
    //===========================================================================
    override void ResolveContentSize(WL_Size maxSize, out WL_Size result)
    {
        if (!_nonWeightedLengthResolved)
        {
            ThrowAbortException("%s: TableRow cannot be used outside of a TableLayout.", GetClassName());
        }

        let ma = GetMainAxis();
        let ca = ma.GetCross();

        double sumCol = 0;
        int totalWeight = 0;

        int n = GetColumnCount();

        // First, calculate total weight and column length.
        for (int i = 0; i < GetColumnCount(); i++)
        {
            int weight = GetColumnWeight(i);

            if (weight > 0)
            {
                totalWeight += weight;
            }
            else
            {
                sumCol += GetColumnLength(i);
            }
        }

        double maxCol = ma.GetLength(maxSize);
        double curDepth = _curDepth;

        // Resolve size of weighted columns (if any).
        if (totalWeight > 0)
        {
            double unitLength = (maxCol - sumCol) / totalWeight;
            sumCol = maxCol;

            // Set lengths for weighted columns to draw them easily
            for (int i = 0; i < GetColumnCount(); i++)
            {
                int weight = GetColumnWeight(i);

                if (weight > 0)
                {
                    SetColumnLength(i, unitLength * weight);
                }
            }

            WL_ViewIterator it;
            GetAllChildren(it);

            int col = 0;

            while (it.MoveNext())
            {
                let v = it.GetView();
                let lp = GetLayoutParams(v);

                col = GetColumnNumberOrDefault(lp, col);

                if (v.IsPresent() && GetColumnWeight(col) > 0)
                {
                    let m = lp.GetMargins();

                    WL_Size testSize;
                    maxSize.SetAxisLength(ma, GetColumnLength(col), testSize);
                    testSize.Pad(m);

                    WL_Size resolved;
                    v.ResolveSize(testSize, resolved);
                    resolved.Unpad(m);
                    resolved.ClipBy(testSize);

                    curDepth = Max(curDepth, ca.GetLength(resolved));
                }

                col++;
            }
        }

        result.Zero();
        result.SetAxisLength(ma, sumCol);
        result.SetAxisLength(ca, curDepth);
    }

    //===========================================================================
    //
    // TableRow::OnPositionNoCache
    //
    // Positions the children of this layout.
    //
    //===========================================================================
    override void OnPositionNoCache(WL_Rectangle bounds)
    {
        let ma = GetMainAxis();

        double curPos = 0;.
        double boundPos = ma.GetRectPos(bounds);
        double boundLength = ma.GetRectLength(bounds);
        bool reversed = IsReversed();

        int col = 0;

        WL_ViewIterator it;
        GetAllChildren(it);

        while (it.MoveNext())
        {
            let v = it.GetView();
            int nextCol = GetColumnNumberOrDefault(GetLayoutParams(v), col);

            // Skip cells until the current column
            for (int i = col; i < nextCol; i++)
            {
                curPos += GetColumnLength(i);
            }

            col = nextCol;
            double cellLength = GetColumnLength(col);

            if (v.IsPresent())
            {
                double pos = reversed
                    ? boundLength - curPos - cellLength
                    : curPos;

                WL_Rectangle cell;
                ma.RepositionAndResize(bounds, pos + boundPos, cellLength, cell);

                PositionChildView(v, cell);
            }

            curPos += cellLength;
            col++;
        }
    }
}
