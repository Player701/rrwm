//===========================================================================
//
// OrientedLayout
//
// Base class for layouts with orientation.
//
//===========================================================================
class WL_OrientedLayout : WL_FrameLayout abstract
{
    // Enable the orientation functionality
    mixin WL_OrientedView;

    // Enable reversibility
    mixin WL_ReversibleView;

    //===========================================================================
    //
    // OrientedLayout::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        SetOrientation(WL_O_Horizontal);
    }
}
