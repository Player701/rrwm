//===========================================================================
//
// GridLayoutBase
//
// Base class for layouts that display a grid or a table.
//
//===========================================================================
class WL_GridLayoutBase : WL_OrientedLayout abstract
{
    private bool _columnsReversed;
    private Array<int> _columnWeights;

    //===========================================================================
    //
    // GridLayoutBase::SetColumnsReversed
    //
    // Sets whether the columns of this layout are drawn in reverse order.
    //
    //===========================================================================
    void SetColumnsReversed(bool columnsReversed)
    {
        CheckContentChanged(columnsReversed == _columnsReversed/*<DebugOnly>*/, string.Format("columns reversed flag change, from %s to %s", _columnsReversed ? "true" : "false", columnsReversed ? "true" : "false")/*</DebugOnly>*/);
        _columnsReversed = columnsReversed;
    }

    //===========================================================================
    //
    // GridLayoutBase::GetColumnsReversed
    //
    // Gets whether the columns of this layout are drawn in reverse order.
    //
    //===========================================================================
    bool GetColumnsReversed() const
    {
        return _columnsReversed;
    }

    //===========================================================================
    //
    // GridLayoutBase::GetColumnWeight
    //
    // Gets the weight of the column with the provided index.
    //
    //===========================================================================
    virtual int GetColumnWeight(int column) const
    {
        return column < _columnWeights.Size()
            ? _columnWeights[column]
            : 0;
    }

    //===========================================================================
    //
    // GridLayoutBase::SetColumnWeight
    //
    // Sets the weight of the column with the provided index
    // to the specified value.
    //
    //===========================================================================
    virtual void SetColumnWeight(int column, int weight)
    {
        if (weight < 0)
        {
            ThrowAbortException("%s: Weight cannot be negative.", GetClassName());
        }

        for (int i = _columnWeights.Size(); i <= column; i++)
        {
            _columnWeights.Push(0);
        }

        CheckContentChanged(weight == _columnWeights[column]/*<DebugOnly>*/, string.Format("column %d weight change, from %d to %d", column, _columnWeights[column], weight)/*</DebugOnly>*/);
        _columnWeights[column] = weight;
    }

    //===========================================================================
    //
    // GridLayoutBase::GetMinColumnCount
    //
    // Returns the minimum number of columns based on the weight array.
    //
    //===========================================================================
    protected int GetMinColumnCount() const
    {
        return _columnWeights.Size();
    }
}
