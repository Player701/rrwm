//===========================================================================
//
// LayoutParams
//
// Base class for widget parameters used to control their behavior
// inside a parent layout.
//
//===========================================================================
class WL_LayoutParams abstract ui
{
    private WL_Gravity _gravity;

    // Used for tracking views that have this LayoutParams assigned to them.
    private Array<WL_View> _subscribedViews;

    //===========================================================================
    //
    // LayoutParams::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        _gravity = WL_Gravity.Unspecified();
    }

    //===========================================================================
    //
    // LayoutParams::SubscribeView
    //
    // Adds the provided view to the subscriber list.
    // Used to pass change events to the views that have this LayoutParams
    // assigned to them.
    //
    //===========================================================================
    void SubscribeView(WL_View view)
    {
        if (view == null)
        {
            ThrowAbortException("%s: Subscribed view cannot be null.", GetClassName());
        }

        _subscribedViews.Push(view);
    }

    //===========================================================================
    //
    // LayoutParams::UnsubscribeView
    //
    // Unsubscribes the provided view from the subscriber list.
    //
    //===========================================================================
    void UnsubscribeView(WL_View view)
    {
        if (view == null)
        {
            ThrowAbortException("%s: Unsubscribed view cannot be null.", GetClassName());
        }

        for (int i = 0; i < _subscribedViews.Size(); i++)
        {
            if (_subscribedViews[i] == view)
            {
                _subscribedViews.Delete(i);
                return;
            }
        }

        ThrowAbortException("%s: Cannot find subscribed view in the list.", GetClassName());
    }

    //===========================================================================
    //
    // LayoutParams::CheckChanged
    //
    // Marks all subscribed views' parent content as changed
    // if the provided value is false.
    //
    //===========================================================================
    protected void CheckChanged(bool equals)
    {
        if (!equals)
        {
            foreach (subscriber : _subscribedViews)
            {
                subscriber.MarkParentContentChanged(/*<DebugOnly>*/"layout params change"/*</DebugOnly>*/);
            }
        }
    }

    //===========================================================================
    //
    // LayoutParams::GetGravity
    //
    // Returns the gravity.
    //
    //===========================================================================
    WL_Gravity GetGravity() const
    {
        return _gravity;
    }

    //===========================================================================
    //
    // LayoutParams::SetGravity
    //
    // Sets the gravity.
    //
    //===========================================================================
    void SetGravity(WL_Gravity gravity)
    {
        if (gravity == null)
        {
            ThrowAbortException("%s: Gravity cannot be null.", GetClassName());
        }

        CheckChanged(gravity.Equals(_gravity));
        _gravity = gravity;
    }
}
