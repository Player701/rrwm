//===========================================================================
//
// FrameLayoutParams
//
// Stores a view's parameters relevant to a parent FrameLayout.
//
//===========================================================================
class WL_FrameLayoutParams : WL_LayoutParams
{
    private WL_Margins _margins;

    //===========================================================================
    //
    // FrameLayoutParams::Create
    //
    // Creates a new instance of the FrameLayoutParams class.
    //
    //===========================================================================
    static WL_FrameLayoutParams Create()
    {
        let lp = new('WL_FrameLayoutParams');
        lp.Init();

        return lp;
    }

    //===========================================================================
    //
    // FrameLayoutParams::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _margins = WL_Margins.Zero();
    }

    //===========================================================================
    //
    // FrameLayoutParams::GetMargins
    //
    // Returns the margin values.
    //
    //===========================================================================
    WL_Margins GetMargins() const
    {
        return _margins;
    }

    //===========================================================================
    //
    // FrameLayoutParams::SetMargins
    //
    // Sets the margin values.
    //
    //===========================================================================
    void SetMargins(WL_Margins margins)
    {
        if (margins == null)
        {
            ThrowAbortException("%s: Margins cannot be null.", GetClassName());
        }

        CheckChanged(margins.Equals(_margins));
        _margins = margins;
    }
}
