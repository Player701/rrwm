//===========================================================================
//
// TableRowParams
//
// Stores a view's parameters relevant to a parent TableRow.
//
//===========================================================================
class WL_TableRowParams : WL_FrameLayoutParams
{
    private int _columnNumber;

    //===========================================================================
    //
    // TableRowParams::Create
    //
    // Creates a new instance of the TableRowParams class.
    //
    //===========================================================================
    static WL_TableRowParams Create()
    {
        let lp = new('WL_TableRowParams');
        lp.Init();

        return lp;
    }

    //===========================================================================
    //
    // TableRowParams::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _columnNumber = -1;
    }

    //===========================================================================
    //
    // TableRowParams::GetColumnNumber
    //
    // Gets the column number for this view.
    // -1 means automatic placement.
    //
    //===========================================================================
    int GetColumnNumber()
    {
        return _columnNumber;
    }

    //===========================================================================
    //
    // TableRowParams::SetColumnNumber
    //
    // Sets the column number for this view.
    // -1 means automatic placement.
    //
    //===========================================================================
    void SetColumnNumber(int columnNumber)
    {
        if (columnNumber < -1)
        {
            ThrowAbortException("%s: Invalid column number. Value must be non-negative or -1 (for automatic view placement).", GetClassName());
        }

        CheckChanged(columnNumber == _columnNumber);
        _columnNumber = columnNumber;
    }
}
