//===========================================================================
//
// LinearLayoutParams
//
// Stores a view's parameters relevant to a parent LinearLayout.
//
//===========================================================================
class WL_LinearLayoutParams : WL_FrameLayoutParams
{
    private int _weight;

    //===========================================================================
    //
    // LinearLayoutParams::Create
    //
    // Creates a new instance of the LinearLayoutParams class.
    //
    //===========================================================================
    static WL_LinearLayoutParams Create()
    {
        let lp = new('WL_LinearLayoutParams');
        lp.Init();

        return lp;
    }

    //===========================================================================
    //
    // LinearLayoutParams::GetWeight
    //
    // Returns the weight.
    //
    //===========================================================================
    int GetWeight()
    {
        return _weight;
    }

    //===========================================================================
    //
    // LinearLayoutParams::SetWeight
    //
    // Sets the weight.
    //
    //===========================================================================
    void SetWeight(int weight)
    {
        if (weight < 0)
        {
            ThrowAbortException("%s: Weight cannot be negative.", GetClassName());
        }

        CheckChanged(weight == _weight);
        _weight = weight;
    }
}
