//===========================================================================
//
// GridLayoutParams
//
// Stores a view's parameters relevant to a parent GridLayout.
//
//===========================================================================
class WL_GridLayoutParams : WL_FrameLayoutParams
{
    private int _columnNumber;

    //===========================================================================
    //
    // GridLayoutParams::Create
    //
    // Creates a new instance of the GridLayoutParams class.
    //
    //===========================================================================
    static WL_GridLayoutParams Create()
    {
        let lp = new('WL_GridLayoutParams');
        lp.Init();

        return lp;
    }

    //===========================================================================
    //
    // GridLayoutParams::Init
    //
    // Performs first-time initialization.
    //
    //===========================================================================
    protected void Init()
    {
        Super.Init();
        _columnNumber = -1;
    }

    //===========================================================================
    //
    // GridLayoutParams::GetColumnNumber
    //
    // Gets the column number for this view.
    // -1 means automatic placement.
    //
    //===========================================================================
    int GetColumnNumber()
    {
        return _columnNumber;
    }

    //===========================================================================
    //
    // GridLayoutParams::SetColumnNumber
    //
    // Sets the column number for this view.
    // -1 means automatic placement.
    //
    //===========================================================================
    void SetColumnNumber(int columnNumber)
    {
        if (columnNumber < -1)
        {
            ThrowAbortException("%s: Invalid column number. Value must be non-negative or -1 (for automatic view placement).", GetClassName());
        }

        CheckChanged(columnNumber == _columnNumber);
        _columnNumber = columnNumber;
    }
}
