# RRWM credits list

This file contains a list of all third-party assets used in RRWM, grouped by their type, their original author, and the work the asset originates from.
To use the resources made by Doom community authors in your own work, please try to contact their authors for permissions. Some of these people haven't replied to my requests for permissions, so unless they answer in the future, it is assumed that they don't mind their resources being used in the mod as long as they're given credit for it.

## The Authors
[//]: # (<ExcludeFromHudPackage>)

* [Xaser](https://forum.zdoom.org/memberlist.php?mode=viewprofile&u=78)
* [Zero Prophet](mailto:zeroprophet@gmail.com)
* eliw00d
* Cory Whittle
* Pawel "nmn" Zarcynski
* [WildWeasel](https://forum.zdoom.org/memberlist.php?mode=viewprofile&u=31)
[//]: # (</ExcludeFromHudPackage>)
* [Nash Muhandes](https://forum.zdoom.org/memberlist.php?mode=viewprofile&u=267)
[//]: # (<ExcludeFromHudPackage>)
* [doomplayer](https://forum.zdoom.org/memberlist.php?mode=viewprofile&u=30164)
* GampiSora
* ...and, of course, the mysterious Unknown Artist (or probably a group of artists)
[//]: # (</ExcludeFromHudPackage>)

## Resource list -- Doom community
[//]: # (<ExcludeFromHudPackage>)

Xaser -- Zen Dynamics:

* Sprites: effects/debris/emptyclips/chaingun/*
* Sprites: effects/debris/emptyclips/rocketlauncher/*
* Sprites: pickups/ammo/rocket/*
* Sprites: projectiles/rocket/*
* Sprites: weapons/chaingun/*
* Sprites: weapons/plasmarifle/*
* Sprites: weapons/rocketlauncher/* (additional edits of STRF\*.png by doomplayer)
* Sprites: weapons/supershotgun/*
* Sounds: weapons/rocketlauncher/* (except DSSCLK.wav; modified)

Zero Prophet -- Zero Tolerance:

* Sprites: effects/bfgextra/*
* Sprites: projectiles/bfgball/*
* Sprites: weapons/bfg9000/*
* Sprites: weapons/chainsaw/* (except ASAW\*.png)
* Sprites: weapons/pistol/*

eliw00d -- ZDoom Advanced Mod v. 0.72h:

* Sprites: effects/debris/casings/*
* Sprites: effects/smoke/*
* Sprites: monsters/*
* Sounds: effects/*
* Sounds: weapons/rocketlauncher/DSSCLK.wav

Cory Whittle -- Immoral Conduct:

* Sprites: effects/debris/emptyclips/pistol/*
* Sprites: pickups/ammo/clip/*
* Sprites: weapons/shotgun/* (except OSH2A0.png)

Pawel "nmn" Zarcynski and WildWeasel -- unknown original work:

* Sprites: effects/debris/emptyclips/bfg9000/*
[//]: # (</ExcludeFromHudPackage>)

Nash Muhandes -- original work (public domain):

* Graphic: graphics/hud/RR_KILLS.png
* Graphic: graphics/hud/RR_ITEMS.png
* Graphic: graphics/hud/RR_SCRTS.png
[//]: # (<ExcludeFromHudPackage>)

Unknown artist -- unknown original work:

* Sprites: effects/debris/emptyclips/plasmarifle/*
* Sprites: effects/debris/emptyclips/railgun/* (modified)
* Sprites: pickups/ammo/rail/*
* Sprites: pickups/armors/*
* Sprites: weapons/fist/*
* Sprites: weapons/railgun/* (except RSC\*.png)
* Sound: weapons/bfg9000/DSPUCHRG.wav
* Sound: weapons/plasmarifle/DSBLCHRG.flac (first part, modified)
* Sound: weapons/plasmarifle/DSBLDCHR.wav (modified)

## Resource list -- Other games

3D Realms -- Duke Nukem 3D:

* Sprite: weapons/shotgun/OSH2A0.png

Bungie -- Halo: Combat Evolved:

* Sprites: effects/spark/*

Epic Games -- Unreal:

* Sound: pickups/DSADRNUP.wav
* Sound: pickups/DSARMRUP.wav
* Sound: pickups/DSHLTHUP.wav
* Sound: pickups/DSRRITEM.wav
* Sound: pickups/HEALTH1.wav
* Sound: pickups/SBELTHE2.wav
* Sound: pickups/suitsnd.wav
* Sound: pickups/VoiceSnd.wav
* Sound: weapons/chainsaw/DSCPCKUP.wav
* Sound: weapons/chainsaw/DSCUPGRD.wav

Epic Games -- Unreal Tournament 2004:

* Sprite: effects/bubble/BUBLA0.png

Gearbox Software -- Half-Life: Opposing Force

* Sprite: effects/laser/LASRA0.png

id Software -- Doom:

* Graphic: graphics/hud/RR_CSWA.png (modified)
* Graphic: graphics/hud/RR_CSWUA.png (modified)
* Graphic: graphics/hud/RR_CSWUH.png (modified)
* Graphic: graphics/hud/RR_FRAGS.png (modified)
* Sprites: pickups/ammo/shell/*
* Sprite: effects/rocketexhaust/* (modified)
* Sprite: weapons/chainsaw/ASAW*.png (animation by GampiSora)

id Software -- Quake II:

* Sprite: pickups/ammo/cell/* (modified)
* Sound: pickups/ar2_pkup.wav
* Sound: pickups/DSCELLUP.wav
* Sound: pickups/DSKEYUP.wav (modified)

id Software -- Doom 3:

* Sounds: projectiles/bfgball/*
* Sounds: weapons/bfg9000/* (except DSPUCHRG.wav)
* Sounds: weapons/chaingun/*
* Sounds: weapons/chainsaw/* (except DSCPCKUP.wav, DSCUPGRD.wav)
* Sounds: weapons/fist/*
* Sounds: weapons/pistol/* (except ROTTPST\*.wav)
* Sounds: weapons/supershotgun/*
* Sound: weapons/plasmarifle/DSHYPCLK.wav

Interceptor Entertainment -- Rise of the Triad (2013):

* Sounds: weapons/pistol/ROTTPST*.wav (modified)

Legend Entertainment -- Unreal II:

* Sounds: weapons/plasmarifle/DSBLCHRG.flac (second part)

Midway Games -- Doom 64:

* Sprites: projectiles/plasmaball/* (modified)

Raven Software -- Quake 4:

* Sound: pickups/DSAMMOUP.ogg
* Sounds: projectiles/plasmaball/*
* Sounds: projectiles/rocket/*.ogg
* Sound: weapons/pistol/DSPLASER.ogg
* Sounds: weapons/plasmarifle/*.ogg
* Sounds: weapons/railgun/*.ogg
* Sound: weapons/railgun/RAILCHRG.wav (modified)
* Sounds: weapons/shotgun/*.ogg

Rogue Entertainment -- Strife:

* Sprites: pickups/misc/* (modified)

Valve Software -- Half-Life 2:

* Sound: projectiles/rocket/DSSMISSL.wav
[//]: # (</ExcludeFromHudPackage>)

## Additional credits

* Dem -- suggestions, language fixes, testing
* StroggVorbis -- suggestions
* TDRR -- testing
* Void Weaver -- testing

**Special thanks to the GZDoom development team and to Graf Zahl personally for making it possible to create RRWM.**
